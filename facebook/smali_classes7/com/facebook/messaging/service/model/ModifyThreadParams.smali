.class public Lcom/facebook/messaging/service/model/ModifyThreadParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/ModifyThreadParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:Ljava/lang/String;

.field public final c:Z

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final f:Z

.field public final g:Z

.field public final h:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field public final i:Z

.field public final j:Z

.field public final k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

.field public final l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Z

.field public final o:I

.field public final p:I

.field public final q:LX/03R;

.field public final r:LX/03R;

.field public final s:Ljava/lang/String;

.field public final t:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129673
    new-instance v0, LX/6if;

    invoke-direct {v0}, LX/6if;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6ih;)V
    .locals 1

    .prologue
    .line 1129674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129675
    iget-object v0, p1, LX/6ih;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v0

    .line 1129676
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1129677
    iget-object v0, p1, LX/6ih;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1129678
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->b:Ljava/lang/String;

    .line 1129679
    iget-boolean v0, p1, LX/6ih;->c:Z

    move v0, v0

    .line 1129680
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->c:Z

    .line 1129681
    iget-object v0, p1, LX/6ih;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1129682
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->d:Ljava/lang/String;

    .line 1129683
    iget-boolean v0, p1, LX/6ih;->e:Z

    move v0, v0

    .line 1129684
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->f:Z

    .line 1129685
    iget-object v0, p1, LX/6ih;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    move-object v0, v0

    .line 1129686
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1129687
    iget-boolean v0, p1, LX/6ih;->g:Z

    move v0, v0

    .line 1129688
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->g:Z

    .line 1129689
    iget-object v0, p1, LX/6ih;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-object v0, v0

    .line 1129690
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1129691
    iget-boolean v0, p1, LX/6ih;->i:Z

    move v0, v0

    .line 1129692
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->i:Z

    .line 1129693
    iget-boolean v0, p1, LX/6ih;->j:Z

    move v0, v0

    .line 1129694
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->j:Z

    .line 1129695
    iget-object v0, p1, LX/6ih;->k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    move-object v0, v0

    .line 1129696
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    .line 1129697
    iget-object v0, p1, LX/6ih;->l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-object v0, v0

    .line 1129698
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1129699
    iget-object v0, p1, LX/6ih;->m:Ljava/lang/String;

    move-object v0, v0

    .line 1129700
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->m:Ljava/lang/String;

    .line 1129701
    iget-boolean v0, p1, LX/6ih;->n:Z

    move v0, v0

    .line 1129702
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->n:Z

    .line 1129703
    iget v0, p1, LX/6ih;->o:I

    move v0, v0

    .line 1129704
    iput v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->o:I

    .line 1129705
    iget v0, p1, LX/6ih;->p:I

    move v0, v0

    .line 1129706
    iput v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->p:I

    .line 1129707
    iget-object v0, p1, LX/6ih;->q:LX/03R;

    move-object v0, v0

    .line 1129708
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->q:LX/03R;

    .line 1129709
    iget-object v0, p1, LX/6ih;->r:Ljava/lang/String;

    move-object v0, v0

    .line 1129710
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->s:Ljava/lang/String;

    .line 1129711
    iget-object v0, p1, LX/6ih;->s:LX/03R;

    move-object v0, v0

    .line 1129712
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->t:LX/03R;

    .line 1129713
    iget-object v0, p1, LX/6ih;->t:LX/03R;

    move-object v0, v0

    .line 1129714
    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->r:LX/03R;

    .line 1129715
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1129716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129717
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1129718
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->b:Ljava/lang/String;

    .line 1129719
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->c:Z

    .line 1129720
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->d:Ljava/lang/String;

    .line 1129721
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->f:Z

    .line 1129722
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1129723
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->g:Z

    .line 1129724
    const-class v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1129725
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->i:Z

    .line 1129726
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->j:Z

    .line 1129727
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1129728
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->n:Z

    .line 1129729
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->o:I

    .line 1129730
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->p:I

    .line 1129731
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->m:Ljava/lang/String;

    .line 1129732
    const-class v0, Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    .line 1129733
    invoke-static {p1}, LX/46R;->f(Landroid/os/Parcel;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->q:LX/03R;

    .line 1129734
    invoke-static {p1}, LX/46R;->f(Landroid/os/Parcel;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->r:LX/03R;

    .line 1129735
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->s:Ljava/lang/String;

    .line 1129736
    invoke-static {p1}, LX/46R;->f(Landroid/os/Parcel;)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->t:LX/03R;

    .line 1129737
    return-void

    :cond_0
    move v0, v2

    .line 1129738
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1129739
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 1129740
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1129741
    goto :goto_3

    :cond_4
    move v1, v2

    .line 1129742
    goto :goto_4
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129743
    const/4 v0, 0x0

    return v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1129744
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-nez v0, :cond_0

    .line 1129745
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->b:Ljava/lang/String;

    .line 1129746
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1129747
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129748
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129749
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129750
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->f:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129751
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->e:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129752
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->g:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129753
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129754
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->i:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129755
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->j:Z

    if-eqz v0, :cond_4

    :goto_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129756
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129757
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->n:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1129758
    iget v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->o:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129759
    iget v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->p:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129760
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129761
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129762
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->q:LX/03R;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/03R;)V

    .line 1129763
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->r:LX/03R;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/03R;)V

    .line 1129764
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129765
    iget-object v0, p0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->t:LX/03R;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;LX/03R;)V

    .line 1129766
    return-void

    :cond_0
    move v0, v2

    .line 1129767
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1129768
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1129769
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1129770
    goto :goto_3

    :cond_4
    move v1, v2

    .line 1129771
    goto :goto_4
.end method
