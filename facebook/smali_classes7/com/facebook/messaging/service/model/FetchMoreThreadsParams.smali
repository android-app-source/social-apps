.class public Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6ek;

.field public final b:LX/6em;

.field public final c:J

.field private final d:J

.field public final e:I

.field public final f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6el;",
            ">;"
        }
    .end annotation
.end field

.field public final g:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128961
    new-instance v0, LX/6i8;

    invoke-direct {v0}, LX/6i8;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6ek;JJI)V
    .locals 12

    .prologue
    .line 1128959
    sget-object v3, LX/6em;->ALL:LX/6em;

    const-wide/16 v9, -0x1

    invoke-static {}, LX/0Rf;->of()LX/0Rf;

    move-result-object v11

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v11}, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;-><init>(LX/6ek;LX/6em;JJIJLX/0Rf;)V

    .line 1128960
    return-void
.end method

.method private constructor <init>(LX/6ek;LX/6em;JJIJLX/0Rf;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6ek;",
            "LX/6em;",
            "JJIJ",
            "LX/0Rf",
            "<",
            "LX/6el;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1128981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128982
    iput-object p1, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    .line 1128983
    iput-object p2, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->b:LX/6em;

    .line 1128984
    iput-wide p3, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->c:J

    .line 1128985
    iput-wide p5, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->d:J

    .line 1128986
    iput p7, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    .line 1128987
    iput-wide p8, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->g:J

    .line 1128988
    iput-object p10, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->f:LX/0Rf;

    .line 1128989
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1128971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128972
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    .line 1128973
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6em;->valueOf(Ljava/lang/String;)LX/6em;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->b:LX/6em;

    .line 1128974
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->c:J

    .line 1128975
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->d:J

    .line 1128976
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    .line 1128977
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->g:J

    .line 1128978
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Rf;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->f:LX/0Rf;

    .line 1128979
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128980
    const/4 v0, 0x0

    return v0
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 1128970
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->g:J

    return-wide v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1128962
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    iget-object v0, v0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128963
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->b:LX/6em;

    invoke-virtual {v0}, LX/6em;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128964
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128965
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128966
    iget v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1128967
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128968
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->f:LX/0Rf;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1128969
    return-void
.end method
