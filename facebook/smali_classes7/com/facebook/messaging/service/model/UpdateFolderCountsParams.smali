.class public Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6ek;

.field public final b:I

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130270
    new-instance v0, LX/6j8;

    invoke-direct {v0}, LX/6j8;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6ek;II)V
    .locals 0

    .prologue
    .line 1130271
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130272
    iput-object p1, p0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->a:LX/6ek;

    .line 1130273
    iput p2, p0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->b:I

    .line 1130274
    iput p3, p0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->c:I

    .line 1130275
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1130276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130277
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/6ek;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->a:LX/6ek;

    .line 1130278
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->b:I

    .line 1130279
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->c:I

    .line 1130280
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130281
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1130282
    iget-object v0, p0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->a:LX/6ek;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1130283
    iget v0, p0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1130284
    iget v0, p0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1130285
    return-void
.end method
