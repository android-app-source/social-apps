.class public Lcom/facebook/messaging/service/model/EditUsernameParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/EditUsernameParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128662
    const-string v0, "editUsernameParams"

    sput-object v0, Lcom/facebook/messaging/service/model/EditUsernameParams;->a:Ljava/lang/String;

    .line 1128663
    new-instance v0, LX/6hn;

    invoke-direct {v0}, LX/6hn;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/EditUsernameParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1128664
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128665
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/EditUsernameParams;->b:Ljava/lang/String;

    .line 1128666
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1128667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128668
    iput-object p1, p0, Lcom/facebook/messaging/service/model/EditUsernameParams;->b:Ljava/lang/String;

    .line 1128669
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128670
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1128671
    iget-object v0, p0, Lcom/facebook/messaging/service/model/EditUsernameParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128672
    return-void
.end method
