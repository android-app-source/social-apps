.class public Lcom/facebook/messaging/service/model/SendMessageParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/SendMessageParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/messages/Message;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final b:Z

.field public final c:Lcom/facebook/fbtrace/FbTraceNode;

.field public final d:I

.field public final e:J

.field public final f:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130163
    new-instance v0, LX/6iy;

    invoke-direct {v0}, LX/6iy;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/SendMessageParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1130154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130155
    const-class v0, Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 1130156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->b:Z

    .line 1130157
    const-class v0, Lcom/facebook/fbtrace/FbTraceNode;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbtrace/FbTraceNode;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 1130158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->d:I

    .line 1130159
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->e:J

    .line 1130160
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->f:J

    .line 1130161
    return-void

    .line 1130162
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/messages/Message;ZLcom/facebook/fbtrace/FbTraceNode;IJJ)V
    .locals 1
    .param p1    # Lcom/facebook/messaging/model/messages/Message;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1130146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130147
    iput-object p1, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    .line 1130148
    iput-boolean p2, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->b:Z

    .line 1130149
    iput-object p3, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    .line 1130150
    iput p4, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->d:I

    .line 1130151
    iput-wide p5, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->e:J

    .line 1130152
    iput-wide p7, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->f:J

    .line 1130153
    return-void
.end method

.method public static a()LX/6iz;
    .locals 1

    .prologue
    .line 1130136
    new-instance v0, LX/6iz;

    invoke-direct {v0}, LX/6iz;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130145
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1130137
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1130138
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1130139
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->c:Lcom/facebook/fbtrace/FbTraceNode;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1130140
    iget v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1130141
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1130142
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/SendMessageParams;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1130143
    return-void

    .line 1130144
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
