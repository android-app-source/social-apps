.class public Lcom/facebook/messaging/service/model/FetchMoreSpacesParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchMoreSpacesParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0rS;

.field private final b:J

.field private final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128933
    new-instance v0, LX/6i4;

    invoke-direct {v0}, LX/6i4;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchMoreSpacesParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1128928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128929
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreSpacesParams;->a:LX/0rS;

    .line 1128930
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreSpacesParams;->b:J

    .line 1128931
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/FetchMoreSpacesParams;->c:I

    .line 1128932
    return-void
.end method

.method public static newBuilder()LX/6i5;
    .locals 1

    .prologue
    .line 1128927
    new-instance v0, LX/6i5;

    invoke-direct {v0}, LX/6i5;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128922
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1128923
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreSpacesParams;->a:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128924
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreSpacesParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128925
    iget v0, p0, Lcom/facebook/messaging/service/model/FetchMoreSpacesParams;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1128926
    return-void
.end method
