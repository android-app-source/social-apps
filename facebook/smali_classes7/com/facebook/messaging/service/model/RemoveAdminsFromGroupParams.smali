.class public Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/messaging/model/threadkey/ThreadKey;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129922
    const-string v0, "removeAdminsFromGroupParams"

    sput-object v0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->a:Ljava/lang/String;

    .line 1129923
    new-instance v0, LX/6im;

    invoke-direct {v0}, LX/6im;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1129924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129925
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1129926
    invoke-virtual {p2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->c()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1129927
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1129928
    invoke-virtual {p1}, LX/0Px;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 1129929
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v0

    sget-object v2, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0, v2}, LX/0XG;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_1

    .line 1129930
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1129931
    :cond_1
    iput-object p1, p0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->b:LX/0Px;

    .line 1129932
    iput-object p2, p0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1129933
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1129934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129935
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->b:LX/0Px;

    .line 1129936
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1129937
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129938
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1129939
    iget-object v0, p0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1129940
    iget-object v0, p0, Lcom/facebook/messaging/service/model/RemoveAdminsFromGroupParams;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129941
    return-void
.end method
