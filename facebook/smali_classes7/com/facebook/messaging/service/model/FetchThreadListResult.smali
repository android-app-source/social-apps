.class public Lcom/facebook/messaging/service/model/FetchThreadListResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadListResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public final b:LX/6ek;

.field public final c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/messaging/model/folders/FolderCounts;

.field public final h:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field public final i:Z

.field public final j:J

.field public final k:J

.field public final l:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129218
    new-instance v0, LX/6iJ;

    invoke-direct {v0}, LX/6iJ;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6iK;)V
    .locals 4

    .prologue
    .line 1129219
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129220
    iget-object v0, p1, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-object v0, v0

    .line 1129221
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1129222
    iget-object v0, p1, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-object v0, v0

    .line 1129223
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1129224
    iget-object v0, p1, LX/6iK;->b:LX/6ek;

    move-object v0, v0

    .line 1129225
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->b:LX/6ek;

    .line 1129226
    iget-object v0, p1, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-object v0, v0

    .line 1129227
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1129228
    iget-object v0, p1, LX/6iK;->d:Ljava/util/List;

    move-object v0, v0

    .line 1129229
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    .line 1129230
    iget-object v0, p1, LX/6iK;->e:Ljava/util/List;

    move-object v0, v0

    .line 1129231
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->e:LX/0Px;

    .line 1129232
    iget-object v0, p1, LX/6iK;->f:Ljava/util/List;

    move-object v0, v0

    .line 1129233
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->f:LX/0Px;

    .line 1129234
    iget-object v0, p1, LX/6iK;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    move-object v0, v0

    .line 1129235
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 1129236
    iget-object v0, p1, LX/6iK;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-object v0, v0

    .line 1129237
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1129238
    iget-boolean v0, p1, LX/6iK;->i:Z

    move v0, v0

    .line 1129239
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->i:Z

    .line 1129240
    iget-wide v2, p1, LX/6iK;->k:J

    move-wide v0, v2

    .line 1129241
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->j:J

    .line 1129242
    iget-wide v2, p1, LX/6iK;->l:J

    move-wide v0, v2

    .line 1129243
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->k:J

    .line 1129244
    iget-wide v2, p1, LX/6iK;->j:J

    move-wide v0, v2

    .line 1129245
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    .line 1129246
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1129247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129248
    const-class v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1129249
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->b:LX/6ek;

    .line 1129250
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1129251
    const-class v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    .line 1129252
    const-class v0, Lcom/facebook/messaging/model/folders/FolderCounts;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/folders/FolderCounts;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    .line 1129253
    const-class v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1129254
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->e:LX/0Px;

    .line 1129255
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->f:LX/0Px;

    .line 1129256
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->i:Z

    .line 1129257
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->j:J

    .line 1129258
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->k:J

    .line 1129259
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    .line 1129260
    return-void
.end method

.method public static newBuilder()LX/6iK;
    .locals 1

    .prologue
    .line 1129261
    new-instance v0, LX/6iK;

    invoke-direct {v0}, LX/6iK;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129262
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1129263
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129264
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->b:LX/6ek;

    iget-object v0, v0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129265
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129266
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1129267
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129268
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129269
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1129270
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->f:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1129271
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1129272
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129273
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->k:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129274
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->l:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129275
    return-void
.end method
