.class public Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/send/SendError;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/messaging/model/threadkey/ThreadKey;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130302
    new-instance v0, LX/6jA;

    invoke-direct {v0}, LX/6jA;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1130307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130308
    const-class v0, Lcom/facebook/messaging/model/send/SendError;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/send/SendError;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->a:Lcom/facebook/messaging/model/send/SendError;

    .line 1130309
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->b:Ljava/lang/String;

    .line 1130310
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1130311
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130312
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1130303
    iget-object v0, p0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->a:Lcom/facebook/messaging/model/send/SendError;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1130304
    iget-object v0, p0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130305
    iget-object v0, p0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1130306
    return-void
.end method
