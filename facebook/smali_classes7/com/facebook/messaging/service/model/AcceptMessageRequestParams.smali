.class public Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128408
    const-string v0, "acceptMessageRequestParams"

    sput-object v0, Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;->a:Ljava/lang/String;

    .line 1128409
    new-instance v0, LX/6hX;

    invoke-direct {v0}, LX/6hX;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1128410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128411
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;->b:J

    .line 1128412
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128413
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1128414
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/AcceptMessageRequestParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128415
    return-void
.end method
