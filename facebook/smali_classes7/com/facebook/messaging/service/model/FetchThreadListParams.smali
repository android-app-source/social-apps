.class public Lcom/facebook/messaging/service/model/FetchThreadListParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadListParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0rS;

.field public final b:LX/6ek;

.field public final c:LX/6em;

.field public final d:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/6el;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/6iH;

.field public final f:J

.field private final g:I

.field public final h:Lcom/facebook/http/interfaces/RequestPriority;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129190
    new-instance v0, LX/6iG;

    invoke-direct {v0}, LX/6iG;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6iI;)V
    .locals 4

    .prologue
    .line 1129133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129134
    iget-object v0, p1, LX/6iI;->a:LX/0rS;

    move-object v0, v0

    .line 1129135
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    .line 1129136
    iget-object v0, p1, LX/6iI;->b:LX/6ek;

    move-object v0, v0

    .line 1129137
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    .line 1129138
    iget-object v0, p1, LX/6iI;->c:LX/6em;

    move-object v0, v0

    .line 1129139
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->c:LX/6em;

    .line 1129140
    iget-object v0, p1, LX/6iI;->d:LX/0Rf;

    move-object v0, v0

    .line 1129141
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->d:LX/0Rf;

    .line 1129142
    iget-object v0, p1, LX/6iI;->h:LX/6iH;

    move-object v0, v0

    .line 1129143
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->e:LX/6iH;

    .line 1129144
    iget-wide v2, p1, LX/6iI;->e:J

    move-wide v0, v2

    .line 1129145
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->f:J

    .line 1129146
    iget v0, p1, LX/6iI;->f:I

    move v0, v0

    .line 1129147
    iput v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g:I

    .line 1129148
    iget-object v0, p1, LX/6iI;->g:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v0, v0

    .line 1129149
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->h:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1129150
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1129180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129181
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    .line 1129182
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    .line 1129183
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6em;->valueOf(Ljava/lang/String;)LX/6em;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->c:LX/6em;

    .line 1129184
    const-class v0, LX/6el;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->d:LX/0Rf;

    .line 1129185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6iH;->valueOf(Ljava/lang/String;)LX/6iH;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->e:LX/6iH;

    .line 1129186
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->f:J

    .line 1129187
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g:I

    .line 1129188
    const-class v0, Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/interfaces/RequestPriority;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->h:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1129189
    return-void
.end method

.method public static newBuilder()LX/6iI;
    .locals 1

    .prologue
    .line 1129179
    new-instance v0, LX/6iI;

    invoke-direct {v0}, LX/6iI;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129178
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1129166
    if-ne p0, p1, :cond_1

    .line 1129167
    :cond_0
    :goto_0
    return v0

    .line 1129168
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1129169
    :cond_3
    check-cast p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 1129170
    iget-wide v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->f:J

    iget-wide v4, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 1129171
    :cond_4
    iget v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g:I

    iget v3, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 1129172
    :cond_5
    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    .line 1129173
    :cond_6
    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    .line 1129174
    :cond_7
    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->c:LX/6em;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->c:LX/6em;

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    .line 1129175
    :cond_8
    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->d:LX/0Rf;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->d:LX/0Rf;

    invoke-virtual {v2, v3}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    .line 1129176
    :cond_9
    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->e:LX/6iH;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->e:LX/6iH;

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    .line 1129177
    :cond_a
    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->h:Lcom/facebook/http/interfaces/RequestPriority;

    iget-object v3, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->h:Lcom/facebook/http/interfaces/RequestPriority;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final g()I
    .locals 2

    .prologue
    .line 1129165
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1129151
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->hashCode()I

    move-result v0

    .line 1129152
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    invoke-virtual {v0}, LX/6ek;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 1129153
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->c:LX/6em;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->c:LX/6em;

    invoke-virtual {v0}, LX/6em;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 1129154
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->d:LX/0Rf;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->d:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 1129155
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->e:LX/6iH;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->e:LX/6iH;

    invoke-virtual {v0}, LX/6iH;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 1129156
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->f:J

    iget-wide v4, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->f:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1129157
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g:I

    add-int/2addr v0, v2

    .line 1129158
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->h:Lcom/facebook/http/interfaces/RequestPriority;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->h:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1}, Lcom/facebook/http/interfaces/RequestPriority;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 1129159
    return v0

    :cond_1
    move v0, v1

    .line 1129160
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1129161
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1129162
    goto :goto_2

    :cond_4
    move v0, v1

    .line 1129163
    goto :goto_3

    :cond_5
    move v0, v1

    .line 1129164
    goto :goto_4
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1129124
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129125
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    iget-object v0, v0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129126
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->c:LX/6em;

    invoke-virtual {v0}, LX/6em;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129127
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->d:LX/0Rf;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1129128
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->e:LX/6iH;

    invoke-virtual {v0}, LX/6iH;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129129
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129130
    iget v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129131
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->h:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129132
    return-void
.end method
