.class public Lcom/facebook/messaging/service/model/SetSettingsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/SetSettingsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Z

.field public b:Lcom/facebook/messaging/model/threads/NotificationSetting;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130215
    new-instance v0, LX/6j3;

    invoke-direct {v0}, LX/6j3;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/SetSettingsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6j4;)V
    .locals 1

    .prologue
    .line 1130216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130217
    iget-boolean v0, p1, LX/6j4;->a:Z

    move v0, v0

    .line 1130218
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/SetSettingsParams;->a:Z

    .line 1130219
    iget-object v0, p1, LX/6j4;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-object v0, v0

    .line 1130220
    iput-object v0, p0, Lcom/facebook/messaging/service/model/SetSettingsParams;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1130221
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1130222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130223
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/SetSettingsParams;->a:Z

    .line 1130224
    const-class v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/NotificationSetting;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SetSettingsParams;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 1130225
    return-void

    .line 1130226
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130227
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1130228
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/SetSettingsParams;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1130229
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SetSettingsParams;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1130230
    return-void

    .line 1130231
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
