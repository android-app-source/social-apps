.class public Lcom/facebook/messaging/service/model/EditUsernameResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/EditUsernameResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128682
    new-instance v0, LX/6ho;

    invoke-direct {v0}, LX/6ho;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/EditUsernameResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1128683
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128684
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/EditUsernameResult;->a:Ljava/lang/String;

    .line 1128685
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1128679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128680
    iput-object p1, p0, Lcom/facebook/messaging/service/model/EditUsernameResult;->a:Ljava/lang/String;

    .line 1128681
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128678
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1128676
    iget-object v0, p0, Lcom/facebook/messaging/service/model/EditUsernameResult;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128677
    return-void
.end method
