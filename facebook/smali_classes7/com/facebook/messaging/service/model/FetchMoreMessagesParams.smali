.class public Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field private final b:Ljava/lang/String;

.field public final c:J

.field public final d:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128880
    new-instance v0, LX/6i2;

    invoke-direct {v0}, LX/6i2;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1128881
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128882
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1128883
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->b:Ljava/lang/String;

    .line 1128884
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->c:J

    .line 1128885
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->d:I

    .line 1128886
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JI)V
    .locals 1

    .prologue
    .line 1128887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128888
    iput-object p1, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1128889
    iput-object p2, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->b:Ljava/lang/String;

    .line 1128890
    iput-wide p3, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->c:J

    .line 1128891
    iput p5, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->d:I

    .line 1128892
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128893
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1128894
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1128895
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128896
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128897
    iget v0, p0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1128898
    return-void
.end method
