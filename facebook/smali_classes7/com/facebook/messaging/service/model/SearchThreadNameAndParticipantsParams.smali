.class public Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130009
    new-instance v0, LX/6ir;

    invoke-direct {v0}, LX/6ir;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6is;)V
    .locals 1

    .prologue
    .line 1130010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130011
    iget v0, p1, LX/6is;->a:I

    move v0, v0

    .line 1130012
    iput v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->a:I

    .line 1130013
    iget-object v0, p1, LX/6is;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1130014
    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->b:Ljava/lang/String;

    .line 1130015
    iget-boolean v0, p1, LX/6is;->c:Z

    move v0, v0

    .line 1130016
    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->c:Z

    .line 1130017
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1130018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130019
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->a:I

    .line 1130020
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->b:Ljava/lang/String;

    .line 1130021
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->c:Z

    .line 1130022
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130023
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1130024
    iget v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1130025
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130026
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1130027
    return-void
.end method
