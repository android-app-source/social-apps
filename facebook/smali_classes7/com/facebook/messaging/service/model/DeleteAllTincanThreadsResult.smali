.class public Lcom/facebook/messaging/service/model/DeleteAllTincanThreadsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/DeleteAllTincanThreadsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128540
    new-instance v0, LX/6hg;

    invoke-direct {v0}, LX/6hg;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/DeleteAllTincanThreadsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1128541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128542
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArray(Ljava/lang/ClassLoader;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/messaging/model/threadkey/ThreadKey;

    check-cast v0, [Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1128543
    invoke-static {v0}, LX/0Rf;->copyOf([Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/DeleteAllTincanThreadsResult;->a:LX/0Rf;

    .line 1128544
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128545
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1128546
    iget-object v0, p0, Lcom/facebook/messaging/service/model/DeleteAllTincanThreadsResult;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/messaging/model/threadkey/ThreadKey;

    check-cast v0, [Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 1128547
    return-void
.end method
