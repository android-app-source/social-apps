.class public Lcom/facebook/messaging/service/model/DeliveredReceiptParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/DeliveredReceiptParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:Lcom/facebook/user/model/UserKey;

.field private final c:Ljava/lang/String;

.field public final d:J

.field public final e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128637
    new-instance v0, LX/6hm;

    invoke-direct {v0}, LX/6hm;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1128638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128639
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1128640
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->b:Lcom/facebook/user/model/UserKey;

    .line 1128641
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->c:Ljava/lang/String;

    .line 1128642
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->d:J

    .line 1128643
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->e:J

    .line 1128644
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/user/model/UserKey;Ljava/lang/String;JJ)V
    .locals 0

    .prologue
    .line 1128645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128646
    iput-object p1, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1128647
    iput-object p2, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->b:Lcom/facebook/user/model/UserKey;

    .line 1128648
    iput-object p3, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->c:Ljava/lang/String;

    .line 1128649
    iput-wide p4, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->d:J

    .line 1128650
    iput-wide p6, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->e:J

    .line 1128651
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128652
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1128653
    iget-object v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1128654
    iget-object v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128655
    iget-object v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128656
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128657
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/DeliveredReceiptParams;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128658
    return-void
.end method
