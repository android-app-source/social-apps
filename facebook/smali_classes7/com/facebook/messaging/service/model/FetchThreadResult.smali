.class public Lcom/facebook/messaging/service/model/FetchThreadResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/service/model/FetchThreadResult;


# instance fields
.field public final b:LX/6j5;

.field public final c:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public final d:Lcom/facebook/messaging/model/threads/ThreadSummary;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/messaging/model/messages/MessagesCollection;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final g:J

.field public final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v4, 0x0

    .line 1129452
    new-instance v1, Lcom/facebook/messaging/service/model/FetchThreadResult;

    sget-object v2, LX/6j5;->UNSPECIFIED:LX/6j5;

    sget-object v3, Lcom/facebook/fbservice/results/DataFetchDisposition;->NO_DATA:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1129453
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v7, v0

    .line 1129454
    const-wide/16 v8, -0x1

    .line 1129455
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v10, v0

    .line 1129456
    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v1 .. v10}, Lcom/facebook/messaging/service/model/FetchThreadResult;-><init>(LX/6j5;Lcom/facebook/fbservice/results/DataFetchDisposition;Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessagesCollection;Ljava/util/Map;LX/0Px;JLX/0Px;)V

    sput-object v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 1129457
    new-instance v0, LX/6iN;

    invoke-direct {v0}, LX/6iN;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(LX/6j5;Lcom/facebook/fbservice/results/DataFetchDisposition;Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessagesCollection;Ljava/util/Map;LX/0Px;JLX/0Px;)V
    .locals 1
    .param p3    # Lcom/facebook/messaging/model/threads/ThreadSummary;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/6j5;",
            "Lcom/facebook/fbservice/results/DataFetchDisposition;",
            "Lcom/facebook/messaging/model/threads/ThreadSummary;",
            "Lcom/facebook/messaging/model/messages/MessagesCollection;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;J",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1129442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129443
    iput-object p1, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->b:LX/6j5;

    .line 1129444
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1129445
    iput-object p3, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 1129446
    iput-object p4, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 1129447
    iput-object p6, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    .line 1129448
    iput-object p5, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->i:Ljava/util/Map;

    .line 1129449
    iput-wide p7, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    .line 1129450
    invoke-static {p9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->h:LX/0Px;

    .line 1129451
    return-void
.end method

.method public synthetic constructor <init>(LX/6j5;Lcom/facebook/fbservice/results/DataFetchDisposition;Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessagesCollection;Ljava/util/Map;LX/0Px;JLX/0Px;B)V
    .locals 1

    .prologue
    .line 1129441
    invoke-direct/range {p0 .. p9}, Lcom/facebook/messaging/service/model/FetchThreadResult;-><init>(LX/6j5;Lcom/facebook/fbservice/results/DataFetchDisposition;Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessagesCollection;Ljava/util/Map;LX/0Px;JLX/0Px;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1129427
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129428
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6j5;->valueOf(Ljava/lang/String;)LX/6j5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->b:LX/6j5;

    .line 1129429
    const-class v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1129430
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 1129431
    const-class v0, Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/MessagesCollection;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 1129432
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1129433
    const-class v1, Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1129434
    const-class v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1129435
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    .line 1129436
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    .line 1129437
    const-class v0, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1129438
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->h:LX/0Px;

    .line 1129439
    return-void

    .line 1129440
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()LX/6iO;
    .locals 2

    .prologue
    .line 1129426
    new-instance v0, LX/6iO;

    invoke-direct {v0}, LX/6iO;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/service/model/FetchThreadResult;)LX/6iO;
    .locals 2

    .prologue
    .line 1129425
    new-instance v0, LX/6iO;

    invoke-direct {v0, p0}, LX/6iO;-><init>(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129424
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1129415
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->b:LX/6j5;

    invoke-virtual {v0}, LX/6j5;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129416
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129417
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129418
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129419
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->i:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1129420
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1129421
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129422
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadResult;->h:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1129423
    return-void
.end method
