.class public Lcom/facebook/messaging/service/model/SearchUserResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/SearchUserResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130090
    new-instance v0, LX/6iw;

    invoke-direct {v0}, LX/6iw;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/SearchUserResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1130091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130092
    const-class v0, Lcom/facebook/messaging/service/model/SearchUserParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->a:LX/0Px;

    .line 1130093
    const-class v0, Lcom/facebook/messaging/service/model/SearchUserParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->c:LX/0Px;

    .line 1130094
    const-class v0, Lcom/facebook/messaging/service/model/SearchUserParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->b:LX/0Px;

    .line 1130095
    const-class v0, Lcom/facebook/messaging/service/model/SearchUserParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->d:LX/0Px;

    .line 1130096
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->e:Ljava/lang/String;

    .line 1130097
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1130098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130099
    iput-object p2, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->a:LX/0Px;

    .line 1130100
    iput-object p3, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->b:LX/0Px;

    .line 1130101
    iput-object p4, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->c:LX/0Px;

    .line 1130102
    iput-object p5, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->d:LX/0Px;

    .line 1130103
    iput-object p1, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->e:Ljava/lang/String;

    .line 1130104
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130105
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1130106
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1130107
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1130108
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1130109
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1130110
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserResult;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130111
    return-void
.end method
