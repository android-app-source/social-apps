.class public Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;",
            ">;"
        }
    .end annotation
.end field

.field public static a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/facebook/messaging/model/threadkey/ThreadKey;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128831
    const-string v0, "fetchIsThreadQueueEnabledParams"

    sput-object v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->a:Ljava/lang/String;

    .line 1128832
    new-instance v0, LX/6hy;

    invoke-direct {v0}, LX/6hy;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1128825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128826
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1128827
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128830
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1128828
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1128829
    return-void
.end method
