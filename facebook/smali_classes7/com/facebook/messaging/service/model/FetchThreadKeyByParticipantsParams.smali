.class public Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/user/model/UserKey;

.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129088
    new-instance v0, LX/6iE;

    invoke-direct {v0}, LX/6iE;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1129089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129090
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-static {p1, v0}, LX/46R;->d(Landroid/os/Parcel;Ljava/lang/Class;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->a:Lcom/facebook/user/model/UserKey;

    .line 1129091
    invoke-static {p1}, LX/46R;->b(Landroid/os/Parcel;)Ljava/util/Set;

    move-result-object v0

    .line 1129092
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->b:LX/0Rf;

    .line 1129093
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->c:Z

    .line 1129094
    return-void
.end method

.method public static b(Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;Lcom/facebook/messaging/model/threads/ThreadSummary;)Z
    .locals 1

    .prologue
    .line 1129095
    iget-boolean v0, p1, Lcom/facebook/messaging/model/threads/ThreadSummary;->w:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/messaging/model/threads/ThreadSummary;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129096
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1129097
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->a:Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129098
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->b:LX/0Rf;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Set;)V

    .line 1129099
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1129100
    return-void
.end method
