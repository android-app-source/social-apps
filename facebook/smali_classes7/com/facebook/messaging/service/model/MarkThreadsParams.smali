.class public Lcom/facebook/messaging/service/model/MarkThreadsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/MarkThreadsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6iW;

.field public final b:Z

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/service/model/MarkThreadFields;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129635
    new-instance v0, LX/6id;

    invoke-direct {v0}, LX/6id;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6ie;)V
    .locals 2

    .prologue
    .line 1129636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129637
    iget-object v0, p1, LX/6ie;->a:LX/6iW;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    .line 1129638
    iget-boolean v0, p1, LX/6ie;->b:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->b:Z

    .line 1129639
    iget-object v0, p1, LX/6ie;->c:LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    .line 1129640
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1129641
    iget-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 1129642
    iget-object v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1129643
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1129644
    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->d:LX/0Px;

    .line 1129645
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1129646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129647
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6iW;->valueOf(Ljava/lang/String;)LX/6iW;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    .line 1129648
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->b:Z

    .line 1129649
    const-class v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    .line 1129650
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->d:LX/0Px;

    .line 1129651
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129652
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1129653
    iget-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    invoke-virtual {v0}, LX/6iW;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129654
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1129655
    iget-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1129656
    iget-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1129657
    return-void
.end method
