.class public Lcom/facebook/messaging/service/model/DeleteMessagesParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/DeleteMessagesParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/6hi;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128557
    new-instance v0, LX/6hh;

    invoke-direct {v0}, LX/6hh;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Rf;LX/6hi;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V
    .locals 0
    .param p3    # Lcom/facebook/messaging/model/threadkey/ThreadKey;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/6hi;",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1128558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128559
    iput-object p1, p0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    .line 1128560
    iput-object p2, p0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->c:LX/6hi;

    .line 1128561
    iput-object p3, p0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1128562
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1128563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128564
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    .line 1128565
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6hi;->valueOf(Ljava/lang/String;)LX/6hi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->c:LX/6hi;

    .line 1128566
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1128567
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128568
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1128569
    iget-object v0, p0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1128570
    iget-object v0, p0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->c:LX/6hi;

    invoke-virtual {v0}, LX/6hi;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128571
    iget-object v0, p0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1128572
    return-void
.end method
