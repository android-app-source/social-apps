.class public Lcom/facebook/messaging/service/model/MarkThreadFields;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/MarkThreadFields;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final b:Z

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:LX/6ek;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129586
    new-instance v0, LX/6iZ;

    invoke-direct {v0}, LX/6iZ;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6ia;)V
    .locals 2

    .prologue
    .line 1129587
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129588
    iget-object v0, p1, LX/6ia;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1129589
    iget-boolean v0, p1, LX/6ia;->b:Z

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    .line 1129590
    iget-wide v0, p1, LX/6ia;->c:J

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    .line 1129591
    iget-wide v0, p1, LX/6ia;->d:J

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    .line 1129592
    iget-wide v0, p1, LX/6ia;->e:J

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    .line 1129593
    iget-object v0, p1, LX/6ia;->f:LX/6ek;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->f:LX/6ek;

    .line 1129594
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1129595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129596
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1129597
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    .line 1129598
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    .line 1129599
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    .line 1129600
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    .line 1129601
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->f:LX/6ek;

    .line 1129602
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129603
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1129604
    iget-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129605
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1129606
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129607
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129608
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129609
    iget-object v0, p0, Lcom/facebook/messaging/service/model/MarkThreadFields;->f:LX/6ek;

    iget-object v0, v0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129610
    return-void
.end method
