.class public Lcom/facebook/messaging/service/model/SearchUserParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/SearchUserParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130086
    new-instance v0, LX/6iv;

    invoke-direct {v0}, LX/6iv;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/SearchUserParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1130082
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130083
    const-class v0, Lcom/facebook/messaging/service/model/SearchUserParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserParams;->a:LX/0Rf;

    .line 1130084
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserParams;->b:Ljava/lang/String;

    .line 1130085
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Rf;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1130078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130079
    iput-object p2, p0, Lcom/facebook/messaging/service/model/SearchUserParams;->a:LX/0Rf;

    .line 1130080
    iput-object p1, p0, Lcom/facebook/messaging/service/model/SearchUserParams;->b:Ljava/lang/String;

    .line 1130081
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1130077
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserParams;->a:LX/0Rf;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1130076
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserParams;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130072
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1130073
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserParams;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1130074
    iget-object v0, p0, Lcom/facebook/messaging/service/model/SearchUserParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130075
    return-void
.end method
