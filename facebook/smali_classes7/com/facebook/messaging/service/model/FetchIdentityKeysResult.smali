.class public Lcom/facebook/messaging/service/model/FetchIdentityKeysResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchIdentityKeysResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128813
    new-instance v0, LX/6hx;

    invoke-direct {v0}, LX/6hx;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchIdentityKeysResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1128814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128815
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1128816
    invoke-static {p1, v0}, LX/46R;->b(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1128817
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchIdentityKeysResult;->a:LX/0P1;

    .line 1128818
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128819
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1128820
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchIdentityKeysResult;->a:LX/0P1;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/util/Map;)V

    .line 1128821
    return-void
.end method
