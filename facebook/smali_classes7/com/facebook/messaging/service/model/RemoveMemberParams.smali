.class public Lcom/facebook/messaging/service/model/RemoveMemberParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/RemoveMemberParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserFbidIdentifier;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129951
    new-instance v0, LX/6in;

    invoke-direct {v0}, LX/6in;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1129952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129953
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1129954
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->a:Z

    .line 1129955
    const-class v0, Lcom/facebook/user/model/UserFbidIdentifier;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->c:LX/0Px;

    .line 1129956
    return-void
.end method

.method public constructor <init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;ZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/model/threadkey/ThreadKey;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/user/model/UserFbidIdentifier;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1129957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129958
    iput-object p1, p0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 1129959
    iput-boolean p2, p0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->a:Z

    .line 1129960
    invoke-static {p3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->c:LX/0Px;

    .line 1129961
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129962
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1129963
    iget-object v0, p0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129964
    iget-boolean v0, p0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1129965
    iget-object v0, p0, Lcom/facebook/messaging/service/model/RemoveMemberParams;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1129966
    return-void
.end method
