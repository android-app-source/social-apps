.class public Lcom/facebook/messaging/service/model/FetchTopThreadsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchTopThreadsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/6ek;

.field private final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1129505
    new-instance v0, LX/6iR;

    invoke-direct {v0}, LX/6iR;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchTopThreadsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1129501
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129502
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchTopThreadsParams;->a:LX/6ek;

    .line 1129503
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/messaging/service/model/FetchTopThreadsParams;->b:I

    .line 1129504
    return-void
.end method

.method public static newBuilder()LX/6iS;
    .locals 1

    .prologue
    .line 1129496
    new-instance v0, LX/6iS;

    invoke-direct {v0}, LX/6iS;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129500
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1129497
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchTopThreadsParams;->a:LX/6ek;

    iget-object v0, v0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129498
    iget v0, p0, Lcom/facebook/messaging/service/model/FetchTopThreadsParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1129499
    return-void
.end method
