.class public Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/fbservice/results/DataFetchDisposition;

.field public final b:LX/6ek;

.field public final c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128993
    new-instance v0, LX/6i9;

    invoke-direct {v0}, LX/6i9;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1128994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128995
    const-class v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/results/DataFetchDisposition;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1128996
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->b:LX/6ek;

    .line 1128997
    const-class v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1128998
    const-class v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->d:LX/0Px;

    .line 1128999
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->e:J

    .line 1129000
    return-void
.end method

.method public constructor <init>(Lcom/facebook/fbservice/results/DataFetchDisposition;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;LX/0Px;J)V
    .locals 1
    .param p4    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/fbservice/results/DataFetchDisposition;",
            "LX/6ek;",
            "Lcom/facebook/messaging/model/threads/ThreadsCollection;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 1129001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1129002
    iput-object p1, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 1129003
    iput-object p2, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->b:LX/6ek;

    .line 1129004
    iput-object p3, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 1129005
    if-nez p4, :cond_0

    .line 1129006
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object p4, v0

    .line 1129007
    :cond_0
    iput-object p4, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->d:LX/0Px;

    .line 1129008
    iput-wide p5, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->e:J

    .line 1129009
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1129010
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1129011
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129012
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->b:LX/6ek;

    iget-object v0, v0, LX/6ek;->dbName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1129013
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1129014
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1129015
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1129016
    return-void
.end method
