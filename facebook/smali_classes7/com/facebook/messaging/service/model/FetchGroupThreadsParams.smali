.class public Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;


# instance fields
.field public final b:LX/0rS;

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1128726
    new-instance v0, LX/6ht;

    invoke-direct {v0}, LX/6ht;-><init>()V

    .line 1128727
    new-instance v1, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;-><init>(LX/6ht;)V

    move-object v0, v1

    .line 1128728
    sput-object v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->a:Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 1128729
    new-instance v0, LX/6hs;

    invoke-direct {v0}, LX/6hs;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0rS;J)V
    .locals 0

    .prologue
    .line 1128730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128731
    iput-object p1, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    .line 1128732
    iput-wide p2, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->c:J

    .line 1128733
    return-void
.end method

.method public constructor <init>(LX/6ht;)V
    .locals 4

    .prologue
    .line 1128734
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128735
    iget-object v0, p1, LX/6ht;->a:LX/0rS;

    move-object v0, v0

    .line 1128736
    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    .line 1128737
    iget-wide v2, p1, LX/6ht;->b:J

    move-wide v0, v2

    .line 1128738
    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->c:J

    .line 1128739
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1128740
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128741
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    .line 1128742
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->c:J

    .line 1128743
    return-void
.end method

.method public static a(Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;LX/6iH;)Lcom/facebook/messaging/service/model/FetchThreadListParams;
    .locals 2

    .prologue
    .line 1128744
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    .line 1128745
    iput-object v1, v0, LX/6iI;->a:LX/0rS;

    .line 1128746
    move-object v0, v0

    .line 1128747
    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    .line 1128748
    iput-object v1, v0, LX/6iI;->b:LX/6ek;

    .line 1128749
    move-object v0, v0

    .line 1128750
    const/16 v1, 0xa

    .line 1128751
    iput v1, v0, LX/6iI;->f:I

    .line 1128752
    move-object v0, v0

    .line 1128753
    iput-object p1, v0, LX/6iI;->h:LX/6iH;

    .line 1128754
    move-object v0, v0

    .line 1128755
    invoke-virtual {v0}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder()LX/6ht;
    .locals 1

    .prologue
    .line 1128756
    new-instance v0, LX/6ht;

    invoke-direct {v0}, LX/6ht;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128757
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1128758
    iget-object v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128759
    iget-wide v0, p0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1128760
    return-void
.end method
