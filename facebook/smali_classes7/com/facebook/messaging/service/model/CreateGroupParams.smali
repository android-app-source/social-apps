.class public Lcom/facebook/messaging/service/model/CreateGroupParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/service/model/CreateGroupParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1128524
    new-instance v0, LX/6hd;

    invoke-direct {v0}, LX/6hd;-><init>()V

    sput-object v0, Lcom/facebook/messaging/service/model/CreateGroupParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/6he;)V
    .locals 1

    .prologue
    .line 1128502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128503
    iget-object v0, p1, LX/6he;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1128504
    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->a:Ljava/lang/String;

    .line 1128505
    iget-object v0, p1, LX/6he;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1128506
    iget-object v0, p1, LX/6he;->c:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->c:LX/0Px;

    .line 1128507
    iget-object v0, p1, LX/6he;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->d:Ljava/lang/String;

    .line 1128508
    iget-object v0, p1, LX/6he;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->e:Ljava/lang/String;

    .line 1128509
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1128510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1128511
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->a:Ljava/lang/String;

    .line 1128512
    const-class v0, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 1128513
    const-class v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->c:LX/0Px;

    .line 1128514
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->d:Ljava/lang/String;

    .line 1128515
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->e:Ljava/lang/String;

    .line 1128516
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1128517
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1128518
    iget-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128519
    iget-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1128520
    iget-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1128521
    iget-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128522
    iget-object v0, p0, Lcom/facebook/messaging/service/model/CreateGroupParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1128523
    return-void
.end method
