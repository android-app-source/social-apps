.class public Lcom/facebook/messaging/xma/ui/ActionLinkBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1143339
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1143340
    invoke-direct {p0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->a()V

    .line 1143341
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143336
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1143337
    invoke-direct {p0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->a()V

    .line 1143338
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143333
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1143334
    invoke-direct {p0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->a()V

    .line 1143335
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1143310
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->setOrientation(I)V

    .line 1143311
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1143312
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->setGravity(I)V

    .line 1143313
    return-void
.end method


# virtual methods
.method public setActionLinks(LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesInterfaces$XMAAttachmentStoryFields$ActionLinks;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1143314
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1143315
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;

    .line 1143316
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1143317
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1143318
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1143319
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1143320
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1143321
    invoke-virtual {p0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->removeAllViews()V

    .line 1143322
    :cond_2
    return-void

    .line 1143323
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->getChildCount()I

    move-result v0

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v2

    if-le v0, v2, :cond_4

    .line 1143324
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->removeViewAt(I)V

    goto :goto_1

    :cond_4
    move v2, v1

    .line 1143325
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1143326
    invoke-virtual {p0, v2}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/6mA;

    .line 1143327
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;

    invoke-virtual {v0, v1}, LX/6mA;->setActionLink(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;)V

    .line 1143328
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1143329
    :cond_5
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->getChildCount()I

    move-result v0

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 1143330
    new-instance v1, LX/6mA;

    invoke-virtual {p0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6mA;-><init>(Landroid/content/Context;)V

    .line 1143331
    invoke-virtual {p0}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->getChildCount()I

    move-result v0

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;

    invoke-virtual {v1, v0}, LX/6mA;->setActionLink(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;)V

    .line 1143332
    invoke-virtual {p0, v1}, Lcom/facebook/messaging/xma/ui/ActionLinkBar;->addView(Landroid/view/View;)V

    goto :goto_3
.end method
