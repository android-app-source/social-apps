.class public Lcom/facebook/messaging/xma/ui/XMALinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/6lq;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:LX/6mF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/6ll;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1143420
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1143421
    invoke-direct {p0}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a()V

    .line 1143422
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143423
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1143424
    invoke-direct {p0}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a()V

    .line 1143425
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143417
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1143418
    invoke-direct {p0}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a()V

    .line 1143419
    return-void
.end method

.method private a()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1143413
    const-class v0, Lcom/facebook/messaging/xma/ui/XMALinearLayout;

    invoke-static {v0, p0}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1143414
    iget-object v0, p0, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a:LX/6mF;

    new-instance v1, LX/6mD;

    invoke-direct {v1, p0}, LX/6mD;-><init>(Lcom/facebook/messaging/xma/ui/XMALinearLayout;)V

    .line 1143415
    iput-object v1, v0, LX/6mF;->b:LX/6m1;

    .line 1143416
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/messaging/xma/ui/XMALinearLayout;

    invoke-static {v0}, LX/6mF;->b(LX/0QB;)LX/6mF;

    move-result-object v0

    check-cast v0, LX/6mF;

    iput-object v0, p0, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a:LX/6mF;

    return-void
.end method


# virtual methods
.method public a(LX/6ll;)V
    .locals 0
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143412
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1143411
    iget-object v0, p0, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a:LX/6mF;

    invoke-virtual {v0, p1}, LX/6mF;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x6dba15cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1143406
    iget-object v1, p0, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a:LX/6mF;

    invoke-virtual {v1, p1}, LX/6mF;->b(Landroid/view/MotionEvent;)V

    .line 1143407
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x5cbd2df0

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setXMACallback(LX/6ll;)V
    .locals 0
    .param p1    # LX/6ll;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1143408
    iput-object p1, p0, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->b:LX/6ll;

    .line 1143409
    invoke-virtual {p0, p1}, Lcom/facebook/messaging/xma/ui/XMALinearLayout;->a(LX/6ll;)V

    .line 1143410
    return-void
.end method
