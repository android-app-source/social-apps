.class public Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1310695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1310696
    iput-object p1, p0, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;->a:Ljava/lang/String;

    .line 1310697
    iput p2, p0, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;->b:I

    .line 1310698
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1310699
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1310700
    iget-object v0, p0, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1310701
    iget v0, p0, Lcom/facebook/messaging/media/imageurirequest/FetchImageParams;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1310702
    return-void
.end method
