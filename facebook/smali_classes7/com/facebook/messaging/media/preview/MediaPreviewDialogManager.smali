.class public Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/1Ad;

.field public c:Landroid/app/Dialog;

.field public d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public e:LX/1bf;

.field public f:I

.field public g:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1310964
    const-class v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;)V
    .locals 1

    .prologue
    .line 1310959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1310960
    const/16 v0, 0xc8

    iput v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->f:I

    .line 1310961
    sget-object v0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->g:Lcom/facebook/common/callercontext/CallerContext;

    .line 1310962
    iput-object p1, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->b:LX/1Ad;

    .line 1310963
    return-void
.end method

.method public static c(Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1310965
    invoke-static {p0}, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d(Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;)V

    .line 1310966
    iput-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    .line 1310967
    iput-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1310968
    iput-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->e:LX/1bf;

    .line 1310969
    return-void
.end method

.method public static d(Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;)V
    .locals 1

    .prologue
    .line 1310954
    iget-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1310955
    iget-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    invoke-interface {v0}, LX/1aZ;->f()Landroid/graphics/drawable/Animatable;

    move-result-object v0

    .line 1310956
    if-eqz v0, :cond_0

    .line 1310957
    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->stop()V

    .line 1310958
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1310951
    invoke-virtual {p0}, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1310952
    iget-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 1310953
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1310950
    iget-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/media/preview/MediaPreviewDialogManager;->c:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
