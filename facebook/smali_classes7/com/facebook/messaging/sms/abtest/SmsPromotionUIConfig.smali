.class public Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/6jF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1130455
    new-instance v0, LX/6jG;

    invoke-direct {v0}, LX/6jG;-><init>()V

    sput-object v0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1130456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1130457
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/6jF;->fromOrdinal(I)LX/6jF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->a:LX/6jF;

    .line 1130458
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->b:Ljava/lang/String;

    .line 1130459
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->c:Ljava/lang/String;

    .line 1130460
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->d:Ljava/lang/String;

    .line 1130461
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->e:Ljava/lang/String;

    .line 1130462
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1130463
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1130464
    iget-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->a:LX/6jF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->a:LX/6jF;

    invoke-virtual {v0}, LX/6jF;->ordinal()I

    move-result v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1130465
    iget-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130466
    iget-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130467
    iget-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130468
    iget-object v0, p0, Lcom/facebook/messaging/sms/abtest/SmsPromotionUIConfig;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1130469
    return-void

    .line 1130470
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
