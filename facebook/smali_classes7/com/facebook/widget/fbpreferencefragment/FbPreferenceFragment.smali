.class public abstract Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/preference/PreferenceManager;

.field public b:Landroid/widget/ListView;

.field private c:Z

.field private d:Z

.field public e:Landroid/os/Handler;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1150508
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1150509
    new-instance v0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment$1;-><init>(Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;)V

    iput-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->f:Ljava/lang/Runnable;

    return-void
.end method

.method private c()Landroid/preference/PreferenceScreen;
    .locals 3

    .prologue
    .line 1150499
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a:Landroid/preference/PreferenceManager;

    .line 1150500
    :try_start_0
    const-class v1, Landroid/preference/PreferenceManager;

    const-string v2, "getPreferenceScreen"

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, p0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1150501
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1150502
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150503
    :goto_0
    move-object v0, v1

    .line 1150504
    return-object v0

    .line 1150505
    :catch_0
    move-exception v1

    .line 1150506
    sget-object v2, LX/7Tp;->a:Ljava/lang/String;

    const-string p0, "Couldn\'t call PreferenceManager.getPreferenceScreen by reflection"

    invoke-static {v2, p0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1150507
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;)V
    .locals 2

    .prologue
    .line 1150493
    invoke-direct {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->c()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1150494
    if-eqz v0, :cond_0

    .line 1150495
    invoke-static {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->l(Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;)V

    .line 1150496
    iget-object v1, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->b:Landroid/widget/ListView;

    move-object v1, v1

    .line 1150497
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->bind(Landroid/widget/ListView;)V

    .line 1150498
    :cond_0
    return-void
.end method

.method public static l(Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;)V
    .locals 3

    .prologue
    .line 1150481
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->b:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 1150482
    :goto_0
    return-void

    .line 1150483
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1150484
    if-nez v0, :cond_1

    .line 1150485
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Content view not yet created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1150486
    :cond_1
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1150487
    instance-of v1, v0, Landroid/widget/ListView;

    if-nez v1, :cond_2

    .line 1150488
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Content has view with id attribute \'android.R.id.list\' that is not a ListView class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1150489
    :cond_2
    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->b:Landroid/widget/ListView;

    .line 1150490
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->b:Landroid/widget/ListView;

    if-nez v0, :cond_3

    .line 1150491
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Your content must have a ListView whose id attribute is \'android.R.id.list\'"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1150492
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->f:Ljava/lang/Runnable;

    const v2, 0x69391b9f

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1150469
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1150470
    new-instance v0, LX/7To;

    invoke-direct {v0, p0}, LX/7To;-><init>(Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;)V

    iput-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->e:Landroid/os/Handler;

    .line 1150471
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1150472
    :try_start_0
    const-class v1, Landroid/preference/PreferenceManager;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class p1, Landroid/content/Context;

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 1150473
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 1150474
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150475
    :goto_0
    move-object v0, v1

    .line 1150476
    iput-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a:Landroid/preference/PreferenceManager;

    .line 1150477
    return-void

    .line 1150478
    :catch_0
    move-exception v1

    .line 1150479
    sget-object v2, LX/7Tp;->a:Ljava/lang/String;

    const-string v3, "Couldn\'t call constructor PreferenceManager by reflection"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1150480
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/preference/PreferenceScreen;)V
    .locals 7

    .prologue
    .line 1150406
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a:Landroid/preference/PreferenceManager;

    const/4 v2, 0x0

    .line 1150407
    :try_start_0
    const-class v1, Landroid/preference/PreferenceManager;

    const-string v3, "setPreferences"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/preference/PreferenceScreen;

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1150408
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1150409
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1150410
    :goto_0
    move v0, v1

    .line 1150411
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1150412
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->c:Z

    .line 1150413
    iget-boolean v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->d:Z

    if-eqz v0, :cond_0

    .line 1150414
    const/4 v1, 0x1

    .line 1150415
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1150416
    :cond_0
    :goto_1
    return-void

    .line 1150417
    :catch_0
    move-exception v1

    .line 1150418
    sget-object v3, LX/7Tp;->a:Ljava/lang/String;

    const-string v4, "Couldn\'t call PreferenceManager.setPreferences by reflection"

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v2

    .line 1150419
    goto :goto_0

    .line 1150420
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_1
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x37e6226d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1150458
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1150459
    iget-boolean v1, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->c:Z

    if-eqz v1, :cond_0

    .line 1150460
    invoke-static {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->e(Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;)V

    .line 1150461
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->d:Z

    .line 1150462
    if-eqz p1, :cond_1

    .line 1150463
    const-string v1, "android:preferences"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1150464
    if-eqz v1, :cond_1

    .line 1150465
    invoke-direct {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->c()Landroid/preference/PreferenceScreen;

    move-result-object v2

    .line 1150466
    if-eqz v2, :cond_1

    .line 1150467
    invoke-virtual {v2, v1}, Landroid/preference/PreferenceScreen;->restoreHierarchyState(Landroid/os/Bundle;)V

    .line 1150468
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x78dc4995

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1150450
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1150451
    iget-object v0, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a:Landroid/preference/PreferenceManager;

    .line 1150452
    :try_start_0
    const-class v1, Landroid/preference/PreferenceManager;

    const-string v2, "dispatchActivityResult"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object p0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object p0, v3, v4

    const/4 v4, 0x1

    sget-object p0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object p0, v3, v4

    const/4 v4, 0x2

    const-class p0, Landroid/content/Intent;

    aput-object p0, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 1150453
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1150454
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150455
    :goto_0
    return-void

    .line 1150456
    :catch_0
    move-exception v1

    .line 1150457
    sget-object v2, LX/7Tp;->a:Ljava/lang/String;

    const-string v3, "Couldn\'t call PreferenceManager.dispatchActivityResult by reflection"

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x35979742    # -3807791.5f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1150449
    const/4 v1, 0x0

    const/16 v2, 0x2b

    const v3, 0x360879a3

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x200dff61

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1150441
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1150442
    iget-object v1, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a:Landroid/preference/PreferenceManager;

    .line 1150443
    :try_start_0
    const-class v2, Landroid/preference/PreferenceManager;

    const-string v4, "dispatchActivityDestroy"

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Class;

    invoke-virtual {v2, v4, p0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 1150444
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1150445
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150446
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x4592227f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1150447
    :catch_0
    move-exception v2

    .line 1150448
    sget-object v4, LX/7Tp;->a:Ljava/lang/String;

    const-string p0, "Couldn\'t call PreferenceManager.dispatchActivityDestroy by reflection"

    invoke-static {v4, p0, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2bcc29d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1150436
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->b:Landroid/widget/ListView;

    .line 1150437
    iget-object v1, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->f:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1150438
    iget-object v1, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->e:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1150439
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1150440
    const/16 v1, 0x2b

    const v2, 0xec42a7b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1150429
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1150430
    invoke-direct {p0}, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->c()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1150431
    if-eqz v0, :cond_0

    .line 1150432
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1150433
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->saveHierarchyState(Landroid/os/Bundle;)V

    .line 1150434
    const-string v0, "android:preferences"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1150435
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4f9928f9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1150421
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 1150422
    iget-object v1, p0, Lcom/facebook/widget/fbpreferencefragment/FbPreferenceFragment;->a:Landroid/preference/PreferenceManager;

    .line 1150423
    :try_start_0
    const-class v2, Landroid/preference/PreferenceManager;

    const-string v4, "dispatchActivityStop"

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Class;

    invoke-virtual {v2, v4, p0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 1150424
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 1150425
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150426
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x1549d0f8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1150427
    :catch_0
    move-exception v2

    .line 1150428
    sget-object v4, LX/7Tp;->a:Ljava/lang/String;

    const-string p0, "Couldn\'t call PreferenceManager.dispatchActivityStop by reflection"

    invoke-static {v4, p0, v2}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
