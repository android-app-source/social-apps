.class public Lcom/facebook/widget/countryspinner/CountrySpinner;
.super Landroid/widget/Spinner;
.source ""


# instance fields
.field public a:LX/7Tn;

.field private b:[LX/7Tl;

.field private c:Ljava/util/Locale;

.field private d:LX/0W9;

.field private e:LX/3Lz;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1210986
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 1210987
    invoke-direct {p0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->a()V

    .line 1210988
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1210983
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1210984
    invoke-direct {p0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->a()V

    .line 1210985
    return-void
.end method

.method private a(Ljava/lang/String;)LX/7Tl;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1210979
    iget-object v0, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->e:LX/3Lz;

    invoke-virtual {v0, p1}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v1

    .line 1210980
    if-nez v1, :cond_0

    .line 1210981
    const/4 v0, 0x0

    .line 1210982
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/7Tm;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/Locale;

    iget-object v3, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->c:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->c:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, p1, v1, v2}, LX/7Tm;-><init>(Lcom/facebook/widget/countryspinner/CountrySpinner;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1210949
    const-class v0, Lcom/facebook/widget/countryspinner/CountrySpinner;

    invoke-static {v0, p0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1210950
    iget-object v0, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->d:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->c:Ljava/util/Locale;

    .line 1210951
    invoke-static {}, Ljava/util/Locale;->getISOCountries()[Ljava/lang/String;

    move-result-object v2

    .line 1210952
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    move v0, v1

    .line 1210953
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_1

    .line 1210954
    aget-object v4, v2, v0

    invoke-direct {p0, v4}, Lcom/facebook/widget/countryspinner/CountrySpinner;->a(Ljava/lang/String;)LX/7Tl;

    move-result-object v4

    .line 1210955
    if-eqz v4, :cond_0

    .line 1210956
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1210957
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1210958
    :cond_1
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 1210959
    new-array v0, v1, [LX/7Tl;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Tl;

    iput-object v0, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->b:[LX/7Tl;

    .line 1210960
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03039a

    const v3, 0x7f0d0462

    iget-object v4, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->b:[LX/7Tl;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1210961
    iget-object v0, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->setCountrySelection(Ljava/lang/String;)V

    .line 1210962
    return-void
.end method

.method private a(LX/0W9;LX/3Lz;LX/0Or;)V
    .locals 1
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/common/hardware/PhoneIsoCountryCode;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0W9;",
            "LX/3Lz;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1210975
    iput-object p1, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->d:LX/0W9;

    .line 1210976
    iput-object p2, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->e:LX/3Lz;

    .line 1210977
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->f:Ljava/lang/String;

    .line 1210978
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/widget/countryspinner/CountrySpinner;

    invoke-static {v2}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {v2}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v1

    check-cast v1, LX/3Lz;

    const/16 v3, 0x15ec

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/widget/countryspinner/CountrySpinner;->a(LX/0W9;LX/3Lz;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public getCountryCodes()[LX/7Tl;
    .locals 1

    .prologue
    .line 1210974
    iget-object v0, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->b:[LX/7Tl;

    return-object v0
.end method

.method public getSelectedCountryDialingCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1210973
    invoke-virtual {p0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Tl;

    iget-object v0, v0, LX/7Tl;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedCountryIsoCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1210972
    invoke-virtual {p0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Tl;

    iget-object v0, v0, LX/7Tl;->a:Ljava/lang/String;

    return-object v0
.end method

.method public setCountryCodeFormatter(LX/7Tn;)V
    .locals 0

    .prologue
    .line 1210970
    iput-object p1, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->a:LX/7Tn;

    .line 1210971
    return-void
.end method

.method public setCountrySelection(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 1210963
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1210964
    :cond_0
    :goto_0
    return-void

    .line 1210965
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->b:[LX/7Tl;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1210966
    iget-object v2, p0, Lcom/facebook/widget/countryspinner/CountrySpinner;->b:[LX/7Tl;

    aget-object v2, v2, v0

    iget-object v2, v2, LX/7Tl;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1210967
    :goto_2
    if-eq v0, v1, :cond_0

    .line 1210968
    invoke-virtual {p0, v0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->setSelection(I)V

    goto :goto_0

    .line 1210969
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
