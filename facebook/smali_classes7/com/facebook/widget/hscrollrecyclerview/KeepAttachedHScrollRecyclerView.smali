.class public Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;
.super Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;
.source ""

# interfaces
.implements LX/1a7;


# instance fields
.field public m:LX/7Tv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1211188
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1211189
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1211186
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1211187
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1211183
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1211184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;->n:Z

    .line 1211185
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;

    const-class v1, LX/7Tv;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/7Tv;

    iput-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;->m:LX/7Tv;

    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 1211179
    iget-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;->n:Z

    if-nez v0, :cond_0

    .line 1211180
    const-class v0, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;

    invoke-static {v0, p0}, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1211181
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;->n:Z

    .line 1211182
    :cond_0
    return-void
.end method


# virtual methods
.method public final cr_()Z
    .locals 1

    .prologue
    .line 1211178
    const/4 v0, 0x1

    return v0
.end method

.method public getLayoutManagerForInit()LX/25T;
    .locals 8

    .prologue
    .line 1211173
    invoke-direct {p0}, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;->m()V

    .line 1211174
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;->m:LX/7Tv;

    invoke-virtual {p0}, Lcom/facebook/widget/hscrollrecyclerview/KeepAttachedHScrollRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1211175
    new-instance v2, LX/7Tu;

    invoke-static {v0}, LX/25U;->a(LX/0QB;)LX/25U;

    move-result-object v5

    check-cast v5, LX/25U;

    invoke-static {v0}, LX/25V;->a(LX/0QB;)LX/25V;

    move-result-object v6

    check-cast v6, LX/25V;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    move-object v3, p0

    move-object v4, v1

    invoke-direct/range {v2 .. v7}, LX/7Tu;-><init>(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;Landroid/content/Context;LX/25U;LX/25V;LX/0ad;)V

    .line 1211176
    move-object v0, v2

    .line 1211177
    return-object v0
.end method
