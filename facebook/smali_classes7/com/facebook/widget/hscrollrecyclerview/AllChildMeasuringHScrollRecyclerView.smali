.class public Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;
.super Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;
.source ""


# instance fields
.field public m:LX/7Tq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1211000
    invoke-direct {p0, p1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;)V

    .line 1211001
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1211002
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1211003
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1211004
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1211005
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;

    new-instance p1, LX/7Tq;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {v0}, LX/25U;->a(LX/0QB;)LX/25U;

    move-result-object v2

    check-cast v2, LX/25U;

    invoke-static {v0}, LX/25V;->a(LX/0QB;)LX/25V;

    move-result-object v3

    check-cast v3, LX/25V;

    invoke-direct {p1, v1, v2, v3}, LX/7Tq;-><init>(Landroid/content/Context;LX/25U;LX/25V;)V

    move-object v0, p1

    check-cast v0, LX/7Tq;

    iput-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;->m:LX/7Tq;

    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 1211006
    iget-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;->n:Z

    if-nez v0, :cond_0

    .line 1211007
    const-class v0, Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;

    invoke-static {v0, p0}, Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1211008
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;->n:Z

    .line 1211009
    :cond_0
    return-void
.end method


# virtual methods
.method public getLayoutManagerForInit()LX/25T;
    .locals 1

    .prologue
    .line 1211010
    invoke-direct {p0}, Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;->m()V

    .line 1211011
    iget-object v0, p0, Lcom/facebook/widget/hscrollrecyclerview/AllChildMeasuringHScrollRecyclerView;->m:LX/7Tq;

    return-object v0
.end method
