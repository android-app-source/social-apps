.class public Lcom/facebook/widget/soundwave/SoundWaveView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/7Uh;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1213373
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/soundwave/SoundWaveView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1213374
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1213294
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1213295
    iput-boolean v2, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->b:Z

    .line 1213296
    invoke-virtual {p0, v2}, Lcom/facebook/widget/soundwave/SoundWaveView;->setOrientation(I)V

    .line 1213297
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/soundwave/SoundWaveView;->setClipChildren(Z)V

    .line 1213298
    sget-object v0, LX/03r;->SoundWaveView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1213299
    const/16 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/widget/soundwave/SoundWaveView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0048

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 1213300
    const/16 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/widget/soundwave/SoundWaveView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0b0651

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1213301
    const/16 v3, 0x2

    const/4 v5, 0x3

    invoke-virtual {v0, v3, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 1213302
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1213303
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->a:Ljava/util/List;

    move v3, v2

    .line 1213304
    :goto_0
    if-ge v3, v5, :cond_1

    .line 1213305
    new-instance v6, Landroid/view/View;

    invoke-direct {v6, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1213306
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v7, v2, v0, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 1213307
    add-int/lit8 v0, v5, -0x1

    if-ge v3, v0, :cond_0

    move v0, v1

    :goto_1
    iput v0, v7, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 1213308
    invoke-virtual {p0, v6, v7}, Lcom/facebook/widget/soundwave/SoundWaveView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1213309
    iget-object v0, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->a:Ljava/util/List;

    new-instance v7, LX/7Uh;

    invoke-direct {v7, p0, v6}, LX/7Uh;-><init>(Lcom/facebook/widget/soundwave/SoundWaveView;Landroid/view/View;)V

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1213310
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 1213311
    goto :goto_1

    .line 1213312
    :cond_1
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-direct {p0, v0}, Lcom/facebook/widget/soundwave/SoundWaveView;->a(Landroid/graphics/drawable/ColorDrawable;)V

    .line 1213313
    invoke-direct {p0}, Lcom/facebook/widget/soundwave/SoundWaveView;->c()V

    .line 1213314
    return-void
.end method

.method private a(Landroid/view/View;)Landroid/animation/AnimatorSet;
    .locals 7

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    const v3, 0x3e99999a    # 0.3f

    const v5, 0x3dcccccd    # 0.1f

    .line 1213363
    invoke-direct {p0, p1, v5, v1}, Lcom/facebook/widget/soundwave/SoundWaveView;->a(Landroid/view/View;FF)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1213364
    invoke-direct {p0, p1, v1, v5}, Lcom/facebook/widget/soundwave/SoundWaveView;->a(Landroid/view/View;FF)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 1213365
    invoke-direct {p0, p1, v5, v3}, Lcom/facebook/widget/soundwave/SoundWaveView;->a(Landroid/view/View;FF)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 1213366
    invoke-direct {p0, p1, v3, v5}, Lcom/facebook/widget/soundwave/SoundWaveView;->a(Landroid/view/View;FF)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 1213367
    invoke-direct {p0, p1, v5, v6}, Lcom/facebook/widget/soundwave/SoundWaveView;->a(Landroid/view/View;FF)Landroid/animation/ValueAnimator;

    move-result-object v4

    .line 1213368
    invoke-direct {p0, p1, v6, v5}, Lcom/facebook/widget/soundwave/SoundWaveView;->a(Landroid/view/View;FF)Landroid/animation/ValueAnimator;

    move-result-object v5

    .line 1213369
    new-instance v6, Landroid/animation/AnimatorSet;

    invoke-direct {v6}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1213370
    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1213371
    new-instance v0, LX/7Ug;

    invoke-direct {v0, p0}, LX/7Ug;-><init>(Lcom/facebook/widget/soundwave/SoundWaveView;)V

    invoke-virtual {v6, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1213372
    return-object v6
.end method

.method private a(Landroid/view/View;FF)Landroid/animation/ValueAnimator;
    .locals 8

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1213357
    sub-float v0, p2, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    .line 1213358
    const-string v2, "scaleY"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    mul-float v5, v6, p2

    aput v5, v3, v4

    const/4 v4, 0x1

    mul-float v5, v6, p3

    aput v5, v3, v4

    invoke-static {p1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1213359
    const-wide/16 v4, 0x108

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    long-to-double v0, v0

    mul-double/2addr v0, v6

    double-to-long v0, v0

    add-long/2addr v0, v4

    .line 1213360
    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1213361
    new-instance v3, LX/7Ui;

    const-wide v4, 0x4040800000000000L    # 33.0

    long-to-double v0, v0

    div-double v0, v4, v0

    double-to-float v0, v0

    invoke-direct {v3, p0, v0}, LX/7Ui;-><init>(Lcom/facebook/widget/soundwave/SoundWaveView;F)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1213362
    return-object v2
.end method

.method private a(Landroid/graphics/drawable/ColorDrawable;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1213348
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 1213349
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Uh;

    .line 1213350
    if-eqz v1, :cond_1

    .line 1213351
    iget-object p0, v0, LX/7Uh;->b:Landroid/view/View;

    move-object v0, p0

    .line 1213352
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1213353
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 1213354
    :cond_1
    iget-object p0, v0, LX/7Uh;->b:Landroid/view/View;

    move-object v0, p0

    .line 1213355
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1213356
    :cond_2
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1213342
    iget-object v0, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Uh;

    .line 1213343
    iget-object v2, v0, LX/7Uh;->b:Landroid/view/View;

    move-object v2, v2

    .line 1213344
    invoke-direct {p0, v2}, Lcom/facebook/widget/soundwave/SoundWaveView;->a(Landroid/view/View;)Landroid/animation/AnimatorSet;

    move-result-object v2

    .line 1213345
    iput-object v2, v0, LX/7Uh;->c:Landroid/animation/AnimatorSet;

    .line 1213346
    goto :goto_0

    .line 1213347
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1213333
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->b:Z

    .line 1213334
    invoke-direct {p0}, Lcom/facebook/widget/soundwave/SoundWaveView;->c()V

    .line 1213335
    iget-object v0, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Uh;

    .line 1213336
    iget-object v2, v0, LX/7Uh;->b:Landroid/view/View;

    move-object v2, v2

    .line 1213337
    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Landroid/view/View;->setScaleY(F)V

    .line 1213338
    invoke-virtual {p0}, Lcom/facebook/widget/soundwave/SoundWaveView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 1213339
    iget-object v2, v0, LX/7Uh;->c:Landroid/animation/AnimatorSet;

    move-object v0, v2

    .line 1213340
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_0

    .line 1213341
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1213322
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->b:Z

    .line 1213323
    iget-object v0, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Uh;

    .line 1213324
    iget-object v2, v0, LX/7Uh;->c:Landroid/animation/AnimatorSet;

    move-object v2, v2

    .line 1213325
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1213326
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1213327
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->end()V

    .line 1213328
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 1213329
    const/4 v2, 0x0

    .line 1213330
    iput-object v2, v0, LX/7Uh;->c:Landroid/animation/AnimatorSet;

    .line 1213331
    goto :goto_0

    .line 1213332
    :cond_1
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7a52f1c0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1213319
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1213320
    invoke-virtual {p0}, Lcom/facebook/widget/soundwave/SoundWaveView;->b()V

    .line 1213321
    const/16 v1, 0x2d

    const v2, 0x770c2796

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4982256f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1213315
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomLinearLayout;->onSizeChanged(IIII)V

    .line 1213316
    iget-boolean v1, p0, Lcom/facebook/widget/soundwave/SoundWaveView;->b:Z

    if-eqz v1, :cond_0

    .line 1213317
    invoke-virtual {p0}, Lcom/facebook/widget/soundwave/SoundWaveView;->a()V

    .line 1213318
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x2f9ce5b5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
