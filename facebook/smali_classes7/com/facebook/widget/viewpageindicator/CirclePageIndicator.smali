.class public Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;
.super Landroid/view/View;
.source ""

# interfaces
.implements LX/7Um;


# instance fields
.field private a:F

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private e:Landroid/support/v4/view/ViewPager;

.field public f:LX/0hc;

.field private g:I

.field private h:I

.field private i:F

.field private j:I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:I

.field private o:F

.field private p:I

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1213553
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1213554
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1213555
    const v0, 0x7f0105d9

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1213556
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    .line 1213557
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1213558
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->b:Landroid/graphics/Paint;

    .line 1213559
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    .line 1213560
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->d:Landroid/graphics/Paint;

    .line 1213561
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->o:F

    .line 1213562
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->p:I

    .line 1213563
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1213564
    :goto_0
    return-void

    .line 1213565
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1213566
    const v1, 0x7f0a04d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1213567
    const v2, 0x7f0a04d1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1213568
    const v3, 0x7f0c0027

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 1213569
    const v4, 0x7f0a04d3

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 1213570
    const v5, 0x7f0b0bab

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 1213571
    const v6, 0x7f0b0baa

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    .line 1213572
    const v7, 0x7f090009

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v7

    .line 1213573
    const v8, 0x7f09000a

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 1213574
    sget-object v8, LX/03r;->CirclePageIndicator:[I

    const/4 v9, 0x0

    invoke-virtual {p1, p2, v8, p3, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v8

    .line 1213575
    const/16 v9, 0x2

    invoke-virtual {v8, v9, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v7

    iput-boolean v7, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->l:Z

    .line 1213576
    const/16 v7, 0x1

    invoke-virtual {v8, v7, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    iput v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->k:I

    .line 1213577
    iget-object v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->b:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1213578
    iget-object v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->b:Landroid/graphics/Paint;

    const/16 v7, 0x6

    invoke-virtual {v8, v7, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213579
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1213580
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    const/16 v3, 0x5

    invoke-virtual {v8, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213581
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    const/16 v3, 0x3

    invoke-virtual {v8, v3, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1213582
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->d:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1213583
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->d:Landroid/graphics/Paint;

    const/16 v3, 0x4

    invoke-virtual {v8, v3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213584
    const/16 v1, 0x7

    invoke-virtual {v8, v1, v6}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    .line 1213585
    const/16 v1, 0x8

    invoke-virtual {v8, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->m:Z

    .line 1213586
    invoke-virtual {v8}, Landroid/content/res/TypedArray;->recycle()V

    .line 1213587
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 1213588
    invoke-static {v0}, LX/0iP;->a(Landroid/view/ViewConfiguration;)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->n:I

    goto/16 :goto_0
.end method

.method private c(I)I
    .locals 6

    .prologue
    .line 1213589
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1213590
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1213591
    const/high16 v0, 0x40000000    # 2.0f

    if-eq v2, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    .line 1213592
    :cond_1
    :goto_0
    return v0

    .line 1213593
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    .line 1213594
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    iget v5, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    iget v4, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    mul-float/2addr v0, v4

    add-float/2addr v0, v3

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v0, v3

    float-to-int v0, v0

    .line 1213595
    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1

    .line 1213596
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private d(I)I
    .locals 4

    .prologue
    .line 1213597
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1213598
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1213599
    const/high16 v1, 0x40000000    # 2.0f

    if-ne v2, v1, :cond_0

    .line 1213600
    :goto_0
    return v0

    .line 1213601
    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    iget v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    mul-float/2addr v1, v3

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingBottom()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 1213602
    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1

    .line 1213603
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final B_(I)V
    .locals 1

    .prologue
    .line 1213604
    iget-boolean v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->m:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->j:I

    if-nez v0, :cond_1

    .line 1213605
    :cond_0
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    .line 1213606
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->h:I

    .line 1213607
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213608
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    if-eqz v0, :cond_2

    .line 1213609
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    .line 1213610
    :cond_2
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 1213611
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    .line 1213612
    iput p2, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->i:F

    .line 1213613
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213614
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    if-eqz v0, :cond_0

    .line 1213615
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    .line 1213616
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1213623
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->j:I

    .line 1213624
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    if-eqz v0, :cond_0

    .line 1213625
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    .line 1213626
    :cond_0
    return-void
.end method

.method public getFillColor()I
    .locals 1

    .prologue
    .line 1213617
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 1213618
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->k:I

    return v0
.end method

.method public getPageColor()I
    .locals 1

    .prologue
    .line 1213619
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getRadius()F
    .locals 1

    .prologue
    .line 1213620
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    return v0
.end method

.method public getStrokeColor()I
    .locals 1

    .prologue
    .line 1213621
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 1213622
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 1213512
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1213513
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 1213514
    :cond_0
    :goto_0
    return-void

    .line 1213515
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v6

    .line 1213516
    if-eqz v6, :cond_0

    .line 1213517
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    if-lt v0, v6, :cond_2

    .line 1213518
    add-int/lit8 v0, v6, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->setCurrentItem(I)V

    goto :goto_0

    .line 1213519
    :cond_2
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->k:I

    if-nez v0, :cond_7

    .line 1213520
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getWidth()I

    move-result v3

    .line 1213521
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingLeft()I

    move-result v2

    .line 1213522
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingRight()I

    move-result v1

    .line 1213523
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingTop()I

    move-result v0

    .line 1213524
    :goto_1
    iget v4, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    const/high16 v5, 0x40400000    # 3.0f

    mul-float v7, v4, v5

    .line 1213525
    int-to-float v0, v0

    iget v4, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    add-float/2addr v4, v0

    .line 1213526
    int-to-float v0, v2

    iget v5, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    add-float/2addr v0, v5

    .line 1213527
    iget-boolean v5, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->l:Z

    if-eqz v5, :cond_3

    .line 1213528
    sub-int v2, v3, v2

    sub-int v1, v2, v1

    int-to-float v1, v1

    div-float/2addr v1, v8

    int-to-float v2, v6

    mul-float/2addr v2, v7

    div-float/2addr v2, v8

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 1213529
    :cond_3
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    .line 1213530
    iget-object v2, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    .line 1213531
    iget-object v2, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    div-float/2addr v2, v8

    sub-float/2addr v1, v2

    .line 1213532
    :cond_4
    const/4 v2, 0x0

    move v5, v2

    :goto_2
    if-ge v5, v6, :cond_9

    .line 1213533
    int-to-float v2, v5

    mul-float/2addr v2, v7

    add-float/2addr v2, v0

    .line 1213534
    iget v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->k:I

    if-nez v3, :cond_8

    move v3, v2

    move v2, v4

    .line 1213535
    :goto_3
    iget-object v8, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v8}, Landroid/graphics/Paint;->getAlpha()I

    move-result v8

    if-lez v8, :cond_5

    .line 1213536
    iget-object v8, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v1, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1213537
    :cond_5
    iget v8, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    cmpl-float v8, v1, v8

    if-eqz v8, :cond_6

    .line 1213538
    iget v8, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    iget-object v9, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v8, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1213539
    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 1213540
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getHeight()I

    move-result v3

    .line 1213541
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingTop()I

    move-result v2

    .line 1213542
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingBottom()I

    move-result v1

    .line 1213543
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getPaddingLeft()I

    move-result v0

    goto :goto_1

    :cond_8
    move v3, v4

    .line 1213544
    goto :goto_3

    .line 1213545
    :cond_9
    iget-boolean v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->m:Z

    if-eqz v1, :cond_b

    iget v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->h:I

    :goto_4
    int-to-float v1, v1

    mul-float/2addr v1, v7

    .line 1213546
    iget-boolean v2, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->m:Z

    if-nez v2, :cond_a

    .line 1213547
    iget v2, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->i:F

    mul-float/2addr v2, v7

    add-float/2addr v1, v2

    .line 1213548
    :cond_a
    iget v2, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->k:I

    if-nez v2, :cond_c

    .line 1213549
    add-float/2addr v0, v1

    .line 1213550
    :goto_5
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    iget-object v2, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 1213551
    :cond_b
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    goto :goto_4

    .line 1213552
    :cond_c
    add-float/2addr v0, v1

    move v10, v0

    move v0, v4

    move v4, v10

    goto :goto_5
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1213508
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->k:I

    if-nez v0, :cond_0

    .line 1213509
    invoke-direct {p0, p1}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->d(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->setMeasuredDimension(II)V

    .line 1213510
    :goto_0
    return-void

    .line 1213511
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->d(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1213404
    check-cast p1, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;

    .line 1213405
    invoke-virtual {p1}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1213406
    iget v0, p1, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;->a:I

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    .line 1213407
    iget v0, p1, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;->a:I

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->h:I

    .line 1213408
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->requestLayout()V

    .line 1213409
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1213410
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1213411
    new-instance v1, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1213412
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    iput v0, v1, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator$SavedState;->a:I

    .line 1213413
    return-object v1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    const v2, -0x597736f

    invoke-static {v4, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1213414
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1213415
    const v0, 0x1fdf75c8

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1213416
    :goto_0
    return v1

    .line 1213417
    :cond_0
    iget-object v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v3

    invoke-virtual {v3}, LX/0gG;->b()I

    move-result v3

    if-nez v3, :cond_2

    .line 1213418
    :cond_1
    const v1, 0x964f013

    invoke-static {v1, v2}, LX/02F;->a(II)V

    move v1, v0

    goto :goto_0

    .line 1213419
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 1213420
    and-int/lit16 v3, v3, 0xff

    packed-switch v3, :pswitch_data_0

    .line 1213421
    :cond_3
    :goto_1
    :pswitch_0
    const v0, -0x1bf39bdf

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1213422
    :pswitch_1
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->p:I

    .line 1213423
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->o:F

    goto :goto_1

    .line 1213424
    :pswitch_2
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->p:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1213425
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 1213426
    iget v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->o:F

    sub-float v3, v0, v3

    .line 1213427
    iget-boolean v4, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->q:Z

    if-nez v4, :cond_4

    .line 1213428
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->n:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    .line 1213429
    iput-boolean v1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->q:Z

    .line 1213430
    :cond_4
    iget-boolean v4, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->q:Z

    if-eqz v4, :cond_3

    .line 1213431
    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->o:F

    .line 1213432
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    .line 1213433
    iget-boolean v4, v0, Landroid/support/v4/view/ViewPager;->S:Z

    move v0, v4

    .line 1213434
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1213435
    :cond_5
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->a(F)V

    goto :goto_1

    .line 1213436
    :pswitch_3
    iget-boolean v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->q:Z

    if-nez v3, :cond_7

    .line 1213437
    iget-object v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v3

    invoke-virtual {v3}, LX/0gG;->b()I

    move-result v3

    .line 1213438
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->getWidth()I

    move-result v4

    .line 1213439
    int-to-float v5, v4

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    .line 1213440
    int-to-float v4, v4

    const/high16 v6, 0x40c00000    # 6.0f

    div-float/2addr v4, v6

    .line 1213441
    iget v6, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    if-lez v6, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    sub-float v7, v5, v4

    cmpg-float v6, v6, v7

    if-gez v6, :cond_6

    .line 1213442
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    iget v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1213443
    const v0, -0x545b3241    # -1.1710007E-12f

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 1213444
    :cond_6
    iget v6, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    add-int/lit8 v3, v3, -0x1

    if-ge v6, v3, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    add-float/2addr v4, v5

    cmpl-float v3, v3, v4

    if-lez v3, :cond_7

    .line 1213445
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    iget v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1213446
    const v0, 0x7a568b5

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 1213447
    :cond_7
    iput-boolean v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->q:Z

    .line 1213448
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->p:I

    .line 1213449
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    .line 1213450
    iget-boolean v3, v0, Landroid/support/v4/view/ViewPager;->S:Z

    move v0, v3

    .line 1213451
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->d()V

    goto/16 :goto_1

    .line 1213452
    :pswitch_4
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1213453
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v3

    .line 1213454
    iput v3, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->o:F

    .line 1213455
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->p:I

    goto/16 :goto_1

    .line 1213456
    :pswitch_5
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v3

    .line 1213457
    invoke-static {p1, v3}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 1213458
    iget v5, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->p:I

    if-ne v4, v5, :cond_9

    .line 1213459
    if-nez v3, :cond_8

    move v0, v1

    .line 1213460
    :cond_8
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->p:I

    .line 1213461
    :cond_9
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->p:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->o:F

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public setCentered(Z)V
    .locals 0

    .prologue
    .line 1213462
    iput-boolean p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->l:Z

    .line 1213463
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213464
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 2

    .prologue
    .line 1213465
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 1213466
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager has not been bound."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1213467
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1213468
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->g:I

    .line 1213469
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213470
    return-void
.end method

.method public setFillColor(I)V
    .locals 1

    .prologue
    .line 1213471
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213472
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213473
    return-void
.end method

.method public setOnPageChangeListener(LX/0hc;)V
    .locals 0

    .prologue
    .line 1213474
    iput-object p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->f:LX/0hc;

    .line 1213475
    return-void
.end method

.method public setOrientation(I)V
    .locals 2

    .prologue
    .line 1213476
    packed-switch p1, :pswitch_data_0

    .line 1213477
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Orientation must be either HORIZONTAL or VERTICAL."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1213478
    :pswitch_0
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->k:I

    .line 1213479
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->requestLayout()V

    .line 1213480
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setPageColor(I)V
    .locals 1

    .prologue
    .line 1213481
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213482
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213483
    return-void
.end method

.method public setPaintStrokeStyle(Landroid/graphics/Paint$Style;)V
    .locals 1

    .prologue
    .line 1213484
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1213485
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213486
    return-void
.end method

.method public setRadius(F)V
    .locals 0

    .prologue
    .line 1213487
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->a:F

    .line 1213488
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213489
    return-void
.end method

.method public setSnap(Z)V
    .locals 0

    .prologue
    .line 1213490
    iput-boolean p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->m:Z

    .line 1213491
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213492
    return-void
.end method

.method public setStrokeColor(I)V
    .locals 1

    .prologue
    .line 1213493
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213494
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213495
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 1

    .prologue
    .line 1213496
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1213497
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    .line 1213498
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 1213499
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    if-ne v0, p1, :cond_0

    .line 1213500
    :goto_0
    return-void

    .line 1213501
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 1213502
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1213503
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1213504
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager does not have adapter instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1213505
    :cond_2
    iput-object p1, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    .line 1213506
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1213507
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/CirclePageIndicator;->invalidate()V

    goto :goto_0
.end method
