.class public Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;
.super Landroid/view/View;
.source ""


# instance fields
.field public A:I

.field private B:Landroid/animation/ValueAnimator;

.field public C:LX/7Up;

.field public a:I

.field public b:I

.field public c:I

.field public d:F

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private final g:Landroid/graphics/Paint;

.field public final h:Landroid/graphics/Paint;

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:F

.field private final n:F

.field private final o:Z

.field private p:I

.field private q:I

.field private r:F

.field private s:Z

.field private t:I

.field public u:I

.field private v:Z

.field private w:I

.field private x:I

.field public y:F

.field public z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1213768
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1213769
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1213770
    const v0, 0x7f0105d9

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1213771
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1213772
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1213773
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    .line 1213774
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    .line 1213775
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    .line 1213776
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->h:Landroid/graphics/Paint;

    .line 1213777
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->y:F

    .line 1213778
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->z:F

    .line 1213779
    const/16 v0, 0x32

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->A:I

    .line 1213780
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    .line 1213781
    const/4 v0, 0x5

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    .line 1213782
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1213783
    const v1, 0x7f0a04d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->i:I

    .line 1213784
    const v1, 0x7f0a04d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->j:I

    .line 1213785
    const v1, 0x7f0c0027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->k:I

    .line 1213786
    const v1, 0x7f0a04d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->l:I

    .line 1213787
    const v1, 0x7f0b0bab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->m:F

    .line 1213788
    const v1, 0x7f0b0baa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->n:F

    .line 1213789
    const v1, 0x7f09000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->o:Z

    .line 1213790
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1213791
    :goto_0
    return-void

    .line 1213792
    :cond_0
    sget-object v0, LX/03r;->CirclePageIndicator:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1213793
    const/16 v1, 0x1

    iget v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->k:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    .line 1213794
    const/16 v1, 0x0

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->t:I

    .line 1213795
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1213796
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    const/16 v2, 0x6

    iget v3, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->i:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213797
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1213798
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    const/16 v2, 0x5

    iget v3, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->l:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213799
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    const/16 v2, 0x3

    iget v3, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->m:F

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1213800
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1213801
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    const/16 v2, 0x4

    iget v3, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->j:I

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213802
    const/16 v1, 0x7

    iget v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->n:F

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    .line 1213803
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->o:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->s:Z

    .line 1213804
    sget-object v1, LX/7Up;->NotScrolled:LX/7Up;

    iput-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->C:LX/7Up;

    .line 1213805
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->h:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1213806
    iget-object v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->h:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1213807
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto/16 :goto_0
.end method

.method private static a(F)I
    .locals 2

    .prologue
    .line 1213808
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, p0

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private a(I)I
    .locals 6

    .prologue
    .line 1213809
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1213810
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1213811
    const/high16 v1, 0x40000000    # 2.0f

    if-ne v2, v1, :cond_0

    .line 1213812
    :goto_0
    return v0

    .line 1213813
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingRight()I

    move-result v3

    add-int/2addr v1, v3

    iget v3, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->u:I

    iget v4, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    iget v5, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    invoke-static {v3, v4, v5}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(IIF)I

    move-result v3

    add-int/2addr v1, v3

    .line 1213814
    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1

    .line 1213815
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static a(IIF)I
    .locals 2

    .prologue
    .line 1213816
    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1213817
    mul-int/lit8 v1, v0, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, p2

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, p2

    add-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private a(Landroid/graphics/Canvas;FF)V
    .locals 3

    .prologue
    .line 1213818
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->A:I

    int-to-float v0, v0

    sub-float v0, p2, v0

    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 1213819
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->p:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    if-nez v1, :cond_0

    .line 1213820
    const/4 v1, 0x0

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(Landroid/graphics/Canvas;FFZ)V

    .line 1213821
    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/Canvas;FFZ)V
    .locals 5

    .prologue
    const v4, 0x3fee147b    # 1.86f

    .line 1213822
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->y:F

    mul-float/2addr v1, v0

    .line 1213823
    new-instance v2, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 1213824
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->h:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213825
    const/high16 v0, 0x40c00000    # 6.0f

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1213826
    new-instance v0, Landroid/graphics/CornerPathEffect;

    const/high16 v3, 0x40400000    # 3.0f

    invoke-direct {v0, v3}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    .line 1213827
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1213828
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 1213829
    sub-float v0, p2, v1

    invoke-virtual {v3, p3, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1213830
    if-eqz p4, :cond_0

    mul-float v0, v1, v4

    add-float/2addr v0, p3

    :goto_0
    invoke-virtual {v3, v0, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1213831
    add-float v0, p2, v1

    invoke-virtual {v3, p3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1213832
    sub-float v0, p2, v1

    invoke-virtual {v3, p3, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1213833
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 1213834
    invoke-virtual {p1, v3, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1213835
    return-void

    .line 1213836
    :cond_0
    mul-float v0, v1, v4

    sub-float v0, p3, v0

    goto :goto_0
.end method

.method private b(I)I
    .locals 4

    .prologue
    .line 1213837
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1213838
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1213839
    const/high16 v1, 0x40000000    # 2.0f

    if-ne v2, v1, :cond_0

    .line 1213840
    :goto_0
    return v0

    .line 1213841
    :cond_0
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    invoke-static {v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(F)I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingTop()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    .line 1213842
    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1

    .line 1213843
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1213844
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->B:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 1213845
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, -0xc2bbae

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->B:Landroid/animation/ValueAnimator;

    .line 1213846
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->B:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1213847
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->B:Landroid/animation/ValueAnimator;

    new-instance v1, LX/7Un;

    invoke-direct {v1, p0}, LX/7Un;-><init>(Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1213848
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->B:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1213849
    return-void
.end method

.method private b(Landroid/graphics/Canvas;FF)V
    .locals 3

    .prologue
    .line 1213877
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->A:I

    int-to-float v0, v0

    add-float/2addr v0, p2

    .line 1213878
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->p:I

    iget v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->u:I

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    iget v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    .line 1213879
    const/4 v1, 0x1

    invoke-direct {p0, p1, p3, v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(Landroid/graphics/Canvas;FFZ)V

    .line 1213880
    :cond_0
    return-void
.end method

.method private getCircleCount()I
    .locals 2

    .prologue
    .line 1213850
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->u:I

    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->u:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    goto :goto_0
.end method

.method private setIsLastSection(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1213851
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    div-int v0, p1, v0

    :goto_0
    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->w:I

    .line 1213852
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->u:I

    add-int/lit8 v0, v0, -0x1

    iget v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    div-int/2addr v0, v2

    :goto_1
    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->x:I

    .line 1213853
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->w:I

    iget v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->x:I

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->v:Z

    .line 1213854
    return-void

    :cond_1
    move v0, v1

    .line 1213855
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1213856
    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1213857
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    .line 1213858
    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->p:I

    .line 1213859
    iput v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->q:I

    .line 1213860
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->r:F

    .line 1213861
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213862
    return-void
.end method

.method public final a(IIZ)V
    .locals 0

    .prologue
    .line 1213863
    iput p2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    .line 1213864
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->p:I

    .line 1213865
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->q:I

    .line 1213866
    invoke-direct {p0, p1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setIsLastSection(I)V

    .line 1213867
    if-eqz p3, :cond_0

    .line 1213868
    invoke-direct {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b()V

    .line 1213869
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213870
    return-void
.end method

.method public getCirclePadding()F
    .locals 1

    .prologue
    .line 1213871
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->z:F

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1213872
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->u:I

    return v0
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 1213873
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->q:I

    return v0
.end method

.method public getFillColor()I
    .locals 1

    .prologue
    .line 1213874
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 1213875
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    return v0
.end method

.method public getPageColor()I
    .locals 1

    .prologue
    .line 1213765
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getRadius()F
    .locals 1

    .prologue
    .line 1213876
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    return v0
.end method

.method public getStrokeColor()I
    .locals 1

    .prologue
    .line 1213766
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getStrokeStyle()Landroid/graphics/Paint$Style;
    .locals 1

    .prologue
    .line 1213767
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v0

    return-object v0
.end method

.method public getStrokeWidth()F
    .locals 1

    .prologue
    .line 1213658
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const v9, 0x800005

    const v8, 0x800003

    const/high16 v11, 0x40000000    # 2.0f

    .line 1213659
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1213660
    invoke-direct {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getCircleCount()I

    move-result v6

    .line 1213661
    if-nez v6, :cond_0

    .line 1213662
    :goto_0
    return-void

    .line 1213663
    :cond_0
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    if-nez v0, :cond_5

    .line 1213664
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getWidth()I

    move-result v3

    .line 1213665
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingLeft()I

    move-result v2

    .line 1213666
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingRight()I

    move-result v1

    .line 1213667
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingTop()I

    move-result v0

    .line 1213668
    :goto_1
    iget v4, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    iget v5, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->z:F

    mul-float v7, v4, v5

    .line 1213669
    int-to-float v0, v0

    iget v4, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    add-float/2addr v4, v0

    .line 1213670
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->t:I

    and-int/2addr v0, v8

    if-eq v0, v8, :cond_6

    .line 1213671
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->t:I

    and-int/2addr v0, v9

    if-ne v0, v9, :cond_6

    .line 1213672
    sub-int v0, v3, v1

    int-to-float v0, v0

    int-to-float v5, v6

    mul-float/2addr v5, v7

    sub-float/2addr v0, v5

    .line 1213673
    :goto_2
    sub-int v5, v3, v2

    sub-int/2addr v5, v1

    int-to-float v5, v5

    div-float/2addr v5, v11

    add-float/2addr v5, v0

    iget v8, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    int-to-float v8, v8

    mul-float/2addr v8, v7

    div-float/2addr v8, v11

    sub-float v8, v5, v8

    .line 1213674
    int-to-float v5, v3

    sub-float v9, v5, v8

    .line 1213675
    iget v5, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->t:I

    and-int/lit8 v5, v5, 0x11

    const/16 v10, 0x11

    if-ne v5, v10, :cond_1

    .line 1213676
    sub-int v2, v3, v2

    sub-int v1, v2, v1

    int-to-float v1, v1

    div-float/2addr v1, v11

    int-to-float v2, v6

    mul-float/2addr v2, v7

    div-float/2addr v2, v11

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 1213677
    :cond_1
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    .line 1213678
    iget-object v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 1213679
    iget-object v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    div-float/2addr v2, v11

    sub-float/2addr v1, v2

    .line 1213680
    :cond_2
    const/4 v2, 0x0

    move v5, v2

    :goto_3
    if-ge v5, v6, :cond_8

    .line 1213681
    int-to-float v2, v5

    mul-float/2addr v2, v7

    add-float/2addr v2, v0

    .line 1213682
    iget v3, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    if-nez v3, :cond_7

    move v3, v2

    move v2, v4

    .line 1213683
    :goto_4
    iget-object v10, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {v10}, Landroid/graphics/Paint;->getAlpha()I

    move-result v10

    if-lez v10, :cond_3

    .line 1213684
    iget-object v10, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v1, v10}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1213685
    :cond_3
    iget v10, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    cmpl-float v10, v1, v10

    if-eqz v10, :cond_4

    .line 1213686
    iget v10, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    iget-object v11, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v10, v11}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1213687
    :cond_4
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    .line 1213688
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getHeight()I

    move-result v3

    .line 1213689
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingTop()I

    move-result v2

    .line 1213690
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingBottom()I

    move-result v1

    .line 1213691
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getPaddingLeft()I

    move-result v0

    goto/16 :goto_1

    .line 1213692
    :cond_6
    int-to-float v0, v2

    iget v5, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    add-float/2addr v0, v5

    goto :goto_2

    :cond_7
    move v3, v4

    .line 1213693
    goto :goto_4

    .line 1213694
    :cond_8
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    int-to-float v1, v1

    mul-float/2addr v1, v7

    .line 1213695
    iget-boolean v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->s:Z

    if-nez v2, :cond_9

    .line 1213696
    iget v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->r:F

    mul-float/2addr v2, v7

    add-float/2addr v1, v2

    .line 1213697
    :cond_9
    iget v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    if-nez v2, :cond_a

    .line 1213698
    add-float/2addr v0, v1

    .line 1213699
    :goto_5
    iget v1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    iget-object v2, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1213700
    invoke-direct {p0, p1, v9, v4}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b(Landroid/graphics/Canvas;FF)V

    .line 1213701
    invoke-direct {p0, p1, v8, v4}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(Landroid/graphics/Canvas;FF)V

    goto/16 :goto_0

    .line 1213702
    :cond_a
    add-float/2addr v0, v1

    move v12, v0

    move v0, v4

    move v4, v12

    goto :goto_5
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1213703
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    if-nez v0, :cond_0

    .line 1213704
    invoke-direct {p0, p1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setMeasuredDimension(II)V

    .line 1213705
    :goto_0
    return-void

    .line 1213706
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1213707
    check-cast p1, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator$SavedState;

    .line 1213708
    invoke-virtual {p1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1213709
    iget v0, p1, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator$SavedState;->a:I

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->p:I

    .line 1213710
    iget v0, p1, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator$SavedState;->a:I

    iput v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->q:I

    .line 1213711
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->requestLayout()V

    .line 1213712
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1213713
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1213714
    new-instance v1, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1213715
    iget v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->p:I

    iput v0, v1, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator$SavedState;->a:I

    .line 1213716
    return-object v1
.end method

.method public setArrowColor(I)V
    .locals 1

    .prologue
    .line 1213717
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213718
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213719
    return-void
.end method

.method public setArrowPadding(I)V
    .locals 0

    .prologue
    .line 1213720
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->A:I

    .line 1213721
    return-void
.end method

.method public setArrowStrokeWidth(I)V
    .locals 2

    .prologue
    .line 1213722
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->h:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1213723
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213724
    return-void
.end method

.method public setArrowStrokeWidthMultiplier(F)V
    .locals 0

    .prologue
    .line 1213725
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->y:F

    .line 1213726
    return-void
.end method

.method public setCirclePaddingMult(F)V
    .locals 0

    .prologue
    .line 1213727
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->z:F

    .line 1213728
    return-void
.end method

.method public setCount(I)V
    .locals 0

    .prologue
    .line 1213729
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->u:I

    .line 1213730
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213731
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 0

    .prologue
    .line 1213652
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a:I

    .line 1213653
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->p:I

    .line 1213654
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->q:I

    .line 1213655
    invoke-direct {p0, p1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setIsLastSection(I)V

    .line 1213656
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213657
    return-void
.end method

.method public setCurrentPage(I)V
    .locals 0

    .prologue
    .line 1213732
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->p:I

    .line 1213733
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213734
    return-void
.end method

.method public setFillColor(I)V
    .locals 1

    .prologue
    .line 1213735
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213736
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213737
    return-void
.end method

.method public setMaxCircles(I)V
    .locals 0

    .prologue
    .line 1213738
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    .line 1213739
    return-void
.end method

.method public setOrientation(I)V
    .locals 2

    .prologue
    .line 1213740
    packed-switch p1, :pswitch_data_0

    .line 1213741
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Orientation must be either HORIZONTAL or VERTICAL."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1213742
    :pswitch_0
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->b:I

    .line 1213743
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->requestLayout()V

    .line 1213744
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setPageColor(I)V
    .locals 1

    .prologue
    .line 1213745
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213746
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213747
    return-void
.end method

.method public setRadius(F)V
    .locals 0

    .prologue
    .line 1213748
    iput p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->d:F

    .line 1213749
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213750
    return-void
.end method

.method public setScrollState(LX/7Up;)V
    .locals 0

    .prologue
    .line 1213751
    iput-object p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->C:LX/7Up;

    .line 1213752
    return-void
.end method

.method public setSnap(Z)V
    .locals 0

    .prologue
    .line 1213753
    iput-boolean p1, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->s:Z

    .line 1213754
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213755
    return-void
.end method

.method public setStrokeColor(I)V
    .locals 1

    .prologue
    .line 1213756
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1213757
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213758
    return-void
.end method

.method public setStrokeStyle(Landroid/graphics/Paint$Style;)V
    .locals 1

    .prologue
    .line 1213759
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1213760
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213761
    return-void
.end method

.method public setStrokeWidth(F)V
    .locals 1

    .prologue
    .line 1213762
    iget-object v0, p0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1213763
    invoke-virtual {p0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->invalidate()V

    .line 1213764
    return-void
.end method
