.class public Lcom/facebook/widget/images/ImageCacheReader;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/imagepipeline/animated/factory/AnimatedDrawableFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1211327
    const-class v0, Lcom/facebook/widget/images/ImageCacheReader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/images/ImageCacheReader;->a:Ljava/lang/String;

    .line 1211328
    const-class v0, Lcom/facebook/widget/images/ImageCacheReader;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/images/ImageCacheReader;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/0Ot",
            "<",
            "LX/1HI;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/imagepipeline/animated/factory/AnimatedDrawableFactory;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1211329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1211330
    iput-object p1, p0, Lcom/facebook/widget/images/ImageCacheReader;->c:Landroid/content/res/Resources;

    .line 1211331
    iput-object p2, p0, Lcom/facebook/widget/images/ImageCacheReader;->d:LX/0Ot;

    .line 1211332
    iput-object p3, p0, Lcom/facebook/widget/images/ImageCacheReader;->e:LX/0Ot;

    .line 1211333
    return-void
.end method

.method public static b(Lcom/facebook/widget/images/ImageCacheReader;LX/1bf;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1211334
    invoke-static {p1}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v0

    sget-object v1, LX/1bY;->BITMAP_MEMORY_CACHE:LX/1bY;

    .line 1211335
    iput-object v1, v0, LX/1bX;->b:LX/1bY;

    .line 1211336
    move-object v0, v0

    .line 1211337
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1211338
    iget-object v0, p0, Lcom/facebook/widget/images/ImageCacheReader;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1HI;

    sget-object v3, Lcom/facebook/widget/images/ImageCacheReader;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v3}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    .line 1211339
    invoke-interface {v1}, LX/1ca;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1211340
    invoke-interface {v1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1211341
    invoke-interface {v1}, LX/1ca;->g()Z

    .line 1211342
    invoke-static {v0}, LX/1FJ;->a(LX/1FJ;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v0, v2

    .line 1211343
    :goto_0
    return-object v0

    .line 1211344
    :cond_0
    :try_start_0
    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ln;

    .line 1211345
    instance-of v3, v1, LX/1lm;

    if-eqz v3, :cond_2

    .line 1211346
    new-instance v2, LX/7U1;

    iget-object v3, p0, Lcom/facebook/widget/images/ImageCacheReader;->c:Landroid/content/res/Resources;

    invoke-direct {v2, v3, v0}, LX/7U1;-><init>(Landroid/content/res/Resources;LX/1FJ;)V

    .line 1211347
    instance-of v3, v1, LX/1ll;

    if-eqz v3, :cond_1

    .line 1211348
    check-cast v1, LX/1ll;

    .line 1211349
    iget v3, v1, LX/1ll;->d:I

    move v3, v3

    .line 1211350
    if-eqz v3, :cond_1

    const/4 v1, -0x1

    if-eq v3, v1, :cond_1

    .line 1211351
    new-instance v1, LX/4AT;

    invoke-direct {v1, v2, v3}, LX/4AT;-><init>(Landroid/graphics/drawable/Drawable;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1211352
    invoke-virtual {v0}, LX/1FJ;->close()V

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, LX/1FJ;->close()V

    move-object v0, v2

    goto :goto_0

    .line 1211353
    :cond_2
    :try_start_1
    instance-of v3, v1, LX/4e7;

    if-eqz v3, :cond_3

    .line 1211354
    iget-object v2, p0, Lcom/facebook/widget/images/ImageCacheReader;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1c3;

    invoke-virtual {v2, v1}, LX/1c3;->a(LX/1ln;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, LX/4dF;

    .line 1211355
    new-instance v2, LX/7Tz;

    invoke-direct {v2, v1, v0}, LX/7Tz;-><init>(LX/4dF;LX/1FJ;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1211356
    invoke-virtual {v0}, LX/1FJ;->close()V

    move-object v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, LX/1FJ;->close()V

    move-object v0, v2

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, LX/1FJ;->close()V

    throw v1
.end method

.method public static b(LX/0QB;)Lcom/facebook/widget/images/ImageCacheReader;
    .locals 4

    .prologue
    .line 1211357
    new-instance v1, Lcom/facebook/widget/images/ImageCacheReader;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    const/16 v2, 0xb99

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xb8c

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/facebook/widget/images/ImageCacheReader;-><init>(Landroid/content/res/Resources;LX/0Ot;LX/0Ot;)V

    .line 1211358
    return-object v1
.end method


# virtual methods
.method public final a(LX/4n9;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # LX/4n9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1211359
    iget-object v0, p0, Lcom/facebook/widget/images/ImageCacheReader;->c:Landroid/content/res/Resources;

    invoke-static {p1, v0}, LX/4eI;->a(LX/4n9;Landroid/content/res/Resources;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1211360
    invoke-static {p0, v0}, Lcom/facebook/widget/images/ImageCacheReader;->b(Lcom/facebook/widget/images/ImageCacheReader;LX/1bf;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
