.class public Lcom/facebook/widget/images/UrlImage;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2eZ;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final ak:Landroid/os/Handler;

.field private static final al:Ljava/lang/Runnable;

.field public static final h:Ljava/util/Set;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/widget/images/UrlImage;",
            ">;"
        }
    .end annotation
.end field

.field private static i:I

.field private static j:I

.field private static final k:[Landroid/widget/ImageView$ScaleType;

.field private static final l:Landroid/graphics/Matrix;


# instance fields
.field public A:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final B:LX/7UI;

.field private C:LX/7UI;

.field private D:LX/33B;

.field public E:LX/7UL;

.field private F:Landroid/view/animation/Animation;

.field public G:LX/7UJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public H:LX/7UK;

.field private I:LX/7UN;

.field private J:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public K:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0yL;",
            ">;"
        }
    .end annotation
.end field

.field private L:Z

.field private final M:Z

.field private final N:Z

.field private O:I

.field private P:Z

.field private Q:Z

.field public R:Z

.field private S:Z

.field private T:Lcom/facebook/common/callercontext/CallerContext;

.field private U:Z

.field public V:I

.field private W:I

.field public final a:LX/7UI;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private aa:Z

.field public ab:Z

.field private ac:Z

.field private ad:LX/1HI;

.field public ae:LX/1eu;

.field private af:I

.field private final ag:LX/0Vq;

.field private ah:Z

.field private ai:LX/5N8;

.field public aj:Z

.field private final am:LX/0T2;

.field public b:LX/0Px;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1bf;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/1ca;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public d:LX/7UH;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public e:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public f:Z
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public g:LX/0yc;

.field private m:Ljava/lang/String;

.field private n:LX/7U4;

.field private o:Landroid/view/LayoutInflater;

.field private p:Ljava/util/concurrent/Executor;

.field private q:LX/5zd;

.field private r:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

.field public s:Lcom/facebook/common/perftest/PerfTestConfig;

.field private t:LX/0Sy;

.field private u:LX/0Yj;

.field private v:Lcom/facebook/widget/images/ImageCacheReader;

.field private w:LX/0So;

.field private x:Landroid/view/View$OnClickListener;

.field public final y:Landroid/widget/ImageView;

.field public final z:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1212440
    sput v2, Lcom/facebook/widget/images/UrlImage;->i:I

    .line 1212441
    sput v2, Lcom/facebook/widget/images/UrlImage;->j:I

    .line 1212442
    const/16 v0, 0x8

    new-array v0, v0, [Landroid/widget/ImageView$ScaleType;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_END:Landroid/widget/ImageView$ScaleType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/widget/images/UrlImage;->k:[Landroid/widget/ImageView$ScaleType;

    .line 1212443
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcom/facebook/widget/images/UrlImage;->l:Landroid/graphics/Matrix;

    .line 1212444
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    move-object v0, v0

    .line 1212445
    invoke-static {v0}, LX/0P9;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    move-object v0, v0

    .line 1212446
    sput-object v0, Lcom/facebook/widget/images/UrlImage;->h:Ljava/util/Set;

    .line 1212447
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/facebook/widget/images/UrlImage;->ak:Landroid/os/Handler;

    .line 1212448
    new-instance v0, Lcom/facebook/widget/images/UrlImage$1;

    invoke-direct {v0}, Lcom/facebook/widget/images/UrlImage$1;-><init>()V

    sput-object v0, Lcom/facebook/widget/images/UrlImage;->al:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1212438
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/images/UrlImage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1212439
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1212436
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/images/UrlImage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1212437
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212434
    const v0, 0x7f0e03e9

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/widget/images/UrlImage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 1212435
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 6
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 1212364
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1212365
    new-instance v0, LX/7UI;

    invoke-direct {v0}, LX/7UI;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    .line 1212366
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->C:LX/7UI;

    .line 1212367
    new-instance v0, LX/7UI;

    invoke-direct {v0}, LX/7UI;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    .line 1212368
    iput-boolean v4, p0, Lcom/facebook/widget/images/UrlImage;->R:Z

    .line 1212369
    iput-boolean v4, p0, Lcom/facebook/widget/images/UrlImage;->e:Z

    .line 1212370
    iput-boolean v4, p0, Lcom/facebook/widget/images/UrlImage;->f:Z

    .line 1212371
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->S:Z

    .line 1212372
    iput v4, p0, Lcom/facebook/widget/images/UrlImage;->V:I

    .line 1212373
    iput v4, p0, Lcom/facebook/widget/images/UrlImage;->W:I

    .line 1212374
    iput-boolean v4, p0, Lcom/facebook/widget/images/UrlImage;->ac:Z

    .line 1212375
    iput v4, p0, Lcom/facebook/widget/images/UrlImage;->af:I

    .line 1212376
    iput-boolean v4, p0, Lcom/facebook/widget/images/UrlImage;->aj:Z

    .line 1212377
    new-instance v0, LX/7UA;

    invoke-direct {v0, p0}, LX/7UA;-><init>(Lcom/facebook/widget/images/UrlImage;)V

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->am:LX/0T2;

    .line 1212378
    const-class v0, Lcom/facebook/widget/images/UrlImage;

    invoke-static {v0, p0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1212379
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    iput-object v1, v0, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    .line 1212380
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    iput-object v1, v0, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    .line 1212381
    sget-object v0, LX/03r;->UrlImage:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1212382
    const/16 v0, 0x6

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->N:Z

    .line 1212383
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->N:Z

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->U:Z

    .line 1212384
    const/16 v0, 0x3

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->M:Z

    .line 1212385
    const/16 v0, 0x4

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 1212386
    iget-boolean v2, p0, Lcom/facebook/widget/images/UrlImage;->M:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/facebook/widget/images/UrlImage;->N:Z

    if-eqz v2, :cond_4

    .line 1212387
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->o:Landroid/view/LayoutInflater;

    const v2, 0x7f030dd1

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1212388
    :goto_0
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->P:Z

    .line 1212389
    const/16 v0, 0x8

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/images/UrlImage;->O:I

    .line 1212390
    const v0, 0x7f0d21e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    .line 1212391
    const v0, 0x7f0d21e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getOptionalView(I)LX/0am;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    .line 1212392
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    .line 1212393
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    .line 1212394
    new-instance v0, LX/7UN;

    new-instance v2, LX/7UM;

    invoke-direct {v2, p0}, LX/7UM;-><init>(Lcom/facebook/widget/images/UrlImage;)V

    invoke-direct {v0, p0, v2}, LX/7UN;-><init>(Lcom/facebook/widget/images/UrlImage;LX/7UM;)V

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->I:LX/7UN;

    .line 1212395
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    .line 1212396
    const/16 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1212397
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1212398
    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    .line 1212399
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x0

    :goto_1
    move-object v0, v3

    .line 1212400
    iput-object v0, v2, LX/7UI;->a:LX/4n9;

    .line 1212401
    :cond_0
    const/16 v0, 0xa

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 1212402
    if-ltz v0, :cond_1

    .line 1212403
    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    sget-object v3, Lcom/facebook/widget/images/UrlImage;->k:[Landroid/widget/ImageView$ScaleType;

    aget-object v0, v3, v0

    iput-object v0, v2, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    .line 1212404
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    const/16 v2, 0x7

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, v0, LX/7UI;->h:Z

    .line 1212405
    const/16 v0, 0x9

    invoke-virtual {v1, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 1212406
    if-ltz v0, :cond_2

    .line 1212407
    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    sget-object v3, Lcom/facebook/widget/images/UrlImage;->k:[Landroid/widget/ImageView$ScaleType;

    aget-object v0, v3, v0

    iput-object v0, v2, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    .line 1212408
    :cond_2
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, LX/7UL;->PROGRESS_BAR_INDETERMINATE:LX/7UL;

    :goto_2
    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->E:LX/7UL;

    .line 1212409
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    const/16 v2, 0x1

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, LX/7UI;->c:Ljava/lang/Integer;

    .line 1212410
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    .line 1212411
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->K:LX/0am;

    .line 1212412
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->i()V

    .line 1212413
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->j()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1212414
    invoke-static {p0}, Lcom/facebook/widget/images/UrlImage;->f(Lcom/facebook/widget/images/UrlImage;)V

    .line 1212415
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v2, v2, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1212416
    const/16 v0, 0x5

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->Q:Z

    .line 1212417
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1212418
    new-instance v0, LX/7UB;

    invoke-direct {v0, p0}, LX/7UB;-><init>(Lcom/facebook/widget/images/UrlImage;)V

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->ag:LX/0Vq;

    .line 1212419
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->t:LX/0Sy;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->ag:LX/0Vq;

    invoke-virtual {v0, v1}, LX/0Sy;->a(LX/0Vq;)V

    .line 1212420
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->am:LX/0T2;

    invoke-static {v0, p1}, LX/43j;->a(LX/0T2;Landroid/content/Context;)V

    .line 1212421
    return-void

    .line 1212422
    :cond_4
    iget-boolean v2, p0, Lcom/facebook/widget/images/UrlImage;->M:Z

    if-eqz v2, :cond_5

    .line 1212423
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->o:Landroid/view/LayoutInflater;

    const v2, 0x7f030dd0

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0

    .line 1212424
    :cond_5
    if-eqz v0, :cond_6

    .line 1212425
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->o:Landroid/view/LayoutInflater;

    const v2, 0x7f030dd3

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0

    .line 1212426
    :cond_6
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->N:Z

    if-eqz v0, :cond_7

    .line 1212427
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->o:Landroid/view/LayoutInflater;

    const v2, 0x7f030dd2

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0

    .line 1212428
    :cond_7
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->o:Landroid/view/LayoutInflater;

    const v2, 0x7f030dcf

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_0

    .line 1212429
    :cond_8
    sget-object v0, LX/7UL;->PROGRESS_BAR_HIDDEN:LX/7UL;

    goto/16 :goto_2

    .line 1212430
    :cond_9
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_3

    .line 1212431
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v2, v2, LX/7UI;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :cond_a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1212432
    if-nez v3, :cond_b

    const/4 v0, 0x0

    :goto_4
    move-object v3, v0

    .line 1212433
    goto/16 :goto_1

    :cond_b
    invoke-static {v3}, LX/4n9;->a(Landroid/net/Uri;)LX/4n8;

    move-result-object v0

    invoke-virtual {v0}, LX/4n8;->a()LX/4n9;

    move-result-object v0

    goto :goto_4
.end method

.method private A()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1212347
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->ac:Z

    if-nez v0, :cond_1

    .line 1212348
    :cond_0
    :goto_0
    return-void

    .line 1212349
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getWidth()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getHeight()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1212350
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getImageDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1212351
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v3, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne v2, v3, :cond_0

    .line 1212352
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    .line 1212353
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    sub-int/2addr v3, v4

    .line 1212354
    iget-object v4, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 1212355
    new-instance v6, LX/5zb;

    invoke-direct {v6, v4}, LX/5zb;-><init>(LX/5zd;)V

    .line 1212356
    iput v2, v6, LX/5zb;->a:I

    .line 1212357
    iput v3, v6, LX/5zb;->b:I

    .line 1212358
    iput v5, v6, LX/5zb;->c:I

    .line 1212359
    iput v0, v6, LX/5zb;->d:I

    .line 1212360
    iget-object v7, v4, LX/5zd;->f:Ljava/util/LinkedList;

    invoke-virtual {v7, v6}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 1212361
    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->L:Z

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, LX/5zd;->a(Z)V

    .line 1212362
    iput-boolean v1, p0, Lcom/facebook/widget/images/UrlImage;->ac:Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1212363
    goto :goto_1
.end method

.method private B()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1212338
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/widget/images/UrlImage;->W:I

    const/4 v2, 0x4

    if-lt v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 1212339
    :goto_0
    return v0

    .line 1212340
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->E()V

    .line 1212341
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1212342
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1212343
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1212344
    iget v0, p0, Lcom/facebook/widget/images/UrlImage;->W:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/widget/images/UrlImage;->W:I

    .line 1212345
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1212346
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private C()V
    .locals 2

    .prologue
    .line 1212335
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1212336
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1212337
    :cond_0
    return-void
.end method

.method private D()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1212329
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212330
    :goto_0
    :pswitch_0
    return-void

    .line 1212331
    :cond_0
    sget-object v0, LX/7UG;->a:[I

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->E:LX/7UL;

    invoke-virtual {v2}, LX/7UL;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 1212332
    :goto_1
    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->o:Landroid/view/LayoutInflater;

    invoke-virtual {v2, v0, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    goto :goto_0

    .line 1212333
    :pswitch_1
    const v0, 0x7f031559

    goto :goto_1

    .line 1212334
    :pswitch_2
    const v0, 0x7f031558

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private E()V
    .locals 1

    .prologue
    .line 1212327
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/images/UrlImage;->V:I

    .line 1212328
    return-void
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1212306
    if-gtz p1, :cond_0

    .line 1212307
    :goto_0
    return-void

    .line 1212308
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1212309
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->M:Z

    if-eqz v0, :cond_3

    .line 1212310
    const v0, 0x7f0d05a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1212311
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1212312
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->g:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1212313
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->ai:LX/5N8;

    if-nez v0, :cond_2

    .line 1212314
    new-instance v0, LX/5N8;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->g:LX/0yc;

    new-instance v2, LX/396;

    iget-object v3, p0, Lcom/facebook/widget/images/UrlImage;->m:Ljava/lang/String;

    sget-object v4, LX/397;->PHOTO:LX/397;

    invoke-direct {v2, v3, v5, v4}, LX/396;-><init>(Ljava/lang/String;ILX/397;)V

    invoke-direct {v0, v1, v2}, LX/5N8;-><init>(LX/0yc;LX/396;)V

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->ai:LX/5N8;

    .line 1212315
    :cond_2
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1212316
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->ai:LX/5N8;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1212317
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    .line 1212318
    :goto_2
    new-instance v0, LX/7UC;

    invoke-direct {v0, p0}, LX/7UC;-><init>(Lcom/facebook/widget/images/UrlImage;)V

    .line 1212319
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->K:LX/0am;

    .line 1212320
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->x:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1212321
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->M:Z

    if-eqz v0, :cond_5

    .line 1212322
    const v0, 0x7f0d05a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1212323
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 1212324
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 1212325
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->o:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p1, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    goto :goto_2

    .line 1212326
    :cond_5
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->J:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->addView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method private a(LX/1eu;LX/7U4;Landroid/view/LayoutInflater;Ljava/util/concurrent/Executor;LX/5zd;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Sy;LX/1HI;Lcom/facebook/widget/images/ImageCacheReader;LX/0So;LX/0yc;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1212142
    iput-object p1, p0, Lcom/facebook/widget/images/UrlImage;->ae:LX/1eu;

    .line 1212143
    iput-object p2, p0, Lcom/facebook/widget/images/UrlImage;->n:LX/7U4;

    .line 1212144
    iput-object p3, p0, Lcom/facebook/widget/images/UrlImage;->o:Landroid/view/LayoutInflater;

    .line 1212145
    iput-object p4, p0, Lcom/facebook/widget/images/UrlImage;->p:Ljava/util/concurrent/Executor;

    .line 1212146
    iput-object p5, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    .line 1212147
    iput-object p6, p0, Lcom/facebook/widget/images/UrlImage;->r:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    .line 1212148
    iput-object p7, p0, Lcom/facebook/widget/images/UrlImage;->s:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 1212149
    iput-object p8, p0, Lcom/facebook/widget/images/UrlImage;->t:LX/0Sy;

    .line 1212150
    iput-object p9, p0, Lcom/facebook/widget/images/UrlImage;->ad:LX/1HI;

    .line 1212151
    iput-object p10, p0, Lcom/facebook/widget/images/UrlImage;->v:Lcom/facebook/widget/images/ImageCacheReader;

    .line 1212152
    iput-object p11, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    .line 1212153
    iput-object p12, p0, Lcom/facebook/widget/images/UrlImage;->g:LX/0yc;

    .line 1212154
    return-void
.end method

.method private a(LX/5zc;)V
    .locals 3

    .prologue
    .line 1212288
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->u:LX/0Yj;

    if-nez v0, :cond_0

    .line 1212289
    :goto_0
    return-void

    .line 1212290
    :cond_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 1212291
    const-string v1, "operationResult"

    invoke-virtual {p1}, LX/5zc;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212292
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->u:LX/0Yj;

    invoke-virtual {v1, v2, v0}, LX/5zd;->a(LX/0Yj;Ljava/util/Map;)V

    .line 1212293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->u:LX/0Yj;

    goto :goto_0
.end method

.method private a(LX/5zc;Ljava/lang/Throwable;)V
    .locals 7
    .param p2    # Ljava/lang/Throwable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212251
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    .line 1212252
    iget-object v1, v0, LX/4n9;->a:Landroid/net/Uri;

    move-object v0, v1

    .line 1212253
    if-eqz v0, :cond_0

    .line 1212254
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->a:LX/4n9;

    .line 1212255
    iget-object v2, v1, LX/4n9;->a:Landroid/net/Uri;

    move-object v1, v2

    .line 1212256
    invoke-virtual {p1}, LX/5zc;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 1212257
    invoke-static {v0}, LX/5zd;->d(LX/5zd;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1212258
    :cond_0
    :goto_0
    return-void

    .line 1212259
    :cond_1
    iget-object v4, v0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5za;

    .line 1212260
    if-eqz v4, :cond_0

    .line 1212261
    iget-object v5, v4, LX/5za;->b:Landroid/net/Uri;

    move-object v5, v5

    .line 1212262
    const v4, 0x530001

    const-string v6, "UrlImageBindModelToRender"

    invoke-static {v0, v5, v4, v6}, LX/5zd;->d(LX/5zd;Landroid/net/Uri;ILjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1212263
    iget-object v4, v0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5za;

    .line 1212264
    iget-object v6, v4, LX/5za;->e:LX/0Yj;

    move-object v4, v6

    .line 1212265
    if-eqz v4, :cond_4

    .line 1212266
    iget-object v6, v4, LX/0Yj;->l:Ljava/util/Map;

    move-object v6, v6

    .line 1212267
    if-eqz v3, :cond_2

    .line 1212268
    const-string p0, "UrlImageFetchedImageSource"

    invoke-interface {v6, p0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212269
    :cond_2
    if-eqz p2, :cond_7

    .line 1212270
    instance-of p0, p2, Ljava/util/concurrent/CancellationException;

    if-nez p0, :cond_6

    .line 1212271
    const-string p0, "operationResult"

    sget-object p1, LX/5zc;->FAILURE:LX/5zc;

    invoke-virtual {p1}, LX/5zc;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v6, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212272
    const-string p0, "UrlImageException"

    invoke-static {p2}, LX/1Bz;->getStackTraceAsString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v6, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212273
    :cond_3
    :goto_1
    const-string p0, "UrlImageUrlBeingFetched"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v6, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212274
    invoke-virtual {v4, v6}, LX/0Yj;->a(Ljava/util/Map;)LX/0Yj;

    .line 1212275
    iget-object v6, v0, LX/5zd;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v6, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 1212276
    :cond_4
    iget-object v4, v0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5za;

    .line 1212277
    if-nez v4, :cond_8

    .line 1212278
    :cond_5
    goto :goto_0

    .line 1212279
    :cond_6
    const-string p0, "operationResult"

    sget-object p1, LX/5zc;->CANCELLED:LX/5zc;

    invoke-virtual {p1}, LX/5zc;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v6, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1212280
    :cond_7
    if-eqz v2, :cond_3

    .line 1212281
    const-string p0, "operationResult"

    invoke-interface {v6, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1212282
    :cond_8
    iget-object v6, v0, LX/5zd;->e:Ljava/util/Map;

    .line 1212283
    iget-object p0, v4, LX/5za;->b:Landroid/net/Uri;

    move-object p0, p0

    .line 1212284
    invoke-interface {v6, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212285
    iget-object v6, v4, LX/5za;->c:Ljava/util/List;

    move-object v4, v6

    .line 1212286
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 1212287
    iget-object p0, v0, LX/5zd;->e:Ljava/util/Map;

    invoke-interface {p0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 1212232
    if-nez p1, :cond_1

    .line 1212233
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    invoke-static {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setMode(Lcom/facebook/widget/images/UrlImage;LX/7UH;)V

    .line 1212234
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->B()Z

    .line 1212235
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212236
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212237
    :cond_0
    sget-object v0, LX/5zc;->FAILURE:LX/5zc;

    invoke-direct {p0, v0, v2}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;Ljava/lang/Throwable;)V

    .line 1212238
    :goto_0
    return-void

    .line 1212239
    :cond_1
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1212240
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    .line 1212241
    :cond_2
    invoke-virtual {p0, p1}, Lcom/facebook/widget/images/UrlImage;->setImageSpecDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1212242
    sget-object v0, LX/7UH;->LOADED_IMAGE:LX/7UH;

    invoke-static {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setMode(Lcom/facebook/widget/images/UrlImage;LX/7UH;)V

    .line 1212243
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1212244
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212245
    :cond_3
    sget-object v0, LX/5zc;->SUCCESS:LX/5zc;

    invoke-direct {p0, v0, v2}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;Ljava/lang/Throwable;)V

    .line 1212246
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    if-eqz v0, :cond_4

    .line 1212247
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->I:LX/7UN;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, LX/7UN;->a(I)V

    .line 1212248
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->G:LX/7UJ;

    if-eqz v0, :cond_5

    .line 1212249
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->G:LX/7UJ;

    invoke-virtual {v0}, LX/7UJ;->a()V

    .line 1212250
    :cond_5
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->A()V

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;LX/4n2;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/4n2;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1212225
    if-nez p1, :cond_0

    .line 1212226
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setImageParams(LX/4n9;)V

    .line 1212227
    :goto_0
    return-void

    .line 1212228
    :cond_0
    invoke-static {p1}, LX/4n9;->a(Landroid/net/Uri;)LX/4n8;

    move-result-object v0

    .line 1212229
    iput-object p2, v0, LX/4n8;->d:LX/4n2;

    .line 1212230
    move-object v0, v0

    .line 1212231
    invoke-virtual {v0}, LX/4n8;->a()LX/4n9;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setImageParams(LX/4n9;)V

    goto :goto_0
.end method

.method private a(Landroid/widget/ImageView;LX/7UI;LX/7UH;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v9, -0x1

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 1212193
    iget-object v1, p2, LX/7UI;->a:LX/4n9;

    if-eqz v1, :cond_a

    .line 1212194
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    iget-object v2, p2, LX/7UI;->a:LX/4n9;

    .line 1212195
    iget-object v3, v2, LX/4n9;->a:Landroid/net/Uri;

    move-object v2, v3

    .line 1212196
    const v3, 0x53000c

    const-string v4, "UrlImageUpdateImageView"

    invoke-virtual {v1, v2, v3, v4}, LX/5zd;->a(Landroid/net/Uri;ILjava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 1212197
    :goto_0
    iget-object v2, p2, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1212198
    iget-object v2, p2, LX/7UI;->g:Landroid/graphics/Matrix;

    if-nez v2, :cond_1

    sget-object v2, Lcom/facebook/widget/images/UrlImage;->l:Landroid/graphics/Matrix;

    :goto_1
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1212199
    iget-boolean v2, p2, LX/7UI;->h:Z

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 1212200
    iget-object v2, p2, LX/7UI;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p2, LX/7UI;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v9, :cond_2

    .line 1212201
    iget-object v2, p2, LX/7UI;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1212202
    :goto_2
    iget-object v2, p2, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_3

    .line 1212203
    iget-object v0, p2, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {p1, v0}, Lcom/facebook/widget/images/UrlImage;->c(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    move v6, v5

    .line 1212204
    :goto_3
    if-eqz v1, :cond_0

    .line 1212205
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    iget-object v2, p2, LX/7UI;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    iget-object v2, p2, LX/7UI;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v9, :cond_6

    move v2, v5

    :goto_4
    iget-object v3, p2, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_7

    move v3, v5

    :goto_5
    iget-object v4, p2, LX/7UI;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_8

    move v4, v5

    :goto_6
    iget-object v8, p2, LX/7UI;->c:Ljava/lang/Integer;

    if-eqz v8, :cond_9

    iget-object v8, p2, LX/7UI;->c:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-eq v8, v9, :cond_9

    :goto_7
    invoke-virtual {p3}, LX/7UH;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1212206
    iget-object v8, v1, LX/0Yj;->l:Ljava/util/Map;

    move-object v9, v8

    .line 1212207
    const-string v8, "UrlImageDoesBackgroundResourceIdExist"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v9, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212208
    const-string v8, "UrlImageIsDarwableFromFetchImageParams"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v9, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212209
    const-string v8, "UrlImageDoesImageSpecDrawableExist"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v9, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212210
    const-string v8, "UrlImageDoesImageSpecResourceIdExist"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v9, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212211
    const-string v8, "UrlImageCurrentMode"

    invoke-interface {v9, v8, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212212
    if-eqz v6, :cond_b

    sget-object v8, LX/5zc;->SUCCESS:LX/5zc;

    invoke-virtual {v8}, LX/5zc;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1212213
    :goto_8
    const-string p0, "operationResult"

    invoke-interface {v9, p0, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1212214
    invoke-virtual {v0, v1, v9}, LX/5zd;->a(LX/0Yj;Ljava/util/Map;)V

    .line 1212215
    :cond_0
    return-void

    .line 1212216
    :cond_1
    iget-object v2, p2, LX/7UI;->g:Landroid/graphics/Matrix;

    goto/16 :goto_1

    .line 1212217
    :cond_2
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1212218
    :cond_3
    iget-object v2, p2, LX/7UI;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_4

    .line 1212219
    iget-object v0, p2, LX/7UI;->d:Landroid/graphics/drawable/Drawable;

    invoke-static {p1, v0}, Lcom/facebook/widget/images/UrlImage;->c(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    move v6, v5

    goto :goto_3

    .line 1212220
    :cond_4
    iget-object v2, p2, LX/7UI;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    iget-object v2, p2, LX/7UI;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v9, :cond_5

    .line 1212221
    iget-object v0, p2, LX/7UI;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    move v6, v5

    goto/16 :goto_3

    .line 1212222
    :cond_5
    invoke-static {p1, v0}, Lcom/facebook/widget/images/UrlImage;->c(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    move v6, v7

    goto/16 :goto_3

    :cond_6
    move v2, v7

    .line 1212223
    goto/16 :goto_4

    :cond_7
    move v3, v7

    goto/16 :goto_5

    :cond_8
    move v4, v7

    goto/16 :goto_6

    :cond_9
    move v5, v7

    goto :goto_7

    :cond_a
    move-object v1, v0

    goto/16 :goto_0

    .line 1212224
    :cond_b
    sget-object v8, LX/5zc;->FAILURE:LX/5zc;

    invoke-virtual {v8}, LX/5zc;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_8
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 13

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v12

    move-object v0, p0

    check-cast v0, Lcom/facebook/widget/images/UrlImage;

    invoke-static {v12}, LX/1eu;->a(LX/0QB;)LX/1eu;

    move-result-object v1

    check-cast v1, LX/1eu;

    new-instance v3, LX/7U4;

    invoke-static {v12}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v2

    check-cast v2, LX/1HI;

    invoke-direct {v3, v2}, LX/7U4;-><init>(LX/1HI;)V

    move-object v2, v3

    check-cast v2, LX/7U4;

    invoke-static {v12}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    invoke-static {v12}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {v12}, LX/5zd;->a(LX/0QB;)LX/5zd;

    move-result-object v5

    check-cast v5, LX/5zd;

    invoke-static {v12}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v6

    check-cast v6, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static {v12}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v7

    check-cast v7, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static {v12}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v8

    check-cast v8, LX/0Sy;

    invoke-static {v12}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v9

    check-cast v9, LX/1HI;

    invoke-static {v12}, Lcom/facebook/widget/images/ImageCacheReader;->b(LX/0QB;)Lcom/facebook/widget/images/ImageCacheReader;

    move-result-object v10

    check-cast v10, Lcom/facebook/widget/images/ImageCacheReader;

    invoke-static {v12}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v11

    check-cast v11, LX/0So;

    invoke-static {v12}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v12

    check-cast v12, LX/0yc;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/widget/images/UrlImage;->a(LX/1eu;LX/7U4;Landroid/view/LayoutInflater;Ljava/util/concurrent/Executor;LX/5zd;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;Lcom/facebook/common/perftest/PerfTestConfig;LX/0Sy;LX/1HI;Lcom/facebook/widget/images/ImageCacheReader;LX/0So;LX/0yc;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1212190
    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212191
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    aput-object p1, v0, v4

    const/4 v1, 0x3

    sget v2, Lcom/facebook/widget/images/UrlImage;->i:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/facebook/widget/images/UrlImage;->j:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1212192
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1212178
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_1

    .line 1212179
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212180
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212181
    :cond_0
    sget-object v0, LX/5zc;->CANCELLED:LX/5zc;

    invoke-direct {p0, v0, p1}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;Ljava/lang/Throwable;)V

    .line 1212182
    :goto_0
    return-void

    .line 1212183
    :cond_1
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1212184
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    .line 1212185
    :cond_2
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    invoke-static {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setMode(Lcom/facebook/widget/images/UrlImage;LX/7UH;)V

    .line 1212186
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1212187
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212188
    :cond_3
    sget-object v0, LX/5zc;->FAILURE:LX/5zc;

    invoke-direct {p0, v0, p1}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;Ljava/lang/Throwable;)V

    .line 1212189
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->B()Z

    goto :goto_0
.end method

.method private static a(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)Z
    .locals 5
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1212167
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1212168
    if-nez p1, :cond_1

    .line 1212169
    :cond_0
    :goto_0
    return v2

    .line 1212170
    :cond_1
    if-ne p1, v0, :cond_2

    move v2, v3

    .line 1212171
    goto :goto_0

    .line 1212172
    :cond_2
    instance-of v1, v0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v1, :cond_0

    .line 1212173
    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    move v1, v2

    .line 1212174
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 1212175
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-ne v4, p1, :cond_3

    move v2, v3

    .line 1212176
    goto :goto_0

    .line 1212177
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1212162
    sparse-switch p0, :sswitch_data_0

    .line 1212163
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1212164
    :sswitch_0
    const-string v0, "GONE"

    goto :goto_0

    .line 1212165
    :sswitch_1
    const-string v0, "INVISIBLE"

    goto :goto_0

    .line 1212166
    :sswitch_2
    const-string v0, "VISIBLE"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch
.end method

.method public static b()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212155
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212156
    sget-object v0, Lcom/facebook/widget/images/UrlImage;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    .line 1212157
    :cond_0
    sget-object v0, Lcom/facebook/widget/images/UrlImage;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/images/UrlImage;

    .line 1212158
    iget-boolean v2, v0, Lcom/facebook/widget/images/UrlImage;->f:Z

    if-nez v2, :cond_2

    iget-boolean v2, v0, Lcom/facebook/widget/images/UrlImage;->e:Z

    if-nez v2, :cond_1

    .line 1212159
    :cond_2
    invoke-direct {v0}, Lcom/facebook/widget/images/UrlImage;->q()V

    goto :goto_0

    .line 1212160
    :cond_3
    sget-object v0, Lcom/facebook/widget/images/UrlImage;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1212161
    return-void
.end method

.method private static b(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1212459
    invoke-static {p0, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212460
    invoke-static {p0, v1}, Lcom/facebook/widget/images/UrlImage;->c(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 1212461
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1212462
    :cond_0
    return-void
.end method

.method private static c(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212453
    if-eqz p1, :cond_1

    instance-of v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;

    if-eqz v0, :cond_1

    .line 1212454
    instance-of v0, p1, LX/7U1;

    if-eqz v0, :cond_0

    .line 1212455
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1212456
    :cond_0
    check-cast p0, Lcom/facebook/widget/images/ImageViewTouchBase;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/images/ImageViewTouchBase;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1212457
    :goto_0
    return-void

    .line 1212458
    :cond_1
    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static f(Lcom/facebook/widget/images/UrlImage;)V
    .locals 2

    .prologue
    .line 1212564
    new-instance v0, LX/7UD;

    invoke-direct {v0, p0}, LX/7UD;-><init>(Lcom/facebook/widget/images/UrlImage;)V

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->x:Landroid/view/View$OnClickListener;

    .line 1212565
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getDialtonePlaceholderImageSpec()LX/7UI;

    move-result-object v1

    iget-object v1, v1, LX/7UI;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1212566
    const v0, 0x7f030419

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(I)V

    .line 1212567
    return-void
.end method

.method public static g(Lcom/facebook/widget/images/UrlImage;)Z
    .locals 2

    .prologue
    .line 1212563
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->g:LX/0yc;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0yc;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private getCallerContext()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1212560
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->T:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v0, :cond_0

    .line 1212561
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->y()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->T:Lcom/facebook/common/callercontext/CallerContext;

    .line 1212562
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->T:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method private getDialtonePlaceholderImageSpec()LX/7UI;
    .locals 2

    .prologue
    .line 1212555
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->C:LX/7UI;

    if-nez v0, :cond_0

    .line 1212556
    new-instance v0, LX/7UI;

    invoke-direct {v0}, LX/7UI;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->C:LX/7UI;

    .line 1212557
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->C:LX/7UI;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    iput-object v1, v0, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    .line 1212558
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->C:LX/7UI;

    const v1, 0x7f0203c7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/7UI;->c:Ljava/lang/Integer;

    .line 1212559
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->C:LX/7UI;

    return-object v0
.end method

.method private getFadeInAnimation()Landroid/view/animation/Animation;
    .locals 2

    .prologue
    .line 1212551
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->F:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    .line 1212552
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04003e

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->F:Landroid/view/animation/Animation;

    .line 1212553
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->F:Landroid/view/animation/Animation;

    new-instance v1, LX/7UF;

    invoke-direct {v1, p0}, LX/7UF;-><init>(Lcom/facebook/widget/images/UrlImage;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1212554
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->F:Landroid/view/animation/Animation;

    return-object v0
.end method

.method private getFetchUrlHashCode()I
    .locals 1

    .prologue
    .line 1212548
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    .line 1212549
    iget-object p0, v0, LX/4n9;->a:Landroid/net/Uri;

    move-object v0, p0

    .line 1212550
    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method private getInternalPlaceholderImageSpec()LX/7UI;
    .locals 1

    .prologue
    .line 1212547
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getDialtonePlaceholderImageSpec()LX/7UI;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    goto :goto_0
.end method

.method private getShortId()I
    .locals 1

    .prologue
    .line 1212546
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    rem-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method private h()Z
    .locals 3

    .prologue
    .line 1212543
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->g:LX/0yc;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->a:LX/4n9;

    .line 1212544
    iget-object v2, v1, LX/4n9;->a:Landroid/net/Uri;

    move-object v1, v2

    .line 1212545
    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->T:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/0yc;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 1212539
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->m:Ljava/lang/String;

    .line 1212540
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->m:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1212541
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->g:LX/0yc;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->T:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/0yc;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->ah:Z

    .line 1212542
    return-void

    :catch_0
    goto :goto_0
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 1212538
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->g:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->ah:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1212527
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1212528
    const-string v0, ""

    .line 1212529
    :goto_0
    return-object v0

    .line 1212530
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1212531
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1212532
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1212533
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1212534
    const-string v2, " <- "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object p0, v0

    .line 1212535
    goto :goto_1

    .line 1212536
    :cond_1
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1212537
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private l()V
    .locals 1

    .prologue
    .line 1212520
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1212521
    sget v0, Lcom/facebook/widget/images/UrlImage;->j:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/facebook/widget/images/UrlImage;->j:I

    .line 1212522
    const-string v0, "onAttachingToViewTree"

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    .line 1212523
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->e:Z

    .line 1212524
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->f:Z

    .line 1212525
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->z()V

    .line 1212526
    return-void
.end method

.method private m()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212514
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212515
    sget v0, Lcom/facebook/widget/images/UrlImage;->j:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/facebook/widget/images/UrlImage;->j:I

    .line 1212516
    const-string v0, "onDetachedFromViewTree"

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    .line 1212517
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->e:Z

    .line 1212518
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->r()V

    .line 1212519
    return-void
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 1212513
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212508
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212509
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212510
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->u()V

    .line 1212511
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->s()V

    .line 1212512
    return-void
.end method

.method private p()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212504
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212505
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212506
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->t()V

    .line 1212507
    return-void
.end method

.method private q()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212499
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212500
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212501
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->o()V

    .line 1212502
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->p()V

    .line 1212503
    return-void
.end method

.method private r()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212494
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 1212495
    :cond_0
    :goto_0
    return-void

    .line 1212496
    :cond_1
    sget-object v0, Lcom/facebook/widget/images/UrlImage;->h:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1212497
    sget-object v0, Lcom/facebook/widget/images/UrlImage;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1212498
    sget-object v0, Lcom/facebook/widget/images/UrlImage;->ak:Landroid/os/Handler;

    sget-object v1, Lcom/facebook/widget/images/UrlImage;->al:Ljava/lang/Runnable;

    const v2, 0x10eff77

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method private s()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 1212479
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    if-eqz v0, :cond_2

    .line 1212480
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lcom/facebook/widget/images/UrlImage;->b(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 1212481
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_3

    .line 1212482
    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212483
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212484
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1212485
    sget v0, Lcom/facebook/widget/images/UrlImage;->i:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/facebook/widget/images/UrlImage;->i:I

    .line 1212486
    const-string v0, "closeImageDrawable"

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    .line 1212487
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    const/4 v1, 0x0

    iput-object v1, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    .line 1212488
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-eq v0, v1, :cond_2

    .line 1212489
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setLoadingMode(LX/7UH;)V

    .line 1212490
    :cond_2
    return-void

    .line 1212491
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 1212492
    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1212493
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    goto :goto_0
.end method

.method public static setActivityStarted(Lcom/facebook/widget/images/UrlImage;Z)V
    .locals 1

    .prologue
    .line 1212473
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->S:Z

    if-eq v0, p1, :cond_0

    .line 1212474
    iput-boolean p1, p0, Lcom/facebook/widget/images/UrlImage;->S:Z

    .line 1212475
    if-eqz p1, :cond_1

    .line 1212476
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->c()V

    .line 1212477
    :cond_0
    :goto_0
    return-void

    .line 1212478
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->q()V

    goto :goto_0
.end method

.method public static setMode(Lcom/facebook/widget/images/UrlImage;LX/7UH;)V
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x4

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1211915
    invoke-static {v7}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211916
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211917
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->H:LX/7UK;

    if-eqz v0, :cond_1

    .line 1211918
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->H:LX/7UK;

    invoke-interface {v0, p1}, LX/7UK;->a(LX/7UH;)V

    .line 1211919
    :cond_1
    const/4 v1, 0x0

    .line 1211920
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    if-eqz v0, :cond_2

    .line 1211921
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->a:LX/4n9;

    .line 1211922
    iget-object v2, v1, LX/4n9;->a:Landroid/net/Uri;

    move-object v1, v2

    .line 1211923
    const v2, 0x53000b

    const-string v3, "UrlImageLogMode"

    invoke-virtual {v0, v1, v2, v3}, LX/5zd;->a(Landroid/net/Uri;ILjava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 1211924
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->M:Z

    if-eqz v0, :cond_d

    .line 1211925
    sget-object v0, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-eq p1, v0, :cond_3

    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-ne p1, v0, :cond_5

    .line 1211926
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1211927
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1211928
    :cond_4
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->C()V

    .line 1211929
    :cond_5
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-ne p1, v0, :cond_a

    .line 1211930
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1211931
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    if-eq v0, v2, :cond_6

    .line 1211932
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211933
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211934
    :cond_6
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getInternalPlaceholderImageSpec()LX/7UI;

    move-result-object v2

    invoke-direct {p0, v0, v2, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/widget/ImageView;LX/7UI;LX/7UH;)V

    .line 1211935
    :cond_7
    :goto_0
    if-eqz v1, :cond_8

    .line 1211936
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    invoke-virtual {p1}, LX/7UH;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    invoke-virtual {v3}, LX/7UH;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/facebook/widget/images/UrlImage;->M:Z

    iget-object v5, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getVisibility()I

    move-result v5

    .line 1211937
    invoke-static {v0}, LX/5zd;->d(LX/5zd;)Z

    move-result v6

    if-nez v6, :cond_1a

    .line 1211938
    :cond_8
    :goto_1
    iput-object p1, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    .line 1211939
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->H:LX/7UK;

    if-eqz v0, :cond_9

    .line 1211940
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->H:LX/7UK;

    invoke-interface {v0, p1}, LX/7UK;->b(LX/7UH;)V

    .line 1211941
    :cond_9
    return-void

    .line 1211942
    :cond_a
    sget-object v0, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne p1, v0, :cond_c

    .line 1211943
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1211944
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211945
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211946
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    invoke-direct {p0, v0, v2, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/widget/ImageView;LX/7UI;LX/7UH;)V

    .line 1211947
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->U:Z

    if-eqz v0, :cond_7

    .line 1211948
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    if-eq v0, v2, :cond_b

    .line 1211949
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getInternalPlaceholderImageSpec()LX/7UI;

    move-result-object v2

    invoke-direct {p0, v0, v2, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/widget/ImageView;LX/7UI;LX/7UH;)V

    .line 1211950
    :cond_b
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFadeInAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 1211951
    :cond_c
    sget-object v0, LX/7UH;->PROGRESS_BAR:LX/7UH;

    if-ne p1, v0, :cond_7

    .line 1211952
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1211953
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1211954
    :cond_d
    sget-object v0, LX/7UH;->PROGRESS_BAR:LX/7UH;

    if-ne p1, v0, :cond_11

    .line 1211955
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->E:LX/7UL;

    sget-object v2, LX/7UL;->PROGRESS_BAR_DETERMINATE_WITH_PLACEHOLDER:LX/7UL;

    if-eq v0, v2, :cond_e

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->E:LX/7UL;

    sget-object v2, LX/7UL;->PROGRESS_BAR_INDETERMINATE_WITH_PLACEHOLDER:LX/7UL;

    if-ne v0, v2, :cond_10

    .line 1211956
    :cond_e
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    invoke-direct {p0, v0, v2, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/widget/ImageView;LX/7UI;LX/7UH;)V

    .line 1211957
    :goto_2
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->D()V

    .line 1211958
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->C()V

    .line 1211959
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1211960
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1211961
    :cond_f
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    goto/16 :goto_0

    .line 1211962
    :cond_10
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211963
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    .line 1211964
    :cond_11
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-ne p1, v0, :cond_14

    .line 1211965
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1211966
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    if-eq v0, v2, :cond_12

    .line 1211967
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211968
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211969
    :cond_12
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1211970
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1211971
    :cond_13
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getInternalPlaceholderImageSpec()LX/7UI;

    move-result-object v2

    invoke-direct {p0, v0, v2, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/widget/ImageView;LX/7UI;LX/7UH;)V

    goto/16 :goto_0

    .line 1211972
    :cond_14
    sget-object v0, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne p1, v0, :cond_7

    .line 1211973
    invoke-static {v7}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1211974
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    .line 1211975
    :cond_15
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v2, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-ne v0, v2, :cond_16

    iget v0, p0, Lcom/facebook/widget/images/UrlImage;->af:I

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    move-result v2

    if-ne v0, v2, :cond_16

    .line 1211976
    invoke-static {v7}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1211977
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    .line 1211978
    :cond_16
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/images/UrlImage;->af:I

    .line 1211979
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 1211980
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211981
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    invoke-direct {p0, v0, v2, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/widget/ImageView;LX/7UI;LX/7UH;)V

    .line 1211982
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->U:Z

    if-eqz v0, :cond_19

    .line 1211983
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    if-eq v0, v2, :cond_17

    .line 1211984
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getInternalPlaceholderImageSpec()LX/7UI;

    move-result-object v2

    invoke-direct {p0, v0, v2, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/widget/ImageView;LX/7UI;LX/7UH;)V

    .line 1211985
    :cond_17
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211986
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFadeInAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1211987
    :goto_3
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1211988
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1211989
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1211990
    :cond_18
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->C()V

    goto/16 :goto_0

    .line 1211991
    :cond_19
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 1211992
    :cond_1a
    iget-object v6, v1, LX/0Yj;->l:Ljava/util/Map;

    move-object v6, v6

    .line 1211993
    const-string v7, "UrlImageNewMode"

    invoke-interface {v6, v7, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211994
    const-string v7, "UrlImageCurrentMode"

    invoke-interface {v6, v7, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211995
    const-string v7, "UrlImageIsShownInGallery"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211996
    const-string v7, "UrlImageIsImageViewVisible"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1211997
    invoke-virtual {v0, v1, v6}, LX/5zd;->a(LX/0Yj;Ljava/util/Map;)V

    goto/16 :goto_1
.end method

.method private t()V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212463
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    if-eqz v0, :cond_2

    .line 1212464
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v1, v1, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lcom/facebook/widget/images/UrlImage;->b(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 1212465
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_1

    .line 1212466
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212467
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212468
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1212469
    sget v0, Lcom/facebook/widget/images/UrlImage;->i:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/facebook/widget/images/UrlImage;->i:I

    .line 1212470
    const-string v0, "closePlaceholderDrawable"

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    .line 1212471
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    const/4 v1, 0x0

    iput-object v1, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    .line 1212472
    :cond_2
    return-void
.end method

.method private u()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    .line 1212294
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    if-eqz v0, :cond_3

    .line 1212295
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212296
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    .line 1212297
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    if-eqz v0, :cond_1

    .line 1212298
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    invoke-interface {v0}, LX/1ca;->g()Z

    .line 1212299
    iput-object v2, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    .line 1212300
    :cond_1
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1212301
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212302
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212303
    :cond_2
    sget-object v0, LX/5zc;->CANCELLED:LX/5zc;

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;)V

    .line 1212304
    sget-object v0, LX/5zc;->CANCELLED:LX/5zc;

    invoke-direct {p0, v0, v2}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;Ljava/lang/Throwable;)V

    .line 1212305
    :cond_3
    return-void
.end method

.method private v()V
    .locals 4

    .prologue
    .line 1212449
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->a:LX/4n9;

    .line 1212450
    iget-object v2, v1, LX/4n9;->a:Landroid/net/Uri;

    move-object v1, v2

    .line 1212451
    const v2, 0x530002

    const-string v3, "UrlImagePipelineExperiment"

    invoke-virtual {v0, v1, v2, v3}, LX/5zd;->b(Landroid/net/Uri;ILjava/lang/String;)LX/0Yj;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->u:LX/0Yj;

    .line 1212452
    return-void
.end method

.method public static w(Lcom/facebook/widget/images/UrlImage;)V
    .locals 4

    .prologue
    .line 1211812
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    .line 1211813
    iget-object v1, v0, LX/4n9;->a:Landroid/net/Uri;

    move-object v0, v1

    .line 1211814
    if-eqz v0, :cond_0

    .line 1211815
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->a:LX/4n9;

    .line 1211816
    iget-object v2, v1, LX/4n9;->a:Landroid/net/Uri;

    move-object v1, v2

    .line 1211817
    const v2, 0x530001

    const-string v3, "UrlImageBindModelToRender"

    invoke-virtual {v0, v1, v2, v3}, LX/5zd;->b(Landroid/net/Uri;ILjava/lang/String;)LX/0Yj;

    .line 1211818
    :cond_0
    return-void
.end method

.method private x()V
    .locals 10
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 1211998
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1211999
    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212000
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212001
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getOrCreateRequests()LX/0Px;

    move-result-object v0

    .line 1212002
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1212003
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->ad:LX/1HI;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1bf;

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getCallerContext()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v0

    .line 1212004
    :goto_0
    invoke-static {v4}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1212005
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    .line 1212006
    :cond_1
    new-instance v1, LX/7UE;

    invoke-direct {v1, p0}, LX/7UE;-><init>(Lcom/facebook/widget/images/UrlImage;)V

    .line 1212007
    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    .line 1212008
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    invoke-interface {v0}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1212009
    iput-boolean v3, p0, Lcom/facebook/widget/images/UrlImage;->U:Z

    .line 1212010
    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v0

    .line 1212011
    :goto_1
    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    invoke-interface {v2, v1, v0}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1212012
    return-void

    .line 1212013
    :cond_2
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->n:LX/7U4;

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getCallerContext()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 1212014
    new-instance v5, LX/4e3;

    invoke-direct {v5}, LX/4e3;-><init>()V

    move-object v5, v5

    .line 1212015
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1bf;

    .line 1212016
    iget-object v8, v1, LX/7U4;->b:LX/1HI;

    invoke-virtual {v8, v6, v2}, LX/1HI;->a(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v8

    .line 1212017
    invoke-interface {v8}, LX/1ca;->b()Z

    move-result v6

    const-string v9, "Bitmap-cache-only requests should be executed synchronously"

    invoke-static {v6, v9}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1212018
    invoke-interface {v8}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1FJ;

    .line 1212019
    invoke-interface {v8}, LX/1ca;->g()Z

    .line 1212020
    if-eqz v6, :cond_3

    .line 1212021
    invoke-virtual {v5, v6}, LX/4e3;->a(LX/1FJ;)Z

    .line 1212022
    invoke-virtual {v6}, LX/1FJ;->close()V

    .line 1212023
    :goto_2
    move-object v0, v5

    .line 1212024
    goto :goto_0

    .line 1212025
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->E:LX/7UL;

    sget-object v2, LX/7UL;->PROGRESS_BAR_HIDDEN:LX/7UL;

    if-eq v0, v2, :cond_5

    sget-object v0, LX/7UH;->PROGRESS_BAR:LX/7UH;

    :goto_3
    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setLoadingMode(LX/7UH;)V

    .line 1212026
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->p:Ljava/util/concurrent/Executor;

    goto :goto_1

    .line 1212027
    :cond_5
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    goto :goto_3

    .line 1212028
    :cond_6
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    invoke-static {v1, v6, v5, v2}, LX/7U4;->a$redex0(LX/7U4;Ljava/util/Iterator;LX/4e3;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_2
.end method

.method private y()Lcom/facebook/common/callercontext/CallerContext;
    .locals 3

    .prologue
    .line 1211912
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->ab:Z

    if-eqz v0, :cond_1

    .line 1211913
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "unknown"

    const-string v2, "instrumented_image_fetch"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 1211914
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    goto :goto_0
.end method

.method private z()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1211891
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211892
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    invoke-static {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setMode(Lcom/facebook/widget/images/UrlImage;LX/7UH;)V

    .line 1211893
    const v0, 0x7f030419

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(I)V

    .line 1211894
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->B()Z

    .line 1211895
    :goto_0
    return-void

    .line 1211896
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 1211897
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->v:Lcom/facebook/widget/images/ImageCacheReader;

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v2, v2, LX/7UI;->a:LX/4n9;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/images/ImageCacheReader;->a(LX/4n9;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    .line 1211898
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_1

    .line 1211899
    sget v0, Lcom/facebook/widget/images/UrlImage;->i:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/facebook/widget/images/UrlImage;->i:I

    .line 1211900
    const-string v0, "updateImage"

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    .line 1211901
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->N:Z

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->U:Z

    .line 1211902
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->Q:Z

    if-nez v0, :cond_2

    .line 1211903
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->o()V

    .line 1211904
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    if-nez v0, :cond_3

    .line 1211905
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setImageSpecDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1211906
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    invoke-static {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setMode(Lcom/facebook/widget/images/UrlImage;LX/7UH;)V

    goto :goto_0

    .line 1211907
    :cond_3
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1211908
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211909
    :cond_4
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->b()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->ab:Z

    if-eqz v0, :cond_6

    .line 1211910
    :cond_5
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->v()V

    .line 1211911
    :cond_6
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->x()V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/4n9;LX/33B;)V
    .locals 8
    .param p1    # LX/4n9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/33B;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 1211870
    iput-object p2, p0, Lcom/facebook/widget/images/UrlImage;->D:LX/33B;

    .line 1211871
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    invoke-virtual {v0, p1}, LX/7UI;->a(LX/4n9;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1211872
    invoke-static {v6}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211873
    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v2, v1

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v6

    const/4 v3, 0x3

    if-nez p1, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x4

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1211874
    :cond_0
    iput-object v7, p0, Lcom/facebook/widget/images/UrlImage;->b:LX/0Px;

    .line 1211875
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->u()V

    .line 1211876
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->Q:Z

    if-nez v0, :cond_1

    .line 1211877
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->s()V

    .line 1211878
    :cond_1
    if-nez p1, :cond_5

    .line 1211879
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iput-object v7, v0, LX/7UI;->a:LX/4n9;

    .line 1211880
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->c()V

    .line 1211881
    :cond_3
    return-void

    .line 1211882
    :cond_4
    iget-object v0, p1, LX/4n9;->a:Landroid/net/Uri;

    move-object v0, v0

    .line 1211883
    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    .line 1211884
    :cond_5
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->E()V

    .line 1211885
    iput v1, p0, Lcom/facebook/widget/images/UrlImage;->W:I

    .line 1211886
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iput-object p1, v0, LX/7UI;->a:LX/4n9;

    .line 1211887
    invoke-static {v6}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1211888
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211889
    :cond_6
    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->b()Z

    move-result v0

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->ab:Z

    if-eqz v0, :cond_2

    .line 1211890
    :cond_7
    invoke-static {p0}, Lcom/facebook/widget/images/UrlImage;->w(Lcom/facebook/widget/images/UrlImage;)V

    goto :goto_1
.end method

.method public final a(Landroid/graphics/drawable/Drawable;LX/1ca;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 1211853
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    if-ne p2, v0, :cond_4

    .line 1211854
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    .line 1211855
    if-nez p1, :cond_2

    .line 1211856
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211857
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211858
    :cond_0
    sget-object v0, LX/5zc;->FAILURE:LX/5zc;

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;)V

    .line 1211859
    :goto_0
    invoke-direct {p0, p1}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1211860
    :cond_1
    :goto_1
    return-void

    .line 1211861
    :cond_2
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1211862
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211863
    :cond_3
    sget-object v0, LX/5zc;->SUCCESS:LX/5zc;

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;)V

    goto :goto_0

    .line 1211864
    :cond_4
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1211865
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211866
    :cond_5
    instance-of v0, p1, Ljava/io/Closeable;

    if-eqz v0, :cond_1

    .line 1211867
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1211868
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211869
    :cond_6
    check-cast p1, Ljava/io/Closeable;

    invoke-static {p1}, LX/1pX;->a(Ljava/io/Closeable;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;LX/1ca;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 1211840
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    if-ne p2, v0, :cond_4

    .line 1211841
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    .line 1211842
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_2

    .line 1211843
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211844
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211845
    :cond_0
    sget-object v0, LX/5zc;->CANCELLED:LX/5zc;

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;)V

    .line 1211846
    :goto_0
    invoke-direct {p0, p1}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/Throwable;)V

    .line 1211847
    :cond_1
    :goto_1
    return-void

    .line 1211848
    :cond_2
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1211849
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211850
    :cond_3
    sget-object v0, LX/5zc;->FAILURE:LX/5zc;

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(LX/5zc;)V

    goto :goto_0

    .line 1211851
    :cond_4
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1211852
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1211836
    iput-boolean p1, p0, Lcom/facebook/widget/images/UrlImage;->L:Z

    .line 1211837
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->q:LX/5zd;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, LX/5zd;->a(Z)V

    .line 1211838
    return-void

    .line 1211839
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1211835
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->R:Z

    return v0
.end method

.method public final c()V
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1211829
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->e:Z

    if-eqz v0, :cond_1

    .line 1211830
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->S:Z

    if-nez v0, :cond_2

    .line 1211831
    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1211832
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211833
    :cond_1
    :goto_0
    return-void

    .line 1211834
    :cond_2
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->z()V

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1211828
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final drawableStateChanged()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1211819
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->drawableStateChanged()V

    .line 1211820
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->P:Z

    if-nez v0, :cond_0

    .line 1211821
    :goto_0
    return-void

    .line 1211822
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->aa:Z

    if-eqz v0, :cond_1

    .line 1211823
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    .line 1211824
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    iget v2, p0, Lcom/facebook/widget/images/UrlImage;->O:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1211825
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    iget v2, p0, Lcom/facebook/widget/images/UrlImage;->O:I

    invoke-virtual {v1, v2, v0}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_0

    .line 1211826
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1211827
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public getAdjustViewBounds()Z
    .locals 1

    .prologue
    .line 1211745
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-boolean v0, v0, LX/7UI;->h:Z

    return v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1211760
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    .line 1211761
    :cond_0
    :goto_0
    return-object v0

    .line 1211762
    :cond_1
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    instance-of v1, v1, LX/7U1;

    if-eqz v1, :cond_2

    .line 1211763
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    check-cast v0, LX/7U1;

    .line 1211764
    invoke-virtual {v0}, LX/7Ty;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1211765
    const/4 v1, 0x0

    .line 1211766
    :goto_1
    move-object v0, v1

    .line 1211767
    goto :goto_0

    .line 1211768
    :cond_2
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    instance-of v1, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 1211769
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 1211770
    :cond_3
    iget-object v1, v0, LX/7U1;->a:LX/1FJ;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ln;

    .line 1211771
    check-cast v1, LX/1lm;

    invoke-virtual {v1}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1
.end method

.method public getImageDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1211772
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getImageParams()LX/4n9;
    .locals 1

    .prologue
    .line 1211773
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    return-object v0
.end method

.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 1211774
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getOrCreateRequests()LX/0Px;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/1bf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1211775
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->b:LX/0Px;

    if-nez v0, :cond_0

    .line 1211776
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, LX/4eI;->a(LX/4n9;Landroid/content/res/Resources;)LX/1bX;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->D:LX/33B;

    .line 1211777
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 1211778
    move-object v2, v0

    .line 1211779
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    .line 1211780
    iget-object v1, v0, LX/4n9;->c:LX/4nA;

    move-object v0, v1

    .line 1211781
    if-nez v0, :cond_1

    .line 1211782
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->b:LX/0Px;

    .line 1211783
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->b:LX/0Px;

    return-object v0

    .line 1211784
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1211785
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    .line 1211786
    iget-object v1, v0, LX/4n9;->c:LX/4nA;

    move-object v0, v1

    .line 1211787
    iget-object v4, v0, LX/4nA;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1211788
    invoke-virtual {v2, v0}, LX/1bX;->b(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1211789
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1211790
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->b:LX/0Px;

    goto :goto_0
.end method

.method public getPlaceHolderResourceId()I
    .locals 1

    .prologue
    .line 1211791
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getPlaceholderImageParams()LX/4n9;
    .locals 1

    .prologue
    .line 1211792
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->a:LX/4n9;

    return-object v0
.end method

.method public getScaleType()Landroid/widget/ImageView$ScaleType;
    .locals 1

    .prologue
    .line 1211793
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x55d1eb0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1211794
    invoke-static {v3}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211795
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211796
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onAttachedToWindow()V

    .line 1211797
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->e:Z

    if-nez v0, :cond_2

    .line 1211798
    const/4 v0, 0x1

    .line 1211799
    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->R:Z

    .line 1211800
    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/images/UrlImage;->T:Lcom/facebook/common/callercontext/CallerContext;

    .line 1211801
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1211802
    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->g:LX/0yc;

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->K:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0yL;

    invoke-virtual {v2, v0}, LX/0yc;->a(LX/0yL;)V

    .line 1211803
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->l()V

    .line 1211804
    :cond_2
    const/16 v0, 0x2d

    const v2, 0x59c48480

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3dc0bd07

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1211805
    invoke-static {v3}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1211806
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211807
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onDetachedFromWindow()V

    .line 1211808
    const/4 v1, 0x0

    .line 1211809
    iput-boolean v1, p0, Lcom/facebook/widget/images/UrlImage;->R:Z

    .line 1211810
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->m()V

    .line 1211811
    const/16 v1, 0x2d

    const v2, 0x3747c041

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x0

    .line 1211746
    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1211747
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1211748
    :cond_0
    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1211749
    invoke-static {p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/widget/images/UrlImage;->T:Lcom/facebook/common/callercontext/CallerContext;

    .line 1211750
    :cond_1
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->n()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1211751
    sget v1, Lcom/facebook/widget/images/UrlImage;->j:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/facebook/widget/images/UrlImage;->j:I

    .line 1211752
    const-string v1, "onFinishTemporaryDetach"

    invoke-direct {p0, v1}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    .line 1211753
    :cond_2
    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->f:Z

    .line 1211754
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->c:LX/1ca;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    const/4 v0, 0x1

    .line 1211755
    :cond_3
    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->Q:Z

    if-eqz v0, :cond_6

    .line 1211756
    :cond_4
    invoke-static {v2}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1211757
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getFetchUrlHashCode()I

    .line 1211758
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->c()V

    .line 1211759
    :cond_6
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1212090
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomViewGroup;->onLayout(ZIIII)V

    .line 1212091
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->A()V

    .line 1212092
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1212131
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212132
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212133
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onStartTemporaryDetach()V

    .line 1212134
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1212135
    sget v0, Lcom/facebook/widget/images/UrlImage;->j:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/facebook/widget/images/UrlImage;->j:I

    .line 1212136
    const-string v0, "onStartTemporaryDetach"

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    .line 1212137
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->f:Z

    .line 1212138
    invoke-static {v1}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1212139
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->w:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    .line 1212140
    :cond_2
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->r()V

    .line 1212141
    return-void
.end method

.method public setAdjustViewBounds(Z)V
    .locals 2

    .prologue
    .line 1212127
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iput-boolean p1, v0, LX/7UI;->h:Z

    .line 1212128
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne v0, v1, :cond_0

    .line 1212129
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 1212130
    :cond_0
    return-void
.end method

.method public setDoFetchImagePerfLogging(Z)V
    .locals 0

    .prologue
    .line 1212125
    iput-boolean p1, p0, Lcom/facebook/widget/images/UrlImage;->ab:Z

    .line 1212126
    return-void
.end method

.method public setHasBeenAttached(Z)V
    .locals 0

    .prologue
    .line 1212123
    iput-boolean p1, p0, Lcom/facebook/widget/images/UrlImage;->R:Z

    .line 1212124
    return-void
.end method

.method public setId(I)V
    .locals 0

    .prologue
    .line 1212120
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setId(I)V

    .line 1212121
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->i()V

    .line 1212122
    return-void
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 1212116
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iput-object p1, v0, LX/7UI;->g:Landroid/graphics/Matrix;

    .line 1212117
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne v0, v1, :cond_0

    .line 1212118
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1212119
    :cond_0
    return-void
.end method

.method public setImageParams(LX/4n9;)V
    .locals 1
    .param p1    # LX/4n9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212114
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/widget/images/UrlImage;->a(LX/4n9;LX/33B;)V

    .line 1212115
    return-void
.end method

.method public setImageParams(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1212110
    if-nez p1, :cond_0

    .line 1212111
    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setImageParams(LX/4n9;)V

    .line 1212112
    :goto_0
    return-void

    .line 1212113
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/images/UrlImage;->a(Landroid/net/Uri;LX/4n2;)V

    goto :goto_0
.end method

.method public setImageSpecDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 5
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212098
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_1

    .line 1212099
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v1, v1, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0, v1}, Lcom/facebook/widget/images/UrlImage;->b(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    .line 1212100
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    check-cast v0, Ljava/io/Closeable;

    invoke-static {v0}, LX/1pX;->a(Ljava/io/Closeable;)V

    .line 1212101
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->Q:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1212102
    const-string v0, "UrlImage"

    const-string v1, "view %d: setImageSpecDrawable: having to explicitly close drawable %x"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->getShortId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v4, v4, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1212103
    :cond_0
    sget v0, Lcom/facebook/widget/images/UrlImage;->i:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/facebook/widget/images/UrlImage;->i:I

    .line 1212104
    const-string v0, "setImageSpecDrawable"

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    .line 1212105
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iput-object p1, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    .line 1212106
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_2

    .line 1212107
    sget v0, Lcom/facebook/widget/images/UrlImage;->i:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/facebook/widget/images/UrlImage;->i:I

    .line 1212108
    const-string v0, "setImageSpecDrawable"

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    .line 1212109
    :cond_2
    return-void
.end method

.method public setIsDialtoneWhitelisted(Z)V
    .locals 0

    .prologue
    .line 1212096
    iput-boolean p1, p0, Lcom/facebook/widget/images/UrlImage;->ah:Z

    .line 1212097
    return-void
.end method

.method public setLoadingMode(LX/7UH;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1212093
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->Q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne v0, v1, :cond_0

    sget-object v0, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-eq p1, v0, :cond_0

    .line 1212094
    :goto_0
    return-void

    .line 1212095
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/widget/images/UrlImage;->setMode(Lcom/facebook/widget/images/UrlImage;LX/7UH;)V

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1212029
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1212030
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->aa:Z

    .line 1212031
    return-void

    .line 1212032
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnImageDownloadListener(LX/7UJ;)V
    .locals 0
    .param p1    # LX/7UJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212088
    iput-object p1, p0, Lcom/facebook/widget/images/UrlImage;->G:LX/7UJ;

    .line 1212089
    return-void
.end method

.method public setOnModeChangedListener(LX/7UK;)V
    .locals 0

    .prologue
    .line 1212086
    iput-object p1, p0, Lcom/facebook/widget/images/UrlImage;->H:LX/7UK;

    .line 1212087
    return-void
.end method

.method public setPlaceHolderDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1212081
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    const/4 v1, 0x0

    iput-object v1, v0, LX/7UI;->c:Ljava/lang/Integer;

    .line 1212082
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iput-object p1, v0, LX/7UI;->d:Landroid/graphics/drawable/Drawable;

    .line 1212083
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1212084
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1212085
    :cond_0
    return-void
.end method

.method public setPlaceHolderResourceId(I)V
    .locals 2

    .prologue
    .line 1212076
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/7UI;->c:Ljava/lang/Integer;

    .line 1212077
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    const/4 v1, 0x0

    iput-object v1, v0, LX/7UI;->d:Landroid/graphics/drawable/Drawable;

    .line 1212078
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1212079
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1212080
    :cond_0
    return-void
.end method

.method public setPlaceHolderScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 2

    .prologue
    .line 1212072
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iput-object p1, v0, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    .line 1212073
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-ne v0, v1, :cond_0

    .line 1212074
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1212075
    :cond_0
    return-void
.end method

.method public setPlaceholderBackgroundResourceId(I)V
    .locals 2

    .prologue
    .line 1212068
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/7UI;->e:Ljava/lang/Integer;

    .line 1212069
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-ne v0, v1, :cond_0

    .line 1212070
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1212071
    :cond_0
    return-void
.end method

.method public setPlaceholderImageParams(LX/4n9;)V
    .locals 2
    .param p1    # LX/4n9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1212054
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    invoke-virtual {v0, p1}, LX/7UI;->a(LX/4n9;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1212055
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->t()V

    .line 1212056
    if-nez p1, :cond_2

    .line 1212057
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iput-object v1, v0, LX/7UI;->a:LX/4n9;

    .line 1212058
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iput-object v1, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    .line 1212059
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->PLACEHOLDER:LX/7UH;

    if-ne v0, v1, :cond_1

    .line 1212060
    sget-object v0, LX/7UH;->PLACEHOLDER:LX/7UH;

    invoke-static {p0, v0}, Lcom/facebook/widget/images/UrlImage;->setMode(Lcom/facebook/widget/images/UrlImage;LX/7UH;)V

    .line 1212061
    :cond_1
    return-void

    .line 1212062
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iput-object p1, v0, LX/7UI;->a:LX/4n9;

    .line 1212063
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->e:Z

    if-eqz v0, :cond_0

    .line 1212064
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->v:Lcom/facebook/widget/images/ImageCacheReader;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/images/ImageCacheReader;->a(LX/4n9;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    .line 1212065
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->B:LX/7UI;

    iget-object v0, v0, LX/7UI;->b:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Ljava/io/Closeable;

    if-eqz v0, :cond_0

    .line 1212066
    sget v0, Lcom/facebook/widget/images/UrlImage;->i:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/facebook/widget/images/UrlImage;->i:I

    .line 1212067
    const-string v0, "setPlaceholderImageParams"

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/UrlImage;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setPressedOverlayColorResourceId(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1212048
    if-gtz p1, :cond_0

    .line 1212049
    iput v0, p0, Lcom/facebook/widget/images/UrlImage;->O:I

    .line 1212050
    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->P:Z

    .line 1212051
    :goto_0
    return-void

    .line 1212052
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/images/UrlImage;->O:I

    .line 1212053
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->P:Z

    goto :goto_0
.end method

.method public setProgressBarMode(LX/7UL;)V
    .locals 2

    .prologue
    .line 1212040
    iput-object p1, p0, Lcom/facebook/widget/images/UrlImage;->E:LX/7UL;

    .line 1212041
    invoke-direct {p0}, Lcom/facebook/widget/images/UrlImage;->D()V

    .line 1212042
    sget-object v0, LX/7UL;->PROGRESS_BAR_HIDDEN:LX/7UL;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1212043
    iget-boolean v0, p0, Lcom/facebook/widget/images/UrlImage;->M:Z

    if-eqz v0, :cond_1

    .line 1212044
    const v0, 0x7f0d05a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1212045
    iget-object v1, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1212046
    :cond_0
    :goto_0
    return-void

    .line 1212047
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->A:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/UrlImage;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setRetainImageDuringUpdate(Z)V
    .locals 0

    .prologue
    .line 1212038
    iput-boolean p1, p0, Lcom/facebook/widget/images/UrlImage;->Q:Z

    .line 1212039
    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 2

    .prologue
    .line 1212034
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    iput-object p1, v0, LX/7UI;->f:Landroid/widget/ImageView$ScaleType;

    .line 1212035
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    sget-object v1, LX/7UH;->LOADED_IMAGE:LX/7UH;

    if-ne v0, v1, :cond_0

    .line 1212036
    iget-object v0, p0, Lcom/facebook/widget/images/UrlImage;->z:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1212037
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1212033
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "mode"

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->d:LX/7UH;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "visibility"

    invoke-virtual {p0}, Lcom/facebook/widget/images/UrlImage;->getVisibility()I

    move-result v2

    invoke-static {v2}, Lcom/facebook/widget/images/UrlImage;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "imageSpec"

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->a:LX/7UI;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "attachedToViewTree"

    iget-boolean v2, p0, Lcom/facebook/widget/images/UrlImage;->e:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "attachedToWindow"

    iget-boolean v2, p0, Lcom/facebook/widget/images/UrlImage;->R:Z

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Z)LX/237;

    move-result-object v0

    const-string v1, "analyticsTagContainer"

    iget-object v2, p0, Lcom/facebook/widget/images/UrlImage;->T:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
