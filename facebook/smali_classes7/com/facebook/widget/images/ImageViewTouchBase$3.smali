.class public final Lcom/facebook/widget/images/ImageViewTouchBase$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:F

.field public final synthetic b:J

.field public final synthetic c:F

.field public final synthetic d:F

.field public final synthetic e:F

.field public final synthetic f:F

.field public final synthetic g:Lcom/facebook/widget/images/ImageViewTouchBase;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/images/ImageViewTouchBase;FJFFFF)V
    .locals 1

    .prologue
    .line 1211364
    iput-object p1, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->g:Lcom/facebook/widget/images/ImageViewTouchBase;

    iput p2, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->a:F

    iput-wide p3, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->b:J

    iput p5, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->c:F

    iput p6, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->d:F

    iput p7, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->e:F

    iput p8, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->f:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1211365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1211366
    iget v2, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->a:F

    iget-wide v4, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->b:J

    sub-long/2addr v0, v4

    long-to-float v0, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1211367
    iget v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->c:F

    iget v2, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->d:F

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    .line 1211368
    iget-object v2, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->g:Lcom/facebook/widget/images/ImageViewTouchBase;

    iget v3, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->e:F

    iget v4, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->f:F

    invoke-virtual {v2, v1, v3, v4}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(FFF)V

    .line 1211369
    iget v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->a:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1211370
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase$3;->g:Lcom/facebook/widget/images/ImageViewTouchBase;

    iget-object v0, v0, Lcom/facebook/widget/images/ImageViewTouchBase;->h:Landroid/os/Handler;

    const v1, -0x19db5e6a

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1211371
    :cond_0
    return-void
.end method
