.class public Lcom/facebook/widget/images/ImageViewTouchBase;
.super Landroid/widget/ImageView;
.source ""


# static fields
.field private static m:F


# instance fields
.field public a:Landroid/graphics/Matrix;

.field public b:Landroid/graphics/Matrix;

.field public final c:LX/7U9;

.field public d:I

.field public e:I

.field public f:Z

.field public g:F

.field public h:Landroid/os/Handler;

.field private final i:Landroid/graphics/Matrix;

.field private final j:[F

.field private k:LX/7U5;

.field private l:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1211572
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/facebook/widget/images/ImageViewTouchBase;->m:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 1211559
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1211560
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    .line 1211561
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->b:Landroid/graphics/Matrix;

    .line 1211562
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    .line 1211563
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->j:[F

    .line 1211564
    new-instance v0, LX/7U9;

    invoke-direct {v0, v2}, LX/7U9;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211565
    iput v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->d:I

    iput v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->e:I

    .line 1211566
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->f:Z

    .line 1211567
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->g:F

    .line 1211568
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->h:Landroid/os/Handler;

    .line 1211569
    iput-object v2, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->l:Ljava/lang/Runnable;

    .line 1211570
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1211571
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 1211546
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1211547
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    .line 1211548
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->b:Landroid/graphics/Matrix;

    .line 1211549
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    .line 1211550
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->j:[F

    .line 1211551
    new-instance v0, LX/7U9;

    invoke-direct {v0, v2}, LX/7U9;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211552
    iput v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->d:I

    iput v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->e:I

    .line 1211553
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->f:Z

    .line 1211554
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->g:F

    .line 1211555
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->h:Landroid/os/Handler;

    .line 1211556
    iput-object v2, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->l:Ljava/lang/Runnable;

    .line 1211557
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1211558
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 1211533
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1211534
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    .line 1211535
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->b:Landroid/graphics/Matrix;

    .line 1211536
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    .line 1211537
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->j:[F

    .line 1211538
    new-instance v0, LX/7U9;

    invoke-direct {v0, v2}, LX/7U9;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211539
    iput v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->d:I

    iput v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->e:I

    .line 1211540
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->f:Z

    .line 1211541
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->g:F

    .line 1211542
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->h:Landroid/os/Handler;

    .line 1211543
    iput-object v2, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->l:Ljava/lang/Runnable;

    .line 1211544
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1211545
    return-void
.end method

.method private a(F)F
    .locals 1

    .prologue
    .line 1211528
    iget v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->g:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 1211529
    iget p1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->g:F

    .line 1211530
    :cond_0
    :goto_0
    return p1

    .line 1211531
    :cond_1
    sget v0, Lcom/facebook/widget/images/ImageViewTouchBase;->m:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 1211532
    sget p1, Lcom/facebook/widget/images/ImageViewTouchBase;->m:F

    goto :goto_0
.end method

.method private a(Landroid/graphics/Matrix;)F
    .locals 1

    .prologue
    .line 1211527
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(Landroid/graphics/Matrix;I)F

    move-result v0

    return v0
.end method

.method private a(Landroid/graphics/Matrix;I)F
    .locals 1

    .prologue
    .line 1211525
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->j:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 1211526
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->j:[F

    aget v0, v0, p2

    return v0
.end method

.method private a(LX/7U9;Landroid/graphics/Matrix;Z)V
    .locals 8

    .prologue
    const/high16 v7, 0x41200000    # 10.0f

    const/high16 v6, 0x40000000    # 2.0f

    .line 1211510
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 1211511
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 1211512
    invoke-virtual {p1}, LX/7U9;->e()I

    move-result v2

    int-to-float v2, v2

    .line 1211513
    invoke-virtual {p1}, LX/7U9;->d()I

    move-result v3

    int-to-float v3, v3

    .line 1211514
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 1211515
    if-eqz p3, :cond_0

    .line 1211516
    invoke-virtual {p1}, LX/7U9;->c()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {p2, v4}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1211517
    :cond_0
    iget-boolean v4, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->f:Z

    if-eqz v4, :cond_1

    .line 1211518
    div-float v4, v0, v2

    invoke-static {v4, v7}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 1211519
    div-float v5, v1, v3

    invoke-static {v5, v7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 1211520
    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 1211521
    invoke-virtual {p2, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1211522
    mul-float/2addr v2, v4

    sub-float/2addr v0, v2

    div-float/2addr v0, v6

    mul-float v2, v3, v4

    sub-float/2addr v1, v2

    div-float/2addr v1, v6

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1211523
    :goto_0
    return-void

    .line 1211524
    :cond_1
    sub-float/2addr v0, v2

    div-float/2addr v0, v6

    sub-float/2addr v1, v3

    div-float/2addr v1, v6

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;I)V
    .locals 2

    .prologue
    .line 1211501
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1211502
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1211503
    if-eqz v0, :cond_0

    .line 1211504
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    .line 1211505
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211506
    iput-object p1, v0, LX/7U9;->b:Landroid/graphics/Bitmap;

    .line 1211507
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211508
    iput p2, v0, LX/7U9;->c:I

    .line 1211509
    return-void
.end method

.method private b()F
    .locals 3

    .prologue
    .line 1211492
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211493
    iget-object v1, v0, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1211494
    if-nez v0, :cond_0

    .line 1211495
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1211496
    :goto_0
    return v0

    .line 1211497
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    invoke-virtual {v0}, LX/7U9;->e()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->d:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1211498
    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    invoke-virtual {v1}, LX/7U9;->d()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->e:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 1211499
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x40800000    # 4.0f

    mul-float/2addr v0, v1

    .line 1211500
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1211490
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(Landroid/graphics/Bitmap;Z)V

    .line 1211491
    return-void
.end method

.method public a(FF)V
    .locals 1

    .prologue
    .line 1211488
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1211489
    return-void
.end method

.method public a(FFF)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1211480
    invoke-direct {p0, p1}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(F)F

    move-result v0

    .line 1211481
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getScale()F

    move-result v1

    .line 1211482
    div-float/2addr v0, v1

    .line 1211483
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Old scale "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", delta "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1211484
    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1211485
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1211486
    invoke-virtual {p0, v4, v4}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(ZZ)V

    .line 1211487
    return-void
.end method

.method public final a(FFFF)V
    .locals 10

    .prologue
    .line 1211475
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getScale()F

    move-result v0

    sub-float v0, p1, v0

    div-float v7, v0, p4

    .line 1211476
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getScale()F

    move-result v6

    .line 1211477
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1211478
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->h:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/widget/images/ImageViewTouchBase$3;

    move-object v2, p0

    move v3, p4

    move v8, p2

    move v9, p3

    invoke-direct/range {v1 .. v9}, Lcom/facebook/widget/images/ImageViewTouchBase$3;-><init>(Lcom/facebook/widget/images/ImageViewTouchBase;FJFFFF)V

    const v2, -0x316e375e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1211479
    return-void
.end method

.method public final a(LX/7U9;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1211573
    iput-boolean v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->f:Z

    .line 1211574
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getWidth()I

    move-result v0

    .line 1211575
    if-gtz v0, :cond_0

    .line 1211576
    new-instance v0, Lcom/facebook/widget/images/ImageViewTouchBase$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/widget/images/ImageViewTouchBase$2;-><init>(Lcom/facebook/widget/images/ImageViewTouchBase;LX/7U9;Z)V

    iput-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->l:Ljava/lang/Runnable;

    .line 1211577
    :goto_0
    return-void

    .line 1211578
    :cond_0
    iget-object v0, p1, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v0, v0

    .line 1211579
    if-eqz v0, :cond_2

    .line 1211580
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(LX/7U9;Landroid/graphics/Matrix;Z)V

    .line 1211581
    iget-object v0, p1, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v0, v0

    .line 1211582
    iget v1, p1, LX/7U9;->c:I

    move v1, v1

    .line 1211583
    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(Landroid/graphics/Bitmap;I)V

    .line 1211584
    :goto_1
    if-eqz p2, :cond_1

    .line 1211585
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1211586
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1211587
    invoke-direct {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->b()F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->g:F

    goto :goto_0

    .line 1211588
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 1211589
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public final a(Landroid/graphics/Bitmap;Z)V
    .locals 1

    .prologue
    .line 1211372
    new-instance v0, LX/7U9;

    invoke-direct {v0, p1}, LX/7U9;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0, p2}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(LX/7U9;Z)V

    .line 1211373
    return-void
.end method

.method public final a(ZZ)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 1211374
    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211375
    iget-object v2, v1, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v1, v2

    .line 1211376
    if-nez v1, :cond_0

    .line 1211377
    :goto_0
    return-void

    .line 1211378
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 1211379
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211380
    iget-object v4, v3, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v3, v4

    .line 1211381
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211382
    iget-object v5, v4, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v4, v5

    .line 1211383
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v0, v0, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1211384
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1211385
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v1

    .line 1211386
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 1211387
    if-eqz p2, :cond_6

    .line 1211388
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getHeight()I

    move-result v4

    .line 1211389
    int-to-float v5, v4

    cmpg-float v5, v1, v5

    if-gez v5, :cond_2

    .line 1211390
    int-to-float v4, v4

    sub-float v1, v4, v1

    div-float/2addr v1, v6

    iget v4, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v4

    .line 1211391
    :goto_1
    if-eqz p1, :cond_1

    .line 1211392
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getWidth()I

    move-result v4

    .line 1211393
    int-to-float v5, v4

    cmpg-float v5, v3, v5

    if-gez v5, :cond_4

    .line 1211394
    int-to-float v0, v4

    sub-float/2addr v0, v3

    div-float/2addr v0, v6

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v2

    .line 1211395
    :cond_1
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "center() delta: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 1211396
    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(FF)V

    .line 1211397
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 1211398
    :cond_2
    iget v1, v2, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_3

    .line 1211399
    iget v1, v2, Landroid/graphics/RectF;->top:F

    neg-float v1, v1

    goto :goto_1

    .line 1211400
    :cond_3
    iget v1, v2, Landroid/graphics/RectF;->bottom:F

    int-to-float v4, v4

    cmpg-float v1, v1, v4

    if-gez v1, :cond_6

    .line 1211401
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v4

    goto :goto_1

    .line 1211402
    :cond_4
    iget v3, v2, Landroid/graphics/RectF;->left:F

    cmpl-float v3, v3, v0

    if-lez v3, :cond_5

    .line 1211403
    iget v0, v2, Landroid/graphics/RectF;->left:F

    neg-float v0, v0

    goto :goto_2

    .line 1211404
    :cond_5
    iget v3, v2, Landroid/graphics/RectF;->right:F

    int-to-float v5, v4

    cmpg-float v3, v3, v5

    if-gez v3, :cond_1

    .line 1211405
    int-to-float v0, v4

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v2

    goto :goto_2

    :cond_6
    move v1, v0

    goto :goto_1
.end method

.method public final b(FF)V
    .locals 1

    .prologue
    .line 1211406
    invoke-virtual {p0, p1, p2}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(FF)V

    .line 1211407
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1211408
    return-void
.end method

.method public getImageLeft()F
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1211409
    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211410
    iget-object v2, v1, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v1, v2

    .line 1211411
    if-nez v1, :cond_0

    .line 1211412
    :goto_0
    return v0

    .line 1211413
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 1211414
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211415
    iget-object v4, v3, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v3, v4

    .line 1211416
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211417
    iget-object p0, v4, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v4, p0

    .line 1211418
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v0, v0, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1211419
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1211420
    iget v0, v2, Landroid/graphics/RectF;->left:F

    goto :goto_0
.end method

.method public getImageRect()Landroid/graphics/RectF;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1211421
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211422
    iget-object v1, v0, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1211423
    if-nez v0, :cond_0

    .line 1211424
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v4, v4, v4, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1211425
    :goto_0
    return-object v0

    .line 1211426
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 1211427
    new-instance v0, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211428
    iget-object v3, v2, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v2, v3

    .line 1211429
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211430
    iget-object p0, v3, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v3, p0

    .line 1211431
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v0, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1211432
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    goto :goto_0
.end method

.method public getImageRight()F
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1211433
    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211434
    iget-object v2, v1, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v1, v2

    .line 1211435
    if-nez v1, :cond_0

    .line 1211436
    :goto_0
    return v0

    .line 1211437
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 1211438
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211439
    iget-object v4, v3, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v3, v4

    .line 1211440
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211441
    iget-object p0, v4, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v4, p0

    .line 1211442
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v0, v0, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1211443
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 1211444
    iget v0, v2, Landroid/graphics/RectF;->right:F

    goto :goto_0
.end method

.method public getImageViewMatrix()Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 1211445
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1211446
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1211447
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->i:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getMaxZoom()F
    .locals 1

    .prologue
    .line 1211448
    iget v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->g:F

    return v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 1211449
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->b:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method public getUnrotatedMatrix()Landroid/graphics/Matrix;
    .locals 3

    .prologue
    .line 1211450
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 1211451
    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(LX/7U9;Landroid/graphics/Matrix;Z)V

    .line 1211452
    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1211453
    return-object v0
.end method

.method public onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 1211454
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 1211455
    sub-int v0, p4, p2

    iput v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->d:I

    .line 1211456
    sub-int v0, p5, p3

    iput v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->e:I

    .line 1211457
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->l:Ljava/lang/Runnable;

    .line 1211458
    if-eqz v0, :cond_1

    .line 1211459
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->l:Ljava/lang/Runnable;

    .line 1211460
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1211461
    :cond_0
    :goto_0
    return-void

    .line 1211462
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    .line 1211463
    iget-object v1, v0, LX/7U9;->b:Landroid/graphics/Bitmap;

    move-object v0, v1

    .line 1211464
    if-eqz v0, :cond_0

    .line 1211465
    iget-object v0, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->c:LX/7U9;

    iget-object v1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->a:Landroid/graphics/Matrix;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(LX/7U9;Landroid/graphics/Matrix;Z)V

    .line 1211466
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 1211467
    invoke-virtual {p0}, Lcom/facebook/widget/images/ImageViewTouchBase;->getScaleType()Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    if-ne v0, v1, :cond_0

    instance-of v0, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 1211468
    new-instance v0, LX/7U9;

    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/7U9;-><init>(Landroid/graphics/Bitmap;I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(LX/7U9;Z)V

    .line 1211469
    :goto_0
    return-void

    .line 1211470
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/widget/images/ImageViewTouchBase;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1211471
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/images/ImageViewTouchBase;->a(Landroid/graphics/Bitmap;I)V

    .line 1211472
    return-void
.end method

.method public setRecycler(LX/7U5;)V
    .locals 0

    .prologue
    .line 1211473
    iput-object p1, p0, Lcom/facebook/widget/images/ImageViewTouchBase;->k:LX/7U5;

    .line 1211474
    return-void
.end method
