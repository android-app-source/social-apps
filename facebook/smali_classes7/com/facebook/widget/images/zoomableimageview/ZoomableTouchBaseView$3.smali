.class public final Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:F

.field public final synthetic b:J

.field public final synthetic c:F

.field public final synthetic d:F

.field public final synthetic e:F

.field public final synthetic f:F

.field public final synthetic g:LX/7UQ;


# direct methods
.method public constructor <init>(LX/7UQ;FJFFFF)V
    .locals 1

    .prologue
    .line 1213173
    iput-object p1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->g:LX/7UQ;

    iput p2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->a:F

    iput-wide p3, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->b:J

    iput p5, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->c:F

    iput p6, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->d:F

    iput p7, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->e:F

    iput p8, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->f:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 1213174
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1213175
    iget v2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->a:F

    iget-wide v4, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->b:J

    sub-long/2addr v0, v4

    long-to-float v0, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1213176
    iget-object v1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->g:LX/7UQ;

    iget-object v1, v1, LX/7UQ;->m:LX/7Uc;

    float-to-double v2, v0

    const-wide/16 v4, 0x0

    iget v6, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->c:F

    float-to-double v6, v6

    iget v8, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->a:F

    float-to-double v8, v8

    invoke-virtual/range {v1 .. v9}, LX/7Uc;->b(DDDD)D

    move-result-wide v2

    double-to-float v1, v2

    .line 1213177
    iget-object v2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->g:LX/7UQ;

    iget v3, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->d:F

    add-float/2addr v1, v3

    iget v3, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->e:F

    iget v4, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->f:F

    invoke-virtual {v2, v1, v3, v4}, LX/7UQ;->a(FFF)V

    .line 1213178
    iget v1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->a:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 1213179
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->g:LX/7UQ;

    iget-object v0, v0, LX/7UQ;->t:Landroid/os/Handler;

    const v1, 0x4b4ffed8    # 1.3631192E7f

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1213180
    :goto_0
    return-void

    .line 1213181
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->g:LX/7UQ;

    iget-object v1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->g:LX/7UQ;

    invoke-virtual {v1}, LX/7UQ;->getScale()F

    move-result v1

    invoke-virtual {v0, v1}, LX/7UQ;->b(F)V

    .line 1213182
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$3;->g:LX/7UQ;

    invoke-virtual {v0, v10, v10}, LX/7UQ;->a(ZZ)V

    goto :goto_0
.end method
