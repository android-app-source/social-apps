.class public final Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:D

.field public b:D

.field public final synthetic c:D

.field public final synthetic d:J

.field public final synthetic e:D

.field public final synthetic f:D

.field public final synthetic g:LX/7UQ;


# direct methods
.method public constructor <init>(LX/7UQ;DJDD)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 1213170
    iput-object p1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->g:LX/7UQ;

    iput-wide p2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->c:D

    iput-wide p4, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->d:J

    iput-wide p6, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->e:D

    iput-wide p8, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->f:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1213171
    iput-wide v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->a:D

    .line 1213172
    iput-wide v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->b:D

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const-wide/16 v4, 0x0

    .line 1213157
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1213158
    iget-wide v2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->c:D

    iget-wide v6, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->d:J

    sub-long/2addr v0, v6

    long-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    .line 1213159
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->g:LX/7UQ;

    iget-object v1, v0, LX/7UQ;->m:LX/7Uc;

    iget-wide v6, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->e:D

    iget-wide v8, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->c:D

    invoke-virtual/range {v1 .. v9}, LX/7Uc;->a(DDDD)D

    move-result-wide v10

    .line 1213160
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->g:LX/7UQ;

    iget-object v1, v0, LX/7UQ;->m:LX/7Uc;

    iget-wide v6, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->f:D

    iget-wide v8, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->c:D

    invoke-virtual/range {v1 .. v9}, LX/7Uc;->a(DDDD)D

    move-result-wide v0

    .line 1213161
    iget-object v4, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->g:LX/7UQ;

    iget-wide v6, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->a:D

    sub-double v6, v10, v6

    iget-wide v8, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->b:D

    sub-double v8, v0, v8

    invoke-virtual {v4, v6, v7, v8, v9}, LX/7UQ;->a(DD)V

    .line 1213162
    iput-wide v10, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->a:D

    .line 1213163
    iput-wide v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->b:D

    .line 1213164
    iget-wide v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->c:D

    cmpg-double v0, v2, v0

    if-gez v0, :cond_1

    .line 1213165
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->g:LX/7UQ;

    iget-object v0, v0, LX/7UQ;->t:Landroid/os/Handler;

    const v1, 0x6f55ffdf

    invoke-static {v0, p0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1213166
    :cond_0
    :goto_0
    return-void

    .line 1213167
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->g:LX/7UQ;

    iget-object v1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->g:LX/7UQ;

    iget-object v1, v1, LX/7UQ;->o:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v13, v13}, LX/7UQ;->a(Landroid/graphics/Matrix;ZZ)Landroid/graphics/RectF;

    move-result-object v0

    .line 1213168
    iget v1, v0, Landroid/graphics/RectF;->left:F

    cmpl-float v1, v1, v12

    if-nez v1, :cond_2

    iget v1, v0, Landroid/graphics/RectF;->top:F

    cmpl-float v1, v1, v12

    if-eqz v1, :cond_0

    .line 1213169
    :cond_2
    iget-object v1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableTouchBaseView$2;->g:LX/7UQ;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2, v0}, LX/7UQ;->c(FF)V

    goto :goto_0
.end method
