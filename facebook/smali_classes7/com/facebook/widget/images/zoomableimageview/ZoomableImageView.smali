.class public Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;
.super LX/7UR;
.source ""

# interfaces
.implements LX/0hY;
.implements LX/7UZ;


# instance fields
.field private K:Z

.field public L:Z

.field public M:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "LX/7US;",
            ">;"
        }
    .end annotation
.end field

.field public N:LX/7Ub;

.field public O:F

.field public P:F

.field public Q:LX/7UX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1213084
    invoke-direct {p0, p1, p2}, LX/7UR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1213085
    iput-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->K:Z

    .line 1213086
    iput-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->L:Z

    .line 1213087
    iput-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->R:Z

    .line 1213088
    iget-boolean p1, p0, LX/7UQ;->B:Z

    if-eq v0, p1, :cond_0

    .line 1213089
    iput-boolean v0, p0, LX/7UQ;->B:Z

    .line 1213090
    invoke-virtual {p0}, LX/7UQ;->requestLayout()V

    .line 1213091
    :cond_0
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 1213092
    return-void
.end method

.method private a(Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 5

    .prologue
    .line 1213093
    invoke-virtual {p0}, LX/7UQ;->getBitmapRect()Landroid/graphics/RectF;

    move-result-object v1

    .line 1213094
    if-eqz v1, :cond_0

    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1213095
    new-instance v0, Landroid/graphics/PointF;

    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v2, v3

    iget v3, p1, Landroid/graphics/PointF;->y:F

    iget v4, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float v1, v3, v1

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1213096
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 1213097
    invoke-direct {p0, p1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public static h(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;)V
    .locals 1

    .prologue
    .line 1213098
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->L:Z

    .line 1213099
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 1213100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->L:Z

    .line 1213101
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1213102
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1213103
    invoke-interface {v0}, LX/7US;->b()V

    goto :goto_0

    .line 1213104
    :cond_0
    const/4 v0, 0x0

    .line 1213105
    iput-boolean v0, p0, LX/7UR;->k:Z

    .line 1213106
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1213077
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1213078
    invoke-interface {v0}, LX/7US;->c()V

    goto :goto_0

    .line 1213079
    :cond_0
    const/4 v0, 0x1

    .line 1213080
    iput-boolean v0, p0, LX/7UR;->k:Z

    .line 1213081
    return-void
.end method


# virtual methods
.method public final a(FF)F
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1213107
    iget v0, p0, LX/7UR;->f:I

    if-ne v0, v1, :cond_0

    .line 1213108
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->f:I

    .line 1213109
    :goto_0
    return p2

    .line 1213110
    :cond_0
    iput v1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->f:I

    .line 1213111
    const/high16 p2, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final a(DD)V
    .locals 1

    .prologue
    .line 1213112
    invoke-super {p0, p1, p2, p3, p4}, LX/7UR;->a(DD)V

    .line 1213113
    return-void
.end method

.method public final a(FFFF)V
    .locals 0

    .prologue
    .line 1213074
    invoke-direct {p0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->j()V

    .line 1213075
    invoke-super {p0, p1, p2, p3, p4}, LX/7UR;->a(FFFF)V

    .line 1213076
    return-void
.end method

.method public final a(FFFFFJ)V
    .locals 12

    .prologue
    .line 1213114
    invoke-direct {p0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->j()V

    .line 1213115
    invoke-virtual {p0}, LX/7UQ;->getMaxZoom()F

    move-result v2

    cmpl-float v2, p1, v2

    if-lez v2, :cond_0

    .line 1213116
    invoke-virtual {p0}, LX/7UQ;->getMaxZoom()F

    move-result p1

    .line 1213117
    :cond_0
    move-wide/from16 v0, p6

    long-to-float v2, v0

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 1213118
    invoke-virtual {p0}, LX/7UQ;->getScale()F

    move-result v7

    .line 1213119
    sub-float v4, p1, v7

    .line 1213120
    const/4 v2, 0x0

    iput v2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->O:F

    .line 1213121
    const/4 v2, 0x0

    iput v2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->P:F

    .line 1213122
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v10

    .line 1213123
    move-wide/from16 v0, p6

    invoke-virtual {v10, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1213124
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v10, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1213125
    new-instance v2, LX/7UU;

    move-object v3, p0

    move/from16 v5, p4

    move/from16 v6, p5

    move v8, p2

    move v9, p3

    invoke-direct/range {v2 .. v9}, LX/7UU;-><init>(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;FFFFFF)V

    invoke-virtual {v10, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1213126
    new-instance v2, LX/7UV;

    invoke-direct {v2, p0}, LX/7UV;-><init>(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;)V

    invoke-virtual {v10, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1213127
    invoke-direct {p0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->i()V

    .line 1213128
    invoke-virtual {v10}, Landroid/animation/ValueAnimator;->start()V

    .line 1213129
    :cond_1
    :goto_0
    return-void

    .line 1213130
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, LX/7UQ;->a(FFF)V

    .line 1213131
    move/from16 v0, p4

    float-to-double v2, v0

    move/from16 v0, p5

    float-to-double v4, v0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->a(DD)V

    .line 1213132
    invoke-virtual {p0}, LX/7UQ;->getScale()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->b(F)V

    .line 1213133
    iget-object v2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->N:LX/7Ub;

    if-eqz v2, :cond_1

    .line 1213134
    iget-object v2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->N:LX/7Ub;

    invoke-interface {v2}, LX/7Ub;->a()V

    .line 1213135
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->N:LX/7Ub;

    goto :goto_0

    .line 1213136
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final a(LX/7US;)V
    .locals 1

    .prologue
    .line 1213137
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 1213138
    return-void
.end method

.method public final a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 1213139
    if-eqz p1, :cond_0

    .line 1213140
    iget-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->L:Z

    move v0, v0

    .line 1213141
    if-eqz v0, :cond_0

    .line 1213142
    invoke-super {p0, p1, p2}, LX/7UR;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 1213143
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 1213144
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1213145
    invoke-virtual {p0, p1}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1213146
    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 1213147
    iget-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->L:Z

    move v0, v0

    .line 1213148
    if-eqz v0, :cond_0

    .line 1213149
    invoke-super {p0, p1, p2}, LX/7UR;->a(ZZ)V

    .line 1213150
    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1213151
    invoke-virtual {p0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1213152
    invoke-super {p0, p1}, LX/7UR;->a(I)Z

    move-result v0

    .line 1213153
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/31M;II)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1213082
    sget-object v1, LX/31M;->UP:LX/31M;

    if-eq p1, v1, :cond_0

    sget-object v1, LX/31M;->DOWN:LX/31M;

    if-ne p1, v1, :cond_1

    .line 1213083
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, LX/31M;->LEFT:LX/31M;

    if-ne p1, v1, :cond_2

    const/4 v0, -0x1

    :cond_2
    invoke-virtual {p0, v0}, LX/7UR;->a(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(F)V
    .locals 1

    .prologue
    .line 1213050
    iget-object v0, p0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1213051
    iput p1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->d:F

    .line 1213052
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->k()V

    .line 1213053
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 1213036
    invoke-virtual {p0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1213037
    invoke-virtual {p0}, LX/7UQ;->getPhotoWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, LX/7UQ;->getPhotoHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 1213038
    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1213039
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->K:Z

    .line 1213040
    return-void
.end method

.method public final c(F)V
    .locals 1

    .prologue
    .line 1213041
    invoke-super {p0, p1}, LX/7UR;->c(F)V

    .line 1213042
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->f:I

    .line 1213043
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1213044
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->K:Z

    .line 1213045
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1213046
    invoke-virtual {p0}, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1213047
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->Q:LX/7UX;

    if-eqz v0, :cond_0

    .line 1213048
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->Q:LX/7UX;

    invoke-interface {v0}, LX/7UX;->a()V

    .line 1213049
    :cond_0
    return-void
.end method

.method public getGestureListener()Landroid/view/GestureDetector$OnGestureListener;
    .locals 1

    .prologue
    .line 1213054
    new-instance v0, LX/7UW;

    invoke-direct {v0, p0}, LX/7UW;-><init>(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;)V

    return-object v0
.end method

.method public getScaleListener()Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
    .locals 1

    .prologue
    .line 1213055
    new-instance v0, LX/7UY;

    invoke-direct {v0, p0}, LX/7UY;-><init>(Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;)V

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1213056
    if-eqz p1, :cond_0

    .line 1213057
    invoke-super/range {p0 .. p5}, LX/7UR;->onLayout(ZIIII)V

    .line 1213058
    :cond_0
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, -0x2622996d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1213059
    iget-boolean v2, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->K:Z

    if-eqz v2, :cond_1

    .line 1213060
    iget-object v2, p0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1213061
    iget-object v2, p0, LX/7UR;->a:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2}, Landroid/view/ScaleGestureDetector;->isInProgress()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/7UR;->b:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1213062
    :cond_0
    const v2, 0x3025a820

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1213063
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    const v2, 0x2d49f09f

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 3

    .prologue
    .line 1213064
    invoke-super {p0, p1}, LX/7UR;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1213065
    iget-object v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->M:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1213066
    iget-object v2, p0, LX/7UQ;->s:Landroid/graphics/Matrix;

    invoke-interface {v0, v2}, LX/7US;->a(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 1213067
    :cond_0
    return-void
.end method

.method public setImageModeListener(LX/7UX;)V
    .locals 0

    .prologue
    .line 1213034
    iput-object p1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->Q:LX/7UX;

    .line 1213035
    return-void
.end method

.method public setMinZoom(F)V
    .locals 1

    .prologue
    .line 1213068
    iget-boolean v0, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->R:Z

    if-eqz v0, :cond_0

    const/high16 p1, 0x3f800000    # 1.0f

    :cond_0
    iput p1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->w:F

    .line 1213069
    return-void
.end method

.method public setRestrictMinZoomToOne(Z)V
    .locals 0

    .prologue
    .line 1213070
    iput-boolean p1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->R:Z

    .line 1213071
    return-void
.end method

.method public setZoomAndPanListener(LX/7Ub;)V
    .locals 0

    .prologue
    .line 1213072
    iput-object p1, p0, Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;->N:LX/7Ub;

    .line 1213073
    return-void
.end method
