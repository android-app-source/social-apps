.class public final Lcom/google/android/gms/cast/internal/zze$zzb$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7Z3;

.field public final synthetic b:Lcom/google/android/gms/cast/internal/ApplicationStatus;

.field public final synthetic c:LX/7Z2;


# direct methods
.method public constructor <init>(LX/7Z2;LX/7Z3;Lcom/google/android/gms/cast/internal/ApplicationStatus;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/cast/internal/zze$zzb$3;->c:LX/7Z2;

    iput-object p2, p0, Lcom/google/android/gms/cast/internal/zze$zzb$3;->a:LX/7Z3;

    iput-object p3, p0, Lcom/google/android/gms/cast/internal/zze$zzb$3;->b:Lcom/google/android/gms/cast/internal/ApplicationStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/gms/cast/internal/zze$zzb$3;->a:LX/7Z3;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/zze$zzb$3;->b:Lcom/google/android/gms/cast/internal/ApplicationStatus;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, v1, Lcom/google/android/gms/cast/internal/ApplicationStatus;->b:Ljava/lang/String;

    move-object v2, v2

    iget-object v5, v0, LX/7Z3;->k:Ljava/lang/String;

    invoke-static {v2, v5}, LX/7Z4;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iput-object v2, v0, LX/7Z3;->k:Ljava/lang/String;

    move v2, v3

    :goto_0
    sget-object v5, LX/7Z3;->d:LX/7Z9;

    const-string v6, "hasChanged=%b, mFirstApplicationStatusUpdate=%b"

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, p0, v4

    iget-boolean v2, v0, LX/7Z3;->m:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, p0, v3

    invoke-virtual {v5, v6, p0}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v4, v0, LX/7Z3;->m:Z

    return-void

    :cond_0
    move v2, v4

    goto :goto_0
.end method
