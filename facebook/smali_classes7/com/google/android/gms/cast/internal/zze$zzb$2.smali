.class public final Lcom/google/android/gms/cast/internal/zze$zzb$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/7Z3;

.field public final synthetic b:Lcom/google/android/gms/cast/internal/DeviceStatus;

.field public final synthetic c:LX/7Z2;


# direct methods
.method public constructor <init>(LX/7Z2;LX/7Z3;Lcom/google/android/gms/cast/internal/DeviceStatus;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/cast/internal/zze$zzb$2;->c:LX/7Z2;

    iput-object p2, p0, Lcom/google/android/gms/cast/internal/zze$zzb$2;->a:LX/7Z3;

    iput-object p3, p0, Lcom/google/android/gms/cast/internal/zze$zzb$2;->b:Lcom/google/android/gms/cast/internal/DeviceStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    iget-object v0, p0, Lcom/google/android/gms/cast/internal/zze$zzb$2;->a:LX/7Z3;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/zze$zzb$2;->b:Lcom/google/android/gms/cast/internal/DeviceStatus;

    const/4 v12, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v2, v1, Lcom/google/android/gms/cast/internal/DeviceStatus;->e:Lcom/google/android/gms/cast/ApplicationMetadata;

    move-object v2, v2

    iget-object v5, v0, LX/7Z3;->e:Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-static {v2, v5}, LX/7Z4;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iput-object v2, v0, LX/7Z3;->e:Lcom/google/android/gms/cast/ApplicationMetadata;

    :cond_0
    iget-wide v13, v1, Lcom/google/android/gms/cast/internal/DeviceStatus;->b:D

    move-wide v6, v13

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_4

    iget-wide v8, v0, LX/7Z3;->p:D

    sub-double v8, v6, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3e7ad7f29abcaf48L    # 1.0E-7

    cmpl-double v2, v8, v10

    if-lez v2, :cond_4

    iput-wide v6, v0, LX/7Z3;->p:D

    move v2, v3

    :goto_0
    iget-boolean v5, v1, Lcom/google/android/gms/cast/internal/DeviceStatus;->c:Z

    move v5, v5

    iget-boolean v6, v0, LX/7Z3;->l:Z

    if-eq v5, v6, :cond_1

    iput-boolean v5, v0, LX/7Z3;->l:Z

    move v2, v3

    :cond_1
    sget-object v5, LX/7Z3;->d:LX/7Z9;

    const-string v6, "hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v4

    iget-boolean v2, v0, LX/7Z3;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v3

    invoke-virtual {v5, v6, v7}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget v2, v1, Lcom/google/android/gms/cast/internal/DeviceStatus;->d:I

    move v2, v2

    iget v5, v0, LX/7Z3;->q:I

    if-eq v2, v5, :cond_3

    iput v2, v0, LX/7Z3;->q:I

    move v2, v3

    :goto_1
    sget-object v5, LX/7Z3;->d:LX/7Z9;

    const-string v6, "hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v4

    iget-boolean v2, v0, LX/7Z3;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v3

    invoke-virtual {v5, v6, v7}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget v2, v1, Lcom/google/android/gms/cast/internal/DeviceStatus;->f:I

    move v2, v2

    iget v5, v0, LX/7Z3;->r:I

    if-eq v2, v5, :cond_2

    iput v2, v0, LX/7Z3;->r:I

    move v2, v3

    :goto_2
    sget-object v5, LX/7Z3;->d:LX/7Z9;

    const-string v6, "hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v4

    iget-boolean v2, v0, LX/7Z3;->n:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v7, v3

    invoke-virtual {v5, v6, v7}, LX/7Z9;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v4, v0, LX/7Z3;->n:Z

    return-void

    :cond_2
    move v2, v4

    goto :goto_2

    :cond_3
    move v2, v4

    goto :goto_1

    :cond_4
    move v2, v4

    goto :goto_0
.end method
