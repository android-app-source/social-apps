.class public Lcom/google/android/gms/cast/MediaQueueItem;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/cast/MediaQueueItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public final b:I

.field public c:Lcom/google/android/gms/cast/MediaInfo;

.field public d:I

.field public e:Z

.field public f:D

.field public g:D

.field public h:D

.field public i:[J

.field private j:Lorg/json/JSONObject;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7ZG;

    invoke-direct {v0}, LX/7ZG;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/MediaQueueItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/cast/MediaInfo;IZDDD[JLjava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput p1, p0, Lcom/google/android/gms/cast/MediaQueueItem;->b:I

    iput-object p2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->c:Lcom/google/android/gms/cast/MediaInfo;

    iput p3, p0, Lcom/google/android/gms/cast/MediaQueueItem;->d:I

    iput-boolean p4, p0, Lcom/google/android/gms/cast/MediaQueueItem;->e:Z

    iput-wide p5, p0, Lcom/google/android/gms/cast/MediaQueueItem;->f:D

    iput-wide p7, p0, Lcom/google/android/gms/cast/MediaQueueItem;->g:D

    iput-wide p9, p0, Lcom/google/android/gms/cast/MediaQueueItem;->h:D

    iput-object p11, p0, Lcom/google/android/gms/cast/MediaQueueItem;->i:[J

    iput-object p12, p0, Lcom/google/android/gms/cast/MediaQueueItem;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/google/android/gms/cast/MediaQueueItem;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    iput-object v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    iput-object v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->a:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    move v2, v1

    :cond_0
    :goto_0
    return v2

    :cond_1
    instance-of v0, p1, Lcom/google/android/gms/cast/MediaQueueItem;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/gms/cast/MediaQueueItem;

    iget-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p1, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    if-nez v3, :cond_4

    move v3, v1

    :goto_2
    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    iget-object v3, p1, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    invoke-static {v0, v3}, LX/4tZ;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->c:Lcom/google/android/gms/cast/MediaInfo;

    iget-object v3, p1, Lcom/google/android/gms/cast/MediaQueueItem;->c:Lcom/google/android/gms/cast/MediaInfo;

    invoke-static {v0, v3}, LX/7Z4;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->d:I

    iget v3, p1, Lcom/google/android/gms/cast/MediaQueueItem;->d:I

    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->e:Z

    iget-boolean v3, p1, Lcom/google/android/gms/cast/MediaQueueItem;->e:Z

    if-ne v0, v3, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/cast/MediaQueueItem;->f:D

    iget-wide v6, p1, Lcom/google/android/gms/cast/MediaQueueItem;->f:D

    cmpl-double v0, v4, v6

    if-nez v0, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/cast/MediaQueueItem;->g:D

    iget-wide v6, p1, Lcom/google/android/gms/cast/MediaQueueItem;->g:D

    cmpl-double v0, v4, v6

    if-nez v0, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/cast/MediaQueueItem;->h:D

    iget-wide v6, p1, Lcom/google/android/gms/cast/MediaQueueItem;->h:D

    cmpl-double v0, v4, v6

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->i:[J

    iget-object v3, p1, Lcom/google/android/gms/cast/MediaQueueItem;->i:[J

    invoke-static {v0, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2
.end method

.method public final hashCode()I
    .locals 4

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->c:Lcom/google/android/gms/cast/MediaInfo;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->f:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->g:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->h:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->i:[J

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([J)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2wy;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 8

    iget-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->a:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v1

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/gms/cast/MediaQueueItem;->b:I

    move v3, v3

    invoke-static {p1, v2, v3}, LX/2xC;->a(Landroid/os/Parcel;II)V

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/cast/MediaQueueItem;->c:Lcom/google/android/gms/cast/MediaInfo;

    move-object v3, v3

    invoke-static {p1, v2, v3, p2, v5}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/gms/cast/MediaQueueItem;->d:I

    move v3, v3

    invoke-static {p1, v2, v3}, LX/2xC;->a(Landroid/os/Parcel;II)V

    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/gms/cast/MediaQueueItem;->e:Z

    move v3, v3

    invoke-static {p1, v2, v3}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v2, 0x5

    iget-wide v6, p0, Lcom/google/android/gms/cast/MediaQueueItem;->f:D

    move-wide v3, v6

    invoke-static {p1, v2, v3, v4}, LX/2xC;->a(Landroid/os/Parcel;ID)V

    const/4 v2, 0x6

    iget-wide v6, p0, Lcom/google/android/gms/cast/MediaQueueItem;->g:D

    move-wide v3, v6

    invoke-static {p1, v2, v3, v4}, LX/2xC;->a(Landroid/os/Parcel;ID)V

    const/4 v2, 0x7

    iget-wide v6, p0, Lcom/google/android/gms/cast/MediaQueueItem;->h:D

    move-wide v3, v6

    invoke-static {p1, v2, v3, v4}, LX/2xC;->a(Landroid/os/Parcel;ID)V

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/gms/cast/MediaQueueItem;->i:[J

    move-object v3, v3

    invoke-static {p1, v2, v3, v5}, LX/2xC;->a(Landroid/os/Parcel;I[JZ)V

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/gms/cast/MediaQueueItem;->a:Ljava/lang/String;

    invoke-static {p1, v2, v3, v5}, LX/2xC;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    invoke-static {p1, v1}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/MediaQueueItem;->j:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
