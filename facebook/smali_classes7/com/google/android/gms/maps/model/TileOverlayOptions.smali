.class public final Lcom/google/android/gms/maps/model/TileOverlayOptions;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""


# static fields
.field public static final CREATOR:LX/7cr;


# instance fields
.field public final a:I

.field public b:LX/7cb;

.field private c:LX/7cD;

.field public d:Z

.field public e:F

.field public f:Z

.field public g:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7cr;

    invoke-direct {v0}, LX/7cr;-><init>()V

    sput-object v0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->CREATOR:LX/7cr;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->d:Z

    iput-boolean v1, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->f:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->g:F

    iput v1, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->a:I

    return-void
.end method

.method public constructor <init>(ILandroid/os/IBinder;ZFZF)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->d:Z

    iput-boolean v0, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->f:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->g:F

    iput p1, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->a:I

    if-nez p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    iput-object v0, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->b:LX/7cb;

    iget-object v0, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->b:LX/7cb;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->c:LX/7cD;

    iput-boolean p3, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->d:Z

    iput p4, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->e:F

    iput-boolean p5, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->f:Z

    iput p6, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->g:F

    return-void

    :cond_0
    new-instance v0, LX/7cE;

    invoke-direct {v0, p0}, LX/7cE;-><init>(Lcom/google/android/gms/maps/model/TileOverlayOptions;)V

    goto :goto_1

    :cond_1
    const-string v0, "com.google.android.gms.maps.model.internal.ITileProviderDelegate"

    invoke-interface {p2, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of p1, v0, LX/7cb;

    if-eqz p1, :cond_2

    check-cast v0, LX/7cb;

    goto :goto_0

    :cond_2
    new-instance v0, LX/7cc;

    invoke-direct {v0, p2}, LX/7cc;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->a:I

    move v2, v2

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->b:LX/7cb;

    invoke-interface {v2}, LX/7cb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    move-object v2, v2

    const/4 p2, 0x0

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->d:Z

    move v2, v2

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->e:F

    move v2, v2

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;IF)V

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->f:Z

    move v2, v2

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/maps/model/TileOverlayOptions;->g:F

    move v2, v2

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;IF)V

    invoke-static {p1, v0}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void
.end method
