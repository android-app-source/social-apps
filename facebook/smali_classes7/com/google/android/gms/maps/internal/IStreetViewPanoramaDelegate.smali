.class public interface abstract Lcom/google/android/gms/maps/internal/IStreetViewPanoramaDelegate;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Lcom/google/android/gms/maps/model/StreetViewPanoramaOrientation;)LX/1ot;
.end method

.method public abstract a(LX/1ot;)Lcom/google/android/gms/maps/model/StreetViewPanoramaOrientation;
.end method

.method public abstract a(LX/7bD;)V
.end method

.method public abstract a(LX/7bF;)V
.end method

.method public abstract a(LX/7bH;)V
.end method

.method public abstract a(LX/7bJ;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;I)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;J)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(Z)V
.end method

.method public abstract b()Z
.end method

.method public abstract c(Z)V
.end method

.method public abstract c()Z
.end method

.method public abstract d(Z)V
.end method

.method public abstract d()Z
.end method

.method public abstract e()Lcom/google/android/gms/maps/model/StreetViewPanoramaCamera;
.end method

.method public abstract f()Lcom/google/android/gms/maps/model/StreetViewPanoramaLocation;
.end method
