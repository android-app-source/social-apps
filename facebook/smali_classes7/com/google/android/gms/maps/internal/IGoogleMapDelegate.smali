.class public interface abstract Lcom/google/android/gms/maps/internal/IGoogleMapDelegate;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Lcom/google/android/gms/maps/model/CircleOptions;)LX/7cJ;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)LX/7cM;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/MarkerOptions;)LX/7cS;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolygonOptions;)LX/7cV;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)LX/7cY;
.end method

.method public abstract a()Lcom/google/android/gms/maps/model/CameraPosition;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/internal/IPolylineDelegate;
.end method

.method public abstract a(F)V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(IIII)V
.end method

.method public abstract a(LX/1ot;)V
.end method

.method public abstract a(LX/1ot;ILX/7ak;)V
.end method

.method public abstract a(LX/1ot;LX/7ak;)V
.end method

.method public abstract a(LX/7aS;)V
.end method

.method public abstract a(LX/7aV;)V
.end method

.method public abstract a(LX/7aY;)V
.end method

.method public abstract a(LX/7ab;)V
.end method

.method public abstract a(LX/7ae;)V
.end method

.method public abstract a(LX/7ah;)V
.end method

.method public abstract a(LX/7ao;)V
.end method

.method public abstract a(LX/7bB;)V
.end method

.method public abstract a(LX/7bN;)V
.end method

.method public abstract a(LX/7bN;LX/1ot;)V
.end method

.method public abstract a(LX/7bV;)V
.end method

.method public abstract a(LX/7bX;)V
.end method

.method public abstract a(LX/7bZ;)V
.end method

.method public abstract a(LX/7bb;)V
.end method

.method public abstract a(LX/7bd;)V
.end method

.method public abstract a(LX/7bf;)V
.end method

.method public abstract a(LX/7bh;)V
.end method

.method public abstract a(LX/7bk;)V
.end method

.method public abstract a(LX/7bm;)V
.end method

.method public abstract a(LX/7br;)V
.end method

.method public abstract a(LX/7bt;)V
.end method

.method public abstract a(LX/7bx;)V
.end method

.method public abstract a(LX/7bz;)V
.end method

.method public abstract a(LX/7c2;)V
.end method

.method public abstract a(LX/7c4;)V
.end method

.method public abstract a(Landroid/os/Bundle;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/ILocationSourceDelegate;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a([B)V
.end method

.method public abstract b()F
.end method

.method public abstract b(F)V
.end method

.method public abstract b(LX/1ot;)V
.end method

.method public abstract b(Landroid/os/Bundle;)V
.end method

.method public abstract b(Z)Z
.end method

.method public abstract c()F
.end method

.method public abstract c(Landroid/os/Bundle;)V
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()V
.end method

.method public abstract d(Z)V
.end method

.method public abstract e()V
.end method

.method public abstract e(Z)V
.end method

.method public abstract f()I
.end method

.method public abstract g()Z
.end method

.method public abstract h()Z
.end method

.method public abstract i()Z
.end method

.method public abstract j()Landroid/location/Location;
.end method

.method public abstract k()Lcom/google/android/gms/maps/internal/IUiSettingsDelegate;
.end method

.method public abstract l()Lcom/google/android/gms/maps/internal/IProjectionDelegate;
.end method

.method public abstract m()Z
.end method

.method public abstract n()LX/7cP;
.end method

.method public abstract o()V
.end method

.method public abstract p()V
.end method

.method public abstract q()V
.end method

.method public abstract r()V
.end method

.method public abstract s()Z
.end method

.method public abstract t()V
.end method

.method public abstract u()V
.end method
