.class public interface abstract Lcom/google/android/gms/maps/internal/ICameraUpdateFactoryDelegate;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()LX/1ot;
.end method

.method public abstract a(F)LX/1ot;
.end method

.method public abstract a(FF)LX/1ot;
.end method

.method public abstract a(FII)LX/1ot;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/CameraPosition;)LX/1ot;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;)LX/1ot;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;F)LX/1ot;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;I)LX/1ot;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;III)LX/1ot;
.end method

.method public abstract b()LX/1ot;
.end method

.method public abstract b(F)LX/1ot;
.end method
