.class public final Lcom/google/android/gms/location/LocationSettingsStates;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/location/LocationSettingsStates;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:Z

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7aP;

    invoke-direct {v0}, LX/7aP;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/LocationSettingsStates;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IZZZZZZ)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput p1, p0, Lcom/google/android/gms/location/LocationSettingsStates;->a:I

    iput-boolean p2, p0, Lcom/google/android/gms/location/LocationSettingsStates;->b:Z

    iput-boolean p3, p0, Lcom/google/android/gms/location/LocationSettingsStates;->c:Z

    iput-boolean p4, p0, Lcom/google/android/gms/location/LocationSettingsStates;->d:Z

    iput-boolean p5, p0, Lcom/google/android/gms/location/LocationSettingsStates;->e:Z

    iput-boolean p6, p0, Lcom/google/android/gms/location/LocationSettingsStates;->f:Z

    iput-boolean p7, p0, Lcom/google/android/gms/location/LocationSettingsStates;->g:Z

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-boolean p2, p0, Lcom/google/android/gms/location/LocationSettingsStates;->b:Z

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x2

    iget-boolean p2, p0, Lcom/google/android/gms/location/LocationSettingsStates;->c:Z

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x3

    iget-boolean p2, p0, Lcom/google/android/gms/location/LocationSettingsStates;->d:Z

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x4

    iget-boolean p2, p0, Lcom/google/android/gms/location/LocationSettingsStates;->e:Z

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x5

    iget-boolean p2, p0, Lcom/google/android/gms/location/LocationSettingsStates;->f:Z

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/4 v1, 0x6

    iget-boolean p2, p0, Lcom/google/android/gms/location/LocationSettingsStates;->g:Z

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;IZ)V

    const/16 v1, 0x3e8

    iget p2, p0, Lcom/google/android/gms/location/LocationSettingsStates;->a:I

    move p2, p2

    invoke-static {p1, v1, p2}, LX/2xC;->a(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void
.end method
