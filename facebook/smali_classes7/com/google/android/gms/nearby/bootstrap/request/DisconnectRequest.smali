.class public Lcom/google/android/gms/nearby/bootstrap/request/DisconnectRequest;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""


# static fields
.field public static final CREATOR:LX/7cz;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/nearby/bootstrap/Device;

.field public final c:LX/7Ze;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7cz;

    invoke-direct {v0}, LX/7cz;-><init>()V

    sput-object v0, Lcom/google/android/gms/nearby/bootstrap/request/DisconnectRequest;->CREATOR:LX/7cz;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/nearby/bootstrap/Device;Landroid/os/IBinder;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput p1, p0, Lcom/google/android/gms/nearby/bootstrap/request/DisconnectRequest;->a:I

    invoke-static {p2}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/nearby/bootstrap/Device;

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/DisconnectRequest;->b:Lcom/google/android/gms/nearby/bootstrap/Device;

    invoke-static {p3}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, LX/7Zg;->a(Landroid/os/IBinder;)LX/7Ze;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/DisconnectRequest;->c:LX/7Ze;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/DisconnectRequest;->b:Lcom/google/android/gms/nearby/bootstrap/Device;

    move-object v2, v2

    invoke-static {p1, v1, v2, p2, v3}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/DisconnectRequest;->c:LX/7Ze;

    invoke-interface {v2}, LX/7Ze;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    move-object v2, v2

    invoke-static {p1, v1, v2, v3}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/DisconnectRequest;->a:I

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void
.end method
