.class public Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""


# static fields
.field public static final CREATOR:LX/7d2;


# instance fields
.field public final a:I

.field public final b:LX/7Zj;

.field public final c:LX/7Ze;

.field public final d:B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7d2;

    invoke-direct {v0}, LX/7d2;-><init>()V

    sput-object v0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->CREATOR:LX/7d2;

    return-void
.end method

.method public constructor <init>(IBLandroid/os/IBinder;Landroid/os/IBinder;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput p1, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->a:I

    iput-byte p2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->d:B

    invoke-static {p3}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->b:LX/7Zj;

    invoke-static {p4}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, LX/7Zg;->a(Landroid/os/IBinder;)LX/7Ze;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->c:LX/7Ze;

    return-void

    :cond_0
    const-string v0, "com.google.android.gms.nearby.bootstrap.internal.IScanListener"

    invoke-interface {p3, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p1, v0, LX/7Zj;

    if-eqz p1, :cond_1

    check-cast v0, LX/7Zj;

    goto :goto_0

    :cond_1
    new-instance v0, LX/7Zk;

    invoke-direct {v0, p3}, LX/7Zk;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 p2, 0x0

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->b:LX/7Zj;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->c:LX/7Ze;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x3

    iget-byte v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->d:B

    move v2, v2

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;IB)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->a:I

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->b:LX/7Zj;

    invoke-interface {v2}, LX/7Zj;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StartScanRequest;->c:LX/7Ze;

    invoke-interface {v2}, LX/7Ze;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_1
.end method
