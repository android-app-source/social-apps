.class public Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""


# static fields
.field public static final CREATOR:LX/7cw;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/nearby/bootstrap/Device;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:LX/7ZY;

.field public final f:LX/7Zb;

.field public final g:LX/7Ze;

.field public final h:B

.field public final i:J

.field public final j:Ljava/lang/String;

.field public final k:B

.field public final l:B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7cw;

    invoke-direct {v0}, LX/7cw;-><init>()V

    sput-object v0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->CREATOR:LX/7cw;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/nearby/bootstrap/Device;Ljava/lang/String;Ljava/lang/String;BJLjava/lang/String;BBLandroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput p1, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->a:I

    invoke-static {p2}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/nearby/bootstrap/Device;

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->b:Lcom/google/android/gms/nearby/bootstrap/Device;

    invoke-static {p3}, LX/1ol;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->c:Ljava/lang/String;

    invoke-static {p4}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->d:Ljava/lang/String;

    iput-byte p5, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->h:B

    iput-wide p6, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->i:J

    iput-byte p9, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->k:B

    iput-byte p10, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->l:B

    iput-object p8, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->j:Ljava/lang/String;

    invoke-static {p11}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p11}, LX/7Za;->a(Landroid/os/IBinder;)LX/7ZY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->e:LX/7ZY;

    invoke-static {p12}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p12}, LX/7Zd;->a(Landroid/os/IBinder;)LX/7Zb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->f:LX/7Zb;

    invoke-static {p13}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p13}, LX/7Zg;->a(Landroid/os/IBinder;)LX/7Ze;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->g:LX/7Ze;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 7

    const/4 v4, 0x0

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->b:Lcom/google/android/gms/nearby/bootstrap/Device;

    move-object v2, v2

    invoke-static {p1, v1, v2, p2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->c:Ljava/lang/String;

    move-object v2, v2

    invoke-static {p1, v1, v2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->d:Ljava/lang/String;

    move-object v2, v2

    invoke-static {p1, v1, v2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->e:LX/7ZY;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    invoke-static {p1, v1, v2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->f:LX/7Zb;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    invoke-static {p1, v1, v2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->g:LX/7Ze;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object v2, v2

    invoke-static {p1, v1, v2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x7

    iget-byte v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->h:B

    move v2, v2

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;IB)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->a:I

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;II)V

    const/16 v1, 0x8

    iget-wide v5, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->i:J

    move-wide v2, v5

    invoke-static {p1, v1, v2, v3}, LX/2xC;->a(Landroid/os/Parcel;IJ)V

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->j:Ljava/lang/String;

    move-object v2, v2

    invoke-static {p1, v1, v2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v1, 0xa

    iget-byte v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->k:B

    move v2, v2

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;IB)V

    const/16 v1, 0xb

    iget-byte v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->l:B

    move v2, v2

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;IB)V

    invoke-static {p1, v0}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->e:LX/7ZY;

    invoke-interface {v2}, LX/7ZY;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->f:LX/7Zb;

    invoke-interface {v2}, LX/7Zb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/ConnectRequest;->g:LX/7Ze;

    invoke-interface {v2}, LX/7Ze;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_2
.end method
