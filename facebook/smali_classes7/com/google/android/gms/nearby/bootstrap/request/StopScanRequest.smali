.class public Lcom/google/android/gms/nearby/bootstrap/request/StopScanRequest;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""


# static fields
.field public static final CREATOR:LX/7d3;


# instance fields
.field public final a:I

.field public final b:LX/7Ze;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7d3;

    invoke-direct {v0}, LX/7d3;-><init>()V

    sput-object v0, Lcom/google/android/gms/nearby/bootstrap/request/StopScanRequest;->CREATOR:LX/7d3;

    return-void
.end method

.method public constructor <init>(ILandroid/os/IBinder;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput p1, p0, Lcom/google/android/gms/nearby/bootstrap/request/StopScanRequest;->a:I

    invoke-static {p2}, LX/1ol;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, LX/7Zg;->a(Landroid/os/IBinder;)LX/7Ze;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/bootstrap/request/StopScanRequest;->b:LX/7Ze;

    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StopScanRequest;->b:LX/7Ze;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    const/4 p2, 0x0

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StopScanRequest;->a:I

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/nearby/bootstrap/request/StopScanRequest;->b:LX/7Ze;

    invoke-interface {v2}, LX/7Ze;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_0
.end method
