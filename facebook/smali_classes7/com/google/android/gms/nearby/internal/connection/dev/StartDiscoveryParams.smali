.class public final Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:LX/7df;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/7dW;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:Lcom/google/android/gms/nearby/connection/dev/DiscoveryOptions;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7dH;

    invoke-direct {v0}, LX/7dH;-><init>()V

    sput-object v0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/os/IBinder;Landroid/os/IBinder;Ljava/lang/String;JLcom/google/android/gms/nearby/connection/dev/DiscoveryOptions;)V
    .locals 1
    .param p2    # Landroid/os/IBinder;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/IBinder;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput p1, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->a:I

    invoke-static {p2}, LX/7dh;->a(Landroid/os/IBinder;)LX/7df;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->b:LX/7df;

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->c:LX/7dW;

    iput-object p4, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->d:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->e:J

    iput-object p7, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->f:Lcom/google/android/gms/nearby/connection/dev/DiscoveryOptions;

    return-void

    :cond_0
    const-string v0, "com.google.android.gms.nearby.internal.connection.dev.IDiscoveryCallback"

    invoke-interface {p3, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p1, v0, LX/7dW;

    if-eqz p1, :cond_1

    check-cast v0, LX/7dW;

    goto :goto_0

    :cond_1
    new-instance v0, LX/7dX;

    invoke-direct {v0, p3}, LX/7dX;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;

    iget v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->a:I

    iget v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->a:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->b:LX/7df;

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->b:LX/7df;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->c:LX/7dW;

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->c:LX/7dW;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->d:Ljava/lang/String;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->f:Lcom/google/android/gms/nearby/connection/dev/DiscoveryOptions;

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->f:Lcom/google/android/gms/nearby/connection/dev/DiscoveryOptions;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->b:LX/7df;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->c:LX/7dW;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->f:Lcom/google/android/gms/nearby/connection/dev/DiscoveryOptions;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2wy;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 7

    const/4 v4, 0x0

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->b:LX/7df;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    invoke-static {p1, v1, v2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->c:LX/7dW;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    invoke-static {p1, v1, v2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->d:Ljava/lang/String;

    move-object v2, v2

    invoke-static {p1, v1, v2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x4

    iget-wide v5, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->e:J

    move-wide v2, v5

    invoke-static {p1, v1, v2, v3}, LX/2xC;->a(Landroid/os/Parcel;IJ)V

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->f:Lcom/google/android/gms/nearby/connection/dev/DiscoveryOptions;

    move-object v2, v2

    invoke-static {p1, v1, v2, p2, v4}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->a:I

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->b:LX/7df;

    invoke-interface {v2}, LX/7df;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/StartDiscoveryParams;->c:LX/7dW;

    invoke-interface {v2}, LX/7dW;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_1
.end method
