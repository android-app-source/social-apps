.class public final Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:LX/7df;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final c:LX/7dY;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final d:LX/7dU;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:[B
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, LX/7dE;

    invoke-direct {v0}, LX/7dE;-><init>()V

    sput-object v0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 1
    .param p2    # Landroid/os/IBinder;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/IBinder;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/os/IBinder;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # [B
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput p1, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->a:I

    invoke-static {p2}, LX/7dh;->a(Landroid/os/IBinder;)LX/7df;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->b:LX/7df;

    invoke-static {p3}, LX/7da;->a(Landroid/os/IBinder;)LX/7dY;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->c:LX/7dY;

    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    iput-object v0, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->d:LX/7dU;

    iput-object p5, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->e:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->g:[B

    return-void

    :cond_0
    const-string v0, "com.google.android.gms.nearby.internal.connection.dev.IConnectionResponseListener"

    invoke-interface {p4, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p1, v0, LX/7dU;

    if-eqz p1, :cond_1

    check-cast v0, LX/7dU;

    goto :goto_0

    :cond_1
    new-instance v0, LX/7dV;

    invoke-direct {v0, p4}, LX/7dV;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;

    iget v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->a:I

    iget v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->a:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->b:LX/7df;

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->b:LX/7df;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->c:LX/7dY;

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->c:LX/7dY;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->d:LX/7dU;

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->d:LX/7dU;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->e:Ljava/lang/String;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->f:Ljava/lang/String;

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->g:[B

    iget-object v3, p1, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->g:[B

    invoke-static {v2, v3}, LX/2wy;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->b:LX/7df;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->c:LX/7dY;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->d:LX/7dU;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->g:[B

    aput-object v2, v0, v1

    invoke-static {v0}, LX/2wy;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 p2, 0x0

    invoke-static {p1}, LX/2xC;->a(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->b:LX/7df;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->c:LX/7dY;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->d:LX/7dU;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_2
    move-object v2, v2

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;ILandroid/os/IBinder;Z)V

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->e:Ljava/lang/String;

    move-object v2, v2

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->f:Ljava/lang/String;

    move-object v2, v2

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->g:[B

    move-object v2, v2

    invoke-static {p1, v1, v2, p2}, LX/2xC;->a(Landroid/os/Parcel;I[BZ)V

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->a:I

    invoke-static {p1, v1, v2}, LX/2xC;->a(Landroid/os/Parcel;II)V

    invoke-static {p1, v0}, LX/2xC;->c(Landroid/os/Parcel;I)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->b:LX/7df;

    invoke-interface {v2}, LX/7df;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->c:LX/7dY;

    invoke-interface {v2}, LX/7dY;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/nearby/internal/connection/dev/SendConnectionRequestParams;->d:LX/7dU;

    invoke-interface {v2}, LX/7dU;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    goto :goto_2
.end method
