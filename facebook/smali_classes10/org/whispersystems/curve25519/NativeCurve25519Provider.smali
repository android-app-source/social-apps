.class public Lorg/whispersystems/curve25519/NativeCurve25519Provider;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EZc;


# static fields
.field private static a:Z

.field private static b:Ljava/lang/Throwable;


# instance fields
.field private c:LX/EZg;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2139770
    sput-boolean v1, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->a:Z

    .line 2139771
    const/4 v0, 0x0

    sput-object v0, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->b:Ljava/lang/Throwable;

    .line 2139772
    :try_start_0
    const-string v0, "curve25519"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 2139773
    const/4 v0, 0x1

    sput-boolean v0, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->a:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2139774
    :goto_0
    return-void

    .line 2139775
    :catch_0
    move-exception v0

    .line 2139776
    :goto_1
    sput-boolean v1, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->a:Z

    .line 2139777
    sput-object v0, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->b:Ljava/lang/Throwable;

    goto :goto_0

    .line 2139778
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2139763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2139764
    new-instance v0, LX/EZg;

    invoke-direct {v0}, LX/EZg;-><init>()V

    iput-object v0, p0, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->c:LX/EZg;

    .line 2139765
    sget-boolean v0, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->a:Z

    if-nez v0, :cond_0

    new-instance v0, LX/EZj;

    sget-object v1, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->b:Ljava/lang/Throwable;

    invoke-direct {v0, v1}, LX/EZj;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 2139766
    :cond_0
    const/16 v0, 0x7a69

    :try_start_0
    invoke-direct {p0, v0}, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->smokeCheck(I)Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2139767
    return-void

    .line 2139768
    :catch_0
    move-exception v0

    .line 2139769
    new-instance v1, LX/EZj;

    invoke-direct {v1, v0}, LX/EZj;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private native smokeCheck(I)Z
.end method


# virtual methods
.method public final a(LX/EZg;)V
    .locals 0

    .prologue
    .line 2139779
    iput-object p1, p0, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->c:LX/EZg;

    .line 2139780
    return-void
.end method

.method public final a()[B
    .locals 1

    .prologue
    .line 2139781
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->a(I)[B

    move-result-object v0

    .line 2139782
    invoke-virtual {p0, v0}, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->generatePrivateKey([B)[B

    move-result-object v0

    return-object v0
.end method

.method public final a(I)[B
    .locals 2

    .prologue
    .line 2139783
    new-array v0, p1, [B

    .line 2139784
    iget-object v1, p0, Lorg/whispersystems/curve25519/NativeCurve25519Provider;->c:LX/EZg;

    invoke-virtual {v1, v0}, LX/EZg;->a([B)V

    .line 2139785
    return-object v0
.end method

.method public native calculateAgreement([B[B)[B
.end method

.method public native calculateSignature([B[B[B)[B
.end method

.method public native calculateVrfSignature([B[B[B)[B
.end method

.method public native generatePrivateKey([B)[B
.end method

.method public native generatePublicKey([B)[B
.end method

.method public native verifySignature([B[B[B)Z
.end method

.method public native verifyVrfSignature([B[B[B)[B
.end method
