.class public Lcom/facebook/survey/activities/SurveyDialogActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1957380
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1957381
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1957382
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v0

    .line 1957383
    new-instance v3, LX/D1o;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v3, v1, v2}, LX/D1o;-><init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1957384
    move-object v0, v3

    .line 1957385
    check-cast v0, LX/D1o;

    .line 1957386
    invoke-virtual {p0}, Lcom/facebook/survey/activities/SurveyDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "survey_id"

    const-wide/16 v4, -0x1

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1957387
    new-instance v1, LX/0ju;

    invoke-direct {v1, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const-string v4, "Take Survey"

    invoke-virtual {v1, v4}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const-string v4, "Close"

    new-instance v5, LX/D1m;

    invoke-direct {v5, p0}, LX/D1m;-><init>(Lcom/facebook/survey/activities/SurveyDialogActivity;)V

    invoke-virtual {v1, v4, v5}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const-string v4, "Take survey"

    new-instance v5, LX/D1l;

    invoke-direct {v5, p0, v0, v2, v3}, LX/D1l;-><init>(Lcom/facebook/survey/activities/SurveyDialogActivity;LX/D1o;J)V

    invoke-virtual {v1, v4, v5}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const-string v1, "Take this survey!"

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1957388
    return-void
.end method
