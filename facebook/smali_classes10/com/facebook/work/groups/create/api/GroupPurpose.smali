.class public final Lcom/facebook/work/groups/create/api/GroupPurpose;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/work/groups/create/api/GroupPurpose;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2129954
    new-instance v0, LX/EWO;

    invoke-direct {v0}, LX/EWO;-><init>()V

    sput-object v0, Lcom/facebook/work/groups/create/api/GroupPurpose;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2129955
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129956
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->a:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 2129957
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->b:Ljava/lang/String;

    .line 2129958
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->c:Ljava/lang/String;

    .line 2129959
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->d:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2129960
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 0

    .prologue
    .line 2129961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129962
    iput-object p1, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->a:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    .line 2129963
    iput-object p2, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->b:Ljava/lang/String;

    .line 2129964
    iput-object p3, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->c:Ljava/lang/String;

    .line 2129965
    iput-object p4, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->d:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2129966
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2129967
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2129968
    iget-object v0, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->a:Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2129969
    iget-object v0, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2129970
    iget-object v0, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2129971
    iget-object v0, p0, Lcom/facebook/work/groups/create/api/GroupPurpose;->d:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2129972
    return-void
.end method
