.class public final Lcom/facebook/work/groups/create/api/GroupExtraSettings;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/work/groups/create/api/GroupExtraSettings;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2129935
    new-instance v0, LX/EWN;

    invoke-direct {v0}, LX/EWN;-><init>()V

    sput-object v0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2129941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129942
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->a:Z

    .line 2129943
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->b:Z

    .line 2129944
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->c:Ljava/lang/String;

    .line 2129945
    return-void
.end method

.method public constructor <init>(ZZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2129946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2129947
    iput-boolean p1, p0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->a:Z

    .line 2129948
    iput-boolean p2, p0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->b:Z

    .line 2129949
    iput-object p3, p0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->c:Ljava/lang/String;

    .line 2129950
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2129940
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2129936
    iget-boolean v0, p0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2129937
    iget-boolean v0, p0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2129938
    iget-object v0, p0, Lcom/facebook/work/groups/create/api/GroupExtraSettings;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2129939
    return-void
.end method
