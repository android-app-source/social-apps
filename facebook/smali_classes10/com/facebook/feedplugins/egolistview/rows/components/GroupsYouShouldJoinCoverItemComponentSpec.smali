.class public Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/1nu;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1974680
    const-class v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1974681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1974682
    iput-object p1, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;->b:LX/1nu;

    .line 1974683
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;
    .locals 4

    .prologue
    .line 1974669
    const-class v1, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;

    monitor-enter v1

    .line 1974670
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1974671
    sput-object v2, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1974672
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974673
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1974674
    new-instance p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;-><init>(LX/1nu;)V

    .line 1974675
    move-object v0, p0

    .line 1974676
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1974677
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinCoverItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1974678
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1974679
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
