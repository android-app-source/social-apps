.class public Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3mE;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1nu;

.field public final c:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1974813
    const-class v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/1nu;LX/0Zb;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/3mE;",
            ">;",
            "LX/1nu;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1974808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1974809
    iput-object p1, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->a:LX/0Ot;

    .line 1974810
    iput-object p2, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->b:LX/1nu;

    .line 1974811
    iput-object p3, p0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->c:LX/0Zb;

    .line 1974812
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;
    .locals 6

    .prologue
    .line 1974797
    const-class v1, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;

    monitor-enter v1

    .line 1974798
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1974799
    sput-object v2, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1974800
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974801
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1974802
    new-instance v5, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;

    const/16 v3, 0x89a

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {v5, p0, v3, v4}, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;-><init>(LX/0Ot;LX/1nu;LX/0Zb;)V

    .line 1974803
    move-object v0, v5

    .line 1974804
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1974805
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/egolistview/rows/components/GroupsYouShouldJoinVisitCommunityComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1974806
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1974807
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
