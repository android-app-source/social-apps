.class public Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/2mY;

.field public final c:LX/2mZ;

.field public final d:LX/1nu;

.field public final e:LX/2mX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1979961
    const-class v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/2mY;LX/2mZ;LX/1nu;LX/2mX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1979945
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->b:LX/2mY;

    .line 1979946
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->c:LX/2mZ;

    .line 1979947
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->d:LX/1nu;

    .line 1979948
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->e:LX/2mX;

    .line 1979949
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;
    .locals 7

    .prologue
    .line 1979950
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;

    monitor-enter v1

    .line 1979951
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979952
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979953
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979954
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979955
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;

    const-class v3, LX/2mY;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2mY;

    const-class v4, LX/2mZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2mZ;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    const-class v6, LX/2mX;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2mX;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;-><init>(LX/2mY;LX/2mZ;LX/1nu;LX/2mX;)V

    .line 1979956
    move-object v0, p0

    .line 1979957
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979958
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetVideoStoryComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979959
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979960
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
