.class public Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/Bsr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1VE;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/TextLinkPartDefinition;Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;LX/1VE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980567
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980568
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->a:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    .line 1980569
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    .line 1980570
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    .line 1980571
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;

    .line 1980572
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    .line 1980573
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->f:LX/1VE;

    .line 1980574
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1980575
    check-cast p2, LX/DGm;

    const/4 v6, -0x1

    .line 1980576
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1980577
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->a:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980578
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->f:LX/1VE;

    invoke-virtual {v1, v0}, LX/1VE;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v1

    .line 1980579
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->f:LX/1VE;

    invoke-virtual {v2, v0}, LX/1VE;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v2

    .line 1980580
    const v3, 0x7f0d160d

    iget-object v4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    new-instance v5, LX/BsB;

    invoke-direct {v5, v0, v2, v1}, LX/BsB;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;II)V

    invoke-interface {p1, v3, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980581
    const v1, 0x7f0d1616

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    new-instance v3, LX/Bs9;

    .line 1980582
    iget-boolean v4, p2, LX/DGm;->e:Z

    move v4, v4

    .line 1980583
    invoke-direct {v3, v0, v4, v6}, LX/Bs9;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZI)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980584
    iget-boolean v1, p2, LX/DGm;->e:Z

    move v1, v1

    .line 1980585
    if-eqz v1, :cond_0

    .line 1980586
    const v1, 0x7f0d1615

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    new-instance v3, LX/Bry;

    invoke-direct {v3, v0, v6}, LX/Bry;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980587
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1980588
    :cond_0
    const v1, 0x7f0d1615

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method
