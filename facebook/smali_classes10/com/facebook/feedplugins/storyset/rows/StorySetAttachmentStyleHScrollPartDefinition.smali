.class public Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/DGZ;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final b:Landroid/content/Context;

.field public final c:LX/2yK;

.field public final d:LX/1LV;

.field public final e:LX/1DR;

.field public final f:LX/2xr;

.field public final g:LX/2dq;

.field private final h:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/Context;LX/2yK;LX/1LV;LX/1DR;LX/2xr;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980077
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1980078
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1980079
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->b:Landroid/content/Context;

    .line 1980080
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->c:LX/2yK;

    .line 1980081
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->d:LX/1LV;

    .line 1980082
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->e:LX/1DR;

    .line 1980083
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->f:LX/2xr;

    .line 1980084
    iput-object p7, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->g:LX/2dq;

    .line 1980085
    iput-object p8, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->h:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 1980086
    iput-object p9, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->i:Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;

    .line 1980087
    iput-object p10, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->j:Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;

    .line 1980088
    return-void
.end method

.method private a(LX/DGZ;)LX/2eI;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DGZ;",
            ")",
            "LX/2eI",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1980072
    iget-object v0, p1, LX/DGZ;->a:LX/99i;

    iget-object v4, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980073
    iget-object v0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980074
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v2

    .line 1980075
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->e:LX/1DR;

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v1, v3}, LX/1DR;->a(Landroid/content/Context;)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v5

    .line 1980076
    new-instance v0, LX/DGX;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/DGX;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;LX/0Px;LX/DGZ;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;
    .locals 14

    .prologue
    .line 1980061
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    monitor-enter v1

    .line 1980062
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980063
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980064
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980065
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980066
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/2yK;->b(LX/0QB;)LX/2yK;

    move-result-object v6

    check-cast v6, LX/2yK;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v7

    check-cast v7, LX/1LV;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v8

    check-cast v8, LX/1DR;

    invoke-static {v0}, LX/2xr;->b(LX/0QB;)LX/2xr;

    move-result-object v9

    check-cast v9, LX/2xr;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v10

    check-cast v10, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/Context;LX/2yK;LX/1LV;LX/1DR;LX/2xr;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/VideoSetsPageItemWithSimpleBlingbarPartDefinition;Lcom/facebook/feedplugins/storyset/rows/LinkSetsPageItemPartDefinition;)V

    .line 1980067
    move-object v0, v3

    .line 1980068
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980069
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980070
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980071
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1980089
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1980044
    check-cast p2, LX/DGZ;

    .line 1980045
    iget-object v0, p2, LX/DGZ;->a:LX/99i;

    iget-object v0, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980046
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v1

    .line 1980047
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStorySet;

    .line 1980048
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p2, LX/DGZ;->a:LX/99i;

    iget-object v1, v1, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980049
    new-instance v2, LX/1X6;

    sget-object v3, LX/2eF;->a:LX/1Ua;

    sget-object v4, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v2, v1, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    move-object v1, v2

    .line 1980050
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980051
    iget-object v6, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->h:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    .line 1980052
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->e:LX/1DR;

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/1DR;->a(Landroid/content/Context;)I

    move-result v1

    .line 1980053
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->g:LX/2dq;

    int-to-float v1, v1

    sget-object v3, LX/2eF;->a:LX/1Ua;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    move-object v1, v1

    .line 1980054
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v2

    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->a(LX/DGZ;)LX/2eI;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980055
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x11a4508c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1980056
    check-cast p1, LX/DGZ;

    check-cast p2, Ljava/lang/Void;

    check-cast p3, LX/1Pf;

    check-cast p4, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1980057
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;->a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 1980058
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->setItemViewCacheSize(I)V

    .line 1980059
    const/16 v1, 0x1f

    const v2, 0x422e7c72

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1980060
    const/4 v0, 0x1

    return v0
.end method
