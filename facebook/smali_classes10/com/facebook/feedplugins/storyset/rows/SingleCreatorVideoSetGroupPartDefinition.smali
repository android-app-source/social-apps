.class public Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/1fv;

.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1fv;Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979962
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1979963
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->a:LX/1fv;

    .line 1979964
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    .line 1979965
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;

    .line 1979966
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;

    .line 1979967
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->e:Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;

    .line 1979968
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;
    .locals 9

    .prologue
    .line 1979969
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;

    monitor-enter v1

    .line 1979970
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979971
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979972
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979973
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979974
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;

    invoke-static {v0}, LX/1fv;->a(LX/0QB;)LX/1fv;

    move-result-object v4

    check-cast v4, LX/1fv;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;-><init>(LX/1fv;Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;)V

    .line 1979975
    move-object v0, v3

    .line 1979976
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979977
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979978
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1979980
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979981
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1979982
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1979983
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1979984
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->e:Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1979985
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1979986
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979987
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1979988
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->SINGLE_CREATOR_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979989
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1979990
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->a:LX/1fv;

    invoke-virtual {v0}, LX/1fv;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
