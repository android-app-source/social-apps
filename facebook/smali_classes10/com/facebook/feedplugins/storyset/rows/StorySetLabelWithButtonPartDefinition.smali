.class public Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/2sO;

.field private final b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final e:LX/1qb;


# direct methods
.method public constructor <init>(LX/2sO;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/1qb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980431
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980432
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->a:LX/2sO;

    .line 1980433
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 1980434
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980435
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980436
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->e:LX/1qb;

    .line 1980437
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;
    .locals 9

    .prologue
    .line 1980438
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;

    monitor-enter v1

    .line 1980439
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980440
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980441
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980442
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980443
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v4

    check-cast v4, LX/2sO;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v8

    check-cast v8, LX/1qb;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;-><init>(LX/2sO;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/1qb;)V

    .line 1980444
    move-object v0, v3

    .line 1980445
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980446
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980447
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980448
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1980449
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x0

    .line 1980450
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980451
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1980452
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->a:LX/2sO;

    invoke-virtual {v1, p2}, LX/2sO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;

    move-result-object v1

    .line 1980453
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->e:LX/1qb;

    invoke-virtual {v2, v0}, LX/1qb;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v2

    .line 1980454
    invoke-static {v0}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v0

    .line 1980455
    const v3, 0x7f0d2dda

    iget-object v4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980456
    const v2, 0x7f0d2ddb

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980457
    invoke-virtual {v1}, LX/2y5;->a()LX/1Nt;

    move-result-object v0

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980458
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->b:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v1, LX/2ya;

    invoke-direct {v1, p2, v5}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/Map;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980459
    return-object v5
.end method
