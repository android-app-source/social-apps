.class public Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/3FQ;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGq;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/2yK;

.field private final b:LX/23r;

.field private final c:LX/1VK;

.field private final d:LX/23s;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2yK;LX/23r;LX/1VK;LX/23s;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2yK;",
            "LX/23r;",
            "LX/1VK;",
            "LX/23s;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980835
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980836
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->a:LX/2yK;

    .line 1980837
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->b:LX/23r;

    .line 1980838
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->c:LX/1VK;

    .line 1980839
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->d:LX/23s;

    .line 1980840
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->e:LX/0Ot;

    .line 1980841
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;
    .locals 9

    .prologue
    .line 1980842
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;

    monitor-enter v1

    .line 1980843
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980844
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980845
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980846
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980847
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;

    invoke-static {v0}, LX/2yK;->b(LX/0QB;)LX/2yK;

    move-result-object v4

    check-cast v4, LX/2yK;

    const-class v5, LX/23r;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/23r;

    const-class v6, LX/1VK;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1VK;

    invoke-static {v0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v7

    check-cast v7, LX/23s;

    const/16 v8, 0x848

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;-><init>(LX/2yK;LX/23r;LX/1VK;LX/23s;LX/0Ot;)V

    .line 1980848
    move-object v0, v3

    .line 1980849
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980850
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980851
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980852
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1980853
    check-cast p2, LX/DGq;

    check-cast p3, LX/1Po;

    const/4 v5, 0x0

    .line 1980854
    iget-object v0, p2, LX/DGq;->a:LX/DGm;

    .line 1980855
    iget-object v1, v0, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v1

    .line 1980856
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980857
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1980858
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v1

    .line 1980859
    iget-object v2, p2, LX/DGq;->d:LX/3FQ;

    if-eqz v2, :cond_0

    iget-object v2, p2, LX/DGq;->d:LX/3FQ;

    invoke-interface {v2}, LX/3FQ;->getVideoStoryPersistentState()LX/2oM;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1980860
    :cond_0
    new-instance v2, LX/2oK;

    iget-object v4, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->c:LX/1VK;

    invoke-direct {v2, v3, v1, v4}, LX/2oK;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;LX/1VK;)V

    move-object v1, p3

    .line 1980861
    check-cast v1, LX/1Pr;

    invoke-interface {v1, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oM;

    move-object v2, v0

    .line 1980862
    :goto_0
    new-instance v0, LX/3Qv;

    invoke-direct {v0}, LX/3Qv;-><init>()V

    .line 1980863
    iput-object v3, v0, LX/3Qv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980864
    move-object v0, v0

    .line 1980865
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-static {v1}, LX/23s;->a(LX/1Qt;)Ljava/lang/String;

    move-result-object v1

    .line 1980866
    iput-object v1, v0, LX/3Qv;->d:Ljava/lang/String;

    .line 1980867
    move-object v0, v0

    .line 1980868
    sget-object v1, LX/04g;->BY_USER:LX/04g;

    .line 1980869
    iput-object v1, v0, LX/3Qv;->h:LX/04g;

    .line 1980870
    move-object v0, v0

    .line 1980871
    sget-object v1, LX/04D;->VIDEO_SETS:LX/04D;

    .line 1980872
    iput-object v1, v0, LX/3Qv;->g:LX/04D;

    .line 1980873
    move-object v0, v0

    .line 1980874
    iget-object v1, p2, LX/DGq;->a:LX/DGm;

    .line 1980875
    iget-object v3, v1, LX/DGm;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v6, v3

    .line 1980876
    iget-object v3, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1980877
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v3}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v7

    .line 1980878
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    .line 1980879
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result p3

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, p3, :cond_1

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1980880
    invoke-virtual {v6, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v8, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1980881
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 1980882
    :cond_1
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v1, v3

    .line 1980883
    iput-object v1, v0, LX/3Qv;->b:Ljava/util/List;

    .line 1980884
    move-object v1, v0

    .line 1980885
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->b:LX/23r;

    invoke-virtual {v1}, LX/3Qv;->a()LX/3Qw;

    move-result-object v1

    iget-object v3, p2, LX/DGq;->b:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v4, p2, LX/DGq;->d:LX/3FQ;

    invoke-virtual/range {v0 .. v5}, LX/23r;->a(LX/3Qw;LX/2oM;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D6L;)LX/3Qx;

    move-result-object v0

    .line 1980886
    new-instance v1, LX/DGr;

    invoke-direct {v1, p0, p2, v0}, LX/DGr;-><init>(Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;LX/DGq;Landroid/view/View$OnClickListener;)V

    .line 1980887
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsChannelFeedLauncherPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v2, LX/3FZ;

    invoke-direct {v2, v1}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980888
    return-object v5

    .line 1980889
    :cond_2
    iget-object v0, p2, LX/DGq;->d:LX/3FQ;

    invoke-interface {v0}, LX/3FQ;->getVideoStoryPersistentState()LX/2oM;

    move-result-object v2

    goto :goto_0
.end method
