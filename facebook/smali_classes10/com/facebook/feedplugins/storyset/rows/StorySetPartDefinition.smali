.class public Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

.field private final b:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;

.field private final e:Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

.field private final f:Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;

.field private final g:LX/03V;

.field public final h:Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;LX/03V;Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980645
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1980646
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    .line 1980647
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    .line 1980648
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    .line 1980649
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;

    .line 1980650
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->e:Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    .line 1980651
    iput-object p7, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->f:Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;

    .line 1980652
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->g:LX/03V;

    .line 1980653
    iput-object p8, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->h:Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;

    .line 1980654
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;
    .locals 12

    .prologue
    .line 1980655
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;

    monitor-enter v1

    .line 1980656
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980657
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980658
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980659
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980660
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;LX/03V;Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;)V

    .line 1980661
    move-object v0, v3

    .line 1980662
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980663
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980664
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980665
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1RF;LX/99i;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<",
            "LX/1Pf;",
            ">;",
            "LX/99i",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1980666
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->b:Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1980667
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1980668
    return-void
.end method

.method public static c(Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;LX/1RF;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;Lcom/facebook/feed/rows/core/props/FeedProps;LX/99i;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<",
            "LX/1Pf;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;",
            "LX/99i",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1980669
    new-instance v0, LX/DGZ;

    invoke-direct {v0, p4, p2}, LX/DGZ;-><init>(LX/99i;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;)V

    .line 1980670
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;

    invoke-static {p1, v1, p3}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->f:Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentStyleHScrollPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1980671
    return-void
.end method

.method public static d(Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/api/MultiRowSubParts",
            "<",
            "LX/1Pf;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1980672
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->e:Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1980673
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1980674
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v7, 0x0

    .line 1980675
    invoke-static {}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/FeedUnitPagerIndicatorPartDefinition;->b()LX/99l;

    move-result-object v0

    .line 1980676
    new-instance v2, LX/99i;

    invoke-direct {v2, p2, v0}, LX/99i;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/8yy;)V

    .line 1980677
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980678
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v3

    .line 1980679
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    .line 1980680
    sget-object v5, LX/DGn;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1980681
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1980682
    :pswitch_0
    invoke-static {p0, p1, p2}, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->d(Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1980683
    invoke-static {p0, p1, v0, p2, v2}, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->c(Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;LX/1RF;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;Lcom/facebook/feed/rows/core/props/FeedProps;LX/99i;)V

    .line 1980684
    :goto_1
    :pswitch_1
    return-object v7

    .line 1980685
    :pswitch_2
    invoke-static {p0, p1, p2}, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->d(Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1980686
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1980687
    goto :goto_1

    .line 1980688
    :pswitch_3
    invoke-static {p0, p1, p2}, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->d(Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1980689
    invoke-static {p0, p1, v0, p2, v2}, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->c(Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;LX/1RF;Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;Lcom/facebook/feed/rows/core/props/FeedProps;LX/99i;)V

    .line 1980690
    goto :goto_1

    .line 1980691
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->h:Lcom/facebook/feedplugins/storyset/rows/SingleCreatorVideoSetGroupPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1980692
    goto :goto_1

    .line 1980693
    :pswitch_5
    invoke-direct {p0, p1, v2}, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->a(LX/1RF;LX/99i;)V

    goto :goto_1

    .line 1980694
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->g:LX/03V;

    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Failed to find GraphQLStorySetCollectionType.FALLBACK"

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1980695
    invoke-direct {p0, p1, v2}, Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;->a(LX/1RF;LX/99i;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1980696
    const/4 v0, 0x1

    return v0
.end method
