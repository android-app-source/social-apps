.class public Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGm;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/DHn;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;

.field private final c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition",
            "<TE;",
            "LX/DHn;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;

.field private final e:Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

.field private final f:Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980111
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980112
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;

    .line 1980113
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;

    .line 1980114
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    .line 1980115
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;

    .line 1980116
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->e:Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    .line 1980117
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->f:Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;

    .line 1980118
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;
    .locals 10

    .prologue
    .line 1980119
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    monitor-enter v1

    .line 1980120
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980121
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980122
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980123
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980124
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;)V

    .line 1980125
    move-object v0, v3

    .line 1980126
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980127
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980128
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980129
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1980130
    check-cast p2, LX/DGm;

    const/4 v3, 0x0

    .line 1980131
    iget-object v0, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1980132
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980133
    const v1, 0x7f0d1d87

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->f:Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;

    invoke-interface {p1, v1, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980134
    const v1, 0x7f0d1d8a

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/page/GraphQLStoryPageTextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980135
    const v1, 0x7f0d2de3

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->d:Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithButtonPartDefinition;

    invoke-static {v0}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980136
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/StorySetPageFooterPartDefinition;

    .line 1980137
    iget-object v1, p2, LX/DGm;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 1980138
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980139
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBasicPagePartDefinition;->e:Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980140
    return-object v3
.end method
