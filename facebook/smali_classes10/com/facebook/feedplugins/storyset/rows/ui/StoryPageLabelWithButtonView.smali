.class public Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;
.super Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;
.source ""

# interfaces
.implements LX/35p;


# instance fields
.field private final j:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1982389
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1982390
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1982391
    const v0, 0x7f0313e0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1982392
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1982393
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1982394
    const v0, 0x7f0d2ddc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;->j:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 1982395
    return-void
.end method


# virtual methods
.method public getActionButton()Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;
    .locals 1

    .prologue
    .line 1982396
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;->j:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    return-object v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1982397
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;->j:Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;

    .line 1982398
    iget-object p0, v0, Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;->a:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    move-object v0, p0

    .line 1982399
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1982400
    return-void
.end method
