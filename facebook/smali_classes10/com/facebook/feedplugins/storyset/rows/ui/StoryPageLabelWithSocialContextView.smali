.class public Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextView;
.super Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;
.source ""


# instance fields
.field private final j:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1982417
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1982418
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1982419
    const v0, 0x7f0313e3

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1982420
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1982421
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1982422
    const v0, 0x7f0d2ddd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextView;->j:Landroid/widget/TextView;

    .line 1982423
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;->setTitleLines(I)V

    .line 1982424
    return-void
.end method


# virtual methods
.method public setSocialContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1982425
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1982426
    return-void
.end method
