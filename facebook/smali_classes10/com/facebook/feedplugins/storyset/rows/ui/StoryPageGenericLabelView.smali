.class public Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/2yX;


# instance fields
.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1982385
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1982386
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1982387
    const v0, 0x7f0313dd

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1982388
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1982379
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1982380
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1982381
    const v0, 0x7f0d2dda

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;->j:Landroid/widget/TextView;

    .line 1982382
    const v0, 0x7f0d2ddb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;->k:Landroid/widget/TextView;

    .line 1982383
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;->setTitleLines(I)V

    .line 1982384
    return-void
.end method


# virtual methods
.method public setContextText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1982377
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1982378
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1982373
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1982374
    return-void
.end method

.method public setTitleLines(I)V
    .locals 1

    .prologue
    .line 1982375
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setLines(I)V

    .line 1982376
    return-void
.end method
