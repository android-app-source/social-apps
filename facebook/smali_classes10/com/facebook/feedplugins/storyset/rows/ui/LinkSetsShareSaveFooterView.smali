.class public Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements LX/1wK;


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/20X;",
            "Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private final e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private final f:Landroid/view/ViewGroup;

.field private final g:LX/DHk;

.field private final h:I

.field private final i:Landroid/graphics/drawable/Drawable;

.field private final j:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1982309
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1982310
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const v6, -0x6e685d

    .line 1982320
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1982321
    const-class v0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;

    invoke-static {v0, p0}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1982322
    const v0, 0x7f0309ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1982323
    invoke-virtual {p0, v3}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->setOrientation(I)V

    .line 1982324
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a011a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1982325
    invoke-virtual {p0}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f27

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 1982326
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1982327
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->b:LX/0Uh;

    const/16 v1, 0x3ce

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0102a4

    const v2, -0xb1a99b

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->h:I

    .line 1982328
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1982329
    sget-object v1, LX/20X;->SHARE:LX/20X;

    const v2, 0x7f0219c9

    iget v3, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->h:I

    invoke-direct {p0, v2, v3}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->b(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const v3, 0x7f081857

    const v4, 0x7f0d1927

    sget v5, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1982330
    sget-object v1, LX/20X;->SAVE:LX/20X;

    const v2, 0x7f020781

    iget v3, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->h:I

    invoke-direct {p0, v2, v3}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->b(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const v3, 0x7f081858

    const v4, 0x7f0d1928

    sget v5, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a:I

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1982331
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    .line 1982332
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    sget-object v1, LX/20X;->SHARE:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->d:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1982333
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    sget-object v1, LX/20X;->SAVE:LX/20X;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1982334
    const v0, 0x7f0d1926

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->f:Landroid/view/ViewGroup;

    .line 1982335
    new-instance v0, LX/DHk;

    invoke-direct {v0}, LX/DHk;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->g:LX/DHk;

    .line 1982336
    const v0, 0x7f020781

    invoke-direct {p0, v0, v6}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->b(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->i:Landroid/graphics/drawable/Drawable;

    .line 1982337
    const v0, 0x7f020781

    const v1, -0xa76f01

    invoke-direct {p0, v0, v1}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->b(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->j:Landroid/graphics/drawable/Drawable;

    .line 1982338
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1982339
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20X;

    .line 1982340
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1982341
    new-instance v3, LX/20a;

    iget-object v4, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->g:LX/DHk;

    invoke-direct {v3, v1, v4}, LX/20a;-><init>(LX/20X;LX/20Z;)V

    invoke-virtual {v0, v3}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1982342
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0102a3

    invoke-static {v0, v1, v6}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    goto/16 :goto_0

    .line 1982343
    :cond_1
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;III)Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;
    .locals 2

    .prologue
    .line 1982314
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1982315
    invoke-virtual {v0, p2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1982316
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSoundEffectsEnabled(Z)V

    .line 1982317
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1982318
    invoke-virtual {v0, p4}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setWarmupBackgroundResId(I)V

    .line 1982319
    return-object v0
.end method

.method private static a(Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;LX/0wM;LX/0Uh;)V
    .locals 0

    .prologue
    .line 1982313
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->b:LX/0Uh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0, v0, v1}, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->a(Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;LX/0wM;LX/0Uh;)V

    return-void
.end method

.method private b(II)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1982312
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->a:LX/0wM;

    invoke-virtual {v0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/20X;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1982311
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1982299
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->d:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a()V

    .line 1982300
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->a()V

    .line 1982301
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1982306
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const v1, 0x7f081859

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1982307
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1982308
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1982303
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const v1, 0x7f081858

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1982304
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->e:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1982305
    return-void
.end method

.method public setBottomDividerStyle(LX/1Wl;)V
    .locals 0

    .prologue
    .line 1982302
    return-void
.end method

.method public setButtonContainerBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1982344
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->f:Landroid/view/ViewGroup;

    invoke-static {v0, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1982345
    return-void
.end method

.method public setButtonContainerHeight(I)V
    .locals 0

    .prologue
    .line 1982273
    return-void
.end method

.method public setButtonWeights([F)V
    .locals 0

    .prologue
    .line 1982274
    return-void
.end method

.method public setButtons(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/20X;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1982275
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20X;

    .line 1982276
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 1982277
    :goto_1
    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    invoke-virtual {v3, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    goto :goto_0

    .line 1982278
    :cond_0
    const/16 v1, 0x8

    goto :goto_1

    .line 1982279
    :cond_1
    return-void
.end method

.method public setDownstateType(LX/1Wk;)V
    .locals 2

    .prologue
    .line 1982280
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1982281
    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setDownstateType(LX/1Wk;)V

    goto :goto_0

    .line 1982282
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 1982283
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1982284
    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setEnabled(Z)V

    goto :goto_0

    .line 1982285
    :cond_0
    return-void
.end method

.method public setHasCachedComments(Z)V
    .locals 0

    .prologue
    .line 1982286
    return-void
.end method

.method public setIsLiked(Z)V
    .locals 0

    .prologue
    .line 1982287
    return-void
.end method

.method public setOnButtonClickedListener(LX/20Z;)V
    .locals 1

    .prologue
    .line 1982288
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->g:LX/DHk;

    .line 1982289
    iput-object p1, v0, LX/DHk;->a:LX/20Z;

    .line 1982290
    return-void
.end method

.method public setShowIcons(Z)V
    .locals 2

    .prologue
    .line 1982291
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->values()LX/0Py;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    .line 1982292
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1982293
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(Z)V

    goto :goto_0

    .line 1982294
    :cond_0
    return-void
.end method

.method public setSprings(Ljava/util/EnumMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumMap",
            "<",
            "LX/20X;",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1982295
    invoke-virtual {p1}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20X;

    .line 1982296
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/ui/LinkSetsShareSaveFooterView;->c:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {p1, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    goto :goto_0

    .line 1982297
    :cond_0
    return-void
.end method

.method public setTopDividerStyle(LX/1Wl;)V
    .locals 0

    .prologue
    .line 1982298
    return-void
.end method
