.class public Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextAndButtonView;
.super Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;
.source ""


# instance fields
.field private final j:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1982415
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextAndButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1982416
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1982407
    const v0, 0x7f0313e2

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextAndButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1982408
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1982411
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1982412
    const v0, 0x7f0d2ddd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextAndButtonView;->j:Landroid/widget/TextView;

    .line 1982413
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;->setTitleLines(I)V

    .line 1982414
    return-void
.end method


# virtual methods
.method public setSocialContextText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1982409
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextAndButtonView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1982410
    return-void
.end method
