.class public Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGi;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextAndButtonView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2sO;

.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(LX/2sO;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980493
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980494
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;->a:LX/2sO;

    .line 1980495
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980496
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980497
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980498
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1980499
    check-cast p2, LX/DGi;

    .line 1980500
    const-string v0, "Temporary Social Context"

    .line 1980501
    const v1, 0x7f0d2dda

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v3, p2, LX/DGi;->a:Ljava/lang/CharSequence;

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980502
    const v1, 0x7f0d2ddb

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v3, p2, LX/DGi;->b:Ljava/lang/CharSequence;

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980503
    const v1, 0x7f0d2ddd

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980504
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextAndButtonPartDefinition;->a:LX/2sO;

    iget-object v1, p2, LX/DGi;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v1}, LX/2sO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;

    move-result-object v0

    invoke-virtual {v0}, LX/2y5;->a()LX/1Nt;

    move-result-object v0

    iget-object v1, p2, LX/DGi;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980505
    const/4 v0, 0x0

    return-object v0
.end method
