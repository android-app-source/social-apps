.class public Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/99i",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static r:LX/0Xm;


# instance fields
.field public final a:LX/1Ua;

.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final c:LX/1LV;

.field private final d:Z

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/Ap1;

.field public final g:Landroid/content/Context;

.field public final h:LX/1DR;

.field public final i:LX/2xr;

.field public final j:LX/1K9;

.field private final k:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;

.field public final m:Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;

.field public final n:Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;

.field public final o:Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;

.field public final p:Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;

.field private final q:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;LX/1DR;LX/1LV;LX/2xr;LX/1K9;LX/0Or;LX/0Or;LX/Ap1;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;LX/0ad;)V
    .locals 4
    .param p8    # LX/0Or;
        .annotation runtime Lcom/facebook/feedplugins/storyset/annotations/IsPaginatedStorySetEnabled;
        .end annotation
    .end param
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsHScrollVideoAutoPlayEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/1V7;",
            "LX/1DR;",
            "LX/1LV;",
            "LX/2xr;",
            "LX/1K9;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/Ap1;",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980242
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1980243
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->g:Landroid/content/Context;

    .line 1980244
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1980245
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->c:LX/1LV;

    .line 1980246
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->h:LX/1DR;

    .line 1980247
    iput-object p11, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->k:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 1980248
    invoke-interface {p8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->d:Z

    .line 1980249
    iput-object p9, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->e:LX/0Or;

    .line 1980250
    iput-object p10, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->f:LX/Ap1;

    .line 1980251
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->i:LX/2xr;

    .line 1980252
    iput-object p7, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->j:LX/1K9;

    .line 1980253
    invoke-static {}, LX/1UY;->e()LX/1UY;

    move-result-object v1

    invoke-virtual {p3}, LX/1V7;->c()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {p3}, LX/1V7;->d()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-virtual {v1, v2}, LX/1UY;->a(F)LX/1UY;

    move-result-object v1

    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->a:LX/1Ua;

    .line 1980254
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->l:Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;

    .line 1980255
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->m:Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;

    .line 1980256
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->n:Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;

    .line 1980257
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->o:Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;

    .line 1980258
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->p:Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;

    .line 1980259
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->q:LX/0ad;

    .line 1980260
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;
    .locals 3

    .prologue
    .line 1980234
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    monitor-enter v1

    .line 1980235
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980236
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980237
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980238
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->b(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980239
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980240
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLStorySet;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1980233
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->q:LX/0ad;

    sget-short v2, LX/Aow;->b:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, LX/39w;->c(Lcom/facebook/graphql/model/GraphQLStorySet;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;ILcom/facebook/graphql/model/GraphQLStorySet;)I
    .locals 1

    .prologue
    .line 1980198
    invoke-static {p0, p2}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->a(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLStorySet;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    return p1
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLStorySet;I)V
    .locals 1

    .prologue
    .line 1980231
    invoke-static {p0, p1, p2}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->b(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLStorySet;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1980232
    :cond_0
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;
    .locals 19

    .prologue
    .line 1980261
    new-instance v1, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static/range {p0 .. p0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static/range {p0 .. p0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-static/range {p0 .. p0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v6

    check-cast v6, LX/1LV;

    invoke-static/range {p0 .. p0}, LX/2xr;->a(LX/0QB;)LX/2xr;

    move-result-object v7

    check-cast v7, LX/2xr;

    invoke-static/range {p0 .. p0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v8

    check-cast v8, LX/1K9;

    const/16 v9, 0x31a

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x315

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/Ap1;->a(LX/0QB;)LX/Ap1;

    move-result-object v11

    check-cast v11, LX/Ap1;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;

    move-result-object v15

    check-cast v15, Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;

    move-result-object v16

    check-cast v16, Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;

    move-result-object v17

    check-cast v17, Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v18

    check-cast v18, LX/0ad;

    invoke-direct/range {v1 .. v18}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;LX/1DR;LX/1LV;LX/2xr;LX/1K9;LX/0Or;LX/0Or;LX/Ap1;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetVideoPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetPhotoPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetAppInstallPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetAutoPlayPageRootPartDefinition;Lcom/facebook/feedplugins/storyset/rows/StorySetLoadingPageRootPartDefinition;LX/0ad;)V

    .line 1980262
    return-object v1
.end method

.method public static b(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLStorySet;I)Z
    .locals 1

    .prologue
    .line 1980230
    iget-boolean v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->f:LX/Ap1;

    invoke-virtual {v0, p1, p2}, LX/Ap1;->a(Lcom/facebook/graphql/model/GraphQLStorySet;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->f:LX/Ap1;

    invoke-virtual {v0, p1}, LX/Ap1;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1980229
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1980200
    check-cast p2, LX/99i;

    const/4 v2, 0x0

    .line 1980201
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iget-object v1, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980202
    new-instance v3, LX/1X6;

    iget-object v4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->a:LX/1Ua;

    sget-object v5, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v3, v1, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    move-object v1, v3

    .line 1980203
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980204
    iget-boolean v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->d:Z

    if-eqz v0, :cond_0

    .line 1980205
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->q:LX/0ad;

    sget-short v1, LX/Aow;->i:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->f:LX/Ap1;

    iget-object v0, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980206
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1980207
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v1, v0}, LX/Ap1;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980208
    iget-object v0, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980209
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1980210
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {p0, v0, v2}, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->a$redex0(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;Lcom/facebook/graphql/model/GraphQLStorySet;I)V

    .line 1980211
    :cond_0
    iget-object v6, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->k:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    const/4 v3, 0x1

    .line 1980212
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->h:LX/1DR;

    invoke-virtual {v1}, LX/1DR;->a()I

    move-result v1

    .line 1980213
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->g:Landroid/content/Context;

    int-to-float v1, v1

    invoke-static {v2, v1}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v1

    .line 1980214
    invoke-static {v1, v3, v3}, LX/2eF;->a(IZZ)LX/2eF;

    move-result-object v1

    move-object v1, v1

    .line 1980215
    iget-object v2, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980216
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 1980217
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v2

    .line 1980218
    iget-object v3, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980219
    iget-object v4, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v4

    .line 1980220
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v3}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v3

    .line 1980221
    iget-object v4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;->h:LX/1DR;

    invoke-virtual {v4}, LX/1DR;->a()I

    move-result v4

    .line 1980222
    new-instance v5, LX/DGa;

    invoke-direct {v5, p0, v3, p2, v4}, LX/DGa;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetHScrollPartDefinition;LX/0Px;LX/99i;I)V

    move-object v3, v5

    .line 1980223
    iget-object v4, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980224
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v5

    .line 1980225
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980226
    iget-object p0, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, p0

    .line 1980227
    check-cast v5, LX/0jW;

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980228
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1980199
    const/4 v0, 0x1

    return v0
.end method
