.class public Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/DGf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DGf",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/DGf;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980387
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1980388
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->d:LX/DGf;

    .line 1980389
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->e:LX/1V0;

    .line 1980390
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1980353
    instance-of v0, p3, LX/5Ok;

    if-eqz v0, :cond_1

    sget-object v0, LX/1dl;->HIDDEN:LX/1dl;

    move-object v1, v0

    .line 1980354
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->d:LX/DGf;

    const/4 v2, 0x0

    .line 1980355
    new-instance v3, LX/DGe;

    invoke-direct {v3, v0}, LX/DGe;-><init>(LX/DGf;)V

    .line 1980356
    iget-object v4, v0, LX/DGf;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DGd;

    .line 1980357
    if-nez v4, :cond_0

    .line 1980358
    new-instance v4, LX/DGd;

    invoke-direct {v4, v0}, LX/DGd;-><init>(LX/DGf;)V

    .line 1980359
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/DGd;->a$redex0(LX/DGd;LX/1De;IILX/DGe;)V

    .line 1980360
    move-object v3, v4

    .line 1980361
    move-object v2, v3

    .line 1980362
    move-object v0, v2

    .line 1980363
    iget-object v2, v0, LX/DGd;->a:LX/DGe;

    iput-object p2, v2, LX/DGe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980364
    iget-object v2, v0, LX/DGd;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1980365
    move-object v2, v0

    .line 1980366
    move-object v0, p3

    check-cast v0, LX/1Pk;

    .line 1980367
    iget-object v3, v2, LX/DGd;->a:LX/DGe;

    iput-object v0, v3, LX/DGe;->b:LX/1Pk;

    .line 1980368
    iget-object v3, v2, LX/DGd;->e:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1980369
    move-object v0, v2

    .line 1980370
    iget-object v2, v0, LX/DGd;->a:LX/DGe;

    iput-object v1, v2, LX/DGe;->c:LX/1dl;

    .line 1980371
    move-object v0, v0

    .line 1980372
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1980373
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->i:LX/1Ua;

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 1980374
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 1980375
    :cond_1
    sget-object v0, LX/1dl;->CLICKABLE:LX/1dl;

    move-object v1, v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 1980376
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    monitor-enter v1

    .line 1980377
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980378
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980379
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980380
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980381
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/DGf;->a(LX/0QB;)LX/DGf;

    move-result-object v4

    check-cast v4, LX/DGf;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/DGf;LX/1V0;)V

    .line 1980382
    move-object v0, p0

    .line 1980383
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980384
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980385
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980386
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1980352
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1980351
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1980350
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1980348
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980349
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
