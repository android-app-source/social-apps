.class public Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGm;",
        "LX/DGl;",
        "LX/1PW;",
        "LX/DHm;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/17Y;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field public final d:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980527
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980528
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->a:Landroid/content/Context;

    .line 1980529
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->b:LX/17Y;

    .line 1980530
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1980531
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->d:LX/0ad;

    .line 1980532
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;
    .locals 7

    .prologue
    .line 1980553
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;

    monitor-enter v1

    .line 1980554
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980555
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980556
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980557
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980558
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v4

    check-cast v4, LX/17Y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;-><init>(Landroid/content/Context;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/0ad;)V

    .line 1980559
    move-object v0, p0

    .line 1980560
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980561
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980562
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980563
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1980564
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->b:LX/17Y;

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->a:Landroid/content/Context;

    sget-object v2, LX/0ax;->fb:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "referrer"

    sget-object v2, LX/BZf;->END_OF_HSCROLL:LX/BZf;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 1980565
    new-instance v1, LX/DGk;

    invoke-direct {v1, p0, v0}, LX/DGk;-><init>(Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;Landroid/content/Intent;)V

    .line 1980566
    new-instance v0, LX/DGl;

    invoke-direct {v0, v1}, LX/DGl;-><init>(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5bae19e2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1980536
    check-cast p1, LX/DGm;

    check-cast p2, LX/DGl;

    check-cast p4, LX/DHm;

    .line 1980537
    iget-object v1, p4, LX/DHm;->b:Lcom/facebook/resources/ui/FbTextView;

    move-object v1, v1

    .line 1980538
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->d:LX/0ad;

    sget-char v4, LX/Aow;->d:C

    const-string p3, "See More Apps"

    invoke-interface {v2, v4, p3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1980539
    iget-object v1, p4, LX/DHm;->c:Lcom/facebook/resources/ui/FbTextView;

    move-object v1, v1

    .line 1980540
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->d:LX/0ad;

    sget-char v4, LX/Aow;->a:C

    const-string p3, "Discover new apps for your phone."

    invoke-interface {v2, v4, p3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1980541
    iget-object v1, p4, LX/DHm;->d:Lcom/facebook/resources/ui/FbTextView;

    move-object v1, v1

    .line 1980542
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageAppInstallPartDefinition;->d:LX/0ad;

    sget-char v4, LX/Aow;->c:C

    const-string p3, "See More Apps"

    invoke-interface {v2, v4, p3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1980543
    iget-object v1, p2, LX/DGl;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/DHm;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1980544
    iget v1, p1, LX/DGm;->d:I

    move v1, v1

    .line 1980545
    invoke-virtual {p4}, LX/DHm;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0b0f30

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1980546
    iget-object v2, p4, LX/DHm;->e:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1980547
    if-eqz v2, :cond_0

    .line 1980548
    iput v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1980549
    iput v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 1980550
    :cond_0
    iget-object v2, p4, LX/DHm;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1980551
    iget-object v2, p4, LX/DHm;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1980552
    :cond_1
    const/16 v1, 0x1f

    const v2, -0x44a326cc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1980533
    check-cast p4, LX/DHm;

    .line 1980534
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, LX/DHm;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1980535
    return-void
.end method
