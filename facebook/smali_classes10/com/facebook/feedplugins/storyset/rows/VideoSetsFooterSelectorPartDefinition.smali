.class public Lcom/facebook/feedplugins/storyset/rows/VideoSetsFooterSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980890
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980891
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsFooterSelectorPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;

    .line 1980892
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1980893
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980894
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsFooterSelectorPartDefinition;->a:Lcom/facebook/feedplugins/storyset/rows/StorySetPageReactionsFooterPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980895
    const/4 v0, 0x0

    return-object v0
.end method
