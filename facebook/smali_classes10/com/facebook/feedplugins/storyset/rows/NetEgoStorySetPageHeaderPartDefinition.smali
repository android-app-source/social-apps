.class public Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGK;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/Bsr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/1VE;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/TextLinkPartDefinition;Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;LX/1VE;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/base/TextLinkPartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;",
            "Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;",
            "LX/1VE;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979565
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1979566
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->a:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    .line 1979567
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    .line 1979568
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    .line 1979569
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;

    .line 1979570
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    .line 1979571
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->f:LX/1VE;

    .line 1979572
    iput-object p7, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->g:LX/0Ot;

    .line 1979573
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1979574
    check-cast p2, LX/DGK;

    const/4 v6, -0x1

    .line 1979575
    iget-object v0, p2, LX/DGK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v0

    .line 1979576
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->a:Lcom/facebook/feedplugins/base/TextLinkPartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1979577
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->f:LX/1VE;

    invoke-virtual {v0, v1}, LX/1VE;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v0

    .line 1979578
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->f:LX/1VE;

    invoke-virtual {v2, v1}, LX/1VE;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v2

    .line 1979579
    const v3, 0x7f0d160d

    iget-object v4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    new-instance v5, LX/BsB;

    invoke-direct {v5, v1, v2, v0}, LX/BsB;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;II)V

    invoke-interface {p1, v3, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1979580
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1979581
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/185;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979582
    const v2, 0x7f0d166f

    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    invoke-interface {p1, v2, v0, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1979583
    :cond_0
    const v0, 0x7f0d1616

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    new-instance v3, LX/Bs9;

    .line 1979584
    iget-boolean v4, p2, LX/DGK;->e:Z

    move v4, v4

    .line 1979585
    invoke-direct {v3, v1, v4, v6}, LX/Bs9;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ZI)V

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1979586
    iget-boolean v0, p2, LX/DGK;->e:Z

    move v0, v0

    .line 1979587
    if-eqz v0, :cond_1

    .line 1979588
    const v0, 0x7f0d1615

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->e:Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    new-instance v3, LX/Bry;

    invoke-direct {v3, v1, v6}, LX/Bry;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1979589
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1979590
    :cond_1
    const v0, 0x7f0d1615

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPageHeaderPartDefinition;->d:Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;

    invoke-interface {p1, v0, v2, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method
