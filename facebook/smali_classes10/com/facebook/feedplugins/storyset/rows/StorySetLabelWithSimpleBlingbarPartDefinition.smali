.class public Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGh;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSimpleBlingbarView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980465
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980466
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980467
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980468
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;

    .line 1980469
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;
    .locals 6

    .prologue
    .line 1980470
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;

    monitor-enter v1

    .line 1980471
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980472
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980473
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980474
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980475
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;)V

    .line 1980476
    move-object v0, p0

    .line 1980477
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980478
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980479
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980480
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1980481
    check-cast p2, LX/DGh;

    .line 1980482
    const v0, 0x7f0d2dda

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/DGh;->a:Ljava/lang/CharSequence;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980483
    const v0, 0x7f0d2ddb

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/DGh;->b:Ljava/lang/CharSequence;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980484
    const v0, 0x7f0d2c70

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSimpleBlingbarPartDefinition;->c:Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;

    iget-object v2, p2, LX/DGh;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980485
    const/4 v0, 0x0

    return-object v0
.end method
