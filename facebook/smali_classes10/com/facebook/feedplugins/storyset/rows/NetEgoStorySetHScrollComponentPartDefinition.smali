.class public Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        "T::",
        "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TT;>;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/DGE;

.field private final f:LX/1V0;

.field private final g:LX/5Oq;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1979499
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/DGE;LX/1V0;LX/5Oq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979494
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1979495
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->e:LX/DGE;

    .line 1979496
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->f:LX/1V0;

    .line 1979497
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->g:LX/5Oq;

    .line 1979498
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pd;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<TT;>;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1979476
    new-instance v0, LX/1X6;

    invoke-static {}, LX/1UY;->e()LX/1UY;

    move-result-object v1

    invoke-virtual {v1}, LX/1UY;->h()LX/1UY;

    move-result-object v1

    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1X6;-><init>(LX/1Ua;)V

    .line 1979477
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->e:LX/DGE;

    const/4 v2, 0x0

    .line 1979478
    new-instance v3, LX/DGD;

    invoke-direct {v3, v1}, LX/DGD;-><init>(LX/DGE;)V

    .line 1979479
    iget-object v4, v1, LX/DGE;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DGC;

    .line 1979480
    if-nez v4, :cond_0

    .line 1979481
    new-instance v4, LX/DGC;

    invoke-direct {v4, v1}, LX/DGC;-><init>(LX/DGE;)V

    .line 1979482
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/DGC;->a$redex0(LX/DGC;LX/1De;IILX/DGD;)V

    .line 1979483
    move-object v3, v4

    .line 1979484
    move-object v2, v3

    .line 1979485
    move-object v1, v2

    .line 1979486
    iget-object v2, v1, LX/DGC;->a:LX/DGD;

    iput-object p2, v2, LX/DGD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979487
    iget-object v2, v1, LX/DGC;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1979488
    move-object v1, v1

    .line 1979489
    iget-object v2, v1, LX/DGC;->a:LX/DGD;

    iput-object p3, v2, LX/DGD;->a:LX/1Pd;

    .line 1979490
    iget-object v2, v1, LX/DGC;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1979491
    move-object v1, v1

    .line 1979492
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 1979493
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;
    .locals 7

    .prologue
    .line 1979465
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;

    monitor-enter v1

    .line 1979466
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979467
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979468
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979469
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979470
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/DGE;->a(LX/0QB;)LX/DGE;

    move-result-object v4

    check-cast v4, LX/DGE;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/5Oq;->a(LX/0QB;)LX/5Oq;

    move-result-object v6

    check-cast v6, LX/5Oq;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;-><init>(Landroid/content/Context;LX/DGE;LX/1V0;LX/5Oq;)V

    .line 1979471
    move-object v0, p0

    .line 1979472
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979473
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979474
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979475
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1979464
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pd;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pd;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1979463
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pd;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pd;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 1979451
    const/4 v0, 0x0

    move v0, v0

    .line 1979452
    return v0
.end method

.method public final synthetic a(LX/1Pn;)Z
    .locals 1

    .prologue
    .line 1979461
    const/4 v0, 0x0

    move v0, v0

    .line 1979462
    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1979456
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->g:LX/5Oq;

    .line 1979457
    iget-object v1, v0, LX/5Oq;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1979458
    iget-object v1, v0, LX/5Oq;->a:LX/0ad;

    sget-short p0, LX/1Dd;->j:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/5Oq;->b:Ljava/lang/Boolean;

    .line 1979459
    :cond_0
    iget-object v1, v0, LX/5Oq;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 1979460
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1979454
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979455
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 1979453
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetHScrollComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
