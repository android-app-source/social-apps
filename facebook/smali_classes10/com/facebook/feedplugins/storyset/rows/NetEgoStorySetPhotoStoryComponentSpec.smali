.class public Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final g:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/1nu;

.field public final b:LX/2yJ;

.field public final c:LX/2yN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2yN",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:LX/2mt;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C8h;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/C8d;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1979706
    const-class v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;

    const-string v1, "newsfeed_storyset_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->g:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/2mt;LX/2yJ;LX/C8d;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nu;",
            "LX/2mt;",
            "LX/2yJ;",
            "LX/C8d;",
            "LX/0Ot",
            "<",
            "LX/C8h;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1979708
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->a:LX/1nu;

    .line 1979709
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->b:LX/2yJ;

    .line 1979710
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->b:LX/2yJ;

    invoke-virtual {v0}, LX/2yJ;->a()LX/2yN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->c:LX/2yN;

    .line 1979711
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->d:LX/2mt;

    .line 1979712
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->e:LX/0Ot;

    .line 1979713
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->f:LX/C8d;

    .line 1979714
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;
    .locals 9

    .prologue
    .line 1979695
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;

    monitor-enter v1

    .line 1979696
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979697
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979698
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979699
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979700
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/2mt;->a(LX/0QB;)LX/2mt;

    move-result-object v5

    check-cast v5, LX/2mt;

    invoke-static {v0}, LX/2yJ;->a(LX/0QB;)LX/2yJ;

    move-result-object v6

    check-cast v6, LX/2yJ;

    invoke-static {v0}, LX/C8d;->a(LX/0QB;)LX/C8d;

    move-result-object v7

    check-cast v7, LX/C8d;

    const/16 v8, 0x200c

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;-><init>(LX/1nu;LX/2mt;LX/2yJ;LX/C8d;LX/0Ot;)V

    .line 1979701
    move-object v0, v3

    .line 1979702
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979703
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/NetEgoStorySetPhotoStoryComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979704
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979705
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
