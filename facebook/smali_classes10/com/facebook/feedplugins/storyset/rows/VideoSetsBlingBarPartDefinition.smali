.class public Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/154;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/154;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980783
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980784
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->a:Landroid/content/Context;

    .line 1980785
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->b:LX/154;

    .line 1980786
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980787
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;
    .locals 6

    .prologue
    .line 1980788
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;

    monitor-enter v1

    .line 1980789
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980790
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980791
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980792
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980793
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v4

    check-cast v4, LX/154;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;-><init>(Landroid/content/Context;LX/154;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 1980794
    move-object v0, p0

    .line 1980795
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980796
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980797
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980798
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(II)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1980799
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->b:LX/154;

    invoke-virtual {v0, p1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1980800
    if-lez p1, :cond_0

    if-lez p2, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, p2, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1980801
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1980802
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1980803
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {v1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    .line 1980804
    if-eqz v1, :cond_0

    .line 1980805
    const v2, 0x7f0f0063

    invoke-direct {p0, v1, v2}, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->a(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1980806
    :cond_0
    invoke-static {p2}, LX/0sa;->e(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v2

    .line 1980807
    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    .line 1980808
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1980809
    :cond_1
    if-eqz v2, :cond_2

    .line 1980810
    const v1, 0x7f0f00c2

    invoke-direct {p0, v2, v1}, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1980811
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/VideoSetsBlingBarPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980812
    const/4 v0, 0x0

    return-object v0
.end method
