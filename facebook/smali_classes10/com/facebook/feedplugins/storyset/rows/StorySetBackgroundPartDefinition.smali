.class public Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:LX/1Ua;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980090
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980091
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1980092
    invoke-static {}, LX/1UY;->e()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p2}, LX/1V7;->e()F

    move-result v2

    sub-float/2addr v1, v2

    .line 1980093
    iput v1, v0, LX/1UY;->c:F

    .line 1980094
    move-object v0, v0

    .line 1980095
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;->b:LX/1Ua;

    .line 1980096
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;
    .locals 5

    .prologue
    .line 1980097
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    monitor-enter v1

    .line 1980098
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980099
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980100
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980101
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980102
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;)V

    .line 1980103
    move-object v0, p0

    .line 1980104
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980105
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980106
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980107
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1980108
    const/4 v1, 0x0

    .line 1980109
    iget-object v6, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v0, LX/1X6;

    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetBackgroundPartDefinition;->b:LX/1Ua;

    const v3, 0x7f020ab0

    const/4 v4, -0x1

    sget-object v5, LX/1X9;->DIVIDER_BOTTOM:LX/1X9;

    invoke-direct/range {v0 .. v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;IILX/1X9;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1980110
    return-object v1
.end method
