.class public Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Landroid/text/Spannable;",
        "LX/1Pk;",
        "LX/DHn;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/1VF;

.field public final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field public final d:LX/3gQ;

.field public final e:LX/3gP;


# direct methods
.method public constructor <init>(LX/1VF;Landroid/content/Context;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/3gQ;LX/3gP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980600
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980601
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->a:LX/1VF;

    .line 1980602
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->b:Landroid/content/Context;

    .line 1980603
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 1980604
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->d:LX/3gQ;

    .line 1980605
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->e:LX/3gP;

    .line 1980606
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;
    .locals 9

    .prologue
    .line 1980607
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;

    monitor-enter v1

    .line 1980608
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980609
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980610
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980611
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980612
    new-instance v3, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v4

    check-cast v4, LX/1VF;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, LX/3gQ;->a(LX/0QB;)LX/3gQ;

    move-result-object v7

    check-cast v7, LX/3gQ;

    invoke-static {v0}, LX/3gP;->a(LX/0QB;)LX/3gP;

    move-result-object v8

    check-cast v8, LX/3gP;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;-><init>(LX/1VF;Landroid/content/Context;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/3gQ;LX/3gP;)V

    .line 1980613
    move-object v0, v3

    .line 1980614
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980615
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980616
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980617
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1980618
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980619
    const v0, 0x7f0d2ded

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v2, LX/24b;

    sget-object v3, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v2, p2, v3}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980620
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980621
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1980622
    invoke-static {v0}, LX/1VF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1980623
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->e:LX/3gP;

    invoke-virtual {v1, p2}, LX/3gP;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1980624
    :goto_0
    move-object v0, v0

    .line 1980625
    return-object v0

    .line 1980626
    :cond_0
    invoke-static {p2}, LX/182;->h(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStorySet;

    move-result-object v1

    .line 1980627
    if-eqz v1, :cond_1

    invoke-static {v1}, LX/39w;->c(Lcom/facebook/graphql/model/GraphQLStorySet;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1980628
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1980629
    :cond_2
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->d:LX/3gQ;

    invoke-virtual {v1, p2}, LX/3gQ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;

    move-result-object v1

    .line 1980630
    if-eqz v1, :cond_3

    move-object v0, v1

    .line 1980631
    goto :goto_0

    .line 1980632
    :cond_3
    const/4 v2, 0x0

    const/4 p2, 0x0

    .line 1980633
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    move-object v1, v2

    .line 1980634
    :goto_1
    move-object v0, v1

    .line 1980635
    goto :goto_0

    .line 1980636
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1980637
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x25d6af

    if-eq v3, v4, :cond_7

    :cond_6
    move-object v1, v2

    .line 1980638
    goto :goto_1

    .line 1980639
    :cond_7
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ae()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ae()Lcom/facebook/graphql/model/GraphQLPageLikersConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPageLikersConnection;->a()I

    move-result v2

    :goto_2
    move v2, v2

    .line 1980640
    new-instance v1, Landroid/text/SpannableString;

    iget-object v3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetPageSocialContextPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00b8

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, p1, p2

    invoke-virtual {v3, v4, v2, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_8
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x43656f09

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1980641
    check-cast p2, Landroid/text/Spannable;

    check-cast p4, LX/DHn;

    .line 1980642
    invoke-virtual {p4, p2}, LX/DHn;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 1980643
    const/16 v1, 0x1f

    const v2, -0x6e284baa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
