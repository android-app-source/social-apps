.class public Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGg;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageGenericLabelView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980412
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980413
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980414
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980415
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;
    .locals 5

    .prologue
    .line 1980416
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    monitor-enter v1

    .line 1980417
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980418
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980419
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980420
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980421
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 1980422
    move-object v0, p0

    .line 1980423
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980424
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980425
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980426
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1980427
    check-cast p2, LX/DGg;

    .line 1980428
    const v0, 0x7f0d2dda

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/DGg;->a:Ljava/lang/CharSequence;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980429
    const v0, 0x7f0d2ddb

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/DGg;->b:Ljava/lang/CharSequence;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980430
    const/4 v0, 0x0

    return-object v0
.end method
