.class public Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1dl;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final c:LX/1nu;

.field public final d:LX/1vb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1980391
    sget-object v0, LX/1dl;->CLICKABLE:LX/1dl;

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->a:LX/1dl;

    .line 1980392
    const-class v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;

    const-string v1, "newsfeed_video_storyset_header_icon"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/1vb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1980405
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->c:LX/1nu;

    .line 1980406
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->d:LX/1vb;

    .line 1980407
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;
    .locals 5

    .prologue
    .line 1980393
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;

    monitor-enter v1

    .line 1980394
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980395
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980396
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980397
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980398
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/1vb;->a(LX/0QB;)LX/1vb;

    move-result-object v4

    check-cast v4, LX/1vb;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;-><init>(LX/1nu;LX/1vb;)V

    .line 1980399
    move-object v0, p0

    .line 1980400
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980401
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetHeaderComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980402
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980403
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
