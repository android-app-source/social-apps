.class public Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/DGZ;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/DHS;

.field private final f:LX/1fv;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/DHS;LX/1fv;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1982052
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1982053
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->d:LX/1V0;

    .line 1982054
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->e:LX/DHS;

    .line 1982055
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->f:LX/1fv;

    .line 1982056
    return-void
.end method

.method private a(LX/1De;LX/DGZ;LX/1Pf;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/DGZ;",
            "LX/1Pf;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1982034
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->e:LX/DHS;

    const/4 v1, 0x0

    .line 1982035
    new-instance v2, LX/DHR;

    invoke-direct {v2, v0}, LX/DHR;-><init>(LX/DHS;)V

    .line 1982036
    iget-object v3, v0, LX/DHS;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DHQ;

    .line 1982037
    if-nez v3, :cond_0

    .line 1982038
    new-instance v3, LX/DHQ;

    invoke-direct {v3, v0}, LX/DHQ;-><init>(LX/DHS;)V

    .line 1982039
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/DHQ;->a$redex0(LX/DHQ;LX/1De;IILX/DHR;)V

    .line 1982040
    move-object v2, v3

    .line 1982041
    move-object v1, v2

    .line 1982042
    move-object v0, v1

    .line 1982043
    iget-object v1, v0, LX/DHQ;->a:LX/DHR;

    iput-object p2, v1, LX/DHR;->b:LX/DGZ;

    .line 1982044
    iget-object v1, v0, LX/DHQ;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1982045
    move-object v0, v0

    .line 1982046
    iget-object v1, v0, LX/DHQ;->a:LX/DHR;

    iput-object p3, v1, LX/DHR;->a:LX/1Pn;

    .line 1982047
    iget-object v1, v0, LX/DHQ;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1982048
    move-object v0, v0

    .line 1982049
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1982050
    new-instance v1, LX/1X6;

    iget-object v2, p2, LX/DGZ;->a:LX/99i;

    iget-object v2, v2, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v3, LX/1Ua;->q:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 1982051
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->d:LX/1V0;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;
    .locals 7

    .prologue
    .line 1982023
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;

    monitor-enter v1

    .line 1982024
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1982025
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1982026
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1982027
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1982028
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/DHS;->a(LX/0QB;)LX/DHS;

    move-result-object v5

    check-cast v5, LX/DHS;

    invoke-static {v0}, LX/1fv;->a(LX/0QB;)LX/1fv;

    move-result-object v6

    check-cast v6, LX/1fv;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/DHS;LX/1fv;)V

    .line 1982029
    move-object v0, p0

    .line 1982030
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1982031
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1982032
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1982033
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1982013
    check-cast p2, LX/DGZ;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->a(LX/1De;LX/DGZ;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1982022
    check-cast p2, LX/DGZ;

    check-cast p3, LX/1Pf;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->a(LX/1De;LX/DGZ;LX/1Pf;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1982016
    check-cast p1, LX/DGZ;

    .line 1982017
    iget-object v0, p1, LX/DGZ;->b:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->LINK_ONLY_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetAttachmentStyleHscrollComponentPartDefinition;->f:LX/1fv;

    .line 1982018
    iget-object v1, v0, LX/1fv;->f:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1982019
    iget-object v1, v0, LX/1fv;->a:LX/0ad;

    sget-short p0, LX/1fw;->a:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/1fv;->f:Ljava/lang/Boolean;

    .line 1982020
    :cond_0
    iget-object v1, v0, LX/1fv;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 1982021
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1982014
    check-cast p1, LX/DGZ;

    .line 1982015
    iget-object v0, p1, LX/DGZ;->a:LX/99i;

    iget-object v0, v0, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
