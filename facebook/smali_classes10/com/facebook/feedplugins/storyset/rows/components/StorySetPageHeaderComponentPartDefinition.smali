.class public Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pv;",
        ">",
        "Lcom/facebook/components/feed/EmbeddedComponentPartDefinition",
        "<",
        "LX/DGm;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final b:LX/DHb;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/DHb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1982211
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/EmbeddedComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1982212
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;->b:LX/DHb;

    .line 1982213
    return-void
.end method

.method private a(LX/1De;LX/DGm;LX/1Pb;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/DGm;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1982195
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;->b:LX/DHb;

    const/4 v1, 0x0

    .line 1982196
    new-instance v2, LX/DHa;

    invoke-direct {v2, v0}, LX/DHa;-><init>(LX/DHb;)V

    .line 1982197
    iget-object p0, v0, LX/DHb;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DHZ;

    .line 1982198
    if-nez p0, :cond_0

    .line 1982199
    new-instance p0, LX/DHZ;

    invoke-direct {p0, v0}, LX/DHZ;-><init>(LX/DHb;)V

    .line 1982200
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/DHZ;->a$redex0(LX/DHZ;LX/1De;IILX/DHa;)V

    .line 1982201
    move-object v2, p0

    .line 1982202
    move-object v1, v2

    .line 1982203
    move-object v0, v1

    .line 1982204
    iget-object v1, v0, LX/DHZ;->a:LX/DHa;

    iput-object p3, v1, LX/DHa;->b:LX/1Pb;

    .line 1982205
    iget-object v1, v0, LX/DHZ;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1982206
    move-object v0, v0

    .line 1982207
    iget-object v1, v0, LX/DHZ;->a:LX/DHa;

    iput-object p2, v1, LX/DHa;->a:LX/DGm;

    .line 1982208
    iget-object v1, v0, LX/DHZ;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1982209
    move-object v0, v0

    .line 1982210
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;
    .locals 5

    .prologue
    .line 1982184
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;

    monitor-enter v1

    .line 1982185
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1982186
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1982187
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1982188
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1982189
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/DHb;->a(LX/0QB;)LX/DHb;

    move-result-object v4

    check-cast v4, LX/DHb;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/DHb;)V

    .line 1982190
    move-object v0, p0

    .line 1982191
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1982192
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1982193
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1982194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1982183
    check-cast p2, LX/DGm;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;->a(LX/1De;LX/DGm;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1982182
    check-cast p2, LX/DGm;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/StorySetPageHeaderComponentPartDefinition;->a(LX/1De;LX/DGm;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method
