.class public Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pv;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static p:LX/0Xm;


# instance fields
.field public final c:Landroid/content/Context;

.field public final d:LX/1nu;

.field public final e:LX/1DR;

.field public final f:LX/DH0;

.field public final g:LX/DGp;

.field public final h:LX/2yJ;

.field public final i:LX/C8d;

.field public final j:LX/2yN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2yN",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation
.end field

.field public final k:LX/1Kf;

.field public final l:LX/03V;

.field public final m:LX/0kL;

.field private final n:LX/2yK;

.field public final o:LX/5up;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1981056
    const-class v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->a:Ljava/lang/String;

    .line 1981057
    const-class v0, Lcom/facebook/feedplugins/storyset/rows/StorySetAttachmentImagePartDefinition;

    const-string v1, "newsfeed_storyset_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1nu;LX/1DR;LX/DH0;LX/DGp;LX/2yJ;LX/C8d;LX/1Kf;LX/03V;LX/0kL;LX/2yK;LX/5up;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1981058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1981059
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->c:Landroid/content/Context;

    .line 1981060
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->d:LX/1nu;

    .line 1981061
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->e:LX/1DR;

    .line 1981062
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->f:LX/DH0;

    .line 1981063
    iput-object p5, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->g:LX/DGp;

    .line 1981064
    iput-object p6, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->h:LX/2yJ;

    .line 1981065
    iput-object p7, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->i:LX/C8d;

    .line 1981066
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->h:LX/2yJ;

    invoke-virtual {v0}, LX/2yJ;->a()LX/2yN;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->j:LX/2yN;

    .line 1981067
    iput-object p8, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->k:LX/1Kf;

    .line 1981068
    iput-object p9, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->l:LX/03V;

    .line 1981069
    iput-object p10, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->m:LX/0kL;

    .line 1981070
    iput-object p11, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->n:LX/2yK;

    .line 1981071
    iput-object p12, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->o:LX/5up;

    .line 1981072
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;
    .locals 3

    .prologue
    .line 1981073
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    monitor-enter v1

    .line 1981074
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981075
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981076
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981077
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->b(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981078
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981079
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981080
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;Lcom/facebook/feed/rows/core/props/FeedProps;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1981081
    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1981082
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;->n:LX/2yK;

    sget-object v3, LX/0ig;->z:LX/0ih;

    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "position:"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStorySet;->I_()I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v0, p2, v1}, LX/2yK;->a(LX/0ih;Lcom/facebook/graphql/model/GraphQLStorySet;ILjava/lang/String;)V

    .line 1981083
    :cond_0
    return-void
.end method

.method private static b(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;
    .locals 13

    .prologue
    .line 1981084
    new-instance v0, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v2

    check-cast v2, LX/1nu;

    invoke-static {p0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v3

    check-cast v3, LX/1DR;

    invoke-static {p0}, LX/DH0;->a(LX/0QB;)LX/DH0;

    move-result-object v4

    check-cast v4, LX/DH0;

    invoke-static {p0}, LX/DGp;->a(LX/0QB;)LX/DGp;

    move-result-object v5

    check-cast v5, LX/DGp;

    invoke-static {p0}, LX/2yJ;->a(LX/0QB;)LX/2yJ;

    move-result-object v6

    check-cast v6, LX/2yJ;

    invoke-static {p0}, LX/C8d;->a(LX/0QB;)LX/C8d;

    move-result-object v7

    check-cast v7, LX/C8d;

    invoke-static {p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v8

    check-cast v8, LX/1Kf;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-static {p0}, LX/2yK;->b(LX/0QB;)LX/2yK;

    move-result-object v11

    check-cast v11, LX/2yK;

    invoke-static {p0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v12

    check-cast v12, LX/5up;

    invoke-direct/range {v0 .. v12}, Lcom/facebook/feedplugins/storyset/rows/components/LinkSetsPageItemComponentSpec;-><init>(Landroid/content/Context;LX/1nu;LX/1DR;LX/DH0;LX/DGp;LX/2yJ;LX/C8d;LX/1Kf;LX/03V;LX/0kL;LX/2yK;LX/5up;)V

    .line 1981085
    return-object v0
.end method
