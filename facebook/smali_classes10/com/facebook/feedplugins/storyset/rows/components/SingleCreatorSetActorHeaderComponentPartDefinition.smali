.class public Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/1VE;

.field private final e:LX/1V0;

.field private final f:LX/DH9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DH9",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1VE;LX/1V0;LX/DH9;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1981436
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1981437
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->d:LX/1VE;

    .line 1981438
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->e:LX/1V0;

    .line 1981439
    iput-object p4, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->f:LX/DH9;

    .line 1981440
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1981441
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1981442
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1981443
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1981444
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1981445
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->f:LX/DH9;

    const/4 v2, 0x0

    .line 1981446
    new-instance v3, LX/DH8;

    invoke-direct {v3, v1}, LX/DH8;-><init>(LX/DH9;)V

    .line 1981447
    iget-object p2, v1, LX/DH9;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/DH7;

    .line 1981448
    if-nez p2, :cond_0

    .line 1981449
    new-instance p2, LX/DH7;

    invoke-direct {p2, v1}, LX/DH7;-><init>(LX/DH9;)V

    .line 1981450
    :cond_0
    invoke-static {p2, p1, v2, v2, v3}, LX/DH7;->a$redex0(LX/DH7;LX/1De;IILX/DH8;)V

    .line 1981451
    move-object v3, p2

    .line 1981452
    move-object v2, v3

    .line 1981453
    move-object v1, v2

    .line 1981454
    iget-object v2, v1, LX/DH7;->a:LX/DH8;

    iput-object v0, v2, LX/DH8;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981455
    iget-object v2, v1, LX/DH7;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1981456
    move-object v1, v1

    .line 1981457
    iget-object v2, v1, LX/DH7;->a:LX/DH8;

    iput-object p3, v2, LX/DH8;->b:LX/1Pb;

    .line 1981458
    iget-object v2, v1, LX/DH7;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1981459
    move-object v1, v1

    .line 1981460
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 1981461
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->d:LX/1VE;

    invoke-virtual {v2, v0}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X6;

    move-result-object v0

    .line 1981462
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;
    .locals 7

    .prologue
    .line 1981463
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;

    monitor-enter v1

    .line 1981464
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981465
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981466
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981467
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981468
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v4

    check-cast v4, LX/1VE;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/DH9;->a(LX/0QB;)LX/DH9;

    move-result-object v6

    check-cast v6, LX/DH9;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1VE;LX/1V0;LX/DH9;)V

    .line 1981469
    move-object v0, p0

    .line 1981470
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981471
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981472
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981473
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1981474
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1981475
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetActorHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1981476
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1981477
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981478
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
