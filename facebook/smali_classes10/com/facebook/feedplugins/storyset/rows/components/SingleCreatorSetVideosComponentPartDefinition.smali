.class public Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/DHO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DHO",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/DHO;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1981873
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1981874
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;->d:LX/1V0;

    .line 1981875
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;->e:LX/DHO;

    .line 1981876
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1981855
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;->e:LX/DHO;

    const/4 v1, 0x0

    .line 1981856
    new-instance v2, LX/DHN;

    invoke-direct {v2, v0}, LX/DHN;-><init>(LX/DHO;)V

    .line 1981857
    iget-object v4, v0, LX/DHO;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DHM;

    .line 1981858
    if-nez v4, :cond_0

    .line 1981859
    new-instance v4, LX/DHM;

    invoke-direct {v4, v0}, LX/DHM;-><init>(LX/DHO;)V

    .line 1981860
    :cond_0
    invoke-static {v4, p1, v1, v1, v2}, LX/DHM;->a$redex0(LX/DHM;LX/1De;IILX/DHN;)V

    .line 1981861
    move-object v2, v4

    .line 1981862
    move-object v1, v2

    .line 1981863
    move-object v0, v1

    .line 1981864
    iget-object v1, v0, LX/DHM;->a:LX/DHN;

    iput-object p2, v1, LX/DHN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981865
    iget-object v1, v0, LX/DHM;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1981866
    move-object v0, v0

    .line 1981867
    iget-object v1, v0, LX/DHM;->a:LX/DHN;

    iput-object p3, v1, LX/DHN;->b:LX/1Pe;

    .line 1981868
    iget-object v1, v0, LX/DHM;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1981869
    move-object v0, v0

    .line 1981870
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1981871
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v1, v3, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 1981872
    iget-object v2, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;->d:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;
    .locals 6

    .prologue
    .line 1981844
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;

    monitor-enter v1

    .line 1981845
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981846
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981847
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981848
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981849
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/DHO;->a(LX/0QB;)LX/DHO;

    move-result-object v5

    check-cast v5, LX/DHO;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/DHO;)V

    .line 1981850
    move-object v0, p0

    .line 1981851
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981852
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981853
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981854
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1981843
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1981842
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetVideosComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1981839
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1981840
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981841
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
