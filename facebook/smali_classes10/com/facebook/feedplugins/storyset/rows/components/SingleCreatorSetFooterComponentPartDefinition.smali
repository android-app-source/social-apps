.class public Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/DHF;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/DHF;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1981637
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1981638
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;->d:LX/DHF;

    .line 1981639
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStorySet;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1981620
    iget-object v0, p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;->d:LX/DHF;

    const/4 v1, 0x0

    .line 1981621
    new-instance v2, LX/DHE;

    invoke-direct {v2, v0}, LX/DHE;-><init>(LX/DHF;)V

    .line 1981622
    iget-object p0, v0, LX/DHF;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/DHD;

    .line 1981623
    if-nez p0, :cond_0

    .line 1981624
    new-instance p0, LX/DHD;

    invoke-direct {p0, v0}, LX/DHD;-><init>(LX/DHF;)V

    .line 1981625
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/DHD;->a$redex0(LX/DHD;LX/1De;IILX/DHE;)V

    .line 1981626
    move-object v2, p0

    .line 1981627
    move-object v1, v2

    .line 1981628
    move-object v0, v1

    .line 1981629
    iget-object v1, v0, LX/DHD;->a:LX/DHE;

    iput-object p2, v1, LX/DHE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981630
    iget-object v1, v0, LX/DHD;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1981631
    move-object v0, v0

    .line 1981632
    check-cast p3, LX/1Po;

    .line 1981633
    iget-object v1, v0, LX/DHD;->a:LX/DHE;

    iput-object p3, v1, LX/DHE;->b:LX/1Po;

    .line 1981634
    iget-object v1, v0, LX/DHD;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1981635
    move-object v0, v0

    .line 1981636
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;
    .locals 5

    .prologue
    .line 1981609
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;

    monitor-enter v1

    .line 1981610
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1981611
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1981612
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981613
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1981614
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/DHF;->a(LX/0QB;)LX/DHF;

    move-result-object v4

    check-cast v4, LX/DHF;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;-><init>(Landroid/content/Context;LX/DHF;)V

    .line 1981615
    move-object v0, p0

    .line 1981616
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1981617
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1981618
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1981619
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1981640
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1981608
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/storyset/rows/components/SingleCreatorSetFooterComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1981607
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1981605
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1981606
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
