.class public Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DGj;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/storyset/rows/ui/StoryPageLabelWithSocialContextView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980511
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1980512
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980513
    iput-object p2, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980514
    iput-object p3, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 1980515
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1980516
    check-cast p2, LX/DGj;

    .line 1980517
    const v0, 0x7f0d2dda

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/DGj;->a:Ljava/lang/CharSequence;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980518
    const v0, 0x7f0d2ddb

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;->b:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/DGj;->b:Ljava/lang/CharSequence;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980519
    const v0, 0x7f0d2ddd

    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetLabelWithSocialContextPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v2, p2, LX/DGj;->c:Ljava/lang/CharSequence;

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1980520
    const/4 v0, 0x0

    return-object v0
.end method
