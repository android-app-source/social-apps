.class public Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1980709
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1980710
    iput-object p1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;->a:Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    .line 1980711
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;
    .locals 4

    .prologue
    .line 1980712
    const-class v1, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;

    monitor-enter v1

    .line 1980713
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1980714
    sput-object v2, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1980715
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1980716
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1980717
    new-instance p0, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;)V

    .line 1980718
    move-object v0, p0

    .line 1980719
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1980720
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1980721
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1980722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1980702
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1980703
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980704
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1980705
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1980706
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1980707
    iget-object v1, p0, Lcom/facebook/feedplugins/storyset/rows/StorySetSingleStoryGroupPartDefinition;->a:Lcom/facebook/feed/rows/sections/BasicGroupPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1980708
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1980697
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x1

    .line 1980698
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980699
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {v0}, LX/39w;->b(Lcom/facebook/graphql/model/GraphQLStorySet;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 1980700
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1980701
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySet;->y()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;->FIRST_VIDEO_STORIES:Lcom/facebook/graphql/enums/GraphQLStorySetCollectionType;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
