.class public final Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1977081
    const-class v0, Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1977080
    const-class v0, Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1977078
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1977079
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1977076
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel;->e:Ljava/lang/String;

    .line 1977077
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1977070
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1977071
    invoke-direct {p0}, Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1977072
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1977073
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1977074
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1977075
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1977062
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1977063
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1977064
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1977067
    new-instance v0, Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel;

    invoke-direct {v0}, Lcom/facebook/feedplugins/gysc/protocols/blacklistGroupsYouShouldCreateMutationModels$GroupsYouShouldCreateXoutMutationModel;-><init>()V

    .line 1977068
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1977069
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1977066
    const v0, -0x1e05b475

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1977065
    const v0, 0xef60ef1

    return v0
.end method
