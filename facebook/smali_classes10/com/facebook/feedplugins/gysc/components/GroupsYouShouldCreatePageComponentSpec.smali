.class public Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field public final b:LX/DE0;

.field public final c:Landroid/content/res/Resources;

.field private final d:LX/1vg;

.field public final e:LX/8yS;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1976652
    const-class v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/DE0;LX/8yS;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1vg;Landroid/content/res/Resources;)V
    .locals 0
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p6    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DE0;",
            "LX/8yS;",
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/1vg;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1976654
    iput-object p8, p0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->c:Landroid/content/res/Resources;

    .line 1976655
    iput-object p2, p0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->e:LX/8yS;

    .line 1976656
    iput-object p1, p0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->b:LX/DE0;

    .line 1976657
    iput-object p4, p0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->f:LX/0Ot;

    .line 1976658
    iput-object p5, p0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->g:LX/0Ot;

    .line 1976659
    iput-object p6, p0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->h:LX/0Ot;

    .line 1976660
    iput-object p7, p0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->d:LX/1vg;

    .line 1976661
    iput-object p3, p0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->i:LX/0Ot;

    .line 1976662
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;
    .locals 12

    .prologue
    .line 1976663
    const-class v1, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;

    monitor-enter v1

    .line 1976664
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976665
    sput-object v2, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976666
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976667
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976668
    new-instance v3, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;

    invoke-static {v0}, LX/DE0;->a(LX/0QB;)LX/DE0;

    move-result-object v4

    check-cast v4, LX/DE0;

    invoke-static {v0}, LX/8yS;->a(LX/0QB;)LX/8yS;

    move-result-object v5

    check-cast v5, LX/8yS;

    const/16 v6, 0x15e7

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x115

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xafd

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1430

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v10

    check-cast v10, LX/1vg;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v11

    check-cast v11, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;-><init>(LX/DE0;LX/8yS;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/1vg;Landroid/content/res/Resources;)V

    .line 1976669
    move-object v0, v3

    .line 1976670
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976671
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976672
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976673
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;LX/1De;)LX/1Di;
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x1

    .line 1976674
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreatePageComponentSpec;->d:LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    const v1, 0x7f02081a

    invoke-virtual {v0, v1}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    const v1, -0x6e685d

    invoke-virtual {v0, v1}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 1976675
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    .line 1976676
    const v1, 0x49b238bd    # 1459991.6f

    const/4 p0, 0x0

    invoke-static {p1, v1, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v1, v1

    .line 1976677
    invoke-interface {v0, v1}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v2, v3}, LX/1Di;->m(II)LX/1Di;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1, v3}, LX/1Di;->m(II)LX/1Di;

    move-result-object v0

    return-object v0
.end method
