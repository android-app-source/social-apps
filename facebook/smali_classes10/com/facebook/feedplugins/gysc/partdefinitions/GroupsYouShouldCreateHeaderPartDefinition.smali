.class public Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/2ee;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/1Ua;

.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976827
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1976828
    iput-object p1, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1976829
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    invoke-virtual {p2}, LX/1V7;->d()F

    move-result v1

    neg-float v1, v1

    .line 1976830
    iput v1, v0, LX/1UY;->b:F

    .line 1976831
    move-object v0, v0

    .line 1976832
    invoke-virtual {p2}, LX/1V7;->c()F

    move-result v1

    .line 1976833
    iput v1, v0, LX/1UY;->c:F

    .line 1976834
    move-object v0, v0

    .line 1976835
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;->a:LX/1Ua;

    .line 1976836
    iput-object p3, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;->c:Landroid/content/res/Resources;

    .line 1976837
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;
    .locals 6

    .prologue
    .line 1976816
    const-class v1, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;

    monitor-enter v1

    .line 1976817
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976818
    sput-object v2, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976819
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976820
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976821
    new-instance p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;Landroid/content/res/Resources;)V

    .line 1976822
    move-object v0, p0

    .line 1976823
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976824
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976825
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976826
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1976838
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1976813
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976814
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1976815
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x39d11815

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1976794
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p4, LX/2ee;

    .line 1976795
    sget-object v1, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    invoke-virtual {p4, v1}, LX/2ee;->setStyle(LX/2ei;)V

    .line 1976796
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1976797
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1976798
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1976799
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1976800
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1976801
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 1976802
    :goto_0
    sget-object v2, LX/2ej;->NOT_SPONSORED:LX/2ej;

    invoke-virtual {p4, v1, v2}, LX/2ee;->a(Ljava/lang/CharSequence;LX/2ej;)V

    .line 1976803
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/2ee;->setMenuButtonActive(Z)V

    .line 1976804
    const/16 v1, 0x1f

    const v2, 0x2f20d4b4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1976805
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;->c:Landroid/content/res/Resources;

    const v2, 0x7f081865

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1976806
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976807
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1976808
    if-nez v0, :cond_0

    .line 1976809
    const/4 v0, 0x0

    .line 1976810
    :goto_0
    return v0

    .line 1976811
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1976812
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method
