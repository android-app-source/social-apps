.class public Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3VK;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public f:LX/17Y;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1976683
    invoke-static {}, LX/1UY;->g()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    .line 1976684
    iput v1, v0, LX/1UY;->b:F

    .line 1976685
    move-object v0, v0

    .line 1976686
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976687
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1976688
    iput-object p1, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1976689
    iput-object p2, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1976690
    iput-object p3, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    .line 1976691
    iput-object p4, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1976692
    iput-object p5, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->f:LX/17Y;

    .line 1976693
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;
    .locals 9

    .prologue
    .line 1976694
    const-class v1, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;

    monitor-enter v1

    .line 1976695
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976696
    sput-object v2, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976697
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976698
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976699
    new-instance v3, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/17Y;)V

    .line 1976700
    move-object v0, v3

    .line 1976701
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976702
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976703
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976704
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/3VK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1976705
    sget-object v0, LX/3VK;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1976706
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976707
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1976708
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1976709
    new-instance v1, LX/DEJ;

    invoke-direct {v1, p0}, LX/DEJ;-><init>(Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;)V

    move-object v1, v1

    .line 1976710
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1976711
    const v0, 0x7f0d175f

    iget-object v1, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    const v2, 0x7f081866

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1976712
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1976713
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976714
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1976715
    if-nez v0, :cond_0

    .line 1976716
    const/4 v0, 0x0

    .line 1976717
    :goto_0
    return v0

    .line 1976718
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1976719
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method
