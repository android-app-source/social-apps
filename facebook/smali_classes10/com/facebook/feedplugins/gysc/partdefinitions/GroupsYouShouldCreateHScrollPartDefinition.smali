.class public Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field public final a:F

.field public final b:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:LX/2dq;

.field private final f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/1LV;

.field private final h:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1976766
    sget-object v0, LX/2eF;->a:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->c:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1LV;Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976729
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1976730
    iput-object p1, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1976731
    iput-object p2, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->e:LX/2dq;

    .line 1976732
    iput-object p3, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 1976733
    iput-object p4, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->g:LX/1LV;

    .line 1976734
    iput-object p5, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->b:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;

    .line 1976735
    iput-object p6, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->h:Landroid/content/res/Resources;

    .line 1976736
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->h:Landroid/content/res/Resources;

    const v1, 0x7f0b0f4e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget-object v1, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->h:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    const/high16 v1, 0x41000000    # 8.0f

    add-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->a:F

    .line 1976737
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;
    .locals 10

    .prologue
    .line 1976755
    const-class v1, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

    monitor-enter v1

    .line 1976756
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976757
    sput-object v2, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976758
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976759
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976760
    new-instance v3, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v5

    check-cast v5, LX/2dq;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v7

    check-cast v7, LX/1LV;

    invoke-static {v0}, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v9

    check-cast v9, Landroid/content/res/Resources;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/2dq;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/1LV;Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;Landroid/content/res/Resources;)V

    .line 1976761
    move-object v0, v3

    .line 1976762
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976763
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976764
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976765
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1976754
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1976745
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976746
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 1976747
    check-cast v5, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    .line 1976748
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->c:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1976749
    iget-object v6, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->f:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v1, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->e:LX/2dq;

    iget v2, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->a:F

    sget-object v3, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->c:LX/1Ua;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->I_()I

    move-result v2

    .line 1976750
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k()LX/0Px;

    move-result-object v3

    .line 1976751
    new-instance v4, LX/DEK;

    invoke-direct {v4, p0, v3, v5}, LX/DEK;-><init>(Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;LX/0Px;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;)V

    move-object v3, v4

    .line 1976752
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1976753
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1976738
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976739
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1976740
    if-nez v0, :cond_0

    .line 1976741
    const/4 v0, 0x0

    .line 1976742
    :goto_0
    return v0

    .line 1976743
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1976744
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    goto :goto_0
.end method
