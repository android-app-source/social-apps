.class public Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

.field private final c:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;

.field public final d:LX/0Uh;

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;LX/0Uh;Ljava/lang/Boolean;)V
    .locals 1
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976767
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1976768
    iput-object p1, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->a:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;

    .line 1976769
    iput-object p2, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->b:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

    .line 1976770
    iput-object p3, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->c:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;

    .line 1976771
    iput-object p4, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->d:LX/0Uh;

    .line 1976772
    invoke-virtual {p5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->e:Z

    .line 1976773
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;
    .locals 9

    .prologue
    .line 1976774
    const-class v1, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;

    monitor-enter v1

    .line 1976775
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976776
    sput-object v2, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976777
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976778
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976779
    new-instance v3, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;-><init>(Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;LX/0Uh;Ljava/lang/Boolean;)V

    .line 1976780
    move-object v0, v3

    .line 1976781
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976782
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976783
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976784
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1976785
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v2, 0x0

    .line 1976786
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->d:LX/0Uh;

    const/16 v1, 0x3c7

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    move v0, v0

    .line 1976787
    if-nez v0, :cond_1

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->FEED:LX/1Qt;

    if-ne v0, v1, :cond_1

    .line 1976788
    :cond_0
    :goto_0
    return-object v2

    .line 1976789
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->a:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1976790
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->b:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1976791
    iget-boolean v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->e:Z

    if-nez v0, :cond_0

    .line 1976792
    iget-object v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;->c:Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1976793
    const/4 v0, 0x1

    return v0
.end method
