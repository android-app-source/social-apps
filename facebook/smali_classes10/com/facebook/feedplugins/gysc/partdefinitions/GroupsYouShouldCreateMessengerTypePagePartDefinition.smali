.class public Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DEP;",
        "LX/DER;",
        "LX/1Pf;",
        "LX/DEX;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/DEX;",
            ">;"
        }
    .end annotation
.end field

.field private static k:LX/0Xm;


# instance fields
.field private final b:LX/17Q;

.field private final c:LX/0Zb;

.field private final d:LX/17W;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field private final h:LX/0tX;

.field private final i:Ljava/util/concurrent/ExecutorService;

.field private final j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1976841
    new-instance v0, LX/DEL;

    invoke-direct {v0}, LX/DEL;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/17W;LX/17Q;LX/0Zb;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0tX;Ljava/lang/Boolean;)V
    .locals 1
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p9    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976842
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 1976843
    iput-object p1, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->e:Landroid/content/Context;

    .line 1976844
    iput-object p2, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->d:LX/17W;

    .line 1976845
    iput-object p3, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->b:LX/17Q;

    .line 1976846
    iput-object p4, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->c:LX/0Zb;

    .line 1976847
    iput-object p6, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->f:Ljava/lang/String;

    .line 1976848
    iput-object p7, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->g:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 1976849
    iput-object p8, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->h:LX/0tX;

    .line 1976850
    iput-object p5, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->i:Ljava/util/concurrent/ExecutorService;

    .line 1976851
    invoke-virtual {p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->j:Z

    .line 1976852
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;
    .locals 13

    .prologue
    .line 1976853
    const-class v1, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;

    monitor-enter v1

    .line 1976854
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976855
    sput-object v2, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976856
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976857
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976858
    new-instance v3, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v6

    check-cast v6, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v10

    check-cast v10, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v12

    check-cast v12, Ljava/lang/Boolean;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;-><init>(Landroid/content/Context;LX/17W;LX/17Q;LX/0Zb;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/0tX;Ljava/lang/Boolean;)V

    .line 1976859
    move-object v0, v3

    .line 1976860
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976861
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976862
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976863
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/DEX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1976864
    sget-object v0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1976865
    check-cast p2, LX/DEP;

    check-cast p3, LX/1Pf;

    const/4 v10, 0x0

    .line 1976866
    iget-object v1, p2, LX/DEP;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    .line 1976867
    iget-object v0, p2, LX/DEP;->a:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    iget-object v2, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->d:LX/17W;

    iget-object v3, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->e:Landroid/content/Context;

    iget-object v4, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->b:LX/17Q;

    iget-object v5, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->c:LX/0Zb;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v6

    invoke-interface {v6}, LX/1PT;->a()LX/1Qt;

    move-result-object v6

    iget-boolean v7, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->j:Z

    invoke-static/range {v0 .. v7}, LX/DES;->a(Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;LX/17W;Landroid/content/Context;LX/17Q;LX/0Zb;LX/1Qt;Z)Landroid/view/View$OnClickListener;

    move-result-object v9

    .line 1976868
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->f:Ljava/lang/String;

    iget-object v4, p2, LX/DEP;->a:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    iget-object v5, p2, LX/DEP;->b:Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;

    iget-object v6, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->h:LX/0tX;

    iget-object v7, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->g:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    iget-object v8, p0, Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateMessengerTypePagePartDefinition;->i:Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v2 .. v8}, LX/DES;->a(LX/1Qt;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;LX/0tX;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Ljava/util/concurrent/ExecutorService;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 1976869
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    .line 1976870
    const/4 v0, 0x0

    .line 1976871
    if-lez v3, :cond_0

    .line 1976872
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1976873
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v0

    .line 1976874
    :cond_0
    new-instance v4, LX/DEQ;

    invoke-direct {v4}, LX/DEQ;-><init>()V

    move-object v4, v4

    .line 1976875
    iput-object v9, v4, LX/DEQ;->a:Landroid/view/View$OnClickListener;

    .line 1976876
    move-object v5, v4

    .line 1976877
    iput-object v2, v5, LX/DEQ;->b:Landroid/view/View$OnClickListener;

    .line 1976878
    move-object v2, v5

    .line 1976879
    iput v3, v2, LX/DEQ;->d:I

    .line 1976880
    move-object v2, v2

    .line 1976881
    iput-object v0, v2, LX/DEQ;->f:Ljava/lang/String;

    .line 1976882
    move-object v0, v2

    .line 1976883
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 1976884
    iput v2, v0, LX/DEQ;->e:I

    .line 1976885
    move-object v0, v0

    .line 1976886
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->a()Ljava/lang/String;

    move-result-object v2

    .line 1976887
    iput-object v2, v0, LX/DEQ;->g:Ljava/lang/String;

    .line 1976888
    move-object v0, v0

    .line 1976889
    const/4 v2, 0x0

    .line 1976890
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1976891
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1976892
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnitItem;->j()Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroupCreationSuggestionDefaultMembersConnection;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v2

    move v3, v2

    :goto_0
    if-ge v5, v8, :cond_1

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLUser;

    .line 1976893
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 1976894
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1976895
    add-int/lit8 v2, v3, 0x1

    .line 1976896
    :goto_1
    const/4 v3, 0x5

    if-ge v2, v3, :cond_1

    .line 1976897
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    goto :goto_0

    .line 1976898
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 1976899
    iput-object v1, v0, LX/DEQ;->h:LX/0Px;

    .line 1976900
    new-instance v0, LX/DER;

    invoke-direct {v0, v4}, LX/DER;-><init>(LX/DEQ;)V

    move-object v0, v0

    .line 1976901
    return-object v0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x71bc34fb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1976902
    check-cast p2, LX/DER;

    check-cast p4, LX/DEX;

    .line 1976903
    iget-object v1, p2, LX/DER;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/DEX;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1976904
    iget-object v1, p2, LX/DER;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/DEX;->setButtonOnclickListener(Landroid/view/View$OnClickListener;)V

    .line 1976905
    iget-object v1, p2, LX/DER;->g:Ljava/lang/String;

    invoke-virtual {p4, v1}, LX/DEX;->setSuggestGroupName(Ljava/lang/String;)V

    .line 1976906
    iget v1, p2, LX/DER;->d:I

    iget-object v2, p2, LX/DER;->f:Ljava/lang/String;

    invoke-virtual {p4, v1, v2}, LX/DEX;->a(ILjava/lang/String;)V

    .line 1976907
    iget-object v1, p2, LX/DER;->h:LX/0Px;

    iget v2, p2, LX/DER;->e:I

    invoke-virtual {p4, v1, v2}, LX/DEX;->a(LX/0Px;I)V

    .line 1976908
    iget-object v1, p2, LX/DER;->b:Landroid/view/View$OnClickListener;

    .line 1976909
    iget-object v2, p4, LX/DEX;->f:Landroid/view/View;

    if-nez v2, :cond_0

    .line 1976910
    const v2, 0x7f0d157a

    invoke-virtual {p4, v2}, LX/DEX;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p4, LX/DEX;->f:Landroid/view/View;

    .line 1976911
    iget-object v2, p4, LX/DEX;->f:Landroid/view/View;

    const/4 p2, 0x0

    invoke-virtual {v2, p2}, Landroid/view/View;->setVisibility(I)V

    .line 1976912
    :cond_0
    iget-object v2, p4, LX/DEX;->f:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1976913
    const/16 v1, 0x1f

    const v2, -0x6680e833

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1976914
    check-cast p4, LX/DEX;

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1976915
    invoke-virtual {p4, v0}, LX/DEX;->setButtonOnclickListener(Landroid/view/View$OnClickListener;)V

    .line 1976916
    invoke-virtual {p4, v0}, LX/DEX;->setSuggestGroupName(Ljava/lang/String;)V

    .line 1976917
    invoke-virtual {p4, v1, v0}, LX/DEX;->a(ILjava/lang/String;)V

    .line 1976918
    invoke-virtual {p4, v0, v1}, LX/DEX;->a(LX/0Px;I)V

    .line 1976919
    return-void
.end method
