.class public final Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuMutationsModels$CrowdsourcingTofuLoggingMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuMutationsModels$CrowdsourcingTofuLoggingMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1974324
    const-class v0, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuMutationsModels$CrowdsourcingTofuLoggingMutationModel;

    new-instance v1, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuMutationsModels$CrowdsourcingTofuLoggingMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuMutationsModels$CrowdsourcingTofuLoggingMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1974325
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1974326
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuMutationsModels$CrowdsourcingTofuLoggingMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1974315
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1974316
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1974317
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1974318
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1974319
    if-eqz p0, :cond_0

    .line 1974320
    const-string p2, "client_mutation_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1974321
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1974322
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1974323
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1974314
    check-cast p1, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuMutationsModels$CrowdsourcingTofuLoggingMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuMutationsModels$CrowdsourcingTofuLoggingMutationModel$Serializer;->a(Lcom/facebook/feedplugins/crowdsourcing/graphql/CrowdsourcingTofuMutationsModels$CrowdsourcingTofuLoggingMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
