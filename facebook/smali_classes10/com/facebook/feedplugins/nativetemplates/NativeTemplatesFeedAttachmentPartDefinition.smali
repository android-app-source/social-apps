.class public Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/3j4;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3j4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977155
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1977156
    iput-object p2, p0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;->d:LX/3j4;

    .line 1977157
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1977144
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1977145
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    .line 1977146
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 1977147
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 1977148
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1977149
    iget-object v0, p0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;->d:LX/3j4;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v1

    move-object v2, p3

    check-cast v2, LX/1Pq;

    if-eqz v4, :cond_1

    .line 1977150
    iget-object v3, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1977151
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    :goto_0
    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/3j4;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/1Pq;LX/0jW;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1De;)LX/1X1;

    move-result-object v0

    .line 1977152
    :goto_1
    return-object v0

    .line 1977153
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 1977154
    :cond_2
    new-instance v0, LX/DEY;

    invoke-direct {v0, p0}, LX/DEY;-><init>(Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;)V

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;
    .locals 5

    .prologue
    .line 1977133
    const-class v1, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;

    monitor-enter v1

    .line 1977134
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1977135
    sput-object v2, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1977136
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977137
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1977138
    new-instance p0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/3j4;->a(LX/0QB;)LX/3j4;

    move-result-object v4

    check-cast v4, LX/3j4;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;-><init>(Landroid/content/Context;LX/3j4;)V

    .line 1977139
    move-object v0, p0

    .line 1977140
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1977141
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977142
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1977143
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1977132
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1977131
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/nativetemplates/NativeTemplatesFeedAttachmentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 1977120
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 1977121
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1977122
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1977123
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 1977124
    :goto_0
    return v0

    .line 1977125
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    .line 1977126
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    .line 1977127
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNativeTemplateView;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1977128
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1977129
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1977130
    const/4 v0, 0x0

    return-object v0
.end method
