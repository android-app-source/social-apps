.class public Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<TT;>;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3VK;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;


# instance fields
.field private final b:Landroid/content/res/Resources;

.field public final c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1977524
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f000000    # -8.0f

    .line 1977525
    iput v1, v0, LX/1UY;->c:F

    .line 1977526
    move-object v0, v0

    .line 1977527
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/intent/feed/IFeedIntentBuilder;)V
    .locals 0
    .param p7    # Lcom/facebook/intent/feed/IFeedIntentBuilder;
        .annotation build Lcom/facebook/inject/NeedsApplicationInjector;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977503
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1977504
    iput-object p1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->b:Landroid/content/res/Resources;

    .line 1977505
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1977506
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 1977507
    iput-object p4, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1977508
    iput-object p5, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->g:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    .line 1977509
    iput-object p6, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->h:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 1977510
    iput-object p7, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->c:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1977511
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1977523
    sget-object v0, LX/3VK;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1977513
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    const/4 v4, 0x0

    .line 1977514
    invoke-interface {p3}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1977515
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-interface {p1, v0, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1977516
    :goto_0
    const v0, 0x7f0d175f

    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->g:Lcom/facebook/multirow/parts/ResourceIdTextPartDefinition;

    const v2, 0x7f081868

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1977517
    const v0, 0x7f0d175f

    iget-object v1, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->h:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->b:Landroid/content/res/Resources;

    const v3, 0x7f081867

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1977518
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->f:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1977519
    new-instance v1, LX/DEw;

    invoke-direct {v1, p0}, LX/DEw;-><init>(Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;)V

    move-object v1, v1

    .line 1977520
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1977521
    return-object v4

    .line 1977522
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/pymk/rows/PeopleYouMayKnowFooterPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1977512
    const/4 v0, 0x1

    return v0
.end method
