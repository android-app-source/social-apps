.class public Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/1V0;

.field private final e:LX/DFA;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/DFA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1977825
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1977826
    iput-object p2, p0, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;->d:LX/1V0;

    .line 1977827
    iput-object p3, p0, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;->e:LX/DFA;

    .line 1977828
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pc;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1977807
    iget-object v0, p0, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;->e:LX/DFA;

    const/4 v1, 0x0

    .line 1977808
    new-instance v2, LX/DF9;

    invoke-direct {v2, v0}, LX/DF9;-><init>(LX/DFA;)V

    .line 1977809
    iget-object v3, v0, LX/DFA;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DF8;

    .line 1977810
    if-nez v3, :cond_0

    .line 1977811
    new-instance v3, LX/DF8;

    invoke-direct {v3, v0}, LX/DF8;-><init>(LX/DFA;)V

    .line 1977812
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/DF8;->a$redex0(LX/DF8;LX/1De;IILX/DF9;)V

    .line 1977813
    move-object v2, v3

    .line 1977814
    move-object v1, v2

    .line 1977815
    move-object v0, v1

    .line 1977816
    iget-object v1, v0, LX/DF8;->a:LX/DF9;

    iput-object p3, v1, LX/DF9;->a:LX/1Pc;

    .line 1977817
    iget-object v1, v0, LX/DF8;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1977818
    move-object v0, v0

    .line 1977819
    iget-object v1, v0, LX/DF8;->a:LX/DF9;

    iput-object p2, v1, LX/DF9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1977820
    iget-object v1, v0, LX/DF8;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1977821
    move-object v0, v0

    .line 1977822
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1977823
    new-instance v1, LX/1X6;

    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 1977824
    iget-object v2, p0, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;->d:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;
    .locals 6

    .prologue
    .line 1977796
    const-class v1, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;

    monitor-enter v1

    .line 1977797
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1977798
    sput-object v2, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1977799
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977800
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1977801
    new-instance p0, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/DFA;->a(LX/0QB;)LX/DFA;

    move-result-object v5

    check-cast v5, LX/DFA;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/DFA;)V

    .line 1977802
    move-object v0, p0

    .line 1977803
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1977804
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1977805
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1977806
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1977829
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pc;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pc;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1977795
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pc;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/pymk/rows/components/PaginatedPeopleYouMayKnowComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pc;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1977794
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1977791
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1977792
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1977793
    check-cast v0, LX/0jW;

    return-object v0
.end method
