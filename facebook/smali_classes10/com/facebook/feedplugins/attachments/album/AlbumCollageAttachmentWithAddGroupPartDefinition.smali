.class public Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0ad;

.field private final b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0ad;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1974183
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1974184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->d:Ljava/lang/Boolean;

    .line 1974185
    iput-object p1, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->a:LX/0ad;

    .line 1974186
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 1974187
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;

    .line 1974188
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;
    .locals 6

    .prologue
    .line 1974189
    const-class v1, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;

    monitor-enter v1

    .line 1974190
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1974191
    sput-object v2, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1974192
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974193
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1974194
    new-instance p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;-><init>(LX/0ad;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;)V

    .line 1974195
    move-object v0, p0

    .line 1974196
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1974197
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1974198
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1974199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1974200
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1974201
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1974202
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1974203
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1974204
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 1974205
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1974206
    if-nez v0, :cond_0

    move v0, v1

    .line 1974207
    :goto_0
    return v0

    .line 1974208
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 1974209
    goto :goto_0

    .line 1974210
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1974211
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x285feb

    if-eq v0, v2, :cond_3

    :cond_2
    move v0, v1

    .line 1974212
    goto :goto_0

    .line 1974213
    :cond_3
    invoke-static {p1}, LX/DCV;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    if-nez v0, :cond_4

    move v0, v1

    .line 1974214
    goto :goto_0

    .line 1974215
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 1974216
    goto :goto_0

    .line 1974217
    :cond_5
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    .line 1974218
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->a:LX/0ad;

    sget-short v2, LX/1EB;->c:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->d:Ljava/lang/Boolean;

    .line 1974219
    :cond_6
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumCollageAttachmentWithAddGroupPartDefinition;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method
