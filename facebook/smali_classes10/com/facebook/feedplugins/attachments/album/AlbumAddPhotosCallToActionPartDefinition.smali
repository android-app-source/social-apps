.class public Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:Landroid/content/res/Resources;

.field private final e:LX/0ad;

.field private final f:LX/DwI;

.field private final g:LX/DCY;

.field private final h:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;LX/0ad;LX/DwI;LX/DCY;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1974152
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1974153
    iput-object p2, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->d:Landroid/content/res/Resources;

    .line 1974154
    iput-object p3, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->e:LX/0ad;

    .line 1974155
    iput-object p4, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->f:LX/DwI;

    .line 1974156
    iput-object p5, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->g:LX/DCY;

    .line 1974157
    iput-object p6, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->h:LX/1V0;

    .line 1974158
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1974159
    invoke-static {p2}, LX/DCV;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 1974160
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->f:LX/DwI;

    .line 1974161
    invoke-static {v1}, LX/DwG;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v2

    .line 1974162
    invoke-virtual {v0, v1, v2}, LX/DwI;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Z

    move-result v2

    move v0, v2

    .line 1974163
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->e:LX/0ad;

    sget-char v2, LX/1EB;->b:C

    const v3, 0x7f0828a6

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->d:Landroid/content/res/Resources;

    invoke-interface {v0, v2, v3, v4}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 1974164
    :goto_0
    iget-object v2, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->g:LX/DCY;

    const/4 v3, 0x0

    .line 1974165
    new-instance v4, LX/DCW;

    invoke-direct {v4, v2}, LX/DCW;-><init>(LX/DCY;)V

    .line 1974166
    sget-object v5, LX/DCY;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/DCX;

    .line 1974167
    if-nez v5, :cond_0

    .line 1974168
    new-instance v5, LX/DCX;

    invoke-direct {v5}, LX/DCX;-><init>()V

    .line 1974169
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/DCX;->a$redex0(LX/DCX;LX/1De;IILX/DCW;)V

    .line 1974170
    move-object v4, v5

    .line 1974171
    move-object v3, v4

    .line 1974172
    move-object v2, v3

    .line 1974173
    iget-object v3, v2, LX/DCX;->a:LX/DCW;

    iput-object v1, v3, LX/DCW;->b:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1974174
    iget-object v3, v2, LX/DCX;->d:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1974175
    move-object v1, v2

    .line 1974176
    iget-object v2, v1, LX/DCX;->a:LX/DCW;

    iput-object v0, v2, LX/DCW;->a:Ljava/lang/String;

    .line 1974177
    iget-object v2, v1, LX/DCX;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1974178
    move-object v0, v1

    .line 1974179
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1974180
    iget-object v1, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->h:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, LX/1Ua;->e:LX/1Ua;

    invoke-direct {v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 1974181
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->e:LX/0ad;

    sget-char v2, LX/1EB;->a:C

    const v3, 0x7f0828a5

    iget-object v4, p0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->d:Landroid/content/res/Resources;

    invoke-interface {v0, v2, v3, v4}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;
    .locals 10

    .prologue
    .line 1974141
    const-class v1, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;

    monitor-enter v1

    .line 1974142
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1974143
    sput-object v2, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1974144
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1974145
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1974146
    new-instance v3, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/DwI;->b(LX/0QB;)LX/DwI;

    move-result-object v7

    check-cast v7, LX/DwI;

    invoke-static {v0}, LX/DCY;->a(LX/0QB;)LX/DCY;

    move-result-object v8

    check-cast v8, LX/DCY;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v9

    check-cast v9, LX/1V0;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;-><init>(Landroid/content/Context;Landroid/content/res/Resources;LX/0ad;LX/DwI;LX/DCY;LX/1V0;)V

    .line 1974147
    move-object v0, v3

    .line 1974148
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1974149
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1974150
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1974151
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1974182
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1974140
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/attachments/album/AlbumAddPhotosCallToActionPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1974137
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1974138
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1974139
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
