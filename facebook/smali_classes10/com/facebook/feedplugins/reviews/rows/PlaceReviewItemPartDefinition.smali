.class public Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/DFw;",
        "LX/DFx;",
        "LX/1PW;",
        "LX/3Ym;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/String;

.field private static k:LX/0Xm;


# instance fields
.field private final c:LX/17Q;

.field public final d:LX/0Zb;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BNP;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0bH;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DFq;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/17W;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1979064
    const-class v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1979065
    const-class v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/17Q;LX/0Zb;LX/0Ot;LX/0Ot;LX/0bH;LX/0Ot;LX/0Ot;LX/17W;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17Q;",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "LX/BNP;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0bH;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DFq;",
            ">;",
            "LX/17W;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979054
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1979055
    iput-object p1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->c:LX/17Q;

    .line 1979056
    iput-object p2, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->d:LX/0Zb;

    .line 1979057
    iput-object p3, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->e:LX/0Ot;

    .line 1979058
    iput-object p4, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->f:LX/0Ot;

    .line 1979059
    iput-object p5, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->g:LX/0bH;

    .line 1979060
    iput-object p6, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->h:LX/0Ot;

    .line 1979061
    iput-object p7, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->i:LX/0Ot;

    .line 1979062
    iput-object p8, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->j:LX/17W;

    .line 1979063
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;
    .locals 12

    .prologue
    .line 1979043
    const-class v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    monitor-enter v1

    .line 1979044
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979045
    sput-object v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979046
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979047
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979048
    new-instance v3, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v4

    check-cast v4, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    const/16 v6, 0x31d4

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    const/16 v9, 0x12b1

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2149

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v11

    check-cast v11, LX/17W;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;-><init>(LX/17Q;LX/0Zb;LX/0Ot;LX/0Ot;LX/0bH;LX/0Ot;LX/0Ot;LX/17W;)V

    .line 1979049
    move-object v0, v3

    .line 1979050
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979051
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979052
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979053
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static synthetic a(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1979042
    invoke-static {p1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)I
    .locals 1

    .prologue
    .line 1979039
    invoke-static {p1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->i(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    .line 1979040
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    move v0, v0

    .line 1979041
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->j()I

    move-result v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1979038
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic c(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1979035
    invoke-static {p1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->i(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    .line 1979036
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-nez p0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    .line 1979037
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic d(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 1

    .prologue
    .line 1979032
    invoke-static {p1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->i(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    .line 1979033
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    .line 1979034
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->k()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v0

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;LX/DFw;)V
    .locals 3

    .prologue
    .line 1978970
    invoke-static {p1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->f(LX/DFw;)LX/162;

    move-result-object v0

    iget-object v1, p1, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Ljava/lang/String;

    move-result-object v1

    .line 1978971
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1978972
    const/4 v2, 0x0

    .line 1978973
    :goto_0
    move-object v0, v2

    .line 1978974
    iget-object v1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1978975
    return-void

    :cond_0
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "place_review_ego_launched_review_composer"

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {v2, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string p1, "page_id"

    invoke-virtual {v2, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string p1, "native_newsfeed"

    .line 1978976
    iput-object p1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1978977
    move-object v2, v2

    .line 1978978
    goto :goto_0
.end method

.method private static e(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1979027
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1979028
    :cond_0
    const/4 v0, 0x0

    .line 1979029
    :goto_0
    return-object v0

    .line 1979030
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->N()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    .line 1979031
    new-instance v1, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v2, "place_review_ego_unit"

    invoke-direct {v1, v2}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(LX/DFw;)LX/162;
    .locals 2

    .prologue
    .line 1979023
    new-instance v0, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/162;-><init>(LX/0mC;)V

    .line 1979024
    iget-object v1, p0, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    invoke-static {v1}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->a(LX/162;)LX/162;

    .line 1979025
    iget-object v1, p0, LX/DFw;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1979026
    return-object v0
.end method

.method public static i(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Lcom/facebook/graphql/model/GraphQLContactRecommendationField;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1979022
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->aO()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1979011
    check-cast p2, LX/DFw;

    .line 1979012
    iget-object v4, p2, LX/DFw;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    .line 1979013
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v1, v0

    .line 1979014
    new-instance v0, LX/DFx;

    .line 1979015
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_1
    move-object v2, v2

    .line 1979016
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    :goto_2
    move-object v3, v3

    .line 1979017
    invoke-static {v4}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->e(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v4

    .line 1979018
    new-instance v5, LX/DFt;

    invoke-direct {v5, p0, p2, v1}, LX/DFt;-><init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;LX/DFw;Ljava/lang/String;)V

    move-object v5, v5

    .line 1979019
    new-instance v6, LX/DFu;

    invoke-direct {v6, p0, p2}, LX/DFu;-><init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;LX/DFw;)V

    move-object v6, v6

    .line 1979020
    new-instance v7, LX/DFs;

    invoke-direct {v7, p0, p2}, LX/DFs;-><init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;LX/DFw;)V

    move-object v7, v7

    .line 1979021
    invoke-direct/range {v0 .. v7}, LX/DFx;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_0
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-static {v3}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v3

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x35a682c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978986
    check-cast p1, LX/DFw;

    check-cast p2, LX/DFx;

    check-cast p4, LX/3Ym;

    const/16 p0, 0x8

    const/4 v4, 0x0

    .line 1978987
    iget-object v1, p2, LX/DFx;->a:Ljava/lang/String;

    .line 1978988
    iget-object v2, p4, LX/3Ym;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1978989
    iget-object v1, p2, LX/DFx;->b:Ljava/lang/String;

    .line 1978990
    iget-object v2, p4, LX/3Ym;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1978991
    iget-object v1, p2, LX/DFx;->c:Landroid/net/Uri;

    if-nez v1, :cond_0

    iget-object v1, p2, LX/DFx;->d:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    if-nez v1, :cond_1

    .line 1978992
    :cond_0
    iget-object v1, p2, LX/DFx;->c:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1978993
    iget-object p3, p4, LX/3Ym;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p3, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1978994
    invoke-virtual {p4, v4}, LX/3Ym;->setMainImageVisibility(I)V

    .line 1978995
    invoke-virtual {p4, p0}, LX/3Ym;->setMapImageVisibility(I)V

    .line 1978996
    :goto_0
    iget-object v1, p2, LX/DFx;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/3Ym;->setReviewButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1978997
    iget-object v1, p2, LX/DFx;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/3Ym;->setXOutIconOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1978998
    iget-object v1, p2, LX/DFx;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/3Ym;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1978999
    iget-object v1, p2, LX/DFx;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/3Ym;->setTitleOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1979000
    iget-object v1, p2, LX/DFx;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/3Ym;->setBottomSectionContainerOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1979001
    iget-boolean v1, p1, LX/DFw;->c:Z

    .line 1979002
    iget-object v2, p4, LX/3Ym;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 1979003
    if-eqz v1, :cond_2

    const/4 v2, -0x1

    :goto_1
    iput v2, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1979004
    iget-object v2, p4, LX/3Ym;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1979005
    const/16 v1, 0x1f

    const v2, 0x52e30b02

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1979006
    :cond_1
    iget-object v1, p2, LX/DFx;->d:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1979007
    iget-object v2, p4, LX/3Ym;->f:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v2, v1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 1979008
    invoke-virtual {p4, v4}, LX/3Ym;->setMapImageVisibility(I)V

    .line 1979009
    invoke-virtual {p4, p0}, LX/3Ym;->setMainImageVisibility(I)V

    goto :goto_0

    .line 1979010
    :cond_2
    invoke-virtual {p4}, LX/3Ym;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0b0a2a

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1978979
    check-cast p4, LX/3Ym;

    const/4 v0, 0x0

    .line 1978980
    invoke-virtual {p4, v0}, LX/3Ym;->setBottomSectionContainerOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1978981
    invoke-virtual {p4, v0}, LX/3Ym;->setReviewButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1978982
    invoke-virtual {p4, v0}, LX/3Ym;->setXOutIconOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1978983
    invoke-virtual {p4, v0}, LX/3Ym;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1978984
    invoke-virtual {p4, v0}, LX/3Ym;->setTitleOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1978985
    return-void
.end method
