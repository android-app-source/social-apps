.class public Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;

.field private final b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979091
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1979092
    iput-object p2, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;->a:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;

    .line 1979093
    iput-object p1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;

    .line 1979094
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;
    .locals 5

    .prologue
    .line 1979096
    const-class v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;

    monitor-enter v1

    .line 1979097
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979098
    sput-object v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979099
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979100
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979101
    new-instance p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;-><init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;)V

    .line 1979102
    move-object v0, p0

    .line 1979103
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979104
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979105
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979106
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1979107
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979108
    iget-object v0, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1979109
    iget-object v0, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPartDefinition;->a:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1979110
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1979095
    const/4 v0, 0x1

    return v0
.end method
