.class public Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;

.field public final c:LX/1g4;

.field private final d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "LX/DFz;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/2dq;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;LX/1g4;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978872
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1978873
    iput-object p1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1978874
    iput-object p2, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;

    .line 1978875
    iput-object p3, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->c:LX/1g4;

    .line 1978876
    iput-object p4, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 1978877
    iput-object p5, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->e:LX/2dq;

    .line 1978878
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;
    .locals 9

    .prologue
    .line 1978841
    const-class v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;

    monitor-enter v1

    .line 1978842
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978843
    sput-object v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978844
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978845
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978846
    new-instance v3, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;

    invoke-static {v0}, LX/1g4;->a(LX/0QB;)LX/1g4;

    move-result-object v6

    check-cast v6, LX/1g4;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v8

    check-cast v8, LX/2dq;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;LX/1g4;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;)V

    .line 1978847
    move-object v0, v3

    .line 1978848
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978849
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978850
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978851
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1978852
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1978853
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 1978854
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v0

    .line 1978855
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1978856
    sget-object v0, LX/2eA;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1978857
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978858
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v0

    .line 1978859
    check-cast v5, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 1978860
    iget-object v0, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/2eF;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1978861
    iget-object v6, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    new-instance v0, LX/2eG;

    iget-object v1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->e:LX/2dq;

    const v2, 0x43898000    # 275.0f

    sget-object v3, LX/2eF;->a:LX/1Ua;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->I_()I

    move-result v2

    .line 1978862
    invoke-static {v5}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v3

    .line 1978863
    new-instance v4, LX/DFr;

    invoke-direct {v4, p0, v3, v5}, LX/DFr;-><init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;LX/0Px;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)V

    move-object v3, v4

    .line 1978864
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->g()Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1978865
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x3ff29cf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978866
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978867
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1978868
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 1978869
    iget-object v2, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->c:LX/1g4;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->I_()I

    move-result p2

    invoke-virtual {v2, v1, p2}, LX/1g4;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 1978870
    const/16 v1, 0x1f

    const v2, -0x2642ee2c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1978871
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
