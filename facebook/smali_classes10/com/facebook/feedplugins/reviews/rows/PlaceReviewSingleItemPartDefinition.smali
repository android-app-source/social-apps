.class public Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3Ym;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:LX/1V7;

.field public final c:LX/1g4;

.field private final d:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;LX/1g4;Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979122
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1979123
    iput-object p1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1979124
    iput-object p2, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->b:LX/1V7;

    .line 1979125
    iput-object p3, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->c:LX/1g4;

    .line 1979126
    iput-object p4, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->d:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    .line 1979127
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;
    .locals 7

    .prologue
    .line 1979111
    const-class v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;

    monitor-enter v1

    .line 1979112
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979113
    sput-object v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979114
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979115
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979116
    new-instance p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v4

    check-cast v4, LX/1V7;

    invoke-static {v0}, LX/1g4;->a(LX/0QB;)LX/1g4;

    move-result-object v5

    check-cast v5, LX/1g4;

    invoke-static {v0}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1V7;LX/1g4;Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;)V

    .line 1979117
    move-object v0, p0

    .line 1979118
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979119
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979120
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979121
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1979135
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1979136
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 1979137
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v0

    .line 1979138
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1979134
    sget-object v0, LX/3Ym;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1979139
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979140
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1979141
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 1979142
    iget-object v1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    iget-object v3, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->b:LX/1V7;

    invoke-virtual {v3}, LX/1V7;->h()LX/1Ua;

    move-result-object v3

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1979143
    iget-object v2, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->d:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    new-instance v3, LX/DFw;

    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    const/4 v4, 0x1

    invoke-direct {v3, v1, v0, v4}, LX/DFw;-><init>(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;Z)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1979144
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4e6929ad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1979129
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1979130
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1979131
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 1979132
    iget-object v2, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->c:LX/1g4;

    const/4 p2, 0x0

    invoke-virtual {v2, v1, p2}, LX/1g4;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 1979133
    const/16 v1, 0x1f

    const v2, -0x76f4a49d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1979128
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
