.class public Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;

.field private final b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978814
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1978815
    iput-object p2, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;->a:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;

    .line 1978816
    iput-object p1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;

    .line 1978817
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;
    .locals 5

    .prologue
    .line 1978818
    const-class v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;

    monitor-enter v1

    .line 1978819
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978820
    sput-object v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978821
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978822
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978823
    new-instance p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;-><init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;)V

    .line 1978824
    move-object v0, p0

    .line 1978825
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978826
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978827
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978828
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1978829
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978830
    iget-object v0, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;->a:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewAttachmentSelectorPartDefinition;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1978831
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1978832
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978833
    invoke-static {p1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewSingleItemPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHScrollPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
