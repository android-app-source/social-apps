.class public Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;",
        ">;",
        "Ljava/lang/CharSequence;",
        "LX/1Ps;",
        "LX/2ee;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1978908
    sget-object v0, LX/1Ua;->i:LX/1Ua;

    sput-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1978905
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1978906
    iput-object p1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1978907
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;
    .locals 4

    .prologue
    .line 1978894
    const-class v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;

    monitor-enter v1

    .line 1978895
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1978896
    sput-object v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1978897
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1978898
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1978899
    new-instance p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 1978900
    move-object v0, p0

    .line 1978901
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1978902
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1978903
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1978904
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1978893
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1978879
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978880
    iget-object v0, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewHeaderPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1978881
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1978882
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4eb3dc37

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1978888
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, LX/2ee;

    .line 1978889
    sget-object v1, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    invoke-virtual {p4, v1}, LX/2ee;->setStyle(LX/2ei;)V

    .line 1978890
    sget-object v1, LX/2ej;->NOT_SPONSORED:LX/2ej;

    invoke-virtual {p4, p2, v1}, LX/2ee;->a(Ljava/lang/CharSequence;LX/2ej;)V

    .line 1978891
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, LX/2ee;->setMenuButtonActive(Z)V

    .line 1978892
    const/16 v1, 0x1f

    const v2, 0x305dc4bf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1978883
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1978884
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1978885
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    .line 1978886
    invoke-static {v0}, LX/1lv;->a(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;)LX/0Px;

    move-result-object v0

    .line 1978887
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
