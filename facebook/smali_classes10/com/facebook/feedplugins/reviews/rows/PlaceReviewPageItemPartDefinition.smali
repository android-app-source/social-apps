.class public Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/DFz;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3Ym;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/3Ym;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1979090
    new-instance v0, LX/DFy;

    invoke-direct {v0}, LX/DFy;-><init>()V

    sput-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1979076
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 1979077
    iput-object p1, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    .line 1979078
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;
    .locals 4

    .prologue
    .line 1979079
    const-class v1, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;

    monitor-enter v1

    .line 1979080
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1979081
    sput-object v2, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1979082
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1979083
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1979084
    new-instance p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;-><init>(Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;)V

    .line 1979085
    move-object v0, p0

    .line 1979086
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1979087
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1979088
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1979089
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1979072
    sget-object v0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1979073
    check-cast p2, LX/DFz;

    .line 1979074
    iget-object v0, p0, Lcom/facebook/feedplugins/reviews/rows/PlaceReviewPageItemPartDefinition;->b:Lcom/facebook/feedplugins/reviews/rows/PlaceReviewItemPartDefinition;

    new-instance v1, LX/DFw;

    iget-object v2, p2, LX/DFz;->a:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;

    iget-object v3, p2, LX/DFz;->b:Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, LX/DFw;-><init>(Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnitItem;Lcom/facebook/graphql/model/GraphQLPlaceReviewFeedUnit;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1979075
    const/4 v0, 0x0

    return-object v0
.end method
