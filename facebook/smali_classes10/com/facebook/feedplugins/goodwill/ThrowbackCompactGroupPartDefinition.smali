.class public Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

.field private final d:Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;

.field private final e:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

.field private final f:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

.field private final g:Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174970
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2174971
    iput-object p7, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    .line 2174972
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    .line 2174973
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;

    .line 2174974
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;

    .line 2174975
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    .line 2174976
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->g:Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;

    .line 2174977
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    .line 2174978
    iput-object p8, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->h:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    .line 2174979
    iput-object p9, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->i:LX/0Uh;

    .line 2174980
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;
    .locals 13

    .prologue
    .line 2174981
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;

    monitor-enter v1

    .line 2174982
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174983
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174984
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174985
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174986
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;LX/0Uh;)V

    .line 2174987
    move-object v0, v3

    .line 2174988
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174989
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174990
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2174992
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 2174993
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174994
    invoke-static {p2}, LX/Es3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174995
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174996
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    .line 2174997
    const-string v1, "video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2174998
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->g:Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174999
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->h:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175000
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175001
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175002
    const/4 v0, 0x0

    return-object v0

    .line 2175003
    :cond_0
    const-string v1, "multi_photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2175004
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175005
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2175006
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2175007
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2175008
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->i:LX/0Uh;

    const/16 v1, 0x92

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2175009
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2175010
    :cond_2
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    .line 2175011
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2175012
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175013
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175014
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/23B;->e(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 2175015
    :goto_0
    return v0

    .line 2175016
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    .line 2175017
    const-string v3, "video"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 2175018
    const-string v4, "multi_photo"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2175019
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2175020
    invoke-static {v0}, LX/23B;->h(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)I

    move-result v4

    if-ne v4, v5, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2175021
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v7

    if-nez v7, :cond_9

    .line 2175022
    const/4 v7, 0x0

    .line 2175023
    :goto_1
    move v4, v7

    .line 2175024
    if-ne v4, v5, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    .line 2175025
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    if-eqz v7, :cond_a

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    if-eqz v7, :cond_a

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v4, v7}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v7

    if-eqz v7, :cond_a

    const/4 v7, 0x1

    :goto_2
    move v4, v7

    .line 2175026
    if-eqz v4, :cond_8

    move v4, v5

    :goto_3
    move v0, v4

    .line 2175027
    if-nez v3, :cond_2

    if-nez v0, :cond_2

    if-eqz v2, :cond_7

    .line 2175028
    :cond_2
    invoke-static {p1}, LX/Es3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2175029
    if-eqz v0, :cond_3

    .line 2175030
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 2175031
    if-nez v0, :cond_4

    :cond_3
    move v0, v1

    .line 2175032
    goto :goto_0

    .line 2175033
    :cond_4
    if-eqz v3, :cond_5

    .line 2175034
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->g:Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feedplugins/goodwill/ThrowbackRichVideoRowPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    goto :goto_0

    .line 2175035
    :cond_5
    if-eqz v2, :cond_6

    .line 2175036
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175037
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2175038
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2175039
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-virtual {v1, v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    goto/16 :goto_0

    .line 2175040
    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 2175041
    goto/16 :goto_0

    :cond_8
    move v4, v6

    goto :goto_3

    :cond_9
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    goto :goto_1

    :cond_a
    const/4 v7, 0x0

    goto :goto_2
.end method
