.class public Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176455
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2176456
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;

    .line 2176457
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;

    .line 2176458
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;
    .locals 5

    .prologue
    .line 2176459
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    monitor-enter v1

    .line 2176460
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176461
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176462
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176463
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176464
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;)V

    .line 2176465
    move-object v0, p0

    .line 2176466
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176467
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176468
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176469
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2176470
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176471
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSharePartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2176472
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2176473
    const/4 v0, 0x1

    return v0
.end method
