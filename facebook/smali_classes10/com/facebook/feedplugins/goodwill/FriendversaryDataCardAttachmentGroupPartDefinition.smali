.class public Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderTextPartDefinition;

.field private final c:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;

.field private final d:F


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderTextPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174309
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2174310
    const/high16 v0, 0x41000000    # 8.0f

    iput v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->d:F

    .line 2174311
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;

    .line 2174312
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderTextPartDefinition;

    .line 2174313
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;

    .line 2174314
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;
    .locals 6

    .prologue
    .line 2174315
    const-class v1, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 2174316
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174317
    sput-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174318
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174319
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174320
    new-instance p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderTextPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderTextPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;)V

    .line 2174321
    move-object v0, p0

    .line 2174322
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174323
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174324
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174325
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2174326
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174327
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174328
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2174329
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174330
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentHeaderTextPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174331
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bO()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bO()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2174332
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->bO()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;->a()LX/0Px;

    move-result-object v0

    .line 2174333
    invoke-virtual {v0}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    .line 2174334
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2174335
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2174336
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    .line 2174337
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2174338
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2174339
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 2174340
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2174341
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    .line 2174342
    sget-object v2, LX/1X9;->MIDDLE:LX/1X9;

    .line 2174343
    const/4 v1, 0x0

    .line 2174344
    const/4 v4, 0x2

    invoke-static {p2, v4}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    move-object v4, v4

    .line 2174345
    if-eqz v4, :cond_4

    const/4 v4, 0x1

    :goto_2
    move v4, v4

    .line 2174346
    if-eqz v4, :cond_2

    .line 2174347
    const/high16 v1, 0x41000000    # 8.0f

    .line 2174348
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2174349
    sget-object v2, LX/1X9;->BOTTOM:LX/1X9;

    .line 2174350
    :cond_2
    new-instance v4, LX/Es4;

    invoke-direct {v4, v0, p2, v2, v1}, LX/Es4;-><init>(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1X9;F)V

    .line 2174351
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardAttachmentDataPointPartDefinition;

    invoke-virtual {p1, v0, v4}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1

    .line 2174352
    :cond_3
    const/4 v0, 0x0

    return-object v0

    :cond_4
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2174353
    const/4 v0, 0x1

    return v0
.end method
