.class public Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2173966
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2173967
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;

    .line 2173968
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;

    .line 2173969
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;

    .line 2173970
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;
    .locals 6

    .prologue
    .line 2173971
    const-class v1, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;

    monitor-enter v1

    .line 2173972
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2173973
    sput-object v2, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2173974
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173975
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2173976
    new-instance p0, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;)V

    .line 2173977
    move-object v0, p0

    .line 2173978
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2173979
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173980
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2173981
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2173982
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2173983
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2173984
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;

    const/4 v2, 0x0

    .line 2173985
    if-nez p2, :cond_0

    move-object v1, v2

    .line 2173986
    :goto_0
    move-object v1, v1

    .line 2173987
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2173988
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2173989
    const/4 v0, 0x0

    return-object v0

    .line 2173990
    :cond_0
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2173991
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    .line 2173992
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v1, v2

    .line 2173993
    goto :goto_0

    .line 2173994
    :cond_1
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q()LX/0Px;

    move-result-object v3

    .line 2173995
    iput-object v3, v2, LX/39x;->q:LX/0Px;

    .line 2173996
    move-object v2, v2

    .line 2173997
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->g()Ljava/lang/String;

    move-result-object p3

    .line 2173998
    iput-object p3, v3, LX/23u;->m:Ljava/lang/String;

    .line 2173999
    move-object v3, v3

    .line 2174000
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2174001
    iput-object v1, v3, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2174002
    move-object v1, v3

    .line 2174003
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2174004
    iput-object v2, v1, LX/23u;->k:LX/0Px;

    .line 2174005
    move-object v1, v1

    .line 2174006
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2174007
    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2174008
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174009
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174010
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    .line 2174011
    invoke-static {v0}, LX/18H;->a(Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;)Z

    move-result v0

    return v0
.end method
