.class public Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/EsW;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/EsW;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174908
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2174909
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->d:LX/EsW;

    .line 2174910
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->e:LX/1V0;

    .line 2174911
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2174903
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174904
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    .line 2174905
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2174906
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->d:LX/EsW;

    invoke-virtual {v1, p1}, LX/EsW;->c(LX/1De;)LX/EsU;

    move-result-object v1

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/EsU;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/EsU;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2174907
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v2, p2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;
    .locals 6

    .prologue
    .line 2174892
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;

    monitor-enter v1

    .line 2174893
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174894
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174895
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174896
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174897
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EsW;->a(LX/0QB;)LX/EsW;

    move-result-object v4

    check-cast v4, LX/EsW;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;-><init>(Landroid/content/Context;LX/EsW;LX/1V0;)V

    .line 2174898
    move-object v0, p0

    .line 2174899
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174900
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174901
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174902
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2174874
    if-nez p0, :cond_1

    .line 2174875
    :cond_0
    :goto_0
    return-object v0

    .line 2174876
    :cond_1
    instance-of v1, p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    if-eqz v1, :cond_0

    .line 2174877
    check-cast p0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->p()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2174891
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2174890
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2174880
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2174881
    if-eqz p1, :cond_0

    .line 2174882
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174883
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2174884
    :goto_0
    return v0

    .line 2174885
    :cond_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174886
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignPermalinkPrivacyLabelPartDefinition;->a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    .line 2174887
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    .line 2174888
    goto :goto_0

    .line 2174889
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2174878
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174879
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
