.class public Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174261
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2174262
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    .line 2174263
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    .line 2174264
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 2174265
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    .line 2174266
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;
    .locals 7

    .prologue
    .line 2174267
    const-class v1, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 2174268
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174269
    sput-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174270
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174271
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174272
    new-instance p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;)V

    .line 2174273
    move-object v0, p0

    .line 2174274
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174275
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174276
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174277
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2174278
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    .line 2174279
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174280
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2174281
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2174282
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_COLLAGE_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, v2}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v1

    .line 2174283
    if-eqz v1, :cond_1

    .line 2174284
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174285
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jD()LX/0Px;

    move-result-object v1

    .line 2174286
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    .line 2174287
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    .line 2174288
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2174289
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    .line 2174290
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v0

    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2174291
    iput-object v0, v2, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2174292
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->d:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174293
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 2174294
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2174295
    :cond_2
    iput-object v1, v2, LX/39x;->q:LX/0Px;

    .line 2174296
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCollageAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2174297
    const/4 v0, 0x1

    return v0
.end method
