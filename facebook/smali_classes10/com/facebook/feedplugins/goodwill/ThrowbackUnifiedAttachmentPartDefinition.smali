.class public Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176353
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2176354
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    .line 2176355
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    .line 2176356
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;
    .locals 5

    .prologue
    .line 2176357
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;

    monitor-enter v1

    .line 2176358
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176359
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176360
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176361
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176362
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;)V

    .line 2176363
    move-object v0, p0

    .line 2176364
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176365
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176366
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176367
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2176368
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176369
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2176370
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2176371
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2176372
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2176373
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;->b:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2176374
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2176375
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2176376
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176377
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2176378
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2176379
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
