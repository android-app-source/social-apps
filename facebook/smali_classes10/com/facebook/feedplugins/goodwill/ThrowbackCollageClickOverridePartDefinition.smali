.class public Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Void;",
        "LX/Aj7",
        "<",
        "LX/26M;",
        ">;",
        "LX/1PW;",
        "Lcom/facebook/feed/collage/ui/CollageAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/23R;

.field private final b:LX/9hF;


# direct methods
.method public constructor <init>(LX/23R;LX/9hF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174946
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2174947
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;->a:LX/23R;

    .line 2174948
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;->b:LX/9hF;

    .line 2174949
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;
    .locals 5

    .prologue
    .line 2174950
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;

    monitor-enter v1

    .line 2174951
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174952
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174953
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174954
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174955
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;

    invoke-static {v0}, Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;->a(LX/0QB;)Lcom/facebook/photos/mediagallery/ui/DefaultMediaGalleryLauncher;

    move-result-object v3

    check-cast v3, LX/23R;

    invoke-static {v0}, LX/9hF;->a(LX/0QB;)LX/9hF;

    move-result-object v4

    check-cast v4, LX/9hF;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;-><init>(LX/23R;LX/9hF;)V

    .line 2174956
    move-object v0, p0

    .line 2174957
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174958
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174959
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174960
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2174961
    new-instance v0, LX/EsJ;

    invoke-direct {v0, p0}, LX/EsJ;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackCollageClickOverridePartDefinition;)V

    move-object v0, v0

    .line 2174962
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7ca10473

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2174963
    check-cast p2, LX/Aj7;

    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 2174964
    iput-object p2, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 2174965
    const/16 v1, 0x1f

    const v2, 0x26709f57

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2174966
    check-cast p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 2174967
    const/4 v0, 0x0

    .line 2174968
    iput-object v0, p4, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->u:LX/Aj7;

    .line 2174969
    return-void
.end method
