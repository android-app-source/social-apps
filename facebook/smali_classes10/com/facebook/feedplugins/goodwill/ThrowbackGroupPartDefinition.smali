.class public Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignGroupPartDefinition;

.field private final d:Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;

.field private final e:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;

.field private final f:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;

.field private final g:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;

.field private final h:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;

.field private final i:Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;

.field private final j:Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCampaignGroupPartDefinition;

.field private final k:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCampaignGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175316
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2175317
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;

    .line 2175318
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;

    .line 2175319
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignGroupPartDefinition;

    .line 2175320
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;

    .line 2175321
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;

    .line 2175322
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->g:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;

    .line 2175323
    iput-object p7, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->h:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;

    .line 2175324
    iput-object p8, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->i:Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;

    .line 2175325
    iput-object p9, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->j:Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCampaignGroupPartDefinition;

    .line 2175326
    iput-object p10, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;

    .line 2175327
    iput-object p11, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    .line 2175328
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;
    .locals 15

    .prologue
    .line 2175329
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;

    monitor-enter v1

    .line 2175330
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175331
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175332
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175333
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175334
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignGroupPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCampaignGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCampaignGroupPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCampaignGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCampaignGroupPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;)V

    .line 2175335
    move-object v0, v3

    .line 2175336
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175337
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175338
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175339
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2175340
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175341
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->j:Lcom/facebook/feedplugins/goodwill/ThrowbackVideoCampaignGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->g:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->h:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->i:Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackCampaignGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackCompactGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2175342
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackGroupPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/inlinesurvey/InlineSurveyGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175343
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2175344
    const/4 v0, 0x1

    return v0
.end method
