.class public Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174227
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2174228
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    .line 2174229
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    .line 2174230
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 2174231
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;

    .line 2174232
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;
    .locals 7

    .prologue
    .line 2174233
    const-class v1, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 2174234
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174235
    sput-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174236
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174237
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174238
    new-instance p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;)V

    .line 2174239
    move-object v0, p0

    .line 2174240
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174241
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174242
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174243
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2174244
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174245
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174246
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GOODWILL_THROWBACK_FRIENDVERSARY_POLAROIDS_CARD_IPB:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0, v1}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v0

    .line 2174247
    if-eqz v0, :cond_0

    .line 2174248
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174249
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2174250
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174251
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174252
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jD()LX/0Px;

    move-result-object v0

    .line 2174253
    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    .line 2174254
    iput-object v0, v1, LX/39x;->q:LX/0Px;

    .line 2174255
    move-object v0, v1

    .line 2174256
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174257
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2174258
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174259
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryCardAttachmentGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2174260
    const/4 v0, 0x1

    return v0
.end method
