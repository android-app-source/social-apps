.class public Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/1nu;

.field public final c:LX/1Uf;

.field public final d:LX/17W;

.field public final e:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2177240
    const-class v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/17W;LX/1Uf;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2177234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2177235
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->b:LX/1nu;

    .line 2177236
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->d:LX/17W;

    .line 2177237
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->c:LX/1Uf;

    .line 2177238
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->e:LX/1vg;

    .line 2177239
    return-void
.end method

.method public static a(Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLImage;)LX/1Dg;
    .locals 2

    .prologue
    .line 2177218
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->b:LX/1nu;

    invoke-virtual {v0, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v0

    sget-object v1, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    invoke-interface {v0, v1}, LX/1Di;->g(I)LX/1Di;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    invoke-interface {v0, v1}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;
    .locals 7

    .prologue
    .line 2177223
    const-class v1, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;

    monitor-enter v1

    .line 2177224
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2177225
    sput-object v2, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2177226
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2177227
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2177228
    new-instance p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v5

    check-cast v5, LX/1Uf;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v6

    check-cast v6, LX/1vg;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;-><init>(LX/1nu;LX/17W;LX/1Uf;LX/1vg;)V

    .line 2177229
    move-object v0, p0

    .line 2177230
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2177231
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2177232
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2177233
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2177219
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->p()LX/0Px;

    move-result-object v0

    .line 2177220
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2177221
    :cond_0
    const/4 v0, 0x0

    .line 2177222
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    goto :goto_0
.end method
