.class public Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174590
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2174591
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 2174592
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    .line 2174593
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    .line 2174594
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;
    .locals 6

    .prologue
    .line 2174595
    const-class v1, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 2174596
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174597
    sput-object v2, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174598
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174599
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174600
    new-instance p0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;)V

    .line 2174601
    move-object v0, p0

    .line 2174602
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174603
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174604
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174605
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2174606
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v4, 0x0

    .line 2174607
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174608
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2174609
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174610
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jD()LX/0Px;

    move-result-object v1

    .line 2174611
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 2174612
    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2174613
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    new-instance v0, LX/39x;

    invoke-direct {v0}, LX/39x;-><init>()V

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v3

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v0

    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2174614
    iput-object v0, v3, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2174615
    move-object v0, v3

    .line 2174616
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174617
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2174618
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/StarversaryCardAttachmentGroupPartDefinition;->b:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    .line 2174619
    iput-object v1, v2, LX/39x;->q:LX/0Px;

    .line 2174620
    move-object v1, v2

    .line 2174621
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2174622
    const/4 v0, 0x1

    return v0
.end method
