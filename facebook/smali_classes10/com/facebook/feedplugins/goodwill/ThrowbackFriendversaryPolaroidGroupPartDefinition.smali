.class public Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidContentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidContentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

.field private final f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidContentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175238
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2175239
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    .line 2175240
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidContentPartDefinition;

    .line 2175241
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    .line 2175242
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    .line 2175243
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    .line 2175244
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    .line 2175245
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;
    .locals 10

    .prologue
    .line 2175246
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;

    monitor-enter v1

    .line 2175247
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175248
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175249
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175250
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175251
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidContentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidContentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidContentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;)V

    .line 2175252
    move-object v0, v3

    .line 2175253
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175254
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175255
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175256
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2175257
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175258
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175259
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175260
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175261
    const-string v1, "friendversary_polaroids_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2175262
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 2175263
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v3

    .line 2175264
    iput-object v3, v2, LX/39x;->q:LX/0Px;

    .line 2175265
    move-object v2, v2

    .line 2175266
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    .line 2175267
    iput-object v0, v3, LX/23u;->m:Ljava/lang/String;

    .line 2175268
    move-object v0, v3

    .line 2175269
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2175270
    iput-object v1, v0, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2175271
    move-object v0, v0

    .line 2175272
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2175273
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 2175274
    move-object v0, v0

    .line 2175275
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2175276
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175277
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175278
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175279
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175280
    const/4 v0, 0x0

    return-object v0

    .line 2175281
    :cond_1
    const-string v1, "friendversary_polaroids"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175282
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryPolaroidContentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2175283
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2175284
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175285
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175286
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2175287
    const-string v2, "friendversary_polaroids"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "friendversary_polaroids_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2175288
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2175289
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2175290
    goto :goto_0
.end method
