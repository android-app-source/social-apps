.class public Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;
.super Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "LX/1Ps;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174224
    invoke-direct {p0}, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;-><init>()V

    .line 2174225
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2174226
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3x;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<",
            "LX/1Ps;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/C3x;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 2174218
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174219
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2174220
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v3, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    sget-object v6, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v3, v5, v6}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2174221
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 2174222
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2174223
    new-instance v0, LX/C3x;

    const/high16 v6, -0x1000000

    const/4 v12, 0x0

    move-object v3, v2

    move-object v8, v2

    move v9, v7

    move-object v10, v2

    move v11, v4

    invoke-direct/range {v0 .. v12}, LX/C3x;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;ZLcom/facebook/graphql/model/GraphQLImage;IZLcom/facebook/graphql/model/GraphQLImage;ZLcom/facebook/graphql/model/GraphQLImage;IF)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;
    .locals 4

    .prologue
    .line 2174207
    const-class v1, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    monitor-enter v1

    .line 2174208
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174209
    sput-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174210
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174211
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174212
    new-instance p0, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2174213
    move-object v0, p0

    .line 2174214
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174215
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174216
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2174206
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/goodwill/FriendversaryAttachmentHeaderPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3x;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2174202
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174203
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174204
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2174205
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
