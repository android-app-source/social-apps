.class public Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

.field private final f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176287
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2176288
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    .line 2176289
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    .line 2176290
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    .line 2176291
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    .line 2176292
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    .line 2176293
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    .line 2176294
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;
    .locals 10

    .prologue
    .line 2176295
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;

    monitor-enter v1

    .line 2176296
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176297
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176298
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176299
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176300
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;)V

    .line 2176301
    move-object v0, v3

    .line 2176302
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176303
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176304
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176305
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2176306
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176307
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2176308
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2176309
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2176310
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 2176311
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    .line 2176312
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v3

    .line 2176313
    iput-object v3, v2, LX/39x;->q:LX/0Px;

    .line 2176314
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    .line 2176315
    iput-object v0, v3, LX/23u;->m:Ljava/lang/String;

    .line 2176316
    move-object v0, v3

    .line 2176317
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2176318
    iput-object v1, v0, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2176319
    move-object v0, v0

    .line 2176320
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2176321
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 2176322
    move-object v0, v0

    .line 2176323
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2176324
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2176325
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 2176326
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v3, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2176327
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2176328
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2176329
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2176330
    const/4 v0, 0x0

    return-object v0

    .line 2176331
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackStarversaryGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2176332
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176333
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2176334
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    .line 2176335
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2176336
    :cond_0
    const/4 v0, 0x0

    .line 2176337
    :goto_0
    return v0

    :cond_1
    const-string v1, "faceversary_card_collage"

    .line 2176338
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2176339
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
