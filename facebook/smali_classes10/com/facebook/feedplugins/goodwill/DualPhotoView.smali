.class public Lcom/facebook/feedplugins/goodwill/DualPhotoView;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/C4f;

.field private b:LX/F27;

.field private c:LX/F27;

.field private d:LX/4Ac;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2174097
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2174098
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a(Landroid/content/Context;)V

    .line 2174099
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2174100
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2174101
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a(Landroid/content/Context;)V

    .line 2174102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2174103
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2174104
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a(Landroid/content/Context;)V

    .line 2174105
    return-void
.end method

.method private a(LX/F27;LX/1aX;LX/1aZ;)V
    .locals 1

    .prologue
    .line 2174138
    invoke-virtual {p2, p3}, LX/1aX;->a(LX/1aZ;)V

    .line 2174139
    invoke-virtual {p2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2174140
    invoke-virtual {p2}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/F27;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2174141
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->postInvalidate()V

    .line 2174142
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2174106
    new-instance v0, LX/C4f;

    invoke-direct {v0}, LX/C4f;-><init>()V

    iput-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    .line 2174107
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2174108
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->c:I

    int-to-float v1, v1

    invoke-static {p1, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 2174109
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v2, v2, LX/C4f;->d:I

    int-to-float v2, v2

    invoke-static {p1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 2174110
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v3, v3, LX/C4f;->f:I

    int-to-float v3, v3

    invoke-static {p1, v3}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v3

    .line 2174111
    new-instance v4, LX/F27;

    invoke-direct {v4, v0, v2, v3, v1}, LX/F27;-><init>(Landroid/content/res/Resources;III)V

    iput-object v4, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->b:LX/F27;

    .line 2174112
    new-instance v4, LX/F27;

    invoke-direct {v4, v0, v2, v3, v1}, LX/F27;-><init>(Landroid/content/res/Resources;III)V

    iput-object v4, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->c:LX/F27;

    .line 2174113
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    invoke-virtual {p0, v1}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->setDualPhotoViewConfig(LX/C4f;)V

    .line 2174114
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v3, 0x7f0a0863

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2174115
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2174116
    move-object v1, v1

    .line 2174117
    const v2, 0x7f020aa6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2174118
    iput-object v2, v1, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 2174119
    move-object v1, v1

    .line 2174120
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    .line 2174121
    new-instance v2, LX/1Uo;

    invoke-direct {v2, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const v4, 0x7f0a0863

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2174122
    iput-object v3, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2174123
    move-object v2, v2

    .line 2174124
    const v3, 0x7f020aa6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2174125
    iput-object v0, v2, LX/1Uo;->h:Landroid/graphics/drawable/Drawable;

    .line 2174126
    move-object v0, v2

    .line 2174127
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2174128
    new-instance v2, LX/4Ac;

    invoke-direct {v2}, LX/4Ac;-><init>()V

    iput-object v2, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    .line 2174129
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    new-instance v3, LX/1aX;

    invoke-direct {v3, v1}, LX/1aX;-><init>(LX/1aY;)V

    invoke-virtual {v2, v3}, LX/4Ac;->a(LX/1aX;)V

    .line 2174130
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    new-instance v2, LX/1aX;

    invoke-direct {v2, v0}, LX/1aX;-><init>(LX/1aY;)V

    invoke-virtual {v1, v2}, LX/4Ac;->a(LX/1aX;)V

    .line 2174131
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 2174132
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 2174133
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->b:LX/F27;

    invoke-virtual {v0, p1}, LX/F27;->draw(Landroid/graphics/Canvas;)V

    .line 2174134
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->c:LX/F27;

    invoke-virtual {v0, p1}, LX/F27;->draw(Landroid/graphics/Canvas;)V

    .line 2174135
    return-void
.end method

.method public final invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 2174136
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->invalidate()V

    .line 2174137
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x696b3aa6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2174091
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 2174092
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->a()V

    .line 2174093
    const/16 v1, 0x2d

    const v2, -0x3460f31a    # -2.0847052E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x71c06090

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2174094
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 2174095
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    invoke-virtual {v1}, LX/4Ac;->b()V

    .line 2174096
    const/16 v1, 0x2d

    const v2, 0x47b145a6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2174088
    invoke-super {p0}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 2174089
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->a()V

    .line 2174090
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 2174081
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 2174082
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2174083
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->b:LX/F27;

    div-int/lit8 v2, v0, 0x2

    iget v3, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->e:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->h:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->k:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->f:I

    invoke-virtual {v1, v2, v3}, LX/F27;->a(II)V

    .line 2174084
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->c:LX/F27;

    div-int/lit8 v0, v0, 0x2

    iget v2, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->e:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->f:I

    invoke-virtual {v1, v0, v2}, LX/F27;->a(II)V

    .line 2174085
    iget v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->g:I

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->f:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->h:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->i:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->j:I

    add-int/2addr v0, v1

    .line 2174086
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->setMeasuredDimension(II)V

    .line 2174087
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2174078
    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 2174079
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    invoke-virtual {v0}, LX/4Ac;->b()V

    .line 2174080
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x37f6edb2

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2174077
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    invoke-virtual {v2, p1}, LX/4Ac;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    const v2, 0x5ac05855

    invoke-static {v2, v1}, LX/02F;->a(II)V

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDualPhotoViewConfig(LX/C4f;)V
    .locals 2

    .prologue
    .line 2174059
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    .line 2174060
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->i:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->e:I

    .line 2174061
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->g:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->f:I

    .line 2174062
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->h:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->g:I

    .line 2174063
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->c:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->h:I

    .line 2174064
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->f:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->i:I

    .line 2174065
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->e:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->j:I

    .line 2174066
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->d:I

    int-to-float v1, v1

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->k:I

    .line 2174067
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->b:LX/F27;

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->h:I

    invoke-virtual {v0, v1}, LX/F27;->c(I)V

    .line 2174068
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->c:LX/F27;

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->h:I

    invoke-virtual {v0, v1}, LX/F27;->c(I)V

    .line 2174069
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->b:LX/F27;

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->i:I

    invoke-virtual {v0, v1}, LX/F27;->b(I)V

    .line 2174070
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->c:LX/F27;

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->i:I

    invoke-virtual {v0, v1}, LX/F27;->b(I)V

    .line 2174071
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->b:LX/F27;

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->k:I

    invoke-virtual {v0, v1}, LX/F27;->a(I)V

    .line 2174072
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->c:LX/F27;

    iget v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->k:I

    invoke-virtual {v0, v1}, LX/F27;->a(I)V

    .line 2174073
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->b:LX/F27;

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->a:F

    invoke-virtual {v0, v1}, LX/F27;->a(F)V

    .line 2174074
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->c:LX/F27;

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a:LX/C4f;

    iget v1, v1, LX/C4f;->b:F

    invoke-virtual {v0, v1}, LX/F27;->a(F)V

    .line 2174075
    invoke-virtual {p0}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->requestLayout()V

    .line 2174076
    return-void
.end method

.method public setPolaroidLeft(LX/1aZ;)V
    .locals 3

    .prologue
    .line 2174051
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->b:LX/F27;

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a(LX/F27;LX/1aX;LX/1aZ;)V

    .line 2174052
    return-void
.end method

.method public setPolaroidRight(LX/1aZ;)V
    .locals 3

    .prologue
    .line 2174057
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->c:LX/F27;

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/4Ac;->b(I)LX/1aX;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->a(LX/F27;LX/1aX;LX/1aZ;)V

    .line 2174058
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2174053
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->d:LX/4Ac;

    invoke-virtual {v1, p1}, LX/4Ac;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2174054
    :cond_0
    :goto_0
    return v0

    .line 2174055
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->b:LX/F27;

    if-eq p1, v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoView;->c:LX/F27;

    if-eq p1, v1, :cond_0

    .line 2174056
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
