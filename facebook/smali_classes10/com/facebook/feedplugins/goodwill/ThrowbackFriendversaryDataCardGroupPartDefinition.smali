.class public Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

.field private final d:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

.field private final e:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175160
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2175161
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;

    .line 2175162
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    .line 2175163
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;

    .line 2175164
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    .line 2175165
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    .line 2175166
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    .line 2175167
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;
    .locals 10

    .prologue
    .line 2175168
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;

    monitor-enter v1

    .line 2175169
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175170
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175171
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175172
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175173
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;)V

    .line 2175174
    move-object v0, v3

    .line 2175175
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175176
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175177
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175178
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2175179
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175180
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175181
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175182
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175183
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175184
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    .line 2175185
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2175186
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->n()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPointsConnection;->a()LX/0Px;

    move-result-object v2

    .line 2175187
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;

    .line 2175188
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackDataPoint;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2175189
    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardDataPointPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175190
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2175191
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175192
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175193
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryDataCardGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175194
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2175195
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175196
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175197
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175198
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    const-string v1, "friendversary_card_data"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
