.class public Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

.field private final f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175107
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2175108
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    .line 2175109
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    .line 2175110
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    .line 2175111
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    .line 2175112
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    .line 2175113
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    .line 2175114
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;
    .locals 10

    .prologue
    .line 2175115
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;

    monitor-enter v1

    .line 2175116
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175117
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175118
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175119
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175120
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;)V

    .line 2175121
    move-object v0, v3

    .line 2175122
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175123
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175124
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175125
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2175126
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175127
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175128
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175129
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175130
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    .line 2175131
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    .line 2175132
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v3

    .line 2175133
    iput-object v3, v2, LX/39x;->q:LX/0Px;

    .line 2175134
    new-instance v3, LX/23u;

    invoke-direct {v3}, LX/23u;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    .line 2175135
    iput-object v0, v3, LX/23u;->m:Ljava/lang/String;

    .line 2175136
    move-object v0, v3

    .line 2175137
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2175138
    iput-object v1, v0, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2175139
    move-object v0, v0

    .line 2175140
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2175141
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 2175142
    move-object v0, v0

    .line 2175143
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2175144
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2175145
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 2175146
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v3, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175147
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175148
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175149
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175150
    const/4 v0, 0x0

    return-object v0

    .line 2175151
    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendversaryCollageGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackCollageAttachmentPartDefinition;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2175152
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2175153
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175154
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175155
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2175156
    const-string v2, "friendversary_card_collage"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "friendversary_card_collage_ipb"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2175157
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2175158
    goto :goto_0

    :cond_2
    move v0, v1

    .line 2175159
    goto :goto_0
.end method
