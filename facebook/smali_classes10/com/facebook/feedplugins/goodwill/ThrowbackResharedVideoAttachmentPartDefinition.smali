.class public Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2176255
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3ea00000    # -14.0f

    .line 2176256
    iput v1, v0, LX/1UY;->c:F

    .line 2176257
    move-object v0, v0

    .line 2176258
    const/high16 v1, -0x3ec00000    # -12.0f

    .line 2176259
    iput v1, v0, LX/1UY;->d:F

    .line 2176260
    move-object v0, v0

    .line 2176261
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176251
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2176252
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    .line 2176253
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2176254
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;
    .locals 5

    .prologue
    .line 2176240
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 2176241
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176242
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176243
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176244
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176245
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2176246
    move-object v0, p0

    .line 2176247
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176248
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176249
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2176239
    sget-object v0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2176233
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176234
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    new-instance v1, LX/33s;

    invoke-direct {v1, p2}, LX/33s;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2176235
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2176236
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2176237
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176238
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    new-instance v1, LX/33s;

    invoke-direct {v1, p1}, LX/33s;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a(LX/33s;)Z

    move-result v0

    return v0
.end method
