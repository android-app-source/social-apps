.class public Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/EsN;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/EsN;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175495
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2175496
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;->d:LX/EsN;

    .line 2175497
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;->e:LX/1V0;

    .line 2175498
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2175499
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x40000000    # -2.0f

    .line 2175500
    iput v1, v0, LX/1UY;->c:F

    .line 2175501
    move-object v0, v0

    .line 2175502
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    .line 2175503
    invoke-static {p2}, LX/Es3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2175504
    new-instance v2, LX/1X6;

    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v2, v1, v0, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 2175505
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;->d:LX/EsN;

    const/4 v1, 0x0

    .line 2175506
    new-instance v3, LX/EsM;

    invoke-direct {v3, v0}, LX/EsM;-><init>(LX/EsN;)V

    .line 2175507
    iget-object v4, v0, LX/EsN;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EsL;

    .line 2175508
    if-nez v4, :cond_0

    .line 2175509
    new-instance v4, LX/EsL;

    invoke-direct {v4, v0}, LX/EsL;-><init>(LX/EsN;)V

    .line 2175510
    :cond_0
    invoke-static {v4, p1, v1, v1, v3}, LX/EsL;->a$redex0(LX/EsL;LX/1De;IILX/EsM;)V

    .line 2175511
    move-object v3, v4

    .line 2175512
    move-object v1, v3

    .line 2175513
    move-object v0, v1

    .line 2175514
    iget-object v1, v0, LX/EsL;->a:LX/EsM;

    iput-object p2, v1, LX/EsM;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175515
    iget-object v1, v0, LX/EsL;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2175516
    move-object v0, v0

    .line 2175517
    iget-object v1, v0, LX/EsL;->a:LX/EsM;

    iput-object p3, v1, LX/EsM;->b:LX/1Pb;

    .line 2175518
    iget-object v1, v0, LX/EsL;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/util/BitSet;->set(I)V

    .line 2175519
    move-object v0, v0

    .line 2175520
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2175521
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;
    .locals 6

    .prologue
    .line 2175522
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 2175523
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175524
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175525
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175526
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175527
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EsN;->a(LX/0QB;)LX/EsN;

    move-result-object v4

    check-cast v4, LX/EsN;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/EsN;LX/1V0;)V

    .line 2175528
    move-object v0, p0

    .line 2175529
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175530
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175531
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175532
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2175533
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2175534
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pb;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pb;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2175535
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2175536
    if-eqz p1, :cond_0

    .line 2175537
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175538
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2175539
    :goto_0
    return v0

    .line 2175540
    :cond_1
    invoke-static {p1}, LX/Es3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2175541
    if-nez v0, :cond_2

    move v0, v1

    .line 2175542
    goto :goto_0

    .line 2175543
    :cond_2
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 2175544
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2175545
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175546
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
