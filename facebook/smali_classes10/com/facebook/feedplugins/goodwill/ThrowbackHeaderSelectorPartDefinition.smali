.class public Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175345
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2175346
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    .line 2175347
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    .line 2175348
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    .line 2175349
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;
    .locals 6

    .prologue
    .line 2175354
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    monitor-enter v1

    .line 2175355
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175356
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175357
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175358
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175359
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;)V

    .line 2175360
    move-object v0, p0

    .line 2175361
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175362
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175363
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2175351
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175352
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2175353
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2175350
    const/4 v0, 0x1

    return v0
.end method
