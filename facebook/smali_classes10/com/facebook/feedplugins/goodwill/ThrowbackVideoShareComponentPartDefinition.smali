.class public Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<+",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static final e:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final f:LX/Et0;

.field private final g:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, -0x40000000    # -2.0f

    .line 2176966
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    .line 2176967
    iput v2, v0, LX/1UY;->b:F

    .line 2176968
    move-object v0, v0

    .line 2176969
    const/high16 v1, -0x3f200000    # -7.0f

    .line 2176970
    iput v1, v0, LX/1UY;->c:F

    .line 2176971
    move-object v0, v0

    .line 2176972
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->d:LX/1Ua;

    .line 2176973
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    .line 2176974
    iput v2, v0, LX/1UY;->b:F

    .line 2176975
    move-object v0, v0

    .line 2176976
    iput v2, v0, LX/1UY;->c:F

    .line 2176977
    move-object v0, v0

    .line 2176978
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->e:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Et0;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176962
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2176963
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->f:LX/Et0;

    .line 2176964
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->g:LX/1V0;

    .line 2176965
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2176943
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->f:LX/Et0;

    const/4 v1, 0x0

    .line 2176944
    new-instance v2, LX/Esz;

    invoke-direct {v2, v0}, LX/Esz;-><init>(LX/Et0;)V

    .line 2176945
    sget-object v3, LX/Et0;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Esy;

    .line 2176946
    if-nez v3, :cond_0

    .line 2176947
    new-instance v3, LX/Esy;

    invoke-direct {v3}, LX/Esy;-><init>()V

    .line 2176948
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Esy;->a$redex0(LX/Esy;LX/1De;IILX/Esz;)V

    .line 2176949
    move-object v2, v3

    .line 2176950
    move-object v1, v2

    .line 2176951
    move-object v0, v1

    .line 2176952
    iget-object v1, v0, LX/Esy;->a:LX/Esz;

    iput-object p2, v1, LX/Esz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176953
    iget-object v1, v0, LX/Esy;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2176954
    move-object v0, v0

    .line 2176955
    iget-object v1, v0, LX/Esy;->a:LX/Esz;

    iput-object p3, v1, LX/Esz;->b:LX/1Pm;

    .line 2176956
    iget-object v1, v0, LX/Esy;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2176957
    move-object v0, v0

    .line 2176958
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v2

    .line 2176959
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2176960
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/Esx;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    .line 2176961
    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->g:LX/1V0;

    new-instance v5, LX/1X6;

    if-eqz v3, :cond_1

    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->d:LX/1Ua;

    move-object v1, v0

    :goto_0
    if-eqz v3, :cond_2

    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    :goto_1
    invoke-direct {v5, p2, v1, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-virtual {v4, p1, p3, v5, v2}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->e:LX/1Ua;

    move-object v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;
    .locals 6

    .prologue
    .line 2176979
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;

    monitor-enter v1

    .line 2176980
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176981
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176982
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176983
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176984
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Et0;->a(LX/0QB;)LX/Et0;

    move-result-object v4

    check-cast v4, LX/Et0;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;-><init>(Landroid/content/Context;LX/Et0;LX/1V0;)V

    .line 2176985
    move-object v0, p0

    .line 2176986
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176987
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176988
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2176942
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2176941
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackVideoShareComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2176936
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2176937
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2176938
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v0}, LX/Esx;->b(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;

    move-result-object v0

    .line 2176939
    if-nez v0, :cond_0

    move v0, v1

    .line 2176940
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->l()Lcom/facebook/graphql/enums/GraphQLGoodwillVideoCampaignTypeEnum;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->j()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillVideoCampaign;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2176934
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176935
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
