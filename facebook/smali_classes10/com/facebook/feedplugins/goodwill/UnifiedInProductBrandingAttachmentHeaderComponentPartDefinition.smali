.class public Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/Et6;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2177167
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3ed00000    # -11.0f

    .line 2177168
    iput v1, v0, LX/1UY;->d:F

    .line 2177169
    move-object v0, v0

    .line 2177170
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Et6;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2177171
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2177172
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->e:LX/Et6;

    .line 2177173
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->f:LX/1V0;

    .line 2177174
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2177175
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->e:LX/Et6;

    const/4 v1, 0x0

    .line 2177176
    new-instance v2, LX/Et5;

    invoke-direct {v2, v0}, LX/Et5;-><init>(LX/Et6;)V

    .line 2177177
    iget-object v3, v0, LX/Et6;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Et4;

    .line 2177178
    if-nez v3, :cond_0

    .line 2177179
    new-instance v3, LX/Et4;

    invoke-direct {v3, v0}, LX/Et4;-><init>(LX/Et6;)V

    .line 2177180
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Et4;->a$redex0(LX/Et4;LX/1De;IILX/Et5;)V

    .line 2177181
    move-object v2, v3

    .line 2177182
    move-object v1, v2

    .line 2177183
    move-object v0, v1

    .line 2177184
    iget-object v1, v0, LX/Et4;->a:LX/Et5;

    iput-object p2, v1, LX/Et5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2177185
    iget-object v1, v0, LX/Et4;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2177186
    move-object v0, v0

    .line 2177187
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2177188
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->d:LX/1Ua;

    invoke-direct {v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 2177189
    const-class v1, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2177190
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2177191
    sput-object v2, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2177192
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2177193
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2177194
    new-instance p0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Et6;->a(LX/0QB;)LX/Et6;

    move-result-object v4

    check-cast v4, LX/Et6;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/Et6;LX/1V0;)V

    .line 2177195
    move-object v0, p0

    .line 2177196
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2177197
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2177198
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2177199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2177200
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2177201
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/UnifiedInProductBrandingAttachmentHeaderComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 7

    .prologue
    .line 2177202
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 2177203
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2177204
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2177205
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->w()LX/0Px;

    move-result-object v4

    .line 2177206
    if-eqz v4, :cond_0

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v1

    if-ge v1, v6, :cond_1

    :cond_0
    move v0, v2

    .line 2177207
    :goto_0
    return v0

    :cond_1
    move v3, v2

    .line 2177208
    :goto_1
    if-ge v3, v6, :cond_4

    .line 2177209
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLImage;

    .line 2177210
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v5

    if-lez v5, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    if-gtz v1, :cond_3

    :cond_2
    move v0, v2

    .line 2177211
    goto :goto_0

    .line 2177212
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2177213
    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_5
    move v0, v2

    .line 2177214
    goto :goto_0

    .line 2177215
    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2177216
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2177217
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
