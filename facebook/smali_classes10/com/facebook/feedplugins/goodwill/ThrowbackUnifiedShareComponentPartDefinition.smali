.class public Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/Esr;

.field private final f:LX/1V0;

.field private final g:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x40000000    # -2.0f

    .line 2176571
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    .line 2176572
    iput v1, v0, LX/1UY;->b:F

    .line 2176573
    move-object v0, v0

    .line 2176574
    iput v1, v0, LX/1UY;->c:F

    .line 2176575
    move-object v0, v0

    .line 2176576
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Esr;LX/1V0;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176577
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2176578
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->e:LX/Esr;

    .line 2176579
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->f:LX/1V0;

    .line 2176580
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->g:LX/0Uh;

    .line 2176581
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2176582
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->e:LX/Esr;

    const/4 v1, 0x0

    .line 2176583
    new-instance v2, LX/Esq;

    invoke-direct {v2, v0}, LX/Esq;-><init>(LX/Esr;)V

    .line 2176584
    sget-object v3, LX/Esr;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Esp;

    .line 2176585
    if-nez v3, :cond_0

    .line 2176586
    new-instance v3, LX/Esp;

    invoke-direct {v3}, LX/Esp;-><init>()V

    .line 2176587
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Esp;->a$redex0(LX/Esp;LX/1De;IILX/Esq;)V

    .line 2176588
    move-object v2, v3

    .line 2176589
    move-object v1, v2

    .line 2176590
    move-object v0, v1

    .line 2176591
    iget-object v1, v0, LX/Esp;->a:LX/Esq;

    iput-object p2, v1, LX/Esq;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176592
    iget-object v1, v0, LX/Esp;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2176593
    move-object v0, v0

    .line 2176594
    iget-object v1, v0, LX/Esp;->a:LX/Esq;

    iput-object p3, v1, LX/Esq;->b:LX/1Pm;

    .line 2176595
    iget-object v1, v0, LX/Esp;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2176596
    move-object v0, v0

    .line 2176597
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2176598
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->f:LX/1V0;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->d:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;
    .locals 7

    .prologue
    .line 2176559
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;

    monitor-enter v1

    .line 2176560
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176561
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176562
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176563
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176564
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Esr;->a(LX/0QB;)LX/Esr;

    move-result-object v4

    check-cast v4, LX/Esr;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;-><init>(Landroid/content/Context;LX/Esr;LX/1V0;LX/0Uh;)V

    .line 2176565
    move-object v0, p0

    .line 2176566
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176567
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176568
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176569
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2176570
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2176556
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2176554
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareComponentPartDefinition;->g:LX/0Uh;

    const/16 v1, 0x4e4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    .line 2176555
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2176557
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176558
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
