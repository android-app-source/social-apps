.class public Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

.field private final d:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

.field private final f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175099
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2175100
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    .line 2175101
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    .line 2175102
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    .line 2175103
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    .line 2175104
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    .line 2175105
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;

    .line 2175106
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;
    .locals 10

    .prologue
    .line 2175088
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;

    monitor-enter v1

    .line 2175089
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175090
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175091
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175092
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175093
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;)V

    .line 2175094
    move-object v0, v3

    .line 2175095
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175096
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175097
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175098
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2175042
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    const/4 v4, 0x0

    .line 2175043
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175044
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175045
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2175046
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v2

    .line 2175047
    new-instance v1, LX/EsD;

    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v3

    invoke-direct {v1, v3}, LX/EsD;-><init>(Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EsE;

    .line 2175048
    const-string v3, "friend_birthday_card"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2175049
    iget-boolean v3, v1, LX/EsE;->a:Z

    move v1, v3

    .line 2175050
    if-eqz v1, :cond_0

    .line 2175051
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedConfirmationBannerPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175052
    :goto_0
    return-object v4

    .line 2175053
    :cond_0
    const-string v1, "friend_birthday_ipb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "friend_birthday_card"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_1
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2175054
    if-eqz v1, :cond_3

    .line 2175055
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedInProductBrandingHeaderPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175056
    :goto_2
    const-string v1, "friend_birthday_card"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_3
    move v0, v1

    .line 2175057
    if-eqz v0, :cond_2

    .line 2175058
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackSimplePhotoAttachmentPartDefinition;

    .line 2175059
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2175060
    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175061
    invoke-static {v1}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v3

    .line 2175062
    new-instance v5, LX/39x;

    invoke-direct {v5}, LX/39x;-><init>()V

    .line 2175063
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->r()LX/0Px;

    move-result-object p3

    .line 2175064
    iput-object p3, v5, LX/39x;->q:LX/0Px;

    .line 2175065
    new-instance p3, LX/23u;

    invoke-direct {p3}, LX/23u;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    .line 2175066
    iput-object v1, p3, LX/23u;->m:Ljava/lang/String;

    .line 2175067
    move-object v1, p3

    .line 2175068
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->z()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 2175069
    iput-object v3, v1, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2175070
    move-object v1, v1

    .line 2175071
    invoke-virtual {v5}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2175072
    iput-object v3, v1, LX/23u;->k:LX/0Px;

    .line 2175073
    move-object v1, v1

    .line 2175074
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2175075
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 2175076
    invoke-virtual {p2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v5, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    move-object v1, v1

    .line 2175077
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175078
    :cond_2
    const-string v0, "friend_birthday_card"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2175079
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/PostPhotoFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2175080
    :cond_3
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2175081
    :cond_4
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackFriendsBirthdayGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/MessageAndPostPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 2175082
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2175083
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175084
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175085
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;

    move-result-object v2

    .line 2175086
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v3

    .line 2175087
    invoke-static {v0}, LX/23B;->f(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "friend_birthday"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "friend_birthday_ipb"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "friend_birthday_card"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
