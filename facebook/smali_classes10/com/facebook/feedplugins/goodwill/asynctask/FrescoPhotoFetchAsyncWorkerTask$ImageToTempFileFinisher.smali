.class public final Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;
.super Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EtB",
        "<",
        "Landroid/graphics/Bitmap;",
        "Landroid/net/Uri;",
        ">.Finisher<",
        "Landroid/graphics/Bitmap;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:LX/EtD;


# direct methods
.method public constructor <init>(LX/EtD;)V
    .locals 0

    .prologue
    .line 2177331
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;->d:LX/EtD;

    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;-><init>(LX/EtB;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2177332
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;->d:LX/EtD;

    iget-object v0, v0, LX/EtD;->d:LX/1Er;

    const-string v2, ".facebook_"

    const-string v3, ".jpg"

    sget-object v4, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v0, v2, v3, v4}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v2

    .line 2177333
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;->d:LX/EtD;

    iget-object v3, v0, LX/EtD;->e:LX/9iU;

    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/async/AsyncWorkerTask$Finisher;->a:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0, v2}, LX/9iU;->a(Landroid/graphics/Bitmap;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;->b:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2177334
    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    .line 2177335
    goto :goto_0

    .line 2177336
    :catch_0
    iput-object v1, p0, Lcom/facebook/feedplugins/goodwill/asynctask/FrescoPhotoFetchAsyncWorkerTask$ImageToTempFileFinisher;->b:Ljava/lang/Object;

    goto :goto_1
.end method
