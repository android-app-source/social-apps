.class public Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2174589
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v0

    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/VisibilityPartDefinition;",
            "Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174567
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2174568
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->b:LX/0Ot;

    .line 2174569
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2174570
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;
    .locals 6

    .prologue
    .line 2174578
    const-class v1, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;

    monitor-enter v1

    .line 2174579
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174580
    sput-object v2, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174581
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174582
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174583
    new-instance v5, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/VisibilityPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/VisibilityPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const/16 p0, 0xa4c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;-><init>(Lcom/facebook/multirow/parts/VisibilityPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/0Ot;)V

    .line 2174584
    move-object v0, v5

    .line 2174585
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174586
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174587
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2174577
    sget-object v0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2174573
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174574
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/33s;

    invoke-direct {v1, p2}, LX/33s;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2174575
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->a:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2174576
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2174572
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;

    new-instance v1, LX/33s;

    invoke-direct {v1, p1}, LX/33s;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/video/RichVideoRowPartDefinition;->a(LX/33s;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2174571
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feedplugins/goodwill/RichVideoWithBackgroundOverridePartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
