.class public Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/1VL;

.field private final f:LX/0qn;

.field private final g:LX/23t;

.field private final h:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2176197
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3e800000    # -16.0f

    .line 2176198
    iput v1, v0, LX/1UY;->c:F

    .line 2176199
    move-object v0, v0

    .line 2176200
    const/high16 v1, -0x3ec00000    # -12.0f

    .line 2176201
    iput v1, v0, LX/1UY;->d:F

    .line 2176202
    move-object v0, v0

    .line 2176203
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/23t;LX/1V0;LX/0qn;LX/1VL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2176191
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2176192
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->g:LX/23t;

    .line 2176193
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->h:LX/1V0;

    .line 2176194
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->f:LX/0qn;

    .line 2176195
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->e:LX/1VL;

    .line 2176196
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2176188
    new-instance v0, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    sget-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->d:LX/1Ua;

    invoke-direct {v0, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2176189
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->g:LX/23t;

    invoke-virtual {v1, p1}, LX/23t;->c(LX/1De;)LX/240;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/240;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/240;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/240;->a(LX/1Pe;)LX/240;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2176190
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->h:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;
    .locals 9

    .prologue
    .line 2176177
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;

    monitor-enter v1

    .line 2176178
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2176179
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2176180
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2176181
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2176182
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/23t;->a(LX/0QB;)LX/23t;

    move-result-object v5

    check-cast v5, LX/23t;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v7

    check-cast v7, LX/0qn;

    invoke-static {v0}, LX/1VL;->b(LX/0QB;)LX/1VL;

    move-result-object v8

    check-cast v8, LX/1VL;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;-><init>(Landroid/content/Context;LX/23t;LX/1V0;LX/0qn;LX/1VL;)V

    .line 2176183
    move-object v0, v3

    .line 2176184
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2176185
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2176186
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2176187
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2176176
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2176166
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pe;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pe;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2176169
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176170
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->f:LX/0qn;

    invoke-static {v0, p1}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentComponentPartDefinition;->a(LX/0qn;Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    .line 2176171
    if-eqz v0, :cond_0

    .line 2176172
    const/4 v0, 0x0

    .line 2176173
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->e:LX/1VL;

    .line 2176174
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2176175
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1, v0}, LX/1VL;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2176167
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2176168
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
