.class public Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;
.super Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final d:LX/1Uf;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2173961
    invoke-direct {p0}, Lcom/facebook/feedplugins/facebookvoice/FacebookVoiceBasePartDefinition;-><init>()V

    .line 2173962
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2173963
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 2173964
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->d:LX/1Uf;

    .line 2173965
    return-void
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3x;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;",
            ">;)",
            "LX/C3x;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x0

    .line 2173947
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2173948
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    .line 2173949
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    .line 2173950
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2173951
    const v1, 0x7f0d0bde

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    sget-object v5, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-direct {v3, p2, v5}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2173952
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 2173953
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->d:LX/1Uf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/1eD;->c(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1eE;

    move-result-object v3

    invoke-virtual {v2, v3, v4, v10}, LX/1Uf;->a(LX/1eE;ZLX/0lF;)Landroid/text/Spannable;

    move-result-object v2

    .line 2173954
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 2173955
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    move-object v3, v10

    .line 2173956
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2173957
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    .line 2173958
    new-instance v0, LX/C3x;

    const/high16 v6, -0x1000000

    const/4 v12, 0x0

    move v9, v7

    move v11, v7

    invoke-direct/range {v0 .. v12}, LX/C3x;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Lcom/facebook/graphql/model/GraphQLTextWithEntities;ZLcom/facebook/graphql/model/GraphQLImage;IZLcom/facebook/graphql/model/GraphQLImage;ZLcom/facebook/graphql/model/GraphQLImage;IF)V

    return-object v0

    :cond_2
    move-object v1, v10

    .line 2173959
    goto :goto_0

    :cond_3
    move-object v2, v10

    .line 2173960
    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;
    .locals 6

    .prologue
    .line 2173928
    const-class v1, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;

    monitor-enter v1

    .line 2173929
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2173930
    sput-object v2, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2173931
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173932
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2173933
    new-instance p0, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v5

    check-cast v5, LX/1Uf;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1Uf;)V

    .line 2173934
    move-object v0, p0

    .line 2173935
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2173936
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173937
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2173938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2173946
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/goodwill/AnniversaryHeaderPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3x;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2173939
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2173940
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2173941
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    .line 2173942
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2173943
    :goto_0
    return v0

    .line 2173944
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2173945
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
