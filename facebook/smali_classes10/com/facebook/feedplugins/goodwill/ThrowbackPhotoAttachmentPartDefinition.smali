.class public Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "LX/EsP;",
        "TE;",
        "LX/3Xj;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/1V8;

.field private final c:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/Context;LX/1V8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175565
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2175566
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->a:Landroid/content/Context;

    .line 2175567
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->b:LX/1V8;

    .line 2175568
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    .line 2175569
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2175570
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;
    .locals 7

    .prologue
    .line 2175571
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;

    monitor-enter v1

    .line 2175572
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175573
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175574
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175575
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175576
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/1V7;->a(LX/0QB;)LX/1V7;

    move-result-object v6

    check-cast v6, LX/1V8;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Landroid/content/Context;LX/1V8;)V

    .line 2175577
    move-object v0, p0

    .line 2175578
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175579
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175580
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175581
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2175582
    sget-object v0, LX/3Xj;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2175583
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2175584
    invoke-static {p2}, LX/Es3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2175585
    if-nez v2, :cond_0

    .line 2175586
    :goto_0
    return-object v1

    .line 2175587
    :cond_0
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175588
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2175589
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->c:Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentPartDefinition;

    invoke-interface {p1, v3, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2175590
    invoke-static {}, LX/1UY;->d()LX/1UY;

    move-result-object v3

    const/high16 v4, -0x40000000    # -2.0f

    .line 2175591
    iput v4, v3, LX/1UY;->c:F

    .line 2175592
    move-object v3, v3

    .line 2175593
    invoke-virtual {v3}, LX/1UY;->i()LX/1Ua;

    move-result-object v3

    .line 2175594
    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v5, LX/1X6;

    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    sget-object v7, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v5, v6, v3, v7}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v4, v5}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2175595
    invoke-static {v2}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2175596
    invoke-static {v0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-static {v0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 2175597
    :goto_1
    if-eqz v0, :cond_4

    .line 2175598
    const/4 v4, 0x0

    .line 2175599
    :goto_2
    move v2, v4

    .line 2175600
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2175601
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175602
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->G()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    const/4 v3, 0x0

    .line 2175603
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 2175604
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    .line 2175605
    :cond_1
    :goto_3
    move-object v0, v3

    .line 2175606
    :goto_4
    new-instance v1, LX/EsP;

    invoke-direct {v1, v0, v2}, LX/EsP;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    .line 2175607
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 2175608
    goto :goto_4

    .line 2175609
    :cond_4
    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0034

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2175610
    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->b:LX/1V8;

    sget-object v6, LX/1Ua;->b:LX/1Ua;

    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    int-to-float v4, v4

    invoke-virtual {v5, v6, v7, v4}, LX/1V8;->a(LX/1Ua;Lcom/facebook/feed/rows/core/props/FeedProps;F)I

    move-result v4

    .line 2175611
    div-int/lit8 v4, v4, 0x2

    goto :goto_2

    .line 2175612
    :cond_5
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2175613
    :try_start_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 2175614
    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPhotoAttachmentPartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f012b

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 p1, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v7, p1

    invoke-virtual {v5, v6, v4, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_3

    .line 2175615
    :catch_0
    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x78065e92

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2175616
    check-cast p2, LX/EsP;

    check-cast p4, LX/3Xj;

    .line 2175617
    iget-object v1, p2, LX/EsP;->a:Ljava/lang/String;

    .line 2175618
    iget-object v2, p4, LX/3Xj;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2175619
    invoke-virtual {p4}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f082a13

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v1, p0, p1

    invoke-static {v2, p0}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2175620
    invoke-virtual {p4}, LX/24d;->getPhotoAttachmentView()Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2175621
    iget-object p0, p4, LX/3Xj;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v2}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2175622
    iget v1, p2, LX/EsP;->b:I

    const/4 p2, 0x0

    .line 2175623
    iget-object v2, p4, LX/3Xj;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p4}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0b1cf4

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    invoke-virtual {p4}, LX/3Xj;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p3, 0x7f0b1cf4

    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    add-int/2addr p1, v1

    invoke-virtual {v2, p2, p0, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2175624
    const/16 v1, 0x1f

    const v2, 0x62c4b8ee

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2175625
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v0, 0x0

    .line 2175626
    if-eqz p1, :cond_0

    .line 2175627
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2175628
    if-nez v1, :cond_1

    .line 2175629
    :cond_0
    :goto_0
    return v0

    .line 2175630
    :cond_1
    invoke-static {p1}, LX/Es3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2175631
    if-eqz v1, :cond_0

    .line 2175632
    invoke-static {v1}, Lcom/facebook/feedplugins/attachments/photo/PhotoAttachmentComponentSpec;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    goto :goto_0
.end method
