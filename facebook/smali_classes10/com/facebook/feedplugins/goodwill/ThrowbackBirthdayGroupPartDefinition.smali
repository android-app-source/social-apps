.class public Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

.field private final d:Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

.field private final e:Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;

.field private final f:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174783
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2174784
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->f:LX/0Uh;

    .line 2174785
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    .line 2174786
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    .line 2174787
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;

    .line 2174788
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

    .line 2174789
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;

    .line 2174790
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;
    .locals 10

    .prologue
    .line 2174791
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;

    monitor-enter v1

    .line 2174792
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174793
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174794
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174795
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174796
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;-><init>(LX/0Uh;Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;)V

    .line 2174797
    move-object v0, v3

    .line 2174798
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174799
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174800
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174801
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2174766
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174767
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackHeaderSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174768
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->f:LX/0Uh;

    const/16 v1, 0x4d9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174769
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174770
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/UploadProfilePicturePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174771
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2174772
    const/4 v0, 0x0

    return-object v0

    .line 2174773
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayGroupPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepilePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2174774
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2174775
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174776
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2174777
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->t()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;

    move-result-object v2

    .line 2174778
    invoke-static {v0}, LX/23B;->f(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2174779
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedCampaignsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, -0x20151104

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 2174780
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 2174781
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2174782
    goto :goto_0
.end method
