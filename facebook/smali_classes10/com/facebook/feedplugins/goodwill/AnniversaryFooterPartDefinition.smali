.class public Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "LX/3VK;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/17W;

.field private final c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/17W;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2173884
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2173885
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2173886
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->b:LX/17W;

    .line 2173887
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    .line 2173888
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2173889
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2173890
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->f:Landroid/content/Context;

    .line 2173891
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;
    .locals 10

    .prologue
    .line 2173917
    const-class v1, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;

    monitor-enter v1

    .line 2173918
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2173919
    sput-object v2, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2173920
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2173921
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2173922
    new-instance v3, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    const-class v9, Landroid/content/Context;

    invoke-interface {v0, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;LX/17W;Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Landroid/content/Context;)V

    .line 2173923
    move-object v0, v3

    .line 2173924
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2173925
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2173926
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2173927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2173916
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 2173915
    if-eqz p0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const-string p0, ""

    goto :goto_0
.end method

.method private a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/Void;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<",
            "LX/1Ps;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 2173898
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2173899
    move-object v10, v0

    check-cast v10, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    .line 2173900
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2173901
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2173902
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/OneButtonFooterStylerPartDefinition;

    invoke-interface {p1, v2, v11}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2173903
    const v2, 0x7f0d175f

    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v2, v3, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2173904
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v2

    .line 2173905
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/String;

    move-result-object v3

    .line 2173906
    new-instance v0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    iget-object v4, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->f:Landroid/content/Context;

    const v5, 0x7f082a20

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->f:Landroid/content/Context;

    const v6, 0x7f082a22

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->f:Landroid/content/Context;

    const v7, 0x7f082a23

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->f()I

    move-result v7

    const-string v8, "news_feed"

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V

    .line 2173907
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2173908
    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->q()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2173909
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2173910
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2173911
    new-instance v5, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-direct {v5, v1}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;-><init>(Lcom/facebook/graphql/model/GraphQLMedia;)V

    invoke-virtual {v0, v5}, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a(Lcom/facebook/goodwill/composer/GoodwillComposerEvent$GoodwillPhoto;)V

    .line 2173912
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2173913
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/Es1;

    invoke-direct {v2, p0, v0}, LX/Es1;-><init>(Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;Lcom/facebook/goodwill/composer/GoodwillComposerEvent;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2173914
    return-object v11
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2173897
    sget-object v0, LX/3VK;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2173896
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2}, Lcom/facebook/feedplugins/goodwill/AnniversaryFooterPartDefinition;->a(LX/1aD;Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2173892
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2173893
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2173894
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;

    .line 2173895
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaignFeedUnit;->o()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->k()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
