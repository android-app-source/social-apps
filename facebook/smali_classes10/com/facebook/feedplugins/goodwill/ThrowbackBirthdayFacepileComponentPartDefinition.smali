.class public Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "LX/1Pm;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/EsH;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/EsH;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174747
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2174748
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;->d:LX/EsH;

    .line 2174749
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;->e:LX/1V0;

    .line 2174750
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
            ">;",
            "LX/1Pm;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2174704
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174705
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2174706
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;->d:LX/EsH;

    const/4 v1, 0x0

    .line 2174707
    new-instance v2, LX/EsG;

    invoke-direct {v2, v0}, LX/EsG;-><init>(LX/EsH;)V

    .line 2174708
    sget-object v3, LX/EsH;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EsF;

    .line 2174709
    if-nez v3, :cond_0

    .line 2174710
    new-instance v3, LX/EsF;

    invoke-direct {v3}, LX/EsF;-><init>()V

    .line 2174711
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EsF;->a$redex0(LX/EsF;LX/1De;IILX/EsG;)V

    .line 2174712
    move-object v2, v3

    .line 2174713
    move-object v1, v2

    .line 2174714
    move-object v0, v1

    .line 2174715
    iget-object v1, v0, LX/EsF;->a:LX/EsG;

    iput-object p2, v1, LX/EsG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174716
    iget-object v1, v0, LX/EsF;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2174717
    move-object v0, v0

    .line 2174718
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2174719
    const-string v2, "birthday_ipb"

    .line 2174720
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174721
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    new-instance v0, LX/1X6;

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v2

    const/high16 v3, 0x41200000    # 10.0f

    .line 2174722
    iput v3, v2, LX/1UY;->b:F

    .line 2174723
    move-object v2, v2

    .line 2174724
    const/high16 v3, 0x41000000    # 8.0f

    .line 2174725
    iput v3, v2, LX/1UY;->c:F

    .line 2174726
    move-object v2, v2

    .line 2174727
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    invoke-direct {v0, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 2174728
    :goto_0
    iget-object v2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;->e:LX/1V0;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 2174729
    :cond_1
    new-instance v0, LX/1X6;

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    const v3, 0x7f0a00ea

    const/4 v4, -0x1

    invoke-direct {v0, p2, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;
    .locals 6

    .prologue
    .line 2174736
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;

    monitor-enter v1

    .line 2174737
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174738
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174739
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174740
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174741
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EsH;->a(LX/0QB;)LX/EsH;

    move-result-object v4

    check-cast v4, LX/EsH;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;-><init>(Landroid/content/Context;LX/EsH;LX/1V0;)V

    .line 2174742
    move-object v0, p0

    .line 2174743
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174744
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174745
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174746
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2174735
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2174734
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pm;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feedplugins/goodwill/ThrowbackBirthdayFacepileComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pm;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2174732
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174733
    invoke-static {p1}, LX/C4e;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2174730
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2174731
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
