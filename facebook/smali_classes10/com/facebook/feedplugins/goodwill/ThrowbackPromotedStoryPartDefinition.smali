.class public Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackResharedPhotoAttachmentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;

.field private final d:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

.field private final f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/Esi;

.field private final h:Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStorySeeOriginalPartDefinition;

.field private final i:Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackResharedPhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;LX/Esi;Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStorySeeOriginalPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175885
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2175886
    iput-object p8, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->g:LX/Esi;

    .line 2175887
    iput-object p7, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->a:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 2175888
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackResharedPhotoAttachmentPartDefinition;

    .line 2175889
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;

    .line 2175890
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->i:Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;

    .line 2175891
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    .line 2175892
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    .line 2175893
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 2175894
    iput-object p9, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->h:Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStorySeeOriginalPartDefinition;

    .line 2175895
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;
    .locals 13

    .prologue
    .line 2175874
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;

    monitor-enter v1

    .line 2175875
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175876
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175877
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175878
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175879
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedPhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackResharedPhotoAttachmentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedPhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, LX/Esi;->a(LX/0QB;)LX/Esi;

    move-result-object v11

    check-cast v11, LX/Esi;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStorySeeOriginalPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStorySeeOriginalPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStorySeeOriginalPartDefinition;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackResharedPhotoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;LX/Esi;Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStorySeeOriginalPartDefinition;)V

    .line 2175880
    move-object v0, v3

    .line 2175881
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175882
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175883
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175884
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z
    .locals 2

    .prologue
    .line 2175872
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    .line 2175873
    const-string v1, "reshare_status_update"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "reshare_photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "reshare_video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "reshare_multi_photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2175817
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175818
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175819
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175820
    const/4 v2, 0x0

    .line 2175821
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2175822
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotedStoriesConnection;->a()LX/0Px;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2175823
    invoke-static {v1, v2}, LX/0x1;->a(LX/16g;LX/162;)V

    .line 2175824
    :goto_0
    new-instance v2, LX/23u;

    invoke-direct {v2}, LX/23u;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->g()Ljava/lang/String;

    move-result-object v3

    .line 2175825
    iput-object v3, v2, LX/23u;->m:Ljava/lang/String;

    .line 2175826
    move-object v2, v2

    .line 2175827
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    .line 2175828
    iput-object v3, v2, LX/23u;->aH:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2175829
    move-object v2, v2

    .line 2175830
    iput-object v1, v2, LX/23u;->j:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2175831
    move-object v1, v2

    .line 2175832
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->F()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 2175833
    iput-object v3, v2, LX/173;->f:Ljava/lang/String;

    .line 2175834
    move-object v2, v2

    .line 2175835
    invoke-virtual {v2}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 2175836
    iput-object v2, v1, LX/23u;->au:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2175837
    move-object v1, v1

    .line 2175838
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2175839
    const/4 v2, 0x1

    .line 2175840
    invoke-static {v1}, LX/0x1;->a(Ljava/lang/Object;)LX/0x2;

    move-result-object v3

    .line 2175841
    iput-boolean v2, v3, LX/0x2;->l:Z

    .line 2175842
    move-object v1, v1

    .line 2175843
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2175844
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2175845
    invoke-virtual {v1, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2175846
    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2175847
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2175848
    invoke-static {v1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->D()LX/0Px;

    move-result-object v4

    .line 2175849
    iput-object v4, v3, LX/23u;->b:LX/0Px;

    .line 2175850
    move-object v3, v3

    .line 2175851
    invoke-virtual {v3}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2175852
    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2175853
    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2175854
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->h:Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStorySeeOriginalPartDefinition;

    invoke-virtual {p1, v3, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175855
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v3, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175856
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-virtual {p1, v3, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175857
    iget-object v3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v3, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175858
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2175859
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2175860
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    .line 2175861
    const-string v2, "reshare_photo"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2175862
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackResharedPhotoAttachmentPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175863
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 2175864
    :cond_1
    const-string v2, "reshare_video"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2175865
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackResharedVideoAttachmentPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1

    .line 2175866
    :cond_2
    const-string v2, "reshare_multi_photo"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175867
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->i:Lcom/facebook/feedplugins/goodwill/ThrowbackResharedCollageAttachmentComponentPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1

    .line 2175868
    :cond_3
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175869
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->e:Lcom/facebook/feedplugins/graphqlstory/translation/TranslationOrContentSelectorPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175870
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->d:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175871
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->a:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v1, v2

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2175809
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2175810
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175811
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2175812
    invoke-static {v0}, LX/23B;->b(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, LX/23B;->e(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 2175813
    :goto_0
    return v0

    .line 2175814
    :cond_1
    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v0}, LX/23B;->c(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2175815
    goto :goto_0

    .line 2175816
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
