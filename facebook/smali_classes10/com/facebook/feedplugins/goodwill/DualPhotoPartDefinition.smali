.class public Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;
.super Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174048
    invoke-direct {p0, p2}, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;-><init>(LX/1Ad;)V

    .line 2174049
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2174050
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;
    .locals 5

    .prologue
    .line 2174037
    const-class v1, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;

    monitor-enter v1

    .line 2174038
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174039
    sput-object v2, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174040
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174041
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174042
    new-instance p0, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v4

    check-cast v4, LX/1Ad;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1Ad;)V

    .line 2174043
    move-object v0, p0

    .line 2174044
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174045
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174046
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174047
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2174017
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    .line 2174018
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174019
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2174020
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/DualPhotoPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    sget-object v4, LX/1Ua;->a:LX/1Ua;

    invoke-direct {v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2174021
    new-instance v2, LX/C4f;

    invoke-direct {v2}, LX/C4f;-><init>()V

    .line 2174022
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->jC()LX/0Px;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jC()LX/0Px;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p3, v1, v0, v2}, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->a(LX/1Ps;Ljava/lang/String;Ljava/lang/String;LX/C4f;)LX/Es2;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2174023
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 2174024
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174025
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2174026
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2174027
    :goto_0
    return v0

    .line 2174028
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jC()LX/0Px;

    move-result-object v3

    .line 2174029
    if-eqz v3, :cond_1

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v0, v5, :cond_2

    :cond_1
    move v0, v1

    .line 2174030
    goto :goto_0

    :cond_2
    move v2, v1

    .line 2174031
    :goto_1
    if-ge v2, v5, :cond_5

    .line 2174032
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2174033
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v1

    .line 2174034
    goto :goto_0

    .line 2174035
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2174036
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method
