.class public Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EsI;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/23P;


# direct methods
.method public constructor <init>(LX/23P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174807
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2174808
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;->a:LX/23P;

    .line 2174809
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;
    .locals 4

    .prologue
    .line 2174810
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;

    monitor-enter v1

    .line 2174811
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174812
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174813
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174814
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174815
    new-instance p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;-><init>(LX/23P;)V

    .line 2174816
    move-object v0, p0

    .line 2174817
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174818
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174819
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174820
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x53e5df7d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2174821
    check-cast p1, LX/EsI;

    check-cast p4, Landroid/widget/TextView;

    .line 2174822
    iget v1, p1, LX/EsI;->a:I

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2174823
    iget-boolean v1, p1, LX/EsI;->c:Z

    if-eqz v1, :cond_0

    .line 2174824
    iget-object v1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackCallToActionTextPartDefinition;->a:LX/23P;

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2174825
    :cond_0
    iget-object v1, p1, LX/EsI;->b:Ljava/lang/String;

    invoke-virtual {p4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2174826
    const/16 v1, 0x1f

    const v2, -0x608e974f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
