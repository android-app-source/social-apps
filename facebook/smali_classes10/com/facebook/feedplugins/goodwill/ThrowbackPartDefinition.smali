.class public Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

.field private final b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

.field private final c:Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;

.field private final d:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

.field private final f:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2175365
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2175366
    iput-object p5, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    .line 2175367
    iput-object p4, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    .line 2175368
    iput-object p3, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;

    .line 2175369
    iput-object p2, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    .line 2175370
    iput-object p1, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    .line 2175371
    iput-object p6, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    .line 2175372
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;
    .locals 10

    .prologue
    .line 2175373
    const-class v1, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;

    monitor-enter v1

    .line 2175374
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2175375
    sput-object v2, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2175376
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2175377
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2175378
    new-instance v3, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;-><init>(Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;)V

    .line 2175379
    move-object v0, v3

    .line 2175380
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2175381
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2175382
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2175383
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2175384
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175385
    const-string v1, "status_update_ipb"

    .line 2175386
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175387
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "link_ipb"

    .line 2175388
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175389
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "reshare_photo"

    .line 2175390
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175391
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "reshare_status_update"

    .line 2175392
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175393
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "reshare_video"

    .line 2175394
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175395
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v1, "reshare_multi_photo"

    .line 2175396
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175397
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2175398
    :cond_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->e:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueBrandedHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175399
    :goto_0
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->c:Lcom/facebook/feedplugins/goodwill/ThrowbackPromotedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175400
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->f:Lcom/facebook/feedplugins/goodwill/ThrowbackPrivacyLabelComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175401
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->b:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedShareButtonSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175402
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->a:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedSeeMorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2175403
    const/4 v0, 0x0

    return-object v0

    .line 2175404
    :cond_1
    iget-object v0, p0, Lcom/facebook/feedplugins/goodwill/ThrowbackPartDefinition;->d:Lcom/facebook/feedplugins/goodwill/ThrowbackUnifiedHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2175405
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2175406
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2175407
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
