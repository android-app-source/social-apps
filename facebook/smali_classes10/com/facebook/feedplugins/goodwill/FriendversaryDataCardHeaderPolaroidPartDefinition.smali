.class public Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;
.super Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# direct methods
.method public constructor <init>(LX/1Ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2174446
    invoke-direct {p0, p1}, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;-><init>(LX/1Ad;)V

    .line 2174447
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;
    .locals 4

    .prologue
    .line 2174405
    const-class v1, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;

    monitor-enter v1

    .line 2174406
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2174407
    sput-object v2, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2174408
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2174409
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2174410
    new-instance p0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v3

    check-cast v3, LX/1Ad;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;-><init>(LX/1Ad;)V

    .line 2174411
    move-object v0, p0

    .line 2174412
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2174413
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/goodwill/FriendversaryDataCardHeaderPolaroidPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2174414
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2174415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2174431
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Ps;

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 2174432
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174433
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2174434
    new-instance v1, LX/C4f;

    invoke-direct {v1}, LX/C4f;-><init>()V

    .line 2174435
    const v2, -0x3f766666    # -4.3f

    iput v2, v1, LX/C4f;->a:F

    .line 2174436
    const v2, 0x4114cccd    # 9.3f

    iput v2, v1, LX/C4f;->b:F

    .line 2174437
    const/16 v2, 0x34

    iput v2, v1, LX/C4f;->c:I

    .line 2174438
    iput v4, v1, LX/C4f;->d:I

    .line 2174439
    iput v4, v1, LX/C4f;->e:I

    .line 2174440
    const/16 v2, 0x10

    iput v2, v1, LX/C4f;->f:I

    .line 2174441
    const/16 v2, 0x14

    iput v2, v1, LX/C4f;->g:I

    .line 2174442
    const/16 v2, 0xc

    iput v2, v1, LX/C4f;->h:I

    .line 2174443
    iput v3, v1, LX/C4f;->i:I

    .line 2174444
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v2

    .line 2174445
    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p3, v3, v0, v1}, Lcom/facebook/feedplugins/goodwill/DualPhotoBasePartDefinition;->a(LX/1Ps;Ljava/lang/String;Ljava/lang/String;LX/C4f;)LX/Es2;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2174416
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x2

    const/4 v1, 0x0

    .line 2174417
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2174418
    check-cast v0, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;

    .line 2174419
    if-nez v0, :cond_0

    move v0, v1

    .line 2174420
    :goto_0
    return v0

    .line 2174421
    :cond_0
    invoke-static {v0}, LX/23B;->g(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPromotionFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v0

    .line 2174422
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v2, v5, :cond_2

    :cond_1
    move v0, v1

    .line 2174423
    goto :goto_0

    .line 2174424
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->u()LX/0Px;

    move-result-object v3

    move v2, v1

    .line 2174425
    :goto_1
    if-ge v2, v5, :cond_5

    .line 2174426
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2174427
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_3
    move v0, v1

    .line 2174428
    goto :goto_0

    .line 2174429
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2174430
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method
