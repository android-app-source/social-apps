.class public Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static l:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final c:LX/1nu;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/17Y;

.field public final f:LX/DDC;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0tX;

.field private final i:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

.field private final j:LX/1g8;

.field public final k:LX/DD1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1975791
    const-class v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/1nu;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/DDC;LX/0Or;LX/0tX;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/1g8;LX/DD1;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/1nu;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            "LX/DDC;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0tX;",
            "Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;",
            "LX/1g8;",
            "LX/DD1;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1975793
    iput-object p1, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->a:Landroid/content/res/Resources;

    .line 1975794
    iput-object p2, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->c:LX/1nu;

    .line 1975795
    iput-object p3, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1975796
    iput-object p4, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->e:LX/17Y;

    .line 1975797
    iput-object p5, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->f:LX/DDC;

    .line 1975798
    iput-object p6, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->g:LX/0Or;

    .line 1975799
    iput-object p7, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->h:LX/0tX;

    .line 1975800
    iput-object p8, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->i:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 1975801
    iput-object p9, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->j:LX/1g8;

    .line 1975802
    iput-object p10, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->k:LX/DD1;

    .line 1975803
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;
    .locals 14

    .prologue
    .line 1975804
    const-class v1, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;

    monitor-enter v1

    .line 1975805
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975806
    sput-object v2, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975807
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975808
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975809
    new-instance v3, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v0}, LX/DDC;->a(LX/0QB;)LX/DDC;

    move-result-object v8

    check-cast v8, LX/DDC;

    const/16 v9, 0x15e7

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v10

    check-cast v10, LX/0tX;

    invoke-static {v0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v11

    check-cast v11, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v12

    check-cast v12, LX/1g8;

    invoke-static {v0}, LX/DD1;->b(LX/0QB;)LX/DD1;

    move-result-object v13

    check-cast v13, LX/DD1;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;-><init>(Landroid/content/res/Resources;LX/1nu;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/DDC;LX/0Or;LX/0tX;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;LX/1g8;LX/DD1;)V

    .line 1975810
    move-object v0, v3

    .line 1975811
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975812
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975813
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975814
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/DDZ;LX/1Po;)V
    .locals 11
    .param p1    # LX/DDZ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DDZ;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 1975815
    iget-object v2, p1, LX/DDZ;->b:Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;

    .line 1975816
    iget-object v3, p1, LX/DDZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975817
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1975818
    :cond_0
    :goto_0
    return-void

    .line 1975819
    :cond_1
    iget-object v1, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->i:Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    .line 1975820
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1975821
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;Ljava/lang/String;)V

    .line 1975822
    new-instance v1, LX/4GJ;

    invoke-direct {v1}, LX/4GJ;-><init>()V

    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1975823
    const-string v4, "actor_id"

    invoke-virtual {v1, v4, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975824
    move-object v0, v1

    .line 1975825
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    .line 1975826
    const-string v4, "group_id"

    invoke-virtual {v0, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975827
    move-object v0, v0

    .line 1975828
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v1

    .line 1975829
    const-string v4, "user_id"

    invoke-virtual {v0, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975830
    move-object v0, v0

    .line 1975831
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->j()Ljava/lang/String;

    move-result-object v1

    .line 1975832
    const-string v4, "recommendation_key"

    invoke-virtual {v0, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975833
    move-object v0, v0

    .line 1975834
    invoke-interface {p2}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-static {v1}, LX/DD1;->a(LX/1PT;)Ljava/lang/String;

    move-result-object v1

    .line 1975835
    const-string v4, "source"

    invoke-virtual {v0, v4, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1975836
    move-object v0, v0

    .line 1975837
    new-instance v1, LX/832;

    invoke-direct {v1}, LX/832;-><init>()V

    move-object v1, v1

    .line 1975838
    const-string v4, "data"

    invoke-virtual {v1, v4, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1975839
    iget-object v0, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->h:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1975840
    const-string v1, ""

    .line 1975841
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1975842
    check-cast v0, LX/16h;

    invoke-static {v2, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    .line 1975843
    if-eqz v0, :cond_2

    .line 1975844
    invoke-virtual {v0}, LX/162;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1975845
    :goto_1
    iget-object v1, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInvitePageComponentSpec;->j:LX/1g8;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->l()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnitUsersEdge;->j()Ljava/lang/String;

    move-result-object v2

    .line 1975846
    const-string v10, "gpymi_xout"

    move-object v5, v1

    move-object v6, v3

    move-object v7, v4

    move-object v8, v2

    move-object v9, v0

    invoke-static/range {v5 .. v10}, LX/1g8;->b(LX/1g8;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1975847
    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
