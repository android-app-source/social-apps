.class public Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final b:LX/1nu;

.field public final c:Landroid/content/res/Resources;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/154;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1975467
    const-class v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/1nu;LX/0Ot;LX/0Ot;LX/154;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/1nu;",
            "LX/0Ot",
            "<",
            "LX/1g8;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/154;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1975469
    iput-object p1, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->c:Landroid/content/res/Resources;

    .line 1975470
    iput-object p2, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->b:LX/1nu;

    .line 1975471
    iput-object p3, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->d:LX/0Ot;

    .line 1975472
    iput-object p4, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->e:LX/0Ot;

    .line 1975473
    iput-object p5, p0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->f:LX/154;

    .line 1975474
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;
    .locals 9

    .prologue
    .line 1975475
    const-class v1, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;

    monitor-enter v1

    .line 1975476
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975477
    sput-object v2, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975478
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975479
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975480
    new-instance v3, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    const/16 v6, 0xb2d

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2eb

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v8

    check-cast v8, LX/154;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;-><init>(Landroid/content/res/Resources;LX/1nu;LX/0Ot;LX/0Ot;LX/154;)V

    .line 1975481
    move-object v0, v3

    .line 1975482
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975483
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/gpymi/rows/components/GroupsPeopleYouMayInviteGroupIntroComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975484
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975485
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/1De;Ljava/lang/CharSequence;)LX/1Di;
    .locals 2

    .prologue
    .line 1975486
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b1250

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b1251

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    return-object v0
.end method
