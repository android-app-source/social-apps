.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

.field private final b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1975909
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1975910
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    .line 1975911
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    .line 1975912
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;
    .locals 5

    .prologue
    .line 1975913
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;

    monitor-enter v1

    .line 1975914
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1975915
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1975916
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1975917
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1975918
    new-instance p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;)V

    .line 1975919
    move-object v0, p0

    .line 1975920
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1975921
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1975922
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1975923
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1975924
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975925
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;->b:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    new-instance v1, LX/DDe;

    invoke-direct {v1, p2}, LX/DDe;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1975926
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceGroupPartDefinition;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceSellerActionsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1975927
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1975928
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1975929
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1975930
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/2vB;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
