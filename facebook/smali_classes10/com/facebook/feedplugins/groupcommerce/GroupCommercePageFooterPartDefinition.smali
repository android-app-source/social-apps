.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommercePageFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976079
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1976080
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommercePageFooterPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    .line 1976081
    iput-object p2, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommercePageFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 1976082
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1976083
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976084
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommercePageFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v1, LX/20d;

    sget-object v2, LX/1Wi;->PAGE:LX/1Wi;

    invoke-direct {v1, p2, v2}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1976085
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommercePageFooterPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    new-instance v1, LX/3Dt;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v2}, LX/3Dt;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1976086
    const/4 v0, 0x0

    return-object v0
.end method
