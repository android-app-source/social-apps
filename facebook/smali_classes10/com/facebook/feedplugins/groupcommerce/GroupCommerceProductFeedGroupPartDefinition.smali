.class public Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/DDe;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1976087
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1976088
    iput-object p1, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    .line 1976089
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;
    .locals 4

    .prologue
    .line 1976090
    const-class v1, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    monitor-enter v1

    .line 1976091
    :try_start_0
    sget-object v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1976092
    sput-object v2, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1976093
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1976094
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1976095
    new-instance p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;)V

    .line 1976096
    move-object v0, p0

    .line 1976097
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1976098
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1976099
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1976100
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1976101
    check-cast p2, LX/DDe;

    .line 1976102
    iget-object v0, p0, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceCompactItemPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1976103
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1976104
    check-cast p1, LX/DDe;

    .line 1976105
    iget-object v0, p1, LX/DDe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1976106
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1976107
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/2vB;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
