.class public Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;
.super LX/Csl;
.source ""

# interfaces
.implements LX/20T;


# static fields
.field private static final d:LX/0wT;

.field private static final e:LX/0wT;


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:Landroid/widget/ImageView;

.field public final g:LX/215;

.field private final h:LX/0wd;

.field private final i:LX/Csk;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1943114
    sget-wide v0, LX/CoL;->H:D

    sget-wide v2, LX/CoL;->I:D

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->d:LX/0wT;

    .line 1943115
    sget-wide v0, LX/CoL;->F:D

    sget-wide v2, LX/CoL;->G:D

    invoke-static {v0, v1, v2, v3}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->e:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1943135
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1943136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1943133
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 1943117
    invoke-direct {p0, p1, p2, p3}, LX/Csl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943118
    const-class v0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1943119
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->g:LX/215;

    .line 1943120
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->g:LX/215;

    invoke-virtual {v0, p0}, LX/215;->a(LX/20T;)V

    .line 1943121
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->g:LX/215;

    .line 1943122
    iput-boolean v4, v0, LX/215;->d:Z

    .line 1943123
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->g:LX/215;

    const/high16 v1, 0x3f800000    # 1.0f

    .line 1943124
    iput v1, v0, LX/215;->c:F

    .line 1943125
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->g:LX/215;

    sget-object v1, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->d:LX/0wT;

    invoke-virtual {v0, v1}, LX/215;->a(LX/0wT;)V

    .line 1943126
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->c:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->e:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    .line 1943127
    iput-boolean v4, v0, LX/0wd;->c:Z

    .line 1943128
    move-object v0, v0

    .line 1943129
    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->h:LX/0wd;

    .line 1943130
    new-instance v0, LX/Csk;

    invoke-direct {v0, p0}, LX/Csk;-><init>(Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->i:LX/Csk;

    .line 1943131
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->h:LX/0wd;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->i:LX/Csk;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1943132
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;LX/Cju;LX/0Or;LX/0wW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;",
            "LX/Cju;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "LX/0wW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1943116
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->a:LX/Cju;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->c:LX/0wW;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;

    invoke-static {v1}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v0

    check-cast v0, LX/Cju;

    const/16 v2, 0x13a4

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->a(Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;LX/Cju;LX/0Or;LX/0wW;)V

    return-void
.end method

.method private b(F)V
    .locals 2

    .prologue
    .line 1943110
    const/high16 v0, 0x3f800000    # 1.0f

    mul-float/2addr v0, p1

    .line 1943111
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1943112
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1943113
    return-void
.end method

.method public static c(Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;F)V
    .locals 1

    .prologue
    .line 1943137
    invoke-virtual {p0, p1}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->setAlpha(F)V

    .line 1943138
    sget v0, LX/CoL;->s:F

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 1943139
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->setVisibility(I)V

    .line 1943140
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1943078
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->h:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1943079
    return-void
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1943080
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->b(F)V

    .line 1943081
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 1943082
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->h:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1943083
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->h:LX/0wd;

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1943084
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->setVisibility(I)V

    .line 1943085
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->setAlpha(F)V

    .line 1943086
    return-void
.end method

.method public getRetryButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 1943087
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final isPressed()Z
    .locals 1

    .prologue
    .line 1943088
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isPressed()Z

    move-result v0

    return v0
.end method

.method public final onFinishInflate()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/16 v0, 0x2c

    const v1, -0x51ec785

    invoke-static {v12, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 1943089
    invoke-super {p0}, LX/Csl;->onFinishInflate()V

    .line 1943090
    const v0, 0x7f0d16d1

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->f:Landroid/widget/ImageView;

    .line 1943091
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->f:Landroid/widget/ImageView;

    new-instance v1, LX/Csj;

    invoke-direct {v1, p0}, LX/Csj;-><init>(Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1943092
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->a:LX/Cju;

    const v1, 0x7f0d012c

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v5

    .line 1943093
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->a:LX/Cju;

    const v1, 0x7f0d012b

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v6

    .line 1943094
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->a:LX/Cju;

    const v1, 0x7f0d011e

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v7

    .line 1943095
    const v0, 0x7f0d16ce

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1943096
    const v1, 0x7f0d16cf

    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1943097
    const v2, 0x7f0d16d0

    invoke-virtual {p0, v2}, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 1943098
    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 1943099
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v3, v8, v7, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1943100
    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1943101
    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->f:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 1943102
    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v9, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v10, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v3, v8, v7, v9, v10}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1943103
    iget-object v7, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->f:Landroid/widget/ImageView;

    invoke-virtual {v7, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1943104
    int-to-float v3, v5

    invoke-virtual {v0, v11, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 1943105
    int-to-float v0, v6

    invoke-virtual {v1, v11, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 1943106
    int-to-float v0, v6

    invoke-virtual {v2, v11, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextSize(IF)V

    .line 1943107
    const/16 v0, 0x2d

    const v1, -0x19acbd96

    invoke-static {v12, v0, v1, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 1943108
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/InstantArticlesRetryViewGroup;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->performClick()Z

    .line 1943109
    const/4 v0, 0x1

    return v0
.end method
