.class public Lcom/facebook/richdocument/view/widget/LocationAnnotationView;
.super Lcom/facebook/richdocument/view/widget/TextAnnotationView;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/richdocument/view/widget/TextAnnotationView",
        "<",
        "LX/ClY;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1943289
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;-><init>(Landroid/content/Context;)V

    .line 1943290
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/LocationAnnotationView;->e()V

    .line 1943291
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1943292
    invoke-direct {p0, p1, p2}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1943293
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/LocationAnnotationView;->e()V

    .line 1943294
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1943295
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943296
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/LocationAnnotationView;->e()V

    .line 1943297
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;LX/ClY;)Lcom/facebook/richdocument/view/widget/LocationAnnotationView;
    .locals 3

    .prologue
    .line 1943298
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1943299
    const v1, 0x7f031209

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/LocationAnnotationView;

    .line 1943300
    invoke-virtual {v0, p2}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setAnnotation(LX/ClU;)V

    .line 1943301
    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 1943302
    const v0, 0x7f02166e

    const v1, 0x7f0d015f

    const v2, 0x7f0d0160

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a(III)V

    .line 1943303
    const v0, 0x7f0b1279

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setDrawablePaddingResource(I)V

    .line 1943304
    return-void
.end method
