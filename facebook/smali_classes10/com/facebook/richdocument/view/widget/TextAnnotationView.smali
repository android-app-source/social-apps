.class public Lcom/facebook/richdocument/view/widget/TextAnnotationView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/Cjr;
.implements LX/Cmx;
.implements LX/CnP;
.implements LX/CnQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "LX/ClU;",
        ">",
        "Lcom/facebook/widget/CustomLinearLayout;",
        "LX/Cjr;",
        "LX/Cmx;",
        "LX/CnP;",
        "LX/CnQ",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Paint;

.field public c:LX/CIg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Clh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/ClU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field private final g:Landroid/graphics/Paint;

.field private final h:Landroid/graphics/Paint;

.field public i:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private j:Landroid/widget/ImageView;

.field private k:LX/Cmo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1941927
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1941928
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a:Landroid/graphics/Paint;

    .line 1941929
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->b:Landroid/graphics/Paint;

    .line 1941930
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->g:Landroid/graphics/Paint;

    .line 1941931
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->h:Landroid/graphics/Paint;

    .line 1941932
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->e()V

    .line 1941933
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1941934
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1941935
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a:Landroid/graphics/Paint;

    .line 1941936
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->b:Landroid/graphics/Paint;

    .line 1941937
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->g:Landroid/graphics/Paint;

    .line 1941938
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->h:Landroid/graphics/Paint;

    .line 1941939
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->e()V

    .line 1941940
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1941941
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1941942
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a:Landroid/graphics/Paint;

    .line 1941943
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->b:Landroid/graphics/Paint;

    .line 1941944
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->g:Landroid/graphics/Paint;

    .line 1941945
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->h:Landroid/graphics/Paint;

    .line 1941946
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->e()V

    .line 1941947
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/TextAnnotationView;LX/CIg;LX/Cju;LX/Clh;)V
    .locals 0

    .prologue
    .line 1941948
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->c:LX/CIg;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->d:LX/Cju;

    iput-object p3, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->e:LX/Clh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    invoke-static {v2}, LX/CIg;->a(LX/0QB;)LX/CIg;

    move-result-object v0

    check-cast v0, LX/CIg;

    invoke-static {v2}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    invoke-static {v2}, LX/Clh;->b(LX/0QB;)LX/Clh;

    move-result-object v2

    check-cast v2, LX/Clh;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a(Lcom/facebook/richdocument/view/widget/TextAnnotationView;LX/CIg;LX/Cju;LX/Clh;)V

    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 1941949
    const-class v0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1941950
    const v0, 0x7f0311fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1941951
    const v0, 0x7f0d0546

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1941952
    const v0, 0x7f0d2a32

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    .line 1941953
    return-void
.end method


# virtual methods
.method public a()V
    .locals 13

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1941954
    new-instance v0, LX/Cle;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Cle;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->f:LX/ClU;

    .line 1941955
    if-nez v1, :cond_5

    .line 1941956
    :cond_0
    :goto_0
    move-object v0, v0

    .line 1941957
    invoke-virtual {v0}, LX/Cle;->a()LX/Clf;

    move-result-object v0

    .line 1941958
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->c:LX/CIg;

    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1941959
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1941960
    invoke-virtual {v1, v2, v0}, LX/CIg;->a(Landroid/widget/TextView;LX/Clf;)V

    .line 1941961
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1941962
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1941963
    :cond_1
    iget-object v1, v0, LX/Clf;->b:LX/0Px;

    move-object v1, v1

    .line 1941964
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0620

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1941965
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1941966
    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    .line 1941967
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CIg;->a(Ljava/lang/String;)I

    move-result v1

    .line 1941968
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->e:LX/Clh;

    invoke-virtual {v2, v0}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)LX/Cmw;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->d:LX/Cju;

    invoke-static {v2, v3}, LX/Cn1;->a(LX/Cmw;LX/Cju;)Landroid/graphics/Rect;

    move-result-object v2

    .line 1941969
    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->e:LX/Clh;

    invoke-virtual {v3, v0}, LX/Clh;->b(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)LX/Cmw;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->d:LX/Cju;

    invoke-static {v3, v4}, LX/Cn1;->a(LX/Cmw;LX/Cju;)Landroid/graphics/Rect;

    move-result-object v3

    .line 1941970
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v0, v5}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;Landroid/content/Context;)LX/Cmo;

    move-result-object v4

    .line 1941971
    invoke-static {v0}, LX/Clh;->c(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)LX/Cmp;

    move-result-object v5

    .line 1941972
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Clh;->a(Ljava/lang/String;)I

    move-result v6

    .line 1941973
    const/4 v0, 0x0

    .line 1941974
    sget-object v7, LX/CtN;->a:[I

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getAnnotation()LX/ClU;

    move-result-object v8

    .line 1941975
    iget-object v12, v8, LX/ClU;->d:LX/ClQ;

    move-object v8, v12

    .line 1941976
    invoke-virtual {v8}, LX/ClQ;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 1941977
    :goto_1
    iget-object v7, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-static {v7, v3}, LX/CnC;->a(Lcom/facebook/richdocument/view/widget/RichTextView;Landroid/graphics/Rect;)V

    .line 1941978
    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-static {v3, v4, p0}, LX/Cn9;->a(Lcom/facebook/richdocument/view/widget/RichTextView;LX/Cmo;LX/Cmx;)V

    .line 1941979
    invoke-static {p0, v2, v0}, LX/Cn4;->a(Landroid/view/View;Landroid/graphics/Rect;LX/Cmr;)V

    .line 1941980
    invoke-static {p0, v5}, LX/Cn3;->a(Landroid/view/View;LX/Cmp;)V

    .line 1941981
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->c:LX/CIg;

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v2, v3, v0}, LX/CIg;->a(Lcom/facebook/richdocument/view/widget/RichTextView;LX/Cmr;)V

    .line 1941982
    invoke-static {p0, v6}, LX/8ba;->a(Landroid/view/View;I)V

    move v0, v1

    .line 1941983
    :cond_2
    new-instance v2, Landroid/content/res/ColorStateList;

    new-array v3, v11, [[I

    new-array v1, v10, [I

    const v4, 0x101009e

    aput v4, v1, v9

    aput-object v1, v3, v9

    new-array v1, v9, [I

    aput-object v1, v3, v10

    new-array v4, v11, [I

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->f:LX/ClU;

    .line 1941984
    iget-object v5, v1, LX/ClU;->a:LX/ClT;

    move-object v1, v5

    .line 1941985
    sget-object v5, LX/ClT;->COPYRIGHT:LX/ClT;

    if-ne v1, v5, :cond_4

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0a0621

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_2
    aput v1, v4, v9

    aput v0, v4, v10

    invoke-direct {v2, v3, v4}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    .line 1941986
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1941987
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1941988
    invoke-virtual {v0, v2}, LX/CtG;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1941989
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->f:LX/ClU;

    .line 1941990
    iget-object v1, v0, LX/ClU;->d:LX/ClQ;

    move-object v0, v1

    .line 1941991
    if-eqz v0, :cond_3

    .line 1941992
    sget-object v1, LX/CtN;->a:[I

    invoke-virtual {v0}, LX/ClQ;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 1941993
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v10}, Lcom/facebook/richdocument/view/widget/RichTextView;->setEnableCopy(Z)V

    .line 1941994
    return-void

    .line 1941995
    :pswitch_0
    sget-object v0, LX/Cmr;->LEFT:LX/Cmr;

    goto :goto_1

    .line 1941996
    :pswitch_1
    sget-object v0, LX/Cmr;->CENTER:LX/Cmr;

    goto :goto_1

    .line 1941997
    :pswitch_2
    sget-object v0, LX/Cmr;->RIGHT:LX/Cmr;

    goto :goto_1

    .line 1941998
    :cond_4
    const/4 v1, -0x1

    goto :goto_2

    .line 1941999
    :pswitch_3
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setGravity(I)V

    goto :goto_3

    .line 1942000
    :pswitch_4
    invoke-virtual {p0, v10}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setGravity(I)V

    goto :goto_3

    .line 1942001
    :pswitch_5
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setGravity(I)V

    goto :goto_3

    .line 1942002
    :cond_5
    iget-object v2, v1, LX/ClU;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1942003
    iput-object v2, v0, LX/Cle;->d:Ljava/lang/CharSequence;

    .line 1942004
    iget-object v2, v0, LX/Cle;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    if-eqz v2, :cond_6

    .line 1942005
    iget-object v2, v0, LX/Cle;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    .line 1942006
    iget-object v3, v1, LX/ClU;->a:LX/ClT;

    move-object v3, v3

    .line 1942007
    iget-object v4, v1, LX/ClU;->c:LX/ClS;

    move-object v4, v4

    .line 1942008
    invoke-static {v2, v3, v4}, LX/Clh;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;LX/ClT;LX/ClS;)Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;

    move-result-object v2

    invoke-static {v0, v2}, LX/Cle;->a(LX/Cle;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentElementStyleModel;)V

    .line 1942009
    :cond_6
    invoke-static {v1}, LX/CIg;->a(LX/ClU;)I

    move-result v2

    .line 1942010
    if-eqz v2, :cond_7

    .line 1942011
    invoke-virtual {v0, v2}, LX/Cle;->a(I)LX/Cle;

    .line 1942012
    :cond_7
    iget-object v2, v1, LX/ClU;->g:LX/8Z4;

    move-object v2, v2

    .line 1942013
    if-eqz v2, :cond_0

    .line 1942014
    iget-object v2, v1, LX/ClU;->g:LX/8Z4;

    move-object v2, v2

    .line 1942015
    invoke-interface {v2}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1942016
    new-instance v2, Landroid/text/SpannableStringBuilder;

    .line 1942017
    iget-object v3, v1, LX/ClU;->g:LX/8Z4;

    move-object v3, v3

    .line 1942018
    invoke-interface {v3}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1942019
    iget-object v3, v1, LX/ClU;->g:LX/8Z4;

    move-object v3, v3

    .line 1942020
    invoke-interface {v3}, LX/8Z4;->c()LX/0Px;

    move-result-object v3

    invoke-static {v2, v3}, LX/Cle;->c(Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    .line 1942021
    iget-object v3, v1, LX/ClU;->g:LX/8Z4;

    move-object v3, v3

    .line 1942022
    invoke-interface {v3}, LX/8Z4;->b()LX/0Px;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/Cle;->a(LX/Cle;Landroid/text/SpannableStringBuilder;Ljava/util/List;)V

    .line 1942023
    iput-object v2, v0, LX/Cle;->d:Ljava/lang/CharSequence;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(III)V
    .locals 2

    .prologue
    .line 1942024
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1942025
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1942026
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1942027
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->d:LX/Cju;

    invoke-interface {v1, p2}, LX/Cju;->c(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1942028
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->d:LX/Cju;

    invoke-interface {v1, p3}, LX/Cju;->c(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1942029
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1942030
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;II)V
    .locals 2

    .prologue
    .line 1942031
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1942032
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1942033
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1942034
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->d:LX/Cju;

    invoke-interface {v1, p2}, LX/Cju;->c(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1942035
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->d:LX/Cju;

    invoke-interface {v1, p3}, LX/Cju;->c(I)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1942036
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1942037
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1942038
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a()V

    .line 1942039
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 0

    .prologue
    .line 1941926
    return-object p0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1941879
    const/4 v0, 0x1

    return v0
.end method

.method public getAnnotation()LX/ClU;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 1942040
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->f:LX/ClU;

    return-object v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1941880
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getExtraPaddingBottom()I
    .locals 1

    .prologue
    .line 1941881
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getExtraPaddingBottom()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTextView()Lcom/facebook/richdocument/view/widget/RichTextView;
    .locals 1

    .prologue
    .line 1941882
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1941883
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->k:LX/Cmo;

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->b:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->g:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->h:Landroid/graphics/Paint;

    move-object v0, p1

    move-object v1, p0

    invoke-static/range {v0 .. v6}, LX/Clh;->a(Landroid/graphics/Canvas;Landroid/view/View;LX/Cmo;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 1941884
    return-void
.end method

.method public setAnnotation(LX/ClU;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1941885
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->f:LX/ClU;

    .line 1941886
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a()V

    .line 1941887
    return-void
.end method

.method public setBorders(LX/Cmo;)V
    .locals 2

    .prologue
    .line 1941888
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setWillNotDraw(Z)V

    .line 1941889
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->k:LX/Cmo;

    .line 1941890
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->k:LX/Cmo;

    if-eqz v0, :cond_0

    .line 1941891
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->k:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->a:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1941892
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->k:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->b:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1941893
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->g:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->k:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->c:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1941894
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->h:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->k:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->d:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1941895
    :cond_0
    return-void
.end method

.method public setDrawablePaddingResource(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1941896
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1941897
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1941898
    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1941899
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1941900
    return-void
.end method

.method public setIsOverlay(Z)V
    .locals 0

    .prologue
    .line 1941901
    invoke-virtual {p0, p1}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setEnabled(Z)V

    .line 1941902
    return-void
.end method

.method public setText(I)V
    .locals 1

    .prologue
    .line 1941903
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1941904
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p0

    .line 1941905
    invoke-virtual {v0, p1}, LX/CtG;->setText(I)V

    .line 1941906
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1941907
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1941908
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p0

    .line 1941909
    invoke-virtual {v0, p1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1941910
    return-void
.end method

.method public setTextOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1941911
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1941912
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1941913
    if-eqz v0, :cond_0

    .line 1941914
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->i:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1941915
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1941916
    invoke-virtual {v0, p1}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1941917
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1941918
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->f:LX/ClU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->f:LX/ClU;

    .line 1941919
    iget-object v1, v0, LX/ClU;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1941920
    if-nez v0, :cond_1

    .line 1941921
    :cond_0
    const/4 v0, 0x0

    .line 1941922
    :goto_0
    return-object v0

    .line 1941923
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->f:LX/ClU;

    .line 1941924
    iget-object v1, v0, LX/ClU;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1941925
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
