.class public Lcom/facebook/richdocument/view/widget/RichDocumentImageView;
.super Lcom/facebook/drawee/view/GenericDraweeView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Ct1;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Crg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/common/callercontext/CallerContext;

.field private e:LX/Ctm;

.field public f:Z

.field private g:Ljava/lang/String;

.field public h:LX/Cov;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1943752
    const-class v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1943742
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;)V

    .line 1943743
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->e()V

    .line 1943744
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1943745
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1943746
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->e()V

    .line 1943747
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1943748
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/view/GenericDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943749
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->e()V

    .line 1943750
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;LX/1Ad;LX/Crg;)V
    .locals 0

    .prologue
    .line 1943751
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->b:LX/Crg;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v1}, LX/Crg;->b(LX/0QB;)LX/Crg;

    move-result-object v1

    check-cast v1, LX/Crg;

    invoke-static {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;LX/1Ad;LX/Crg;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1943761
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->getControllerBuilder()LX/1Ad;

    move-result-object v0

    .line 1943762
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/1Ae;->a([Ljava/lang/Object;Z)LX/1Ae;

    .line 1943763
    if-eqz p1, :cond_0

    .line 1943764
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    .line 1943765
    :cond_0
    if-eqz p2, :cond_1

    .line 1943766
    invoke-static {p2}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    .line 1943767
    :cond_1
    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1943768
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1943769
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1943754
    const-class v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1943755
    new-instance v0, LX/Ctm;

    invoke-direct {v0, p0}, LX/Ctm;-><init>(LX/Ct1;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->e:LX/Ctm;

    .line 1943756
    sget-object v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->c:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1943757
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/ChJ;->b(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v0

    .line 1943758
    if-eqz v0, :cond_0

    .line 1943759
    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1943760
    :cond_0
    return-void
.end method

.method private getControllerBuilder()LX/1Ad;
    .locals 2

    .prologue
    .line 1943753
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a:LX/1Ad;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    new-instance v1, LX/Ct0;

    invoke-direct {v1, p0}, LX/Ct0;-><init>(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    return-object v0
.end method


# virtual methods
.method public final a(LX/1bf;II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1943711
    if-nez p1, :cond_0

    .line 1943712
    :goto_0
    return-void

    .line 1943713
    :cond_0
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->getControllerBuilder()LX/1Ad;

    move-result-object v0

    .line 1943714
    const/4 v1, 0x2

    new-array v1, v1, [LX/1bf;

    .line 1943715
    aput-object p1, v1, v4

    .line 1943716
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->g:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1943717
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    sget-object v3, LX/1bY;->DISK_CACHE:LX/1bY;

    .line 1943718
    iput-object v3, v2, LX/1bX;->b:LX/1bY;

    .line 1943719
    move-object v2, v2

    .line 1943720
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 1943721
    invoke-virtual {v0, v2}, LX/1Ae;->d(Ljava/lang/Object;)LX/1Ae;

    .line 1943722
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v2

    sget-object v3, LX/1bY;->FULL_FETCH:LX/1bY;

    .line 1943723
    iput-object v3, v2, LX/1bX;->b:LX/1bY;

    .line 1943724
    move-object v2, v2

    .line 1943725
    invoke-virtual {v2}, LX/1bX;->n()LX/1bf;

    move-result-object v2

    .line 1943726
    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 1943727
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    .line 1943728
    invoke-virtual {v0, v1, v4}, LX/1Ae;->a([Ljava/lang/Object;Z)LX/1Ae;

    .line 1943729
    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1943730
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1943731
    invoke-virtual {p0, v4}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->setVisibility(I)V

    .line 1943732
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->e:LX/Ctm;

    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    .line 1943733
    iput v1, v0, LX/Ctm;->b:F

    .line 1943734
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 1943735
    if-nez p1, :cond_0

    .line 1943736
    :goto_0
    return-void

    .line 1943737
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->b:LX/Crg;

    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    invoke-virtual {v0, p1, v1}, LX/BA0;->a(Ljava/lang/String;F)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/Csz;

    invoke-direct {v1, p0}, LX/Csz;-><init>(Lcom/facebook/richdocument/view/widget/RichDocumentImageView;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1943738
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->setVisibility(I)V

    .line 1943739
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->e:LX/Ctm;

    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    .line 1943740
    iput v1, v0, LX/Ctm;->b:F

    .line 1943741
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;IILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1943684
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->g:Ljava/lang/String;

    .line 1943685
    invoke-direct {p0, p1, p4}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1943686
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->setVisibility(I)V

    .line 1943687
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->e:LX/Ctm;

    int-to-float v1, p2

    int-to-float v2, p3

    div-float/2addr v1, v2

    .line 1943688
    iput v1, v0, LX/Ctm;->b:F

    .line 1943689
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1943690
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->setVisibility(I)V

    .line 1943691
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, v1}, LX/1af;->a(I)V

    .line 1943692
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->f:Z

    .line 1943693
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->g:Ljava/lang/String;

    .line 1943694
    return-void
.end method

.method public getMediaAspectRatio()F
    .locals 1

    .prologue
    .line 1943695
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->e:LX/Ctm;

    .line 1943696
    iget p0, v0, LX/Ctm;->b:F

    move v0, p0

    .line 1943697
    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 1943698
    return-object p0
.end method

.method public final jk_()Z
    .locals 1

    .prologue
    .line 1943699
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->f:Z

    return v0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1943700
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->e:LX/Ctm;

    invoke-virtual {v0}, LX/Ctm;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 1943701
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Lcom/facebook/drawee/view/GenericDraweeView;->onMeasure(II)V

    .line 1943702
    return-void
.end method

.method public setCallerContext(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 1943703
    if-eqz p1, :cond_0

    .line 1943704
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1943705
    :cond_0
    return-void
.end method

.method public setFadeDuration(I)V
    .locals 1

    .prologue
    .line 1943706
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p1}, LX/1af;->a(I)V

    .line 1943707
    return-void
.end method

.method public setImageSetListener(LX/Cov;)V
    .locals 0

    .prologue
    .line 1943708
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->h:LX/Cov;

    .line 1943709
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1943710
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
