.class public Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;
.super LX/Cti;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/Ct1;",
        ">",
        "LX/Cti",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public f:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:Z

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "LX/Ctr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1945536
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1945537
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1945505
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945506
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1945507
    invoke-direct {p0, p1, p2, p3}, LX/Cti;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945508
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    .line 1945509
    const-class v0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1945510
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->f:LX/0Uh;

    const/16 v1, 0x3d5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->g:Z

    .line 1945511
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->f:LX/0Uh;

    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1945512
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945513
    invoke-interface {v0}, LX/Ctr;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1945514
    invoke-interface {v0}, LX/Ctr;->d()V

    goto :goto_0

    .line 1945515
    :cond_1
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1945516
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945517
    invoke-interface {v0}, LX/Ctr;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1945518
    invoke-interface {v0}, LX/Ctr;->e()V

    goto :goto_0

    .line 1945519
    :cond_1
    return-void
.end method

.method private i()Z
    .locals 4

    .prologue
    .line 1945520
    invoke-virtual {p0}, LX/Cte;->getAnnotationViews()LX/Cs7;

    move-result-object v1

    .line 1945521
    const/4 v0, 0x0

    .line 1945522
    invoke-virtual {v1}, LX/Cs7;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    .line 1945523
    instance-of v3, v0, LX/CnR;

    if-eqz v3, :cond_1

    .line 1945524
    check-cast v0, LX/CnR;

    invoke-virtual {v0}, LX/CnR;->getIsDirtyAndReset()Z

    move-result v0

    or-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 1945525
    goto :goto_0

    .line 1945526
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/CrS;)V
    .locals 3

    .prologue
    .line 1945527
    invoke-super {p0, p1}, LX/Cti;->a(LX/CrS;)V

    .line 1945528
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945529
    invoke-interface {v0}, LX/Ctr;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1945530
    invoke-interface {v0, p1}, LX/Ctr;->b(LX/CrS;)V

    goto :goto_0

    .line 1945531
    :cond_1
    return-void
.end method

.method public final a(LX/Ctr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/Ctr;",
            ">(TT;)V"
        }
    .end annotation

    .prologue
    .line 1945532
    if-eqz p1, :cond_0

    .line 1945533
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1945534
    invoke-interface {p1}, LX/Ctr;->c()V

    .line 1945535
    :cond_0
    return-void
.end method

.method public final a(LX/Ctr;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1945499
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945500
    if-eq v0, p1, :cond_0

    .line 1945501
    invoke-interface {v0, v0, p2}, LX/Ctr;->a(LX/Ctr;Ljava/lang/Object;)V

    goto :goto_0

    .line 1945502
    :cond_1
    return-void
.end method

.method public final a(LX/Crd;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1945538
    const/4 v1, 0x0

    .line 1945539
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945540
    invoke-interface {v0}, LX/Ctr;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    instance-of v4, v0, LX/Cu1;

    if-nez v4, :cond_0

    .line 1945541
    invoke-interface {v0, p1}, LX/Cre;->a(LX/Crd;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 1945542
    :cond_1
    const-class v0, LX/Cu1;

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->b(Ljava/lang/Class;)LX/Ctr;

    move-result-object v0

    check-cast v0, LX/Cu1;

    .line 1945543
    if-eqz v0, :cond_2

    .line 1945544
    invoke-virtual {v0, p1}, LX/Cts;->a(LX/Crd;)Z

    .line 1945545
    :cond_2
    if-eqz v1, :cond_3

    :goto_0
    return v2

    :cond_3
    invoke-super {p0, p1}, LX/Cti;->a(LX/Crd;)Z

    move-result v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Class;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/Ctr;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 1945503
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Class;)LX/Ctr;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/Ctr;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1945504
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1945447
    invoke-super {p0}, LX/Cti;->b()V

    .line 1945448
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945449
    invoke-interface {v0}, LX/Ctr;->c()V

    goto :goto_0

    .line 1945450
    :cond_0
    return-void
.end method

.method public final c(LX/Cqw;)V
    .locals 3

    .prologue
    .line 1945451
    invoke-super {p0, p1}, LX/Cti;->c(LX/Cqw;)V

    .line 1945452
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945453
    invoke-interface {v0}, LX/Ctr;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1945454
    invoke-interface {v0, p1}, LX/Ctr;->a(LX/Cqw;)V

    goto :goto_0

    .line 1945455
    :cond_1
    return-void
.end method

.method public final c(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/Ctr;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 1945456
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1945457
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1945458
    invoke-super {p0}, LX/Cti;->d()V

    .line 1945459
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->g:Z

    if-eqz v0, :cond_0

    .line 1945460
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->g()V

    .line 1945461
    :cond_0
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1945462
    invoke-super {p0, p1}, LX/Cti;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1945463
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945464
    invoke-interface {v0}, LX/Ctr;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1945465
    invoke-interface {v0, p1}, LX/Ctr;->a(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1945466
    :cond_1
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1945467
    invoke-super {p0}, LX/Cti;->e()V

    .line 1945468
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->g:Z

    if-eqz v0, :cond_0

    .line 1945469
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h()V

    .line 1945470
    :cond_0
    return-void
.end method

.method public getPlugins()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/Ctr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1945471
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xf80a025

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1945472
    invoke-super {p0}, LX/Cti;->onAttachedToWindow()V

    .line 1945473
    iget-boolean v1, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->g:Z

    if-nez v1, :cond_0

    .line 1945474
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->g()V

    .line 1945475
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x3b383be5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x543824f5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1945476
    invoke-super {p0}, LX/Cti;->onDetachedFromWindow()V

    .line 1945477
    iget-boolean v1, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->g:Z

    if-nez v1, :cond_0

    .line 1945478
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h()V

    .line 1945479
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x12fc74e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1945480
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945481
    instance-of v2, v0, LX/Ctn;

    if-eqz v2, :cond_0

    .line 1945482
    check-cast v0, LX/Ctn;

    .line 1945483
    invoke-interface {v0, p1}, LX/Ctn;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1945484
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 1945485
    invoke-super/range {p0 .. p5}, LX/Cti;->onLayout(ZIIII)V

    .line 1945486
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1945487
    invoke-virtual {p0}, LX/Cti;->f()V

    .line 1945488
    :cond_0
    invoke-virtual {p0}, LX/Cti;->getCurrentLayout()LX/CrS;

    move-result-object v1

    .line 1945489
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945490
    invoke-interface {v0}, LX/Ctr;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1945491
    invoke-interface {v0, v1}, LX/Ctr;->a(LX/CrS;)V

    goto :goto_0

    .line 1945492
    :cond_2
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x2

    const v0, -0x3024fc05

    invoke-static {v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1945493
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ctr;

    .line 1945494
    instance-of v4, v0, LX/Ctn;

    if-eqz v4, :cond_0

    .line 1945495
    check-cast v0, LX/Ctn;

    .line 1945496
    invoke-interface {v0, p1}, LX/Ctn;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1945497
    const v0, -0x67b457c8

    invoke-static {v5, v5, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move v0, v1

    .line 1945498
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, LX/Cti;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v1, -0x6d31541

    invoke-static {v1, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method
