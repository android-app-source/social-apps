.class public Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;
.super Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;
.source ""


# instance fields
.field public g:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:I

.field private i:I

.field public j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1945675
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1945676
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1945673
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945674
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1945668
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945669
    const-class v0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1945670
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->g:LX/Cju;

    const v1, 0x7f0d0121

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->h:I

    .line 1945671
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->g:LX/Cju;

    const v1, 0x7f0d0122

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->i:I

    .line 1945672
    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1945638
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1945639
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->j:Z

    if-nez v0, :cond_0

    move-object v0, v1

    .line 1945640
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v4, v3, v4, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1945641
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_0

    move-object v0, v1

    .line 1945642
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 1945643
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 1945644
    :cond_0
    invoke-virtual {p0}, LX/Cti;->getMediaView()LX/Ct1;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_2

    .line 1945645
    invoke-virtual {p0}, LX/Cti;->getMediaView()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1945646
    sget-object v1, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    invoke-virtual {v0, v4, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1945647
    const-class v1, LX/Ctz;

    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->b(Ljava/lang/Class;)LX/Ctr;

    move-result-object v1

    check-cast v1, LX/Ctz;

    .line 1945648
    if-eqz p2, :cond_3

    .line 1945649
    iget-object v2, v1, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1945650
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1945651
    invoke-virtual {v2}, LX/CtG;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1945652
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/Ctz;->d:Z

    .line 1945653
    iget-object v2, v1, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1945654
    :cond_1
    :goto_0
    const-class v1, LX/CuN;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, LX/CuN;

    .line 1945655
    const/4 v1, 0x1

    .line 1945656
    iput-boolean v1, v0, LX/CuN;->b:Z

    .line 1945657
    iget-object v1, v0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    if-eqz v1, :cond_2

    .line 1945658
    iget-object v1, v0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/soundwave/SoundWaveView;->setVisibility(I)V

    .line 1945659
    iget-object v1, v0, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    invoke-virtual {v1}, Lcom/facebook/widget/soundwave/SoundWaveView;->b()V

    .line 1945660
    :cond_2
    return-void

    .line 1945661
    :cond_3
    const/4 v3, 0x0

    .line 1945662
    iget-object v2, v1, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1945663
    iget-object v4, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v4

    .line 1945664
    invoke-virtual {v2}, LX/CtG;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1945665
    iput-boolean v3, v1, LX/Ctz;->d:Z

    .line 1945666
    iget-object v2, v1, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v2, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1945667
    :cond_4
    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v0

    check-cast v0, LX/Cju;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->g:LX/Cju;

    return-void
.end method

.method private c(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1945617
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1945618
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->j:Z

    if-nez v0, :cond_0

    move-object v0, v1

    .line 1945619
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->h:I

    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v5, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->i:I

    move-object v2, v1

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1945620
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_0

    move-object v0, v1

    .line 1945621
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v2, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->h:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V

    .line 1945622
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->i:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 1945623
    :cond_0
    invoke-virtual {p0}, LX/Cti;->getMediaView()LX/Ct1;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    .line 1945624
    invoke-virtual {p0}, LX/Cti;->getMediaView()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1945625
    const/4 v1, 0x1

    sget-object v2, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1945626
    const-class v1, LX/Ctz;

    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->b(Ljava/lang/Class;)LX/Ctr;

    move-result-object v1

    check-cast v1, LX/Ctz;

    .line 1945627
    iget-object v2, v1, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-nez v2, :cond_2

    .line 1945628
    :goto_0
    const-class v1, LX/CuN;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v1

    check-cast v1, LX/CuN;

    .line 1945629
    const/4 v2, 0x0

    .line 1945630
    iput-boolean v2, v1, LX/CuN;->b:Z

    .line 1945631
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1945632
    iget-object v0, v1, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    if-eqz v0, :cond_1

    .line 1945633
    iget-object v0, v1, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    invoke-virtual {v0}, Lcom/facebook/widget/soundwave/SoundWaveView;->a()V

    .line 1945634
    iget-object v0, v1, LX/7NY;->a:Lcom/facebook/widget/soundwave/SoundWaveView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/soundwave/SoundWaveView;->setVisibility(I)V

    .line 1945635
    :cond_1
    return-void

    .line 1945636
    :cond_2
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/Ctz;->d:Z

    .line 1945637
    iget-object v2, v1, LX/Ctz;->a:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Cqw;)V
    .locals 6

    .prologue
    .line 1945596
    invoke-virtual {p0, p1}, LX/Cti;->b(LX/Cqw;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1945597
    invoke-virtual {p0}, LX/Cti;->getCurrentLayout()LX/CrS;

    move-result-object v2

    .line 1945598
    invoke-virtual {p0}, LX/Cti;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/CqX;->b(LX/Cqv;)LX/CrS;

    move-result-object v3

    .line 1945599
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1945600
    invoke-interface {v3}, LX/CrS;->a()LX/Cqv;

    move-result-object v1

    check-cast v1, LX/Cqw;

    .line 1945601
    iget-object v4, v1, LX/Cqw;->e:LX/Cqu;

    move-object v4, v4

    .line 1945602
    invoke-interface {v3}, LX/CrS;->a()LX/Cqv;

    move-result-object v1

    check-cast v1, LX/Cqw;

    .line 1945603
    iget-object v5, v1, LX/Cqw;->f:LX/Cqt;

    move-object v1, v5

    .line 1945604
    sget-object v5, LX/Cqu;->EXPANDED:LX/Cqu;

    if-ne v4, v5, :cond_3

    .line 1945605
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->a(Landroid/view/View;Z)V

    .line 1945606
    :cond_0
    :goto_0
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v3, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1945607
    invoke-virtual {p0}, LX/Cti;->getTransitionStrategy()LX/Cqj;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/CqX;->f(LX/Cqv;)V

    .line 1945608
    invoke-interface {v2}, LX/CrS;->a()LX/Cqv;

    move-result-object v0

    check-cast v0, LX/Cqw;

    invoke-virtual {v0}, LX/Cqw;->e()LX/Cqw;

    move-result-object v0

    .line 1945609
    invoke-virtual {p0}, LX/Cti;->getTransitionStrategy()LX/Cqj;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, LX/Cqj;->a(LX/Cqw;LX/Cqw;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1945610
    invoke-virtual {p0}, LX/Cti;->c()V

    .line 1945611
    :cond_1
    invoke-virtual {p0, p1}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->c(LX/Cqw;)V

    .line 1945612
    :cond_2
    return-void

    .line 1945613
    :cond_3
    sget-object v5, LX/Cqu;->COLLAPSED:LX/Cqu;

    if-ne v4, v5, :cond_5

    sget-object v5, LX/Cqt;->LANDSCAPE_LEFT:LX/Cqt;

    if-eq v1, v5, :cond_4

    sget-object v5, LX/Cqt;->LANDSCAPE_RIGHT:LX/Cqt;

    if-ne v1, v5, :cond_5

    .line 1945614
    :cond_4
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->a(Landroid/view/View;Z)V

    goto :goto_0

    .line 1945615
    :cond_5
    sget-object v5, LX/Cqu;->COLLAPSED:LX/Cqu;

    if-ne v4, v5, :cond_0

    sget-object v4, LX/Cqt;->PORTRAIT:LX/Cqt;

    if-ne v1, v4, :cond_0

    .line 1945616
    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->c(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 1945593
    if-eqz p1, :cond_0

    .line 1945594
    iget-object v0, p0, LX/Cti;->d:LX/Chv;

    new-instance v1, LX/CiK;

    sget-object v2, LX/CiJ;->NATIVE_ADS_VIDEO_SCROLL_FOCUSED_VIEW_TO_RECT:LX/CiJ;

    invoke-direct {v1, v2, p0, p1}, LX/CiK;-><init>(LX/CiJ;Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1945595
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1945589
    iget-object v0, p0, LX/Cti;->d:LX/Chv;

    new-instance v1, LX/CiK;

    sget-object v2, LX/CiJ;->NATIVE_ADS_VIDEO_SET_FOCUSED_VIEW:LX/CiJ;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, v3}, LX/CiK;-><init>(LX/CiJ;Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1945590
    return-void
.end method

.method public setIsEdgeToEdgeEnabled(Z)V
    .locals 0

    .prologue
    .line 1945591
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->j:Z

    .line 1945592
    return-void
.end method
