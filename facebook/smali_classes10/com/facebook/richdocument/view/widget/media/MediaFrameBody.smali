.class public Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;
.super LX/Cte;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "LX/Ct1;",
        ">",
        "LX/Cte;"
    }
.end annotation


# instance fields
.field public d:LX/Ct1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1945319
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1945320
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1945317
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945318
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1945311
    invoke-direct {p0, p1, p2, p3}, LX/Cte;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1945312
    const/4 v0, 0x1

    .line 1945313
    iput-boolean v0, p0, LX/Cte;->i:Z

    .line 1945314
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->setClipChildren(Z)V

    .line 1945315
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->setClipToPadding(Z)V

    .line 1945316
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 1945310
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 1945306
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->getCurrentLayout()LX/CrS;

    move-result-object v0

    sget-object v1, LX/CrQ;->RECT:LX/CrQ;

    const-class v2, LX/CrW;

    invoke-interface {v0, p1, v1, v2}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v0

    check-cast v0, LX/CrW;

    .line 1945307
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1945308
    :cond_0
    iget-object v1, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v0, v1

    .line 1945309
    goto :goto_0
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 1945295
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1945296
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->getOverlayBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1945297
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    invoke-interface {v1}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 1945298
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1945299
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1945300
    sget-object v2, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 1945301
    iget-object v1, p0, LX/Cte;->g:Landroid/graphics/Paint;

    move-object v1, v1

    .line 1945302
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1945303
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1945304
    :cond_0
    :goto_0
    return-void

    .line 1945305
    :cond_1
    invoke-super {p0, p1}, LX/Cte;->a(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1945293
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->getCurrentLayout()LX/CrS;

    move-result-object v0

    sget-object v1, LX/CrQ;->OPACITY:LX/CrQ;

    const-class v2, LX/CrV;

    invoke-interface {v0, p1, v1, v2}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v0

    check-cast v0, LX/CrV;

    .line 1945294
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/CrV;->b()Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentLayout()LX/CrS;
    .locals 1

    .prologue
    .line 1945321
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->getMediaFrame()LX/Ctg;

    move-result-object v0

    invoke-interface {v0}, LX/Ctg;->getCurrentLayout()LX/CrS;

    move-result-object v0

    return-object v0
.end method

.method public getMediaFrame()LX/Ctg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/Ctg",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 1945292
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, LX/Ctg;

    return-object v0
.end method

.method public getMediaView()LX/Ct1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 1945291
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    return-object v0
.end method

.method public getOverlayBounds()Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1945289
    invoke-virtual {p0, p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1945290
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-direct {v1, v3, v3, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method

.method public getOverlayShadowBounds()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1945286
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    if-eqz v0, :cond_0

    .line 1945287
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1945288
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, LX/Cte;->getOverlayShadowBounds()Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewAngle()F
    .locals 3

    .prologue
    .line 1945282
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->getCurrentLayout()LX/CrS;

    move-result-object v0

    sget-object v1, LX/CrQ;->ANGLE:LX/CrQ;

    const-class v2, LX/CrP;

    invoke-interface {v0, p0, v1, v2}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v0

    check-cast v0, LX/CrP;

    .line 1945283
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1945284
    :cond_0
    iget-object v1, v0, LX/CrP;->a:Ljava/lang/Float;

    move-object v0, v1

    .line 1945285
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public final jl_()Z
    .locals 1

    .prologue
    .line 1945272
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    move-object v0, v0

    .line 1945273
    invoke-interface {v0}, LX/Ct1;->jk_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, LX/Cte;->jl_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1945276
    invoke-super/range {p0 .. p5}, LX/Cte;->onLayout(ZIIII)V

    .line 1945277
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1945278
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 1945279
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    invoke-interface {v1}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, LX/Cte;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1945280
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->getViewAngle()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->setRotation(F)V

    .line 1945281
    return-void
.end method

.method public setMediaView(LX/Ct1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 1945274
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/media/MediaFrameBody;->d:LX/Ct1;

    .line 1945275
    return-void
.end method
