.class public Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;
.super Landroid/view/View;
.source ""

# interfaces
.implements LX/CsN;


# instance fields
.field public a:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/graphics/Paint;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/Runnable;

.field private e:Landroid/support/v4/view/ViewPager;

.field private f:F

.field private g:F

.field private h:I

.field private i:Z

.field private j:I

.field private k:F

.field private l:I

.field private m:I

.field private n:Z

.field private o:I

.field private p:F

.field private q:Z

.field private r:I

.field private s:I

.field public t:Z

.field public u:Landroid/animation/AnimatorSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1942672
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1942673
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1942585
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942586
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    .line 1942587
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942588
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->b:Landroid/graphics/Paint;

    .line 1942589
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->c:Ljava/util/List;

    .line 1942590
    new-instance v0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$1;

    invoke-direct {v0, p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$1;-><init>(Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->d:Ljava/lang/Runnable;

    .line 1942591
    const-class v0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1942592
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1942593
    const v1, 0x7f0b12d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1942594
    const v2, 0x7f0c0043

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 1942595
    const v3, 0x7f09000b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    .line 1942596
    const v4, 0x7f0b12d9

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1942597
    const v5, 0x7f0a062e

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 1942598
    const v6, 0x7f09000c

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 1942599
    sget-object v6, LX/03r;->DotCarouselPageIndicator:[I

    const/4 v7, 0x0

    invoke-virtual {p1, p2, v6, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 1942600
    const/16 v7, 0x0

    int-to-float v1, v1

    invoke-virtual {v6, v7, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    .line 1942601
    const/16 v1, 0x1

    int-to-float v4, v4

    invoke-virtual {v6, v1, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->g:F

    .line 1942602
    const/16 v1, 0x2

    invoke-virtual {v6, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    .line 1942603
    const/16 v1, 0x3

    invoke-virtual {v6, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->setCentered(Z)V

    .line 1942604
    const/16 v0, 0x4

    invoke-virtual {v6, v0, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->setEnableScrolling(Z)V

    .line 1942605
    const/16 v0, 0x5

    invoke-virtual {v6, v0, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->setFocusedPageDotColor(I)V

    .line 1942606
    const/16 v0, 0x6

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v6, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->setUnFocusedPageDotRelativeOpacity(F)V

    .line 1942607
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 1942608
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1942609
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->a:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->q:Z

    .line 1942610
    return-void
.end method

.method private static a(IIF)I
    .locals 2

    .prologue
    .line 1942611
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p2

    .line 1942612
    int-to-float v1, p0

    mul-float/2addr v0, v1

    int-to-float v1, p1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v0

    check-cast v0, LX/Crz;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->a:LX/Crz;

    return-void
.end method

.method private b(II)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1942613
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->e:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1942614
    :cond_0
    :goto_0
    return v0

    .line 1942615
    :cond_1
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v1

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    .line 1942616
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    if-ltz p2, :cond_0

    if-ge p2, v1, :cond_0

    if-lt p2, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c(I)I
    .locals 6

    .prologue
    .line 1942617
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1942618
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1942619
    const/high16 v0, 0x40000000    # 2.0f

    if-eq v2, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->e:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    .line 1942620
    :cond_1
    :goto_0
    return v0

    .line 1942621
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    .line 1942622
    iget-boolean v3, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->i:Z

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    if-le v0, v3, :cond_3

    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    .line 1942623
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    iget v5, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    iget v4, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->g:F

    mul-float/2addr v0, v4

    add-float/2addr v0, v3

    float-to-int v0, v0

    .line 1942624
    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1

    .line 1942625
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 1942626
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->b:Landroid/graphics/Paint;

    const/16 v1, 0xff

    iget v2, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->j:I

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    iget v3, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->j:I

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    iget v4, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->j:I

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1942627
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->j:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->l:I

    .line 1942628
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->k:F

    iget v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->l:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->m:I

    .line 1942629
    return-void
.end method

.method private c(II)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x2

    const/4 v1, 0x0

    .line 1942630
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->u:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 1942631
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1942632
    :cond_0
    iput p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->r:I

    .line 1942633
    iput p2, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->s:I

    .line 1942634
    iput-boolean v11, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->t:Z

    .line 1942635
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->s:I

    iget v2, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->r:I

    sub-int/2addr v0, v2

    add-int/lit8 v2, v0, 0x1

    .line 1942636
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    move v0, v1

    .line 1942637
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1942638
    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->c:Ljava/util/List;

    iget v4, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->m:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1942639
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1942640
    :cond_1
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->u:Landroid/animation/AnimatorSet;

    .line 1942641
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 1942642
    :goto_1
    if-ge v0, v2, :cond_3

    .line 1942643
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1942644
    new-instance v5, Landroid/animation/ValueAnimator;

    invoke-direct {v5}, Landroid/animation/ValueAnimator;-><init>()V

    .line 1942645
    const-wide/16 v6, 0x1e

    invoke-virtual {v5, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1942646
    mul-int/lit8 v6, v0, 0x1e

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 1942647
    new-array v6, v10, [F

    fill-array-data v6, :array_0

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 1942648
    new-instance v6, LX/CsJ;

    invoke-direct {v6, p0, v0}, LX/CsJ;-><init>(Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;I)V

    invoke-virtual {v5, v6}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1942649
    new-instance v6, Landroid/animation/ValueAnimator;

    invoke-direct {v6}, Landroid/animation/ValueAnimator;-><init>()V

    .line 1942650
    new-array v7, v10, [F

    fill-array-data v7, :array_1

    invoke-virtual {v6, v7}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 1942651
    const-wide/16 v8, 0x1f4

    invoke-virtual {v6, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1942652
    new-instance v7, LX/CsK;

    invoke-direct {v7, p0, v0}, LX/CsK;-><init>(Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;I)V

    invoke-virtual {v6, v7}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1942653
    add-int/lit8 v7, v2, -0x1

    if-ne v0, v7, :cond_2

    .line 1942654
    new-instance v7, LX/CsL;

    invoke-direct {v7, p0}, LX/CsL;-><init>(Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;)V

    invoke-virtual {v6, v7}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1942655
    :cond_2
    new-array v7, v10, [Landroid/animation/Animator;

    aput-object v5, v7, v1

    aput-object v6, v7, v11

    invoke-virtual {v4, v7}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 1942656
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1942657
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1942658
    :cond_3
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1942659
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1942660
    return-void

    nop

    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f666666    # 0.9f
    .end array-data

    .line 1942661
    :array_1
    .array-data 4
        0x3f666666    # 0.9f
        0x3f000000    # 0.5f
    .end array-data
.end method

.method private d(I)I
    .locals 4

    .prologue
    .line 1942662
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 1942663
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 1942664
    const/high16 v1, 0x40000000    # 2.0f

    if-ne v2, v1, :cond_0

    .line 1942665
    :goto_0
    return v0

    .line 1942666
    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    iget v3, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    mul-float/2addr v1, v3

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getPaddingBottom()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 1942667
    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1

    .line 1942668
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1942669
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->b()V

    .line 1942670
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->s:I

    iput v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->r:I

    .line 1942671
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 0

    .prologue
    .line 1942674
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->invalidate()V

    .line 1942675
    return-void
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 1942676
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->d()V

    .line 1942677
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->requestLayout()V

    .line 1942678
    return-void
.end method

.method public final a(IFI)V
    .locals 0

    .prologue
    .line 1942679
    iput p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->o:I

    .line 1942680
    iput p2, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->p:F

    .line 1942681
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->invalidate()V

    .line 1942682
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1942683
    invoke-direct {p0, p1, p2}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->b(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1942684
    :goto_0
    return-void

    .line 1942685
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->c(II)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1942686
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->t:Z

    .line 1942687
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->u:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    .line 1942688
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->u:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1942689
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->d:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1942690
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->invalidate()V

    .line 1942691
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 1942583
    return-void
.end method

.method public getDotRadius()F
    .locals 1

    .prologue
    .line 1942584
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    return v0
.end method

.method public getDotSpacing()F
    .locals 1

    .prologue
    .line 1942488
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->g:F

    return v0
.end method

.method public getFocusedPageDotColor()I
    .locals 1

    .prologue
    .line 1942489
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->j:I

    return v0
.end method

.method public getFragmentPager()LX/CqD;
    .locals 1

    .prologue
    .line 1942490
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaxDots()I
    .locals 1

    .prologue
    .line 1942491
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    return v0
.end method

.method public getUnfocusedPageDotRelativeOpacity()F
    .locals 1

    .prologue
    .line 1942492
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->k:F

    return v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0xc41dc6b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1942493
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 1942494
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->d:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1942495
    const/16 v1, 0x2d

    const v2, 0x5a6f76da

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 1942496
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1942497
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->e:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_1

    .line 1942498
    :cond_0
    :goto_0
    return-void

    .line 1942499
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->e:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v4

    .line 1942500
    if-eqz v4, :cond_0

    .line 1942501
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getWidth()I

    move-result v1

    .line 1942502
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getPaddingLeft()I

    move-result v2

    .line 1942503
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getPaddingRight()I

    move-result v5

    .line 1942504
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->getPaddingTop()I

    move-result v0

    .line 1942505
    int-to-float v0, v0

    iget v6, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    add-float/2addr v6, v0

    .line 1942506
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->q:Z

    if-eqz v0, :cond_3

    int-to-float v0, v5

    iget v7, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    add-float/2addr v0, v7

    .line 1942507
    :goto_1
    iget v7, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    mul-float/2addr v7, v9

    iget v8, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->g:F

    add-float/2addr v7, v8

    .line 1942508
    iget-boolean v8, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->n:Z

    if-eqz v8, :cond_2

    .line 1942509
    sub-int v2, v1, v2

    sub-int/2addr v2, v5

    .line 1942510
    add-int/lit8 v5, v4, -0x1

    int-to-float v5, v5

    mul-float/2addr v5, v7

    iget v8, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    mul-float/2addr v8, v9

    add-float/2addr v5, v8

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1942511
    int-to-float v2, v2

    div-float/2addr v2, v9

    int-to-float v5, v5

    div-float/2addr v5, v9

    sub-float/2addr v2, v5

    iget v5, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    sub-float/2addr v2, v5

    add-float/2addr v0, v2

    .line 1942512
    :cond_2
    iget-boolean v2, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->q:Z

    if-eqz v2, :cond_a

    .line 1942513
    int-to-float v1, v1

    sub-float v0, v1, v0

    move v1, v0

    .line 1942514
    :goto_2
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    mul-float/2addr v0, v9

    iget v2, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->g:F

    add-float v5, v0, v2

    move v2, v3

    .line 1942515
    :goto_3
    if-ge v2, v4, :cond_8

    .line 1942516
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->q:Z

    if-eqz v0, :cond_4

    neg-int v0, v2

    :goto_4
    int-to-float v0, v0

    mul-float/2addr v0, v5

    add-float v8, v1, v0

    .line 1942517
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->o:I

    if-ne v2, v0, :cond_5

    .line 1942518
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->l:I

    iget v9, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->m:I

    iget v10, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->p:F

    invoke-static {v0, v9, v10}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->a(IIF)I

    move-result v0

    .line 1942519
    :goto_5
    iget-object v9, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1942520
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    iget-object v9, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v6, v0, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1942521
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1942522
    :cond_3
    int-to-float v0, v2

    iget v7, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    add-float/2addr v0, v7

    goto :goto_1

    :cond_4
    move v0, v2

    .line 1942523
    goto :goto_4

    .line 1942524
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->t:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->r:I

    if-lt v2, v0, :cond_6

    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->s:I

    if-gt v2, v0, :cond_6

    .line 1942525
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->c:Ljava/util/List;

    iget v9, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->r:I

    sub-int v9, v2, v9

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_5

    .line 1942526
    :cond_6
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->o:I

    add-int/lit8 v0, v0, 0x1

    if-ne v2, v0, :cond_7

    .line 1942527
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->m:I

    iget v9, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->l:I

    iget v10, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->p:F

    invoke-static {v0, v9, v10}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->a(IIF)I

    move-result v0

    goto :goto_5

    .line 1942528
    :cond_7
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->m:I

    goto :goto_5

    .line 1942529
    :cond_8
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    if-le v4, v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->i:Z

    if-eqz v0, :cond_0

    .line 1942530
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->o:I

    add-int/lit8 v0, v0, 0x1

    .line 1942531
    add-int/lit8 v1, v0, 0x1

    iget v2, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    if-lt v1, v2, :cond_0

    .line 1942532
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    sub-int/2addr v0, v1

    .line 1942533
    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->p:F

    add-float/2addr v0, v1

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1942534
    iget-boolean v1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->q:Z

    if-eqz v1, :cond_9

    neg-int v0, v0

    :cond_9
    invoke-virtual {p0, v0, v3}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->scrollTo(II)V

    goto/16 :goto_0

    :cond_a
    move v1, v0

    goto/16 :goto_2
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1942581
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->c(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->d(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->setMeasuredDimension(II)V

    .line 1942582
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1942535
    check-cast p1, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;

    .line 1942536
    invoke-virtual {p1}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1942537
    iget v0, p1, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;->a:I

    iput v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->o:I

    .line 1942538
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->requestLayout()V

    .line 1942539
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1942540
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1942541
    new-instance v1, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1942542
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->o:I

    iput v0, v1, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator$SavedState;->a:I

    .line 1942543
    return-object v1
.end method

.method public setCentered(Z)V
    .locals 3

    .prologue
    .line 1942544
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->n:Z

    if-eq v0, p1, :cond_1

    .line 1942545
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->i:Z

    if-eqz v0, :cond_0

    .line 1942546
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot center dots when scrolling is enabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1942547
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->n:Z

    .line 1942548
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->invalidate()V

    .line 1942549
    :cond_1
    return-void
.end method

.method public setDotRadius(F)V
    .locals 1

    .prologue
    .line 1942550
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 1942551
    iput p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->f:F

    .line 1942552
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->invalidate()V

    .line 1942553
    :cond_0
    return-void
.end method

.method public setDotSpacing(F)V
    .locals 1

    .prologue
    .line 1942554
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->g:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 1942555
    iput p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->g:F

    .line 1942556
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->invalidate()V

    .line 1942557
    :cond_0
    return-void
.end method

.method public setEnableScrolling(Z)V
    .locals 3

    .prologue
    .line 1942558
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->i:Z

    if-eq v0, p1, :cond_1

    .line 1942559
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->n:Z

    if-eqz v0, :cond_0

    .line 1942560
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot be scrollable when dot centering is enabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1942561
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->i:Z

    .line 1942562
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->requestLayout()V

    .line 1942563
    :cond_1
    return-void
.end method

.method public setFocusedPageDotColor(I)V
    .locals 1

    .prologue
    .line 1942564
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->j:I

    if-eq v0, p1, :cond_0

    .line 1942565
    iput p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->j:I

    .line 1942566
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->c()V

    .line 1942567
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->invalidate()V

    .line 1942568
    :cond_0
    return-void
.end method

.method public setFragmentPager(LX/CqD;)V
    .locals 0

    .prologue
    .line 1942569
    return-void
.end method

.method public setMaxDots(I)V
    .locals 1

    .prologue
    .line 1942570
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    if-eq v0, p1, :cond_0

    if-lez p1, :cond_0

    .line 1942571
    iput p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->h:I

    .line 1942572
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->requestLayout()V

    .line 1942573
    :cond_0
    return-void
.end method

.method public setUnFocusedPageDotRelativeOpacity(F)V
    .locals 1

    .prologue
    .line 1942574
    iget v0, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->k:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 1942575
    iput p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->k:F

    .line 1942576
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->c()V

    .line 1942577
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->invalidate()V

    .line 1942578
    :cond_0
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 0

    .prologue
    .line 1942579
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/DotCarouselPageIndicator;->e:Landroid/support/v4/view/ViewPager;

    .line 1942580
    return-void
.end method
