.class public Lcom/facebook/richdocument/view/widget/video/VideoControlPlayIcon;
.super LX/Cue;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1947053
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/video/VideoControlPlayIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1947054
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1947055
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Cue;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947056
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1947057
    invoke-direct {p0, p1, p2, p3}, LX/Cue;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947058
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;)Landroid/graphics/Path;
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1947059
    const v0, 0x3f5a5a5a

    invoke-static {p1, v0}, LX/CsC;->a(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v0

    .line 1947060
    iget v1, p0, LX/CsC;->d:F

    iget v2, p0, LX/CsC;->a:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, LX/CsC;->c:F

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    .line 1947061
    iget v2, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v1

    .line 1947062
    iget v3, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v1

    .line 1947063
    iget v4, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v4, v1

    .line 1947064
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v1

    .line 1947065
    sub-float v1, v4, v2

    .line 1947066
    sub-float/2addr v0, v3

    .line 1947067
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947068
    new-instance v2, Landroid/graphics/PointF;

    iget v3, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v3

    iget v3, v4, Landroid/graphics/PointF;->y:F

    div-float v5, v0, v5

    add-float/2addr v3, v5

    invoke-direct {v2, v1, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947069
    new-instance v1, Landroid/graphics/PointF;

    iget v3, v4, Landroid/graphics/PointF;->x:F

    iget v5, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v5

    invoke-direct {v1, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947070
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 1947071
    iget v3, v4, Landroid/graphics/PointF;->x:F

    iget v5, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v3, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1947072
    iget v3, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947073
    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947074
    iget v1, v4, Landroid/graphics/PointF;->x:F

    iget v2, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947075
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1947076
    return-object v0
.end method
