.class public Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;
.super LX/Cud;
.source ""


# instance fields
.field private a:LX/Clj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1946999
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1947000
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1946997
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946998
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1946994
    invoke-direct {p0, p1, p2, p3}, LX/Cud;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1946995
    new-instance v0, LX/Clj;

    invoke-direct {v0}, LX/Clj;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;->a:LX/Clj;

    .line 1946996
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;)LX/Cud;
    .locals 3

    .prologue
    .line 1946991
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1946992
    const v1, 0x7f03121b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/Cud;

    .line 1946993
    return-object v0
.end method


# virtual methods
.method public final a(LX/Cuw;LX/Cub;)V
    .locals 1

    .prologue
    .line 1946989
    new-instance v0, LX/Cuc;

    invoke-direct {v0, p0, p2, p1}, LX/Cuc;-><init>(Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;LX/Cub;LX/Cuw;)V

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1946990
    return-void
.end method

.method public bridge synthetic getAnnotation()LX/ClU;
    .locals 1

    .prologue
    .line 1946984
    invoke-virtual {p0}, LX/Cud;->getAnnotation()LX/Clj;

    move-result-object v0

    return-object v0
.end method

.method public getAnnotation()LX/Clj;
    .locals 1

    .prologue
    .line 1946988
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/video/InstantArticlesVideoControlsView;->a:LX/Clj;

    return-object v0
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1946987
    const v0, 0x7f03121c

    return v0
.end method

.method public getPauseIcon()Landroid/view/View;
    .locals 1

    .prologue
    .line 1946986
    const v0, 0x7f0d2a56

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getPlayIcon()Landroid/view/View;
    .locals 1

    .prologue
    .line 1946985
    const v0, 0x7f0d2a55

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
