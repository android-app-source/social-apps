.class public Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements LX/CnQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/LinearLayout;",
        "LX/CnQ",
        "<",
        "LX/Clk;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/Clk;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1947266
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1947267
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1947268
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947269
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1947262
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947263
    new-instance v0, LX/Clk;

    invoke-direct {v0}, LX/Clk;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;->a:LX/Clk;

    .line 1947264
    return-void
.end method


# virtual methods
.method public final c()Landroid/view/View;
    .locals 0

    .prologue
    .line 1947259
    return-object p0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1947265
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic getAnnotation()LX/ClU;
    .locals 1

    .prologue
    .line 1947260
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;->a:LX/Clk;

    move-object v0, v0

    .line 1947261
    return-object v0
.end method

.method public getAnnotation()LX/Clk;
    .locals 1

    .prologue
    .line 1947258
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;->a:LX/Clk;

    return-object v0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1947254
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 1947255
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1947256
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/richdocument/view/widget/video/VideoSeekBarView;->setMeasuredDimension(II)V

    .line 1947257
    return-void
.end method

.method public setIsOverlay(Z)V
    .locals 0

    .prologue
    .line 1947253
    return-void
.end method
