.class public Lcom/facebook/richdocument/view/widget/video/VideoControlPauseIcon;
.super LX/Cue;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1947051
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/video/VideoControlPauseIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1947052
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1947049
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Cue;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947050
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1947012
    invoke-direct {p0, p1, p2, p3}, LX/Cue;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1947013
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;)Landroid/graphics/Path;
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    .line 1947015
    const v0, 0x3f4ccccd    # 0.8f

    invoke-static {p1, v0}, LX/CsC;->a(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v0

    .line 1947016
    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget v3, p0, LX/CsC;->d:F

    mul-float/2addr v3, v11

    sub-float/2addr v2, v3

    iget v3, p0, LX/CsC;->a:I

    mul-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    .line 1947017
    iget v2, p0, LX/CsC;->d:F

    iget v3, p0, LX/CsC;->a:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, p0, LX/CsC;->c:F

    div-float/2addr v3, v11

    add-float/2addr v2, v3

    .line 1947018
    iget v3, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v2

    .line 1947019
    iget v4, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v2

    .line 1947020
    add-float v5, v3, v1

    iget v6, p0, LX/CsC;->c:F

    sub-float/2addr v5, v6

    .line 1947021
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v2

    .line 1947022
    sub-float v2, v5, v3

    .line 1947023
    sub-float/2addr v0, v4

    .line 1947024
    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947025
    new-instance v3, Landroid/graphics/PointF;

    iget v7, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v7, v2

    iget v8, v6, Landroid/graphics/PointF;->y:F

    invoke-direct {v3, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947026
    new-instance v7, Landroid/graphics/PointF;

    iget v8, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, v8

    iget v8, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v8, v0

    invoke-direct {v7, v2, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947027
    new-instance v2, Landroid/graphics/PointF;

    iget v8, v6, Landroid/graphics/PointF;->x:F

    iget v9, v6, Landroid/graphics/PointF;->y:F

    add-float/2addr v9, v0

    invoke-direct {v2, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947028
    new-instance v8, Landroid/graphics/Path;

    invoke-direct {v8}, Landroid/graphics/Path;-><init>()V

    .line 1947029
    iget v9, v6, Landroid/graphics/PointF;->x:F

    iget v10, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1947030
    iget v9, v3, Landroid/graphics/PointF;->x:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v9, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947031
    iget v3, v7, Landroid/graphics/PointF;->x:F

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v3, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947032
    iget v3, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947033
    iget v2, v6, Landroid/graphics/PointF;->x:F

    iget v3, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947034
    div-float v2, v1, v11

    .line 1947035
    add-float/2addr v2, v5

    iget v3, p0, LX/CsC;->c:F

    add-float/2addr v2, v3

    .line 1947036
    add-float/2addr v1, v2

    iget v3, p0, LX/CsC;->c:F

    sub-float/2addr v1, v3

    .line 1947037
    sub-float/2addr v1, v2

    .line 1947038
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947039
    new-instance v2, Landroid/graphics/PointF;

    iget v4, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v1

    iget v5, v3, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947040
    new-instance v4, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v5

    iget v5, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v5, v0

    invoke-direct {v4, v1, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947041
    new-instance v1, Landroid/graphics/PointF;

    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget v6, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v6

    invoke-direct {v1, v5, v0}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1947042
    iget v0, v3, Landroid/graphics/PointF;->x:F

    iget v5, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v0, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1947043
    iget v0, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947044
    iget v0, v4, Landroid/graphics/PointF;->x:F

    iget v2, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947045
    iget v0, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947046
    iget v0, v3, Landroid/graphics/PointF;->x:F

    iget v1, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1947047
    invoke-virtual {v8}, Landroid/graphics/Path;->close()V

    .line 1947048
    return-object v8
.end method

.method public setLoading(Z)V
    .locals 0

    .prologue
    .line 1947014
    return-void
.end method
