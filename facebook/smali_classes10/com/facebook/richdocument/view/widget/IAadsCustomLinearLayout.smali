.class public Lcom/facebook/richdocument/view/widget/IAadsCustomLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/1OV;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:LX/1OV;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1942950
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1942951
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1942961
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1942962
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1942959
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942960
    return-void
.end method


# virtual methods
.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1942958
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/IAadsCustomLinearLayout;->a:LX/1OV;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/IAadsCustomLinearLayout;->a:LX/1OV;

    invoke-interface {v0, p1}, LX/1OV;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x17654b8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1942957
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, 0x3890252a

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setMultishareOnInterceptTouchEventListener(LX/1OV;)V
    .locals 1

    .prologue
    .line 1942952
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/IAadsCustomLinearLayout;->a:LX/1OV;

    .line 1942953
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/IAadsCustomLinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/richdocument/view/widget/IAadsFrameLayout;

    if-eqz v0, :cond_0

    .line 1942954
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/IAadsCustomLinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/IAadsFrameLayout;

    .line 1942955
    iput-object p1, v0, Lcom/facebook/richdocument/view/widget/IAadsFrameLayout;->a:LX/1OV;

    .line 1942956
    :cond_0
    return-void
.end method
