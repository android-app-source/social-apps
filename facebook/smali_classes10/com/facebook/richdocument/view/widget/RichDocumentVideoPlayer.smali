.class public Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;
.super LX/2oW;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Cpn;
.implements LX/Ct1;


# instance fields
.field public m:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/Ctm;

.field private o:Z

.field public p:LX/Cpn;

.field private q:Z

.field public r:LX/Cso;

.field private s:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1944115
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1944116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1944117
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944118
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1944119
    invoke-direct {p0, p1, p2, p3}, LX/2oW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944120
    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->q:Z

    .line 1944121
    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->t:Z

    .line 1944122
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->c()V

    .line 1944123
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    invoke-static {v0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v0

    check-cast v0, LX/1C2;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->m:LX/1C2;

    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1944124
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v1

    .line 1944125
    if-nez v1, :cond_0

    .line 1944126
    :goto_0
    return v0

    .line 1944127
    :cond_0
    sget-object v1, LX/CtB;->a:[I

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v2

    invoke-virtual {v2}, LX/2qV;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1944128
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/content/Context;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/0Px",
            "<+",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1944129
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1944130
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->l()Z

    .line 1944131
    new-instance v1, LX/Csy;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-direct {v1, v2, p0, v3}, LX/Csy;-><init>(Landroid/content/Context;LX/2oW;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1944132
    new-instance v1, LX/Ctw;

    invoke-direct {v1, p1}, LX/Ctw;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1944133
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1944165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->o:Z

    .line 1944166
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->p:LX/Cpn;

    if-eqz v0, :cond_0

    .line 1944167
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->p:LX/Cpn;

    invoke-interface {v0}, LX/Cpn;->a()V

    .line 1944168
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 1944134
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->n:LX/Ctm;

    int-to-float v1, p1

    int-to-float v2, p2

    div-float/2addr v1, v2

    .line 1944135
    iput v1, v0, LX/Ctm;->b:F

    .line 1944136
    return-void
.end method

.method public final a(LX/Cuo;)V
    .locals 2

    .prologue
    .line 1944141
    if-nez p1, :cond_1

    .line 1944142
    :cond_0
    :goto_0
    return-void

    .line 1944143
    :cond_1
    iget-object v0, p1, LX/Cuo;->a:LX/Cup;

    sget-object v1, LX/Cup;->MUTE:LX/Cup;

    if-ne v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1944144
    if-eqz v0, :cond_2

    .line 1944145
    const/4 v0, 0x1

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    goto :goto_0

    .line 1944146
    :cond_2
    iget-object v0, p1, LX/Cuo;->a:LX/Cup;

    sget-object v1, LX/Cup;->UNMUTE:LX/Cup;

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 1944147
    if-eqz v0, :cond_3

    .line 1944148
    const/4 v0, 0x0

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    goto :goto_0

    .line 1944149
    :cond_3
    iget-object v0, p1, LX/Cuo;->a:LX/Cup;

    sget-object v1, LX/Cup;->USE_EXISTING_MUTE_STATE:LX/Cup;

    if-ne v0, v1, :cond_6

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 1944150
    if-eqz v0, :cond_0

    .line 1944151
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->q:Z

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final a(ZLX/04g;)V
    .locals 0

    .prologue
    .line 1944152
    invoke-super {p0, p1, p2}, LX/2oW;->a(ZLX/04g;)V

    .line 1944153
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->q:Z

    .line 1944154
    return-void
.end method

.method public final b(Z)V
    .locals 8

    .prologue
    .line 1944155
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->s:LX/2pa;

    if-nez v0, :cond_0

    .line 1944156
    :goto_0
    return-void

    .line 1944157
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->s:LX/2pa;

    iget-object v7, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1944158
    if-eqz p1, :cond_1

    .line 1944159
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->m:LX/1C2;

    iget-object v1, v7, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v2, LX/04G;->RICH_DOCUMENT:LX/04G;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 1944160
    iget-object v6, p0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v6, v6

    .line 1944161
    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_0

    .line 1944162
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->m:LX/1C2;

    iget-object v1, v7, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v2, LX/04G;->RICH_DOCUMENT:LX/04G;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, v7, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    .line 1944163
    iget-object v6, p0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v6, v6

    .line 1944164
    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 1944112
    const-class v0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1944113
    new-instance v0, LX/Ctm;

    invoke-direct {v0, p0}, LX/Ctm;-><init>(LX/Ct1;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->n:LX/Ctm;

    .line 1944114
    return-void
.end method

.method public final declared-synchronized c(LX/2pa;)V
    .locals 1

    .prologue
    .line 1944137
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, LX/2oW;->c(LX/2pa;)V

    .line 1944138
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->s:LX/2pa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1944139
    monitor-exit p0

    return-void

    .line 1944140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getAudioPolicy()LX/Cso;
    .locals 1

    .prologue
    .line 1944111
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->r:LX/Cso;

    return-object v0
.end method

.method public getDefaultPlayerOrigin()LX/04D;
    .locals 1

    .prologue
    .line 1944110
    sget-object v0, LX/04D;->INSTANT_ARTICLES:LX/04D;

    return-object v0
.end method

.method public getDefaultPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 1944109
    sget-object v0, LX/04G;->RICH_DOCUMENT:LX/04G;

    return-object v0
.end method

.method public getMediaAspectRatio()F
    .locals 1

    .prologue
    .line 1944106
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->n:LX/Ctm;

    .line 1944107
    iget p0, v0, LX/Ctm;->b:F

    move v0, p0

    .line 1944108
    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 1944091
    return-object p0
.end method

.method public final jk_()Z
    .locals 1

    .prologue
    .line 1944105
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->o:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1944104
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->q:Z

    return v0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1944101
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->n:LX/Ctm;

    invoke-virtual {v0}, LX/Ctm;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 1944102
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, LX/2oW;->onMeasure(II)V

    .line 1944103
    return-void
.end method

.method public setAudioPolicy(LX/Cso;)V
    .locals 0

    .prologue
    .line 1944099
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->r:LX/Cso;

    .line 1944100
    return-void
.end method

.method public setCoverImageListener(LX/Cpn;)V
    .locals 0

    .prologue
    .line 1944097
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->p:LX/Cpn;

    .line 1944098
    return-void
.end method

.method public setVideoControlsEnabled(Z)V
    .locals 0

    .prologue
    .line 1944095
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->t:Z

    .line 1944096
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1944094
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 1944092
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->o:Z

    .line 1944093
    return-void
.end method
