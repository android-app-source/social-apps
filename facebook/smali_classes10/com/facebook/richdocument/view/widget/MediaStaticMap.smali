.class public Lcom/facebook/richdocument/view/widget/MediaStaticMap;
.super Lcom/facebook/maps/FbStaticMapView;
.source ""


# instance fields
.field public d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1943336
    invoke-direct {p0, p1}, Lcom/facebook/maps/FbStaticMapView;-><init>(Landroid/content/Context;)V

    .line 1943337
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1943334
    invoke-direct {p0, p1, p2}, Lcom/facebook/maps/FbStaticMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1943335
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1943332
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/maps/FbStaticMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943333
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/16 v5, 0x10

    .line 1943310
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, LX/Ctg;

    .line 1943311
    invoke-interface {v0}, LX/Ctg;->getTransitionStrategy()LX/Cqj;

    move-result-object v3

    .line 1943312
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 1943313
    invoke-virtual {p0, v2}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1943314
    instance-of v4, v1, Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    .line 1943315
    invoke-interface {v0}, LX/Ctg;->getCurrentLayout()LX/CrS;

    move-result-object v2

    .line 1943316
    iget-object v0, v3, LX/Cqi;->b:Landroid/graphics/Rect;

    move-object v3, v0

    .line 1943317
    move-object v0, v1

    .line 1943318
    check-cast v0, Landroid/widget/ImageView;

    .line 1943319
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1943320
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1943321
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iput v3, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1943322
    iput v5, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1943323
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1943324
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->d:Landroid/view/View;

    sget-object v1, LX/CrQ;->RECT:LX/CrQ;

    const-class v3, LX/CrW;

    invoke-interface {v2, v0, v1, v3}, LX/CrS;->a(Landroid/view/View;LX/CrQ;Ljava/lang/Class;)LX/CqY;

    move-result-object v0

    check-cast v0, LX/CrW;

    .line 1943325
    iget-object v1, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v1, v1

    .line 1943326
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1943327
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1943328
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1943329
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1943330
    :cond_0
    return-void

    .line 1943331
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x69cafd4c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1943305
    invoke-super {p0}, Lcom/facebook/maps/FbStaticMapView;->onAttachedToWindow()V

    .line 1943306
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->b()V

    .line 1943307
    const/16 v1, 0x2d

    const v2, 0x6d88c16d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setMapPlaceholder(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1943308
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/MediaStaticMap;->d:Landroid/view/View;

    .line 1943309
    return-void
.end method
