.class public Lcom/facebook/richdocument/view/widget/PressStateButton;
.super Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;
.source ""

# interfaces
.implements LX/20T;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:LX/215;

.field public f:Z

.field private g:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1943511
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/PressStateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1943512
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1943509
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/PressStateButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943510
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1943497
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943498
    iput-boolean v2, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->f:Z

    .line 1943499
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->g:F

    .line 1943500
    const-class v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/PressStateButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1943501
    new-instance v0, LX/Css;

    invoke-direct {v0, p0}, LX/Css;-><init>(Lcom/facebook/richdocument/view/widget/PressStateButton;)V

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1943502
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->e:LX/215;

    .line 1943503
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->e:LX/215;

    invoke-virtual {v0, p0}, LX/215;->a(LX/20T;)V

    .line 1943504
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->e:LX/215;

    .line 1943505
    iput-boolean v2, v0, LX/215;->d:Z

    .line 1943506
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setClipChildren(Z)V

    .line 1943507
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setClipToPadding(Z)V

    .line 1943508
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/PressStateButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/16 v1, 0x13a4

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->a:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 1943493
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->f:Z

    if-eqz v0, :cond_0

    .line 1943494
    invoke-virtual {p0, p1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageScaleX(F)V

    .line 1943495
    invoke-virtual {p0, p1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageScaleY(F)V

    .line 1943496
    :cond_0
    return-void
.end method

.method public setDrawableBaseScale(F)V
    .locals 1

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1943489
    iput p1, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->g:F

    .line 1943490
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageScaleX(F)V

    .line 1943491
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageScaleY(F)V

    .line 1943492
    return-void
.end method

.method public setHasAnimation(Z)V
    .locals 0

    .prologue
    .line 1943487
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->f:Z

    .line 1943488
    return-void
.end method

.method public setImageScaleX(F)V
    .locals 1

    .prologue
    .line 1943484
    iget v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->g:F

    mul-float/2addr v0, p1

    .line 1943485
    invoke-super {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageScaleX(F)V

    .line 1943486
    return-void
.end method

.method public setImageScaleY(F)V
    .locals 1

    .prologue
    .line 1943481
    iget v0, p0, Lcom/facebook/richdocument/view/widget/PressStateButton;->g:F

    mul-float/2addr v0, p1

    .line 1943482
    invoke-super {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageScaleY(F)V

    .line 1943483
    return-void
.end method
