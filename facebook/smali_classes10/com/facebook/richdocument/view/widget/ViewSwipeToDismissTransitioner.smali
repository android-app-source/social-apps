.class public Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final g:LX/0wT;


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CsO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final h:I

.field public i:Landroid/view/View;

.field private j:F

.field private k:F

.field private l:F

.field private m:LX/CtU;

.field private n:I

.field private o:LX/0wd;

.field private p:Landroid/view/VelocityTracker;

.field private q:Z

.field public r:Z

.field private s:Z

.field public t:Z

.field public u:Z

.field private v:Z

.field private w:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1944990
    const-wide v0, 0x4071800000000000L    # 280.0

    const-wide/high16 v2, 0x403e000000000000L    # 30.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->g:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1944963
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1944964
    sget-object v0, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    .line 1944965
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->t:Z

    .line 1944966
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    .line 1944967
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->v:Z

    .line 1944968
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->w:Z

    .line 1944969
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1283

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->h:I

    .line 1944970
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->f()V

    .line 1944971
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1944972
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1944973
    sget-object v0, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    .line 1944974
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->t:Z

    .line 1944975
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    .line 1944976
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->v:Z

    .line 1944977
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->w:Z

    .line 1944978
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1283

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->h:I

    .line 1944979
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->f()V

    .line 1944980
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1944981
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944982
    sget-object v0, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    .line 1944983
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->t:Z

    .line 1944984
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    .line 1944985
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->v:Z

    .line 1944986
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->w:Z

    .line 1944987
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1283

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->h:I

    .line 1944988
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->f()V

    .line 1944989
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;LX/0wW;LX/Chv;LX/CsO;LX/Crz;LX/0Uh;LX/CqV;)V
    .locals 0

    .prologue
    .line 1945006
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a:LX/0wW;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b:LX/Chv;

    iput-object p3, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->c:LX/CsO;

    iput-object p4, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->d:LX/Crz;

    iput-object p5, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->e:LX/0Uh;

    iput-object p6, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->f:LX/CqV;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-static {v6}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    invoke-static {v6}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v2

    check-cast v2, LX/Chv;

    invoke-static {v6}, LX/CsO;->a(LX/0QB;)LX/CsO;

    move-result-object v3

    check-cast v3, LX/CsO;

    invoke-static {v6}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v4

    check-cast v4, LX/Crz;

    invoke-static {v6}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v6}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v6

    check-cast v6, LX/CqV;

    invoke-static/range {v0 .. v6}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;LX/0wW;LX/Chv;LX/CsO;LX/Crz;LX/0Uh;LX/CqV;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;F)V
    .locals 3

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    const/4 v1, 0x0

    .line 1944991
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->r:Z

    if-eqz v0, :cond_1

    .line 1944992
    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    .line 1944993
    div-float/2addr p1, v2

    .line 1944994
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setX(F)V

    .line 1944995
    return-void

    .line 1944996
    :cond_1
    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    .line 1944997
    div-float/2addr p1, v2

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;FZ)V
    .locals 2

    .prologue
    .line 1944998
    invoke-static {p0, p1}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;F)V

    .line 1944999
    if-eqz p2, :cond_0

    .line 1945000
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b:LX/Chv;

    new-instance v1, LX/CiL;

    invoke-direct {v1}, LX/CiL;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1945001
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->o:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->k()LX/0wd;

    .line 1945002
    if-nez p2, :cond_1

    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->q:Z

    if-nez v0, :cond_1

    .line 1945003
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->q:Z

    .line 1945004
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b:LX/Chv;

    new-instance v1, LX/CiF;

    invoke-direct {v1}, LX/CiF;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1945005
    :cond_1
    return-void
.end method

.method public static a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;ZF)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1944945
    iget-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->r:Z

    if-eqz v1, :cond_3

    .line 1944946
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 1944947
    :cond_0
    :goto_0
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->t:Z

    if-eqz v1, :cond_2

    :cond_1
    if-eqz p1, :cond_4

    iget-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    if-nez v1, :cond_4

    .line 1944948
    :cond_2
    invoke-static {p0, v0, p1}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;FZ)V

    .line 1944949
    :goto_1
    return-void

    .line 1944950
    :cond_3
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getWidth()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0

    .line 1944951
    :cond_4
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b(Landroid/view/View;)F

    move-result v1

    .line 1944952
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->o:LX/0wd;

    float-to-double v4, p2

    invoke-virtual {v2, v4, v5}, LX/0wd;->c(D)LX/0wd;

    .line 1944953
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->o:LX/0wd;

    new-instance v3, LX/CtS;

    invoke-direct {v3, p0, v0, p1}, LX/CtS;-><init>(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;FZ)V

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1944954
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->o:LX/0wd;

    float-to-double v4, v1

    invoke-virtual {v2, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 1944955
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->o:LX/0wd;

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_1
.end method

.method private b(Landroid/view/View;)F
    .locals 4

    .prologue
    const/high16 v3, 0x41200000    # 10.0f

    const/4 v2, 0x0

    .line 1944956
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v0

    .line 1944957
    iget-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->r:Z

    if-eqz v1, :cond_1

    .line 1944958
    cmpl-float v1, v0, v2

    if-lez v1, :cond_0

    .line 1944959
    mul-float/2addr v0, v3

    .line 1944960
    :cond_0
    :goto_0
    return v0

    .line 1944961
    :cond_1
    cmpg-float v1, v0, v2

    if-gez v1, :cond_0

    .line 1944962
    mul-float/2addr v0, v3

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 1944934
    const-class v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1944935
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->e:LX/0Uh;

    const/16 v1, 0xb4

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->s:Z

    .line 1944936
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->d:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->r:Z

    .line 1944937
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->p:Landroid/view/VelocityTracker;

    .line 1944938
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->g:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide v2, 0x4093880000000000L    # 1250.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->c(D)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1944939
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1944940
    move-object v0, v0

    .line 1944941
    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->o:LX/0wd;

    .line 1944942
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0618

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->n:I

    .line 1944943
    new-instance v0, LX/CtR;

    invoke-direct {v0, p0}, LX/CtR;-><init>(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;)V

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1944944
    return-void
.end method

.method public static g(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;)V
    .locals 4

    .prologue
    .line 1944929
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b(Landroid/view/View;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1944930
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    sub-float v0, v1, v0

    .line 1944931
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->n:I

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    iget v2, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->n:I

    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v2

    iget v3, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->n:I

    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 1944932
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->setBackgroundColor(I)V

    .line 1944933
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 1944927
    const/4 v0, 0x0

    const v1, 0x449c4000    # 1250.0f

    invoke-static {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;ZF)V

    .line 1944928
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1944925
    const/4 v0, 0x1

    const v1, 0x449c4000    # 1250.0f

    invoke-static {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;ZF)V

    .line 1944926
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1944889
    :try_start_0
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->c:LX/CsO;

    invoke-virtual {v0}, LX/CsO;->a()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->f:LX/CqV;

    .line 1944890
    iget-boolean v3, v0, LX/CqV;->a:Z

    move v0, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1944891
    if-nez v0, :cond_1

    .line 1944892
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    move v0, v1

    :goto_0
    return v0

    .line 1944893
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    if-nez v0, :cond_2

    .line 1944894
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 1944895
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    goto :goto_0

    .line 1944896
    :cond_2
    :try_start_2
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->v:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    instance-of v0, v0, LX/CtT;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    check-cast v0, LX/CtT;

    invoke-interface {v0, p1}, LX/CtT;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->v:Z

    .line 1944897
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->v:Z

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v2, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eq v0, v4, :cond_5

    .line 1944898
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1944899
    goto :goto_1

    .line 1944900
    :cond_5
    :try_start_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    sget-object v3, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    if-ne v0, v3, :cond_6

    .line 1944901
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 1944902
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1944903
    sget-object v0, LX/CtU;->WAITING_FOR_MOVES:LX/CtU;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    .line 1944904
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->j:F

    .line 1944905
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->k:F
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1944906
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    move v0, v1

    goto :goto_0

    .line 1944907
    :cond_6
    :try_start_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_b

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    sget-object v3, LX/CtU;->WAITING_FOR_MOVES:LX/CtU;

    if-ne v0, v3, :cond_b

    .line 1944908
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1944909
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v3, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->j:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1944910
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v4, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->k:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 1944911
    iget v4, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->h:I

    int-to-float v4, v4

    cmpg-float v4, v0, v4

    if-gez v4, :cond_7

    iget v4, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->h:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    int-to-float v4, v4

    cmpg-float v4, v3, v4

    if-gez v4, :cond_7

    .line 1944912
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    move v0, v1

    goto/16 :goto_0

    .line 1944913
    :cond_7
    :try_start_5
    iget v4, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->h:I

    int-to-float v4, v4

    cmpg-float v4, v0, v4

    if-gez v4, :cond_8

    iget v4, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->h:I

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-gtz v4, :cond_9

    :cond_8
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_a

    .line 1944914
    :cond_9
    sget-object v0, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1944915
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    move v0, v1

    goto/16 :goto_0

    .line 1944916
    :cond_a
    :try_start_6
    sget-object v0, LX/CtU;->ACCEPTING_MOVE_EVENTS:LX/CtU;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    .line 1944917
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b(Landroid/view/View;)F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    sub-float/2addr v0, v1

    invoke-static {p0, v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;F)V

    .line 1944918
    invoke-static {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->g(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1944919
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    move v0, v2

    goto/16 :goto_0

    .line 1944920
    :cond_b
    :try_start_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v2, :cond_c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v4, :cond_d

    .line 1944921
    :cond_c
    sget-object v0, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    .line 1944922
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->v:Z

    .line 1944923
    :cond_d
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result v0

    .line 1944924
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    throw v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    const v0, 0x4f4bb33a    # 3.41752064E9f

    invoke-static {v5, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 1944855
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->c:LX/CsO;

    invoke-virtual {v0}, LX/CsO;->a()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->f:LX/CqV;

    .line 1944856
    iget-boolean v1, v0, LX/CqV;->a:Z

    move v0, v1

    .line 1944857
    if-nez v0, :cond_1

    .line 1944858
    :cond_0
    const v0, 0x7ae29329

    invoke-static {v5, v5, v0, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1944859
    :goto_0
    return v3

    .line 1944860
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    if-nez v0, :cond_2

    .line 1944861
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    const v0, 0x237a7c45

    invoke-static {v0, v4}, LX/02F;->a(II)V

    goto :goto_0

    .line 1944862
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1944863
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_b

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    sget-object v1, LX/CtU;->ACCEPTING_MOVE_EVENTS:LX/CtU;

    if-ne v0, v1, :cond_b

    .line 1944864
    sget-object v0, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    .line 1944865
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->p:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 1944866
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->p:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v5

    .line 1944867
    const/high16 v0, 0x3e800000    # 0.25f

    mul-float v1, v5, v0

    .line 1944868
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->r:Z

    if-eqz v0, :cond_4

    .line 1944869
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getWidth()I

    move-result v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    .line 1944870
    :goto_1
    iget-boolean v6, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->r:Z

    if-eqz v6, :cond_7

    .line 1944871
    int-to-float v6, v0

    cmpg-float v1, v1, v6

    if-gtz v1, :cond_5

    move v1, v2

    .line 1944872
    :goto_2
    iget-object v6, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    invoke-direct {p0, v6}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b(Landroid/view/View;)F

    move-result v6

    int-to-float v0, v0

    cmpg-float v0, v6, v0

    if-gez v0, :cond_6

    move v0, v2

    .line 1944873
    :goto_3
    if-nez v1, :cond_3

    if-eqz v0, :cond_a

    .line 1944874
    :cond_3
    invoke-static {p0, v2, v5}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;ZF)V

    .line 1944875
    :goto_4
    const v0, -0x2b4d05c7

    invoke-static {v0, v4}, LX/02F;->a(II)V

    move v3, v2

    goto :goto_0

    .line 1944876
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_5
    move v1, v3

    .line 1944877
    goto :goto_2

    :cond_6
    move v0, v3

    .line 1944878
    goto :goto_3

    .line 1944879
    :cond_7
    int-to-float v6, v0

    cmpl-float v1, v1, v6

    if-ltz v1, :cond_8

    move v1, v2

    .line 1944880
    :goto_5
    iget-object v6, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    invoke-direct {p0, v6}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b(Landroid/view/View;)F

    move-result v6

    int-to-float v0, v0

    cmpl-float v0, v6, v0

    if-lez v0, :cond_9

    move v0, v2

    goto :goto_3

    :cond_8
    move v1, v3

    .line 1944881
    goto :goto_5

    :cond_9
    move v0, v3

    .line 1944882
    goto :goto_3

    .line 1944883
    :cond_a
    invoke-static {p0, v3, v5}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;ZF)V

    goto :goto_4

    .line 1944884
    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v5, :cond_c

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    sget-object v1, LX/CtU;->ACCEPTING_MOVE_EVENTS:LX/CtU;

    if-ne v0, v1, :cond_c

    .line 1944885
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->i:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b(Landroid/view/View;)F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    add-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    sub-float/2addr v0, v1

    invoke-static {p0, v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->a$redex0(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;F)V

    .line 1944886
    invoke-static {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->g(Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;)V

    .line 1944887
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->l:F

    goto :goto_4

    .line 1944888
    :cond_c
    const v0, -0x66ae6041

    invoke-static {v0, v4}, LX/02F;->a(II)V

    goto/16 :goto_0
.end method

.method public final requestDisallowInterceptTouchEvent(Z)V
    .locals 2

    .prologue
    .line 1944839
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->s:Z

    if-nez v0, :cond_1

    .line 1944840
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 1944841
    :cond_0
    :goto_0
    return-void

    .line 1944842
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->f:LX/CqV;

    .line 1944843
    iget-boolean v1, v0, LX/CqV;->a:Z

    move v0, v1

    .line 1944844
    if-nez v0, :cond_2

    .line 1944845
    sget-object v0, LX/CtU;->WAITING_FOR_DOWN:LX/CtU;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->m:LX/CtU;

    .line 1944846
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->v:Z

    .line 1944847
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1944848
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0
.end method

.method public setEnableIncomingAnimation(Z)V
    .locals 0

    .prologue
    .line 1944853
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->t:Z

    .line 1944854
    return-void
.end method

.method public setOutgoingAnimationEnabled(Z)V
    .locals 0

    .prologue
    .line 1944851
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    .line 1944852
    return-void
.end method

.method public setSwipeToDismissEnabled(Z)V
    .locals 0

    .prologue
    .line 1944849
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->w:Z

    .line 1944850
    return-void
.end method
