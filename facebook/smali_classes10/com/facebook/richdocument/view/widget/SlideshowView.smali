.class public Lcom/facebook/richdocument/view/widget/SlideshowView;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""

# interfaces
.implements LX/Ct1;


# instance fields
.field public i:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/CjL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/Ctm;

.field public l:I

.field public m:I

.field public n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1944640
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/SlideshowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1944641
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1944642
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/SlideshowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944643
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1944644
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944645
    const-class v0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1944646
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->p()V

    .line 1944647
    return-void
.end method

.method private static a(LX/Clo;)F
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1944648
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/Clo;->d()I

    move-result v0

    if-nez v0, :cond_1

    .line 1944649
    :cond_0
    const/4 v0, 0x0

    .line 1944650
    :goto_0
    return v0

    .line 1944651
    :cond_1
    invoke-virtual {p0}, LX/Clo;->d()I

    move-result v0

    new-array v3, v0, [F

    move v0, v1

    .line 1944652
    :goto_1
    invoke-virtual {p0}, LX/Clo;->d()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1944653
    invoke-virtual {p0, v0}, LX/Clo;->a(I)LX/Clr;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/richdocument/view/widget/SlideshowView;->a(LX/Clr;)F

    move-result v2

    aput v2, v3, v0

    .line 1944654
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1944655
    :cond_2
    invoke-virtual {p0}, LX/Clo;->d()I

    move-result v0

    new-array v4, v0, [I

    move v0, v1

    move v2, v1

    .line 1944656
    :goto_2
    invoke-virtual {p0}, LX/Clo;->d()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 1944657
    aget v5, v3, v1

    sget-wide v6, LX/CoL;->n:D

    double-to-float v6, v6

    invoke-static {v3, v5, v6}, Lcom/facebook/richdocument/view/widget/SlideshowView;->a([FFF)I

    move-result v5

    aput v5, v4, v1

    .line 1944658
    aget v5, v4, v1

    if-le v5, v2, :cond_3

    .line 1944659
    aget v2, v4, v1

    move v0, v1

    .line 1944660
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1944661
    :cond_4
    aget v0, v3, v0

    goto :goto_0
.end method

.method private static a(LX/Clr;)F
    .locals 2

    .prologue
    .line 1944662
    instance-of v0, p0, LX/Clw;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, LX/Clw;

    invoke-interface {v0}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1944663
    check-cast p0, LX/Clw;

    invoke-interface {p0}, LX/Clw;->a()LX/8Yr;

    move-result-object v0

    invoke-interface {v0}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v0

    .line 1944664
    invoke-interface {v0}, LX/1Fb;->c()I

    move-result v1

    int-to-float v1, v1

    invoke-interface {v0}, LX/1Fb;->a()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 1944665
    :goto_0
    return v0

    .line 1944666
    :cond_0
    instance-of v0, p0, LX/Cm4;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, LX/Cm4;

    invoke-interface {v0}, LX/Cm4;->r()LX/8Ys;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1944667
    check-cast p0, LX/Cm4;

    invoke-interface {p0}, LX/Cm4;->r()LX/8Ys;

    move-result-object v0

    .line 1944668
    invoke-interface {v0}, LX/8Ys;->D()I

    move-result v1

    int-to-float v1, v1

    invoke-interface {v0}, LX/8Ys;->m()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_0

    .line 1944669
    :cond_1
    instance-of v0, p0, LX/CmU;

    if-eqz v0, :cond_2

    .line 1944670
    check-cast p0, LX/CmU;

    .line 1944671
    iget-boolean v0, p0, LX/CmU;->c:Z

    if-eqz v0, :cond_3

    const v0, 0x3fa3d70a    # 1.28f

    :goto_1
    move v0, v0

    .line 1944672
    goto :goto_0

    .line 1944673
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const v0, 0x3f970a3d    # 1.18f

    goto :goto_1
.end method

.method private static a(Landroid/view/View;I)I
    .locals 3

    .prologue
    .line 1944674
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public static a([FFF)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1944675
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget v3, p0, v1

    .line 1944676
    sub-float/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, p2

    if-gtz v3, :cond_0

    .line 1944677
    add-int/lit8 v0, v0, 0x1

    .line 1944678
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1944679
    :cond_1
    return v0
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/SlideshowView;LX/Ckw;LX/CjL;)V
    .locals 0

    .prologue
    .line 1944680
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->i:LX/Ckw;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->j:LX/CjL;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/richdocument/view/widget/SlideshowView;

    invoke-static {v1}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v0

    check-cast v0, LX/Ckw;

    const-class v2, LX/CjL;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/CjL;

    invoke-static {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/SlideshowView;->a(Lcom/facebook/richdocument/view/widget/SlideshowView;LX/Ckw;LX/CjL;)V

    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1944636
    new-instance v0, LX/CtM;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v2, v2}, LX/CtM;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1944637
    new-instance v0, LX/Ctm;

    invoke-direct {v0, p0}, LX/Ctm;-><init>(LX/Ct1;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->k:LX/Ctm;

    .line 1944638
    new-instance v0, LX/CtL;

    invoke-direct {v0, p0}, LX/CtL;-><init>(Lcom/facebook/richdocument/view/widget/SlideshowView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemClickListener(LX/1OZ;)V

    .line 1944639
    return-void
.end method


# virtual methods
.method public b(Z)V
    .locals 4

    .prologue
    .line 1944681
    if-eqz p1, :cond_0

    .line 1944682
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->i:LX/Ckw;

    const-string v1, "swipe"

    iget v2, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->l:I

    add-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->n:I

    invoke-virtual {v0, v1, v2, v3}, LX/Ckw;->a(Ljava/lang/String;II)V

    .line 1944683
    :cond_0
    return-void
.end method

.method public final b(II)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1944610
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 1944611
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v1

    iput v1, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->l:I

    .line 1944612
    invoke-virtual {v0}, LX/1P1;->n()I

    move-result v1

    iput v1, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->m:I

    .line 1944613
    iget v1, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->l:I

    iget v4, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->m:I

    if-ne v1, v4, :cond_0

    .line 1944614
    :goto_0
    return v3

    .line 1944615
    :cond_0
    iget v1, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->l:I

    invoke-virtual {v0, v1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v1

    .line 1944616
    iget v4, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->m:I

    invoke-virtual {v0, v4}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1944617
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->getWidth()I

    move-result v4

    .line 1944618
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    sget v6, LX/CoL;->r:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_3

    .line 1944619
    if-lez p1, :cond_1

    .line 1944620
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v1, v4, v1

    div-int/lit8 v1, v1, 0x2

    .line 1944621
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int/2addr v0, v1

    .line 1944622
    invoke-virtual {p0, v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    move v0, v2

    .line 1944623
    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->b(Z)V

    move v3, v2

    .line 1944624
    goto :goto_0

    .line 1944625
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    sub-int v0, v4, v0

    div-int/lit8 v0, v0, 0x2

    .line 1944626
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v0, v1, v0

    .line 1944627
    invoke-virtual {p0, v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    :cond_2
    move v0, v3

    .line 1944628
    goto :goto_1

    .line 1944629
    :cond_3
    invoke-static {v1, v4}, Lcom/facebook/richdocument/view/widget/SlideshowView;->a(Landroid/view/View;I)I

    move-result v5

    .line 1944630
    invoke-static {v0, v4}, Lcom/facebook/richdocument/view/widget/SlideshowView;->a(Landroid/view/View;I)I

    move-result v6

    .line 1944631
    if-lt v5, v6, :cond_4

    move-object v0, v1

    .line 1944632
    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v1, v4, v1

    div-int/lit8 v1, v1, 0x2

    .line 1944633
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    sub-int/2addr v0, v1

    .line 1944634
    invoke-virtual {p0, v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    .line 1944635
    if-ge v5, v6, :cond_2

    move v0, v2

    goto :goto_1
.end method

.method public getMediaAspectRatio()F
    .locals 1

    .prologue
    .line 1944607
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->k:LX/Ctm;

    .line 1944608
    iget p0, v0, LX/Ctm;->b:F

    move v0, p0

    .line 1944609
    return v0
.end method

.method public getMediaFrame()LX/Ctg;
    .locals 1

    .prologue
    .line 1944606
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, LX/Ctg;

    return-object v0
.end method

.method public getSlideCount()I
    .locals 1

    .prologue
    .line 1944604
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 1944605
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 1944603
    return-object p0
.end method

.method public final jk_()Z
    .locals 1

    .prologue
    .line 1944602
    const/4 v0, 0x0

    return v0
.end method

.method public o()V
    .locals 0

    .prologue
    .line 1944601
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x34edcd46    # -9581242.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1944599
    invoke-super {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->onDetachedFromWindow()V

    .line 1944600
    const/16 v1, 0x2d

    const v2, -0x9deed1a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1944596
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->k:LX/Ctm;

    invoke-virtual {v0}, LX/Ctm;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 1944597
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->onMeasure(II)V

    .line 1944598
    return-void
.end method

.method public final setSlides$6708424b(LX/Clo;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1944590
    invoke-virtual {p1}, LX/Clo;->d()I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->n:I

    .line 1944591
    new-instance v0, LX/CoJ;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/SlideshowView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->j:LX/CjL;

    invoke-virtual {v2, v6}, LX/CjL;->a(LX/0Pq;)LX/CjK;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v4

    check-cast v4, LX/1P0;

    move-object v2, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, LX/CoJ;-><init>(Landroid/content/Context;LX/Clo;LX/CjK;LX/1P1;Landroid/support/v7/widget/RecyclerView;LX/0Pq;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1944592
    invoke-static {p1}, Lcom/facebook/richdocument/view/widget/SlideshowView;->a(LX/Clo;)F

    move-result v0

    .line 1944593
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/SlideshowView;->k:LX/Ctm;

    .line 1944594
    iput v0, v1, LX/Ctm;->b:F

    .line 1944595
    return-void
.end method
