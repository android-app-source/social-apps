.class public Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/Cjr;
.implements LX/Cmx;


# instance fields
.field private final a:Landroid/graphics/Paint;

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private e:LX/Cmo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1943277
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1943278
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->a:Landroid/graphics/Paint;

    .line 1943279
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->b:Landroid/graphics/Paint;

    .line 1943280
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->c:Landroid/graphics/Paint;

    .line 1943281
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->d:Landroid/graphics/Paint;

    .line 1943282
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1943271
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1943272
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->a:Landroid/graphics/Paint;

    .line 1943273
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->b:Landroid/graphics/Paint;

    .line 1943274
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->c:Landroid/graphics/Paint;

    .line 1943275
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->d:Landroid/graphics/Paint;

    .line 1943276
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1943283
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943284
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->a:Landroid/graphics/Paint;

    .line 1943285
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->b:Landroid/graphics/Paint;

    .line 1943286
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->c:Landroid/graphics/Paint;

    .line 1943287
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->d:Landroid/graphics/Paint;

    .line 1943288
    return-void
.end method


# virtual methods
.method public getExtraPaddingBottom()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1943265
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->getChildCount()I

    move-result v4

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v4, :cond_0

    .line 1943266
    invoke-virtual {p0, v3}, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1943267
    instance-of v0, v1, LX/Cjr;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, LX/Cjr;

    invoke-interface {v0}, LX/Cjr;->getExtraPaddingBottom()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 1943268
    check-cast v1, LX/Cjr;

    invoke-interface {v1}, LX/Cjr;->getExtraPaddingBottom()I

    move-result v0

    .line 1943269
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 1943270
    :cond_0
    return v2

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1943255
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->e:LX/Cmo;

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->a:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->b:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->c:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->d:Landroid/graphics/Paint;

    move-object v0, p1

    move-object v1, p0

    invoke-static/range {v0 .. v6}, LX/Clh;->a(Landroid/graphics/Canvas;Landroid/view/View;LX/Cmo;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 1943256
    return-void
.end method

.method public setBorders(LX/Cmo;)V
    .locals 2

    .prologue
    .line 1943257
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->setWillNotDraw(Z)V

    .line 1943258
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->e:LX/Cmo;

    .line 1943259
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->e:LX/Cmo;

    if-eqz v0, :cond_0

    .line 1943260
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->a:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->e:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->a:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1943261
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->e:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->b:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1943262
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->e:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->c:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1943263
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->d:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ListItemLinearLayout;->e:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->d:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1943264
    :cond_0
    return-void
.end method
