.class public Lcom/facebook/richdocument/view/widget/RichTextView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/Cjr;
.implements LX/Cmx;


# instance fields
.field public a:LX/CIg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CsH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CtG;

.field private final f:Landroid/graphics/Rect;

.field public g:I

.field public h:I

.field private i:I

.field private j:Landroid/graphics/drawable/Drawable;

.field public k:LX/Cjq;

.field private l:LX/CsG;

.field public m:Z

.field private n:LX/Cmo;

.field private final o:Landroid/graphics/Paint;

.field private final p:Landroid/graphics/Paint;

.field private final q:Landroid/graphics/Paint;

.field private final r:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1944433
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1944434
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1944435
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944436
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, -0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1944437
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944438
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    .line 1944439
    iput-boolean v3, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->m:Z

    .line 1944440
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->o:Landroid/graphics/Paint;

    .line 1944441
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->p:Landroid/graphics/Paint;

    .line 1944442
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->q:Landroid/graphics/Paint;

    .line 1944443
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->r:Landroid/graphics/Paint;

    .line 1944444
    const-class v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1944445
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->j:Landroid/graphics/drawable/Drawable;

    .line 1944446
    new-instance v0, LX/CtG;

    invoke-direct {v0, p0, p1, p2}, LX/CtG;-><init>(Lcom/facebook/richdocument/view/widget/RichTextView;Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    .line 1944447
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-static {v0, v2}, LX/8ba;->a(Landroid/view/View;I)V

    .line 1944448
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;)V

    .line 1944449
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0, v2, v2, v2, v2}, LX/CtG;->setPadding(IIII)V

    .line 1944450
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0, v2}, LX/CtG;->setGravity(I)V

    .line 1944451
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0}, LX/CtG;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1944452
    if-nez v0, :cond_0

    .line 1944453
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1944454
    :cond_0
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 1944455
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1944456
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v1, v0}, LX/CtG;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1944457
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0, v2}, LX/CtG;->setIncludeFontPadding(Z)V

    .line 1944458
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0, v2}, LX/CtG;->setVisibility(I)V

    .line 1944459
    invoke-virtual {p0, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setClipChildren(Z)V

    .line 1944460
    invoke-virtual {p0, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setClipToPadding(Z)V

    .line 1944461
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0}, LX/CtG;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setLinearText(Z)V

    .line 1944462
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0}, LX/CtG;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setSubpixelText(Z)V

    .line 1944463
    invoke-virtual {p0, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setWillNotDraw(Z)V

    .line 1944464
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/RichTextView;LX/CIg;LX/Cju;LX/CsH;LX/Chi;)V
    .locals 0

    .prologue
    .line 1944465
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->a:LX/CIg;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->b:LX/Cju;

    iput-object p3, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->c:LX/CsH;

    iput-object p4, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->d:LX/Chi;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-static {v3}, LX/CIg;->a(LX/0QB;)LX/CIg;

    move-result-object v0

    check-cast v0, LX/CIg;

    invoke-static {v3}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    const-class v2, LX/CsH;

    invoke-interface {v3, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/CsH;

    invoke-static {v3}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v3

    check-cast v3, LX/Chi;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->a(Lcom/facebook/richdocument/view/widget/RichTextView;LX/CIg;LX/Cju;LX/CsH;LX/Chi;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;II)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1944466
    instance-of v0, p1, Landroid/text/SpannableString;

    if-eqz v0, :cond_1

    .line 1944467
    check-cast p1, Landroid/text/SpannableString;

    .line 1944468
    const-class v0, Landroid/text/style/UnderlineSpan;

    invoke-virtual {p1, p2, p3, v0}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/UnderlineSpan;

    .line 1944469
    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 1944470
    :goto_0
    return v0

    .line 1944471
    :cond_0
    const-class v0, LX/Clm;

    invoke-virtual {p1, p2, p3, v0}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Clm;

    .line 1944472
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->d:LX/Chi;

    .line 1944473
    iget-object p1, v2, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v2, p1

    .line 1944474
    if-eqz v2, :cond_1

    .line 1944475
    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->d:LX/Chi;

    .line 1944476
    iget-object v2, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v2

    .line 1944477
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;->w()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLinkStyleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentLinkStyleModel;->c()Lcom/facebook/graphql/enums/GraphQLUnderlineStyle;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLUnderlineStyle;->SIMPLE_UNDERLINE:Lcom/facebook/graphql/enums/GraphQLUnderlineStyle;

    if-ne v0, v2, :cond_1

    move v0, v1

    .line 1944478
    goto :goto_0

    .line 1944479
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1944480
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->n:LX/Cmo;

    .line 1944481
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    const v3, 0x800033

    const/4 v0, -0x2

    const/4 v2, 0x0

    .line 1944482
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1944483
    if-eqz v1, :cond_1

    .line 1944484
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1944485
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1944486
    instance-of v0, v1, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 1944487
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1944488
    :cond_0
    :goto_0
    instance-of v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    .line 1944489
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1944490
    :cond_1
    return-void

    .line 1944491
    :cond_2
    instance-of v0, v1, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1944492
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput v3, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    goto :goto_0
.end method

.method private setExtraPaddingBottom(I)V
    .locals 2

    .prologue
    .line 1944493
    iget v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->i:I

    .line 1944494
    iput p1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->i:I

    .line 1944495
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->k:LX/Cjq;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->i:I

    if-eq v0, v1, :cond_0

    .line 1944496
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->k:LX/Cjq;

    invoke-interface {v0}, LX/Cjq;->a()V

    .line 1944497
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1944356
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1944357
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setWillNotDraw(Z)V

    .line 1944358
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->b()V

    .line 1944359
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->e()V

    .line 1944360
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0}, LX/CtG;->a()V

    .line 1944361
    return-void
.end method

.method public final addView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1944498
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1944499
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;)V

    .line 1944500
    :cond_0
    return-void
.end method

.method public final addView(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1944430
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1944431
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;I)V

    .line 1944432
    :cond_0
    return-void
.end method

.method public final addView(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 1944501
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1944502
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;II)V

    .line 1944503
    :cond_0
    return-void
.end method

.method public getBottomPixelsRemovedFromPadding()I
    .locals 1

    .prologue
    .line 1944355
    iget v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->h:I

    return v0
.end method

.method public getExtraPaddingBottom()I
    .locals 1

    .prologue
    .line 1944362
    iget v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->i:I

    return v0
.end method

.method public getInnerRichTextView()LX/CtG;
    .locals 1

    .prologue
    .line 1944363
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    return-object v0
.end method

.method public getTopPixelsRemovedFromPadding()I
    .locals 1

    .prologue
    .line 1944364
    iget v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->g:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1944365
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->n:LX/Cmo;

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->o:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->p:Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->q:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->r:Landroid/graphics/Paint;

    move-object v0, p1

    move-object v1, p0

    invoke-static/range {v0 .. v6}, LX/Clh;->a(Landroid/graphics/Canvas;Landroid/view/View;LX/Cmo;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    .line 1944366
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1944367
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1944368
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    iget v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->g:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/CtG;->setY(F)V

    .line 1944369
    return-void
.end method

.method public final onMeasure(II)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1944370
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1944371
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->m:Z

    if-nez v0, :cond_1

    .line 1944372
    :cond_0
    :goto_0
    return-void

    .line 1944373
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0}, LX/CtG;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1944374
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1944375
    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v3}, LX/CtG;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 1944376
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredHeight()I

    move-result v4

    .line 1944377
    iget-object v5, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v5}, LX/CtG;->getLineCount()I

    move-result v5

    if-ne v5, v6, :cond_3

    .line 1944378
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    invoke-virtual {v3, v2, v1, v5, v6}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1944379
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v2, v5

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getPaddingTop()I

    move-result v5

    sub-int/2addr v2, v5

    iput v2, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->g:I

    .line 1944380
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    .line 1944381
    if-eqz v0, :cond_2

    invoke-virtual {v3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 1944382
    :goto_1
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v1}, LX/CtG;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v1}, LX/CtG;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 1944383
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setMeasuredDimension(II)V

    .line 1944384
    sub-int v0, v4, v0

    iget v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->g:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->h:I

    .line 1944385
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setExtraPaddingBottom(I)V

    goto :goto_0

    .line 1944386
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    .line 1944387
    :cond_3
    iget-object v5, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v5}, LX/CtG;->getLineCount()I

    move-result v5

    if-le v5, v6, :cond_0

    .line 1944388
    iget-object v5, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v5}, LX/CtG;->getLayout()Landroid/text/Layout;

    move-result-object v5

    .line 1944389
    invoke-virtual {v5, v1}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v6

    .line 1944390
    iget-object v7, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v7}, LX/CtG;->getLineCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v5, v7}, Landroid/text/Layout;->getLineStart(I)I

    move-result v5

    .line 1944391
    iget-object v7, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    invoke-virtual {v3, v2, v1, v6, v7}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1944392
    iget-object v6, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->g:I

    .line 1944393
    iget-object v6, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v6, v7

    .line 1944394
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    iget-object v8, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    invoke-virtual {v3, v2, v5, v7, v8}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1944395
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-direct {p0, v0, v5, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->a(Ljava/lang/CharSequence;II)Z

    move-result v0

    .line 1944396
    if-eqz v0, :cond_4

    move v0, v1

    .line 1944397
    :goto_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_5

    .line 1944398
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v2}, LX/CtG;->getLineSpacingExtra()F

    move-result v2

    float-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1944399
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v2, v6

    sub-int v0, v2, v0

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    sub-int/2addr v0, v1

    .line 1944400
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setMeasuredDimension(II)V

    .line 1944401
    sub-int v0, v4, v0

    iget v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->g:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->h:I

    .line 1944402
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setExtraPaddingBottom(I)V

    goto/16 :goto_0

    .line 1944403
    :cond_4
    invoke-virtual {v3}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v2

    goto :goto_2

    .line 1944404
    :cond_5
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v1}, LX/CtG;->getLineSpacingExtra()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_3
.end method

.method public setBorders(LX/Cmo;)V
    .locals 2

    .prologue
    .line 1944405
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->setWillNotDraw(Z)V

    .line 1944406
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->n:LX/Cmo;

    .line 1944407
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->n:LX/Cmo;

    if-eqz v0, :cond_0

    .line 1944408
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->o:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->n:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->a:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1944409
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->p:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->n:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->b:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1944410
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->q:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->n:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->c:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1944411
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->r:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->n:LX/Cmo;

    iget-object v1, v1, LX/Cmo;->d:LX/Cmn;

    iget v1, v1, LX/Cmn;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1944412
    :cond_0
    return-void
.end method

.method public setEnableCopy(Z)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1944413
    if-eqz p1, :cond_1

    .line 1944414
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->l:LX/CsG;

    if-nez v0, :cond_0

    .line 1944415
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->c:LX/CsH;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    .line 1944416
    new-instance v2, LX/CsG;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v4

    check-cast v4, LX/Chi;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v5

    check-cast v5, LX/Ckw;

    invoke-static {v0}, LX/CoV;->a(LX/0QB;)LX/CoV;

    move-result-object v6

    check-cast v6, LX/CoV;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    move-object v3, v1

    invoke-direct/range {v2 .. v7}, LX/CsG;-><init>(Landroid/widget/TextView;LX/Chi;LX/Ckw;LX/CoV;LX/03V;)V

    .line 1944417
    move-object v0, v2

    .line 1944418
    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->l:LX/CsG;

    .line 1944419
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->l:LX/CsG;

    invoke-virtual {v0, v1}, LX/CtG;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1944420
    :cond_0
    :goto_0
    return-void

    .line 1944421
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0, v1}, LX/CtG;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1944422
    iput-object v1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->l:LX/CsG;

    goto :goto_0
.end method

.method public setLayoutDirection(I)V
    .locals 1

    .prologue
    .line 1944423
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setLayoutDirection(I)V

    .line 1944424
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    invoke-virtual {v0, p1}, LX/CtG;->setLayoutDirection(I)V

    .line 1944425
    return-void
.end method

.method public setOnExtraPaddingChangedListener(LX/Cjq;)V
    .locals 0

    .prologue
    .line 1944426
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->k:LX/Cjq;

    .line 1944427
    return-void
.end method

.method public setShouldCustomMeasure(Z)V
    .locals 0

    .prologue
    .line 1944428
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/RichTextView;->m:Z

    .line 1944429
    return-void
.end method
