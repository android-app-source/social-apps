.class public Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public b:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8bU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private f:Z

.field private g:I

.field public h:I

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1943470
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1943471
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1943430
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943431
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/16 v2, 0x10

    const/4 v3, 0x0

    .line 1943432
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943433
    const-class v0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1943434
    invoke-virtual {p0, v3}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setOrientation(I)V

    .line 1943435
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    .line 1943436
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1943437
    new-instance v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/facebook/richdocument/view/widget/RichTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1943438
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setupChild(Landroid/view/View;)V

    .line 1943439
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setupChild(Landroid/view/View;)V

    .line 1943440
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->b:LX/Cju;

    const v1, 0x7f0d016d

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    .line 1943441
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setPaddingRelative(IIII)V

    .line 1943442
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/ImageView;->setPaddingRelative(IIII)V

    .line 1943443
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setCropToPadding(Z)V

    .line 1943444
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1943445
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1943446
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1943447
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1943448
    if-eqz p2, :cond_4

    .line 1943449
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->ScalableImageWithTextView:[I

    invoke-virtual {v0, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1943450
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1943451
    if-eqz v1, :cond_0

    .line 1943452
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->b:LX/Cju;

    invoke-interface {v2, v1}, LX/Cju;->c(I)I

    move-result v1

    .line 1943453
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1943454
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 1943455
    :cond_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1943456
    if-eqz v1, :cond_1

    .line 1943457
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->b:LX/Cju;

    invoke-interface {v2, v1}, LX/Cju;->c(I)I

    move-result v1

    .line 1943458
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1943459
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 1943460
    :cond_1
    const/16 v1, 0x4

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->i:Z

    .line 1943461
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1943462
    if-eqz v1, :cond_2

    .line 1943463
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setImageResource(I)V

    .line 1943464
    :cond_2
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 1943465
    if-eqz v1, :cond_3

    .line 1943466
    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v2, v1, v3, v3, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setPaddingRelative(IIII)V

    .line 1943467
    :cond_3
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1943468
    :cond_4
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;LX/Cju;LX/0Uh;LX/8bU;)V
    .locals 0

    .prologue
    .line 1943469
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->b:LX/Cju;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->c:LX/0Uh;

    iput-object p3, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->d:LX/8bU;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;

    invoke-static {v2}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v0

    check-cast v0, LX/Cju;

    invoke-static {v2}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {v2}, LX/8bU;->a(LX/0QB;)LX/8bU;

    move-result-object v2

    check-cast v2, LX/8bU;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a(Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;LX/Cju;LX/0Uh;LX/8bU;)V

    return-void
.end method

.method private setupChild(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1943472
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1943473
    const/16 v1, 0x10

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1943474
    invoke-virtual {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1943475
    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1943476
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getRichTextView()Lcom/facebook/richdocument/view/widget/RichTextView;
    .locals 1

    .prologue
    .line 1943477
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    return-object v0
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 1943478
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1943479
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p0

    .line 1943480
    invoke-virtual {v0}, LX/CtG;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 1943416
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 1943417
    iget v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->g:I

    if-lez v0, :cond_0

    .line 1943418
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTop()I

    move-result v0

    iget v1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->g:I

    add-int/2addr v0, v1

    .line 1943419
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLeft()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBottom()I

    move-result v4

    iget-object v5, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    add-int/2addr v4, v0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 1943420
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1943421
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1943422
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->f:Z

    if-eqz v0, :cond_0

    .line 1943423
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getMeasuredHeight()I

    move-result v0

    .line 1943424
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    .line 1943425
    if-ge v0, v1, :cond_1

    .line 1943426
    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->g:I

    .line 1943427
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->g:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setMeasuredDimension(II)V

    .line 1943428
    :cond_0
    :goto_0
    return-void

    .line 1943429
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->g:I

    goto :goto_0
.end method

.method public setAlignImageToTextTop(Z)V
    .locals 1

    .prologue
    .line 1943412
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->f:Z

    if-eq v0, p1, :cond_0

    .line 1943413
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->g:I

    .line 1943414
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->f:Z

    .line 1943415
    return-void
.end method

.method public setDecodeImageOffUiThread(Z)V
    .locals 0

    .prologue
    .line 1943410
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->i:Z

    .line 1943411
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1943408
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1943409
    return-void
.end method

.method public setImageHeight(I)V
    .locals 1

    .prologue
    .line 1943404
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1943405
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->invalidate()V

    .line 1943406
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->requestLayout()V

    .line 1943407
    return-void
.end method

.method public setImageResource(I)V
    .locals 3

    .prologue
    .line 1943399
    iput p1, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->h:I

    .line 1943400
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->c:LX/0Uh;

    const/16 v1, 0x3ea

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1943401
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->d:LX/8bU;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/CtJ;

    invoke-direct {v2, p0}, LX/CtJ;-><init>(Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;)V

    invoke-virtual {v0, v1, p1, v2}, LX/8bU;->a(Landroid/content/Context;ILX/8bT;)V

    .line 1943402
    :goto_0
    return-void

    .line 1943403
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setImageScaleX(F)V
    .locals 1

    .prologue
    .line 1943397
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 1943398
    return-void
.end method

.method public setImageScaleY(F)V
    .locals 1

    .prologue
    .line 1943395
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 1943396
    return-void
.end method

.method public setImageWidth(I)V
    .locals 1

    .prologue
    .line 1943377
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1943378
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->invalidate()V

    .line 1943379
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->requestLayout()V

    .line 1943380
    return-void
.end method

.method public setText(I)V
    .locals 1

    .prologue
    .line 1943391
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1943392
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p0

    .line 1943393
    invoke-virtual {v0, p1}, LX/CtG;->setText(I)V

    .line 1943394
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1943387
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1943388
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p0

    .line 1943389
    invoke-virtual {v0, p1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1943390
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 1943383
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1943384
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p0

    .line 1943385
    invoke-virtual {v0, p1}, LX/CtG;->setTextColor(I)V

    .line 1943386
    return-void
.end method

.method public setTextVisibility(I)V
    .locals 1

    .prologue
    .line 1943381
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1943382
    return-void
.end method
