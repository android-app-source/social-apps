.class public Lcom/facebook/richdocument/view/widget/AudioAnnotationView;
.super Lcom/facebook/richdocument/view/widget/TextAnnotationView;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/richdocument/view/widget/TextAnnotationView",
        "<",
        "LX/ClV;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/7Cb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Landroid/net/Uri;

.field private h:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

.field public i:Z

.field public j:Z

.field private final k:LX/Ci3;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1942091
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1942092
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1942093
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942094
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1942095
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942096
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->j:Z

    .line 1942097
    new-instance v0, LX/Cs8;

    invoke-direct {v0, p0}, LX/Cs8;-><init>(Lcom/facebook/richdocument/view/widget/AudioAnnotationView;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->k:LX/Ci3;

    .line 1942098
    const-class v0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1942099
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->i:Z

    .line 1942100
    new-instance v0, LX/Cs9;

    invoke-direct {v0, p0}, LX/Cs9;-><init>(Lcom/facebook/richdocument/view/widget/AudioAnnotationView;)V

    .line 1942101
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1942102
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setTextOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1942103
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/ViewGroup;LX/ClV;)Lcom/facebook/richdocument/view/widget/AudioAnnotationView;
    .locals 3

    .prologue
    .line 1942104
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1942105
    const v1, 0x7f0311fd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;

    .line 1942106
    invoke-virtual {v0, p2}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->setAnnotation(LX/ClV;)V

    .line 1942107
    return-object v0
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/AudioAnnotationView;LX/Chv;LX/7Cb;)V
    .locals 0

    .prologue
    .line 1942108
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->a:LX/Chv;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->b:LX/7Cb;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;

    invoke-static {v1}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v0

    check-cast v0, LX/Chv;

    invoke-static {v1}, LX/7Cb;->b(LX/0QB;)LX/7Cb;

    move-result-object v1

    check-cast v1, LX/7Cb;

    invoke-static {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->a(Lcom/facebook/richdocument/view/widget/AudioAnnotationView;LX/Chv;LX/7Cb;)V

    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1942109
    const v0, 0x7f0200dd

    const v1, 0x7f0d0169

    const v2, 0x7f0d016a

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a(III)V

    .line 1942110
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Animatable;

    invoke-interface {v0}, Landroid/graphics/drawable/Animatable;->start()V

    .line 1942111
    return-void
.end method

.method public static i(Lcom/facebook/richdocument/view/widget/AudioAnnotationView;)V
    .locals 3

    .prologue
    .line 1942081
    const v0, 0x7f02167b

    const v1, 0x7f0d0169

    const v2, 0x7f0d016a

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a(III)V

    .line 1942082
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1942083
    invoke-super {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a()V

    .line 1942084
    const v0, 0x7f02167a

    const v1, 0x7f0d0169

    const v2, 0x7f0d016a

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->a(III)V

    .line 1942085
    const v0, 0x7f0b1278

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setDrawablePaddingResource(I)V

    .line 1942086
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0617

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1942087
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1942088
    invoke-static {v1, v0}, LX/CoV;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 1942089
    return-void

    .line 1942090
    :cond_0
    const/high16 v0, 0x1060000

    goto :goto_0
.end method

.method public final a(LX/CoS;)V
    .locals 2

    .prologue
    .line 1942076
    sget-object v0, LX/CsB;->b:[I

    invoke-virtual {p1}, LX/CoS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1942077
    :cond_0
    :goto_0
    return-void

    .line 1942078
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->h:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->ON_EXPAND:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    if-ne v0, v1, :cond_0

    .line 1942079
    :goto_1
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->f()V

    goto :goto_0

    .line 1942080
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->h:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->AMBIENT:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    if-ne v0, v1, :cond_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final b(LX/CoS;)V
    .locals 2

    .prologue
    .line 1942072
    sget-object v0, LX/CsB;->b:[I

    invoke-virtual {p1}, LX/CoS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1942073
    :cond_0
    :goto_0
    return-void

    .line 1942074
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->h:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->ON_EXPAND:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    if-ne v0, v1, :cond_0

    .line 1942075
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->g()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1942068
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->i:Z

    if-nez v0, :cond_0

    .line 1942069
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->f()V

    .line 1942070
    :goto_0
    return-void

    .line 1942071
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->g()V

    goto :goto_0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1942063
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->i:Z

    if-eqz v0, :cond_0

    .line 1942064
    :goto_0
    return-void

    .line 1942065
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->b:LX/7Cb;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->g:Landroid/net/Uri;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, LX/7Cb;->a(Landroid/net/Uri;I)V

    .line 1942066
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->h()V

    .line 1942067
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->i:Z

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1942058
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->i:Z

    if-nez v0, :cond_0

    .line 1942059
    :goto_0
    return-void

    .line 1942060
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->b:LX/7Cb;

    invoke-virtual {v0}, LX/7Cb;->a()V

    .line 1942061
    invoke-static {p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->i(Lcom/facebook/richdocument/view/widget/AudioAnnotationView;)V

    .line 1942062
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->i:Z

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x64b7ec3d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1942056
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->a:LX/Chv;

    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->k:LX/Ci3;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 1942057
    const/16 v1, 0x2d

    const v2, -0x23bb997e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x64eaa2f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1942053
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->a:LX/Chv;

    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->k:LX/Ci3;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1942054
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->g()V

    .line 1942055
    const/16 v1, 0x2d

    const v2, -0x409e0a43

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public bridge synthetic setAnnotation(LX/ClU;)V
    .locals 0

    .prologue
    .line 1942052
    check-cast p1, LX/ClV;

    invoke-virtual {p0, p1}, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->setAnnotation(LX/ClV;)V

    return-void
.end method

.method public setAnnotation(LX/ClV;)V
    .locals 2

    .prologue
    .line 1942041
    iget-object v0, p1, LX/ClV;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1942042
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1942043
    :goto_0
    return-void

    .line 1942044
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/richdocument/view/widget/TextAnnotationView;->setAnnotation(LX/ClU;)V

    .line 1942045
    iget-object v0, p1, LX/ClV;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1942046
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->g:Landroid/net/Uri;

    .line 1942047
    iget-object v0, p1, LX/ClV;->b:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-object v0, v0

    .line 1942048
    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->h:Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    .line 1942049
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/AudioAnnotationView;->b:LX/7Cb;

    new-instance v1, LX/CsA;

    invoke-direct {v1, p0}, LX/CsA;-><init>(Lcom/facebook/richdocument/view/widget/AudioAnnotationView;)V

    .line 1942050
    iput-object v1, v0, LX/7Cb;->g:LX/3RY;

    .line 1942051
    goto :goto_0
.end method
