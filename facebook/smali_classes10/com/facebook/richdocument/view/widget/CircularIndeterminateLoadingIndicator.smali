.class public Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;
.super LX/CsC;
.source ""

# interfaces
.implements LX/CnQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CsC;",
        "LX/CnQ",
        "<",
        "LX/ClX;",
        ">;"
    }
.end annotation


# instance fields
.field public e:LX/ClX;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1942304
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1942305
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1942302
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942303
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1942299
    invoke-direct {p0, p1, p2, p3}, LX/CsC;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1942300
    new-instance v0, LX/ClX;

    invoke-direct {v0}, LX/ClX;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->e:LX/ClX;

    .line 1942301
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/RectF;)Landroid/graphics/Path;
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1942286
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, LX/CsC;->a(Landroid/graphics/RectF;F)Landroid/graphics/RectF;

    move-result-object v0

    .line 1942287
    iget v1, p0, LX/CsC;->d:F

    iget v2, p0, LX/CsC;->a:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p0, LX/CsC;->c:F

    div-float/2addr v2, v5

    add-float/2addr v1, v2

    .line 1942288
    iget v2, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v1

    .line 1942289
    iget v3, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v1

    .line 1942290
    iget v4, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v4, v1

    .line 1942291
    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v1

    .line 1942292
    sub-float v1, v4, v2

    .line 1942293
    sub-float/2addr v0, v3

    .line 1942294
    div-float v4, v1, v5

    add-float/2addr v2, v4

    .line 1942295
    div-float/2addr v0, v5

    add-float/2addr v0, v3

    .line 1942296
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 1942297
    const v4, 0x40066666    # 2.1f

    div-float/2addr v1, v4

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v3, v2, v0, v1, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 1942298
    return-object v3
.end method

.method public final c()Landroid/view/View;
    .locals 0

    .prologue
    .line 1942285
    return-object p0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1942280
    const/4 v0, 0x0

    return v0
.end method

.method public bridge synthetic getAnnotation()LX/ClU;
    .locals 1

    .prologue
    .line 1942283
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->e:LX/ClX;

    move-object v0, v0

    .line 1942284
    return-object v0
.end method

.method public getAnnotation()LX/ClX;
    .locals 1

    .prologue
    .line 1942282
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/CircularIndeterminateLoadingIndicator;->e:LX/ClX;

    return-object v0
.end method

.method public setIsOverlay(Z)V
    .locals 0

    .prologue
    .line 1942281
    return-void
.end method
