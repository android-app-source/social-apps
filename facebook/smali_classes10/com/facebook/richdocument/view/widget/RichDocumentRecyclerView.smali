.class public Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""


# instance fields
.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11i;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:LX/Ct2;

.field private final o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field public t:LX/0Pq;

.field private u:LX/Ct6;

.field private v:LX/Ct7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1944028
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1944029
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1944026
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944027
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1943939
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1943940
    iput-boolean v2, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->p:Z

    .line 1943941
    iput-boolean v2, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->q:Z

    .line 1943942
    iput-boolean v2, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->r:Z

    .line 1943943
    iput-boolean v3, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->s:Z

    .line 1943944
    sget-object v0, LX/Ct7;->NOT_PROCESSING_ANY_REQUEST:LX/Ct7;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->v:LX/Ct7;

    .line 1943945
    const-class v0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1943946
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->l:LX/0Uh;

    const/16 v1, 0x3d3

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->o:Z

    .line 1943947
    new-instance v0, LX/Ct2;

    invoke-direct {v0, p0}, LX/Ct2;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->n:LX/Ct2;

    .line 1943948
    invoke-direct {p0, v3}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->b(Z)V

    .line 1943949
    new-instance v0, LX/Ct3;

    invoke-direct {v0, p0}, LX/Ct3;-><init>(Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 1943950
    return-void
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 1943999
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->p:Z

    if-nez v0, :cond_0

    .line 1944000
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 1944001
    if-eqz v0, :cond_0

    .line 1944002
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 1944003
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    if-lez v0, :cond_0

    .line 1944004
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->p:Z

    .line 1944005
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v2

    .line 1944006
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/CiS;

    sub-long v4, v2, p1

    invoke-direct {v1, v2, v3, v4, v5}, LX/CiS;-><init>(JJ)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1944007
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/CqV;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;",
            "LX/0Ot",
            "<",
            "LX/Chv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0So;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/11i;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/CqV;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1943998
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->i:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->j:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->k:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->l:LX/0Uh;

    iput-object p5, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->m:LX/CqV;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;

    const/16 v1, 0x31e0

    invoke-static {v5, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x2db

    invoke-static {v5, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x11e9

    invoke-static {v5, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v5}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v5}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object v5

    check-cast v5, LX/CqV;

    invoke-static/range {v0 .. v5}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->a(Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/CqV;)V

    return-void
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 1943994
    if-eqz p1, :cond_0

    .line 1943995
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->n:LX/Ct2;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 1943996
    :goto_0
    return-void

    .line 1943997
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->n:LX/Ct2;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    goto :goto_0
.end method

.method public static d(Landroid/support/v7/widget/RecyclerView;II)Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1943975
    if-nez p0, :cond_0

    move v0, v1

    .line 1943976
    :goto_0
    return v0

    .line 1943977
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 1943978
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v3

    .line 1943979
    invoke-virtual {v0}, LX/1OR;->v()I

    move-result v0

    add-int/2addr v0, v3

    add-int/lit8 v4, v0, -0x1

    .line 1943980
    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 1943981
    invoke-virtual {p0, v5}, Landroid/support/v7/widget/RecyclerView;->getLocationOnScreen([I)V

    .line 1943982
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 1943983
    :goto_1
    if-gt v3, v4, :cond_2

    .line 1943984
    invoke-virtual {p0, v3}, Landroid/support/v7/widget/RecyclerView;->c(I)LX/1a1;

    move-result-object v0

    check-cast v0, LX/Cs4;

    .line 1943985
    if-eqz v0, :cond_1

    .line 1943986
    iget-object v7, v0, LX/1a1;->a:Landroid/view/View;

    .line 1943987
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v8

    aget v9, v5, v1

    add-int/2addr v8, v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v9

    aget v10, v5, v2

    add-int/2addr v9, v10

    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v10

    aget v11, v5, v1

    add-int/2addr v10, v11

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v7

    aget v11, v5, v2

    add-int/2addr v7, v11

    invoke-virtual {v6, v8, v9, v10, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 1943988
    invoke-virtual {v6, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    .line 1943989
    iget-object v7, v0, LX/CnT;->d:LX/CnG;

    move-object v0, v7

    .line 1943990
    invoke-interface {v0, p1, p2}, LX/CnG;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 1943991
    goto :goto_0

    .line 1943992
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1943993
    goto :goto_0
.end method

.method private h(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1943960
    if-nez p1, :cond_0

    .line 1943961
    :goto_0
    return v1

    .line 1943962
    :cond_0
    instance-of v0, p1, LX/Cre;

    if-eqz v0, :cond_1

    .line 1943963
    check-cast p1, LX/Cre;

    sget-object v0, LX/Crd;->SCROLL_FINISHED:LX/Crd;

    invoke-interface {p1, v0}, LX/Cre;->a(LX/Crd;)Z

    move-result v1

    goto :goto_0

    .line 1943964
    :cond_1
    instance-of v0, p1, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 1943965
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v3

    move v2, v1

    .line 1943966
    :goto_1
    if-ge v2, v3, :cond_2

    move-object v0, p1

    .line 1943967
    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->h(Landroid/view/View;)Z

    move-result v0

    or-int/2addr v0, v1

    .line 1943968
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v1

    :goto_2
    move v1, v0

    .line 1943969
    goto :goto_0

    .line 1943970
    :cond_3
    instance-of v0, p1, Lcom/facebook/widget/CustomLinearLayout;

    if-eqz v0, :cond_4

    move-object v0, p1

    .line 1943971
    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->getChildCount()I

    move-result v3

    move v2, v1

    .line 1943972
    :goto_3
    if-ge v2, v3, :cond_4

    move-object v0, p1

    .line 1943973
    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->h(Landroid/view/View;)Z

    move-result v0

    or-int/2addr v0, v1

    .line 1943974
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public static o(Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;)Z
    .locals 5

    .prologue
    .line 1943951
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    .line 1943952
    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v1

    .line 1943953
    invoke-virtual {v0}, LX/1OR;->v()I

    move-result v2

    add-int v3, v1, v2

    .line 1943954
    const/4 v2, 0x0

    .line 1943955
    :goto_0
    if-gt v1, v3, :cond_0

    .line 1943956
    invoke-virtual {v0, v1}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v4

    .line 1943957
    invoke-direct {p0, v4}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->h(Landroid/view/View;)Z

    move-result v4

    or-int/2addr v2, v4

    .line 1943958
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1943959
    :cond_0
    return v2
.end method

.method private p()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1944008
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->u:LX/Ct6;

    if-eqz v0, :cond_0

    .line 1944009
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 1944010
    if-eqz v0, :cond_0

    .line 1944011
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 1944012
    instance-of v0, v0, LX/CoJ;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    instance-of v0, v0, LX/1OS;

    if-nez v0, :cond_1

    .line 1944013
    :cond_0
    :goto_0
    return-void

    .line 1944014
    :cond_1
    sget-object v0, LX/Ct4;->a:[I

    iget-object v2, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->v:LX/Ct7;

    invoke-virtual {v2}, LX/Ct7;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 1944015
    :goto_1
    if-eqz v0, :cond_0

    .line 1944016
    sget-object v0, LX/Ct7;->NOT_PROCESSING_ANY_REQUEST:LX/Ct7;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->v:LX/Ct7;

    .line 1944017
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->u:LX/Ct6;

    iget-object v0, v0, LX/Ct6;->b:LX/Ct5;

    invoke-interface {v0}, LX/Ct5;->a()V

    .line 1944018
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->u:LX/Ct6;

    goto :goto_0

    .line 1944019
    :pswitch_0
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->s:Z

    if-eqz v0, :cond_2

    .line 1944020
    invoke-virtual {p0, v1}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1944021
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v2, v0, 0x2

    .line 1944022
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1OS;

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->u:LX/Ct6;

    iget v3, v3, LX/Ct6;->a:I

    invoke-interface {v0, v3, v2}, LX/1OS;->d(II)V

    .line 1944023
    sget-object v0, LX/Ct7;->WAITING_FOR_VIEW_CENTERING:LX/Ct7;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->v:LX/Ct7;

    move v0, v1

    .line 1944024
    goto :goto_1

    .line 1944025
    :cond_2
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/Ct6;)V
    .locals 2

    .prologue
    .line 1943871
    if-eqz p1, :cond_0

    iget v0, p1, LX/Ct6;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1943872
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->u:LX/Ct6;

    .line 1943873
    sget-object v0, LX/Ct7;->WAITING_FOR_INITIAL_TOP_PLACEMENT:LX/Ct7;

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->v:LX/Ct7;

    .line 1943874
    iget v0, p1, LX/Ct6;->a:I

    invoke-super {p0, v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->g_(I)V

    .line 1943875
    :cond_0
    return-void
.end method

.method public final b(II)Z
    .locals 3

    .prologue
    .line 1943876
    invoke-static {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->o(Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;)Z

    move-result v0

    .line 1943877
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->n:LX/Ct2;

    if-eqz v1, :cond_0

    .line 1943878
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->n:LX/Ct2;

    int-to-float v2, p2

    .line 1943879
    iput v2, v1, LX/Ct2;->k:F

    .line 1943880
    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->n:LX/Ct2;

    invoke-virtual {v1}, LX/Ct2;->a()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1943881
    :cond_0
    if-eqz v0, :cond_1

    .line 1943882
    const/4 v0, 0x0

    .line 1943883
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->b(II)Z

    move-result v0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1943884
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 1943885
    invoke-super {p0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->draw(Landroid/graphics/Canvas;)V

    .line 1943886
    invoke-direct {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->a(J)V

    .line 1943887
    return-void
.end method

.method public final g_(I)V
    .locals 2

    .prologue
    .line 1943888
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "RichDocumentRecyclerView doesn\'t support this method. Use submitScrollToPositionRequest(ScrollToPositionRequest request) instead."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1943889
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->m:LX/CqV;

    .line 1943890
    iget-boolean v2, v0, LX/CqV;->b:Z

    move v2, v2

    .line 1943891
    if-eqz v2, :cond_0

    .line 1943892
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0, v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(FF)Landroid/view/View;

    move-result-object v0

    .line 1943893
    :goto_0
    if-eqz v0, :cond_1

    instance-of v3, v0, LX/1OV;

    if-eqz v3, :cond_1

    .line 1943894
    check-cast v0, LX/1OV;

    invoke-interface {v0, p1}, LX/1OV;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1943895
    if-eqz v0, :cond_1

    move v0, v1

    .line 1943896
    :goto_1
    return v0

    .line 1943897
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->m:LX/CqV;

    .line 1943898
    iget-object v3, v0, LX/CqV;->d:Landroid/view/View;

    move-object v0, v3

    .line 1943899
    goto :goto_0

    .line 1943900
    :cond_1
    if-eqz v2, :cond_2

    invoke-super {p0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1943901
    const/4 v2, 0x0

    .line 1943902
    const/4 v0, 0x0

    .line 1943903
    iget-boolean v3, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->r:Z

    if-nez v3, :cond_0

    .line 1943904
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v3, v3

    .line 1943905
    if-eqz v3, :cond_0

    .line 1943906
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v3, v3

    .line 1943907
    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->t:LX/0Pq;

    if-eqz v3, :cond_0

    .line 1943908
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11i;

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->t:LX/0Pq;

    invoke-interface {v0, v3}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v3

    .line 1943909
    if-eqz v3, :cond_2

    .line 1943910
    const-string v0, "rich_document_first_layout"

    const v2, -0x28c4c52f

    invoke-static {v3, v0, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    move v0, v1

    .line 1943911
    :goto_0
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->r:Z

    move v2, v0

    move-object v0, v3

    .line 1943912
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->onLayout(ZIIII)V

    .line 1943913
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1943914
    const-string v1, "rich_document_first_layout"

    const v2, -0x2746c3d7

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1943915
    :cond_1
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->p()V

    .line 1943916
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1943917
    const/4 v2, 0x0

    .line 1943918
    const/4 v0, 0x0

    .line 1943919
    iget-boolean v3, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->q:Z

    if-nez v3, :cond_0

    .line 1943920
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v3, v3

    .line 1943921
    if-eqz v3, :cond_0

    .line 1943922
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v3, v3

    .line 1943923
    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->t:LX/0Pq;

    if-eqz v3, :cond_0

    .line 1943924
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11i;

    iget-object v3, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->t:LX/0Pq;

    invoke-interface {v0, v3}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v3

    .line 1943925
    if-eqz v3, :cond_2

    .line 1943926
    const-string v0, "rich_document_first_measure"

    const v2, -0x5672edc2

    invoke-static {v3, v0, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    move v0, v1

    .line 1943927
    :goto_0
    iput-boolean v1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->q:Z

    move v2, v0

    move-object v0, v3

    .line 1943928
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->onMeasure(II)V

    .line 1943929
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1943930
    const-string v1, "rich_document_first_measure"

    const v2, -0x518bfd4

    invoke-static {v0, v1, v2}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1943931
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1943932
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->o:Z

    if-eqz v0, :cond_0

    instance-of v0, p2, Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    .line 1943933
    :goto_0
    return-void

    .line 1943934
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    goto :goto_0
.end method

.method public setCenterScrollToPositionRequests(Z)V
    .locals 0

    .prologue
    .line 1943935
    iput-boolean p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->s:Z

    .line 1943936
    return-void
.end method

.method public setSequenceDefinition(LX/0Pq;)V
    .locals 0

    .prologue
    .line 1943937
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->t:LX/0Pq;

    .line 1943938
    return-void
.end method
