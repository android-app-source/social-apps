.class public Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/CkJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CoV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "LX/CnQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1941826
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1941827
    new-instance v0, Ljava/util/TreeSet;

    sget-object v1, LX/Cs6;->a:LX/Cs6;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->c:Ljava/util/TreeSet;

    .line 1941828
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a()V

    .line 1941829
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1941822
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1941823
    new-instance v0, Ljava/util/TreeSet;

    sget-object v1, LX/Cs6;->a:LX/Cs6;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->c:Ljava/util/TreeSet;

    .line 1941824
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a()V

    .line 1941825
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1941818
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1941819
    new-instance v0, Ljava/util/TreeSet;

    sget-object v1, LX/Cs6;->a:LX/Cs6;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->c:Ljava/util/TreeSet;

    .line 1941820
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a()V

    .line 1941821
    return-void
.end method

.method private a(LX/CnQ;LX/CnQ;)I
    .locals 1

    .prologue
    .line 1941817
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a:LX/CkJ;

    invoke-virtual {v0, p1, p2}, LX/CkJ;->a(LX/CnQ;LX/CnQ;)I

    move-result v0

    return v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1941814
    const-class v0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1941815
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->setOrientation(I)V

    .line 1941816
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;LX/CkJ;LX/CoV;)V
    .locals 0

    .prologue
    .line 1941813
    iput-object p1, p0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a:LX/CkJ;

    iput-object p2, p0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->b:LX/CoV;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;

    invoke-static {v1}, LX/CkJ;->a(LX/0QB;)LX/CkJ;

    move-result-object v0

    check-cast v0, LX/CkJ;

    invoke-static {v1}, LX/CoV;->a(LX/0QB;)LX/CoV;

    move-result-object v1

    check-cast v1, LX/CoV;

    invoke-static {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a(Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;LX/CkJ;LX/CoV;)V

    return-void
.end method

.method private static b()Landroid/widget/LinearLayout$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 1941812
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/CnQ;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1941792
    if-eqz p1, :cond_0

    .line 1941793
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->getBottommostAnnotation()LX/CnQ;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->a(LX/CnQ;LX/CnQ;)I

    move-result v2

    .line 1941794
    iget-object v0, p0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->c:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 1941795
    invoke-interface {p1}, LX/CnQ;->c()Landroid/view/View;

    move-result-object v3

    .line 1941796
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    move-object v0, v1

    .line 1941797
    :goto_0
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1941798
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1941799
    invoke-interface {p1}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v1

    .line 1941800
    iget-object v2, v1, LX/ClU;->d:LX/ClQ;

    move-object v1, v2

    .line 1941801
    sget-object v2, LX/Cs5;->a:[I

    invoke-virtual {v1}, LX/ClQ;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 1941802
    :goto_1
    invoke-virtual {p0, v3}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->addView(Landroid/view/View;)V

    .line 1941803
    invoke-interface {p1}, LX/CnQ;->getAnnotation()LX/ClU;

    move-result-object v0

    .line 1941804
    iget-object v1, v0, LX/ClU;->e:LX/ClR;

    move-object v0, v1

    .line 1941805
    sget-object v1, LX/ClR;->CENTER:LX/ClR;

    if-ne v0, v1, :cond_2

    .line 1941806
    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 1941807
    :cond_0
    :goto_2
    return-void

    .line 1941808
    :cond_1
    invoke-static {}, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->b()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    goto :goto_0

    .line 1941809
    :pswitch_0
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto :goto_1

    .line 1941810
    :pswitch_1
    const/4 v1, 0x5

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto :goto_1

    .line 1941811
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getBottommostAnnotation()LX/CnQ;
    .locals 2

    .prologue
    .line 1941788
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/richdocument/view/widget/AnnotationSlotLinearLayout;->c:Ljava/util/TreeSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1941789
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1941790
    const/4 v0, 0x0

    .line 1941791
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CnQ;

    goto :goto_0
.end method
