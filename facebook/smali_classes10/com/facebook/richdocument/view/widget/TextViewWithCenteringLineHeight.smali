.class public Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field private final a:Z

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1944711
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 1944712
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->b:I

    .line 1944713
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a()V

    .line 1944714
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a:Z

    .line 1944715
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1944716
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1944717
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->b:I

    .line 1944718
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a()V

    .line 1944719
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a:Z

    .line 1944720
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1944721
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1944722
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->b:I

    .line 1944723
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a()V

    .line 1944724
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a:Z

    .line 1944725
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1944726
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->b:I

    .line 1944727
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1944728
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getLineSpacingExtra()F

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getLineSpacingMultiplier()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a(FF)V

    .line 1944729
    :goto_0
    return-void

    .line 1944730
    :cond_0
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->d()V

    goto :goto_0
.end method

.method private a(F)V
    .locals 4

    .prologue
    .line 1944703
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 1944704
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getPaddingStart()I

    move-result v0

    iget v1, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->b:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getPaddingEnd()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getPaddingBottom()I

    move-result v3

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setPaddingRelative(IIII)V

    .line 1944705
    :goto_0
    return-void

    .line 1944706
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->b:I

    int-to-float v1, v1

    add-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getPaddingBottom()I

    move-result v3

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method private a(FF)V
    .locals 2

    .prologue
    .line 1944707
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    .line 1944708
    iget v1, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float v0, v1, v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, p2, v1

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    .line 1944709
    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a(F)V

    .line 1944710
    return-void
.end method

.method private c()I
    .locals 2

    .prologue
    .line 1944702
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getLineHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->getTextSize()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1944700
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->c()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a(F)V

    .line 1944701
    return-void
.end method


# virtual methods
.method public final setLineSpacing(FF)V
    .locals 0

    .prologue
    .line 1944697
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;->setLineSpacing(FF)V

    .line 1944698
    invoke-direct {p0, p1, p2}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a(FF)V

    .line 1944699
    return-void
.end method

.method public final setPadding(IIII)V
    .locals 2

    .prologue
    .line 1944691
    const/4 v0, 0x0

    .line 1944692
    iget-boolean v1, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a:Z

    if-nez v1, :cond_0

    .line 1944693
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->c()I

    move-result v0

    .line 1944694
    :cond_0
    iput p2, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->b:I

    .line 1944695
    add-int/2addr v0, p2

    invoke-super {p0, p1, v0, p3, p4}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 1944696
    return-void
.end method

.method public final setPaddingRelative(IIII)V
    .locals 2

    .prologue
    .line 1944685
    const/4 v0, 0x0

    .line 1944686
    iget-boolean v1, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->a:Z

    if-nez v1, :cond_0

    .line 1944687
    invoke-direct {p0}, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->c()I

    move-result v0

    .line 1944688
    :cond_0
    iput p2, p0, Lcom/facebook/richdocument/view/widget/TextViewWithCenteringLineHeight;->b:I

    .line 1944689
    add-int/2addr v0, p2

    invoke-super {p0, p1, v0, p3, p4}, Lcom/facebook/widget/text/BetterTextView;->setPaddingRelative(IIII)V

    .line 1944690
    return-void
.end method
