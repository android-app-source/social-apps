.class public Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cnf;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/richdocument/view/block/InlineEmailCtaBlockView;"
    }
.end annotation


# static fields
.field public static final B:Ljava/lang/String;

.field public static final C:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final A:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public D:Ljava/lang/String;

.field public E:Ljava/lang/String;

.field public F:Ljava/lang/String;

.field public G:Z

.field private final H:I

.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Cj9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final i:Landroid/widget/LinearLayout;

.field public final j:Landroid/widget/LinearLayout;

.field private final k:Landroid/widget/LinearLayout;

.field private final l:Landroid/widget/LinearLayout;

.field public final m:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final n:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final o:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final p:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public final q:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private final r:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private final s:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final v:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final w:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final x:Landroid/widget/LinearLayout;

.field public final y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1936515
    const-class v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->B:Ljava/lang/String;

    .line 1936516
    const-class v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->C:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 1936517
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1936518
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;

    invoke-static {v0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v3

    check-cast v3, LX/Ck0;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v4

    check-cast v4, LX/0WJ;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v8

    check-cast v8, LX/Chv;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v11

    check-cast v11, LX/Chi;

    invoke-static {v0}, LX/Cj9;->a(LX/0QB;)LX/Cj9;

    move-result-object v12

    check-cast v12, LX/Cj9;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v0

    check-cast v0, LX/Cju;

    iput-object v3, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iput-object v4, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->b:LX/0WJ;

    iput-object v6, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v7, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->d:LX/03V;

    iput-object v8, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->e:LX/Chv;

    iput-object v11, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->f:LX/Chi;

    iput-object v12, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->g:LX/Cj9;

    iput-object v0, v2, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->h:LX/Cju;

    .line 1936519
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1936520
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->b:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 1936521
    iget-object v1, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1936522
    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->D:Ljava/lang/String;

    .line 1936523
    :cond_0
    const v0, 0x7f0d1686

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->i:Landroid/widget/LinearLayout;

    .line 1936524
    const v0, 0x7f0d1693

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->j:Landroid/widget/LinearLayout;

    .line 1936525
    const v0, 0x7f0d1687

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->k:Landroid/widget/LinearLayout;

    .line 1936526
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    invoke-virtual {v0, p1}, LX/Ck0;->a(Landroid/view/View;)V

    .line 1936527
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->k:Landroid/widget/LinearLayout;

    const v2, 0x7f0d011c

    const v3, 0x7f0d011c

    const v4, 0x7f0d011c

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936528
    const v0, 0x7f0d168b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->l:Landroid/widget/LinearLayout;

    .line 1936529
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->l:Landroid/widget/LinearLayout;

    const v4, 0x7f0d011c

    move v6, v5

    move v7, v5

    invoke-virtual/range {v2 .. v7}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936530
    const v0, 0x7f0d1689

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936531
    const v0, 0x7f0d168a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->u:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936532
    const v0, 0x7f0d168c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936533
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v8, 0x7f0d011b

    move v6, v5

    move v7, v5

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936534
    const v0, 0x7f0d168d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936535
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v8, 0x7f0d011b

    move v6, v5

    move v7, v5

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936536
    const v0, 0x7f0d168e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936537
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v8, 0x7f0d011c

    move v6, v5

    move v7, v5

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936538
    const v0, 0x7f0d168f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->p:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1936539
    const v0, 0x7f0d1690

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936540
    const v0, 0x7f0d1691

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936541
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v4, 0x7f0d011c

    move v6, v5

    move v7, v5

    invoke-virtual/range {v2 .. v7}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936542
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v7, 0x7f0d011c

    move v6, v5

    move v8, v5

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936543
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04005e

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1936544
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f04005f

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 1936545
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    const v3, 0x7f040060

    invoke-static {v0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 1936546
    const v0, 0x7f0d1692

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936547
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    new-instance v4, LX/Coy;

    invoke-direct {v4, p0, v2, v1, v3}, LX/Coy;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    invoke-virtual {v0, v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1936548
    const v0, 0x7f0d1694

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1936549
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    const v2, 0x7f0d011c

    const v3, 0x7f0d011c

    const v4, 0x7f0d011c

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936550
    const v0, 0x7f0d1695    # 1.875384E38f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936551
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v8, 0x7f0d011b

    move v6, v5

    move v7, v5

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936552
    const v0, 0x7f0d1696

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->w:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936553
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->w:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v8, 0x7f0d011c

    move v6, v5

    move v7, v5

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936554
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->v:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936555
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936556
    invoke-virtual {v0, v9}, LX/CtG;->setGravity(I)V

    .line 1936557
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->w:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936558
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936559
    invoke-virtual {v0, v9}, LX/CtG;->setGravity(I)V

    .line 1936560
    const v0, 0x7f0d1697

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->x:Landroid/widget/LinearLayout;

    .line 1936561
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->a:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->x:Landroid/widget/LinearLayout;

    const v8, 0x7f0d011c

    move v6, v5

    move v7, v5

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936562
    const v0, 0x7f0d1698

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936563
    const v0, 0x7f0d169a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->z:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936564
    const v0, 0x7f0d169c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->A:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936565
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->A:Lcom/facebook/richdocument/view/widget/RichTextView;

    new-instance v1, LX/Coz;

    invoke-direct {v1, p0}, LX/Coz;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1936566
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->h:LX/Cju;

    const v1, 0x7f0d011e

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->H:I

    .line 1936567
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->A:Lcom/facebook/richdocument/view/widget/RichTextView;

    iget v1, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->H:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v10, v2}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1936568
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    iget v1, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->H:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v10, v2}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1936569
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1936570
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->G:Z

    if-eqz v0, :cond_0

    .line 1936571
    :goto_0
    return-void

    .line 1936572
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->g:LX/Cj9;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->D:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->f:LX/Chi;

    .line 1936573
    iget-object v3, v2, LX/Chi;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1936574
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/Cj9;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1936575
    new-instance v1, LX/Cp4;

    invoke-direct {v1, p0}, LX/Cp4;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaBlockViewImpl;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
