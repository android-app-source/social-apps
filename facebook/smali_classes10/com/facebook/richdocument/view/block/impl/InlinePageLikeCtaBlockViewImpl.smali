.class public Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cnj;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/richdocument/view/block/InlinePageLikeCtaBlockView;"
    }
.end annotation


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final d:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final e:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final f:Lcom/facebook/fbui/glyph/GlyphView;

.field public final g:Lcom/facebook/fbui/facepile/FacepileView;

.field private final h:Landroid/widget/LinearLayout;

.field private final i:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1936768
    const-class v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1936757
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1936758
    const v0, 0x7f0d169f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936759
    const v0, 0x7f0d16a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936760
    const v0, 0x7f0d16a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936761
    const v0, 0x7f0d16a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936762
    const v0, 0x7f0d16a5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1936763
    const v0, 0x7f0d16a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->g:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1936764
    const v0, 0x7f0d16a3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->h:Landroid/widget/LinearLayout;

    .line 1936765
    const v0, 0x7f0d169e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->i:Landroid/view/View;

    .line 1936766
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;

    const/16 v0, 0x3231

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->a:LX/0Ot;

    .line 1936767
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;II)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentSubscriptionCTAPublisher$FriendsWhoLike$Edges;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1936707
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1936708
    if-nez v1, :cond_1

    .line 1936709
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936710
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936711
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081c7c

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1936712
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936713
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936714
    invoke-virtual {v0, v8}, LX/CtG;->setGravity(I)V

    .line 1936715
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936716
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936717
    invoke-virtual {v0, v8}, LX/CtG;->setGravity(I)V

    .line 1936718
    return-void

    .line 1936719
    :cond_1
    if-ne v1, v8, :cond_2

    .line 1936720
    invoke-virtual {p1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1936721
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936722
    iget-object v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v2

    .line 1936723
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081c7d

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v9

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1936724
    :cond_2
    if-ne v1, v10, :cond_3

    .line 1936725
    invoke-virtual {p1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1936726
    invoke-virtual {p1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1936727
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936728
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1936729
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081c7e

    new-array v5, v10, [Ljava/lang/Object;

    aput-object v1, v5, v9

    aput-object v0, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1936730
    :cond_3
    if-le v1, v10, :cond_0

    .line 1936731
    invoke-virtual {p1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1936732
    invoke-virtual {p1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1936733
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936734
    iget-object v4, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v4

    .line 1936735
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00d4

    add-int/lit8 v6, v1, -0x2

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v9

    aput-object v0, v7, v8

    add-int/lit8 v0, v1, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v10

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const v2, -0x6e685d

    const v1, -0xa76f01

    .line 1936736
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bL;

    invoke-virtual {v0}, LX/8bL;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Clz;->valueOf(Ljava/lang/String;)LX/Clz;

    move-result-object v0

    .line 1936737
    sget-object v5, LX/CpD;->a:[I

    invoke-virtual {v0}, LX/Clz;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    .line 1936738
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936739
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v1

    .line 1936740
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f081c7b

    :goto_1
    invoke-virtual {v1, v0}, LX/CtG;->setText(I)V

    .line 1936741
    return-void

    .line 1936742
    :pswitch_0
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1936743
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936744
    iget-object v3, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v3

    .line 1936745
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_3
    invoke-virtual {v0, v1}, LX/CtG;->setTextColor(I)V

    goto :goto_0

    :cond_0
    move v0, v2

    .line 1936746
    goto :goto_2

    :cond_1
    move v1, v2

    .line 1936747
    goto :goto_3

    .line 1936748
    :pswitch_1
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {v2, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1936749
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936750
    iget-object v2, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v2

    .line 1936751
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    move v3, v1

    :cond_2
    invoke-virtual {v0, v3}, LX/CtG;->setTextColor(I)V

    .line 1936752
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    move v1, v4

    :cond_3
    invoke-static {v0, v1}, LX/8ba;->a(Landroid/view/View;I)V

    .line 1936753
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaBlockViewImpl;->i:Landroid/view/View;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_5
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v3

    .line 1936754
    goto :goto_4

    .line 1936755
    :cond_5
    const/4 v4, 0x4

    goto :goto_5

    .line 1936756
    :cond_6
    const v0, 0x7f081c7a

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
