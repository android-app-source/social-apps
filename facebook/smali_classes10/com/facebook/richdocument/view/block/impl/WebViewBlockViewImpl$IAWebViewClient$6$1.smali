.class public final Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$6$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;

.field public final synthetic b:LX/Cq2;


# direct methods
.method public constructor <init>(LX/Cq2;Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;)V
    .locals 0

    .prologue
    .line 1938911
    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$6$1;->b:LX/Cq2;

    iput-object p2, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$6$1;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1938912
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$6$1;->b:LX/Cq2;

    iget-object v0, v0, LX/Cq2;->a:LX/Cq4;

    iget-object v0, v0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->l()V

    .line 1938913
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$6$1;->b:LX/Cq2;

    iget-object v0, v0, LX/Cq2;->a:LX/Cq4;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$6$1;->a:Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;

    .line 1938914
    if-nez v1, :cond_0

    .line 1938915
    iget-object v2, v0, LX/Cq4;->b:LX/CqA;

    iget-object v2, v2, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v2}, LX/CpO;->r()V

    .line 1938916
    :goto_0
    return-void

    .line 1938917
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->t()Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;

    move-result-object v2

    invoke-static {v2}, LX/8bO;->a(Lcom/facebook/graphql/enums/GraphQLDocumentNativeAdType;)LX/8bN;

    move-result-object v2

    .line 1938918
    iget-object v3, v0, LX/Cq4;->b:LX/CqA;

    iget-object v3, v3, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v3, v2}, LX/CpO;->a(LX/8bN;)V

    .line 1938919
    iget-object v3, v0, LX/Cq4;->b:LX/CqA;

    iget-object v3, v3, LX/CqA;->ad:LX/CpO;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1938920
    iget-object v3, v0, LX/Cq4;->b:LX/CqA;

    iget-object v3, v3, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v3}, LX/CpO;->o()V

    .line 1938921
    iget-object v3, v0, LX/Cq4;->b:LX/CqA;

    iget-object v3, v3, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->s()Ljava/lang/String;

    move-result-object v4

    .line 1938922
    iput-object v4, v3, LX/CpO;->v:Ljava/lang/String;

    .line 1938923
    iget-object v3, v0, LX/Cq4;->b:LX/CqA;

    iget-object v3, v3, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v3}, LX/CpO;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1938924
    iget-object v3, v0, LX/Cq4;->b:LX/CqA;

    iget-object v3, v3, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v3}, LX/CpO;->f()V

    .line 1938925
    :cond_1
    new-instance v3, LX/CmV;

    invoke-direct {v3}, LX/CmV;-><init>()V

    .line 1938926
    iput-object v2, v3, LX/CmV;->p:LX/8bN;

    .line 1938927
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 1938928
    iput-object v4, v3, LX/CmV;->h:Ljava/lang/String;

    .line 1938929
    iget-object v4, v0, LX/Cq4;->b:LX/CqA;

    iget-object v4, v4, LX/CqA;->ai:Landroid/os/Bundle;

    invoke-virtual {v3, v4}, LX/CmV;->a(Landroid/os/Bundle;)LX/CmV;

    .line 1938930
    sget-object v4, LX/Cps;->a:[I

    invoke-virtual {v2}, LX/8bN;->ordinal()I

    move-result v2

    aget v2, v4, v2

    packed-switch v2, :pswitch_data_0

    .line 1938931
    :cond_2
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->w()Ljava/lang/String;

    move-result-object v2

    .line 1938932
    iput-object v2, v3, LX/CmV;->d:Ljava/lang/String;

    .line 1938933
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->v()Ljava/lang/String;

    move-result-object v2

    .line 1938934
    iput-object v2, v3, LX/CmV;->e:Ljava/lang/String;

    .line 1938935
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 1938936
    iput-object v2, v3, LX/CmV;->f:Ljava/lang/String;

    .line 1938937
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 1938938
    iput-object v2, v3, LX/CmV;->g:Ljava/lang/String;

    .line 1938939
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->n()Ljava/lang/String;

    move-result-object v2

    .line 1938940
    iput-object v2, v3, LX/CmV;->i:Ljava/lang/String;

    .line 1938941
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->m()Ljava/lang/String;

    move-result-object v2

    .line 1938942
    iput-object v2, v3, LX/CmV;->j:Ljava/lang/String;

    .line 1938943
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->o()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1938944
    iput-object v2, v3, LX/CmV;->s:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1938945
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->p()Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v2

    .line 1938946
    iput-object v2, v3, LX/CmV;->t:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    .line 1938947
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->u()Ljava/lang/String;

    move-result-object v2

    .line 1938948
    iput-object v2, v3, LX/CmV;->m:Ljava/lang/String;

    .line 1938949
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->x()Ljava/lang/String;

    move-result-object v2

    .line 1938950
    iput-object v2, v3, LX/CmV;->n:Ljava/lang/String;

    .line 1938951
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->q()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    .line 1938952
    if-eqz v2, :cond_3

    .line 1938953
    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 1938954
    iput-object v2, v3, LX/CmV;->l:Ljava/lang/String;

    .line 1938955
    :cond_3
    iget-object v2, v0, LX/Cq4;->b:LX/CqA;

    iget-object v2, v2, LX/CqA;->ai:Landroid/os/Bundle;

    invoke-virtual {v3, v2}, LX/CmV;->a(Landroid/os/Bundle;)LX/CmV;

    .line 1938956
    iget-object v2, v0, LX/Cq4;->b:LX/CqA;

    iget-object v2, v2, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v2, v3}, LX/CpO;->a(LX/CmV;)V

    .line 1938957
    iget-object v2, v0, LX/Cq4;->b:LX/CqA;

    iget-object v2, v2, LX/CqA;->ad:LX/CpO;

    invoke-virtual {v2}, LX/CpO;->d()V

    goto/16 :goto_0

    .line 1938958
    :pswitch_0
    iget-object v2, v0, LX/Cq4;->b:LX/CqA;

    iget-object v2, v2, LX/CqA;->t:LX/1m0;

    iget-object v4, v0, LX/Cq4;->b:LX/CqA;

    iget-object v4, v4, LX/CqA;->u:LX/CoM;

    iget-object v5, v0, LX/Cq4;->b:LX/CqA;

    iget-object v5, v5, LX/CqA;->c:LX/03V;

    iget-object v6, v0, LX/Cq4;->b:LX/CqA;

    iget-object v6, v6, LX/CqA;->n:LX/0Uh;

    const/16 v7, 0xa5

    const/4 p0, 0x0

    invoke-virtual {v6, v7, p0}, LX/0Uh;->a(IZ)Z

    move-result v6

    invoke-static {v1, v2, v4, v5, v6}, LX/Cli;->a(Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;LX/1m0;LX/CoM;LX/03V;Z)LX/Cli;

    move-result-object v2

    .line 1938959
    iput-object v2, v3, LX/CmV;->q:LX/Cli;

    .line 1938960
    goto :goto_1

    .line 1938961
    :pswitch_1
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    .line 1938962
    if-eqz v2, :cond_2

    .line 1938963
    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 1938964
    iput-object v2, v3, LX/CmV;->k:Ljava/lang/String;

    .line 1938965
    goto/16 :goto_1

    .line 1938966
    :pswitch_2
    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentHTMLNativeAdFragmentModel$FallbackNativeAdModel;->l()Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdMultiShareObjectFragmentModel$ChildAdObjectsModel;

    move-result-object v2

    .line 1938967
    iput-object v2, v3, LX/CmV;->o:Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdMultiShareObjectFragmentModel$ChildAdObjectsModel;

    .line 1938968
    iget-object v2, v0, LX/Cq4;->b:LX/CqA;

    const/4 v4, 0x1

    .line 1938969
    iput-boolean v4, v2, LX/CqA;->ap:Z

    .line 1938970
    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
