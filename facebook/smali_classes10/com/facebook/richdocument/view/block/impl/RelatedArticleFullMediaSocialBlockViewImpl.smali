.class public Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/02k;
.implements LX/CnG;
.implements LX/CoW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Co4;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/02k;",
        "LX/CoW;",
        "Lcom/facebook/richdocument/view/block/RelatedArticleFullMediaSocialBlockView;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/String;

.field private static final k:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Ljava/lang/String;

.field private B:Landroid/view/View$OnClickListener;

.field public C:Ljava/lang/String;

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalUFI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/ClD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Cig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:Landroid/widget/LinearLayout;

.field private final m:Lcom/facebook/fbui/facepile/FacepileView;

.field private final n:Lcom/facebook/resources/ui/FbTextView;

.field private final o:Landroid/view/View;

.field private final p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final r:Lcom/facebook/resources/ui/FbTextView;

.field private final s:Landroid/widget/LinearLayout;

.field private final t:Landroid/widget/ImageView;

.field private final u:Lcom/facebook/resources/ui/FbTextView;

.field private final v:LX/CnK;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public w:I

.field public x:I

.field public y:I

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1938591
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->j:Ljava/lang/String;

    .line 1938592
    const-class v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1938557
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1938558
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;

    const/16 v3, 0x3227

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v5

    check-cast v5, LX/Ckw;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v9

    check-cast v9, LX/Crz;

    invoke-static {v0}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v10

    check-cast v10, LX/ClD;

    invoke-static {v0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v11

    check-cast v11, LX/Ck0;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v12

    check-cast v12, LX/Chi;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object p1

    check-cast p1, LX/Cju;

    invoke-static {v0}, LX/Cig;->a(LX/0QB;)LX/Cig;

    move-result-object v0

    check-cast v0, LX/Cig;

    iput-object v3, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->c:LX/Ckw;

    iput-object v9, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->d:LX/Crz;

    iput-object v10, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->e:LX/ClD;

    iput-object v11, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->f:LX/Ck0;

    iput-object v12, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->g:LX/Chi;

    iput-object p1, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->h:LX/Cju;

    iput-object v0, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->i:LX/Cig;

    .line 1938559
    const v0, 0x7f0d2a49

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->l:Landroid/widget/LinearLayout;

    .line 1938560
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->f:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->l:Landroid/widget/LinearLayout;

    const v2, 0x7f0d011b

    const v3, 0x7f0d011b

    const v4, 0x7f0d011b

    const v5, 0x7f0d011b

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->b(Landroid/view/View;IIII)V

    .line 1938561
    const v0, 0x7f0d2a4b

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->o:Landroid/view/View;

    .line 1938562
    const v0, 0x7f0d2a4a

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1938563
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v8}, Lcom/facebook/fbui/facepile/FacepileView;->setReverseFacesZIndex(Z)V

    .line 1938564
    const v0, 0x7f0d056b

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1938565
    const v0, 0x7f0d16c9

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1938566
    const v0, 0x7f0d2a48

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1938567
    const v0, 0x7f0d2a44

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 1938568
    const v0, 0x7f0d2a46

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->t:Landroid/widget/ImageView;

    .line 1938569
    const v0, 0x7f0d2a47

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->u:Lcom/facebook/resources/ui/FbTextView;

    .line 1938570
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->f:LX/Ck0;

    const v1, 0x7f0d2a43

    invoke-virtual {p0, v1}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d011b

    const v3, 0x7f0d011b

    const v4, 0x7f0d011b

    const v5, 0x7f0d011b

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->b(Landroid/view/View;IIII)V

    .line 1938571
    const v0, 0x7f0d2a45

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->s:Landroid/widget/LinearLayout;

    .line 1938572
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->f:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->s:Landroid/widget/LinearLayout;

    const v2, 0x7f0d011b

    const v3, 0x7f0d011b

    const v4, 0x7f0d011b

    const v5, 0x7f0d011b

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->b(Landroid/view/View;IIII)V

    .line 1938573
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K2B;

    .line 1938574
    const/4 v1, 0x1

    move v0, v1

    .line 1938575
    if-eqz v0, :cond_1

    .line 1938576
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K2B;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, LX/K2B;->b(Landroid/content/Context;Landroid/view/ViewGroup;)LX/CnK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    .line 1938577
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    if-eqz v0, :cond_0

    .line 1938578
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v7, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1938579
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1938580
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1938581
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1938582
    invoke-virtual {v1, v7}, Landroid/view/View;->setClickable(Z)V

    .line 1938583
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1938584
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->s:Landroid/widget/LinearLayout;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1938585
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->s:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1938586
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setClickable(Z)V

    .line 1938587
    new-instance v0, LX/Cmz;

    new-instance v1, LX/Cn4;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->h:LX/Cju;

    invoke-direct {v1, v2}, LX/Cn4;-><init>(LX/Cju;)V

    invoke-direct {v0, v1, v6, v6, v6}, LX/Cmz;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V

    .line 1938588
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1938589
    return-void

    .line 1938590
    :cond_1
    iput-object v6, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1938554
    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->B:Landroid/view/View$OnClickListener;

    .line 1938555
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->B:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1938556
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1938543
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1938544
    if-eqz p4, :cond_0

    .line 1938545
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081c66

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->j:Ljava/lang/String;

    new-array v5, v8, [I

    const v6, 0x7f0e07d6

    aput v6, v5, v7

    invoke-static/range {v0 .. v5}, LX/Crt;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    .line 1938546
    :cond_0
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1938547
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->j:Ljava/lang/String;

    new-array v5, v8, [I

    const v3, 0x7f0e07d1

    aput v3, v5, v7

    move-object v3, p1

    invoke-static/range {v0 .. v5}, LX/Crt;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    .line 1938548
    :cond_1
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1938549
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v5, v8, [I

    const v3, 0x7f0e07d2

    aput v3, v5, v7

    move-object v3, p2

    move-object v4, v2

    invoke-static/range {v0 .. v5}, LX/Crt;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    .line 1938550
    :cond_2
    invoke-static {p3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1938551
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v5, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->j:Ljava/lang/String;

    new-array v8, v8, [I

    const v0, 0x7f0e07d5

    aput v0, v8, v7

    move-object v4, v1

    move-object v6, p3

    move-object v7, v2

    invoke-static/range {v3 .. v8}, LX/Crt;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    .line 1938552
    :cond_3
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->r:Lcom/facebook/resources/ui/FbTextView;

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1938553
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1938542
    iget v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->w:I

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1938527
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1938528
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938529
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1938530
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->r:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938531
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1938532
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->u:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938533
    invoke-static {p0, v2}, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;Landroid/view/View$OnClickListener;)V

    .line 1938534
    iput-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->C:Ljava/lang/String;

    .line 1938535
    iput-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->A:Ljava/lang/String;

    .line 1938536
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->x:I

    .line 1938537
    iput-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->z:Ljava/lang/String;

    .line 1938538
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->y:I

    .line 1938539
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    if-eqz v0, :cond_0

    .line 1938540
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    invoke-virtual {v0, v2}, LX/CnK;->setFeedback(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1938541
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1938476
    invoke-static {p11}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p12}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1938477
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->l:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1938478
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1938479
    :goto_0
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1938480
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1938481
    :goto_1
    invoke-static {p3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1938482
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1938483
    :goto_2
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p9}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938484
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    if-eqz v0, :cond_0

    .line 1938485
    if-eqz p8, :cond_8

    if-nez p6, :cond_8

    invoke-static {p7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1938486
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    invoke-virtual {v0, p8}, LX/CnK;->setFeedback(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1938487
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CnK;->setVisibility(I)V

    .line 1938488
    :cond_0
    :goto_3
    if-nez p6, :cond_1

    invoke-static {p7}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1938489
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->t:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1938490
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->u:Lcom/facebook/resources/ui/FbTextView;

    sget-object v1, LX/Cs1;->SHORT_DOMAIN_NAME:LX/Cs1;

    invoke-static {p1, v1}, LX/Cs2;->a(Ljava/lang/String;LX/Cs1;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938491
    :goto_4
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1938492
    new-instance v0, LX/Cpd;

    invoke-direct {v0, p0, p1, p7}, LX/Cpd;-><init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;Landroid/view/View$OnClickListener;)V

    .line 1938493
    :cond_2
    invoke-static {p0, p5, p4, p10, p6}, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1938494
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1938495
    :goto_5
    return-void

    .line 1938496
    :cond_3
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->l:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1938497
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->o:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1938498
    invoke-static {p11}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1938499
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1938500
    :goto_6
    invoke-static {p12}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1938501
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1938502
    :cond_4
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p11}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1938503
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    goto :goto_6

    .line 1938504
    :cond_5
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p12}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938505
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1938506
    :cond_6
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1938507
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x3ff47ae1    # 1.91f

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1938508
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_1

    .line 1938509
    :cond_7
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1938510
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_2

    .line 1938511
    :cond_8
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->v:LX/CnK;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/CnK;->setVisibility(I)V

    goto/16 :goto_3

    .line 1938512
    :cond_9
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->t:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 1938513
    :cond_a
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->d:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    .line 1938514
    :goto_7
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->d:LX/Crz;

    invoke-virtual {v1}, LX/Crz;->a()Z

    move-result v1

    if-eqz v1, :cond_c

    const/4 v1, 0x4

    .line 1938515
    :goto_8
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1938516
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTextDirection(I)V

    goto/16 :goto_5

    .line 1938517
    :cond_b
    const/4 v0, 0x0

    goto :goto_7

    .line 1938518
    :cond_c
    const/4 v1, 0x3

    goto :goto_8
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1938519
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1938520
    const-string v1, "position"

    iget v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->x:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938521
    const-string v1, "num_related_articles"

    iget v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->y:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938522
    const-string v1, "click_source"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->z:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938523
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->C:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1938524
    const-string v1, "block_id"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->C:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938525
    :cond_0
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->c:LX/Ckw;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleFullMediaSocialBlockViewImpl;->A:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/Ckw;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1938526
    return-void
.end method
