.class public final Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/webkit/WebView;

.field public final synthetic c:LX/Cq4;


# direct methods
.method public constructor <init>(LX/Cq4;Ljava/lang/String;Landroid/webkit/WebView;)V
    .locals 0

    .prologue
    .line 1938886
    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->c:LX/Cq4;

    iput-object p2, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->b:Landroid/webkit/WebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1938887
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->c:LX/Cq4;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "placement"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Cq4;->a:Ljava/lang/String;

    .line 1938888
    :try_start_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1938889
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->c:LX/Cq4;

    iget-object v0, v0, LX/Cq4;->b:LX/CqA;

    const/4 v1, 0x1

    .line 1938890
    iput-boolean v1, v0, LX/CqA;->ao:Z

    .line 1938891
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->c:LX/Cq4;

    iget-object v0, v0, LX/Cq4;->b:LX/CqA;

    iget-object v0, v0, LX/CqA;->i:LX/ClK;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->c:LX/Cq4;

    iget-object v1, v1, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->f:LX/Ckw;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->c:LX/Cq4;

    iget-object v2, v2, LX/Cq4;->b:LX/CqA;

    iget-object v2, v2, LX/CqA;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/ClK;->h(LX/Ckw;Ljava/lang/String;)V

    .line 1938892
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->c:LX/Cq4;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->b:Landroid/webkit/WebView;

    invoke-static {v0, v1}, LX/Cq4;->d(LX/Cq4;Landroid/webkit/WebView;)V

    .line 1938893
    return-void

    .line 1938894
    :catch_0
    move-exception v0

    .line 1938895
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$3;->c:LX/Cq4;

    iget-object v1, v1, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->c:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/CqA;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_NPE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NPE while to attempting to stop loading the webview"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 1938896
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1938897
    move-object v0, v2

    .line 1938898
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method
