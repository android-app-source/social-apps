.class public Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cnc;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/richdocument/view/block/BylineBlockView;"
    }
.end annotation


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final f:Landroid/widget/LinearLayout;

.field public final g:I

.field public final h:I

.field public i:LX/Cmr;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1936042
    const-class v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/facebook/richdocument/view/widget/RichTextView;Lcom/facebook/drawee/fbpipeline/FbDraweeView;Landroid/widget/LinearLayout;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1936073
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1936074
    iput-object p2, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936075
    iput-object p4, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->f:Landroid/widget/LinearLayout;

    .line 1936076
    iput-object p3, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936077
    const-class v0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936078
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->a:LX/Cju;

    const v1, 0x7f0d011b

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->g:I

    .line 1936079
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->a:LX/Cju;

    const v1, 0x7f0d011d

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->h:I

    .line 1936080
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->a:LX/Cju;

    const v1, 0x7f0d011e

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    .line 1936081
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1936082
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1936083
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1936084
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1936085
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1936086
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->b:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1936087
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1936088
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936089
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936090
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    .line 1936091
    :cond_0
    :goto_0
    new-instance v0, LX/Cn7;

    new-instance v1, LX/Col;

    invoke-direct {v1, p0}, LX/Col;-><init>(Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;)V

    new-instance v2, LX/Cok;

    invoke-direct {v2, p0}, LX/Cok;-><init>(Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;)V

    new-instance v3, LX/CnA;

    invoke-direct {v3}, LX/CnA;-><init>()V

    new-instance v6, LX/Com;

    invoke-direct {v6, p0}, LX/Com;-><init>(Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;)V

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, LX/Cn7;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;LX/Cmm;LX/Cmt;)V

    .line 1936092
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1936093
    return-void

    .line 1936094
    :cond_1
    invoke-virtual {p1, v5}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1936095
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936096
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936097
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1936069
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 1936070
    :cond_0
    :goto_0
    return-void

    .line 1936071
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1936072
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    .line 1936051
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d2a37

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1936052
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1936053
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    instance-of v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    .line 1936054
    new-array v0, v2, [I

    .line 1936055
    new-array v2, v2, [I

    .line 1936056
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v3, v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->getLocationOnScreen([I)V

    .line 1936057
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLocationOnScreen([I)V

    move-object v0, v1

    .line 1936058
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1936059
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1936060
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->i:LX/Cmr;

    if-eqz v3, :cond_4

    .line 1936061
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->i:LX/Cmr;

    sget-object v5, LX/Cmr;->RIGHT:LX/Cmr;

    if-ne v3, v5, :cond_3

    .line 1936062
    :cond_0
    :goto_0
    move v1, v1

    .line 1936063
    if-eqz v1, :cond_2

    .line 1936064
    iget v1, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->g:I

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1936065
    :cond_1
    :goto_1
    return-void

    .line 1936066
    :cond_2
    iget v1, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->g:I

    invoke-virtual {v0, v4, v4, v1, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_1

    :cond_3
    move v1, v2

    .line 1936067
    goto :goto_0

    .line 1936068
    :cond_4
    invoke-static {}, LX/Crz;->c()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->b:LX/Crz;

    invoke-virtual {v3}, LX/Crz;->a()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_5
    move v1, v2

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    invoke-static {p0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object p0

    check-cast p0, LX/Crz;

    iput-object v1, p1, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->a:LX/Cju;

    iput-object p0, p1, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->b:LX/Crz;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1936043
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1936044
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a()V

    .line 1936045
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1936046
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1936047
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1936048
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1936049
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/BylineBlockViewImpl;->i:LX/Cmr;

    .line 1936050
    return-void
.end method
