.class public Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/02k;
.implements LX/CoZ;


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Coa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8bO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/widget/CustomLinearLayout;

.field public g:Lcom/facebook/widget/CustomLinearLayout;

.field public h:Lcom/facebook/widget/CustomLinearLayout;

.field public i:Lcom/facebook/widget/CustomLinearLayout;

.field public j:LX/Cou;

.field public k:LX/CpO;

.field private l:Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;

.field private o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1937440
    const-class v0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/CpO;)V
    .locals 1

    .prologue
    .line 1937362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1937363
    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->n:Landroid/view/View;

    .line 1937364
    iput-object p2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->k:LX/CpO;

    .line 1937365
    const-class v0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 1937366
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a(Landroid/view/View;)V

    .line 1937367
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 13

    .prologue
    .line 1937368
    const v0, 0x7f0d16ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937369
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308c3

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1937370
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308c2

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1937371
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1937372
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/Coa;->a(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    .line 1937373
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/Coa;->a(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    .line 1937374
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1937375
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308c4

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1937376
    const v0, 0x7f0d16c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937377
    :cond_1
    const v0, 0x7f0d16b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937378
    const v0, 0x7f0d16c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->l:Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;

    .line 1937379
    const v0, 0x7f0d16c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937380
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16ad

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->m:Landroid/view/View;

    .line 1937381
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->l:Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;

    .line 1937382
    const v1, 0x7f0d16c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1937383
    new-instance p1, LX/Cou;

    move-object v2, v0

    check-cast v2, LX/Ctg;

    invoke-direct {p1, v2, v0, v1}, LX/Cou;-><init>(LX/Ctg;Landroid/view/View;Landroid/widget/ImageView;)V

    move-object v0, p1

    .line 1937384
    check-cast v0, LX/Cou;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->j:LX/Cou;

    .line 1937385
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->j:LX/Cou;

    const/4 p1, 0x0

    .line 1937386
    iget-object v1, v0, LX/Cou;->a:LX/Cju;

    const v2, 0x7f0d0121

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    iget-object v2, v0, LX/Cou;->a:LX/Cju;

    const v3, 0x7f0d016f

    invoke-interface {v2, v3}, LX/Cju;->c(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 1937387
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1937388
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v2

    invoke-virtual {v2, v1, p1, v1, p1}, Lcom/facebook/richdocument/view/widget/SlideshowView;->setPadding(IIII)V

    .line 1937389
    :cond_2
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1937390
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->k:LX/CpO;

    new-instance v4, LX/Cmy;

    sget-object v5, LX/Cmw;->a:LX/Cmw;

    invoke-direct {v4, v5, v7, v7, v6}, LX/Cmy;-><init>(LX/Cmw;LX/Cmr;LX/Cmp;I)V

    invoke-virtual {v3, v4}, LX/Cod;->a(LX/Cml;)V

    .line 1937391
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d0121

    const v7, 0x7f0d0122

    move v8, v6

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937392
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->c:LX/8bO;

    invoke-virtual {v3}, LX/8bO;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1937393
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    const v9, 0x7f0d0121

    const v10, 0x7f0d0173

    const v11, 0x7f0d0121

    move v12, v6

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937394
    :cond_3
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    check-cast v0, Lcom/facebook/richdocument/view/widget/IAadsCustomLinearLayout;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->l:Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/IAadsCustomLinearLayout;->setMultishareOnInterceptTouchEventListener(LX/1OV;)V

    .line 1937395
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;

    invoke-static {v3}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v1

    check-cast v1, LX/Ck0;

    const/16 v2, 0x323a

    invoke-static {v3, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v3}, LX/8bO;->b(LX/0QB;)LX/8bO;

    move-result-object v2

    check-cast v2, LX/8bO;

    const/16 p0, 0x509

    invoke-static {v3, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    iput-object v1, p1, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a:LX/Ck0;

    iput-object v4, p1, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->b:LX/0Ot;

    iput-object v2, p1, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->c:LX/8bO;

    iput-object v3, p1, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->d:LX/0Or;

    return-void
.end method

.method private b(LX/CmW;)V
    .locals 13

    .prologue
    .line 1937396
    iget-object v0, p1, LX/CmW;->l:Ljava/lang/String;

    move-object v0, v0

    .line 1937397
    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    .line 1937398
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16bb

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1937399
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 1937400
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v3, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1937401
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1937402
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16bc

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937403
    iget-object v1, p1, LX/CmW;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1937404
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1937405
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v1

    .line 1937406
    iget-object v2, p1, LX/CmW;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1937407
    invoke-virtual {v1, v2}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937408
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937409
    const/4 v6, 0x0

    .line 1937410
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16ba

    invoke-virtual {v4, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/facebook/widget/CustomLinearLayout;

    .line 1937411
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16bb

    invoke-virtual {v4, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1937412
    if-eqz v10, :cond_2

    if-eqz v5, :cond_2

    .line 1937413
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a:LX/Ck0;

    const v8, 0x7f0d011b

    move v7, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937414
    invoke-virtual {v10, v6}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1937415
    invoke-virtual {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1937416
    :cond_2
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    if-eqz v4, :cond_3

    .line 1937417
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    move v7, v6

    move v8, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937418
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v9, 0x7f0d0121

    const v10, 0x7f0d011b

    const v11, 0x7f0d0122

    const v12, 0x7f0d011b

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937419
    :cond_3
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16bd

    invoke-virtual {v4, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937420
    if-eqz v5, :cond_4

    .line 1937421
    iget-object v4, v5, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v4

    .line 1937422
    const v7, 0x800015

    invoke-virtual {v4, v7}, LX/CtG;->setGravity(I)V

    .line 1937423
    invoke-virtual {v5}, Lcom/facebook/richdocument/view/widget/RichTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 1937424
    const v7, 0x3e4ccccd    # 0.2f

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1937425
    const/16 v7, 0x10

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1937426
    invoke-virtual {v5, v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1937427
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a:LX/Ck0;

    move v7, v6

    move v8, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937428
    :cond_4
    const/4 v3, 0x0

    .line 1937429
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16c5

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937430
    iget-object v1, p1, LX/CmW;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1937431
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1937432
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v1

    .line 1937433
    iget-object v2, p1, LX/CmW;->f:Ljava/lang/String;

    move-object v2, v2

    .line 1937434
    invoke-virtual {v1, v2}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937435
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1937436
    invoke-virtual {v0, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937437
    :cond_5
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d0121

    const v4, 0x7f0d0122

    const v5, 0x7f0d011b

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937438
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->a:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    const v6, 0x7f0d0121

    const v7, 0x7f0d0172

    const v8, 0x7f0d0121

    move v9, v3

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937439
    return-void
.end method


# virtual methods
.method public final a(LX/CmW;)V
    .locals 8

    .prologue
    .line 1937333
    iget-object v0, p1, LX/CmW;->o:Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdMultiShareObjectFragmentModel$ChildAdObjectsModel;

    move-object v0, v0

    .line 1937334
    iget-object v1, p1, LX/CmW;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1937335
    if-nez v0, :cond_2

    .line 1937336
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1937337
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Coa;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937338
    iget-object v3, p1, LX/CmW;->s:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v3, v3

    .line 1937339
    iget-object v4, p1, LX/CmW;->t:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-object v4, v4

    .line 1937340
    iget-object v5, p1, LX/CmW;->m:Ljava/lang/String;

    move-object v5, v5

    .line 1937341
    iget-object v6, p1, LX/CmW;->n:Ljava/lang/String;

    move-object v6, v6

    .line 1937342
    invoke-virtual/range {v0 .. v6}, LX/Coa;->a(Landroid/content/Context;Lcom/facebook/widget/CustomLinearLayout;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Ljava/lang/String;Ljava/lang/String;)V

    .line 1937343
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1937344
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->b(LX/CmW;)V

    .line 1937345
    :cond_1
    return-void

    .line 1937346
    :cond_2
    new-instance v4, LX/Clo;

    invoke-direct {v4, v1}, LX/Clo;-><init>(Ljava/lang/String;)V

    .line 1937347
    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdMultiShareObjectFragmentModel$ChildAdObjectsModel;->a()LX/0Px;

    move-result-object v5

    .line 1937348
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_3

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel;

    .line 1937349
    invoke-virtual {v2}, Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;

    move-result-object v2

    .line 1937350
    new-instance v7, LX/CmU;

    invoke-direct {v7, v1}, LX/CmU;-><init>(Ljava/lang/String;)V

    .line 1937351
    iput-object v2, v7, LX/CmU;->d:Lcom/facebook/richdocument/model/graphql/RichDocumentNativeAdsGraphqlModels$RichDocumentNativeAdToChildAdObjectsEdgeModel$NodeModel;

    .line 1937352
    const/4 v2, 0x1

    .line 1937353
    iput-boolean v2, v7, LX/CmU;->b:Z

    .line 1937354
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->c:LX/8bO;

    invoke-virtual {v2}, LX/8bO;->c()Z

    move-result v2

    .line 1937355
    iput-boolean v2, v7, LX/CmU;->c:Z

    .line 1937356
    invoke-virtual {v4, v7}, LX/Clo;->a(LX/Clr;)V

    .line 1937357
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1937358
    :cond_3
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->j:LX/Cou;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->NON_INTERACTIVE:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    invoke-virtual {v2, v3, v4}, LX/Cot;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;LX/Clo;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1937359
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->k:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->f()V

    .line 1937360
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->o:Z

    .line 1937361
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1937318
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1937332
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->o:Z

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1937319
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1937320
    const-string v1, "strategyType"

    sget-object v2, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY_MULTISHARE:LX/CrN;

    invoke-virtual {v2}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1937321
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->j:LX/Cou;

    invoke-virtual {v1, v0}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1937322
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1937323
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->o:Z

    .line 1937324
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1937325
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1937326
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->j:LX/Cou;

    .line 1937327
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1937328
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/richdocument/view/widget/SlideshowView;->setVisibility(I)V

    .line 1937329
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->l:Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/media/MediaFrameWithPlugins;->setVisibility(I)V

    .line 1937330
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1937331
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdMultishareViewImpl;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
