.class public Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;
.implements LX/CoW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/CnZ;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/richdocument/view/block/AuthorsBlockView;",
        "LX/CoW;"
    }
.end annotation


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:F

.field public final d:Landroid/widget/TextView;

.field public final e:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final g:I

.field public final h:Lcom/facebook/richdocument/view/widget/PressStateButton;

.field public final i:Lcom/facebook/richdocument/view/widget/PressStateButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1935939
    const-class v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 9

    .prologue
    const v5, 0x3f333333    # 0.7f

    const/4 v2, 0x0

    .line 1935920
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1935921
    iput v5, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->c:F

    .line 1935922
    const-class v0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 1935923
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0622

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->g:I

    .line 1935924
    const v0, 0x7f0d1673

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->d:Landroid/widget/TextView;

    .line 1935925
    const v0, 0x7f0d1674

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1935926
    const v0, 0x7f0d1671

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1935927
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->a:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v3, 0x7f0d0175

    const v4, 0x7f0d0175

    invoke-virtual {v0, v1, v3, v4}, LX/Ck0;->a(Landroid/view/View;II)V

    .line 1935928
    const v0, 0x7f0d1675

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->h:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 1935929
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->h:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v5}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setDrawableBaseScale(F)V

    .line 1935930
    const v0, 0x7f0d1676

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->i:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 1935931
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->i:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v5}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setDrawableBaseScale(F)V

    .line 1935932
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->i:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 1935933
    iput-boolean v2, v0, Lcom/facebook/richdocument/view/widget/PressStateButton;->f:Z

    .line 1935934
    const v0, 0x7f0d1670

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1935935
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->a:LX/Ck0;

    const v5, 0x7f0d011b

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1935936
    const v0, 0x7f0d1672

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1935937
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->a:LX/Ck0;

    const v5, 0x7f0d011b

    const v7, 0x7f0d011b

    move v6, v2

    move v8, v2

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1935938
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object p0

    check-cast p0, LX/Ck0;

    iput-object p0, p1, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->a:LX/Ck0;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1935919
    iget v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->g:I

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1935913
    invoke-super {p0, v2}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1935914
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a()V

    .line 1935915
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->h:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 1935916
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->h:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1935917
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->i:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 1935918
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1935906
    sget-object v0, LX/Coe;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1935907
    :goto_0
    return-void

    .line 1935908
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->i:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const v1, 0x7f081c54

    const v2, 0x7f021669    # 1.72916E38f

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a061c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a061b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, LX/CoV;->a(Lcom/facebook/richdocument/view/widget/PressStateButton;IIII)V

    .line 1935909
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->i:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v6}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    goto :goto_0

    .line 1935910
    :pswitch_1
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0615

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1935911
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->i:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const v2, 0x7f081c54

    const v3, 0x7f02166a

    invoke-static {v1, v2, v3, v0, v0}, LX/CoV;->a(Lcom/facebook/richdocument/view/widget/PressStateButton;IIII)V

    .line 1935912
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->i:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v6}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ZZ)V
    .locals 5

    .prologue
    .line 1935890
    if-nez p1, :cond_0

    .line 1935891
    :goto_0
    return-void

    .line 1935892
    :cond_0
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/AuthorsBlockViewImpl;->h:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 1935893
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0615

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 1935894
    goto :goto_1

    .line 1935895
    :goto_1
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p0, 0x7f0a0621

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getColor(I)I

    .line 1935896
    goto :goto_5

    .line 1935897
    :goto_2
    goto :goto_6

    .line 1935898
    :goto_3
    if-eqz p2, :cond_1

    move p0, v3

    .line 1935899
    :cond_1
    if-eqz p2, :cond_2

    .line 1935900
    :goto_4
    invoke-static {v1, p0, v3}, LX/CoV;->a(Lcom/facebook/richdocument/view/widget/PressStateButton;II)V

    .line 1935901
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 1935902
    goto :goto_0

    .line 1935903
    :goto_5
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p1, 0x7f0a061b

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    goto :goto_2

    .line 1935904
    :goto_6
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0a061c

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p0

    goto :goto_3

    :cond_2
    move v3, v4

    .line 1935905
    goto :goto_4
.end method
