.class public Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;
.implements LX/CoW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cns;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/CoW;",
        "Lcom/facebook/richdocument/view/block/LogoBlockView;"
    }
.end annotation


# static fields
.field private static final i:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/8bK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final k:Lcom/facebook/richdocument/view/widget/PressStateButton;

.field public final l:Landroid/view/View;

.field public m:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1936909
    const-class v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->i:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1936887
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1936888
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v3

    check-cast v3, LX/Cju;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v8

    check-cast v8, LX/1Ad;

    invoke-static {v0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v9

    check-cast v9, LX/Ck0;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v10

    check-cast v10, LX/Cju;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v12

    check-cast v12, LX/Crz;

    invoke-static {v0}, LX/8bK;->a(LX/0QB;)LX/8bK;

    move-result-object v13

    check-cast v13, LX/8bK;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v3, v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->a:LX/Cju;

    iput-object v8, v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->b:LX/1Ad;

    iput-object v9, v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->c:LX/Ck0;

    iput-object v10, v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->d:LX/Cju;

    iput-object v11, v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->e:LX/03V;

    iput-object v12, v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->f:LX/Crz;

    iput-object v13, v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->g:LX/8bK;

    iput-object v0, v2, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->h:LX/0Uh;

    .line 1936889
    const v0, 0x7f0d2a40

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936890
    const v0, 0x7f0d2a41

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/PressStateButton;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    .line 1936891
    const v0, 0x7f0d2a42

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->l:Landroid/view/View;

    .line 1936892
    const v0, 0x7f0d2a3f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1936893
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a004f

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    iput v1, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->m:I

    .line 1936894
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->c:LX/Ck0;

    const v2, 0x7f0d0174

    invoke-virtual {v1, v0, v4, v2}, LX/Ck0;->a(Landroid/view/View;II)V

    .line 1936895
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->d:LX/Cju;

    const v1, 0x7f0d016b

    invoke-interface {v0, v1}, LX/Cju;->c(I)I

    move-result v0

    .line 1936896
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1936897
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1936898
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->f:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1936899
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1936900
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->f:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1936901
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setTextDirection(I)V

    .line 1936902
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v6}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setLayoutDirection(I)V

    .line 1936903
    :cond_0
    :goto_1
    new-instance v0, LX/Cmz;

    new-instance v1, LX/Cn4;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->a:LX/Cju;

    invoke-direct {v1, v2}, LX/Cn4;-><init>(LX/Cju;)V

    invoke-direct {v0, v1, v5, v5, v5}, LX/Cmz;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V

    .line 1936904
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1936905
    return-void

    .line 1936906
    :cond_1
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setLayoutDirection(I)V

    goto :goto_0

    .line 1936907
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v7}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setTextDirection(I)V

    .line 1936908
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v4}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setLayoutDirection(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1936867
    iget v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->m:I

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1936881
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1936882
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1936883
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    .line 1936884
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1936885
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a004f

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->m:I

    .line 1936886
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 1936875
    if-nez p1, :cond_0

    .line 1936876
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1936877
    :goto_0
    return-void

    .line 1936878
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1936879
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->b:LX/1Ad;

    sget-object v1, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    new-instance v1, LX/CpH;

    invoke-direct {v1, p0, p2}, LX/CpH;-><init>(Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;I)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1936880
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 3

    .prologue
    .line 1936868
    if-nez p1, :cond_0

    .line 1936869
    :goto_0
    return-void

    .line 1936870
    :cond_0
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    if-eqz p2, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setAlpha(F)V

    .line 1936871
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    if-eqz p2, :cond_2

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f081c4e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/richdocument/view/widget/ScalableImageWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1936872
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/LogoBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/PressStateButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/PressStateButton;->setVisibility(I)V

    goto :goto_0

    .line 1936873
    :cond_1
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_1

    .line 1936874
    :cond_2
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f081c4d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
