.class public Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/02k;
.implements LX/CoZ;


# static fields
.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8bO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Coa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/widget/CustomLinearLayout;

.field public h:Lcom/facebook/widget/CustomLinearLayout;

.field public i:Lcom/facebook/widget/CustomLinearLayout;

.field public j:Lcom/facebook/widget/CustomLinearLayout;

.field public k:Lcom/facebook/widget/CustomLinearLayout;

.field private l:LX/Ctg;

.field public m:LX/CpO;

.field public n:LX/CpU;

.field private o:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public p:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public q:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public r:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public s:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public t:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private u:Landroid/view/View;

.field private v:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1938093
    const-class v0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/CpO;)V
    .locals 7

    .prologue
    .line 1938087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1938088
    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->v:Landroid/view/View;

    .line 1938089
    iput-object p2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->m:LX/CpO;

    .line 1938090
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p2

    move-object v2, p0

    check-cast v2, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;

    invoke-static {p2}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {p2}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v4

    check-cast v4, LX/Ck0;

    invoke-static {p2}, LX/8bO;->b(LX/0QB;)LX/8bO;

    move-result-object v5

    check-cast v5, LX/8bO;

    const/16 v6, 0x509

    invoke-static {p2, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v0, 0x323a

    invoke-static {p2, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    iput-object v3, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->a:LX/0Uh;

    iput-object v4, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iput-object v5, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    iput-object v6, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->d:LX/0Or;

    iput-object p2, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->e:LX/0Ot;

    .line 1938091
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->a(Landroid/view/View;)V

    .line 1938092
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 1938055
    const v0, 0x7f0d16ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    .line 1938056
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308c5

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1938057
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308be

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1938058
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1938059
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    invoke-static {v0, v1, v3}, LX/Coa;->a(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    .line 1938060
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/Coa;->a(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    .line 1938061
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1938062
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0308c4

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1938063
    const v0, 0x7f0d16c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->j:Lcom/facebook/widget/CustomLinearLayout;

    .line 1938064
    :cond_1
    const v0, 0x7f0d16b9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    .line 1938065
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16b5

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938066
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16b4

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938067
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16c8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938068
    const v0, 0x7f0d16ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    .line 1938069
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16b2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938070
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16b1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938071
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16b0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->k:Lcom/facebook/widget/CustomLinearLayout;

    .line 1938072
    const v0, 0x7f0d16c7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/Ctg;

    check-cast v0, LX/Ctg;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->l:LX/Ctg;

    .line 1938073
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16ad

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->u:Landroid/view/View;

    .line 1938074
    new-instance v0, LX/CpU;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->l:LX/Ctg;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->m:LX/CpO;

    invoke-direct {v0, v1, p1, v2}, LX/CpU;-><init>(LX/Ctg;Landroid/view/View;LX/CpO;)V

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    .line 1938075
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16bd

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938076
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->a:LX/0Uh;

    const/16 v1, 0x3de

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1938077
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    const/16 v1, 0x14

    .line 1938078
    iput v1, v0, LX/CpT;->y:I

    .line 1938079
    :cond_2
    const/4 v6, 0x0

    .line 1938080
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16af

    invoke-virtual {v4, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/facebook/widget/CustomLinearLayout;

    .line 1938081
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->k:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f0d011b

    move v7, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1938082
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    const v7, 0x7f0d011c

    move-object v5, v10

    move v8, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1938083
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const v9, 0x7f0d011b

    const v11, 0x7f0d011b

    const v12, 0x7f0d011c

    move v10, v6

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1938084
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v4}, LX/8bO;->d()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1938085
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->j:Lcom/facebook/widget/CustomLinearLayout;

    const v7, 0x7f0d0172

    move v8, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1938086
    :cond_3
    return-void
.end method

.method public static a(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;II)V
    .locals 1

    .prologue
    .line 1938052
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundResource(I)V

    .line 1938053
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundResource(I)V

    .line 1938054
    return-void
.end method

.method public static b(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1938045
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1938046
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1938047
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938048
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1938049
    invoke-virtual {v0, p1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1938050
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v3, 0x7f0d011b

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1938051
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 1937983
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1937984
    :goto_0
    return-void

    .line 1937985
    :cond_0
    const/4 v6, 0x0

    .line 1937986
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16ba

    invoke-virtual {v4, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/facebook/widget/CustomLinearLayout;

    .line 1937987
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16bb

    invoke-virtual {v4, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1937988
    if-eqz v10, :cond_1

    if-eqz v5, :cond_1

    .line 1937989
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    const v8, 0x7f0d011b

    move v7, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937990
    invoke-virtual {v10, v6}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1937991
    invoke-virtual {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1937992
    :cond_1
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    if-eqz v4, :cond_2

    .line 1937993
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    const v9, 0x7f0d011b

    const v10, 0x7f0d011b

    const v11, 0x7f0d011b

    const v12, 0x7f0d011b

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937994
    :cond_2
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v4, :cond_3

    .line 1937995
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937996
    iget-object v5, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v5

    .line 1937997
    const v5, 0x800015

    invoke-virtual {v4, v5}, LX/CtG;->setGravity(I)V

    .line 1937998
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 1937999
    const v5, 0x3e4ccccd    # 0.2f

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1938000
    const/16 v5, 0x10

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1938001
    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v5, v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1938002
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    move v7, v6

    move v8, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1938003
    :cond_3
    invoke-static {p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    .line 1938004
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16bb

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1938005
    if-eqz v0, :cond_4

    .line 1938006
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v3, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1938007
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1938008
    :cond_4
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16bc

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938009
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1938010
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v1

    .line 1938011
    invoke-virtual {v1, p2}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1938012
    :cond_5
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public static h(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1938028
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1938029
    :cond_0
    :goto_0
    return-void

    .line 1938030
    :cond_1
    const v0, 0x7f0d011b

    .line 1938031
    const v7, 0x7f0d011b

    .line 1938032
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v1}, LX/8bO;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1938033
    const v6, 0x7f0d0121

    .line 1938034
    const v7, 0x7f0d0122

    .line 1938035
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d011c

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    move v9, v6

    .line 1938036
    :goto_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1938037
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    move v5, v9

    move v6, v2

    move v8, v2

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1938038
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    move v5, v9

    move v6, v2

    move v8, v2

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1938039
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v1}, LX/8bO;->a()Z

    move-result v1

    .line 1938040
    iget-object v3, v0, LX/CpU;->D:LX/Ctg;

    instance-of v3, v3, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;

    if-eqz v3, :cond_2

    .line 1938041
    iget-object v3, v0, LX/CpU;->D:LX/Ctg;

    check-cast v3, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;

    .line 1938042
    iput-boolean v1, v3, Lcom/facebook/richdocument/view/widget/media/NativeAdsMediaFrameWithPlugins;->j:Z

    .line 1938043
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1938044
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->j:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d0121

    const v6, 0x7f0d0172

    const v7, 0x7f0d0122

    move v8, v2

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    goto :goto_0

    :cond_3
    move v9, v0

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/CmW;)V
    .locals 14

    .prologue
    .line 1938094
    iget-object v0, p1, LX/CmW;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1938095
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1938096
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1938097
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938098
    iget-object v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v2

    .line 1938099
    invoke-virtual {v1, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1938100
    :cond_0
    iget-object v0, p1, LX/CmW;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1938101
    iget-object v1, p1, LX/CmW;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1938102
    const/4 v9, 0x0

    .line 1938103
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1938104
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v9}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1938105
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938106
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1938107
    invoke-virtual {v7, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1938108
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v10, 0x7f0d011b

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1938109
    invoke-static {p0, v1}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;Ljava/lang/String;)V

    .line 1938110
    :goto_0
    iget-object v0, p1, LX/CmW;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1938111
    invoke-static {p0, v0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;Ljava/lang/String;)V

    .line 1938112
    iget-object v0, p1, LX/CmW;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1938113
    const/4 v9, 0x1

    .line 1938114
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1938115
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1938116
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938117
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1938118
    invoke-virtual {v7, v9}, LX/CtG;->setAllCaps(Z)V

    .line 1938119
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938120
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1938121
    invoke-virtual {v7, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1938122
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938123
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1938124
    invoke-virtual {v7, v9}, LX/CtG;->setAllCaps(Z)V

    .line 1938125
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938126
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1938127
    invoke-virtual {v7, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1938128
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v9, 0x7f0d011b

    const v10, 0x7f0d011b

    const v11, 0x7f0d011b

    const v12, 0x7f0d011b

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1938129
    :cond_1
    iget-object v0, p1, LX/CmW;->q:LX/Cli;

    move-object v0, v0

    .line 1938130
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    invoke-virtual {v1, v0}, LX/CpU;->a(LX/Cli;)V

    .line 1938131
    iget-object v0, p1, LX/CmW;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1938132
    iget-object v1, p1, LX/CmW;->l:Ljava/lang/String;

    move-object v1, v1

    .line 1938133
    iget-object v2, p1, LX/CmW;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1938134
    invoke-static {p0, v1, v2}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;Ljava/lang/String;Ljava/lang/String;)V

    .line 1938135
    const/16 v13, 0x8

    const/4 v9, 0x0

    .line 1938136
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v7}, LX/8bO;->b()Z

    move-result v7

    if-nez v7, :cond_5

    .line 1938137
    :goto_1
    iget-object v0, p1, LX/CmW;->r:Landroid/os/Bundle;

    move-object v0, v0

    .line 1938138
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 1938139
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->a(Landroid/os/Bundle;)V

    .line 1938140
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    invoke-virtual {v1}, LX/CpT;->f()V

    .line 1938141
    :cond_2
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1938142
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Coa;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->j:Lcom/facebook/widget/CustomLinearLayout;

    .line 1938143
    iget-object v3, p1, LX/CmW;->s:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v3, v3

    .line 1938144
    iget-object v4, p1, LX/CmW;->t:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-object v4, v4

    .line 1938145
    iget-object v5, p1, LX/CmW;->m:Ljava/lang/String;

    move-object v5, v5

    .line 1938146
    iget-object v6, p1, LX/CmW;->n:Ljava/lang/String;

    move-object v6, v6

    .line 1938147
    invoke-virtual/range {v0 .. v6}, LX/Coa;->a(Landroid/content/Context;Lcom/facebook/widget/CustomLinearLayout;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Ljava/lang/String;Ljava/lang/String;)V

    .line 1938148
    :cond_3
    return-void

    .line 1938149
    :cond_4
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f0d16b3

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938150
    invoke-virtual {v8, v9}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1938151
    iget-object v7, v8, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v7

    .line 1938152
    invoke-virtual {v7, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1938153
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    const v10, 0x7f0d011b

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->c(Landroid/view/View;IIII)V

    goto/16 :goto_0

    .line 1938154
    :cond_5
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v13}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1938155
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v13}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1938156
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->k:Lcom/facebook/widget/CustomLinearLayout;

    move v10, v9

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1938157
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    move v10, v9

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1938158
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f0d16b6

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/widget/CustomLinearLayout;

    .line 1938159
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->b:LX/Ck0;

    const v10, 0x7f0d011b

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1938160
    invoke-virtual {v8, v9}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1938161
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v7}, LX/8bO;->i()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1938162
    const/16 v7, 0x13

    invoke-virtual {v8, v7}, Lcom/facebook/widget/CustomLinearLayout;->setGravity(I)V

    .line 1938163
    :cond_6
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f0d16b7

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938164
    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v10, 0x7f0d16b8

    invoke-virtual {v8, v10}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1938165
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 1938166
    iget-object v10, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v10, v10

    .line 1938167
    invoke-virtual {v10, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1938168
    invoke-virtual {v7, v9}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1938169
    invoke-virtual {v8, v9}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1938170
    :goto_2
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f02106d

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundResource(I)V

    .line 1938171
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f02106c

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundResource(I)V

    .line 1938172
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v7}, LX/8bO;->g()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1938173
    invoke-static {p0, v9, v9}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;II)V

    .line 1938174
    :goto_3
    invoke-static {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;)V

    goto/16 :goto_1

    .line 1938175
    :cond_7
    invoke-virtual {v8, v13}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_2

    .line 1938176
    :cond_8
    const v7, 0x7f02106c

    const v8, 0x7f02106d

    invoke-static {p0, v7, v8}, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;II)V

    goto :goto_3
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1938017
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->m:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->f()V

    .line 1938018
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    invoke-virtual {v0, p1}, LX/Cod;->b(Landroid/os/Bundle;)V

    .line 1938019
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    .line 1938020
    iget-boolean v1, v0, LX/CpU;->A:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, LX/CpU;->a:Z

    if-nez v1, :cond_1

    .line 1938021
    :cond_0
    :goto_0
    return-void

    .line 1938022
    :cond_1
    iget-object v1, v0, LX/CpU;->B:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, LX/CpT;->e(Landroid/os/Bundle;)V

    .line 1938023
    iget-object v1, v0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 1938024
    invoke-interface {v1}, LX/Ctf;->getPlugins()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ctr;

    .line 1938025
    invoke-interface {v1}, LX/Ctr;->b()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1938026
    invoke-interface {v1}, LX/Ctr;->d()V

    goto :goto_1

    .line 1938027
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/CpU;->A:Z

    goto :goto_0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1938013
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1938014
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1938015
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1938016
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1937976
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    .line 1937977
    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    .line 1937978
    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    .line 1937979
    invoke-virtual {v0}, LX/Cod;->c()Landroid/view/View;

    move-result-object p0

    invoke-virtual {v1, p0}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    const/4 p0, -0x1

    if-eq v1, p0, :cond_0

    .line 1937980
    const/4 v1, 0x1

    .line 1937981
    :goto_0
    move v0, v1

    .line 1937982
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1937969
    sget-object v0, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY:LX/CrN;

    .line 1937970
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v1}, LX/8bO;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->c:LX/8bO;

    invoke-virtual {v1}, LX/8bO;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1937971
    sget-object v0, LX/CrN;->NATIVE_ADS_ASPECT_FIT_ONLY_EDGE_TO_EDGE:LX/CrN;

    .line 1937972
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1937973
    const-string v2, "strategyType"

    invoke-virtual {v0}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1937974
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    invoke-virtual {v0, v1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 1937975
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1937960
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    invoke-virtual {v0, p1}, LX/Cod;->c(Landroid/os/Bundle;)V

    .line 1937961
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->n:LX/CpU;

    .line 1937962
    iget-boolean v1, v0, LX/CpU;->A:Z

    if-nez v1, :cond_0

    .line 1937963
    :goto_0
    return-void

    .line 1937964
    :cond_0
    iget-object v1, v0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 1937965
    invoke-interface {v1}, LX/Ctf;->getPlugins()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ctr;

    .line 1937966
    invoke-interface {v1}, LX/Ctr;->b()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1937967
    invoke-interface {v1}, LX/Ctr;->e()V

    goto :goto_1

    .line 1937968
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/CpU;->A:Z

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1937956
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->u:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1937957
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v1, LX/CpV;

    invoke-direct {v1, p0}, LX/CpV;-><init>(Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 1937958
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1937959
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1937955
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdVideoViewImpl;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
