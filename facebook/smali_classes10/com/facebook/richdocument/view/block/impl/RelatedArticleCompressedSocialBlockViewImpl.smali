.class public Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/02k;
.implements LX/CnG;
.implements LX/CoW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Co3;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/02k;",
        "LX/CoW;",
        "Lcom/facebook/richdocument/view/block/RelatedArticleCompressedSocialBlockView;"
    }
.end annotation


# static fields
.field private static final j:Ljava/lang/String;

.field private static final k:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:Landroid/view/View$OnClickListener;

.field public B:Ljava/lang/String;

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalUFI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/ClD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Cig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final l:Landroid/widget/LinearLayout;

.field private final m:Lcom/facebook/fbui/facepile/FacepileView;

.field private final n:Lcom/facebook/resources/ui/FbTextView;

.field private final o:Landroid/view/View;

.field private final p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final q:Lcom/facebook/resources/ui/FbTextView;

.field private final r:Landroid/widget/LinearLayout;

.field private final s:Landroid/widget/ImageView;

.field private final t:Lcom/facebook/resources/ui/FbTextView;

.field private final u:LX/CnK;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public v:I

.field public w:I

.field public x:I

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1938328
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->j:Ljava/lang/String;

    .line 1938329
    const-class v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1938407
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1938408
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;

    const/16 v3, 0x3227

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v5

    check-cast v5, LX/Ckw;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v9

    check-cast v9, LX/Crz;

    invoke-static {v0}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v10

    check-cast v10, LX/ClD;

    invoke-static {v0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v11

    check-cast v11, LX/Ck0;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v12

    check-cast v12, LX/Chi;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object p1

    check-cast p1, LX/Cju;

    invoke-static {v0}, LX/Cig;->a(LX/0QB;)LX/Cig;

    move-result-object v0

    check-cast v0, LX/Cig;

    iput-object v3, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->c:LX/Ckw;

    iput-object v9, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->d:LX/Crz;

    iput-object v10, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->e:LX/ClD;

    iput-object v11, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->f:LX/Ck0;

    iput-object v12, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->g:LX/Chi;

    iput-object p1, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->h:LX/Cju;

    iput-object v0, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->i:LX/Cig;

    .line 1938409
    const v0, 0x7f0d2a49

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->l:Landroid/widget/LinearLayout;

    .line 1938410
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->f:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->l:Landroid/widget/LinearLayout;

    const v2, 0x7f0d011b

    const v3, 0x7f0d011b

    const v4, 0x7f0d011b

    const v5, 0x7f0d011b

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->b(Landroid/view/View;IIII)V

    .line 1938411
    const v0, 0x7f0d2a4b

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->o:Landroid/view/View;

    .line 1938412
    const v0, 0x7f0d2a4a

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1938413
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v8}, Lcom/facebook/fbui/facepile/FacepileView;->setReverseFacesZIndex(Z)V

    .line 1938414
    const v0, 0x7f0d056b

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1938415
    const v0, 0x7f0d16c9

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1938416
    const v0, 0x7f0d2a44

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 1938417
    const v0, 0x7f0d2a46

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->s:Landroid/widget/ImageView;

    .line 1938418
    const v0, 0x7f0d2a47

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 1938419
    const v0, 0x7f0d2a45

    invoke-virtual {p0, v0}, LX/Cod;->d(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->r:Landroid/widget/LinearLayout;

    .line 1938420
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->f:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->r:Landroid/widget/LinearLayout;

    const v2, 0x7f0d011b

    const v3, 0x7f0d011b

    const v4, 0x7f0d011b

    const v5, 0x7f0d011b

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->b(Landroid/view/View;IIII)V

    .line 1938421
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K2B;

    .line 1938422
    const/4 v1, 0x1

    move v0, v1

    .line 1938423
    if-eqz v0, :cond_1

    .line 1938424
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/K2B;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, LX/K2B;->b(Landroid/content/Context;Landroid/view/ViewGroup;)LX/CnK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    .line 1938425
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    if-eqz v0, :cond_0

    .line 1938426
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v7, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1938427
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1938428
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1938429
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1938430
    invoke-virtual {v1, v7}, Landroid/view/View;->setClickable(Z)V

    .line 1938431
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1938432
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->r:Landroid/widget/LinearLayout;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 1938433
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->r:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1938434
    :cond_0
    :goto_0
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setClickable(Z)V

    .line 1938435
    new-instance v0, LX/Cmz;

    new-instance v1, LX/Cn4;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->h:LX/Cju;

    invoke-direct {v1, v2}, LX/Cn4;-><init>(LX/Cju;)V

    invoke-direct {v0, v1, v6, v6, v6}, LX/Cmz;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V

    .line 1938436
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1938437
    return-void

    .line 1938438
    :cond_1
    iput-object v6, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    goto :goto_0
.end method

.method private static a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1938439
    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->A:Landroid/view/View$OnClickListener;

    .line 1938440
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1938441
    return-void
.end method

.method private static a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1938388
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1938389
    if-eqz p4, :cond_0

    .line 1938390
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081c66

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->j:Ljava/lang/String;

    new-array v5, v8, [I

    const v6, 0x7f0e07d6

    aput v6, v5, v7

    const v6, 0x7f0e07d4

    aput v6, v5, v9

    invoke-static/range {v0 .. v5}, LX/Crt;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    .line 1938391
    :cond_0
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1938392
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->j:Ljava/lang/String;

    new-array v5, v8, [I

    const v3, 0x7f0e07d1

    aput v3, v5, v7

    const v3, 0x7f0e07d4

    aput v3, v5, v9

    move-object v3, p1

    invoke-static/range {v0 .. v5}, LX/Crt;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    .line 1938393
    :cond_1
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1938394
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v5, v8, [I

    const v3, 0x7f0e07d2

    aput v3, v5, v7

    const v3, 0x7f0e07d3

    aput v3, v5, v9

    move-object v3, p2

    move-object v4, v2

    invoke-static/range {v0 .. v5}, LX/Crt;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    .line 1938395
    :cond_2
    invoke-static {p3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1938396
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v5, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->j:Ljava/lang/String;

    new-array v8, v8, [I

    const v0, 0x7f0e07d5

    aput v0, v8, v7

    const v0, 0x7f0e07d3

    aput v0, v8, v9

    move-object v4, v1

    move-object v6, p3

    move-object v7, v2

    invoke-static/range {v3 .. v8}, LX/Crt;->a(Landroid/content/Context;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    .line 1938397
    :cond_3
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->q:Lcom/facebook/resources/ui/FbTextView;

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1938398
    return-void
.end method

.method private static d(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 1938399
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1938400
    :goto_0
    return-void

    .line 1938401
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->d:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1938402
    :goto_1
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->d:LX/Crz;

    invoke-virtual {v1}, LX/Crz;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    .line 1938403
    :goto_2
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1938404
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTextDirection(I)V

    goto :goto_0

    .line 1938405
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1938406
    :cond_2
    const/4 v1, 0x3

    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1938387
    iget v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->v:I

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1938373
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1938374
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938375
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1938376
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->q:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938377
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->t:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938378
    invoke-static {p0, v2}, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;Landroid/view/View$OnClickListener;)V

    .line 1938379
    iput-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->B:Ljava/lang/String;

    .line 1938380
    iput-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->z:Ljava/lang/String;

    .line 1938381
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->w:I

    .line 1938382
    iput-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->y:Ljava/lang/String;

    .line 1938383
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->x:I

    .line 1938384
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    if-eqz v0, :cond_0

    .line 1938385
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    invoke-virtual {v0, v2}, LX/CnK;->setFeedback(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1938386
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1938338
    invoke-static/range {p10 .. p10}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static/range {p11 .. p11}, LX/0YN;->e(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1938339
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->l:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1938340
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->o:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1938341
    :goto_0
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1938342
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1938343
    :goto_1
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p8}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938344
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    if-eqz v1, :cond_0

    .line 1938345
    if-eqz p7, :cond_8

    if-nez p5, :cond_8

    invoke-static {p6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1938346
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    invoke-virtual {v1, p7}, LX/CnK;->setFeedback(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1938347
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/CnK;->setVisibility(I)V

    .line 1938348
    :cond_0
    :goto_2
    if-nez p5, :cond_1

    invoke-static {p6}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1938349
    :cond_1
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->s:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1938350
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->t:Lcom/facebook/resources/ui/FbTextView;

    sget-object v2, LX/Cs1;->SHORT_DOMAIN_NAME:LX/Cs1;

    invoke-static {p1, v2}, LX/Cs2;->a(Ljava/lang/String;LX/Cs1;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938351
    :goto_3
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1938352
    new-instance v1, LX/Cpc;

    invoke-direct {v1, p0, p1, p6}, LX/Cpc;-><init>(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v1}, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;Landroid/view/View$OnClickListener;)V

    .line 1938353
    :cond_2
    invoke-static {p0, p4, p3, p9, p5}, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1938354
    invoke-static {p0}, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->d(Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;)V

    .line 1938355
    return-void

    .line 1938356
    :cond_3
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->l:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1938357
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->o:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1938358
    invoke-static/range {p10 .. p10}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1938359
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1938360
    :goto_4
    invoke-static/range {p11 .. p11}, LX/0YN;->e(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1938361
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 1938362
    :cond_4
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1938363
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->m:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    goto :goto_4

    .line 1938364
    :cond_5
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1938365
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1938366
    :cond_6
    invoke-static {}, LX/Crz;->c()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1938367
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->d:LX/Crz;

    invoke-virtual {v1}, LX/Crz;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1938368
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b12d1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPaddingRelative(IIII)V

    .line 1938369
    :cond_7
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1938370
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_1

    .line 1938371
    :cond_8
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->u:LX/CnK;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/CnK;->setVisibility(I)V

    goto/16 :goto_2

    .line 1938372
    :cond_9
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->s:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1938330
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1938331
    const-string v1, "position"

    iget v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->w:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938332
    const-string v1, "num_related_articles"

    iget v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->x:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938333
    const-string v1, "click_source"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->y:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938334
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->B:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1938335
    const-string v1, "block_id"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->B:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938336
    :cond_0
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->c:LX/Ckw;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleCompressedSocialBlockViewImpl;->z:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/Ckw;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1938337
    return-void
.end method
