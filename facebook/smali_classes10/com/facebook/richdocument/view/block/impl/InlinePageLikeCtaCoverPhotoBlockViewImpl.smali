.class public Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cnm;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/richdocument/view/block/InlinePageLikeCtaCoverPhotoBlockView;"
    }
.end annotation


# static fields
.field public static final h:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final d:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final e:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final f:Lcom/facebook/fbui/glyph/GlyphView;

.field public final g:Lcom/facebook/fbui/facepile/FacepileView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1936769
    const-class v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->h:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1936770
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1936771
    const v0, 0x7f0d16a7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936772
    const v0, 0x7f0d169f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936773
    const v0, 0x7f0d16a0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936774
    const v0, 0x7f0d16a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936775
    const v0, 0x7f0d16a6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936776
    const v0, 0x7f0d16a5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1936777
    const v0, 0x7f0d16a2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->g:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1936778
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;II)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlInterfaces$RichDocumentSubscriptionCTAPublisher$FriendsWhoLike$Edges;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 1936779
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1936780
    if-nez v1, :cond_1

    .line 1936781
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936782
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936783
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081c7c

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1936784
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936785
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936786
    invoke-virtual {v0, v8}, LX/CtG;->setGravity(I)V

    .line 1936787
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936788
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1936789
    invoke-virtual {v0, v8}, LX/CtG;->setGravity(I)V

    .line 1936790
    return-void

    .line 1936791
    :cond_1
    if-ne v1, v8, :cond_2

    .line 1936792
    invoke-virtual {p1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1936793
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936794
    iget-object v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v2

    .line 1936795
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081c7d

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v9

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1936796
    :cond_2
    if-ne v1, v10, :cond_3

    .line 1936797
    invoke-virtual {p1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1936798
    invoke-virtual {p1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1936799
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936800
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1936801
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f081c7e

    new-array v5, v10, [Ljava/lang/Object;

    aput-object v1, v5, v9

    aput-object v0, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1936802
    :cond_3
    if-le v1, v10, :cond_0

    .line 1936803
    invoke-virtual {p1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1936804
    invoke-virtual {p1, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentSubscriptionCTAPublisherModel$FriendsWhoLikeModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1936805
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->d:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936806
    iget-object v4, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v4

    .line 1936807
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f00d4

    add-int/lit8 v6, v1, -0x2

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v2, v7, v9

    aput-object v0, v7, v8

    add-int/lit8 v0, v1, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v10

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    const v1, -0x6e685d

    const v2, -0xa76f01

    .line 1936808
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1936809
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlinePageLikeCtaCoverPhotoBlockViewImpl;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936810
    iget-object v3, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v3

    .line 1936811
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, LX/CtG;->setTextColor(I)V

    .line 1936812
    return-void

    :cond_0
    move v0, v2

    .line 1936813
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1936814
    goto :goto_1
.end method
