.class public Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/02k;
.implements LX/CnG;
.implements LX/CoW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Co5;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/02k;",
        "LX/CoW;",
        "Lcom/facebook/richdocument/view/block/RelatedArticleBlockView;"
    }
.end annotation


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ckw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Crz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/ClD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Cig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final k:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final l:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final m:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final o:Landroid/view/View;

.field public p:I

.field public q:Ljava/lang/String;

.field public r:I

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1938218
    const-class v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 1938219
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1938220
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0622

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->p:I

    .line 1938221
    const v0, 0x7f0d16cd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938222
    const v0, 0x7f0d16cc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938223
    const v0, 0x7f0d16cb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938224
    const v0, 0x7f0d16c9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1938225
    const v0, 0x7f0d16ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->o:Landroid/view/View;

    .line 1938226
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/Ckw;->a(LX/0QB;)LX/Ckw;

    move-result-object v5

    check-cast v5, LX/Ckw;

    invoke-static {v0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v6

    check-cast v6, LX/Ck0;

    invoke-static {v0}, LX/Crz;->a(LX/0QB;)LX/Crz;

    move-result-object v8

    check-cast v8, LX/Crz;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static {v0}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v10

    check-cast v10, LX/ClD;

    invoke-static {v0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v11

    check-cast v11, LX/Chi;

    invoke-static {v0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v12

    check-cast v12, LX/Cju;

    invoke-static {v0}, LX/Cig;->a(LX/0QB;)LX/Cig;

    move-result-object v0

    check-cast v0, LX/Cig;

    iput-object v4, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->b:LX/Ckw;

    iput-object v6, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->c:LX/Ck0;

    iput-object v8, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->d:LX/Crz;

    iput-object v9, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->e:LX/0Uh;

    iput-object v10, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->f:LX/ClD;

    iput-object v11, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->g:LX/Chi;

    iput-object v12, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->h:LX/Cju;

    iput-object v0, v2, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->i:LX/Cig;

    .line 1938227
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->c:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v2, 0x7f0d011b

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->b(Landroid/view/View;IIII)V

    .line 1938228
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->c:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v2, 0x7f0d011b

    const v5, 0x7f0d0177

    move v4, v3

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->b(Landroid/view/View;IIII)V

    .line 1938229
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->c:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v2, 0x7f0d011b

    const v5, 0x7f0d0145

    move v4, v3

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->b(Landroid/view/View;IIII)V

    .line 1938230
    const v0, 0x7f0d1670

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1938231
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->c:LX/Ck0;

    const v6, 0x7f0d011c

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v6}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1938232
    invoke-static {}, LX/Crz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1938233
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->d:LX/Crz;

    invoke-virtual {v0}, LX/Crz;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1938234
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1938235
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938236
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1938237
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    .line 1938238
    :cond_0
    :goto_0
    new-instance v0, LX/Cmz;

    new-instance v1, LX/Cn4;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->h:LX/Cju;

    invoke-direct {v1, v2}, LX/Cn4;-><init>(LX/Cju;)V

    invoke-direct {v0, v1, v7, v7, v7}, LX/Cmz;-><init>(LX/Cms;LX/Cmj;LX/Cmq;LX/Cmk;)V

    .line 1938239
    iput-object v0, p0, LX/Cod;->d:LX/Cmz;

    .line 1938240
    return-void

    .line 1938241
    :cond_1
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutDirection(I)V

    .line 1938242
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1938243
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1938244
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, LX/CtG;->setGravity(I)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1938245
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1938246
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1938247
    const-string v0, "extra_instant_articles_id"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938248
    const-string v0, "extra_instant_articles_canonical_url"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938249
    const-string v0, "com.android.browser.headers"

    invoke-static {}, LX/Cs3;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1938250
    const-string v0, "extra_instant_articles_referrer"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->q:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938251
    const-string v0, "extra_parent_article_click_source"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->g:LX/Chi;

    .line 1938252
    iget-object v3, v2, LX/Chi;->j:Ljava/lang/String;

    move-object v2, v3

    .line 1938253
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938254
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->f:LX/ClD;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/ClD;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1938255
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1938256
    const-string v2, "click_source_document_chaining_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1938257
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->f:LX/ClD;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v0

    .line 1938258
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1938259
    const-string v2, "click_source_document_depth"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1938260
    :cond_0
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1938261
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->i:LX/Cig;

    new-instance v2, LX/Cin;

    invoke-direct {v2}, LX/Cin;-><init>()V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1938262
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1938263
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1938264
    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1938265
    const-string v0, "article_ID"

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938266
    :cond_2
    const-string v0, "ia_source"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->q:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938267
    const-string v0, "position"

    iget v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938268
    const-string v2, "is_instant_article"

    invoke-static {p2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938269
    const-string v0, "click_source"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->q:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938270
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->t:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1938271
    const-string v0, "block_id"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->t:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938272
    :cond_3
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->b:LX/Ckw;

    invoke-virtual {v0, p1, v1}, LX/Ckw;->b(Ljava/lang/String;Ljava/util/Map;)V

    .line 1938273
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->b:LX/Ckw;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->t:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2}, LX/Ckw;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1938274
    return-void

    .line 1938275
    :cond_4
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 1938276
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1938277
    iget v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->p:I

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1938278
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a()V

    .line 1938279
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/RichTextView;->a()V

    .line 1938280
    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->t:Ljava/lang/String;

    .line 1938281
    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->s:Ljava/lang/String;

    .line 1938282
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->r:I

    .line 1938283
    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->q:Ljava/lang/String;

    .line 1938284
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->u:I

    .line 1938285
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1938286
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1938287
    const-string v1, "position"

    iget v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->r:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938288
    const-string v1, "num_related_articles"

    iget v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->u:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938289
    const-string v1, "click_source"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->q:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938290
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->t:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1938291
    const-string v1, "block_id"

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->t:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1938292
    :cond_0
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->b:LX/Ckw;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/RelatedArticleBlockViewImpl;->s:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/Ckw;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 1938293
    return-void
.end method
