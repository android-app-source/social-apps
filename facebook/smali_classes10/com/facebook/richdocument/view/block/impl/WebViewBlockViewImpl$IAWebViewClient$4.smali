.class public final Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/webkit/WebView;

.field public final synthetic b:LX/Cq4;


# direct methods
.method public constructor <init>(LX/Cq4;Landroid/webkit/WebView;)V
    .locals 0

    .prologue
    .line 1938899
    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$4;->b:LX/Cq4;

    iput-object p2, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$4;->a:Landroid/webkit/WebView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1938900
    :try_start_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$4;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1938901
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$4;->b:LX/Cq4;

    invoke-static {v0}, LX/Cq4;->a$redex0(LX/Cq4;)V

    .line 1938902
    return-void

    .line 1938903
    :catch_0
    move-exception v0

    .line 1938904
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/WebViewBlockViewImpl$IAWebViewClient$4;->b:LX/Cq4;

    iget-object v1, v1, LX/Cq4;->b:LX/CqA;

    iget-object v1, v1, LX/CqA;->c:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/CqA;->C:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_NPE"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "NPE while to attempting to stop loading the webview"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 1938905
    iput-object v0, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1938906
    move-object v0, v2

    .line 1938907
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method
