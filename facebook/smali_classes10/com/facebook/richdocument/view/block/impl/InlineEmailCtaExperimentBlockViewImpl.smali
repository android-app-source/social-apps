.class public Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;
.super LX/Cod;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Cng;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/richdocument/view/block/InlineEmailCtaExperimentBlockView;"
    }
.end annotation


# static fields
.field public static final y:Ljava/lang/String;

.field public static final z:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Ljava/lang/String;

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Z

.field private final E:I

.field public a:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Chi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Cj9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final i:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public final j:Landroid/widget/LinearLayout;

.field public final k:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final l:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final m:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public final o:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private final p:Lcom/facebook/richdocument/view/widget/RichTextView;

.field private final q:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final s:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final t:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public final u:Landroid/widget/LinearLayout;

.field public final v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final x:Lcom/facebook/richdocument/view/widget/RichTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1936652
    const-class v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->y:Ljava/lang/String;

    .line 1936653
    const-class v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->z:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 16

    .prologue
    .line 1936654
    invoke-direct/range {p0 .. p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 1936655
    const-class v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    move-object/from16 v0, p0

    invoke-static {v1, v0}, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 1936656
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->b:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1936657
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->b:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->A:Ljava/lang/String;

    .line 1936658
    :cond_0
    const v1, 0x7f0d1686

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->i:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1936659
    const v1, 0x7f0d1693

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->j:Landroid/widget/LinearLayout;

    .line 1936660
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, LX/Ck0;->a(Landroid/view/View;)V

    .line 1936661
    const v1, 0x7f0d168a

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936662
    const v1, 0x7f0d168c

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936663
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->getInnerRichTextView()LX/CtG;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/CtG;->setGravity(I)V

    .line 1936664
    const v6, 0x7f0d011b

    .line 1936665
    const v12, 0x7f0d011c

    .line 1936666
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936667
    const v1, 0x7f0d168d

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936668
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936669
    const v1, 0x7f0d168e

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936670
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->getInnerRichTextView()LX/CtG;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/CtG;->setGravity(I)V

    .line 1936671
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936672
    const v1, 0x7f0d168f

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 1936673
    const v1, 0x7f0d1690

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936674
    const v1, 0x7f0d1691

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936675
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual/range {v10 .. v15}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936676
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936677
    invoke-virtual/range {p0 .. p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04005e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 1936678
    invoke-virtual/range {p0 .. p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f04005f

    invoke-static {v1, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 1936679
    invoke-virtual/range {p0 .. p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    const v4, 0x7f040060

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    .line 1936680
    const v1, 0x7f0d1692

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936681
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    new-instance v5, LX/Cp6;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v3, v2, v4}, LX/Cp6;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    invoke-virtual {v1, v5}, Lcom/facebook/richdocument/view/widget/RichTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1936682
    const v1, 0x7f0d1694

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 1936683
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    const/4 v15, 0x0

    move v13, v12

    move v14, v12

    invoke-virtual/range {v10 .. v15}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936684
    const v1, 0x7f0d1695    # 1.875384E38f

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936685
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v1 .. v6}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936686
    const v1, 0x7f0d1696

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936687
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936688
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->s:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->getInnerRichTextView()LX/CtG;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/CtG;->setGravity(I)V

    .line 1936689
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->t:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->getInnerRichTextView()LX/CtG;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/CtG;->setGravity(I)V

    .line 1936690
    const v1, 0x7f0d1697

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->u:Landroid/widget/LinearLayout;

    .line 1936691
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->u:Landroid/widget/LinearLayout;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1936692
    const v1, 0x7f0d1698

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936693
    const v1, 0x7f0d169a

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->w:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1936694
    const v1, 0x7f0d169c

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->x:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1936695
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->x:Lcom/facebook/richdocument/view/widget/RichTextView;

    new-instance v2, LX/Cp7;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LX/Cp7;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;)V

    invoke-virtual {v1, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1936696
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->h:LX/Cju;

    const v2, 0x7f0d011e

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->E:I

    .line 1936697
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->x:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->E:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1936698
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->E:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, LX/CqS;->a(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1936699
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;

    invoke-static {p0}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v2

    check-cast v2, LX/Ck0;

    invoke-static {p0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v3

    check-cast v3, LX/0WJ;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v6

    check-cast v6, LX/Chv;

    invoke-static {p0}, LX/Chi;->a(LX/0QB;)LX/Chi;

    move-result-object v7

    check-cast v7, LX/Chi;

    invoke-static {p0}, LX/Cj9;->a(LX/0QB;)LX/Cj9;

    move-result-object v8

    check-cast v8, LX/Cj9;

    invoke-static {p0}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object p0

    check-cast p0, LX/Cju;

    iput-object v2, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->a:LX/Ck0;

    iput-object v3, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->b:LX/0WJ;

    iput-object v4, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->d:LX/03V;

    iput-object v6, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->e:LX/Chv;

    iput-object v7, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->f:LX/Chi;

    iput-object v8, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->g:LX/Cj9;

    iput-object p0, v1, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->h:LX/Cju;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1936700
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->D:Z

    if-eqz v0, :cond_0

    .line 1936701
    :goto_0
    return-void

    .line 1936702
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->g:LX/Cj9;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->A:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->f:LX/Chi;

    .line 1936703
    iget-object v3, v2, LX/Chi;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1936704
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;->C:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/Cj9;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1936705
    new-instance v1, LX/CpC;

    invoke-direct {v1, p0}, LX/CpC;-><init>(Lcom/facebook/richdocument/view/block/impl/InlineEmailCtaExperimentBlockViewImpl;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
