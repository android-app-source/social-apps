.class public Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/02k;
.implements LX/CoZ;


# static fields
.field public static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/8bO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/widget/CustomLinearLayout;

.field public g:Lcom/facebook/widget/CustomLinearLayout;

.field public h:Lcom/facebook/widget/CustomLinearLayout;

.field public i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public j:LX/CpO;

.field public k:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public l:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public m:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public n:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1937574
    const-class v0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/CpO;)V
    .locals 11

    .prologue
    .line 1937547
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1937548
    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->p:Landroid/view/View;

    .line 1937549
    iput-object p2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->j:LX/CpO;

    .line 1937550
    const-class v0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->a(Ljava/lang/Class;LX/02k;)V

    .line 1937551
    const/4 v3, 0x0

    .line 1937552
    const v1, 0x7f0d16ac

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937553
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0308c1

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1937554
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0308be

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1937555
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b5

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937556
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b4

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937557
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16bf

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1937558
    const v1, 0x7f0d16ae

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937559
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937560
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b1

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937561
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937562
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16ad

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->o:Landroid/view/View;

    .line 1937563
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16af

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/facebook/widget/CustomLinearLayout;

    .line 1937564
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d011b

    move v4, v3

    move v6, v3

    invoke-virtual/range {v1 .. v6}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937565
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1937566
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1937567
    int-to-float v2, v2

    const v4, 0x3f19999a    # 0.6f

    mul-float/2addr v2, v4

    float-to-int v2, v2

    move v2, v2

    .line 1937568
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1937569
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    move v4, v3

    move v5, v3

    move v6, v3

    invoke-virtual/range {v1 .. v6}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937570
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v6, 0x7f0d016e

    const v8, 0x7f0d016e

    move v7, v3

    move v9, v3

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937571
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    const v4, 0x7f0d011c

    move-object v2, v10

    move v5, v3

    move v6, v3

    invoke-virtual/range {v1 .. v6}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937572
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v6, 0x7f0d011b

    const v8, 0x7f0d011b

    const v9, 0x7f0d011c

    move v7, v3

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937573
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;

    invoke-static {v3}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v1

    check-cast v1, LX/Cju;

    invoke-static {v3}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v2

    check-cast v2, LX/Ck0;

    const/16 p0, 0x509

    invoke-static {v3, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v3}, LX/8bO;->b(LX/0QB;)LX/8bO;

    move-result-object v3

    check-cast v3, LX/8bO;

    iput-object v1, p1, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->a:LX/Cju;

    iput-object v2, p1, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iput-object p0, p1, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->c:LX/0Or;

    iput-object v3, p1, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->d:LX/8bO;

    return-void
.end method

.method public static b(Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1937533
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1937534
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937535
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937536
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1937537
    invoke-virtual {v0, p1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937538
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v3, 0x7f0d011b

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937539
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937540
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1937541
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/CtG;->setSingleLine(Z)V

    .line 1937542
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937543
    iput-boolean v2, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->m:Z

    .line 1937544
    :cond_0
    return-void
.end method

.method public static e(Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;)I
    .locals 3

    .prologue
    .line 1937530
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1937531
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->a:LX/Cju;

    const v2, 0x7f0d016e

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    .line 1937532
    int-to-float v0, v0

    const v2, 0x3f19999a    # 0.6f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(LX/CmW;)V
    .locals 9

    .prologue
    .line 1937460
    iget-object v0, p1, LX/CmW;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1937461
    const/4 v2, 0x0

    .line 1937462
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1937463
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937464
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937465
    iget-object v3, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v3

    .line 1937466
    invoke-virtual {v1, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937467
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937468
    iput-boolean v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->m:Z

    .line 1937469
    :cond_0
    iget-object v0, p1, LX/CmW;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1937470
    iget-object v1, p1, LX/CmW;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1937471
    const/4 v4, 0x0

    .line 1937472
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 1937473
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v2, v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937474
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937475
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1937476
    invoke-virtual {v2, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937477
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937478
    iput-boolean v4, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->m:Z

    .line 1937479
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v5, 0x7f0d011b

    move v6, v4

    move v7, v4

    invoke-virtual/range {v2 .. v7}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937480
    invoke-static {p0, v1}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b(Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;Ljava/lang/String;)V

    .line 1937481
    :goto_0
    iget-object v0, p1, LX/CmW;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1937482
    invoke-static {p0, v0}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b(Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;Ljava/lang/String;)V

    .line 1937483
    iget-object v0, p1, LX/CmW;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1937484
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1937485
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937486
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937487
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1937488
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/CtG;->setAllCaps(Z)V

    .line 1937489
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937490
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 1937491
    invoke-virtual {v2, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937492
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v4, 0x7f0d011b

    const v5, 0x7f0d011b

    const v6, 0x7f0d011b

    const v7, 0x7f0d011b

    invoke-virtual/range {v2 .. v7}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937493
    :cond_1
    iget-object v0, p1, LX/CmW;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1937494
    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    .line 1937495
    new-instance v3, LX/CpS;

    invoke-direct {v3, p0}, LX/CpS;-><init>(Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;)V

    .line 1937496
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v3}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1937497
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1937498
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1937499
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->e(Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1937500
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->e(Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1937501
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->d:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1937502
    iget-object v0, p1, LX/CmW;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1937503
    const/16 v8, 0x8

    const/4 v4, 0x0

    .line 1937504
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v2, v8}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937505
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->k:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v2, v8}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937506
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v2, v8}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937507
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    move v5, v4

    move v6, v4

    move v7, v4

    invoke-virtual/range {v2 .. v7}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937508
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->m:Lcom/facebook/richdocument/view/widget/RichTextView;

    move v5, v4

    move v6, v4

    move v7, v4

    invoke-virtual/range {v2 .. v7}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937509
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v3, 0x7f0d16b6

    invoke-virtual {v2, v3}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/CustomLinearLayout;

    .line 1937510
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    const v5, 0x7f0d011b

    move v6, v4

    move v7, v4

    invoke-virtual/range {v2 .. v7}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937511
    invoke-virtual {v3, v4}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1937512
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->d:LX/8bO;

    .line 1937513
    iget-object v5, v2, LX/8bO;->a:LX/0ad;

    sget-short v6, LX/2yD;->a:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v2, v5

    .line 1937514
    if-eqz v2, :cond_2

    .line 1937515
    const/16 v2, 0x13

    invoke-virtual {v3, v2}, Lcom/facebook/widget/CustomLinearLayout;->setGravity(I)V

    .line 1937516
    :cond_2
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v3, 0x7f0d16b7

    invoke-virtual {v2, v3}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937517
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16b8

    invoke-virtual {v3, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1937518
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1937519
    iget-object v5, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v5, v5

    .line 1937520
    invoke-virtual {v5, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937521
    invoke-virtual {v2, v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937522
    invoke-virtual {v3, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1937523
    :cond_3
    :goto_1
    return-void

    .line 1937524
    :cond_4
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->f:Lcom/facebook/widget/CustomLinearLayout;

    const v3, 0x7f0d16b3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937525
    invoke-virtual {v3, v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937526
    iget-object v2, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v2

    .line 1937527
    invoke-virtual {v2, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937528
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->b:LX/Ck0;

    const v5, 0x7f0d011b

    move v6, v4

    move v7, v4

    invoke-virtual/range {v2 .. v7}, LX/Ck0;->c(Landroid/view/View;IIII)V

    goto/16 :goto_0

    .line 1937529
    :cond_5
    invoke-virtual {v3, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1937545
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->q:Z

    .line 1937546
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1937456
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->i:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1937457
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->l:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1937458
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1937459
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1937455
    iget-boolean v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->q:Z

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1937454
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1937447
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->q:Z

    .line 1937448
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1937450
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->o:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1937451
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v1, LX/CpR;

    invoke-direct {v1, p0}, LX/CpR;-><init>(Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 1937452
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1937453
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1937449
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdSingleShareImageViewImpl;->p:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
