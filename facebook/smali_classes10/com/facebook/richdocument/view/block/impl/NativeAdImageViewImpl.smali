.class public Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/02k;
.implements LX/CoZ;


# static fields
.field public static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/Cju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ck0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8bO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Coa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/widget/CustomLinearLayout;

.field public h:Lcom/facebook/widget/CustomLinearLayout;

.field public i:Lcom/facebook/widget/CustomLinearLayout;

.field public j:Lcom/facebook/widget/CustomLinearLayout;

.field public k:Lcom/facebook/widget/CustomLinearLayout;

.field public l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public m:LX/CpO;

.field public n:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public o:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public p:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public q:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public r:Lcom/facebook/richdocument/view/widget/RichTextView;

.field public s:Landroid/view/View;

.field private t:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1937317
    const-class v0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/CpO;)V
    .locals 13

    .prologue
    .line 1937124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1937125
    iput-object p1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->t:Landroid/view/View;

    .line 1937126
    iput-object p2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->m:LX/CpO;

    .line 1937127
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p2

    move-object v2, p0

    check-cast v2, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;

    invoke-static {p2}, LX/Cjv;->a(LX/0QB;)LX/Cjv;

    move-result-object v3

    check-cast v3, LX/Cju;

    invoke-static {p2}, LX/Ck0;->a(LX/0QB;)LX/Ck0;

    move-result-object v4

    check-cast v4, LX/Ck0;

    invoke-static {p2}, LX/8bO;->b(LX/0QB;)LX/8bO;

    move-result-object v5

    check-cast v5, LX/8bO;

    const/16 v6, 0x509

    invoke-static {p2, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v0, 0x323a

    invoke-static {p2, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p2

    iput-object v3, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->a:LX/Cju;

    iput-object v4, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iput-object v5, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    iput-object v6, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->d:LX/0Or;

    iput-object p2, v2, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->e:LX/0Ot;

    .line 1937128
    const v1, 0x7f0d16ac

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937129
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0308c1

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1937130
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0308be

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1937131
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v1}, LX/8bO;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1937132
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LX/Coa;->a(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    .line 1937133
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, LX/Coa;->a(Landroid/content/Context;Landroid/view/ViewGroup;I)V

    .line 1937134
    :cond_0
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v1}, LX/8bO;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1937135
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0308c4

    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1937136
    const v1, 0x7f0d16c6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->j:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937137
    :cond_1
    const v1, 0x7f0d16b9

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937138
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b5

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937139
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b4

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937140
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16bf

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1937141
    const v1, 0x7f0d16ae

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937142
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937143
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b1

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937144
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16b0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->k:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937145
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16ad

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->s:Landroid/view/View;

    .line 1937146
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v2, 0x7f0d16bd

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937147
    const/4 v6, 0x0

    .line 1937148
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16af

    invoke-virtual {v4, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/facebook/widget/CustomLinearLayout;

    .line 1937149
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->k:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f0d011b

    move v7, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937150
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    const v7, 0x7f0d011c

    move-object v5, v10

    move v8, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937151
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const v9, 0x7f0d011b

    const v11, 0x7f0d011b

    const v12, 0x7f0d011c

    move v10, v6

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937152
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v4}, LX/8bO;->d()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1937153
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->j:Lcom/facebook/widget/CustomLinearLayout;

    const v7, 0x7f0d0172

    move v8, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937154
    :cond_2
    return-void
.end method

.method public static a(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;III)V
    .locals 1

    .prologue
    .line 1937313
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundResource(I)V

    .line 1937314
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/CustomLinearLayout;->setBackgroundResource(I)V

    .line 1937315
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundResource(I)V

    .line 1937316
    return-void
.end method

.method public static b(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 1937283
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1937284
    :goto_0
    return-void

    .line 1937285
    :cond_0
    const/4 v6, 0x0

    .line 1937286
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16ba

    invoke-virtual {v4, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/facebook/widget/CustomLinearLayout;

    .line 1937287
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d16bb

    invoke-virtual {v4, v5}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1937288
    if-eqz v10, :cond_1

    if-eqz v5, :cond_1

    .line 1937289
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    const v8, 0x7f0d011b

    move v7, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937290
    invoke-virtual {v10, v6}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1937291
    invoke-virtual {v5, v6}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1937292
    :cond_1
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    if-eqz v4, :cond_2

    .line 1937293
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    const v9, 0x7f0d011b

    const v10, 0x7f0d011b

    const v11, 0x7f0d011b

    const v12, 0x7f0d011b

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937294
    :cond_2
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    if-eqz v4, :cond_3

    .line 1937295
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937296
    iget-object v5, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, v5

    .line 1937297
    const v5, 0x800015

    invoke-virtual {v4, v5}, LX/CtG;->setGravity(I)V

    .line 1937298
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 1937299
    const v5, 0x3e4ccccd    # 0.2f

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1937300
    const/16 v5, 0x10

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1937301
    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v5, v4}, Lcom/facebook/richdocument/view/widget/RichTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1937302
    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v5, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->r:Lcom/facebook/richdocument/view/widget/RichTextView;

    move v7, v6

    move v8, v6

    move v9, v6

    invoke-virtual/range {v4 .. v9}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937303
    :cond_3
    invoke-static {p1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    .line 1937304
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16bb

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1937305
    if-eqz v0, :cond_4

    .line 1937306
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v3, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1937307
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1937308
    :cond_4
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v1, 0x7f0d16bc

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937309
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1937310
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v1

    .line 1937311
    invoke-virtual {v1, p2}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937312
    :cond_5
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public static c(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1937276
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1937277
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937278
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 1937279
    invoke-virtual {v0, p1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937280
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937281
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->n:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v3, 0x7f0d011b

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937282
    :cond_0
    return-void
.end method

.method public static e(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;)I
    .locals 4

    .prologue
    .line 1937271
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1937272
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->a:LX/Cju;

    const v2, 0x7f0d0120

    invoke-interface {v1, v2}, LX/Cju;->c(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->a:LX/Cju;

    const v3, 0x7f0d0120

    invoke-interface {v2, v3}, LX/Cju;->c(I)I

    move-result v2

    add-int/2addr v1, v2

    .line 1937273
    sub-int v1, v0, v1

    .line 1937274
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v2}, LX/8bO;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1937275
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static h(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1937250
    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b12ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 1937251
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->a:LX/Cju;

    const v1, 0x7f0d011b

    invoke-interface {v0, v1}, LX/Cju;->b(I)F

    move-result v0

    float-to-int v0, v0

    .line 1937252
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v1}, LX/8bO;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1937253
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->a:LX/Cju;

    const v1, 0x7f0d0121

    invoke-interface {v0, v1}, LX/Cju;->b(I)F

    move-result v0

    float-to-int v7, v0

    .line 1937254
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->a:LX/Cju;

    const v1, 0x7f0d0122

    invoke-interface {v0, v1}, LX/Cju;->b(I)F

    move-result v0

    float-to-int v6, v0

    .line 1937255
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d011c

    move v3, v2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937256
    :goto_0
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1937257
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937258
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1937259
    neg-int v1, v8

    invoke-virtual {v0, v7, v2, v6, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1937260
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->i:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1937261
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1937262
    neg-int v1, v8

    invoke-virtual {v0, v7, v1, v6, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1937263
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1937264
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2, v8, v2, v8}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 1937265
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1937266
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1937267
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1937268
    iget-object v3, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->j:Lcom/facebook/widget/CustomLinearLayout;

    const v5, 0x7f0d0121

    const v6, 0x7f0d0172

    const v7, 0x7f0d0122

    move v8, v2

    invoke-virtual/range {v3 .. v8}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937269
    :cond_0
    :goto_1
    return-void

    .line 1937270
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v8, v2, v8, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    goto :goto_1

    :cond_2
    move v6, v0

    move v7, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CmW;)V
    .locals 14

    .prologue
    .line 1937172
    iget-object v0, p1, LX/CmW;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1937173
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1937174
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937175
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937176
    iget-object v2, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, v2

    .line 1937177
    invoke-virtual {v1, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937178
    :cond_0
    iget-object v0, p1, LX/CmW;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1937179
    iget-object v1, p1, LX/CmW;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1937180
    const/4 v9, 0x0

    .line 1937181
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 1937182
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v9}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937183
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937184
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1937185
    invoke-virtual {v7, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937186
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v10, 0x7f0d011b

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937187
    invoke-static {p0, v1}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;Ljava/lang/String;)V

    .line 1937188
    :goto_0
    iget-object v0, p1, LX/CmW;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1937189
    invoke-static {p0, v0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;Ljava/lang/String;)V

    .line 1937190
    iget-object v0, p1, LX/CmW;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1937191
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1937192
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937193
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937194
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1937195
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, LX/CtG;->setAllCaps(Z)V

    .line 1937196
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937197
    iget-object v8, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v8

    .line 1937198
    invoke-virtual {v7, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937199
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    const v9, 0x7f0d011b

    const v10, 0x7f0d011b

    const v11, 0x7f0d011b

    const v12, 0x7f0d011b

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937200
    :cond_1
    iget-object v0, p1, LX/CmW;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1937201
    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v2

    .line 1937202
    new-instance v3, LX/CpQ;

    invoke-direct {v3, p0}, LX/CpQ;-><init>(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;)V

    .line 1937203
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v4, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    iget-object v4, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, v3}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1937204
    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1937205
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1937206
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->e(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1937207
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1937208
    invoke-static {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->e(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;)I

    move-result v2

    int-to-float v2, v2

    const v3, 0x3ff47ae1    # 1.91f

    div-float/2addr v2, v3

    float-to-int v2, v2

    move v2, v2

    .line 1937209
    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1937210
    iget-object v0, p1, LX/CmW;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1937211
    iget-object v1, p1, LX/CmW;->l:Ljava/lang/String;

    move-object v1, v1

    .line 1937212
    iget-object v2, p1, LX/CmW;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1937213
    invoke-static {p0, v1, v2}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;Ljava/lang/String;Ljava/lang/String;)V

    .line 1937214
    const/16 v13, 0x8

    const/4 v9, 0x0

    .line 1937215
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v7}, LX/8bO;->b()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1937216
    :goto_1
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v0}, LX/8bO;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1937217
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Coa;

    invoke-virtual {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->j:Lcom/facebook/widget/CustomLinearLayout;

    .line 1937218
    iget-object v3, p1, LX/CmW;->s:Lcom/facebook/graphql/model/GraphQLFeedback;

    move-object v3, v3

    .line 1937219
    iget-object v4, p1, LX/CmW;->t:Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-object v4, v4

    .line 1937220
    iget-object v5, p1, LX/CmW;->m:Ljava/lang/String;

    move-object v5, v5

    .line 1937221
    iget-object v6, p1, LX/CmW;->n:Ljava/lang/String;

    move-object v6, v6

    .line 1937222
    invoke-virtual/range {v0 .. v6}, LX/Coa;->a(Landroid/content/Context;Lcom/facebook/widget/CustomLinearLayout;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;Ljava/lang/String;Ljava/lang/String;)V

    .line 1937223
    :cond_2
    return-void

    .line 1937224
    :cond_3
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f0d16b3

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937225
    invoke-virtual {v8, v9}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937226
    iget-object v7, v8, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v7, v7

    .line 1937227
    invoke-virtual {v7, v1}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937228
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    const v10, 0x7f0d011b

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->c(Landroid/view/View;IIII)V

    goto/16 :goto_0

    .line 1937229
    :cond_4
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->q:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v13}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937230
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v7, v13}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937231
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->k:Lcom/facebook/widget/CustomLinearLayout;

    move v10, v9

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937232
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->p:Lcom/facebook/richdocument/view/widget/RichTextView;

    move v10, v9

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->a(Landroid/view/View;IIII)V

    .line 1937233
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f0d16b6

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/widget/CustomLinearLayout;

    .line 1937234
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->b:LX/Ck0;

    const v10, 0x7f0d011b

    move v11, v9

    move v12, v9

    invoke-virtual/range {v7 .. v12}, LX/Ck0;->c(Landroid/view/View;IIII)V

    .line 1937235
    invoke-virtual {v8, v9}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1937236
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v7}, LX/8bO;->i()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1937237
    const/16 v7, 0x13

    invoke-virtual {v8, v7}, Lcom/facebook/widget/CustomLinearLayout;->setGravity(I)V

    .line 1937238
    :cond_5
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v8, 0x7f0d16b7

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 1937239
    iget-object v8, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->g:Lcom/facebook/widget/CustomLinearLayout;

    const v10, 0x7f0d16b8

    invoke-virtual {v8, v10}, Lcom/facebook/widget/CustomLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1937240
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_6

    .line 1937241
    iget-object v10, v7, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v10, v10

    .line 1937242
    invoke-virtual {v10, v0}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 1937243
    invoke-virtual {v7, v9}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 1937244
    invoke-virtual {v8, v9}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1937245
    :goto_2
    iget-object v7, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->c:LX/8bO;

    invoke-virtual {v7}, LX/8bO;->f()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1937246
    const v7, 0x7f02106f

    invoke-static {p0, v9, v9, v7}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;III)V

    .line 1937247
    :goto_3
    invoke-static {p0}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;)V

    goto/16 :goto_1

    .line 1937248
    :cond_6
    invoke-virtual {v8, v13}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_2

    .line 1937249
    :cond_7
    const v7, 0x7f02106c

    const v8, 0x7f02106d

    const v9, 0x7f02106f

    invoke-static {p0, v7, v8, v9}, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->a(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;III)V

    goto :goto_3
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1937170
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->m:LX/CpO;

    invoke-virtual {v0}, LX/CpO;->f()V

    .line 1937171
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1937166
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1937167
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->o:Lcom/facebook/richdocument/view/widget/RichTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1937168
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1937169
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1937161
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 1937162
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 1937163
    iget-object v1, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1937164
    const/4 v0, 0x1

    .line 1937165
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1937160
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1937159
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1937155
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->s:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1937156
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v1, LX/CpP;

    invoke-direct {v1, p0}, LX/CpP;-><init>(Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 1937157
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->h:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1937158
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1937123
    iget-object v0, p0, Lcom/facebook/richdocument/view/block/impl/NativeAdImageViewImpl;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method
