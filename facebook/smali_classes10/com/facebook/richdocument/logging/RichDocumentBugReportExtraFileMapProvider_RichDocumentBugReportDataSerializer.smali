.class public Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider_RichDocumentBugReportDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1931848
    const-class v0, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;

    new-instance v1, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider_RichDocumentBugReportDataSerializer;

    invoke-direct {v1}, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider_RichDocumentBugReportDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1931849
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1931847
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1931841
    if-nez p0, :cond_0

    .line 1931842
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1931843
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1931844
    invoke-static {p0, p1, p2}, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider_RichDocumentBugReportDataSerializer;->b(Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;LX/0nX;LX/0my;)V

    .line 1931845
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1931846
    return-void
.end method

.method private static b(Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1931839
    const-string v0, "last_article_debug_info"

    iget-object v1, p0, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;->mLastARticleDebugInfo:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1931840
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1931838
    check-cast p1, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;

    invoke-static {p1, p2, p3}, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider_RichDocumentBugReportDataSerializer;->a(Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;LX/0nX;LX/0my;)V

    return-void
.end method
