.class public final Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider_RichDocumentBugReportDataSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mLastARticleDebugInfo:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_article_debug_info"
    .end annotation
.end field


# direct methods
.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1931819
    const-class v0, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider_RichDocumentBugReportDataSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1931818
    new-instance v0, LX/Cky;

    invoke-direct {v0}, LX/Cky;-><init>()V

    sput-object v0, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1931815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931816
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;->mLastARticleDebugInfo:Ljava/lang/String;

    .line 1931817
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1931809
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1931810
    iput-object p1, p0, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;->mLastARticleDebugInfo:Ljava/lang/String;

    .line 1931811
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1931814
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1931812
    iget-object v0, p0, Lcom/facebook/richdocument/logging/RichDocumentBugReportExtraFileMapProvider$RichDocumentBugReportData;->mLastARticleDebugInfo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1931813
    return-void
.end method
