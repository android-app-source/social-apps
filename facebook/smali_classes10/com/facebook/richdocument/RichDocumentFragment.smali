.class public abstract Lcom/facebook/richdocument/RichDocumentFragment;
.super Lcom/facebook/richdocument/view/carousel/PageableFragment;
.source ""

# interfaces
.implements LX/0hF;


# instance fields
.field public n:LX/ChL;

.field private o:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1928404
    invoke-direct {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public S_()Z
    .locals 1

    .prologue
    .line 1928409
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0}, LX/ChL;->c()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 1928410
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0}, LX/ChL;->j()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1928411
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1928412
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 3

    .prologue
    .line 1928413
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->o:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 1928414
    new-instance v0, LX/ChJ;

    invoke-super {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/ChJ;-><init>(Landroid/content/Context;)V

    .line 1928415
    const-string v1, "loggingClass"

    .line 1928416
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    move-object v2, v2

    .line 1928417
    invoke-virtual {v0, v1, v2}, LX/ChJ;->setProperty(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1928418
    iput-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->o:Landroid/content/Context;

    .line 1928419
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->o:Landroid/content/Context;

    return-object v0
.end method

.method public abstract k()LX/ChL;
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1928420
    invoke-super {p0, p1}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onAttach(Landroid/content/Context;)V

    .line 1928421
    invoke-virtual {p0}, Lcom/facebook/richdocument/RichDocumentFragment;->k()LX/ChL;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    .line 1928422
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0, p0}, LX/ChL;->a(Lcom/facebook/richdocument/RichDocumentFragment;)V

    .line 1928423
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, LX/ChL;->b(Landroid/content/Context;)V

    .line 1928424
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0, p1}, LX/ChL;->a(Landroid/content/Context;)V

    .line 1928425
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    .line 1928426
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1928427
    invoke-interface {v0, v1}, LX/ChL;->c(Landroid/os/Bundle;)V

    .line 1928428
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1928429
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0}, LX/ChL;->h()V

    .line 1928430
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1bcf4ade

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1928405
    invoke-super {p0, p1}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1928406
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    if-eqz v1, :cond_0

    .line 1928407
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v1, p1}, LX/ChL;->a(Landroid/os/Bundle;)V

    .line 1928408
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x555c6fb4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x510b3aae

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1928403
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    const v2, 0x2820de9a

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0, p1, p2, p3}, LX/ChL;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4ba3fb02

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1928399
    invoke-super {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onDestroyView()V

    .line 1928400
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    if-eqz v1, :cond_0

    .line 1928401
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v1}, LX/ChL;->g()V

    .line 1928402
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x10790df7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLowMemory()V
    .locals 1

    .prologue
    .line 1928396
    invoke-super {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onLowMemory()V

    .line 1928397
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0}, LX/ChL;->i()V

    .line 1928398
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x31f76581

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1928392
    invoke-super {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onPause()V

    .line 1928393
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    if-eqz v1, :cond_0

    .line 1928394
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v1}, LX/ChL;->f()V

    .line 1928395
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x10d65e3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2a9ba984

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1928388
    invoke-super {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onResume()V

    .line 1928389
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    if-eqz v1, :cond_0

    .line 1928390
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v1}, LX/ChL;->e()V

    .line 1928391
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x7a76ecd2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1928385
    invoke-super {p0, p1}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1928386
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0, p1}, LX/ChL;->b(Landroid/os/Bundle;)V

    .line 1928387
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x59ecbf50

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1928381
    invoke-super {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onStart()V

    .line 1928382
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    if-eqz v1, :cond_0

    .line 1928383
    iget-object v1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v1}, LX/ChL;->mB_()V

    .line 1928384
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x2c7105be

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x61c6f4dd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1928379
    invoke-super {p0}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onStop()V

    .line 1928380
    const/16 v1, 0x2b

    const v2, 0x58ed7932

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1928373
    invoke-super {p0, p1, p2}, Lcom/facebook/richdocument/view/carousel/PageableFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1928374
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    if-eqz v0, :cond_0

    .line 1928375
    iget-object v0, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {v0, p1, p2}, LX/ChL;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1928376
    :cond_0
    new-instance v0, LX/Chd;

    invoke-direct {v0, p0}, LX/Chd;-><init>(Lcom/facebook/richdocument/RichDocumentFragment;)V

    .line 1928377
    iget-object p1, p0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {p1, v0}, LX/ChL;->a(LX/ChK;)V

    .line 1928378
    return-void
.end method
