.class public abstract Lcom/facebook/richdocument/BaseRichDocumentActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/richdocument/RichDocumentFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1927843
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lcom/facebook/richdocument/RichDocumentFragment;
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1927844
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1927845
    invoke-virtual {p0, v0, v0}, Lcom/facebook/richdocument/BaseRichDocumentActivity;->overridePendingTransition(II)V

    .line 1927846
    invoke-virtual {p0}, Lcom/facebook/richdocument/BaseRichDocumentActivity;->a()Lcom/facebook/richdocument/RichDocumentFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/richdocument/BaseRichDocumentActivity;->p:Lcom/facebook/richdocument/RichDocumentFragment;

    .line 1927847
    invoke-virtual {p0}, Lcom/facebook/richdocument/BaseRichDocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1927848
    invoke-virtual {p0}, Lcom/facebook/richdocument/BaseRichDocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "extra_instant_articles_click_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1927849
    const-string v1, "extra_instant_articles_click_url"

    invoke-virtual {p0}, Lcom/facebook/richdocument/BaseRichDocumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1927850
    :cond_0
    iget-object v1, p0, Lcom/facebook/richdocument/BaseRichDocumentActivity;->p:Lcom/facebook/richdocument/RichDocumentFragment;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1927851
    iget-object v0, p0, Lcom/facebook/richdocument/BaseRichDocumentActivity;->p:Lcom/facebook/richdocument/RichDocumentFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "rich_document_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1927852
    return-void
.end method

.method public final onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1927853
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 1927854
    iget-object v0, p0, Lcom/facebook/richdocument/BaseRichDocumentActivity;->p:Lcom/facebook/richdocument/RichDocumentFragment;

    new-instance v1, LX/ChI;

    invoke-direct {v1, p0}, LX/ChI;-><init>(Lcom/facebook/richdocument/BaseRichDocumentActivity;)V

    .line 1927855
    iget-object p0, v0, Lcom/facebook/richdocument/RichDocumentFragment;->n:LX/ChL;

    invoke-interface {p0, v1}, LX/ChL;->a(LX/ChI;)V

    .line 1927856
    return-void
.end method
