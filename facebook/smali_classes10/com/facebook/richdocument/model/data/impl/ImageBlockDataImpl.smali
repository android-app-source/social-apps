.class public Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;
.super LX/Cm9;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Clw;
.implements LX/Cm0;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/CjG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/richdocument/optional/OptionalSphericalPhoto;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:LX/8Yr;

.field private final e:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field private final f:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

.field private final g:Z

.field private final h:Z

.field private final i:Ljava/lang/String;

.field private j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1933288
    const-class v0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/CmG;)V
    .locals 1

    .prologue
    .line 1933293
    invoke-direct {p0, p1}, LX/Cm9;-><init>(LX/Cm8;)V

    .line 1933294
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->j:Z

    .line 1933295
    iget-object v0, p1, LX/CmG;->a:LX/8Yr;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->d:LX/8Yr;

    .line 1933296
    iget-object v0, p1, LX/CmG;->b:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->e:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1933297
    iget-boolean v0, p1, LX/CmG;->e:Z

    iput-boolean v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->g:Z

    .line 1933298
    iget-boolean v0, p1, LX/CmG;->f:Z

    iput-boolean v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->h:Z

    .line 1933299
    iget-object v0, p1, LX/CmG;->c:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->f:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    .line 1933300
    iget-object v0, p1, LX/CmG;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->i:Ljava/lang/String;

    .line 1933301
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;

    invoke-static {v1}, LX/CjG;->a(LX/0QB;)LX/CjG;

    move-result-object v0

    check-cast v0, LX/CjG;

    const/16 p0, 0x3226

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v0, p1, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->a:LX/CjG;

    iput-object v1, p1, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->b:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a()LX/8Yr;
    .locals 1

    .prologue
    .line 1933292
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->d:LX/8Yr;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1933289
    const-class v0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;

    invoke-static {v0, p0, p1}, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1933290
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->a:LX/CjG;

    invoke-virtual {p0}, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->a()LX/8Yr;

    move-result-object v1

    invoke-interface {v1}, LX/8Yr;->eg_()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->i:Ljava/lang/String;

    sget-object v3, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/CjG;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1933291
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1933279
    iput-boolean p1, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->j:Z

    .line 1933280
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1933287
    invoke-virtual {p0}, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->a()LX/8Yr;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->b:LX/0Ot;

    invoke-static {v0, v1}, LX/CmG;->a(LX/8Yr;LX/0Ot;)Z

    move-result v0

    return v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 1933302
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method public final iY_()Z
    .locals 1

    .prologue
    .line 1933286
    iget-boolean v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->h:Z

    return v0
.end method

.method public final iZ_()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1

    .prologue
    .line 1933285
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->f:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method

.method public final ja_()Z
    .locals 1

    .prologue
    .line 1933284
    iget-boolean v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final jb_()I
    .locals 1

    .prologue
    .line 1933283
    const/4 v0, 0x5

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1933282
    iget-boolean v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->g:Z

    return v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;
    .locals 1

    .prologue
    .line 1933281
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/ImageBlockDataImpl;->e:Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    return-object v0
.end method
