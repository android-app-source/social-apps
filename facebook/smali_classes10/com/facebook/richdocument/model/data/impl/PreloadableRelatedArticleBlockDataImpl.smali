.class public Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;
.super LX/CmB;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Clr;
.implements LX/Clu;
.implements LX/Cm0;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/8bW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Z

.field public final l:I

.field public final m:Ljava/lang/String;

.field public final n:LX/Cm2;

.field public final o:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final p:Ljava/lang/String;

.field public final q:I

.field public final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1933457
    const-class v0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/CmX;)V
    .locals 1

    .prologue
    .line 1933439
    invoke-direct {p0, p1}, LX/CmB;-><init>(LX/CmA;)V

    .line 1933440
    iget-object v0, p1, LX/CmX;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->d:Ljava/lang/String;

    .line 1933441
    iget-object v0, p1, LX/CmX;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->e:Ljava/lang/String;

    .line 1933442
    iget-object v0, p1, LX/CmX;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->f:Ljava/lang/String;

    .line 1933443
    iget-object v0, p1, LX/CmX;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->g:Ljava/lang/String;

    .line 1933444
    iget-object v0, p1, LX/CmX;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->h:Ljava/lang/String;

    .line 1933445
    iget-object v0, p1, LX/CmX;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->i:Ljava/lang/String;

    .line 1933446
    iget-object v0, p1, LX/CmX;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->j:Ljava/lang/String;

    .line 1933447
    iget-boolean v0, p1, LX/CmX;->h:Z

    iput-boolean v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->k:Z

    .line 1933448
    iget v0, p1, LX/CmX;->i:I

    iput v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->l:I

    .line 1933449
    iget-object v0, p1, LX/CmX;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->m:Ljava/lang/String;

    .line 1933450
    iget-object v0, p1, LX/CmX;->k:LX/Cm2;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->n:LX/Cm2;

    .line 1933451
    iget-object v0, p1, LX/CmX;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->o:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1933452
    iget-object v0, p1, LX/CmX;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->p:Ljava/lang/String;

    .line 1933453
    iget v0, p1, LX/CmX;->n:I

    iput v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->q:I

    .line 1933454
    iget-object v0, p1, LX/CmX;->o:Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->r:Ljava/util/List;

    .line 1933455
    iget-object v0, p1, LX/CmX;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->s:Ljava/lang/String;

    .line 1933456
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    invoke-static {p0}, LX/8bW;->b(LX/0QB;)LX/8bW;

    move-result-object v0

    check-cast v0, LX/8bW;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object p0

    check-cast p0, LX/Chv;

    iput-object v0, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->a:LX/8bW;

    iput-object p0, p1, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->b:LX/Chv;

    return-void
.end method


# virtual methods
.method public final b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1933430
    const-class v0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;

    invoke-static {v0, p0, p1}, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1933431
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->b:LX/Chv;

    if-eqz v0, :cond_0

    .line 1933432
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->b:LX/Chv;

    new-instance v1, LX/CiY;

    iget-object v2, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->m:Ljava/lang/String;

    sget-object v3, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v3}, LX/CiY;-><init>(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1933433
    :cond_0
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1933434
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->a:LX/8bW;

    iget-object v1, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->g:Ljava/lang/String;

    sget-object v2, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/8bW;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;

    .line 1933435
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->h:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1933436
    iget-object v0, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->a:LX/8bW;

    iget-object v1, p0, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->h:Ljava/lang/String;

    sget-object v2, Lcom/facebook/richdocument/model/data/impl/PreloadableRelatedArticleBlockDataImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/8bW;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)LX/1ca;

    .line 1933437
    :cond_2
    return-void
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 1933438
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->RELATED_ARTICLES:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method
