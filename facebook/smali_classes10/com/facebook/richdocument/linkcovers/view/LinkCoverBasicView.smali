.class public Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1931337
    const-class v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1931334
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1931335
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->a()V

    .line 1931336
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1931331
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1931332
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->a()V

    .line 1931333
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1931328
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1931329
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->a()V

    .line 1931330
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1931322
    const v0, 0x7f0309f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1931323
    const v0, 0x7f0d192d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1931324
    const v0, 0x7f0d192e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->c:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    .line 1931325
    const v0, 0x7f0d192f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1931326
    const v0, 0x7f0d1930

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1931327
    return-void
.end method


# virtual methods
.method public getBylineView()Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;
    .locals 1

    .prologue
    .line 1931321
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->c:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    return-object v0
.end method

.method public getCoverImageView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1

    .prologue
    .line 1931320
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public getDescriptionTextView()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 1931319
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->e:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public getTitleTextView()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 1931312
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->d:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public setDescriptionText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1931317
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1931318
    return-void
.end method

.method public setImageParams(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1931315
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1931316
    return-void
.end method

.method public setTitleText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1931313
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverBasicView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1931314
    return-void
.end method
