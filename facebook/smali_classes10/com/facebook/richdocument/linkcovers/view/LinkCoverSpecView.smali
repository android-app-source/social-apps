.class public Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1931424
    const-class v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1931416
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1931417
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a:Ljava/util/List;

    .line 1931418
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a()V

    .line 1931419
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1931420
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1931421
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a:Ljava/util/List;

    .line 1931422
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a()V

    .line 1931423
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1931425
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1931426
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a:Ljava/util/List;

    .line 1931427
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a()V

    .line 1931428
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1931455
    const v0, 0x7f0309f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1931456
    const v0, 0x7f0d192d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1931457
    const v0, 0x7f0d192e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->d:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    .line 1931458
    const v0, 0x7f0d192f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1931459
    const v0, 0x7f0d1930

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1931460
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 1931429
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1931430
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 1931431
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1931432
    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1931433
    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1931434
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1931435
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1931436
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1931437
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1931438
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 1931439
    :cond_1
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1931440
    return-void
.end method

.method private b(LX/CkL;)V
    .locals 8

    .prologue
    .line 1931441
    iget-object v0, p1, LX/CkL;->q:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1931442
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CkP;

    .line 1931443
    new-instance v2, Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1931444
    iget-object v3, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1931445
    invoke-virtual {p0, v2}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->addView(Landroid/view/View;)V

    .line 1931446
    iget v3, v0, LX/CkP;->b:I

    .line 1931447
    iget-object v4, p1, LX/CkL;->q:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lt v3, v4, :cond_1

    .line 1931448
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 1931449
    :goto_1
    move-object v3, v4

    .line 1931450
    invoke-static {v2, v3}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 1931451
    iget v0, v0, LX/CkP;->a:I

    invoke-static {v2, v0}, LX/8ba;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 1931452
    :cond_0
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->c()V

    .line 1931453
    return-void

    .line 1931454
    :cond_1
    iget-object v4, p1, LX/CkL;->q:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CkP;

    iget-object v4, v4, LX/CkO;->c:Ljava/lang/String;

    new-instance v5, LX/Cka;

    iget v6, p1, LX/CkL;->w:F

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, LX/Cka;-><init>(FF)V

    const/4 v6, 0x1

    invoke-static {p1, v4, v5, v6}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v4

    goto :goto_1
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1931392
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->d:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    invoke-virtual {v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->bringToFront()V

    .line 1931393
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->bringToFront()V

    .line 1931394
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->bringToFront()V

    .line 1931395
    return-void
.end method


# virtual methods
.method public final a(LX/CkL;)V
    .locals 7

    .prologue
    .line 1931396
    invoke-virtual {p1}, LX/CkL;->b()Landroid/graphics/RectF;

    move-result-object v0

    .line 1931397
    iget-object v1, p1, LX/CkL;->j:LX/CkS;

    iget-object v1, v1, LX/CkO;->c:Ljava/lang/String;

    iget-object v2, p1, LX/CkL;->F:LX/Cka;

    const/4 v3, 0x1

    invoke-static {p1, v1, v2, v3}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v1

    move-object v1, v1

    .line 1931398
    iget-object v2, p1, LX/CkL;->k:LX/CkS;

    iget-object v2, v2, LX/CkO;->c:Ljava/lang/String;

    iget-object v3, p1, LX/CkL;->z:LX/Cka;

    const/4 v4, 0x1

    invoke-static {p1, v2, v3, v4}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v2

    move-object v2, v2

    .line 1931399
    iget-object v3, p1, LX/CkL;->p:LX/CkO;

    iget-object v3, v3, LX/CkO;->c:Ljava/lang/String;

    iget-object v4, p1, LX/CkL;->D:LX/Cka;

    const/4 v5, 0x1

    invoke-static {p1, v3, v4, v5}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v3

    move-object v3, v3

    .line 1931400
    iget-object v4, p1, LX/CkL;->u:LX/CkS;

    iget-object v4, v4, LX/CkO;->c:Ljava/lang/String;

    iget-object v5, p1, LX/CkL;->B:LX/Cka;

    const/4 v6, 0x1

    invoke-static {p1, v4, v5, v6}, LX/CkL;->a(LX/CkL;Ljava/lang/String;LX/Cka;Z)Landroid/graphics/RectF;

    move-result-object v4

    move-object v4, v4

    .line 1931401
    iget v5, v3, Landroid/graphics/RectF;->right:F

    iget v4, v4, Landroid/graphics/RectF;->left:F

    sub-float v4, v5, v4

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 1931402
    iget-object v4, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v4, v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 1931403
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0, v1}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 1931404
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0, v2}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 1931405
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->d:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    invoke-static {v0, v3}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 1931406
    iget-object v0, p1, LX/CkL;->j:LX/CkS;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/CkL;->j:LX/CkS;

    iget v0, v0, LX/CkS;->a:I

    :goto_0
    move v0, v0

    .line 1931407
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->setHeadlineTextColor(I)V

    .line 1931408
    iget-object v0, p1, LX/CkL;->k:LX/CkS;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/CkL;->k:LX/CkS;

    iget v0, v0, LX/CkS;->a:I

    :goto_1
    move v0, v0

    .line 1931409
    invoke-virtual {p0, v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->setDescriptionTextColor(I)V

    .line 1931410
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->d:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    .line 1931411
    iget-object v1, p1, LX/CkL;->u:LX/CkS;

    if-eqz v1, :cond_2

    iget-object v1, p1, LX/CkL;->u:LX/CkS;

    iget v1, v1, LX/CkS;->a:I

    :goto_2
    move v1, v1

    .line 1931412
    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->setAuthorTextColor(I)V

    .line 1931413
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->b()V

    .line 1931414
    invoke-direct {p0, p1}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->b(LX/CkL;)V

    .line 1931415
    return-void

    :cond_0
    const/high16 v0, -0x1000000

    goto :goto_0

    :cond_1
    const/high16 v0, -0x1000000

    goto :goto_1

    :cond_2
    const v1, -0x777778

    goto :goto_2
.end method

.method public getBylineView()Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;
    .locals 1

    .prologue
    .line 1931391
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->d:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    return-object v0
.end method

.method public getCoverImageView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1

    .prologue
    .line 1931390
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public getDescriptionTextView()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 1931389
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->f:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public getTitleTextView()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 1931388
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->e:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public setBylineAreaBackgroundColor(I)V
    .locals 1

    .prologue
    .line 1931386
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->d:Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    invoke-static {v0, p1}, LX/8ba;->a(Landroid/view/View;I)V

    .line 1931387
    return-void
.end method

.method public setDescriptionText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1931384
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1931385
    return-void
.end method

.method public setDescriptionTextColor(I)V
    .locals 1

    .prologue
    .line 1931382
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1931383
    return-void
.end method

.method public setHeadlineTextColor(I)V
    .locals 1

    .prologue
    .line 1931376
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1931377
    return-void
.end method

.method public setImageParams(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1931380
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1931381
    return-void
.end method

.method public setTitleText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1931378
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverSpecView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1931379
    return-void
.end method
