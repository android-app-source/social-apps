.class public Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1931375
    const-class v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1931352
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1931353
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->a()V

    .line 1931354
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1931372
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1931373
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->a()V

    .line 1931374
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1931369
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1931370
    invoke-direct {p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->a()V

    .line 1931371
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1931364
    const-class v0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    invoke-static {v0, p0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1931365
    const v0, 0x7f0309f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1931366
    const v0, 0x7f0d1935

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1931367
    const v0, 0x7f0d1937

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1931368
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->a:LX/1Ad;

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;I)V
    .locals 2

    .prologue
    .line 1931361
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->a:LX/1Ad;

    sget-object v1, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    new-instance v1, LX/Ckn;

    invoke-direct {v1, p0, p2}, LX/Ckn;-><init>(Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;I)V

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1931362
    iget-object v1, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1931363
    return-void
.end method

.method public getAuthorTextView()Lcom/facebook/resources/ui/FbTextView;
    .locals 1

    .prologue
    .line 1931360
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->d:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method

.method public getLogoImageView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1

    .prologue
    .line 1931359
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public setAuthorText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1931357
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1931358
    return-void
.end method

.method public setAuthorTextColor(I)V
    .locals 1

    .prologue
    .line 1931355
    iget-object v0, p0, Lcom/facebook/richdocument/linkcovers/view/LinkCoverByLineView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1931356
    return-void
.end method
