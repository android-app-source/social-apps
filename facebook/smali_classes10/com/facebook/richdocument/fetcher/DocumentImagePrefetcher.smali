.class public Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/1HI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/Cj5;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1929093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1929094
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->b:Ljava/util/Queue;

    .line 1929095
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->c:Ljava/util/List;

    .line 1929096
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    invoke-static {v0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v0

    check-cast v0, LX/1HI;

    iput-object v0, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->a:LX/1HI;

    .line 1929097
    return-void
.end method

.method public static declared-synchronized a$redex0(Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1929061
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1929062
    invoke-direct {p0}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929063
    monitor-exit p0

    return-void

    .line 1929064
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 2

    .prologue
    .line 1929089
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 1929090
    invoke-direct {p0}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929091
    :cond_0
    monitor-exit p0

    return-void

    .line 1929092
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 4

    .prologue
    .line 1929077
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cj5;

    .line 1929078
    if-eqz v0, :cond_0

    .line 1929079
    iget-object v1, v0, LX/Cj5;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    sget-object v2, LX/1bc;->MEDIUM:LX/1bc;

    .line 1929080
    iput-object v2, v1, LX/1bX;->i:LX/1bc;

    .line 1929081
    move-object v1, v1

    .line 1929082
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1929083
    if-eqz v1, :cond_0

    .line 1929084
    iget-object v2, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->a:LX/1HI;

    const-class v3, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/1HI;->e(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    .line 1929085
    iget-object v2, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->c:Ljava/util/List;

    iget-object v3, v0, LX/Cj5;->a:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1929086
    new-instance v2, LX/Cj4;

    invoke-direct {v2, v0, p0}, LX/Cj4;-><init>(LX/Cj5;Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929087
    :cond_0
    monitor-exit p0

    return-void

    .line 1929088
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 1929073
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1929074
    iget-object v0, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929075
    monitor-exit p0

    return-void

    .line 1929076
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/CGy;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2

    .prologue
    .line 1929068
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1929069
    iget-object v0, p0, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->b:Ljava/util/Queue;

    new-instance v1, LX/Cj5;

    invoke-direct {v1, p1, p2, p3}, LX/Cj5;-><init>(Ljava/lang/String;LX/CGy;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1929070
    invoke-direct {p0}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929071
    :cond_0
    monitor-exit p0

    return-void

    .line 1929072
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1929065
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, p2}, Lcom/facebook/richdocument/fetcher/DocumentImagePrefetcher;->a(Ljava/lang/String;LX/CGy;Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1929066
    monitor-exit p0

    return-void

    .line 1929067
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
