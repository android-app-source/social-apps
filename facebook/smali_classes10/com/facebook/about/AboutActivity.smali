.class public Lcom/facebook/about/AboutActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0WV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1gy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03R;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Or;
    .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2151846
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2151847
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2151828
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 2151829
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/about/AboutActivity;->z:LX/0h5;

    .line 2151830
    iget-object v0, p0, Lcom/facebook/about/AboutActivity;->u:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-eq v0, v1, :cond_0

    .line 2151831
    iget-object v0, p0, Lcom/facebook/about/AboutActivity;->z:LX/0h5;

    new-instance v1, LX/Edv;

    invoke-direct {v1, p0}, LX/Edv;-><init>(Lcom/facebook/about/AboutActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2151832
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const/4 v1, 0x1

    .line 2151833
    iput v1, v0, LX/108;->a:I

    .line 2151834
    move-object v0, v0

    .line 2151835
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a56

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2151836
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2151837
    move-object v0, v0

    .line 2151838
    iget-object v1, p0, Lcom/facebook/about/AboutActivity;->w:LX/0wM;

    const v2, 0x7f02091b

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2151839
    iput-object v1, v0, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2151840
    move-object v0, v0

    .line 2151841
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2151842
    iget-object v1, p0, Lcom/facebook/about/AboutActivity;->z:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2151843
    iget-object v0, p0, Lcom/facebook/about/AboutActivity;->z:LX/0h5;

    new-instance v1, LX/Edw;

    invoke-direct {v1, p0}, LX/Edw;-><init>(Lcom/facebook/about/AboutActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2151844
    :cond_0
    iget-object v0, p0, Lcom/facebook/about/AboutActivity;->z:LX/0h5;

    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a57

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2151845
    return-void
.end method

.method private static a(Lcom/facebook/about/AboutActivity;LX/0Ot;LX/0WV;LX/1gy;Lcom/facebook/content/SecureContextHelper;LX/03R;LX/01T;LX/17Y;LX/0wM;LX/0Or;LX/0Xl;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/about/AboutActivity;",
            "LX/0Ot",
            "<",
            "LX/FiT;",
            ">;",
            "LX/0WV;",
            "LX/1gy;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/03R;",
            "LX/01T;",
            "LX/17Y;",
            "LX/0wM;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Xl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2151827
    iput-object p1, p0, Lcom/facebook/about/AboutActivity;->p:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/about/AboutActivity;->q:LX/0WV;

    iput-object p3, p0, Lcom/facebook/about/AboutActivity;->r:LX/1gy;

    iput-object p4, p0, Lcom/facebook/about/AboutActivity;->s:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/about/AboutActivity;->t:LX/03R;

    iput-object p6, p0, Lcom/facebook/about/AboutActivity;->u:LX/01T;

    iput-object p7, p0, Lcom/facebook/about/AboutActivity;->v:LX/17Y;

    iput-object p8, p0, Lcom/facebook/about/AboutActivity;->w:LX/0wM;

    iput-object p9, p0, Lcom/facebook/about/AboutActivity;->x:LX/0Or;

    iput-object p10, p0, Lcom/facebook/about/AboutActivity;->y:LX/0Xl;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/about/AboutActivity;

    const/16 v1, 0x34e9

    invoke-static {v10, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {v10}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v2

    check-cast v2, LX/0WV;

    invoke-static {v10}, LX/1Mn;->a(LX/0QB;)LX/1gy;

    move-result-object v3

    check-cast v3, LX/1gy;

    invoke-static {v10}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v10}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v5

    check-cast v5, LX/03R;

    invoke-static {v10}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v6

    check-cast v6, LX/01T;

    invoke-static {v10}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v10}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    const/16 v9, 0x14d1

    invoke-static {v10, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static {v10}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v10

    check-cast v10, LX/0Xl;

    invoke-static/range {v0 .. v10}, Lcom/facebook/about/AboutActivity;->a(Lcom/facebook/about/AboutActivity;LX/0Ot;LX/0WV;LX/1gy;Lcom/facebook/content/SecureContextHelper;LX/03R;LX/01T;LX/17Y;LX/0wM;LX/0Or;LX/0Xl;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/about/AboutActivity;Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2151824
    iget-object v0, p0, Lcom/facebook/about/AboutActivity;->v:LX/17Y;

    sget-object v1, LX/0ax;->dn:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2151825
    iget-object v1, p0, Lcom/facebook/about/AboutActivity;->s:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2151826
    return-void
.end method

.method private b()V
    .locals 0

    .prologue
    .line 2151820
    invoke-direct {p0}, Lcom/facebook/about/AboutActivity;->p()V

    .line 2151821
    invoke-direct {p0}, Lcom/facebook/about/AboutActivity;->o()V

    .line 2151822
    invoke-direct {p0}, Lcom/facebook/about/AboutActivity;->n()V

    .line 2151823
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 2151737
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082a52

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/about/AboutActivity;->A:Ljava/lang/String;

    .line 2151738
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 2151739
    if-eqz v0, :cond_0

    .line 2151740
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082a53

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/about/AboutActivity;->A:Ljava/lang/String;

    .line 2151741
    :cond_0
    iget-object v0, p0, Lcom/facebook/about/AboutActivity;->u:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_1

    .line 2151742
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082a54

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/about/AboutActivity;->A:Ljava/lang/String;

    .line 2151743
    :cond_1
    return-void
.end method

.method private m()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 2151804
    const v0, 0x7f0d0359

    invoke-virtual {p0, v0}, Lcom/facebook/about/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2151805
    const v1, 0x7f0d035a

    invoke-virtual {p0, v1}, Lcom/facebook/about/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2151806
    const v2, 0x7f0d035b

    invoke-virtual {p0, v2}, Lcom/facebook/about/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 2151807
    iget-object v3, p0, Lcom/facebook/about/AboutActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2151808
    sget-boolean v3, LX/007;->i:Z

    move v3, v3

    .line 2151809
    if-nez v3, :cond_0

    sget-object v3, LX/03R;->YES:LX/03R;

    iget-object v4, p0, Lcom/facebook/about/AboutActivity;->t:LX/03R;

    invoke-virtual {v3, v4}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2151810
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/facebook/about/AboutActivity;->q:LX/0WV;

    invoke-virtual {v4}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/about/AboutActivity;->q:LX/0WV;

    invoke-virtual {v4}, LX/0WV;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2151811
    iget-object v1, p0, Lcom/facebook/about/AboutActivity;->r:LX/1gy;

    iget-object v1, v1, LX/1gy;->a:Ljava/lang/String;

    .line 2151812
    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_1

    .line 2151813
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2151814
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2151815
    :goto_0
    new-instance v1, LX/Edx;

    invoke-direct {v1, p0}, LX/Edx;-><init>(Lcom/facebook/about/AboutActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2151816
    return-void

    .line 2151817
    :cond_1
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 2151818
    :cond_2
    iget-object v3, p0, Lcom/facebook/about/AboutActivity;->q:LX/0WV;

    invoke-virtual {v3}, LX/0WV;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2151819
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private n()V
    .locals 6

    .prologue
    .line 2151791
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2151792
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 2151793
    if-eqz v0, :cond_0

    const v0, 0x7f082a5e

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2151794
    sget-boolean v0, LX/007;->j:Z

    move v0, v0

    .line 2151795
    if-eqz v0, :cond_1

    const-string v0, "/legal/FB_Work_AUP"

    move-object v1, v0

    .line 2151796
    :goto_1
    const v0, 0x7f0d035e

    invoke-virtual {p0, v0}, Lcom/facebook/about/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2151797
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2151798
    new-instance v4, LX/Edy;

    invoke-direct {v4, p0, v1}, LX/Edy;-><init>(Lcom/facebook/about/AboutActivity;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-static {v2}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v2

    const/16 v5, 0x21

    invoke-virtual {v3, v4, v1, v2, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2151799
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2151800
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2151801
    return-void

    .line 2151802
    :cond_0
    const v0, 0x7f082a5d

    goto :goto_0

    .line 2151803
    :cond_1
    const-string v0, "/terms.php"

    move-object v1, v0

    goto :goto_1
.end method

.method private o()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v1, 0x0

    .line 2151775
    const v0, 0x7f0d035d

    invoke-virtual {p0, v0}, Lcom/facebook/about/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2151776
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082a5b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2151777
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f082a5c

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/facebook/about/AboutActivity;->A:Ljava/lang/String;

    aput-object v6, v5, v1

    const/4 v6, 0x1

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2151778
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    move v2, v1

    .line 2151779
    :cond_0
    :goto_0
    if-eq v1, v8, :cond_1

    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 2151780
    invoke-virtual {v4, v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 2151781
    if-eq v1, v8, :cond_0

    .line 2151782
    invoke-virtual {v4, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2151783
    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2151784
    new-instance v2, LX/Edy;

    const-string v6, "/legal/thirdpartynotices"

    invoke-direct {v2, p0, v6}, LX/Edy;-><init>(Lcom/facebook/about/AboutActivity;Ljava/lang/String;)V

    invoke-static {v3}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v6, v1

    const/16 v7, 0x21

    invoke-virtual {v5, v2, v1, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2151785
    invoke-static {v3}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v2

    add-int/2addr v2, v1

    goto :goto_0

    .line 2151786
    :cond_1
    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 2151787
    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2151788
    :cond_2
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2151789
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2151790
    return-void
.end method

.method private p()V
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/4 v1, 0x0

    const/4 v9, -0x1

    .line 2151752
    const v0, 0x7f0d035c

    invoke-virtual {p0, v0}, Lcom/facebook/about/AboutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2151753
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082a58

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2151754
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082a59

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2151755
    invoke-virtual {p0}, Lcom/facebook/about/AboutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082a5a

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v4, v6, v1

    const/4 v7, 0x1

    aput-object v5, v6, v7

    invoke-virtual {v2, v3, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 2151756
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7}, Landroid/text/SpannableStringBuilder;-><init>()V

    move v2, v1

    move v3, v1

    .line 2151757
    :cond_0
    :goto_0
    if-ne v2, v9, :cond_1

    if-eq v1, v9, :cond_4

    :cond_1
    invoke-static {v6}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 2151758
    invoke-virtual {v6, v5, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 2151759
    invoke-virtual {v6, v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 2151760
    if-le v2, v1, :cond_2

    if-ne v1, v9, :cond_3

    :cond_2
    if-eq v2, v9, :cond_3

    .line 2151761
    invoke-virtual {v6, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2151762
    invoke-virtual {v7, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2151763
    new-instance v3, LX/Edz;

    invoke-direct {v3, p0}, LX/Edz;-><init>(Lcom/facebook/about/AboutActivity;)V

    invoke-static {v5}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v8

    add-int/2addr v8, v2

    invoke-virtual {v7, v3, v2, v8, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2151764
    invoke-static {v5}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v3, v2

    goto :goto_0

    .line 2151765
    :cond_3
    if-eq v1, v9, :cond_0

    .line 2151766
    invoke-virtual {v6, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2151767
    invoke-virtual {v7, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2151768
    new-instance v3, LX/Edz;

    invoke-direct {v3, p0}, LX/Edz;-><init>(Lcom/facebook/about/AboutActivity;)V

    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v8

    add-int/2addr v8, v1

    invoke-virtual {v7, v3, v1, v8, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2151769
    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v3, v1

    goto :goto_0

    .line 2151770
    :cond_4
    invoke-static {v6}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    if-ge v3, v1, :cond_5

    .line 2151771
    invoke-virtual {v6, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2151772
    :cond_5
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2151773
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2151774
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2151744
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2151745
    invoke-static {p0, p0}, Lcom/facebook/about/AboutActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2151746
    const v0, 0x7f030017

    invoke-virtual {p0, v0}, Lcom/facebook/about/AboutActivity;->setContentView(I)V

    .line 2151747
    invoke-direct {p0}, Lcom/facebook/about/AboutActivity;->l()V

    .line 2151748
    invoke-direct {p0}, Lcom/facebook/about/AboutActivity;->a()V

    .line 2151749
    invoke-direct {p0}, Lcom/facebook/about/AboutActivity;->m()V

    .line 2151750
    invoke-direct {p0}, Lcom/facebook/about/AboutActivity;->b()V

    .line 2151751
    return-void
.end method
