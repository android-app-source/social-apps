.class public Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1986030
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;Lcom/facebook/groupcommerce/model/GroupCommerceCategory;)V
    .locals 3

    .prologue
    .line 1986031
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "category_id"

    iget-object v2, p1, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->categoryID:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1986032
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;->setResult(ILandroid/content/Intent;)V

    .line 1986033
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;->finish()V

    .line 1986034
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1986035
    const v0, 0x7f0312ee

    invoke-virtual {p0, v0}, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;->setContentView(I)V

    .line 1986036
    const v0, 0x7f0d0846

    invoke-virtual {p0, v0}, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1986037
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "categories"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;->p:Ljava/util/ArrayList;

    .line 1986038
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1986039
    iget-object v1, p0, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v1, p0, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;

    .line 1986040
    iget-object v1, v1, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->name:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1986041
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1986042
    :cond_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x1090016

    invoke-direct {v1, v2, v4, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1986043
    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1986044
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/4 v3, -0x1

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1986045
    new-instance v1, LX/DJv;

    invoke-direct {v1, p0}, LX/DJv;-><init>(Lcom/facebook/groupcommerce/ui/SelectCategoryActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1986046
    return-void
.end method
