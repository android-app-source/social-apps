.class public final Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Landroid/content/DialogInterface$OnClickListener;

.field public n:Landroid/content/DialogInterface$OnClickListener;

.field public o:LX/B0k;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1985851
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 1985852
    const v0, 0x7f082efd

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1985853
    const v1, 0x7f082efe

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1985854
    const v2, 0x7f082eff

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1985855
    const v3, 0x7f082f00

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1985856
    new-instance v4, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 1985857
    invoke-virtual {v4, v0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1985858
    invoke-virtual {v4, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1985859
    new-instance v0, LX/DJm;

    invoke-direct {v0, p0}, LX/DJm;-><init>(Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;)V

    invoke-virtual {v4, v2, v0}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1985860
    new-instance v0, LX/DJn;

    invoke-direct {v0, p0}, LX/DJn;-><init>(Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;)V

    invoke-virtual {v4, v3, v0}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1985861
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;->o:LX/B0k;

    const/4 v1, 0x0

    sget-object v2, LX/21D;->GROUP_FEED:LX/21D;

    invoke-virtual {v0, v1, v2}, LX/B0k;->a(Ljava/lang/String;LX/21D;)V

    .line 1985862
    invoke-virtual {v4}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x147958cb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1985863
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1985864
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;

    invoke-static {v1}, LX/B0k;->a(LX/0QB;)LX/B0k;

    move-result-object v1

    check-cast v1, LX/B0k;

    iput-object v1, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostInterceptDialogFragment;->o:LX/B0k;

    .line 1985865
    const/16 v1, 0x2b

    const v2, -0xd913517

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
