.class public Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/DJu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

.field private p:Ljava/lang/String;

.field public q:Landroid/content/DialogInterface$OnClickListener;

.field public r:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1985750
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1985751
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1985752
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/DJu;->a(LX/0QB;)LX/DJu;

    move-result-object p0

    check-cast p0, LX/DJu;

    iput-object v1, p1, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->m:LX/0ad;

    iput-object p0, p1, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->n:LX/DJu;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1985714
    new-instance v1, Lcom/facebook/components/ComponentView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    .line 1985715
    new-instance v0, LX/1De;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 1985716
    iget-object v3, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->n:LX/DJu;

    const/4 v4, 0x0

    .line 1985717
    new-instance v5, LX/DJt;

    invoke-direct {v5, v3}, LX/DJt;-><init>(LX/DJu;)V

    .line 1985718
    sget-object p1, LX/DJu;->a:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/DJs;

    .line 1985719
    if-nez p1, :cond_0

    .line 1985720
    new-instance p1, LX/DJs;

    invoke-direct {p1}, LX/DJs;-><init>()V

    .line 1985721
    :cond_0
    invoke-static {p1, v0, v4, v4, v5}, LX/DJs;->a$redex0(LX/DJs;LX/1De;IILX/DJt;)V

    .line 1985722
    move-object v5, p1

    .line 1985723
    move-object v4, v5

    .line 1985724
    move-object v3, v4

    .line 1985725
    iget-object v4, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iget-object v4, v4, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->title:Ljava/lang/String;

    .line 1985726
    iget-object v5, v3, LX/DJs;->a:LX/DJt;

    iput-object v4, v5, LX/DJt;->a:Ljava/lang/String;

    .line 1985727
    move-object v3, v3

    .line 1985728
    iget-object v4, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iget-object v4, v4, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->price:Ljava/lang/Long;

    .line 1985729
    iget-object v5, v3, LX/DJs;->a:LX/DJt;

    iput-object v4, v5, LX/DJt;->b:Ljava/lang/Long;

    .line 1985730
    move-object v3, v3

    .line 1985731
    iget-object v4, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iget-object v4, v4, Lcom/facebook/ipc/composer/model/ProductItemAttachment;->currencyCode:Ljava/lang/String;

    .line 1985732
    iget-object v5, v3, LX/DJs;->a:LX/DJt;

    iput-object v4, v5, LX/DJt;->c:Ljava/lang/String;

    .line 1985733
    move-object v3, v3

    .line 1985734
    iget-object v4, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->p:Ljava/lang/String;

    .line 1985735
    iget-object v5, v3, LX/DJs;->a:LX/DJt;

    iput-object v4, v5, LX/DJt;->d:Ljava/lang/String;

    .line 1985736
    move-object v3, v3

    .line 1985737
    invoke-static {v0, v3}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 1985738
    invoke-virtual {v1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1985739
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    move v3, v2

    move v4, v2

    move v5, v2

    .line 1985740
    invoke-virtual/range {v0 .. v5}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    .line 1985741
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1985742
    iget-object v3, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->m:LX/0ad;

    sget-char v4, LX/DIp;->e:C

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f082f1b

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 1985743
    iget-object v3, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->r:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v3}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1985744
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1985745
    iget-object v3, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->m:LX/0ad;

    sget-char v4, LX/DIp;->d:C

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f082f1a

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 1985746
    iget-object v3, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->q:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1985747
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    .line 1985748
    invoke-virtual {v0, v2}, LX/2EJ;->setCanceledOnTouchOutside(Z)V

    .line 1985749
    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xab06242

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1985707
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1985708
    const-class v0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;

    invoke-static {v0, p0}, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1985709
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1985710
    const-string v2, "ARG_PRODUCT_ITEM_ATTACHMENT"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->o:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1985711
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1985712
    const-string v2, "ARG_LOCATION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupCommerceNLUDialogFragment;->p:Ljava/lang/String;

    .line 1985713
    const/16 v0, 0x2b

    const v2, 0x27821e30

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
