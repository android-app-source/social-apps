.class public final Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Landroid/view/View$OnClickListener;

.field public n:Landroid/view/View$OnClickListener;

.field public o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:LX/0tX;

.field public s:Z

.field public t:Z

.field public u:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1985887
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1985898
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0310fc

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1985899
    const v0, 0x7f0d2860

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/resources/ui/FbButton;

    .line 1985900
    const v0, 0x7f0d285f

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/resources/ui/FbButton;

    .line 1985901
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1985902
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1985903
    new-instance v3, LX/DJo;

    invoke-direct {v3, p0, v1}, LX/DJo;-><init>(Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;Landroid/widget/LinearLayout;)V

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1985904
    :cond_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->p:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1985905
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->p:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1985906
    :cond_1
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->q:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1985907
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->t:Z

    if-eqz v0, :cond_3

    .line 1985908
    :cond_2
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1985909
    :cond_3
    const v0, 0x7f0d285d

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1985910
    const v3, 0x7f0d285e

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/widget/text/BetterTextView;

    .line 1985911
    iget-object v4, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v4, v4, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellTitleLabel:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1985912
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v0, v0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellSubtitleLabel:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1985913
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    move v3, v2

    move v4, v2

    move v5, v2

    .line 1985914
    invoke-virtual/range {v0 .. v5}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    .line 1985915
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    .line 1985916
    new-instance v1, LX/DJp;

    invoke-direct {v1, p0, v0}, LX/DJp;-><init>(Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;LX/2EJ;)V

    invoke-virtual {v6, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1985917
    new-instance v1, LX/DJq;

    invoke-direct {v1, p0, v0}, LX/DJq;-><init>(Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;LX/2EJ;)V

    invoke-virtual {v7, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1985918
    return-object v0
.end method

.method public final a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;Z)V
    .locals 1

    .prologue
    .line 1985891
    iput-object p1, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    .line 1985892
    iput-boolean p2, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->t:Z

    .line 1985893
    if-nez p2, :cond_0

    .line 1985894
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v0, v0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellAcceptButtonLabel:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->p:Ljava/lang/String;

    .line 1985895
    :cond_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->o:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v0, v0, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->upsellDeclineButtonLabel:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->q:Ljava/lang/String;

    .line 1985896
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->s:Z

    .line 1985897
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x53ac1b94

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1985888
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1985889
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    iput-object v1, p0, Lcom/facebook/groupcommerce/ui/GroupsSalePostMarketplaceInfoDialogFragment;->r:LX/0tX;

    .line 1985890
    const/16 v1, 0x2b

    const v2, -0x7d06da05

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
