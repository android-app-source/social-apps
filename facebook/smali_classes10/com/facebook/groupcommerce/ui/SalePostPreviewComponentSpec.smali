.class public Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile i:Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;


# instance fields
.field private final b:LX/34u;

.field private final c:LX/0WJ;

.field public final d:LX/0ad;

.field private final e:LX/CDt;

.field private final f:LX/11R;

.field private final g:LX/0SG;

.field private final h:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1985977
    const-class v0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;

    const-string v1, "c2c_commerce"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/34u;LX/0WJ;LX/0ad;LX/CDt;LX/11R;LX/0SG;LX/1vg;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1985978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1985979
    iput-object p1, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->b:LX/34u;

    .line 1985980
    iput-object p2, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->c:LX/0WJ;

    .line 1985981
    iput-object p3, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->d:LX/0ad;

    .line 1985982
    iput-object p4, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->e:LX/CDt;

    .line 1985983
    iput-object p5, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->f:LX/11R;

    .line 1985984
    iput-object p6, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->g:LX/0SG;

    .line 1985985
    iput-object p7, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->h:LX/1vg;

    .line 1985986
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;
    .locals 11

    .prologue
    .line 1985987
    sget-object v0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->i:Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;

    if-nez v0, :cond_1

    .line 1985988
    const-class v1, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;

    monitor-enter v1

    .line 1985989
    :try_start_0
    sget-object v0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->i:Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1985990
    if-eqz v2, :cond_0

    .line 1985991
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1985992
    new-instance v3, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;

    invoke-static {v0}, LX/34u;->a(LX/0QB;)LX/34u;

    move-result-object v4

    check-cast v4, LX/34u;

    invoke-static {v0}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v5

    check-cast v5, LX/0WJ;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {v0}, LX/CDt;->a(LX/0QB;)LX/CDt;

    move-result-object v7

    check-cast v7, LX/CDt;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v8

    check-cast v8, LX/11R;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v10

    check-cast v10, LX/1vg;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;-><init>(LX/34u;LX/0WJ;LX/0ad;LX/CDt;LX/11R;LX/0SG;LX/1vg;)V

    .line 1985993
    move-object v0, v3

    .line 1985994
    sput-object v0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->i:Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1985995
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1985996
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1985997
    :cond_1
    sget-object v0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->i:Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;

    return-object v0

    .line 1985998
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1985999
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;LX/1De;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)LX/1Dh;
    .locals 10

    .prologue
    .line 1986000
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    .line 1986001
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->d:LX/0ad;

    sget-short v1, LX/DIp;->i:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1986002
    if-nez v0, :cond_1

    .line 1986003
    :cond_0
    const/4 v0, 0x0

    .line 1986004
    :goto_0
    return-object v0

    .line 1986005
    :cond_1
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->c:LX/0WJ;

    invoke-virtual {v0}, LX/0WJ;->c()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 1986006
    new-instance v0, LX/4W3;

    invoke-direct {v0}, LX/4W3;-><init>()V

    .line 1986007
    iput-object p4, v0, LX/4W3;->d:Ljava/lang/String;

    .line 1986008
    move-object v0, v0

    .line 1986009
    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1986010
    iput-object v2, v0, LX/4W3;->g:Ljava/lang/String;

    .line 1986011
    move-object v0, v0

    .line 1986012
    invoke-virtual {v0}, LX/4W3;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    .line 1986013
    iget-object v2, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->b:LX/34u;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/34u;->a(Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;Z)Ljava/lang/String;

    move-result-object v2

    .line 1986014
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->f:LX/11R;

    sget-object v3, LX/1lB;->FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

    iget-object v4, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->g:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v3

    .line 1986015
    const/4 v0, 0x0

    .line 1986016
    if-eqz p5, :cond_2

    .line 1986017
    iget-object v0, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->h:LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    const v4, 0x7f020967

    invoke-virtual {v0, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    const v4, 0x7f0a00e5

    invoke-virtual {v0, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 1986018
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v0

    invoke-interface {v4, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a010e

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, p5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-interface {v0, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    .line 1986019
    :cond_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a009c

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/1Dh;->o(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    const/16 v6, 0x18

    invoke-interface {v4, v5, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x4

    const/16 v6, 0x10

    invoke-interface {v4, v5, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x5

    const/16 v6, 0x10

    invoke-interface {v4, v5, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0a00d5

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    const/16 v6, 0x8

    const/16 v7, 0xc

    invoke-interface {v5, v6, v7}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->e:LX/CDt;

    invoke-virtual {v7, p1}, LX/CDt;->c(LX/1De;)LX/CDs;

    move-result-object v7

    sget-object v8, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, v8}, LX/CDs;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/CDs;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/CDs;->b(Ljava/lang/String;)LX/CDs;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/16 v8, 0x28

    invoke-interface {v7, v8}, LX/1Di;->j(I)LX/1Di;

    move-result-object v7

    const/16 v8, 0x28

    invoke-interface {v7, v8}, LX/1Di;->r(I)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x0

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x4

    const/16 v9, 0x8

    invoke-interface {v7, v8, v9}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    const v9, 0x7f0b0050

    invoke-virtual {v8, v9}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    invoke-interface {v7, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    const v8, 0x7f0a010e

    invoke-virtual {v7, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    const v8, 0x7f0b004e

    invoke-virtual {v7, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v6, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v5, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b0050

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v5, 0x1

    const/16 v6, 0x8

    invoke-interface {v3, v5, v6}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b0050

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a00d4

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v4, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)LX/1Dg;
    .locals 8
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Long;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x4

    const/16 v4, 0x10

    .line 1986020
    invoke-virtual {p1}, LX/1De;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1986021
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    invoke-static/range {p0 .. p5}, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->b(Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;LX/1De;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0052

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    .line 1986022
    iget-object v3, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->d:LX/0ad;

    sget-char p2, LX/DIp;->g:C

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const p4, 0x7f082f18

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-interface {v3, p2, p3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 1986023
    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, v5, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v7, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0060

    invoke-interface {v2, v6, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a010e

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    .line 1986024
    iget-object v3, p0, Lcom/facebook/groupcommerce/ui/SalePostPreviewComponentSpec;->d:LX/0ad;

    sget-char p1, LX/DIp;->f:C

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f082f19

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v3, p1, p2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1986025
    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, v5, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v7, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    const v2, 0x7f0b0062

    invoke-interface {v0, v6, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
