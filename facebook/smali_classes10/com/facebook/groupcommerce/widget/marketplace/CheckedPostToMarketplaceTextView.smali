.class public Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/resources/ui/FbCheckBox;

.field private e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1986299
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1986300
    invoke-direct {p0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a()V

    .line 1986301
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1986296
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1986297
    invoke-direct {p0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a()V

    .line 1986298
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1986293
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1986294
    invoke-direct {p0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a()V

    .line 1986295
    return-void
.end method

.method private a()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1986286
    const-class v0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    invoke-static {v0, p0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1986287
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->setOrientation(I)V

    .line 1986288
    const v0, 0x7f030286

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1986289
    const v0, 0x7f0d092f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 1986290
    const v0, 0x7f0d0298

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    iput-object v0, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->d:Lcom/facebook/resources/ui/FbCheckBox;

    .line 1986291
    new-instance v0, LX/DJz;

    invoke-direct {v0, p0, p0}, LX/DJz;-><init>(Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1986292
    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;)V
    .locals 0

    .prologue
    .line 1986285
    iput-object p1, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a:LX/0tX;

    iput-object p2, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->b:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->c:LX/03V;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ExecutorService;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a(Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/03V;)V

    return-void
.end method

.method public static final a$redex0(Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1986267
    const v0, 0x7f0d092f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1986268
    new-instance v1, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1986269
    iget-object v2, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->f:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v2, v2, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->nuxLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1986270
    sget-object v2, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 1986271
    const/4 v2, -0x1

    .line 1986272
    iput v2, v1, LX/0hs;->t:I

    .line 1986273
    invoke-virtual {v1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1986274
    new-instance v0, LX/4KO;

    invoke-direct {v0}, LX/4KO;-><init>()V

    .line 1986275
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1986276
    move-object v0, v0

    .line 1986277
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1986278
    const-string v2, "seen_marketplace_cross_post_nux"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1986279
    move-object v0, v0

    .line 1986280
    new-instance v1, LX/9Kk;

    invoke-direct {v1}, LX/9Kk;-><init>()V

    move-object v1, v1

    .line 1986281
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1986282
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1986283
    iget-object v1, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1986284
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1986259
    iput-object p1, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->f:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    .line 1986260
    iget-object v0, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->f:Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;

    iget-object v1, v1, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->checkBoxLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1986261
    new-instance v0, LX/9Jy;

    invoke-direct {v0}, LX/9Jy;-><init>()V

    move-object v0, v0

    .line 1986262
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1986263
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1986264
    iget-object v1, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1986265
    new-instance v1, LX/DK0;

    invoke-direct {v1, p0, p2}, LX/DK0;-><init>(Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1986266
    return-void
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 1986258
    iget-object v0, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1986256
    iget-object v0, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1986257
    return-void
.end method

.method public setOnPostToMarketplaceCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 1986254
    iget-object v0, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1986255
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 1986252
    iget-object v0, p0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckBox;->toggle()V

    .line 1986253
    return-void
.end method
