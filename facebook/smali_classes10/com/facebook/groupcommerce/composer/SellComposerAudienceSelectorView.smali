.class public Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field private a:LX/DJ9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/34x;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

.field private e:Landroid/widget/LinearLayout;

.field private f:Lcom/facebook/fbui/glyph/GlyphView;

.field public g:Lcom/facebook/widget/text/BetterTextView;

.field public h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public i:Lcom/facebook/fbui/glyph/GlyphView;

.field public j:Landroid/widget/LinearLayout;

.field public k:Landroid/widget/LinearLayout;

.field public l:Lcom/facebook/resources/ui/FbCheckBox;

.field private m:Lcom/facebook/widget/text/BetterTextView;

.field public n:I

.field private o:I

.field private p:Z

.field public q:Ljava/lang/String;

.field private r:Z

.field private s:Z

.field private t:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1985037
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1985038
    invoke-direct {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a()V

    .line 1985039
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1985169
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1985170
    invoke-direct {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a()V

    .line 1985171
    return-void
.end method

.method private a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)Landroid/text/SpannableString;
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v7, 0x21

    .line 1985152
    new-instance v0, LX/DJD;

    invoke-direct {v0, p0}, LX/DJD;-><init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;)V

    .line 1985153
    new-instance v1, LX/DJG;

    invoke-direct {v1, p0, p1}, LX/DJG;-><init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)V

    .line 1985154
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082f24

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1985155
    const-string v3, "[[learn_more_link]]"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 1985156
    add-int/lit8 v4, v3, 0x13

    .line 1985157
    new-instance v5, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1985158
    invoke-virtual {v5, v0, v7}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 1985159
    const/4 v6, 0x0

    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1985160
    invoke-virtual {v5}, LX/47x;->a()LX/47x;

    .line 1985161
    invoke-virtual {v5, v1, v7}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 1985162
    const v1, 0x7f08002c

    invoke-virtual {v5, v1}, LX/47x;->a(I)LX/47x;

    .line 1985163
    invoke-virtual {v5}, LX/47x;->a()LX/47x;

    .line 1985164
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_0

    .line 1985165
    invoke-virtual {v5, v0, v7}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 1985166
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1985167
    invoke-virtual {v5}, LX/47x;->a()LX/47x;

    .line 1985168
    :cond_0
    invoke-virtual {v5}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1985142
    const-class v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-static {v0, p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1985143
    const v0, 0x7f0312f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1985144
    const v0, 0x7f0d2c18

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->e:Landroid/widget/LinearLayout;

    .line 1985145
    const v0, 0x7f0d2c19

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1985146
    const v0, 0x7f0d2c1a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 1985147
    const v0, 0x7f0d2c1c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1985148
    const v0, 0x7f0d2c1b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1985149
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x1010036

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->n:I

    .line 1985150
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x1010212

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->o:I

    .line 1985151
    return-void
.end method

.method private static a(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;LX/DJ9;Landroid/view/inputmethod/InputMethodManager;LX/34x;)V
    .locals 0

    .prologue
    .line 1985141
    iput-object p1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a:LX/DJ9;

    iput-object p2, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->b:Landroid/view/inputmethod/InputMethodManager;

    iput-object p3, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->c:LX/34x;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    const-class v0, LX/DJ9;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/DJ9;

    invoke-static {v2}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v2}, LX/34x;->a(LX/0QB;)LX/34x;

    move-result-object v2

    check-cast v2, LX/34x;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;LX/DJ9;Landroid/view/inputmethod/InputMethodManager;LX/34x;)V

    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1985125
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->p:Z

    if-eqz v0, :cond_0

    .line 1985126
    :goto_0
    return-void

    .line 1985127
    :cond_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->c:LX/34x;

    .line 1985128
    iget-object v4, v0, LX/34x;->c:LX/0W3;

    sget-wide v6, LX/0X5;->dH:J

    invoke-interface {v4, v6, v7}, LX/0W4;->a(J)Z

    move-result v4

    move v0, v4

    .line 1985129
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->e:Landroid/widget/LinearLayout;

    new-instance v2, LX/DJC;

    invoke-direct {v2, p0, v0}, LX/DJC;-><init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;Z)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1985130
    if-eqz v0, :cond_3

    .line 1985131
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->k:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 1985132
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1985133
    :cond_1
    :goto_1
    invoke-static {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->c(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;)V

    .line 1985134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->p:Z

    goto :goto_0

    .line 1985135
    :cond_2
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1985136
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->j:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 1985137
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 1985138
    :cond_3
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1985139
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->k:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 1985140
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->k:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public static c(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;)V
    .locals 2

    .prologue
    .line 1985122
    invoke-direct {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getLabelTextTemplate()Ljava/lang/String;

    move-result-object v0

    .line 1985123
    new-instance v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;

    invoke-direct {v1, p0, v0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;-><init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->post(Ljava/lang/Runnable;)Z

    .line 1985124
    return-void
.end method

.method private getLabelTextTemplate()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1985104
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->r:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->s:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->t:I

    if-nez v0, :cond_1

    .line 1985105
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->c:LX/34x;

    .line 1985106
    iget-object v8, v0, LX/34x;->c:LX/0W3;

    sget-wide v10, LX/0X5;->dI:J

    invoke-interface {v8, v10, v11}, LX/0W4;->a(J)Z

    move-result v8

    move v0, v8

    .line 1985107
    if-eqz v0, :cond_0

    .line 1985108
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082f22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1985109
    :goto_0
    return-object v0

    .line 1985110
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082f21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1985111
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->s:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->r:Z

    if-eqz v0, :cond_5

    .line 1985112
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->r:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 1985113
    :goto_1
    iget-boolean v3, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->s:Z

    if-eqz v3, :cond_4

    move v3, v1

    .line 1985114
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f014b

    iget v6, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->t:I

    add-int/2addr v6, v0

    add-int/2addr v6, v3

    new-array v1, v1, [Ljava/lang/Object;

    iget v7, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->t:I

    add-int/2addr v0, v7

    add-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {v4, v5, v6, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1985115
    goto :goto_1

    :cond_4
    move v3, v2

    .line 1985116
    goto :goto_2

    .line 1985117
    :cond_5
    iget v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->t:I

    if-lez v0, :cond_6

    .line 1985118
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f014a

    iget v4, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->t:I

    new-array v1, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->t:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v0, v3, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1985119
    :cond_6
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->q:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1985120
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->q:Ljava/lang/String;

    goto :goto_0

    .line 1985121
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082f1e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;ZLX/DJH;Landroid/widget/LinearLayout;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1985082
    if-eqz p2, :cond_1

    .line 1985083
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1985084
    iput-object p4, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->j:Landroid/widget/LinearLayout;

    .line 1985085
    const/4 v0, 0x1

    .line 1985086
    invoke-direct {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->b()V

    .line 1985087
    :goto_0
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a:LX/DJ9;

    iget-object v2, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->c:LX/34x;

    .line 1985088
    iget-object v4, v2, LX/34x;->c:LX/0W3;

    sget-wide v6, LX/0X5;->hp:J

    invoke-interface {v4, v6, v7}, LX/0W4;->a(J)Z

    move-result v4

    move v2, v4

    .line 1985089
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1985090
    new-instance v4, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    const/16 v5, 0x15e7

    invoke-static {v1, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    move-object v5, p3

    move v6, v0

    move v7, v2

    move-object v8, v3

    invoke-direct/range {v4 .. v9}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;-><init>(LX/DJH;ZZLandroid/content/Context;LX/0Or;)V

    .line 1985091
    move-object v0, v4

    .line 1985092
    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->d:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    .line 1985093
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->d:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1985094
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/DJA;

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/DJA;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1985095
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/62a;

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v1, v2}, LX/62a;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1985096
    return-void

    .line 1985097
    :cond_0
    const v0, 0x7f0d2c1d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->k:Landroid/widget/LinearLayout;

    .line 1985098
    const v0, 0x7f0d2c1e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->l:Lcom/facebook/resources/ui/FbCheckBox;

    .line 1985099
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->l:Lcom/facebook/resources/ui/FbCheckBox;

    new-instance v2, LX/DJB;

    invoke-direct {v2, p0, p3}, LX/DJB;-><init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;LX/DJH;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbCheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1985100
    const v0, 0x7f0d2c1f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 1985101
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-direct {p0, p1}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1985102
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1985103
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    move v0, v1

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;ZZI)V
    .locals 2

    .prologue
    .line 1985068
    iput-object p1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->q:Ljava/lang/String;

    .line 1985069
    iput-boolean p2, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->r:Z

    .line 1985070
    iput-boolean p3, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->s:Z

    .line 1985071
    iput p4, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->t:I

    .line 1985072
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->r:Z

    if-eqz v0, :cond_1

    .line 1985073
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f0208b2

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1985074
    :goto_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->q:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->r:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->s:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->t:I

    if-nez v0, :cond_2

    .line 1985075
    :cond_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    iget v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->o:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1985076
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->g:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->o:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1985077
    :goto_1
    invoke-static {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->c(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;)V

    .line 1985078
    return-void

    .line 1985079
    :cond_1
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f0208be

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    goto :goto_0

    .line 1985080
    :cond_2
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    iget v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->n:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1985081
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->g:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->n:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_1
.end method

.method public getIsMarketplaceSelected()Z
    .locals 1

    .prologue
    .line 1985067
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->l:Lcom/facebook/resources/ui/FbCheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->l:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->d:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public getSelectedTargets()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/DJh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1985060
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->d:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    .line 1985061
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1985062
    iget-object v1, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DJh;

    .line 1985063
    iget-object v4, v1, LX/DJh;->a:Ljava/lang/String;

    const-string p0, "0"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    iget-object p0, v1, LX/DJh;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1985064
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1985065
    :cond_1
    move-object v0, v2

    .line 1985066
    return-object v0
.end method

.method public setCrossPostGroups(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/DJh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1985052
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->d:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    .line 1985053
    iget-object v1, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1985054
    invoke-static {v0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;)V

    .line 1985055
    iget-object v1, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->e:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1985056
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1985057
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1985058
    invoke-direct {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->b()V

    .line 1985059
    :cond_0
    return-void
.end method

.method public setIsMarketplaceSelected(Z)V
    .locals 2

    .prologue
    .line 1985040
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->l:Lcom/facebook/resources/ui/FbCheckBox;

    if-eqz v0, :cond_0

    .line 1985041
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->l:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1985042
    :goto_0
    return-void

    .line 1985043
    :cond_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->d:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    .line 1985044
    iget-boolean v1, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->a:Z

    if-nez v1, :cond_2

    .line 1985045
    :cond_1
    :goto_1
    goto :goto_0

    .line 1985046
    :cond_2
    if-eqz p1, :cond_3

    .line 1985047
    iget-object v1, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    const-string p0, "0"

    invoke-virtual {v1, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1985048
    :goto_2
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1985049
    iget-object v1, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->d:LX/DJH;

    if-eqz v1, :cond_1

    .line 1985050
    iget-object v1, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->d:LX/DJH;

    invoke-interface {v1}, LX/DJH;->a()V

    goto :goto_1

    .line 1985051
    :cond_3
    iget-object v1, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    const-string p0, "0"

    invoke-virtual {v1, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_2
.end method
