.class public Lcom/facebook/groupcommerce/composer/ComposerSellView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/resources/ui/FbEditText;

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field public e:Lcom/facebook/resources/ui/FbEditText;

.field private f:Lcom/facebook/resources/ui/FbEditText;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field private i:Lcom/facebook/resources/ui/FbEditText;

.field private j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

.field private k:I

.field private l:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

.field private m:LX/0W9;

.field public n:LX/34u;

.field private o:LX/34x;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field private r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1984737
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1984738
    invoke-direct {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c()V

    .line 1984739
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1984740
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1984741
    invoke-direct {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c()V

    .line 1984742
    return-void
.end method

.method private a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)Landroid/text/SpannableString;
    .locals 5

    .prologue
    .line 1984743
    new-instance v0, LX/DJ3;

    invoke-direct {v0, p0, p1}, LX/DJ3;-><init>(Lcom/facebook/groupcommerce/composer/ComposerSellView;Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)V

    .line 1984744
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1984745
    const v2, 0x7f082f23

    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    .line 1984746
    const-string v2, "[[learn_more_link]]"

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f08002c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v3, v0, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1984747
    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1984748
    invoke-static {p1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    .line 1984749
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->m:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Currency;->getSymbol(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0W9;LX/34u;LX/34x;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1984750
    iput-object p1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->m:LX/0W9;

    .line 1984751
    iput-object p2, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->n:LX/34u;

    .line 1984752
    iput-object p3, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->o:LX/34x;

    .line 1984753
    return-void
.end method

.method public static a(Lcom/facebook/resources/ui/FbEditText;)V
    .locals 2

    .prologue
    .line 1984727
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    .line 1984728
    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1, v0, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 1984729
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-static {v2}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {v2}, LX/34u;->a(LX/0QB;)LX/34u;

    move-result-object v1

    check-cast v1, LX/34u;

    invoke-static {v2}, LX/34x;->a(LX/0QB;)LX/34x;

    move-result-object v2

    check-cast v2, LX/34x;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(LX/0W9;LX/34u;LX/34x;)V

    return-void
.end method

.method public static b(Lcom/facebook/groupcommerce/composer/ComposerSellView;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1984754
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    .line 1984755
    :goto_0
    new-instance v2, LX/4W3;

    invoke-direct {v2}, LX/4W3;-><init>()V

    iget-object v3, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->p:Ljava/lang/String;

    .line 1984756
    iput-object v3, v2, LX/4W3;->d:Ljava/lang/String;

    .line 1984757
    move-object v2, v2

    .line 1984758
    const-wide/16 v4, 0x64

    mul-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1984759
    iput-object v0, v2, LX/4W3;->g:Ljava/lang/String;

    .line 1984760
    move-object v0, v2

    .line 1984761
    invoke-virtual {v0}, LX/4W3;->a()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    .line 1984762
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->n:LX/34u;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/34u;->a(Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1984763
    :cond_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1984764
    const-class v0, Lcom/facebook/groupcommerce/composer/ComposerSellView;

    invoke-static {v0, p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1984765
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setOrientation(I)V

    .line 1984766
    const v0, 0x7f03034a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1984767
    const v0, 0x7f0d0ad4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c:Lcom/facebook/resources/ui/FbEditText;

    .line 1984768
    const v0, 0x7f0d0ad5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1984769
    const v0, 0x7f0d0ad6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    .line 1984770
    const v0, 0x7f0d0ad9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 1984771
    const v0, 0x7f0d0ad8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->f:Lcom/facebook/resources/ui/FbEditText;

    .line 1984772
    const v0, 0x7f0d0ad7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1984773
    const v0, 0x7f0d0ada

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->i:Lcom/facebook/resources/ui/FbEditText;

    .line 1984774
    const v0, 0x7f0d0ad2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->l:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    .line 1984775
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->k:I

    .line 1984776
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/DIx;

    invoke-direct {v1, p0}, LX/DIx;-><init>(Lcom/facebook/groupcommerce/composer/ComposerSellView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1984777
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/DIy;

    invoke-direct {v1, p0}, LX/DIy;-><init>(Lcom/facebook/groupcommerce/composer/ComposerSellView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1984778
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/DIz;

    invoke-direct {v1, p0}, LX/DIz;-><init>(Lcom/facebook/groupcommerce/composer/ComposerSellView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1984779
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/DJ0;

    invoke-direct {v1, p0}, LX/DJ0;-><init>(Lcom/facebook/groupcommerce/composer/ComposerSellView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1984780
    return-void
.end method

.method public static d(Lcom/facebook/groupcommerce/composer/ComposerSellView;)V
    .locals 6

    .prologue
    .line 1984786
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->d:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, "%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->k:I

    iget-object v5, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v5}, Lcom/facebook/resources/ui/FbEditText;->length()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1984787
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 1984781
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1984782
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1984783
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->f:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1984784
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->i:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1984785
    return-void
.end method

.method public final a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1984827
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->l:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;Ljava/lang/String;)V

    .line 1984828
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->l:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->setVisibility(I)V

    .line 1984829
    return-void
.end method

.method public final a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;ZLX/DJH;)V
    .locals 5

    .prologue
    .line 1984812
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->s:Z

    if-eqz v0, :cond_0

    .line 1984813
    :goto_0
    return-void

    .line 1984814
    :cond_0
    const v0, 0x7f0d0adb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1984815
    const v0, 0x7f0d2c25

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    .line 1984816
    const v0, 0x7f0d2c24

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1984817
    sget-object v1, LX/DJf;->BLACK:LX/DJf;

    iget-object v2, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->o:LX/34x;

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082f13

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/34x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/DJg;->b(LX/DJf;Landroid/view/View;Ljava/lang/String;)V

    .line 1984818
    const/4 v0, 0x0

    .line 1984819
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1984820
    const v0, 0x7f0d2c26

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1984821
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1984822
    const v1, 0x7f0d2c23

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 1984823
    invoke-direct {p0, p1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;)Landroid/text/SpannableString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1984824
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1984825
    :cond_1
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-virtual {v1, p1, p2, p3, v0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a(Lcom/facebook/ipc/composer/model/MarketplaceCrossPostSettingModel;ZLX/DJH;Landroid/widget/LinearLayout;)V

    .line 1984826
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->s:Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1984808
    iput-object p2, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->p:Ljava/lang/String;

    .line 1984809
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->b(Lcom/facebook/groupcommerce/composer/ComposerSellView;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1984810
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Lcom/facebook/resources/ui/FbEditText;)V

    .line 1984811
    return-void
.end method

.method public final a(Ljava/lang/String;ZZI)V
    .locals 1

    .prologue
    .line 1984805
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    if-eqz v0, :cond_0

    .line 1984806
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->a(Ljava/lang/String;ZZI)V

    .line 1984807
    :cond_0
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1984801
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->h:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082f0c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1984802
    invoke-virtual {p0, p2}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->setCategoryID(Ljava/lang/String;)V

    .line 1984803
    return-void

    .line 1984804
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082f0d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1984791
    if-eqz p1, :cond_1

    .line 1984792
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->f:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setVisibility(I)V

    .line 1984793
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082f0e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1984794
    :goto_0
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->f:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1984795
    :goto_1
    return-void

    .line 1984796
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082f0f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1984797
    :cond_1
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1984798
    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082f10

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1984799
    :goto_2
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1984800
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082f11

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1984790
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->r:LX/0Px;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1984788
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->requestFocus()Z

    .line 1984789
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 1984730
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 1984731
    const v0, 0x7f0d0ad1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a:Landroid/widget/LinearLayout;

    .line 1984732
    const v0, 0x7f0d2c27

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1984733
    :cond_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1984734
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1984735
    return-void
.end method

.method public getCurrencySymbol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1984736
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->p:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1984680
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->i:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public getPrice()LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1984670
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->n:LX/34u;

    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/34u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1984671
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1984672
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 1984673
    :goto_0
    return-object v0

    .line 1984674
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1984675
    :catch_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public getSelectedCategoryID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1984676
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->q:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedTargets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/DJh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1984677
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getSelectedTargets()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShouldPostToMarketplace()Z
    .locals 1

    .prologue
    .line 1984678
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->getIsMarketplaceSelected()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->l:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    invoke-virtual {v0}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->isChecked()Z

    move-result v0

    goto :goto_0
.end method

.method public getStructuredLocationText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1984679
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1984681
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public getZipcodeText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1984682
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->f:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public setCategories(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/groupcommerce/model/GroupCommerceCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1984683
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1984684
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->r:LX/0Px;

    .line 1984685
    :goto_0
    return-void

    .line 1984686
    :cond_1
    iput-object p1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->r:LX/0Px;

    .line 1984687
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->h:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setCategoryID(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1984688
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->r:LX/0Px;

    if-nez v0, :cond_1

    .line 1984689
    :cond_0
    :goto_0
    return-void

    .line 1984690
    :cond_1
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->r:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->r:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;

    .line 1984691
    iget-object v3, v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->categoryID:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1984692
    iput-object p1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->q:Ljava/lang/String;

    .line 1984693
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, v0, Lcom/facebook/groupcommerce/model/GroupCommerceCategory;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1984694
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1984695
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1984696
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->clearFocus()V

    goto :goto_0

    .line 1984697
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public setCrossPostGroups(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/DJh;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1984698
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    if-eqz v0, :cond_0

    .line 1984699
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->setCrossPostGroups(Ljava/util/List;)V

    .line 1984700
    :cond_0
    return-void
.end method

.method public setCurrencyCode(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1984701
    iput-object p1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->p:Ljava/lang/String;

    .line 1984702
    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getCurrencySymbol()Ljava/lang/String;

    move-result-object v0

    .line 1984703
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082f0b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1984704
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->e:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1984705
    return-void
.end method

.method public setDescriptionText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1984706
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->i:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1984707
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->i:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Lcom/facebook/resources/ui/FbEditText;)V

    .line 1984708
    return-void
.end method

.method public setOnCategoryClickedListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1984709
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1984710
    return-void
.end method

.method public setOnLocationClickedListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1984711
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1984712
    return-void
.end method

.method public setOnPostToMarketplaceCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 1984713
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->l:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->setOnPostToMarketplaceCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1984714
    return-void
.end method

.method public setShouldCrossPostToMarketPlace(Z)V
    .locals 1

    .prologue
    .line 1984715
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    if-eqz v0, :cond_0

    .line 1984716
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->j:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->setIsMarketplaceSelected(Z)V

    .line 1984717
    :goto_0
    return-void

    .line 1984718
    :cond_0
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->l:Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/groupcommerce/widget/marketplace/CheckedPostToMarketplaceTextView;->setChecked(Z)V

    goto :goto_0
.end method

.method public setStructuredLocationText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1984719
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1984720
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1984721
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1984722
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Lcom/facebook/resources/ui/FbEditText;)V

    .line 1984723
    return-void
.end method

.method public setZipcodeText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1984724
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->f:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1984725
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/ComposerSellView;->f:Lcom/facebook/resources/ui/FbEditText;

    invoke-static {v0}, Lcom/facebook/groupcommerce/composer/ComposerSellView;->a(Lcom/facebook/resources/ui/FbEditText;)V

    .line 1984726
    return-void
.end method
