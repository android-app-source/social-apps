.class public final Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;


# direct methods
.method public constructor <init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1984991
    iput-object p1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iput-object p2, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1984992
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v0, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getWidth()I

    move-result v2

    .line 1984993
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v0, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 1984994
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->a:Ljava/lang/String;

    .line 1984995
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->q:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1984996
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->a:Ljava/lang/String;

    const-string v1, "[[group_name]]"

    iget-object v4, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v4, v4, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1984997
    :cond_0
    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    int-to-float v4, v2

    cmpg-float v1, v1, v4

    if-gez v1, :cond_1

    .line 1984998
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1984999
    :goto_0
    return-void

    .line 1985000
    :cond_1
    invoke-static {}, Ljava/text/BreakIterator;->getCharacterInstance()Ljava/text/BreakIterator;

    move-result-object v4

    .line 1985001
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->q:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/text/BreakIterator;->setText(Ljava/lang/String;)V

    .line 1985002
    invoke-virtual {v4}, Ljava/text/BreakIterator;->last()I

    .line 1985003
    invoke-virtual {v4}, Ljava/text/BreakIterator;->previous()I

    move-result v1

    :goto_1
    const/4 v5, -0x1

    if-eq v1, v5, :cond_3

    .line 1985004
    iget-object v5, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v5, v5, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->q:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1985005
    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1985006
    iget-object v5, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->a:Ljava/lang/String;

    const-string v6, "[[group_name]]"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "\u2026"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v6, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 1985007
    invoke-virtual {v3, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    int-to-float v6, v2

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    .line 1985008
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v0, v0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1985009
    :cond_2
    invoke-virtual {v4}, Ljava/text/BreakIterator;->previous()I

    move-result v1

    goto :goto_1

    .line 1985010
    :cond_3
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView$3;->b:Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;

    iget-object v1, v1, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
