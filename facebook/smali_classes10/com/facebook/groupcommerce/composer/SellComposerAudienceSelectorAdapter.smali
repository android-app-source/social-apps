.class public Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/DJ8;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public final a:Z

.field private final b:Z

.field private final c:Landroid/content/Context;

.field public final d:LX/DJH;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/DJh;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/DJH;ZZLandroid/content/Context;LX/0Or;)V
    .locals 1
    .param p1    # LX/DJH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/DJH;",
            "ZZ",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1984904
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1984905
    iput-object p1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->d:LX/DJH;

    .line 1984906
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->e:Ljava/util/List;

    .line 1984907
    iput-boolean p2, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->a:Z

    .line 1984908
    iput-boolean p3, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->b:Z

    .line 1984909
    iput-object p4, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->c:Landroid/content/Context;

    .line 1984910
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    .line 1984911
    iput-object p5, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->g:LX/0Or;

    .line 1984912
    invoke-static {p0}, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;)V

    .line 1984913
    return-void
.end method

.method public static f(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1984914
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->a:Z

    if-eqz v0, :cond_0

    .line 1984915
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->e:Ljava/util/List;

    new-instance v1, LX/DJh;

    const-string v2, "0"

    iget-object v3, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082f20

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, LX/DJh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1984916
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->b:Z

    if-eqz v0, :cond_1

    .line 1984917
    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->e:Ljava/util/List;

    new-instance v2, LX/DJh;

    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082f1f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3, v5}, LX/DJh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1984918
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1984919
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0312f6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1984920
    new-instance v1, LX/DJ8;

    invoke-direct {v1, v0}, LX/DJ8;-><init>(Landroid/view/View;)V

    .line 1984921
    new-instance v0, LX/DJ7;

    invoke-direct {v0, p0, v1}, LX/DJ7;-><init>(Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;LX/DJ8;)V

    .line 1984922
    iput-object v0, v1, LX/DJ8;->p:LX/DJ7;

    .line 1984923
    iget-object v0, v1, LX/DJ8;->l:Landroid/view/View;

    iget-object v2, v1, LX/DJ8;->p:LX/DJ7;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1984924
    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 1984925
    check-cast p1, LX/DJ8;

    .line 1984926
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DJh;

    .line 1984927
    iget-object v2, p1, LX/DJ8;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    iget-object v3, v0, LX/DJh;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1984928
    iget-object v1, p1, LX/DJ8;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v1

    check-cast v1, LX/1af;

    .line 1984929
    iget-object v2, v0, LX/DJh;->a:Ljava/lang/String;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1984930
    const v2, 0x7f0202e2

    .line 1984931
    :goto_1
    move v2, v2

    .line 1984932
    invoke-virtual {v1, v2}, LX/1af;->b(I)V

    .line 1984933
    iget-object v2, p1, LX/DJ8;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, v0, LX/DJh;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/DJh;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_2
    const-class v3, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1984934
    iget-object v1, p1, LX/DJ8;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v2, v0, LX/DJh;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1984935
    iget-object v1, p1, LX/DJ8;->p:LX/DJ7;

    iget-object v0, v0, LX/DJh;->a:Ljava/lang/String;

    .line 1984936
    iput-object v0, v1, LX/DJ7;->c:Ljava/lang/String;

    .line 1984937
    return-void

    .line 1984938
    :cond_0
    const/4 v1, 0x4

    goto :goto_0

    .line 1984939
    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    .line 1984940
    :cond_2
    iget-object v2, v0, LX/DJh;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->g:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1984941
    const v2, 0x7f0202e1

    goto :goto_1

    .line 1984942
    :cond_3
    const v2, 0x7f0219a5

    goto :goto_1
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1984943
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->f:Ljava/util/HashSet;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1984944
    iget-object v0, p0, Lcom/facebook/groupcommerce/composer/SellComposerAudienceSelectorAdapter;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
