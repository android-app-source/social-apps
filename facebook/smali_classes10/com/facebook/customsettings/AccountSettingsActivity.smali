.class public Lcom/facebook/customsettings/AccountSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164625
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2164599
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2164600
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/customsettings/AccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2164601
    if-eqz v0, :cond_0

    .line 2164602
    new-instance v1, LX/Elg;

    invoke-direct {v1, p0}, LX/Elg;-><init>(Lcom/facebook/customsettings/AccountSettingsActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2164603
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/customsettings/AccountSettingsActivity;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0Uh;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 2164624
    iput-object p1, p0, Lcom/facebook/customsettings/AccountSettingsActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/customsettings/AccountSettingsActivity;->q:LX/17Y;

    iput-object p3, p0, Lcom/facebook/customsettings/AccountSettingsActivity;->r:LX/0Uh;

    iput-object p4, p0, Lcom/facebook/customsettings/AccountSettingsActivity;->s:Ljava/lang/Boolean;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/customsettings/AccountSettingsActivity;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-static {v3}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {v3}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/customsettings/AccountSettingsActivity;->a(Lcom/facebook/customsettings/AccountSettingsActivity;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0Uh;Ljava/lang/Boolean;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2164612
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2164613
    invoke-static {p0, p0}, Lcom/facebook/customsettings/AccountSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2164614
    const v0, 0x7f030021

    invoke-virtual {p0, v0}, Lcom/facebook/customsettings/AccountSettingsActivity;->setContentView(I)V

    .line 2164615
    invoke-direct {p0}, Lcom/facebook/customsettings/AccountSettingsActivity;->a()V

    .line 2164616
    iget-object v0, p0, Lcom/facebook/customsettings/AccountSettingsActivity;->r:LX/0Uh;

    const/16 v1, 0x365

    invoke-virtual {v0, v1, v3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2164617
    const v0, 0x7f0d038c

    invoke-virtual {p0, v0}, Lcom/facebook/customsettings/AccountSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2164618
    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setVisibility(I)V

    .line 2164619
    :cond_0
    iget-object v0, p0, Lcom/facebook/customsettings/AccountSettingsActivity;->s:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2164620
    const v0, 0x7f0d038b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2164621
    const v0, 0x7f0d038a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2164622
    const v0, 0x7f0d0389

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2164623
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x75a96239

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2164604
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2164605
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2164606
    :try_start_0
    iget-object v2, p0, Lcom/facebook/customsettings/AccountSettingsActivity;->q:LX/17Y;

    invoke-static {v0, p0, v2}, LX/Ell;->a(Ljava/lang/String;Landroid/content/Context;LX/17Y;)Landroid/content/Intent;

    move-result-object v0

    .line 2164607
    iget-object v2, p0, Lcom/facebook/customsettings/AccountSettingsActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2164608
    :cond_0
    const v0, -0xbdfbd

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2164609
    :catch_0
    move-exception v0

    .line 2164610
    :goto_0
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    const v0, 0x759b0abe

    invoke-static {v0, v1}, LX/02F;->a(II)V

    throw v2

    .line 2164611
    :catch_1
    move-exception v0

    goto :goto_0
.end method
