.class public Lcom/facebook/customsettings/CustomSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164648
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2164643
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2164644
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/customsettings/CustomSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2164645
    if-eqz v0, :cond_0

    .line 2164646
    new-instance v1, LX/Elh;

    invoke-direct {v1, p0}, LX/Elh;-><init>(Lcom/facebook/customsettings/CustomSettingsActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2164647
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/customsettings/CustomSettingsActivity;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0

    .prologue
    .line 2164649
    iput-object p1, p0, Lcom/facebook/customsettings/CustomSettingsActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/customsettings/CustomSettingsActivity;->q:LX/17Y;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/customsettings/CustomSettingsActivity;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-static {p0, v0, v1}, Lcom/facebook/customsettings/CustomSettingsActivity;->a(Lcom/facebook/customsettings/CustomSettingsActivity;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 2164637
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2164638
    invoke-static {p0, p0}, Lcom/facebook/customsettings/CustomSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2164639
    invoke-virtual {p0}, Lcom/facebook/customsettings/CustomSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/customsettings/CustomSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/customsettings/CustomSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_layout"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2164640
    invoke-virtual {p0}, Lcom/facebook/customsettings/CustomSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_layout"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/customsettings/CustomSettingsActivity;->setContentView(I)V

    .line 2164641
    invoke-direct {p0}, Lcom/facebook/customsettings/CustomSettingsActivity;->a()V

    return-void

    .line 2164642
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "You must supply %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "extra_layout"

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x67e411d9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2164629
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2164630
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2164631
    :try_start_0
    iget-object v2, p0, Lcom/facebook/customsettings/CustomSettingsActivity;->q:LX/17Y;

    invoke-static {v0, p0, v2}, LX/Ell;->a(Ljava/lang/String;Landroid/content/Context;LX/17Y;)Landroid/content/Intent;

    move-result-object v0

    .line 2164632
    iget-object v2, p0, Lcom/facebook/customsettings/CustomSettingsActivity;->p:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2164633
    :cond_0
    const v0, 0x21048bc0

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2164634
    :catch_0
    move-exception v0

    .line 2164635
    :goto_0
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    const v0, 0xe76436f

    invoke-static {v0, v1}, LX/02F;->a(II)V

    throw v2

    .line 2164636
    :catch_1
    move-exception v0

    goto :goto_0
.end method
