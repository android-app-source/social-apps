.class public final Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1971909
    const-class v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;

    new-instance v1, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1971910
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1971890
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1971892
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1971893
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1971894
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1971895
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1971896
    if-eqz v2, :cond_0

    .line 1971897
    const-string p0, "application"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1971898
    invoke-static {v1, v2, p1, p2}, LX/ER0;->a$redex0(LX/15i;ILX/0nX;LX/0my;)V

    .line 1971899
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1971900
    if-eqz v2, :cond_1

    .line 1971901
    const-string p0, "device_name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1971902
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1971903
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1971904
    if-eqz v2, :cond_2

    .line 1971905
    const-string p0, "device_record"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1971906
    invoke-static {v1, v2, p1}, LX/DB2;->a(LX/15i;ILX/0nX;)V

    .line 1971907
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1971908
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1971891
    check-cast p1, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$Serializer;->a(Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;LX/0nX;LX/0my;)V

    return-void
.end method
