.class public final Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x27ad9b66
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:I

.field private s:I

.field private t:I

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1971887
    const-class v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1971886
    const-class v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1971884
    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1971885
    return-void
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971882
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->e:Ljava/lang/String;

    .line 1971883
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971880
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->g:Ljava/lang/String;

    .line 1971881
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971878
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->h:Ljava/lang/String;

    .line 1971879
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private r()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971876
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->j:Ljava/lang/String;

    .line 1971877
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private s()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971874
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->k:Ljava/lang/String;

    .line 1971875
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971872
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->l:Ljava/lang/String;

    .line 1971873
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971870
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->n:Ljava/lang/String;

    .line 1971871
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method private v()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971868
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->o:Ljava/lang/String;

    .line 1971869
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971866
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->q:Ljava/lang/String;

    .line 1971867
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method private x()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971888
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->u:Ljava/lang/String;

    const/16 v1, 0x10

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->u:Ljava/lang/String;

    .line 1971889
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->u:Ljava/lang/String;

    return-object v0
.end method

.method private y()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971800
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->w:Ljava/lang/String;

    const/16 v1, 0x12

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->w:Ljava/lang/String;

    .line 1971801
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->w:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 19

    .prologue
    .line 1971803
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1971804
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1971805
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1971806
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->p()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1971807
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->q()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1971808
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->j()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1971809
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->r()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1971810
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->s()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1971811
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->t()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1971812
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->k()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1971813
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->u()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1971814
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->v()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1971815
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->l()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1971816
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->w()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 1971817
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->x()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1971818
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->n()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1971819
    invoke-direct/range {p0 .. p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->y()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    .line 1971820
    const/16 v18, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1971821
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1971822
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1971823
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1971824
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1971825
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1971826
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1971827
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1971828
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1971829
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1971830
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1971831
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1971832
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1971833
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1971834
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->r:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1971835
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->s:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1971836
    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->t:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1971837
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1971838
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1971839
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1971840
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1971841
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1971842
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1971843
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1971844
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971845
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->f:Ljava/lang/String;

    .line 1971846
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1971847
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1971848
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->r:I

    .line 1971849
    const/16 v0, 0xe

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->s:I

    .line 1971850
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->t:I

    .line 1971851
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1971852
    new-instance v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    invoke-direct {v0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;-><init>()V

    .line 1971853
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1971854
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1971802
    const v0, 0x5b7bdd61

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1971855
    const v0, -0x3014cd99

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971856
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->i:Ljava/lang/String;

    .line 1971857
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971858
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->m:Ljava/lang/String;

    .line 1971859
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971860
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->p:Ljava/lang/String;

    const/16 v1, 0xb

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->p:Ljava/lang/String;

    .line 1971861
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 1971862
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1971863
    iget v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->r:I

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971864
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->v:Ljava/lang/String;

    .line 1971865
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;->v:Ljava/lang/String;

    return-object v0
.end method
