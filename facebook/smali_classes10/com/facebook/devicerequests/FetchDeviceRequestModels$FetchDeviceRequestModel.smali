.class public final Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x62eb28de
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1971911
    const-class v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1971912
    const-class v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1971913
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1971914
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1971915
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1971916
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->a()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1971917
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1971918
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->k()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1971919
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1971920
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1971921
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1971922
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1971923
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1971924
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1971925
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1971926
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->a()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1971927
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->a()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    .line 1971928
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->a()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1971929
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;

    .line 1971930
    iput-object v0, v1, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->e:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    .line 1971931
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->k()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1971932
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->k()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    .line 1971933
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->k()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1971934
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;

    .line 1971935
    iput-object v0, v1, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    .line 1971936
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1971937
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971938
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->e:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->e:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    .line 1971939
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->e:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1971940
    new-instance v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;

    invoke-direct {v0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;-><init>()V

    .line 1971941
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1971942
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1971943
    const v0, -0x2d4d1bc0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1971944
    const v0, 0x2e42ef39

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971945
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->f:Ljava/lang/String;

    .line 1971946
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971947
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    .line 1971948
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel;->g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$DeviceRecordModel;

    return-object v0
.end method
