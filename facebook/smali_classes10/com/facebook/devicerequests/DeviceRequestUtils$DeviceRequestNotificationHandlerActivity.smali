.class public final Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/content/SecureContextHelper;

.field private q:LX/2c4;

.field private r:LX/0if;

.field private final s:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1971597
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1971598
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->s:I

    return-void
.end method

.method private a(Lcom/facebook/content/SecureContextHelper;LX/2c4;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1971599
    iput-object p1, p0, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->p:Lcom/facebook/content/SecureContextHelper;

    .line 1971600
    iput-object p2, p0, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->q:LX/2c4;

    .line 1971601
    iput-object p3, p0, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->r:LX/0if;

    .line 1971602
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/2c4;->a(LX/0QB;)LX/2c4;

    move-result-object v1

    check-cast v1, LX/2c4;

    invoke-static {v2}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v2

    check-cast v2, LX/0if;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->a(Lcom/facebook/content/SecureContextHelper;LX/2c4;LX/0if;)V

    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, 0x1

    .line 1971603
    invoke-static {p0, p0}, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1971604
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(Landroid/os/Bundle;)V

    .line 1971605
    invoke-virtual {p0}, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 1971606
    invoke-static {v0}, LX/DAx;->b(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1971607
    iget-object v1, p0, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->r:LX/0if;

    sget-object v2, LX/0ig;->aV:LX/0ih;

    const-string v3, "tapped_device_notification"

    invoke-virtual {v1, v2, v4, v5, v3}, LX/0if;->b(LX/0ih;JLjava/lang/String;)V

    .line 1971608
    iget-object v1, p0, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->r:LX/0if;

    sget-object v2, LX/0ig;->aV:LX/0ih;

    invoke-virtual {v1, v2, v4, v5}, LX/0if;->c(LX/0ih;J)V

    .line 1971609
    iget-object v1, p0, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->q:LX/2c4;

    sget-object v2, Lcom/facebook/notifications/constants/NotificationType;->DEVICE_REQUEST:Lcom/facebook/notifications/constants/NotificationType;

    invoke-virtual {v1, v2}, LX/2c4;->a(Lcom/facebook/notifications/constants/NotificationType;)V

    .line 1971610
    iget-object v1, p0, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->p:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1971611
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/devicerequests/DeviceRequestUtils$DeviceRequestNotificationHandlerActivity;->finish()V

    .line 1971612
    return-void
.end method
