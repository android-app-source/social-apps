.class public final Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x43b0d736
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1971747
    const-class v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1971746
    const-class v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1971744
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1971745
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1971734
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1971735
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1971736
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1971737
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->l()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1971738
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1971739
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1971740
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1971741
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1971742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1971743
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1971726
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1971727
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->l()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1971728
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->l()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    .line 1971729
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->l()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1971730
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    .line 1971731
    iput-object v0, v1, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    .line 1971732
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1971733
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971725
    invoke-virtual {p0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1971714
    new-instance v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;

    invoke-direct {v0}, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;-><init>()V

    .line 1971715
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1971716
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1971724
    const v0, -0x5c6d17d3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1971723
    const v0, -0x3ff252d0

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971721
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->e:Ljava/lang/String;

    .line 1971722
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971719
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->f:Ljava/lang/String;

    .line 1971720
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1971717
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    iput-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    .line 1971718
    iget-object v0, p0, Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel;->g:Lcom/facebook/devicerequests/FetchDeviceRequestModels$FetchDeviceRequestModel$ApplicationModel$SquareLogoModel;

    return-object v0
.end method
