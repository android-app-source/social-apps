.class public final Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6ba12387
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2165427
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2165398
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2165425
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2165426
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2165419
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2165420
    invoke-virtual {p0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x76fd124a

    invoke-static {v1, v0, v2}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2165421
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2165422
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2165423
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2165424
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2165409
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2165410
    invoke-virtual {p0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2165411
    invoke-virtual {p0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x76fd124a

    invoke-static {v2, v0, v3}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2165412
    invoke-virtual {p0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2165413
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;

    .line 2165414
    iput v3, v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->e:I

    .line 2165415
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2165416
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 2165417
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 2165418
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getWhatToVerify"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2165407
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2165408
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2165404
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2165405
    const/4 v0, 0x0

    const v1, 0x76fd124a

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;->e:I

    .line 2165406
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2165401
    new-instance v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;

    invoke-direct {v0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;-><init>()V

    .line 2165402
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2165403
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2165400
    const v0, 0x70979dde

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2165399
    const v0, 0x8d103c6

    return v0
.end method
