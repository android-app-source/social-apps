.class public final Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6dfbfffd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2165331
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2165332
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2165324
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2165325
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2165326
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2165327
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2165328
    iget v0, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 2165329
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2165330
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2165321
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2165322
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2165323
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2165318
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2165319
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;->e:I

    .line 2165320
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2165315
    new-instance v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;

    invoke-direct {v0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;-><init>()V

    .line 2165316
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2165317
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2165314
    const v0, -0x56ac6281

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2165313
    const v0, 0x522630c6

    return v0
.end method
