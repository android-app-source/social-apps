.class public final Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x77374d8e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2165608
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2165607
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2165605
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2165606
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2165597
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2165598
    invoke-virtual {p0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 2165599
    invoke-virtual {p0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2165600
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2165601
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2165602
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2165603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2165604
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2165609
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2165610
    invoke-virtual {p0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2165611
    invoke-virtual {p0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2165612
    if-eqz v1, :cond_0

    .line 2165613
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;

    .line 2165614
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->e:LX/3Sb;

    .line 2165615
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2165616
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDeviceList"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2165595
    iget-object v0, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, -0x1aebb3f5

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->e:LX/3Sb;

    .line 2165596
    iget-object v0, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2165592
    new-instance v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;

    invoke-direct {v0}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;-><init>()V

    .line 2165593
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2165594
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2165591
    const v0, 0x53e66ba4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2165590
    const v0, -0x61630d40

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2165588
    iget-object v0, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->f:Ljava/lang/String;

    .line 2165589
    iget-object v0, p0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method
