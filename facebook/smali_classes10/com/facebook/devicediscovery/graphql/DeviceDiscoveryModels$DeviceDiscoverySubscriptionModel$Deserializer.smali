.class public final Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2165333
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;

    new-instance v1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2165334
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2165375
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2165335
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2165336
    const/4 v2, 0x0

    .line 2165337
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2165338
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2165339
    :goto_0
    move v1, v2

    .line 2165340
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2165341
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2165342
    new-instance v1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;

    invoke-direct {v1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;-><init>()V

    .line 2165343
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2165344
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2165345
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2165346
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2165347
    :cond_0
    return-object v1

    .line 2165348
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2165349
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2165350
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2165351
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2165352
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2165353
    const-string v4, "what_to_verify"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2165354
    const/4 v3, 0x0

    .line 2165355
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2165356
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2165357
    :goto_2
    move v1, v3

    .line 2165358
    goto :goto_1

    .line 2165359
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2165360
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2165361
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2165362
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2165363
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_8

    .line 2165364
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2165365
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2165366
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v5, :cond_6

    .line 2165367
    const-string p0, "command"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2165368
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_3

    .line 2165369
    :cond_7
    const-string p0, "end_point"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2165370
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 2165371
    :cond_8
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2165372
    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 2165373
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2165374
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    move v4, v3

    goto :goto_3
.end method
