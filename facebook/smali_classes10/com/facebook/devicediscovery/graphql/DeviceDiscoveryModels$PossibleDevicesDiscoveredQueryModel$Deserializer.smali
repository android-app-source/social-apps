.class public final Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2165555
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;

    new-instance v1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2165556
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2165506
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2165507
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2165508
    const/4 v2, 0x0

    .line 2165509
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 2165510
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2165511
    :goto_0
    move v1, v2

    .line 2165512
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2165513
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2165514
    new-instance v1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;

    invoke-direct {v1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;-><init>()V

    .line 2165515
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2165516
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2165517
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2165518
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2165519
    :cond_0
    return-object v1

    .line 2165520
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2165521
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 2165522
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2165523
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2165524
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 2165525
    const-string v5, "device_list"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2165526
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2165527
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_3

    .line 2165528
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_3

    .line 2165529
    const/4 v5, 0x0

    .line 2165530
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_b

    .line 2165531
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2165532
    :goto_3
    move v4, v5

    .line 2165533
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2165534
    :cond_3
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2165535
    goto :goto_1

    .line 2165536
    :cond_4
    const-string v5, "topic_tag"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2165537
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 2165538
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2165539
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2165540
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2165541
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 2165542
    :cond_7
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2165543
    :cond_8
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_a

    .line 2165544
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2165545
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2165546
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_8

    if-eqz v7, :cond_8

    .line 2165547
    const-string p0, "command"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2165548
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_4

    .line 2165549
    :cond_9
    const-string p0, "end_point"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2165550
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_4

    .line 2165551
    :cond_a
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2165552
    invoke-virtual {v0, v5, v6}, LX/186;->b(II)V

    .line 2165553
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2165554
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto/16 :goto_3

    :cond_b
    move v4, v5

    move v6, v5

    goto :goto_4
.end method
