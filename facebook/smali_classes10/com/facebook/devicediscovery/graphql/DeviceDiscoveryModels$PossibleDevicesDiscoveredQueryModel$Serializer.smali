.class public final Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2165557
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;

    new-instance v1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2165558
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2165559
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2165560
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2165561
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2165562
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2165563
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2165564
    if-eqz v2, :cond_3

    .line 2165565
    const-string v3, "device_list"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2165566
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2165567
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 2165568
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    .line 2165569
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2165570
    const/4 p0, 0x0

    invoke-virtual {v1, v4, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2165571
    if-eqz p0, :cond_0

    .line 2165572
    const-string p2, "command"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2165573
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2165574
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v4, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2165575
    if-eqz p0, :cond_1

    .line 2165576
    const-string p2, "end_point"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2165577
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2165578
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2165579
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2165580
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2165581
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2165582
    if-eqz v2, :cond_4

    .line 2165583
    const-string v3, "topic_tag"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2165584
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2165585
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2165586
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2165587
    check-cast p1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel$Serializer;->a(Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$PossibleDevicesDiscoveredQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
