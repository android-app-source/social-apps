.class public final Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2165300
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;

    new-instance v1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2165301
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2165302
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2165303
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2165304
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2165305
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2165306
    invoke-virtual {v1, v0, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 2165307
    if-eqz p0, :cond_0

    .line 2165308
    const-string p2, "seconds_before_next_discovery"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2165309
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 2165310
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2165311
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2165312
    check-cast p1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel$Serializer;->a(Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$ConnectedDeviceDiscoveredMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
