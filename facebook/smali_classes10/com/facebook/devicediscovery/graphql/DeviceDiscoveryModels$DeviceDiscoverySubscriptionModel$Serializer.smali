.class public final Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2165376
    const-class v0, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;

    new-instance v1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2165377
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2165379
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2165380
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2165381
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2165382
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2165383
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2165384
    if-eqz v2, :cond_2

    .line 2165385
    const-string p0, "what_to_verify"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2165386
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2165387
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2165388
    if-eqz p0, :cond_0

    .line 2165389
    const-string v0, "command"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2165390
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2165391
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 2165392
    if-eqz p0, :cond_1

    .line 2165393
    const-string v0, "end_point"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2165394
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2165395
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2165396
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2165397
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2165378
    check-cast p1, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel$Serializer;->a(Lcom/facebook/devicediscovery/graphql/DeviceDiscoveryModels$DeviceDiscoverySubscriptionModel;LX/0nX;LX/0my;)V

    return-void
.end method
