.class public Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;
.super LX/3Ai;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:Ljava/lang/String;


# instance fields
.field public final c:LX/03V;

.field private final d:LX/1Ad;

.field public e:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1969813
    const-class v0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;

    const-string v1, "widget"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1969814
    const-class v0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ad;LX/03V;LX/5OG;Ljava/lang/CharSequence;)V
    .locals 3
    .param p3    # LX/5OG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/CharSequence;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1969815
    invoke-direct {p0, p3, v2, v2, p4}, LX/3Ai;-><init>(Landroid/view/Menu;IILjava/lang/CharSequence;)V

    .line 1969816
    iput-object p1, p0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->d:LX/1Ad;

    .line 1969817
    iput-object p2, p0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->c:LX/03V;

    .line 1969818
    new-instance v0, LX/1Uo;

    .line 1969819
    iget-object v1, p3, LX/5OG;->c:Landroid/content/Context;

    move-object v1, v1

    .line 1969820
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1969821
    iput v2, v0, LX/1Uo;->d:I

    .line 1969822
    move-object v0, v0

    .line 1969823
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1969824
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1969825
    move-object v0, v0

    .line 1969826
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1969827
    iget-object v1, p3, LX/5OG;->c:Landroid/content/Context;

    move-object v1, v1

    .line 1969828
    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->e:LX/1aX;

    .line 1969829
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/view/MenuItem;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1969830
    iget-object v0, p0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1969831
    iget-object v1, p0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->e:LX/1aX;

    iget-object v0, p0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->d:LX/1Ad;

    sget-object v2, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    new-instance v2, LX/D9D;

    invoke-direct {v2, p0}, LX/D9D;-><init>(Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;)V

    invoke-virtual {v0, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    .line 1969832
    iget-object v0, p0, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 1969833
    return-object p0
.end method
