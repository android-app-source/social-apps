.class public Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Elu;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/facebook/http/common/FbHttpRequestProcessor;

.field private final c:LX/03V;

.field private final d:LX/Elm;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/Elu;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:LX/Em6;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2165007
    const-class v0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;LX/03V;LX/Elm;LX/Em6;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/http/common/FbHttpRequestProcessor;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/Elm;",
            "LX/Em6;",
            "LX/0Ot",
            "<",
            "Ljava/util/Set",
            "<",
            "LX/Elu;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2165008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2165009
    iput-object p1, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 2165010
    iput-object p2, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->c:LX/03V;

    .line 2165011
    iput-object p3, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->d:LX/Elm;

    .line 2165012
    iput-object p4, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->f:LX/Em6;

    .line 2165013
    iput-object p5, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->e:LX/0Ot;

    .line 2165014
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2164986
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2164987
    new-instance v0, Ljava/net/URI;

    invoke-virtual {v5}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Ljava/net/URL;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2164988
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 2164989
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const-string v2, "http.protocol.handle-redirects"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/apache/http/params/BasicHttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/apache/http/client/methods/HttpGet;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 2164990
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v0

    const-string v2, "DeepLinkNotificationUrlRequest"

    .line 2164991
    iput-object v2, v0, LX/15E;->c:Ljava/lang/String;

    .line 2164992
    move-object v0, v0

    .line 2164993
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 2164994
    iput-object v2, v0, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2164995
    move-object v0, v0

    .line 2164996
    iput-object v1, v0, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2164997
    move-object v0, v0

    .line 2164998
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2164999
    iput-object v1, v0, LX/15E;->k:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2165000
    move-object v0, v0

    .line 2165001
    new-instance v1, LX/Em0;

    invoke-direct {v1, p0}, LX/Em0;-><init>(Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;)V

    .line 2165002
    iput-object v1, v0, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2165003
    move-object v0, v0

    .line 2165004
    invoke-virtual {v0}, LX/15E;->a()LX/15D;

    move-result-object v0

    .line 2165005
    iget-object v1, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->b:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v1, v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    .line 2165006
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2164959
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v2, "/n/"

    invoke-static {v0, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2164960
    :try_start_0
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2164961
    :goto_0
    invoke-virtual {p2}, Landroid/net/Uri;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 2164962
    const-string v2, "&"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 2164963
    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    .line 2164964
    array-length v6, v4

    move v2, v3

    move-object v0, v1

    :goto_1
    if-ge v2, v6, :cond_2

    .line 2164965
    aget-object v7, v4, v2

    const/16 v8, 0x3d

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 2164966
    if-ltz v7, :cond_0

    .line 2164967
    aget-object v8, v4, v2

    invoke-virtual {v8, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    aget-object v9, v4, v2

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v9, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v8, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2164968
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2164969
    :catch_0
    move-exception v0

    .line 2164970
    iget-object v2, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->c:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".handleDeepLinkUri"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Error logging notification click"

    invoke-static {v4, v5}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v4

    .line 2164971
    iput-object v0, v4, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2164972
    move-object v0, v4

    .line 2164973
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 2164974
    :cond_0
    if-nez v0, :cond_1

    .line 2164975
    aget-object v0, v4, v2

    goto :goto_2

    .line 2164976
    :cond_1
    iget-object v7, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->c:LX/03V;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->a:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".handleDeepLinkUri"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Error parsing notification URL (found a second key with no value): "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v8

    invoke-virtual {v8}, LX/0VK;->g()LX/0VG;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/03V;->a(LX/0VG;)V

    goto :goto_2

    .line 2164977
    :cond_2
    const-string v2, ""

    invoke-virtual {v5, v2}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2164978
    if-eqz v0, :cond_4

    .line 2164979
    invoke-virtual {v5, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2164980
    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 2164981
    iget-object v0, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Elu;

    .line 2164982
    invoke-interface {v0, p1, v2}, LX/Elu;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2164983
    if-eqz v0, :cond_3

    .line 2164984
    iget-object v1, p0, Lcom/facebook/deeplinking/handler/NotificationsDeepLinkHandler;->f:LX/Em6;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/Em6;->a(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 2164985
    :goto_3
    return-object v0

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
