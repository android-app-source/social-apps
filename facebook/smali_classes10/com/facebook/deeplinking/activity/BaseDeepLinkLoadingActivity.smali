.class public abstract Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final w:Ljava/lang/String;


# instance fields
.field public p:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/Em6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/Elm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4nT;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2164765
    const-class v0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->w:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164764
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 2164766
    if-eqz p1, :cond_3

    .line 2164767
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "force_faceweb"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 2164768
    :goto_0
    iget-object v1, p0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->t:LX/Em6;

    invoke-virtual {p0}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2164769
    iget-object v4, v1, LX/Em6;->a:LX/0Zb;

    const-string v5, "unsuccessful_deeplink"

    const/4 p1, 0x1

    invoke-interface {v4, v5, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 2164770
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2164771
    if-eqz v2, :cond_4

    .line 2164772
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 2164773
    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 2164774
    const-string p1, "incoming_uri"

    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2164775
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    const-string v1, "ref"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2164776
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2164777
    const-string v1, "referral"

    invoke-interface {v5, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2164778
    :cond_0
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 2164779
    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object p1

    .line 2164780
    const-string v1, "source_component"

    invoke-virtual {p1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v5, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2164781
    :cond_1
    invoke-interface {v5}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_4

    .line 2164782
    :goto_1
    move-object v5, v5

    .line 2164783
    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/util/Map;)LX/0oG;

    .line 2164784
    const-string v5, "fallback_uri"

    invoke-virtual {v4, v5, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2164785
    invoke-virtual {v4}, LX/0oG;->d()V

    .line 2164786
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2164787
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2164788
    iget-object v0, p0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->r:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2164789
    invoke-virtual {p0}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->finish()V

    .line 2164790
    return-void

    .line 2164791
    :cond_3
    sget-object v0, LX/0ax;->cL:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public abstract a(Landroid/net/Uri;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;",
            ">;)V"
        }
    .end annotation
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2164756
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2164757
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v1, p0

    check-cast v1, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p1}, LX/Em6;->b(LX/0QB;)LX/Em6;

    move-result-object v6

    check-cast v6, LX/Em6;

    invoke-static {p1}, LX/Elm;->b(LX/0QB;)LX/Elm;

    move-result-object v7

    check-cast v7, LX/Elm;

    const/16 v0, 0x3770

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, v1, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->p:LX/0tX;

    iput-object v3, v1, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->q:LX/1Ck;

    iput-object v4, v1, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->r:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v1, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->s:LX/03V;

    iput-object v6, v1, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->t:LX/Em6;

    iput-object v7, v1, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->u:LX/Elm;

    iput-object p1, v1, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->v:LX/0Ot;

    .line 2164758
    const v0, 0x7f0303f2

    invoke-virtual {p0, v0}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->setContentView(I)V

    .line 2164759
    iget-object v1, p0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->p:LX/0tX;

    .line 2164760
    new-instance v0, LX/Elp;

    invoke-direct {v0}, LX/Elp;-><init>()V

    move-object v0, v0

    .line 2164761
    const-string v2, "url"

    invoke-virtual {p0}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Elp;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2164762
    iget-object v1, p0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->q:LX/1Ck;

    const-string v2, "DeepLinkUrlRequest"

    new-instance v3, LX/Elo;

    invoke-direct {v3, p0}, LX/Elo;-><init>(Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2164763
    return-void
.end method
