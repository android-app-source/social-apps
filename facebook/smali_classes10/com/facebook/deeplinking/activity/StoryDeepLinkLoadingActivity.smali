.class public Lcom/facebook/deeplinking/activity/StoryDeepLinkLoadingActivity;
.super Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2164792
    invoke-direct {p0}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2164793
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 2164794
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2164795
    if-nez v0, :cond_1

    .line 2164796
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->a(Landroid/net/Uri;)V

    .line 2164797
    :goto_0
    return-void

    .line 2164798
    :cond_1
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2164799
    check-cast v0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;

    invoke-virtual {v0}, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 2164800
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4c808d5

    if-eq v0, v1, :cond_3

    .line 2164801
    :cond_2
    iget-object v0, p0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nT;

    const v1, 0x7f083212

    invoke-virtual {p0, v1}, Lcom/facebook/deeplinking/activity/StoryDeepLinkLoadingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4nT;->a(Ljava/lang/String;)V

    .line 2164802
    invoke-virtual {p0, v2}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 2164803
    :cond_3
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2164804
    check-cast v0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;

    invoke-virtual {v0}, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2164805
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2164806
    sget-object v1, LX/0ax;->bz:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2164807
    iget-object v1, p0, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->r:Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/Elm;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2164808
    :cond_4
    invoke-virtual {p0, p1}, Lcom/facebook/deeplinking/activity/BaseDeepLinkLoadingActivity;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method
