.class public final Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6a914637
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2164896
    const-class v0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2164895
    const-class v0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2164893
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2164894
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2164885
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2164886
    invoke-virtual {p0}, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2164887
    invoke-virtual {p0}, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2164888
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2164889
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2164890
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2164891
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2164892
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2164882
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2164883
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2164884
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2164881
    new-instance v0, LX/Elr;

    invoke-direct {v0, p1}, LX/Elr;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2164880
    invoke-virtual {p0}, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2164867
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2164868
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2164879
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2164876
    new-instance v0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;

    invoke-direct {v0}, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;-><init>()V

    .line 2164877
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2164878
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2164875
    const v0, 0x7b10be6c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2164874
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2164871
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2164872
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2164873
    :cond_0
    iget-object v0, p0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2164869
    iget-object v0, p0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->f:Ljava/lang/String;

    .line 2164870
    iget-object v0, p0, Lcom/facebook/deeplinking/graphql/DeepLinkingGraphQlQueryFragmentsModels$GetURLInfoModel;->f:Ljava/lang/String;

    return-object v0
.end method
