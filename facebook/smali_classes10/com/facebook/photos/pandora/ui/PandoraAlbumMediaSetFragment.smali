.class public Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field public static final L:Ljava/lang/String;


# instance fields
.field public A:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/20j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/3iG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dy7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/Bol;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0zG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/9bD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/0tF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private M:LX/9FH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/3iK;

.field public O:LX/9DG;

.field private P:LX/1Qq;

.field private Q:Landroid/content/Context;

.field private R:LX/1Cq;

.field private S:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/21l",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public T:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/21l",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final U:LX/Dwm;

.field private final V:LX/Dwl;

.field public W:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

.field public X:Landroid/widget/TextView;

.field public Y:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

.field public Z:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dwa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:Lcom/facebook/common/callercontext/CallerContext;

.field public ab:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field private ac:LX/Dwn;

.field public ad:LX/8A4;

.field private ae:Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

.field public af:LX/62C;

.field private ag:LX/99T;

.field public ah:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public ai:J

.field public aj:Z

.field private ak:LX/DxF;

.field private al:LX/Dwk;

.field private final am:LX/Dwo;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dv9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0b3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxn;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/DwK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Dwp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/DxZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/D3w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1L1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/CZm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/9DH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/BFJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/1My;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/1B1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2061634
    const-class v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->L:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2061601
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2061602
    new-instance v0, LX/Dwm;

    invoke-direct {v0, p0}, LX/Dwm;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->U:LX/Dwm;

    .line 2061603
    new-instance v0, LX/Dwl;

    invoke-direct {v0, p0}, LX/Dwl;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->V:LX/Dwl;

    .line 2061604
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ai:J

    .line 2061605
    iput-boolean v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->aj:Z

    .line 2061606
    new-instance v0, LX/Dwk;

    invoke-direct {v0, p0}, LX/Dwk;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->al:LX/Dwk;

    .line 2061607
    new-instance v0, LX/Dwo;

    invoke-direct {v0, p0}, LX/Dwo;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->am:LX/Dwo;

    .line 2061608
    return-void
.end method

.method private a(LX/Dwa;)V
    .locals 1

    .prologue
    .line 2061609
    new-instance v0, LX/Dwe;

    invoke-direct {v0, p0, p1}, LX/Dwe;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;LX/Dwa;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->a:LX/0Ot;

    .line 2061610
    return-void
.end method

.method private static a(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0b3;LX/0Ot;LX/DwK;LX/0Ot;LX/Dwp;LX/0Ot;LX/DxZ;Lcom/facebook/auth/viewercontext/ViewerContext;LX/01T;LX/D3w;LX/0Ot;LX/0ad;LX/CZm;LX/9DH;LX/1DS;LX/0Ot;LX/BFJ;LX/1My;LX/0SG;LX/03V;LX/1B1;LX/0bH;LX/20j;LX/0Or;LX/3iG;LX/0Ot;LX/0Ot;LX/Bol;LX/0Ot;LX/0Ot;LX/9bD;LX/0tF;LX/9FH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;",
            "LX/0Ot",
            "<",
            "LX/Dwa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dxd;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dv9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;",
            "LX/0b3;",
            "LX/0Ot",
            "<",
            "LX/Dxn;",
            ">;",
            "LX/DwK;",
            "LX/0Ot",
            "<",
            "LX/Dxa;",
            ">;",
            "Lcom/facebook/photos/pandora/ui/AlbumMediaSetPropertyHandler;",
            "LX/0Ot",
            "<",
            "LX/Dxc;",
            ">;",
            "LX/DxZ;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/01T;",
            "LX/D3w;",
            "LX/0Ot",
            "<",
            "LX/1L1;",
            ">;",
            "LX/0ad;",
            "LX/CZm;",
            "LX/9DH;",
            "LX/1DS;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;",
            ">;",
            "LX/BFJ;",
            "LX/1My;",
            "LX/0SG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1B1;",
            "LX/0bH;",
            "LX/20j;",
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/3iG;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dy7;",
            ">;",
            "LX/Bol;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0zG;",
            ">;",
            "LX/9bD;",
            "LX/0tF;",
            "LX/9FH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2061611
    iput-object p1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->f:LX/0b3;

    iput-object p7, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->g:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->h:LX/DwK;

    iput-object p9, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->i:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->j:LX/Dwp;

    iput-object p11, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->k:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->l:LX/DxZ;

    iput-object p13, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p14, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->n:LX/01T;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->o:LX/D3w;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->q:LX/0ad;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->r:LX/CZm;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->s:LX/9DH;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->t:LX/1DS;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->u:LX/0Ot;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->v:LX/BFJ;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->w:LX/1My;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->x:LX/0SG;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->y:LX/03V;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->z:LX/1B1;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->A:LX/0bH;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->B:LX/20j;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->C:LX/0Or;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->D:LX/3iG;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->E:LX/0Ot;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->F:LX/0Ot;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->G:LX/Bol;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->H:LX/0Ot;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->I:LX/0Ot;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->J:LX/9bD;

    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->K:LX/0tF;

    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->M:LX/9FH;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 42

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v40

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    const/16 v3, 0x2ea1

    move-object/from16 v0, v40

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2ead

    move-object/from16 v0, v40

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2dff

    move-object/from16 v0, v40

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2e8a

    move-object/from16 v0, v40

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e01

    move-object/from16 v0, v40

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {v40 .. v40}, LX/0b3;->a(LX/0QB;)LX/0b3;

    move-result-object v8

    check-cast v8, LX/0b3;

    const/16 v9, 0x2eaf

    move-object/from16 v0, v40

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v40 .. v40}, LX/DwK;->a(LX/0QB;)LX/DwK;

    move-result-object v10

    check-cast v10, LX/DwK;

    const/16 v11, 0x2eaa

    move-object/from16 v0, v40

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {v40 .. v40}, LX/Dwp;->a(LX/0QB;)LX/Dwp;

    move-result-object v12

    check-cast v12, LX/Dwp;

    const/16 v13, 0x2eac

    move-object/from16 v0, v40

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const-class v14, LX/DxZ;

    move-object/from16 v0, v40

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/DxZ;

    invoke-static/range {v40 .. v40}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v15

    check-cast v15, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {v40 .. v40}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v16

    check-cast v16, LX/01T;

    invoke-static/range {v40 .. v40}, LX/D3w;->a(LX/0QB;)LX/D3w;

    move-result-object v17

    check-cast v17, LX/D3w;

    const/16 v18, 0xf2b

    move-object/from16 v0, v40

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {v40 .. v40}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v19

    check-cast v19, LX/0ad;

    const-class v20, LX/CZm;

    move-object/from16 v0, v40

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/CZm;

    const-class v21, LX/9DH;

    move-object/from16 v0, v40

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/9DH;

    invoke-static/range {v40 .. v40}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v22

    check-cast v22, LX/1DS;

    const/16 v23, 0x2ea3

    move-object/from16 v0, v40

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const-class v24, LX/BFJ;

    move-object/from16 v0, v40

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/BFJ;

    invoke-static/range {v40 .. v40}, LX/1My;->a(LX/0QB;)LX/1My;

    move-result-object v25

    check-cast v25, LX/1My;

    invoke-static/range {v40 .. v40}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v26

    check-cast v26, LX/0SG;

    invoke-static/range {v40 .. v40}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v27

    check-cast v27, LX/03V;

    invoke-static/range {v40 .. v40}, LX/1B1;->a(LX/0QB;)LX/1B1;

    move-result-object v28

    check-cast v28, LX/1B1;

    invoke-static/range {v40 .. v40}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v29

    check-cast v29, LX/0bH;

    invoke-static/range {v40 .. v40}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v30

    check-cast v30, LX/20j;

    const/16 v31, 0x123

    move-object/from16 v0, v40

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v31

    const-class v32, LX/3iG;

    move-object/from16 v0, v40

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v32

    check-cast v32, LX/3iG;

    const/16 v33, 0x12b1

    move-object/from16 v0, v40

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v33

    const/16 v34, 0x2eb2

    move-object/from16 v0, v40

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v34

    invoke-static/range {v40 .. v40}, LX/Bol;->a(LX/0QB;)LX/Bol;

    move-result-object v35

    check-cast v35, LX/Bol;

    const/16 v36, 0x2b68

    move-object/from16 v0, v40

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v36

    const/16 v37, 0x12bf

    move-object/from16 v0, v40

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v37

    const-class v38, LX/9bD;

    move-object/from16 v0, v40

    move-object/from16 v1, v38

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v38

    check-cast v38, LX/9bD;

    invoke-static/range {v40 .. v40}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v39

    check-cast v39, LX/0tF;

    const-class v41, LX/9FH;

    invoke-interface/range {v40 .. v41}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v40

    check-cast v40, LX/9FH;

    invoke-static/range {v2 .. v40}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->a(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0b3;LX/0Ot;LX/DwK;LX/0Ot;LX/Dwp;LX/0Ot;LX/DxZ;Lcom/facebook/auth/viewercontext/ViewerContext;LX/01T;LX/D3w;LX/0Ot;LX/0ad;LX/CZm;LX/9DH;LX/1DS;LX/0Ot;LX/BFJ;LX/1My;LX/0SG;LX/03V;LX/1B1;LX/0bH;LX/20j;LX/0Or;LX/3iG;LX/0Ot;LX/0Ot;LX/Bol;LX/0Ot;LX/0Ot;LX/9bD;LX/0tF;LX/9FH;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 8

    .prologue
    .line 2061612
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2061613
    :cond_0
    :goto_0
    return-void

    .line 2061614
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v3

    .line 2061615
    iget-object v4, v3, LX/Dvb;->i:LX/Dvc;

    move-object v3, v4

    .line 2061616
    invoke-virtual {v3}, LX/Dvc;->d()LX/0Px;

    move-result-object v5

    sget-object v6, LX/74S;->ALBUM_PERMALINK:LX/74S;

    const/4 v7, 0x0

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v7}, LX/Dxd;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLAlbum;)I
    .locals 1

    .prologue
    .line 2061627
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2061628
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLAlbum;->x()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    .line 2061629
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 6

    .prologue
    .line 2061617
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->M:LX/9FH;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ah:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2061618
    iget-wide v4, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v2, v4

    .line 2061619
    invoke-virtual {v0, v2, v3}, LX/9FH;->a(J)LX/9FG;

    move-result-object v0

    .line 2061620
    sget-object v1, LX/9FF;->TOP_LEVEL:LX/9FF;

    invoke-virtual {v0, v1}, LX/9FG;->a(LX/9FF;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2061621
    :goto_0
    return-void

    .line 2061622
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ah:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {v1}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v1

    .line 2061623
    iget-wide v4, v0, LX/9FG;->b:J

    move-wide v2, v4

    .line 2061624
    iput-wide v2, v1, LX/21A;->j:J

    .line 2061625
    move-object v0, v1

    .line 2061626
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ah:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    goto :goto_0
.end method

.method public static m$redex0(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 3

    .prologue
    .line 2061365
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    move-object v1, v0

    .line 2061366
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->S:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21l;

    .line 2061367
    invoke-interface {v0, v1}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 2061368
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 2061369
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->R:LX/1Cq;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 2061370
    iput-object v1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 2061371
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->P:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2061372
    invoke-static {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->q(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    .line 2061373
    return-void
.end method

.method private o()Landroid/content/Context;
    .locals 3

    .prologue
    .line 2061631
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Q:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 2061632
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e076e

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Q:Landroid/content/Context;

    .line 2061633
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Q:Landroid/content/Context;

    return-object v0
.end method

.method private p()Z
    .locals 3

    .prologue
    .line 2061630
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->q:LX/0ad;

    sget-short v1, LX/5lr;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static q(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V
    .locals 7

    .prologue
    .line 2061584
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2061585
    :goto_0
    return-void

    .line 2061586
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->w:LX/1My;

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->x:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-static {v0}, LX/11F;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v0

    .line 2061587
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->w:LX/1My;

    new-instance v2, LX/Dwg;

    invoke-direct {v2, p0}, LX/Dwg;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_0
.end method

.method public static s()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
    .locals 2

    .prologue
    .line 2061588
    new-instance v0, LX/21A;

    invoke-direct {v0}, LX/21A;-><init>()V

    const-string v1, "album_permalink"

    .line 2061589
    iput-object v1, v0, LX/21A;->c:Ljava/lang/String;

    .line 2061590
    move-object v0, v0

    .line 2061591
    const-string v1, "permalink_ufi"

    .line 2061592
    iput-object v1, v0, LX/21A;->b:Ljava/lang/String;

    .line 2061593
    move-object v0, v0

    .line 2061594
    sget-object v1, LX/21B;->STORY_PERMALINK:LX/21B;

    .line 2061595
    iput-object v1, v0, LX/21A;->e:LX/21B;

    .line 2061596
    move-object v0, v0

    .line 2061597
    sget-object v1, LX/21D;->PERMALINK:LX/21D;

    .line 2061598
    iput-object v1, v0, LX/21A;->i:LX/21D;

    .line 2061599
    move-object v0, v0

    .line 2061600
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 2061577
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->o()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9DG;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2061578
    const/4 v0, 0x1

    .line 2061579
    :goto_0
    return v0

    .line 2061580
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2061581
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    invoke-virtual {v0}, LX/Dvb;->j()V

    .line 2061582
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->a(LX/Dwa;)V

    .line 2061583
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2061497
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2061498
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2061499
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2061500
    new-instance v0, LX/Dwn;

    invoke-direct {v0, p0}, LX/Dwn;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ac:LX/Dwn;

    .line 2061501
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061502
    const-string v1, "extra_album_selected"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2061503
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061504
    const-string v1, "extra_caller_context"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->aa:Lcom/facebook/common/callercontext/CallerContext;

    .line 2061505
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061506
    const-string v1, "extra_photo_tab_mode_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Y:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2061507
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061508
    const-string v1, "owner_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ai:J

    .line 2061509
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061510
    const-string v1, "is_page"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->aj:Z

    .line 2061511
    new-instance v0, LX/99T;

    invoke-direct {v0}, LX/99T;-><init>()V

    move-object v0, v0

    .line 2061512
    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ag:LX/99T;

    .line 2061513
    const/4 v1, 0x0

    .line 2061514
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061515
    iget-boolean v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v2

    .line 2061516
    if-eqz v0, :cond_4

    .line 2061517
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061518
    const-string v1, "extra_composer_target_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ab:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2061519
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ab:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-nez v0, :cond_0

    .line 2061520
    new-instance v0, LX/89I;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061521
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2061522
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ab:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2061523
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061524
    const-string v1, "extra_pages_admin_permissions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2061525
    if-eqz v0, :cond_6

    .line 2061526
    new-instance v1, LX/8A4;

    invoke-direct {v1, v0}, LX/8A4;-><init>(Ljava/util/List;)V

    iput-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ad:LX/8A4;

    move-object v12, v0

    .line 2061527
    :goto_0
    new-instance v0, LX/DxF;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DxF;-><init>(LX/Dvb;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ak:LX/DxF;

    .line 2061528
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v13

    .line 2061529
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ak:LX/DxF;

    invoke-virtual {v13, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2061530
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2061531
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->D:LX/3iG;

    sget-object v1, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v0, v1}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->N:LX/3iK;

    .line 2061532
    new-instance v4, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment$1;

    invoke-direct {v4, p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment$1;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    .line 2061533
    new-instance v0, LX/1Cq;

    invoke-direct {v0}, LX/1Cq;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->R:LX/1Cq;

    .line 2061534
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2061535
    sget-object v1, LX/37R;->a:LX/37R;

    move-object v1, v1

    .line 2061536
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ag:LX/99T;

    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->G:LX/Bol;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, LX/BFJ;->a(Landroid/content/Context;LX/1PT;LX/1PY;LX/1QU;Ljava/lang/Runnable;Ljava/lang/String;)LX/3Ij;

    move-result-object v0

    .line 2061537
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->t:LX/1DS;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->u:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->R:LX/1Cq;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2061538
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2061539
    move-object v0, v1

    .line 2061540
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->P:LX/1Qq;

    .line 2061541
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->P:LX/1Qq;

    invoke-virtual {v13, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2061542
    const-string v0, "feedback"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2061543
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v1, :cond_7

    .line 2061544
    :cond_1
    :goto_1
    if-nez p1, :cond_8

    .line 2061545
    invoke-static {}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->s()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    .line 2061546
    :cond_2
    :goto_2
    move-object v0, v0

    .line 2061547
    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ah:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2061548
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->k()V

    .line 2061549
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->s:LX/9DH;

    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->o()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->r:LX/CZm;

    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->o()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/CZm;->a(Landroid/content/Context;)LX/CZl;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ah:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance v9, LX/Dwc;

    invoke-direct {v9, p0}, LX/Dwc;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    const/4 v10, 0x0

    new-instance v11, LX/Dwd;

    invoke-direct {v11, p0}, LX/Dwd;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    move-object v1, p0

    invoke-virtual/range {v0 .. v11}, LX/9DH;->a(Landroid/support/v4/app/Fragment;Landroid/content/Context;LX/9Bj;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZZLX/9Br;ZLX/0QK;)LX/9DG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    .line 2061550
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ah:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0, v1}, LX/9DG;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 2061551
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    .line 2061552
    iget-object v1, v0, LX/9DG;->b:LX/9CC;

    move-object v0, v1

    .line 2061553
    invoke-virtual {v13, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2061554
    const/4 v0, 0x1

    new-array v0, v0, [LX/21l;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->S:Ljava/util/Set;

    .line 2061555
    const/4 v0, 0x1

    new-array v0, v0, [LX/21l;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->T:Ljava/util/Set;

    .line 2061556
    invoke-static {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->q(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    .line 2061557
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->z:LX/1B1;

    new-instance v1, LX/Dwh;

    invoke-direct {v1, p0}, LX/Dwh;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2061558
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->z:LX/1B1;

    new-instance v1, LX/Dwi;

    invoke-direct {v1, p0}, LX/Dwi;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 2061559
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-nez v0, :cond_3

    .line 2061560
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "refetchAlbumDetails_%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Dwj;

    invoke-direct {v2, p0}, LX/Dwj;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    new-instance v3, LX/Dwb;

    invoke-direct {v3, p0}, LX/Dwb;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2061561
    :cond_3
    invoke-virtual {v13}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/62C;->a(Ljava/util/List;)LX/62C;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->af:LX/62C;

    .line 2061562
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ab:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 2061563
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061564
    const-string v4, "disable_adding_photos_to_albums"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iget-wide v6, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ai:J

    iget-boolean v8, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->aj:Z

    move-object v4, v12

    invoke-virtual/range {v1 .. v8}, LX/Dwa;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/util/ArrayList;ZJZ)V

    .line 2061565
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->al:LX/Dwk;

    invoke-virtual {v0, v1}, LX/Dwa;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2061566
    return-void

    .line 2061567
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ab:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-nez v0, :cond_5

    .line 2061568
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061569
    const-string v2, "extra_composer_target_data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ab:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    :cond_5
    move-object v12, v1

    goto/16 :goto_0

    :cond_6
    move-object v12, v0

    goto/16 :goto_0

    .line 2061570
    :cond_7
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->B:LX/20j;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/20j;->b(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 2061571
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v2}, LX/4Vp;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)LX/4Vp;

    move-result-object v2

    .line 2061572
    iput-object v1, v2, LX/4Vp;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2061573
    move-object v1, v2

    .line 2061574
    invoke-virtual {v1}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    goto/16 :goto_1

    .line 2061575
    :cond_8
    const-string v0, "feedbackLoggingParams"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2061576
    if-nez v0, :cond_2

    invoke-static {}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->s()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public final b()LX/Dwa;
    .locals 1

    .prologue
    .line 2061496
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dwa;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2061482
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2061483
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    invoke-virtual {v0, p1, p2, p3}, LX/9DG;->a(IILandroid/content/Intent;)V

    .line 2061484
    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 2061485
    const-string v0, "Updated_ALBUM"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2061486
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v1

    .line 2061487
    iput-object v0, v1, LX/Dwa;->x:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2061488
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    const v1, -0x474605cf

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2061489
    :goto_0
    return-void

    .line 2061490
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxc;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, LX/Dxc;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2061491
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->n:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_5

    const/16 v0, 0x7d0

    if-eq p1, v0, :cond_2

    const/16 v0, 0x7d1

    if-ne p1, v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2061492
    if-eqz v0, :cond_3

    .line 2061493
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0

    .line 2061494
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 2061495
    :cond_4
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1600c8ba

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2061476
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->h:LX/DwK;

    const-string v2, "InflateAlbumMediaSetFragment"

    invoke-virtual {v0, v2}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2061477
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->o()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2061478
    const v2, 0x7f0300cc

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2061479
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->h:LX/DwK;

    const-string v3, "InflateAlbumMediaSetFragment"

    invoke-virtual {v0, v3}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2061480
    const v0, 0x7f0d009c

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ae:Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

    .line 2061481
    const/16 v0, 0x2b

    const v3, 0x1d4710ca

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x66aebf62

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2061467
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2061468
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v1

    invoke-virtual {v1}, LX/Dvb;->j()V

    .line 2061469
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->a(LX/Dwa;)V

    .line 2061470
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2061471
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->P:LX/1Qq;

    invoke-interface {v1}, LX/0Vf;->dispose()V

    .line 2061472
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->c()V

    .line 2061473
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->w:LX/1My;

    invoke-virtual {v1}, LX/1My;->a()V

    .line 2061474
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2061475
    const/16 v1, 0x2b

    const v2, 0x554df0ed

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x127fadd8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2061459
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2061460
    :try_start_0
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->b()LX/Dwa;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->al:LX/Dwk;

    invoke-virtual {v0, v2}, LX/Dwa;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2061461
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2061462
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->b()V

    .line 2061463
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2061464
    const v0, 0x2bb7cd8b

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2061465
    :catch_0
    move-exception v0

    .line 2061466
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->y:LX/03V;

    sget-object v3, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->L:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onDestoryView "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x23f0b753

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2061448
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2061449
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dv9;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->U:LX/Dwm;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2061450
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->V:LX/Dwl;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2061451
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a()V

    .line 2061452
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->f:LX/0b3;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ac:LX/Dwn;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2061453
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->am:LX/Dwo;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2061454
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2061455
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->d()V

    .line 2061456
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->w:LX/1My;

    invoke-virtual {v0}, LX/1My;->d()V

    .line 2061457
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->z:LX/1B1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->A:LX/0bH;

    invoke-virtual {v0, v2}, LX/1B1;->b(LX/0b4;)V

    .line 2061458
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x5b767ea

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6f5a1df1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2061401
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2061402
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dv9;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->U:LX/Dwm;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2061403
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->V:LX/Dwl;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2061404
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->f:LX/0b3;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ac:LX/Dwn;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2061405
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->am:LX/Dwo;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2061406
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2061407
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    invoke-virtual {v0}, LX/9DG;->e()V

    .line 2061408
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->w:LX/1My;

    invoke-virtual {v0}, LX/1My;->e()V

    .line 2061409
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->z:LX/1B1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->A:LX/0bH;

    invoke-virtual {v0, v2}, LX/1B1;->a(LX/0b4;)V

    .line 2061410
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_1

    .line 2061411
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2061412
    goto :goto_1

    .line 2061413
    :cond_1
    :goto_0
    const/16 v0, 0x2b

    const v2, -0x218dcd0

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2061414
    :goto_1
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Dxn;

    .line 2061415
    iget-object v4, v2, LX/Dxn;->a:LX/0h5;

    move-object v2, v4

    .line 2061416
    if-nez v2, :cond_5

    .line 2061417
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->I:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0zG;

    invoke-interface {v2}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0h5;

    move-object v4, v2

    .line 2061418
    :goto_2
    if-eqz v4, :cond_2

    .line 2061419
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f081271

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2061420
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->WALL:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->COVER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->MOBILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->APP:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    if-eq v2, v5, :cond_1

    .line 2061421
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2061422
    invoke-static {v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->e(Lcom/facebook/graphql/model/GraphQLAlbum;)I

    move-result v2

    const v7, 0x25d6af

    if-ne v2, v7, :cond_9

    .line 2061423
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061424
    iget-object v7, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v7

    .line 2061425
    invoke-static {v0, v2}, LX/Dxo;->b(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061426
    iget-object v7, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v7

    .line 2061427
    invoke-static {v0, v2}, LX/Dxo;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    move v2, v6

    .line 2061428
    :goto_3
    iget-object v7, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ad:LX/8A4;

    if-eqz v7, :cond_8

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ad:LX/8A4;

    sget-object v7, LX/8A3;->EDIT_PROFILE:LX/8A3;

    invoke-virtual {v2, v7}, LX/8A4;->a(LX/8A3;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2061429
    :goto_4
    move v2, v6

    .line 2061430
    if-eqz v2, :cond_1

    .line 2061431
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v5

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->n:LX/01T;

    sget-object v6, LX/01T;->PAA:LX/01T;

    if-ne v2, v6, :cond_4

    const v2, 0x7f020723

    .line 2061432
    :goto_5
    iput v2, v5, LX/108;->i:I

    .line 2061433
    move-object v2, v5

    .line 2061434
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0811dd

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2061435
    iput-object v5, v2, LX/108;->j:Ljava/lang/String;

    .line 2061436
    move-object v2, v2

    .line 2061437
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 2061438
    new-instance v5, LX/Dwf;

    invoke-direct {v5, p0, v0}, LX/Dwf;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;Lcom/facebook/graphql/model/GraphQLAlbum;)V

    .line 2061439
    if-eqz v4, :cond_1

    .line 2061440
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v4, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2061441
    invoke-interface {v4, v5}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto/16 :goto_0

    .line 2061442
    :cond_4
    const v2, 0x7f02004b

    goto :goto_5

    :cond_5
    move-object v4, v2

    goto/16 :goto_2

    :cond_6
    move v2, v5

    .line 2061443
    goto :goto_3

    :cond_7
    move v6, v5

    .line 2061444
    goto :goto_4

    :cond_8
    move v6, v2

    goto :goto_4

    .line 2061445
    :cond_9
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2061446
    iget-object v5, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v2, v5

    .line 2061447
    invoke-static {v0, v2}, LX/Dxo;->b(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v6

    goto :goto_4
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2061393
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2061394
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    if-eqz v0, :cond_0

    .line 2061395
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    invoke-virtual {v0, p1}, LX/9DG;->b(Landroid/os/Bundle;)V

    .line 2061396
    :cond_0
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2061397
    const-string v0, "feedback"

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->Z:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->t()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2061398
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ah:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-eqz v0, :cond_2

    .line 2061399
    const-string v0, "feedbackLoggingParams"

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ah:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2061400
    :cond_2
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2061374
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2061375
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2061376
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    invoke-virtual {v0, p2}, LX/9DG;->a(Landroid/os/Bundle;)V

    .line 2061377
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->O:LX/9DG;

    new-instance v1, LX/2iI;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ae:Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

    invoke-direct {v1, v2}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->af:LX/62C;

    invoke-virtual {v0, p1, v1, v2}, LX/9DG;->a(Landroid/view/View;LX/0g8;Landroid/widget/BaseAdapter;)V

    .line 2061378
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ae:Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->af:LX/62C;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2061379
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ae:Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ak:LX/DxF;

    .line 2061380
    iput-object v1, v0, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->a:LX/DxF;

    .line 2061381
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ag:LX/99T;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->ae:Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

    .line 2061382
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v0, LX/99T;->a:Ljava/lang/ref/WeakReference;

    .line 2061383
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2061384
    invoke-static {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->m$redex0(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;)V

    .line 2061385
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->h:LX/DwK;

    const-string v1, "SpinnerAlbumMediaSetFragment"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2061386
    const v0, 0x7f0d0517

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->W:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    .line 2061387
    const/4 v0, 0x1

    move v0, v0

    .line 2061388
    if-eqz v0, :cond_2

    .line 2061389
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->W:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->setVisibility(I)V

    .line 2061390
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->h:LX/DwK;

    const-string v1, "SpinnerAlbumMediaSetFragment"

    invoke-virtual {v0, v1}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2061391
    :cond_2
    const v0, 0x7f0d0518

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;->X:Landroid/widget/TextView;

    .line 2061392
    return-void
.end method
