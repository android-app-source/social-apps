.class public abstract Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DxK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dv9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/DwK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1L1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dvg;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/DxJ;

.field public l:Ljava/lang/String;

.field public m:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

.field public n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

.field public o:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

.field public p:Lcom/facebook/common/callercontext/CallerContext;

.field private q:Z

.field public r:Landroid/view/View;

.field private s:LX/DxN;

.field private final t:LX/Dch;

.field private final u:LX/DxO;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2018792
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2018793
    new-instance v0, LX/DxN;

    invoke-direct {v0, p0}, LX/DxN;-><init>(Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->s:LX/DxN;

    .line 2018794
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->d()LX/Dch;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->t:LX/Dch;

    .line 2018795
    new-instance v0, LX/DxO;

    invoke-direct {v0, p0}, LX/DxO;-><init>(Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->u:LX/DxO;

    .line 2018796
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 2018791
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2018771
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2018772
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    const/16 v3, 0x2ead

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p1}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-class v5, LX/DxK;

    invoke-interface {p1, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/DxK;

    const/16 v6, 0x2e8a

    invoke-static {p1, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e05

    invoke-static {p1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2dff

    invoke-static {p1, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2eaa

    invoke-static {p1, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {p1}, LX/DwK;->a(LX/0QB;)LX/DwK;

    move-result-object v10

    check-cast v10, LX/DwK;

    const/16 v11, 0xf2b

    invoke-static {p1, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v0, 0x2e94

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->b:Ljava/lang/String;

    iput-object v5, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->c:LX/DxK;

    iput-object v6, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->e:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->f:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->g:LX/0Ot;

    iput-object v10, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->h:LX/DwK;

    iput-object v11, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->i:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->j:LX/0Ot;

    .line 2018773
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setHasOptionsMenu(Z)V

    .line 2018774
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018775
    const-string v1, "profileId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2018776
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018777
    const-string v1, "profileId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->l:Ljava/lang/String;

    .line 2018778
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018779
    const-string v1, "pandora_instance_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    .line 2018780
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018781
    const-string v1, "isDefaultLandingPage"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->q:Z

    .line 2018782
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018783
    const-string v1, "extra_photo_tab_mode_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->o:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2018784
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Dxa;->a(Landroid/content/Intent;)V

    .line 2018785
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018786
    const-string v1, "callerContext"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->p:Lcom/facebook/common/callercontext/CallerContext;

    .line 2018787
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->p:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v0, :cond_0

    .line 2018788
    const-class v0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->p:Lcom/facebook/common/callercontext/CallerContext;

    .line 2018789
    :cond_0
    return-void

    .line 2018790
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 2018770
    return-void
.end method

.method public abstract a(Ljava/lang/String;Landroid/net/Uri;)V
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract c()LX/Dcc;
.end method

.method public d()LX/Dch;
    .locals 1

    .prologue
    .line 2018769
    new-instance v0, LX/Dch;

    invoke-direct {v0, p0}, LX/Dch;-><init>(Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;)V

    return-object v0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2018759
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    if-nez v1, :cond_1

    .line 2018760
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2018761
    iget-object v2, v1, LX/Dvb;->i:LX/Dvc;

    move-object v1, v2

    .line 2018762
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2018763
    iget-object v2, v1, LX/Dvb;->i:LX/Dvc;

    move-object v1, v2

    .line 2018764
    iget-object v2, v1, LX/Dvc;->a:LX/0Px;

    move-object v1, v2

    .line 2018765
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2018766
    iget-object v2, v1, LX/Dvb;->i:LX/Dvc;

    move-object v1, v2

    .line 2018767
    iget-object v2, v1, LX/Dvc;->a:LX/0Px;

    move-object v1, v2

    .line 2018768
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x5adfefcf

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2018720
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->h:LX/DwK;

    const-string v1, "InflateUploadedMediaSetFragment"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2018721
    new-instance v8, Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2018722
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v8, v0}, Lcom/facebook/widget/CustomFrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2018723
    new-instance v9, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v9, v0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;-><init>(Landroid/content/Context;)V

    .line 2018724
    invoke-virtual {p0, v9}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a(Landroid/widget/ListView;)V

    .line 2018725
    const v0, 0x7f0d00c3

    invoke-virtual {v9, v0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setId(I)V

    .line 2018726
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->c:LX/DxK;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->c()LX/Dcc;

    move-result-object v2

    .line 2018727
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018728
    const-string v3, "pandora_two_views_row"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2018729
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2018730
    const-string v4, "pandora_non_highlight_worthy_single_photo"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dvf;

    invoke-virtual {v1, v2, v3, v4, v0}, LX/DxK;->a(LX/Dcc;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/Dvf;)LX/DxJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2018731
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    if-nez v2, :cond_1

    new-instance v2, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->l:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;-><init>(Ljava/lang/String;)V

    :goto_0
    const-string v3, "LoadScreenImagesUploads"

    iget-boolean v4, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->q:Z

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/Dvb;->a(Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;Ljava/lang/String;ZZZ)V

    .line 2018732
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->t:LX/Dch;

    invoke-virtual {v0, v1}, LX/DxJ;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2018733
    new-instance v0, LX/DxF;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    invoke-direct {v0, v1}, LX/DxF;-><init>(LX/Dvb;)V

    .line 2018734
    invoke-virtual {v9, v0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2018735
    iput-object v0, v9, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->a:LX/DxF;

    .line 2018736
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2018737
    invoke-virtual {v8, v9, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2018738
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->h:LX/DwK;

    const-string v1, "SpinnerUploadMediaFragment"

    invoke-virtual {v0, v1}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2018739
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->m:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    .line 2018740
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2018741
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2018742
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->m:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    invoke-virtual {v8, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2018743
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2018744
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->m:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->setVisibility(I)V

    .line 2018745
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->h:LX/DwK;

    const-string v1, "SpinnerUploadMediaFragment"

    invoke-virtual {v0, v1}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2018746
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a(Landroid/content/Context;)Landroid/widget/TextView;

    move-result-object v0

    .line 2018747
    if-eqz v0, :cond_2

    .line 2018748
    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->r:Landroid/view/View;

    .line 2018749
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->r:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2018750
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2018751
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2018752
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->r:Landroid/view/View;

    invoke-virtual {v8, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2018753
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->h:LX/DwK;

    const-string v1, "InflateUploadedMediaSetFragment"

    invoke-virtual {v0, v1}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2018754
    const v0, 0x70dd05b0

    invoke-static {v0, v7}, LX/02F;->f(II)V

    return-object v8

    .line 2018755
    :cond_1
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    goto/16 :goto_0

    .line 2018756
    :cond_2
    new-instance v0, Landroid/view/ViewStub;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030ef6

    invoke-direct {v0, v1, v2}, Landroid/view/ViewStub;-><init>(Landroid/content/Context;I)V

    .line 2018757
    new-instance v1, LX/DxM;

    invoke-direct {v1, p0}, LX/DxM;-><init>(Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V

    .line 2018758
    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->r:Landroid/view/View;

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4f1a99a1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2018716
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2018717
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->t:LX/Dch;

    invoke-virtual {v1, v2}, LX/DxJ;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2018718
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    invoke-virtual {v1}, LX/Dvb;->j()V

    .line 2018719
    const/16 v1, 0x2b

    const v2, 0x3c6b790a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x749906fa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2018687
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2018688
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dv9;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->s:LX/DxN;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2018689
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->u:LX/DxO;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2018690
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a()V

    .line 2018691
    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    if-nez v4, :cond_0

    .line 2018692
    :goto_0
    const/16 v0, 0x2b

    const v2, -0x76395527

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2018693
    :cond_0
    new-instance v5, LX/9lP;

    iget-object v6, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->l:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->b:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-lez v4, :cond_1

    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->b:Ljava/lang/String;

    .line 2018694
    :goto_1
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v7, v7

    .line 2018695
    const-string v8, "friendship_status"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    .line 2018696
    iget-object v8, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 2018697
    const-string v9, "subscribe_status"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v8

    invoke-direct {v5, v6, v4, v7, v8}, LX/9lP;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2018698
    new-instance v6, LX/74X;

    invoke-virtual {v5}, LX/9lP;->g()LX/9lQ;

    move-result-object v4

    invoke-virtual {v4}, LX/9lQ;->name()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->l:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->b:Ljava/lang/String;

    iget-object v8, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2018699
    iget-object v9, v8, LX/Dvb;->i:LX/Dvc;

    move-object v8, v9

    .line 2018700
    invoke-virtual {v8}, LX/Dvc;->c()I

    move-result v8

    invoke-direct {v6, v4, v5, v7, v8}, LX/74X;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2018701
    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/73w;

    .line 2018702
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2018703
    const-string v7, "session_id"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/73w;->a(Ljava/lang/String;)V

    .line 2018704
    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/73w;

    .line 2018705
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 2018706
    const-string v7, "relationship_type"

    iget-object v8, v6, LX/74X;->a:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2018707
    const-string v7, "profile_id"

    iget-object v8, v6, LX/74X;->b:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2018708
    const-string v7, "viewer_id"

    iget-object v8, v6, LX/74X;->c:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2018709
    const-string v7, "photos_uploaded"

    iget-object v8, v6, LX/74X;->d:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2018710
    move-object v7, v5

    .line 2018711
    sget-object v8, LX/74R;->PHOTOS_UPLOADED_LOADING:LX/74R;

    .line 2018712
    iget-object v5, v6, LX/74X;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2018713
    invoke-virtual {v7, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v4, v8, v7, v5}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2018714
    goto/16 :goto_0

    .line 2018715
    :cond_1
    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->l:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4b156d42

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2018683
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2018684
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dv9;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->s:LX/DxN;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2018685
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->u:LX/DxO;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2018686
    const/16 v0, 0x2b

    const v2, 0x203afaed

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
