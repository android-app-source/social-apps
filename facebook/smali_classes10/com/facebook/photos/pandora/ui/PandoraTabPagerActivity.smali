.class public Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f1;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final F:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DwF;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/5lu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private G:LX/0h5;

.field private H:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

.field private I:LX/1LD;

.field public p:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Hz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/DwK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/DwJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/2cj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/1L1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2061864
    const-class v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    const-string v1, "photos_tabs"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->F:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2061863
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;Ljava/lang/String;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/DwK;LX/DwJ;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/2cj;LX/1L1;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/5lu;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dxb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8Hz;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9at;",
            ">;",
            "LX/DwK;",
            "LX/DwJ;",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            "LX/2cj;",
            "LX/1L1;",
            "LX/0Ot",
            "<",
            "LX/Dxc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DwF;",
            ">;",
            "LX/0ad;",
            "LX/5lu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2061862
    iput-object p1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->p:Ljava/lang/String;

    iput-object p2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->q:LX/0Or;

    iput-object p3, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->r:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->s:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->t:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->u:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->v:LX/DwK;

    iput-object p8, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->w:LX/DwJ;

    iput-object p9, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->x:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p10, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->y:LX/2cj;

    iput-object p11, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->z:LX/1L1;

    iput-object p12, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->A:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->B:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->C:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->D:LX/0ad;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->E:LX/5lu;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 18

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;

    invoke-static/range {v17 .. v17}, LX/0dG;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/16 v3, 0x12cb

    move-object/from16 v0, v17

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2eab

    move-object/from16 v0, v17

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x259a

    move-object/from16 v0, v17

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x455

    move-object/from16 v0, v17

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2dfd

    move-object/from16 v0, v17

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {v17 .. v17}, LX/DwK;->a(LX/0QB;)LX/DwK;

    move-result-object v8

    check-cast v8, LX/DwK;

    invoke-static/range {v17 .. v17}, LX/DwJ;->a(LX/0QB;)LX/DwJ;

    move-result-object v9

    check-cast v9, LX/DwJ;

    invoke-static/range {v17 .. v17}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v10

    check-cast v10, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static/range {v17 .. v17}, LX/2cj;->a(LX/0QB;)LX/2cj;

    move-result-object v11

    check-cast v11, LX/2cj;

    invoke-static/range {v17 .. v17}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v12

    check-cast v12, LX/1L1;

    const/16 v13, 0x2eac

    move-object/from16 v0, v17

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x122d

    move-object/from16 v0, v17

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x2e9b

    move-object/from16 v0, v17

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v17 .. v17}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v16

    check-cast v16, LX/0ad;

    invoke-static/range {v17 .. v17}, LX/5lu;->a(LX/0QB;)LX/5lu;

    move-result-object v17

    check-cast v17, LX/5lu;

    invoke-static/range {v1 .. v17}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->a(Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;Ljava/lang/String;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/DwK;LX/DwJ;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/2cj;LX/1L1;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/5lu;)V

    return-void
.end method

.method public static b(Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;I)V
    .locals 1

    .prologue
    .line 2061859
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->getRequestedOrientation()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 2061860
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->setRequestedOrientation(I)V

    .line 2061861
    :cond_0
    return-void
.end method

.method private l()Z
    .locals 1

    .prologue
    .line 2061858
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->H:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->H:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Z
    .locals 1

    .prologue
    .line 2061857
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->H:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->H:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->H:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2061856
    sget-object v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->F:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2061755
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "owner_id"

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->p:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2061756
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v2

    .line 2061757
    const-string v3, "profile_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2061758
    return-object v2
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2061786
    invoke-static {p0, p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2061787
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->v:LX/DwK;

    .line 2061788
    iget-object v1, v0, LX/DwK;->a:LX/11i;

    sget-object v2, LX/DwM;->a:LX/DwL;

    invoke-interface {v1, v2}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 2061789
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->v:LX/DwK;

    const-string v1, "InflateTabPagerActivity"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2061790
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2061791
    const v0, 0x7f030ef7

    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->setContentView(I)V

    .line 2061792
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 2061793
    const-string v0, "owner_id"

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->p:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2061794
    const-string v0, "profile_name"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2061795
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->p:Ljava/lang/String;

    invoke-static {v3, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2061796
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2061797
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->w:LX/DwJ;

    if-eqz v0, :cond_0

    .line 2061798
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->w:LX/DwJ;

    const/4 v4, 0x0

    .line 2061799
    iput v4, v0, LX/DwJ;->a:I

    .line 2061800
    iput v4, v0, LX/DwJ;->b:I

    .line 2061801
    iput v4, v0, LX/DwJ;->c:I

    .line 2061802
    :cond_0
    const-string v0, "extra_photo_tab_mode_params"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->H:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2061803
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->v:LX/DwK;

    const-string v4, "InflateTabPagerActivity"

    invoke-virtual {v0, v4}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2061804
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->v:LX/DwK;

    const-string v4, "InflateTitleBar"

    invoke-virtual {v0, v4}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2061805
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2061806
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->G:LX/0h5;

    .line 2061807
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->G:LX/0h5;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, LX/0h5;->setHasBackButton(Z)V

    .line 2061808
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->G:LX/0h5;

    new-instance v4, LX/Dws;

    invoke-direct {v4, p0}, LX/Dws;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;)V

    invoke-interface {v0, v4}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2061809
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->l()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2061810
    :cond_1
    const-string v0, "title"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2061811
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2061812
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->G:LX/0h5;

    invoke-interface {v2, v0}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2061813
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->p:Ljava/lang/String;

    invoke-static {v3, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2061814
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->E:LX/5lu;

    .line 2061815
    iget-object v2, v0, LX/5lu;->b:Ljava/lang/Boolean;

    if-nez v2, :cond_3

    .line 2061816
    iget-object v2, v0, LX/5lu;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v4, LX/5lr;->h:S

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, LX/5lu;->b:Ljava/lang/Boolean;

    .line 2061817
    :cond_3
    iget-object v2, v0, LX/5lu;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v0, v2

    .line 2061818
    if-eqz v0, :cond_4

    .line 2061819
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->G:LX/0h5;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f08128c

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2061820
    :cond_4
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v2, 0x7f0201d0

    .line 2061821
    iput v2, v0, LX/108;->i:I

    .line 2061822
    move-object v0, v0

    .line 2061823
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0811d5

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2061824
    iput-object v2, v0, LX/108;->j:Ljava/lang/String;

    .line 2061825
    move-object v0, v0

    .line 2061826
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2061827
    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->G:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v2, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2061828
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->G:LX/0h5;

    new-instance v2, LX/Dwt;

    invoke-direct {v2, p0}, LX/Dwt;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;)V

    invoke-interface {v0, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2061829
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->getRequestedOrientation()I

    move-result v0

    .line 2061830
    new-instance v2, LX/Dwu;

    invoke-direct {v2, p0, v0}, LX/Dwu;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;I)V

    iput-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->I:LX/1LD;

    .line 2061831
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->z:LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->I:LX/1LD;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2061832
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->v:LX/DwK;

    const-string v2, "InflateTitleBar"

    invoke-virtual {v0, v2}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2061833
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->v:LX/DwK;

    const-string v2, "AttachTabPagerFragment"

    invoke-virtual {v0, v2}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2061834
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 2061835
    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v2

    if-lez v2, :cond_7

    .line 2061836
    const v2, 0x7f0d002f

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 2061837
    instance-of v2, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    if-eqz v2, :cond_7

    .line 2061838
    :goto_2
    return-void

    .line 2061839
    :cond_6
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2061840
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->G:LX/0h5;

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2061841
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    sget-object v4, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->F:Lcom/facebook/common/callercontext/CallerContext;

    .line 2061842
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_b

    const/4 v5, 0x1

    :goto_3
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 2061843
    if-nez v2, :cond_8

    .line 2061844
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2061845
    :cond_8
    const-string v5, "userId"

    invoke-virtual {v2, v5, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2061846
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 2061847
    const-string v5, "userName"

    invoke-virtual {v2, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2061848
    :cond_9
    const-string v5, "callerContext"

    invoke-virtual {v2, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2061849
    new-instance v5, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    invoke-direct {v5}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;-><init>()V

    .line 2061850
    invoke-virtual {v5, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2061851
    move-object v1, v5

    .line 2061852
    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const v3, 0x7f0d002f

    const-class v4, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v1, v4}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->c()I

    .line 2061853
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2061854
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->v:LX/DwK;

    const-string v1, "AttachTabPagerFragment"

    invoke-virtual {v0, v1}, LX/DwK;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_a
    move-object v1, v0

    goto/16 :goto_0

    .line 2061855
    :cond_b
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2061768
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    if-nez p2, :cond_1

    .line 2061769
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "472084026295600"

    .line 2061770
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2061771
    move-object v0, v0

    .line 2061772
    invoke-virtual {v0, p0}, LX/0gt;->a(Landroid/content/Context;)V

    .line 2061773
    :cond_0
    :goto_0
    return-void

    .line 2061774
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2061775
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxc;

    invoke-virtual {v0, p0, p1, p2, p3}, LX/Dxc;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2061776
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->finish()V

    goto :goto_0

    .line 2061777
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .line 2061778
    :cond_3
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2061779
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->x:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2061780
    :sswitch_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->y:LX/2cj;

    invoke-virtual {v0, p1, p2, p3}, LX/2cj;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2061781
    :sswitch_2
    invoke-virtual {p0, p2, p3}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2061782
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->finish()V

    goto :goto_0

    .line 2061783
    :sswitch_3
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->D:LX/0ad;

    sget-short v1, LX/5lr;->a:S

    invoke-interface {v0, v1, v6}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2061784
    const-string v0, "extra_album"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2061785
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DwF;

    sget-object v3, LX/8AB;->ALBUM:LX/8AB;

    sget-object v4, LX/21D;->ALBUM:LX/21D;

    const-string v5, "album"

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, LX/DwF;->a(Landroid/app/Activity;Lcom/facebook/graphql/model/GraphQLAlbum;LX/8AB;LX/21D;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x7d2

    invoke-interface {v7, v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x3ec -> :sswitch_1
        0x6dc -> :sswitch_0
        0x26b9 -> :sswitch_2
    .end sparse-switch
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2061763
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 2061764
    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2061765
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->finish()V

    .line 2061766
    :goto_0
    return-void

    .line 2061767
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2d4e6157

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2061759
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->z:LX/1L1;

    if-eqz v1, :cond_0

    .line 2061760
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->z:LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerActivity;->I:LX/1LD;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2061761
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2061762
    const/16 v1, 0x23

    const v2, 0x370f532f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
