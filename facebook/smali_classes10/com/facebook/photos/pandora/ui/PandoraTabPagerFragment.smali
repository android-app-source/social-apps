.class public Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0yL;


# static fields
.field public static final m:Ljava/lang/String;


# instance fields
.field public A:I

.field private B:I

.field public a:LX/Dwx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/74I;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DwK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/EQj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/EQh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Dwr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2TI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2TH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/5lu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Landroid/support/v4/view/ViewPager;

.field public o:LX/Dww;

.field private p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field private q:LX/Dwz;

.field public r:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:LX/ERA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;

.field public u:LX/9lP;

.field private v:Lcom/facebook/common/callercontext/CallerContext;

.field public w:Z

.field public x:Z

.field public y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2062188
    const-class v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2062189
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2062190
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->A:I

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2062226
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const v4, 0x7f0d00bc

    invoke-virtual {v0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2062227
    if-nez v0, :cond_1

    .line 2062228
    :cond_0
    :goto_0
    return-void

    .line 2062229
    :cond_1
    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2062230
    if-gt p1, v7, :cond_0

    .line 2062231
    iget-boolean v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->x:Z

    if-eqz v4, :cond_3

    .line 2062232
    iget-boolean v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->y:Z

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->z:Z

    if-eqz v4, :cond_2

    move v4, v2

    .line 2062233
    :goto_1
    if-ne v4, p1, :cond_3

    .line 2062234
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08128b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2062235
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2062236
    move-object v1, v1

    .line 2062237
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2062238
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2062239
    new-instance v1, LX/Dx2;

    invoke-direct {v1, p0}, LX/Dx2;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2062240
    goto :goto_0

    :cond_2
    move v4, v3

    .line 2062241
    goto :goto_1

    .line 2062242
    :cond_3
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2062243
    const-string v5, "userId"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2062244
    iget-object v5, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->b:Ljava/lang/String;

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2062245
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "extra_cancel_button_enabled"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 2062246
    if-nez v4, :cond_9

    .line 2062247
    if-ge p1, v6, :cond_b

    .line 2062248
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "disable_camera_roll"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 2062249
    :goto_2
    if-ne p1, v6, :cond_4

    move v4, v2

    .line 2062250
    :cond_4
    if-ne p1, v7, :cond_a

    .line 2062251
    iget-object v5, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->h:LX/Dwr;

    iget-object v6, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->r:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-virtual {v5, v6}, LX/Dwr;->a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)LX/Dwq;

    move-result-object v5

    sget-object v6, LX/Dwq;->SYNC:LX/Dwq;

    if-ne v5, v6, :cond_c

    const/4 v5, 0x1

    :goto_3
    move v5, v5

    .line 2062252
    if-eqz v5, :cond_5

    move v3, v4

    .line 2062253
    :goto_4
    if-eqz v3, :cond_6

    :goto_5
    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    goto/16 :goto_0

    :cond_5
    move v8, v3

    move v3, v2

    move v2, v8

    .line 2062254
    goto :goto_4

    .line 2062255
    :cond_6
    const/4 v6, 0x1

    .line 2062256
    if-eqz v2, :cond_d

    const v1, 0x7f020bf6

    .line 2062257
    :goto_6
    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->l:LX/5lu;

    .line 2062258
    iget-object v4, v3, LX/5lu;->d:Ljava/lang/Integer;

    if-nez v4, :cond_7

    .line 2062259
    iget-object v4, v3, LX/5lu;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0ad;

    sget v5, LX/5lr;->g:I

    const/4 v2, 0x0

    invoke-interface {v4, v5, v2}, LX/0ad;->a(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, v3, LX/5lu;->d:Ljava/lang/Integer;

    .line 2062260
    :cond_7
    iget-object v4, v3, LX/5lu;->d:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move v4, v4

    .line 2062261
    packed-switch v4, :pswitch_data_0

    .line 2062262
    const/4 v4, 0x0

    :goto_7
    move v3, v4

    .line 2062263
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    .line 2062264
    iput v1, v4, LX/108;->i:I

    .line 2062265
    move-object v1, v4

    .line 2062266
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0811d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2062267
    iput-object v4, v1, LX/108;->j:Ljava/lang/String;

    .line 2062268
    move-object v1, v1

    .line 2062269
    if-eqz v3, :cond_8

    .line 2062270
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2062271
    iput-object v3, v1, LX/108;->g:Ljava/lang/String;

    .line 2062272
    move-object v3, v1

    .line 2062273
    iput-boolean v6, v3, LX/108;->q:Z

    .line 2062274
    move-object v3, v3

    .line 2062275
    const/4 v4, -0x2

    .line 2062276
    iput v4, v3, LX/108;->h:I

    .line 2062277
    move-object v3, v3

    .line 2062278
    iput-boolean v6, v3, LX/108;->t:Z

    .line 2062279
    :cond_8
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    move-object v1, v1

    .line 2062280
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    goto :goto_5

    .line 2062281
    :cond_9
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08128a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2062282
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2062283
    move-object v1, v1

    .line 2062284
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2062285
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2062286
    new-instance v1, LX/Dx1;

    invoke-direct {v1, p0}, LX/Dx1;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto/16 :goto_0

    :cond_a
    move v2, v3

    move v3, v4

    goto/16 :goto_4

    :cond_b
    move v4, v3

    goto/16 :goto_2

    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 2062287
    :cond_d
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->l:LX/5lu;

    .line 2062288
    iget-object v3, v1, LX/5lu;->c:Ljava/lang/Integer;

    if-nez v3, :cond_e

    .line 2062289
    iget-object v3, v1, LX/5lu;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0ad;

    sget v4, LX/5lr;->f:I

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v1, LX/5lu;->c:Ljava/lang/Integer;

    .line 2062290
    :cond_e
    iget-object v3, v1, LX/5lu;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v3, v3

    .line 2062291
    packed-switch v3, :pswitch_data_1

    .line 2062292
    const v3, 0x7f0201d0

    :goto_8
    move v1, v3

    .line 2062293
    goto/16 :goto_6

    .line 2062294
    :pswitch_0
    const v4, 0x7f0811d6

    goto/16 :goto_7

    .line 2062295
    :pswitch_1
    const v4, 0x7f0811d5

    goto/16 :goto_7

    .line 2062296
    :pswitch_2
    const/4 v3, 0x0

    goto :goto_8

    .line 2062297
    :pswitch_3
    const v3, 0x7f020958

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;)V
    .locals 9

    .prologue
    .line 2062191
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->f:LX/EQj;

    invoke-virtual {v0}, LX/EQj;->a()Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    move-result-object v0

    .line 2062192
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->r:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2062193
    :cond_0
    :goto_0
    return-void

    .line 2062194
    :cond_1
    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->r:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2062195
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->o:LX/Dww;

    .line 2062196
    iget-object v2, v1, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-static {v2, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2062197
    :goto_1
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->r:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2062198
    iget-object v2, v1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    move-object v1, v2

    .line 2062199
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->r:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2062200
    iget-object v2, v1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->c:Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;

    move-object v1, v2

    .line 2062201
    iget-boolean v2, v1, Lcom/facebook/vault/momentsupsell/model/MomentsAppInterstitialInfo;->j:Z

    move v1, v2

    .line 2062202
    if-nez v1, :cond_2

    .line 2062203
    new-instance v1, LX/ERA;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/ERA;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->s:LX/ERA;

    .line 2062204
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->s:LX/ERA;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->r:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    const/4 v8, 0x1

    .line 2062205
    iget-object v3, v1, LX/ERA;->a:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030b2e

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2062206
    const v3, 0x7f0d1c2a

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;

    .line 2062207
    invoke-virtual {v3, v2}, Lcom/facebook/vault/momentsupsell/ui/MomentsAppPromotionView;->a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)V

    .line 2062208
    const v3, 0x7f0d1c2b

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 2062209
    new-instance v5, LX/0wM;

    iget-object v6, v1, LX/ERA;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    .line 2062210
    const v6, 0x7f020818

    const v7, -0xb1a99b

    invoke-virtual {v5, v6, v7}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2062211
    new-instance v5, LX/ER9;

    invoke-direct {v5, v1}, LX/ER9;-><init>(LX/ERA;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2062212
    iget-object v3, v1, LX/ERA;->b:LX/0ht;

    invoke-virtual {v3, v4}, LX/0ht;->d(Landroid/view/View;)V

    .line 2062213
    iget-object v3, v1, LX/ERA;->b:LX/0ht;

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {v3, v4}, LX/0ht;->b(F)V

    .line 2062214
    iget-object v3, v1, LX/ERA;->b:LX/0ht;

    invoke-virtual {v3, v8}, LX/0ht;->d(Z)V

    .line 2062215
    iget-object v3, v1, LX/ERA;->b:LX/0ht;

    .line 2062216
    iput-boolean v8, v3, LX/0ht;->w:Z

    .line 2062217
    :cond_2
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->b()V

    .line 2062218
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-static {p0, v1}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->a$redex0(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;I)V

    .line 2062219
    iget-boolean v1, v0, Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;->f:Z

    move v0, v1

    .line 2062220
    if-nez v0, :cond_0

    .line 2062221
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->j:LX/2TI;

    const-string v1, "OFF"

    invoke-virtual {v0, v1}, LX/2TI;->a(Ljava/lang/String;)V

    .line 2062222
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->k:LX/2TH;

    invoke-virtual {v0}, LX/2TH;->a()V

    .line 2062223
    goto/16 :goto_0

    .line 2062224
    :cond_3
    iput-object v0, v1, LX/Dww;->o:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    .line 2062225
    invoke-virtual {v1}, LX/0gG;->kV_()V

    goto/16 :goto_1
.end method

.method public static d(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;I)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2062298
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->x:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 2062299
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->y:Z

    if-eqz v3, :cond_1

    move v3, v1

    .line 2062300
    :goto_1
    iget-boolean v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->w:Z

    if-eqz v4, :cond_2

    .line 2062301
    :goto_2
    iget-boolean v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->y:Z

    if-eqz v4, :cond_4

    iget-boolean v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->z:Z

    if-eqz v4, :cond_4

    .line 2062302
    if-nez p1, :cond_3

    .line 2062303
    const-string v0, "Suggested Photos"

    .line 2062304
    :goto_3
    return-object v0

    :cond_0
    move v0, v2

    .line 2062305
    goto :goto_0

    :cond_1
    move v3, v2

    .line 2062306
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2062307
    goto :goto_2

    .line 2062308
    :cond_3
    if-ne p1, v2, :cond_7

    iget-boolean v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->x:Z

    if-eqz v2, :cond_7

    .line 2062309
    const-string v0, "camera_roll"

    goto :goto_3

    .line 2062310
    :cond_4
    iget-boolean v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->y:Z

    if-eqz v4, :cond_6

    iget-boolean v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->z:Z

    if-nez v4, :cond_6

    .line 2062311
    if-nez p1, :cond_5

    .line 2062312
    const-string v0, "camera_roll"

    goto :goto_3

    .line 2062313
    :cond_5
    if-ne p1, v2, :cond_7

    .line 2062314
    const-string v0, "Suggested Photos"

    goto :goto_3

    .line 2062315
    :cond_6
    iget-boolean v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->y:Z

    if-nez v2, :cond_7

    if-nez p1, :cond_7

    iget-boolean v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->x:Z

    if-eqz v2, :cond_7

    .line 2062316
    const-string v0, "camera_roll"

    goto :goto_3

    .line 2062317
    :cond_7
    add-int/lit8 v2, v3, 0x0

    .line 2062318
    add-int/2addr v0, v2

    .line 2062319
    rsub-int/lit8 v2, v0, 0x2

    if-ne p1, v2, :cond_8

    iget-boolean v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->w:Z

    if-eqz v2, :cond_8

    .line 2062320
    const-string v0, "photos_of"

    goto :goto_3

    .line 2062321
    :cond_8
    add-int/2addr v0, v1

    .line 2062322
    rsub-int/lit8 v1, v0, 0x3

    if-ne p1, v1, :cond_9

    .line 2062323
    const-string v0, "photo_uploads"

    goto :goto_3

    .line 2062324
    :cond_9
    rsub-int/lit8 v1, v0, 0x4

    if-ne p1, v1, :cond_a

    .line 2062325
    const-string v0, "albums"

    goto :goto_3

    .line 2062326
    :cond_a
    rsub-int/lit8 v0, v0, 0x5

    if-ne p1, v0, :cond_b

    .line 2062327
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->h:LX/Dwr;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->r:Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;

    invoke-virtual {v0, v1}, LX/Dwr;->a(Lcom/facebook/vault/momentsupsell/model/MomentsAppInfo;)LX/Dwq;

    move-result-object v0

    .line 2062328
    sget-object v1, LX/Dx3;->a:[I

    invoke-virtual {v0}, LX/Dwq;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2062329
    sget-object v1, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->m:Ljava/lang/String;

    const-string v2, "Unknown sync tab state: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2062330
    const-string v0, ""

    :goto_4
    move-object v0, v0

    .line 2062331
    goto :goto_3

    .line 2062332
    :cond_b
    const/4 v0, 0x0

    goto :goto_3

    .line 2062333
    :pswitch_0
    const-string v0, ""

    goto :goto_4

    .line 2062334
    :pswitch_1
    const-string v0, "sync"

    goto :goto_4

    .line 2062335
    :pswitch_2
    const-string v0, "loading"

    goto :goto_4

    .line 2062336
    :pswitch_3
    const-string v0, "moments_promotion"

    goto :goto_4

    .line 2062337
    :pswitch_4
    const-string v0, "moments_segue"

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 2062182
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2062183
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;

    const-class v3, LX/Dwx;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Dwx;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/16 v5, 0x2e05

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2e0f

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/DwK;->a(LX/0QB;)LX/DwK;

    move-result-object v7

    check-cast v7, LX/DwK;

    invoke-static {v0}, LX/EQj;->a(LX/0QB;)LX/EQj;

    move-result-object v8

    check-cast v8, LX/EQj;

    invoke-static {v0}, LX/EQh;->a(LX/0QB;)LX/EQh;

    move-result-object v9

    check-cast v9, LX/EQh;

    invoke-static {v0}, LX/Dwr;->b(LX/0QB;)LX/Dwr;

    move-result-object v10

    check-cast v10, LX/Dwr;

    invoke-static {v0}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v11

    check-cast v11, LX/0yc;

    invoke-static {v0}, LX/2TI;->a(LX/0QB;)LX/2TI;

    move-result-object v12

    check-cast v12, LX/2TI;

    invoke-static {v0}, LX/2TH;->a(LX/0QB;)LX/2TH;

    move-result-object p1

    check-cast p1, LX/2TH;

    invoke-static {v0}, LX/5lu;->b(LX/0QB;)LX/5lu;

    move-result-object v0

    check-cast v0, LX/5lu;

    iput-object v3, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->a:LX/Dwx;

    iput-object v4, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->b:Ljava/lang/String;

    iput-object v5, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->e:LX/DwK;

    iput-object v8, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->f:LX/EQj;

    iput-object v9, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->g:LX/EQh;

    iput-object v10, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->h:LX/Dwr;

    iput-object v11, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->i:LX/0yc;

    iput-object v12, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->j:LX/2TI;

    iput-object p1, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->k:LX/2TH;

    iput-object v0, v2, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->l:LX/5lu;

    .line 2062184
    return-void
.end method

.method public final e_(Z)V
    .locals 1

    .prologue
    .line 2062185
    if-nez p1, :cond_0

    .line 2062186
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->o:LX/Dww;

    invoke-virtual {v0}, LX/0gG;->kV_()V

    .line 2062187
    :cond_0
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 2062181
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7

    .prologue
    .line 2062168
    iget v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->B:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 2062169
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->B:I

    .line 2062170
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062171
    const-string v1, "userId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2062172
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062173
    const-string v2, "userName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2062174
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->a:LX/Dwx;

    .line 2062175
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2062176
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->v:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v6, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->t:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, LX/Dwx;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;LX/0gc;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)LX/Dww;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->o:LX/Dww;

    .line 2062177
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->o:LX/Dww;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2062178
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2062179
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->A:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 2062180
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x64a83039

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2062108
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->e:LX/DwK;

    const-string v1, "InflateTabPagerFragment"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2062109
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062110
    const-string v1, "userId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2062111
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062112
    const-string v2, "userName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2062113
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062114
    const-string v3, "callerContext"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->v:Lcom/facebook/common/callercontext/CallerContext;

    .line 2062115
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062116
    const-string v3, "session_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->t:Ljava/lang/String;

    .line 2062117
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->t:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062118
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {}, LX/74I;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->t:Ljava/lang/String;

    .line 2062119
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062120
    const-string v3, "session_id"

    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2062121
    :cond_0
    new-instance v0, LX/9lP;

    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->b:Ljava/lang/String;

    .line 2062122
    iget-object v4, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2062123
    const-string v5, "friendship_status"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    .line 2062124
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2062125
    const-string v6, "subscribe_status"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, LX/9lP;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->u:LX/9lP;

    .line 2062126
    new-instance v8, Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2062127
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2062128
    new-instance v0, Landroid/support/v4/view/ViewPager;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    .line 2062129
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    const v3, 0x7f0d00bf

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setId(I)V

    .line 2062130
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->a:LX/Dwx;

    .line 2062131
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2062132
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->v:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v6, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->t:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, LX/Dwx;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;LX/0gc;Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)LX/Dww;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->o:LX/Dww;

    .line 2062133
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->o:LX/Dww;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2062134
    new-instance v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2062135
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2062136
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    new-instance v2, LX/Dwy;

    invoke-direct {v2, p0, v1}, LX/Dwy;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;Ljava/lang/String;)V

    .line 2062137
    iput-object v2, v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->l:LX/0hc;

    .line 2062138
    new-instance v0, LX/Dx0;

    invoke-direct {v0, p0}, LX/Dx0;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->q:LX/Dwz;

    .line 2062139
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->e:LX/DwK;

    const-string v1, "InflateTabPagerFragment"

    invoke-virtual {v0, v1}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2062140
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->e:LX/DwK;

    const-string v1, "AttachTabPagerSubfragments"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2062141
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062142
    const-string v1, "has_tagged_mediaset"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->w:Z

    .line 2062143
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062144
    const-string v1, "extra_should_merge_camera_roll"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->x:Z

    .line 2062145
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062146
    const-string v1, "extra_should_show_suggested_photos"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->y:Z

    .line 2062147
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062148
    const-string v1, "extra_should_show_suggested_photos_before_camera_roll"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->z:Z

    .line 2062149
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2062150
    const/16 v1, 0x30

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 2062151
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    invoke-virtual {v8, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2062152
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2062153
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v8, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2062154
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->e:LX/DwK;

    const-string v1, "AttachTabPagerSubfragments"

    invoke-virtual {v0, v1}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2062155
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062156
    const-string v1, "extra_launch_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2062157
    const/4 v0, -0x1

    .line 2062158
    const-string v2, "tab"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2062159
    :try_start_0
    const-string v2, "tab"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2062160
    :cond_1
    :goto_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2062161
    const-string v2, "land_on_uploads_tab"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 2062162
    if-eqz v1, :cond_2

    .line 2062163
    const/4 v0, 0x1

    .line 2062164
    :cond_2
    move v0, v0

    .line 2062165
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->o:LX/Dww;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 2062166
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->n:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 2062167
    :cond_3
    const/4 v0, 0x2

    const/16 v1, 0x2b

    const v2, -0x18d5832c

    invoke-static {v0, v1, v2, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v8

    :catch_0
    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7ade6ea6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2062104
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2062105
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->f:LX/EQj;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->q:LX/Dwz;

    invoke-virtual {v1, v2}, LX/EQj;->b(LX/Dwz;)V

    .line 2062106
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->i:LX/0yc;

    invoke-virtual {v1, p0}, LX/0yc;->b(LX/0yL;)V

    .line 2062107
    const/16 v1, 0x2b

    const v2, 0x40c52aeb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x708675ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2062099
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2062100
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->f:LX/EQj;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->q:LX/Dwz;

    invoke-virtual {v1, v2}, LX/EQj;->a(LX/Dwz;)V

    .line 2062101
    invoke-static {p0}, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->c(Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;)V

    .line 2062102
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->i:LX/0yc;

    invoke-virtual {v1, p0}, LX/0yc;->a(LX/0yL;)V

    .line 2062103
    const/16 v1, 0x2b

    const v2, -0x5ac29349

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5a34b30d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2062095
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 2062096
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->o:LX/Dww;

    if-eqz v1, :cond_0

    .line 2062097
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTabPagerFragment;->o:LX/Dww;

    invoke-virtual {v1}, LX/0gG;->kV_()V

    .line 2062098
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x3f98dba9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
