.class public Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/0f1;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0yL;


# static fields
.field private static final y:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private A:LX/1LD;

.field public p:LX/Dxn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/DwJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Dxa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxc;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/2cj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1L1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0yc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final z:LX/DwV;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2060540
    const-class v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;

    const-string v1, "photos_album"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->y:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2060537
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2060538
    new-instance v0, LX/DwV;

    invoke-direct {v0, p0}, LX/DwV;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->z:LX/DwV;

    .line 2060539
    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/os/Bundle;)Lcom/facebook/base/fragment/FbFragment;
    .locals 3

    .prologue
    .line 2060523
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->w:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->w:LX/0yc;

    invoke-virtual {v0}, LX/0yc;->k()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2060524
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2060525
    :goto_0
    sget-object v1, LX/DwS;->ALBUM:LX/DwS;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->a(LX/DwS;ILjava/lang/String;)Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    move-result-object v0

    .line 2060526
    :goto_1
    return-object v0

    .line 2060527
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v0

    goto :goto_0

    .line 2060528
    :cond_1
    sget-object v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->y:Lcom/facebook/common/callercontext/CallerContext;

    .line 2060529
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2060530
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2060531
    const-string v2, "extra_album_selected"

    invoke-static {v1, v2, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2060532
    const-string v2, "extra_caller_context"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2060533
    new-instance v2, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;

    invoke-direct {v2}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetFragment;-><init>()V

    .line 2060534
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2060535
    move-object v0, v2

    .line 2060536
    invoke-virtual {v0, p2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method private static a(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;LX/Dxn;LX/DwJ;LX/Dxa;LX/0Ot;LX/0Ot;LX/2cj;LX/0Ot;LX/0yc;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;",
            "LX/Dxn;",
            "LX/DwJ;",
            "LX/Dxa;",
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dxc;",
            ">;",
            "LX/2cj;",
            "LX/0Ot",
            "<",
            "LX/1L1;",
            ">;",
            "LX/0yc;",
            "LX/0Ot",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2060522
    iput-object p1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->p:LX/Dxn;

    iput-object p2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->q:LX/DwJ;

    iput-object p3, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->r:LX/Dxa;

    iput-object p4, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->s:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->t:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->u:LX/2cj;

    iput-object p7, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->v:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->w:LX/0yc;

    iput-object p9, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->x:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;

    invoke-static {v9}, LX/Dxn;->a(LX/0QB;)LX/Dxn;

    move-result-object v1

    check-cast v1, LX/Dxn;

    invoke-static {v9}, LX/DwJ;->a(LX/0QB;)LX/DwJ;

    move-result-object v2

    check-cast v2, LX/DwJ;

    invoke-static {v9}, LX/Dxa;->a(LX/0QB;)LX/Dxa;

    move-result-object v3

    check-cast v3, LX/Dxa;

    const/16 v4, 0x2e01

    invoke-static {v9, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2eac

    invoke-static {v9, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v9}, LX/2cj;->a(LX/0QB;)LX/2cj;

    move-result-object v6

    check-cast v6, LX/2cj;

    const/16 v7, 0xf2b

    invoke-static {v9, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v9}, LX/0yb;->a(LX/0QB;)LX/0yb;

    move-result-object v8

    check-cast v8, LX/0yc;

    const/16 v10, 0x122d

    invoke-static {v9, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v0 .. v9}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->a(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;LX/Dxn;LX/DwJ;LX/Dxa;LX/0Ot;LX/0Ot;LX/2cj;LX/0Ot;LX/0yc;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;Lcom/facebook/graphql/model/GraphQLAlbum;)V
    .locals 3

    .prologue
    .line 2060508
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2060509
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2060510
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 2060511
    new-instance v0, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorsFragment;

    invoke-direct {v0}, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorsFragment;-><init>()V

    .line 2060512
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2060513
    const-string v2, "arg_album"

    invoke-static {v1, v2, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2060514
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2060515
    move-object v0, v0

    .line 2060516
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    .line 2060517
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    .line 2060518
    invoke-virtual {v0}, LX/0hH;->c()I

    .line 2060519
    return-void

    :cond_0
    move v0, v2

    .line 2060520
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2060521
    goto :goto_1
.end method

.method public static b(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;I)V
    .locals 1

    .prologue
    .line 2060505
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->getRequestedOrientation()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 2060506
    invoke-virtual {p0, p1}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->setRequestedOrientation(I)V

    .line 2060507
    :cond_0
    return-void
.end method

.method private l()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2060504
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_album_selected"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 2060490
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 2060491
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_album_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2060492
    if-eqz v0, :cond_0

    .line 2060493
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/os/Bundle;)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    .line 2060494
    :goto_0
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    const-string v3, "PandoraAlbumFragment"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    .line 2060495
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2060496
    return-void

    .line 2060497
    :cond_0
    new-instance v0, LX/4Vp;

    invoke-direct {v0}, LX/4Vp;-><init>()V

    .line 2060498
    iput-object v1, v0, LX/4Vp;->m:Ljava/lang/String;

    .line 2060499
    move-object v0, v0

    .line 2060500
    invoke-virtual {v0}, LX/4Vp;->a()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 2060501
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 2060502
    const-string v2, "extra_album_selected"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2060503
    invoke-direct {p0, v0, v1}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->a(Lcom/facebook/graphql/model/GraphQLAlbum;Landroid/os/Bundle;)Lcom/facebook/base/fragment/FbFragment;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2060489
    sget-object v0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->y:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0}, Lcom/facebook/common/callercontext/CallerContext;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2060435
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2060436
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v1

    .line 2060437
    if-eqz v1, :cond_0

    .line 2060438
    const-string v2, "content_id"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2060439
    :cond_0
    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2060471
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2060472
    invoke-static {p0, p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2060473
    const v0, 0x7f0300cd

    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->setContentView(I)V

    .line 2060474
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2060475
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2060476
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2060477
    new-instance v1, LX/DwT;

    invoke-direct {v1, p0}, LX/DwT;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2060478
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->p:LX/Dxn;

    .line 2060479
    iput-object v0, v1, LX/Dxn;->a:LX/0h5;

    .line 2060480
    :cond_0
    if-eqz p1, :cond_1

    .line 2060481
    :goto_0
    return-void

    .line 2060482
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->q:LX/DwJ;

    if-eqz v0, :cond_2

    .line 2060483
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->q:LX/DwJ;

    .line 2060484
    const/4 v1, 0x0

    iput v1, v0, LX/DwJ;->c:I

    .line 2060485
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->r:LX/Dxa;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Dxa;->a(Landroid/content/Intent;)V

    .line 2060486
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->getRequestedOrientation()I

    move-result v0

    .line 2060487
    new-instance v1, LX/DwU;

    invoke-direct {v1, p0, v0}, LX/DwU;-><init>(Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;I)V

    iput-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->A:LX/1LD;

    .line 2060488
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->m()V

    goto :goto_0
.end method

.method public final e_(Z)V
    .locals 0

    .prologue
    .line 2060468
    if-nez p1, :cond_0

    .line 2060469
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->m()V

    .line 2060470
    :cond_0
    return-void
.end method

.method public final ig_()V
    .locals 0

    .prologue
    .line 2060467
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2060455
    sparse-switch p1, :sswitch_data_0

    .line 2060456
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxc;

    invoke-virtual {v0, p0, p1, p2, p3}, LX/Dxc;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2060457
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->finish()V

    .line 2060458
    :cond_0
    :goto_0
    return-void

    .line 2060459
    :sswitch_0
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->l()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    .line 2060460
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 2060461
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1042007652536688"

    .line 2060462
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2060463
    move-object v0, v0

    .line 2060464
    invoke-virtual {v0, p0}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 2060465
    :sswitch_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->u:LX/2cj;

    invoke-virtual {v0, p1, p2, p3}, LX/2cj;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2060466
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3ec -> :sswitch_1
        0x7d2 -> :sswitch_0
    .end sparse-switch
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 2060450
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "PandoraAlbumFragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    .line 2060451
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/0fj;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2060452
    check-cast v0, LX/0fj;

    invoke-interface {v0}, LX/0fj;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2060453
    :goto_0
    return-void

    .line 2060454
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x5844d524

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2060445
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2060446
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->z:LX/DwV;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2060447
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->A:LX/1LD;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2060448
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->w:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->b(LX/0yL;)V

    .line 2060449
    const/16 v0, 0x23

    const v2, -0x478e27eb

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x189cca95

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2060440
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2060441
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->z:LX/DwV;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2060442
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->A:LX/1LD;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2060443
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraAlbumMediaSetActivity;->w:LX/0yc;

    invoke-virtual {v0, p0}, LX/0yc;->a(LX/0yL;)V

    .line 2060444
    const/16 v0, 0x23

    const v2, -0x67991790

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
