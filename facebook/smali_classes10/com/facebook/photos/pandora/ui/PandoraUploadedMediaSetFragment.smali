.class public Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;
.super Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;
.source ""


# instance fields
.field public n:LX/DvV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2062618
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;

    invoke-static {p0}, LX/DvV;->a(LX/0QB;)LX/DvV;

    move-result-object p0

    check-cast p0, LX/DvV;

    iput-object p0, p1, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;->n:LX/DvV;

    return-void
.end method

.method public static b(Landroid/os/Bundle;Ljava/lang/String;ZLcom/facebook/common/callercontext/CallerContext;)V
    .locals 2

    .prologue
    .line 2062613
    const-string v0, "profileId"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2062614
    const-string v0, "pandora_instance_id"

    new-instance v1, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    invoke-direct {v1, p1}, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2062615
    const-string v0, "isDefaultLandingPage"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2062616
    const-string v0, "callerContext"

    invoke-virtual {p0, v0, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2062617
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2062607
    invoke-super {p0, p1}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a(Landroid/os/Bundle;)V

    .line 2062608
    const-class v0, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;

    invoke-static {v0, p0}, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2062609
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062610
    const-string v1, "profileId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;->o:Ljava/lang/String;

    .line 2062611
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;->o:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/photos/data/model/PhotoSet;->b(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;->p:Ljava/lang/String;

    .line 2062612
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 8
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2062602
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    move-object v0, v0

    .line 2062603
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2062604
    iget-object v4, v3, LX/Dvb;->i:LX/Dvc;

    move-object v3, v4

    .line 2062605
    invoke-virtual {v3}, LX/Dvc;->d()LX/0Px;

    move-result-object v5

    sget-object v6, LX/74S;->YOUR_PHOTOS:LX/74S;

    const/4 v7, 0x0

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v7}, LX/Dxd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V

    .line 2062606
    return-void
.end method

.method public final c()LX/Dcc;
    .locals 1

    .prologue
    .line 2062601
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraUploadedMediaSetFragment;->n:LX/DvV;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2062596
    if-eq p2, v1, :cond_0

    .line 2062597
    :goto_0
    return-void

    .line 2062598
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2062599
    :pswitch_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 2062600
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x26bc
        :pswitch_0
    .end packed-switch
.end method
