.class public Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

.field private final b:Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

.field private final c:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2061645
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2061646
    iput-object p1, p0, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;->a:Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    .line 2061647
    iput-object p2, p0, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;->b:Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    .line 2061648
    iput-object p3, p0, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;->c:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    .line 2061649
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;
    .locals 6

    .prologue
    .line 2061650
    const-class v1, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;

    monitor-enter v1

    .line 2061651
    :try_start_0
    sget-object v0, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2061652
    sput-object v2, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2061653
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2061654
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2061655
    new-instance p0, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;-><init>(Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;)V

    .line 2061656
    move-object v0, p0

    .line 2061657
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2061658
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2061659
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2061660
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2061661
    check-cast p2, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2061662
    new-instance v0, LX/23u;

    invoke-direct {v0}, LX/23u;-><init>()V

    .line 2061663
    iput-object p2, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2061664
    move-object v0, v0

    .line 2061665
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    move-object v0, v0

    .line 2061666
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;->a:Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    invoke-static {p1, v1, v0}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;->b:Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2061667
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraFooterPartDefinition;->c:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2061668
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2061669
    const/4 v0, 0x1

    return v0
.end method
