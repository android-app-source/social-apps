.class public Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;
.super Lcom/facebook/widget/listview/BetterListView;
.source ""


# instance fields
.field public a:LX/DxF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2062649
    invoke-direct {p0, p1}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;)V

    .line 2062650
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->e()V

    .line 2062651
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2062646
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2062647
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->e()V

    .line 2062648
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2062643
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/listview/BetterListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2062644
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->e()V

    .line 2062645
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2062632
    const-class v0, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;

    invoke-static {v0, p0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2062633
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a04b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2062634
    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setDrawingCacheBackgroundColor(I)V

    .line 2062635
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2062636
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 2062637
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2062638
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setOverscrollFooter(Landroid/graphics/drawable/Drawable;)V

    .line 2062639
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0b41

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setDividerHeight(I)V

    .line 2062640
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->setCacheColorHint(I)V

    .line 2062641
    new-instance v0, LX/DxG;

    invoke-direct {v0, p0}, LX/DxG;-><init>(Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2062642
    return-void
.end method


# virtual methods
.method public setPandoraStoryFetcher(LX/DxF;)V
    .locals 0

    .prologue
    .line 2062630
    iput-object p1, p0, Lcom/facebook/photos/pandora/ui/listview/PandoraFeedListView;->a:LX/DxF;

    .line 2062631
    return-void
.end method
