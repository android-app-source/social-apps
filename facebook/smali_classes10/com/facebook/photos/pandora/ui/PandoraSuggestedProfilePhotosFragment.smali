.class public Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;
.super Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;
.source ""


# instance fields
.field public n:LX/DvT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2061692
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;
    .locals 4

    .prologue
    .line 2061684
    new-instance v0, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;-><init>()V

    .line 2061685
    if-nez p0, :cond_0

    .line 2061686
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2061687
    :cond_0
    const-string v1, "profileId"

    invoke-virtual {p0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2061688
    const-string v1, "callerContext"

    invoke-virtual {p0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2061689
    const-string v1, "pandora_instance_id"

    new-instance v2, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    const-string v3, "sg"

    invoke-direct {v2, p1, v3}, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2061690
    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2061691
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;

    invoke-static {v2}, LX/DvT;->a(LX/0QB;)LX/DvT;

    move-result-object v1

    check-cast v1, LX/DvT;

    const/16 p0, 0x2ead

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->n:LX/DvT;

    iput-object v2, p1, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->o:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2061678
    invoke-super {p0, p1}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->a(Landroid/os/Bundle;)V

    .line 2061679
    const-class v0, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;

    invoke-static {v0, p0}, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2061680
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2061681
    const-string v1, "profileId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->q:Ljava/lang/String;

    .line 2061682
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->q:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/photos/data/model/PhotoSet;->b(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->p:Ljava/lang/String;

    .line 2061683
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 8
    .param p2    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2061674
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    .line 2061675
    iget-object v4, v3, LX/Dvb;->i:LX/Dvc;

    move-object v3, v4

    .line 2061676
    invoke-virtual {v3}, LX/Dvc;->d()LX/0Px;

    move-result-object v5

    sget-object v6, LX/74S;->TIMELINE_PROFILE_PHOTO:LX/74S;

    const/4 v7, 0x0

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v7}, LX/Dxd;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V

    .line 2061677
    return-void
.end method

.method public final c()LX/Dcc;
    .locals 1

    .prologue
    .line 2061673
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraSuggestedProfilePhotosFragment;->n:LX/DvT;

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x142a3749

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2061670
    invoke-super {p0}, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->onDestroyView()V

    .line 2061671
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/photocollage/PandoraPhotoCollageFragment;->k:LX/DxJ;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/DxJ;->a(LX/Dce;)V

    .line 2061672
    const/16 v1, 0x2b

    const v2, 0x7ba7c92e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
