.class public Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/DvH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2060402
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2060403
    return-void
.end method

.method public static a(LX/DwS;ILjava/lang/String;)Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;
    .locals 4

    .prologue
    .line 2060409
    new-instance v0, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    invoke-direct {v0}, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;-><init>()V

    .line 2060410
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2060411
    const-string v2, "pandoraType"

    invoke-virtual {p0}, LX/DwS;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2060412
    const-string v2, "photoCount"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2060413
    const-string v2, "userId"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2060414
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2060415
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    invoke-static {v2}, LX/DvH;->a(LX/0QB;)LX/DvH;

    move-result-object v1

    check-cast v1, LX/DvH;

    const/16 p0, 0x12b1

    invoke-static {v2, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    iput-object v1, p1, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->a:LX/DvH;

    iput-object v2, p1, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->b:LX/0Ot;

    return-void
.end method

.method public static c(Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 3

    .prologue
    .line 2060404
    invoke-virtual {p1}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    .line 2060405
    instance-of v1, v0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    if-eqz v1, :cond_0

    .line 2060406
    check-cast v0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    iget v1, p0, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->c:I

    sget-object v2, LX/397;->PHOTO:LX/397;

    .line 2060407
    invoke-static {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->b(Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;ILX/397;)V

    .line 2060408
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2060416
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2060417
    const-class v0, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;

    invoke-static {v0, p0}, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2060418
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2060419
    const-string v1, "photoCount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->c:I

    .line 2060420
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6e89cd03

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2060380
    const v0, 0x7f030413

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2060381
    const v0, 0x7f0d0c89

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2060382
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2060383
    invoke-static {p0, v0}, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->c(Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 2060384
    const/16 v0, 0x2b

    const v3, -0x7a13917e

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6f098829

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2060385
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2060386
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2060387
    const/16 v0, 0x2b

    const v2, -0x645e6c1d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2060388
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2060389
    const v0, 0x7f0d0c89

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2060390
    sget-object v1, LX/DwR;->a:[I

    invoke-static {}, LX/DwS;->values()[LX/DwS;

    move-result-object v2

    .line 2060391
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2060392
    const-string v4, "pandoraType"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2}, LX/DwS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2060393
    :goto_0
    return-void

    .line 2060394
    :pswitch_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2060395
    const-string v2, "userId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2060396
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fetch tagged media count "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/DwP;

    invoke-direct {v4, p0, v2}, LX/DwP;-><init>(Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;Ljava/lang/String;)V

    new-instance v2, LX/DwQ;

    invoke-direct {v2, p0, v0}, LX/DwQ;-><init>(Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    invoke-virtual {v1, v3, v4, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2060397
    goto :goto_0

    .line 2060398
    :pswitch_1
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2060399
    const-string v2, "userId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2060400
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fetch uploaded media count "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/DwN;

    invoke-direct {v4, p0, v2}, LX/DwN;-><init>(Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;Ljava/lang/String;)V

    new-instance v2, LX/DwO;

    invoke-direct {v2, p0, v0}, LX/DwO;-><init>(Lcom/facebook/photos/pandora/ui/DialtonePhotosCoverFragment;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    invoke-virtual {v1, v3, v4, v2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2060401
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
