.class public Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dv9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1L1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DxE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/base/analytics/PhotoFlowLogger;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/DwK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Z

.field public n:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

.field public o:Lcom/facebook/common/callercontext/CallerContext;

.field private p:Lcom/facebook/widget/CustomFrameLayout;

.field public q:LX/DxD;

.field public r:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

.field public s:Landroid/view/View;

.field private t:LX/Dx9;

.field private u:LX/DxA;

.field private v:LX/DxB;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2062457
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2062458
    new-instance v0, LX/Dx9;

    invoke-direct {v0, p0}, LX/Dx9;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->t:LX/Dx9;

    .line 2062459
    new-instance v0, LX/DxA;

    invoke-direct {v0, p0}, LX/DxA;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->u:LX/DxA;

    .line 2062460
    new-instance v0, LX/DxB;

    invoke-direct {v0, p0}, LX/DxB;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->v:LX/DxB;

    .line 2062461
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 8

    .prologue
    .line 2062462
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/DxE;

    invoke-virtual {v3}, LX/DxE;->c()LX/Dvc;

    move-result-object v3

    invoke-virtual {v3}, LX/Dvc;->d()LX/0Px;

    move-result-object v5

    sget-object v6, LX/74S;->TIMELINE_PHOTOS_OF_USER:LX/74S;

    const/4 v7, 0x0

    move-object v3, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v7}, LX/Dxd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V

    .line 2062463
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 2062464
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxE;

    invoke-virtual {v0}, LX/DxE;->d()LX/Dx6;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxE;

    invoke-virtual {v0}, LX/DxE;->d()LX/Dx6;

    move-result-object v0

    .line 2062465
    iget-boolean p0, v0, LX/Dvb;->m:Z

    move v0, p0

    .line 2062466
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)Z
    .locals 2

    .prologue
    .line 2062467
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxE;

    invoke-virtual {v0}, LX/DxE;->c()LX/Dvc;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxE;

    invoke-virtual {v0}, LX/DxE;->c()LX/Dvc;

    move-result-object v0

    .line 2062468
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2062469
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxE;

    invoke-virtual {v0}, LX/DxE;->c()LX/Dvc;

    move-result-object v0

    .line 2062470
    iget-object v1, v0, LX/Dvc;->a:LX/0Px;

    move-object v0, v1

    .line 2062471
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2062472
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2062473
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/16 v4, 0x2e8a

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf2b

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2ea6

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2ead

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2e05

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2dff

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 p1, 0x2eaa

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/DwK;->a(LX/0QB;)LX/DwK;

    move-result-object v0

    check-cast v0, LX/DwK;

    iput-object v3, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->a:Ljava/lang/String;

    iput-object v4, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->b:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->e:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->f:LX/0Ot;

    iput-object v9, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->g:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->h:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->i:LX/DwK;

    .line 2062474
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062475
    if-nez v0, :cond_0

    .line 2062476
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->j:Ljava/lang/String;

    .line 2062477
    :goto_0
    return-void

    .line 2062478
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062479
    const-string v1, "userId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->j:Ljava/lang/String;

    .line 2062480
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->j:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2062481
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->j:Ljava/lang/String;

    .line 2062482
    :cond_1
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062483
    const-string v1, "userName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->k:Ljava/lang/String;

    .line 2062484
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062485
    const-string v1, "callerContext"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->o:Lcom/facebook/common/callercontext/CallerContext;

    .line 2062486
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2062487
    const-string v1, "extra_photo_tab_mode_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->n:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2062488
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Dxa;->a(Landroid/content/Intent;)V

    .line 2062489
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->m:Z

    .line 2062490
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->j:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/photos/data/model/PhotoSet;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/16 v8, 0x8

    const/4 v7, -0x1

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x7e26043

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2062491
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->i:LX/DwK;

    const-string v1, "InflatePhotosOfFragment"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2062492
    new-instance v0, Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->p:Lcom/facebook/widget/CustomFrameLayout;

    .line 2062493
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->p:Lcom/facebook/widget/CustomFrameLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2062494
    new-instance v0, LX/DxD;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/DxD;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->q:LX/DxD;

    .line 2062495
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->q:LX/DxD;

    const v1, 0x7f0d00c4

    invoke-virtual {v0, v1}, LX/DxD;->setId(I)V

    .line 2062496
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxE;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->k:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->m:Z

    const/4 v4, 0x1

    invoke-virtual/range {v0 .. v5}, LX/DxE;->a(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 2062497
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->q:LX/DxD;

    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    invoke-virtual {v1, v0}, LX/DxD;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2062498
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2062499
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->p:Lcom/facebook/widget/CustomFrameLayout;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->q:LX/DxD;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2062500
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->i:LX/DwK;

    const-string v1, "SpinnerPhotosOfFragment"

    invoke-virtual {v0, v1}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2062501
    new-instance v0, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->r:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    .line 2062502
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2062503
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2062504
    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->p:Lcom/facebook/widget/CustomFrameLayout;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->r:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2062505
    invoke-static {p0}, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062506
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->r:Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;

    invoke-virtual {v0, v8}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->setVisibility(I)V

    .line 2062507
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->i:LX/DwK;

    const-string v1, "SpinnerPhotosOfFragment"

    invoke-virtual {v0, v1}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2062508
    :cond_0
    new-instance v0, Landroid/view/ViewStub;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030ef6

    invoke-direct {v0, v1, v2}, Landroid/view/ViewStub;-><init>(Landroid/content/Context;I)V

    .line 2062509
    new-instance v1, LX/Dx8;

    invoke-direct {v1, p0}, LX/Dx8;-><init>(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setOnInflateListener(Landroid/view/ViewStub$OnInflateListener;)V

    .line 2062510
    iput-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->s:Landroid/view/View;

    .line 2062511
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->p:Lcom/facebook/widget/CustomFrameLayout;

    iget-object v1, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;)V

    .line 2062512
    invoke-direct {p0}, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->c()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d(Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2062513
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2062514
    :goto_0
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->i:LX/DwK;

    const-string v1, "InflatePhotosOfFragment"

    invoke-virtual {v0, v1}, LX/DwK;->b(Ljava/lang/String;)V

    .line 2062515
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->p:Lcom/facebook/widget/CustomFrameLayout;

    const v1, -0xcee2b5d

    invoke-static {v1, v6}, LX/02F;->f(II)V

    return-object v0

    .line 2062516
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2062517
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->q:LX/DxD;

    invoke-virtual {v0, v8}, LX/DxD;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xfade4c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2062518
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2062519
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxE;

    invoke-virtual {v0}, LX/DxE;->d()LX/Dx6;

    move-result-object v0

    invoke-virtual {v0}, LX/Dvb;->j()V

    .line 2062520
    const/16 v0, 0x2b

    const v2, 0x190b24ad

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1582ab1f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2062521
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2062522
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dv9;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->t:LX/Dx9;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2062523
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->u:LX/DxA;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2062524
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxE;

    invoke-virtual {v0}, LX/DxE;->d()LX/Dx6;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->v:LX/DxB;

    invoke-virtual {v0, v2}, LX/Dx6;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2062525
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a()V

    .line 2062526
    new-instance v5, LX/9lP;

    iget-object v6, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->j:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->a:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->a:Ljava/lang/String;

    .line 2062527
    :goto_0
    iget-object v7, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v7, v7

    .line 2062528
    const-string v8, "friendship_status"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v7

    .line 2062529
    iget-object v8, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 2062530
    const-string v9, "subscribe_status"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v8

    invoke-direct {v5, v6, v4, v7, v8}, LX/9lP;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2062531
    new-instance v6, LX/74V;

    invoke-virtual {v5}, LX/9lP;->g()LX/9lQ;

    move-result-object v4

    invoke-virtual {v4}, LX/9lQ;->name()Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->j:Ljava/lang/String;

    iget-object v8, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DxE;

    .line 2062532
    iget-object v9, v4, LX/DxE;->c:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/Dx6;

    .line 2062533
    iget-object v10, v9, LX/Dvb;->i:LX/Dvc;

    move-object v9, v10

    .line 2062534
    if-nez v9, :cond_1

    .line 2062535
    const/4 v9, 0x0

    .line 2062536
    :goto_1
    move v4, v9

    .line 2062537
    invoke-direct {v6, v5, v7, v8, v4}, LX/74V;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2062538
    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/73w;

    .line 2062539
    iget-object v5, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2062540
    const-string v7, "session_id"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/73w;->a(Ljava/lang/String;)V

    .line 2062541
    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/73w;

    .line 2062542
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v5

    .line 2062543
    const-string v7, "relationship_type"

    iget-object v8, v6, LX/74V;->a:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2062544
    const-string v7, "profile_id"

    iget-object v8, v6, LX/74V;->b:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2062545
    const-string v7, "viewer_id"

    iget-object v8, v6, LX/74V;->c:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2062546
    const-string v7, "photos_of_user"

    iget-object v8, v6, LX/74V;->d:Ljava/lang/String;

    invoke-virtual {v5, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2062547
    move-object v7, v5

    .line 2062548
    sget-object v8, LX/74R;->PHOTOS_OF_LOADING_SECTIONS:LX/74R;

    .line 2062549
    iget-object v5, v6, LX/74V;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2062550
    invoke-virtual {v7, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v4, v8, v7, v5}, LX/73w;->a(LX/73w;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 2062551
    const/16 v0, 0x2b

    const v2, -0x7692fca9

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2062552
    :cond_0
    iget-object v4, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->j:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    iget-object v9, v4, LX/DxE;->c:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/Dx6;

    .line 2062553
    iget-object v10, v9, LX/Dvb;->i:LX/Dvc;

    move-object v9, v10

    .line 2062554
    invoke-virtual {v9}, LX/Dvc;->c()I

    move-result v9

    goto :goto_1
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1d4e103f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2062555
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2062556
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dv9;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->t:LX/Dx9;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2062557
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1L1;

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->u:LX/DxA;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2062558
    iget-object v0, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxE;

    invoke-virtual {v0}, LX/DxE;->d()LX/Dx6;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/photos/pandora/ui/PandoraTaggedPhotosFragment;->v:LX/DxB;

    invoke-virtual {v0, v2}, LX/Dx6;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2062559
    const/16 v0, 0x2b

    const v2, -0x52960b1b

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
