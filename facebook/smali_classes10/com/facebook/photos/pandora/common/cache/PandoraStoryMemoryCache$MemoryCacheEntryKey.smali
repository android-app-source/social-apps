.class public final Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

.field public final b:LX/DvW;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2057871
    new-instance v0, LX/Duw;

    invoke-direct {v0}, LX/Duw;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2057872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057873
    const-class v0, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    .line 2057874
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/DvW;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->b:LX/DvW;

    .line 2057875
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;)V
    .locals 0

    .prologue
    .line 2057876
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057877
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    .line 2057878
    iput-object p2, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->b:LX/DvW;

    .line 2057879
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2057880
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2057881
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    if-nez v1, :cond_1

    .line 2057882
    :cond_0
    :goto_0
    return v0

    .line 2057883
    :cond_1
    check-cast p1, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;

    .line 2057884
    iget-object v1, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-object v2, p1, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->b:LX/DvW;

    iget-object v2, p1, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->b:LX/DvW;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2057885
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->b:LX/DvW;

    invoke-virtual {v1}, LX/DvW;->ordinal()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2057886
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->a:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2057887
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/cache/PandoraStoryMemoryCache$MemoryCacheEntryKey;->b:LX/DvW;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2057888
    return-void
.end method
