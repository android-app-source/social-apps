.class public Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;
.super Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2057936
    new-instance v0, LX/Duy;

    invoke-direct {v0}, LX/Duy;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2057933
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;-><init>()V

    .line 2057934
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->a:Ljava/lang/String;

    .line 2057935
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2057929
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;-><init>()V

    .line 2057930
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->a:Ljava/lang/String;

    .line 2057931
    iput-object p2, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->b:Ljava/lang/String;

    .line 2057932
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2057928
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2057921
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;

    if-nez v1, :cond_1

    .line 2057922
    :cond_0
    :goto_0
    return v0

    .line 2057923
    :cond_1
    check-cast p1, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;

    .line 2057924
    iget-object v1, p1, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2057925
    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->a:Ljava/lang/String;

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2057926
    iget-object v1, p1, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2057927
    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2057920
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2057916
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2057917
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057918
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057919
    return-void
.end method
