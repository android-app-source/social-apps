.class public Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoStoryModel;
.super Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoStoryModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2058074
    new-instance v0, LX/Dv7;

    invoke-direct {v0}, LX/Dv7;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoStoryModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2058075
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;-><init>()V

    .line 2058076
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoStoryModel;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2058077
    return-void
.end method


# virtual methods
.method public final a()LX/Dv3;
    .locals 1

    .prologue
    .line 2058078
    sget-object v0, LX/Dv3;->SINGLE_PHOTO_STORY:LX/Dv3;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2058079
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2058080
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoStoryModel;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2058081
    return-void
.end method
