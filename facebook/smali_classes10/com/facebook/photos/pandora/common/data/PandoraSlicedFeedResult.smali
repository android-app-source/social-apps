.class public Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2057940
    new-instance v0, LX/Duz;

    invoke-direct {v0}, LX/Duz;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2057941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057942
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2057943
    const-class v0, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    .line 2057944
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0Px;)V
    .locals 0
    .param p1    # Lcom/facebook/graphql/model/GraphQLPageInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPageInfo;",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2057945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2057946
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2057947
    iput-object p2, p0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    .line 2057948
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2057949
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2057950
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->a:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2057951
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2057952
    return-void
.end method
