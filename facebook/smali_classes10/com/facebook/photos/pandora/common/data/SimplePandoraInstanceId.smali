.class public Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;
.super Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2057973
    new-instance v0, LX/Dv1;

    invoke-direct {v0}, LX/Dv1;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2057991
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;-><init>()V

    .line 2057992
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    .line 2057993
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->b:Ljava/lang/String;

    .line 2057994
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2057988
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;-><init>()V

    .line 2057989
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    .line 2057990
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2057984
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;-><init>()V

    .line 2057985
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    .line 2057986
    iput-object p2, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->b:Ljava/lang/String;

    .line 2057987
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2057983
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2057979
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    if-nez v1, :cond_1

    .line 2057980
    :cond_0
    :goto_0
    return v0

    .line 2057981
    :cond_1
    check-cast p1, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;

    .line 2057982
    iget-object v1, p1, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2057978
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2057977
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2057974
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057975
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/SimplePandoraInstanceId;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2057976
    return-void
.end method
