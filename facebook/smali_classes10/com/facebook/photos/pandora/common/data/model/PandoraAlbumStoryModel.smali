.class public Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;
.super Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2058013
    new-instance v0, LX/Dv2;

    invoke-direct {v0}, LX/Dv2;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2058010
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;-><init>()V

    .line 2058011
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    .line 2058012
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2058007
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;-><init>()V

    .line 2058008
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    .line 2058009
    return-void
.end method


# virtual methods
.method public final a()LX/Dv3;
    .locals 1

    .prologue
    .line 2058014
    sget-object v0, LX/Dv3;->ALBUM_POST_SECTION:LX/Dv3;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2058006
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2058001
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;

    if-nez v0, :cond_1

    .line 2058002
    :cond_0
    const/4 v0, 0x0

    .line 2058003
    :goto_0
    return v0

    .line 2058004
    :cond_1
    check-cast p1, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;

    .line 2058005
    iget-object v0, p1, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    iget-object v1, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2057999
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraAlbumStoryModel;->a:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 2058000
    return-void
.end method
