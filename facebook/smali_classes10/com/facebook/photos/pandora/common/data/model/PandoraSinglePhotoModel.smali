.class public Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoModel;
.super Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLPhoto;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2058063
    new-instance v0, LX/Dv6;

    invoke-direct {v0}, LX/Dv6;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2058064
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;-><init>()V

    .line 2058065
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoModel;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2058066
    return-void
.end method


# virtual methods
.method public final a()LX/Dv3;
    .locals 1

    .prologue
    .line 2058067
    sget-object v0, LX/Dv3;->SINGLE_PHOTO:LX/Dv3;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2058068
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2058069
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSinglePhotoModel;->a:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2058070
    return-void
.end method
