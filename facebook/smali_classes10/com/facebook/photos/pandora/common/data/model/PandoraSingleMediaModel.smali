.class public Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;
.super Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2058059
    new-instance v0, LX/Dv5;

    invoke-direct {v0}, LX/Dv5;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/8IH;)V
    .locals 1

    .prologue
    .line 2058057
    invoke-static {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->a(LX/8IH;)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;)V

    .line 2058058
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2058050
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;-><init>()V

    .line 2058051
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    .line 2058052
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->b:Ljava/lang/String;

    .line 2058053
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->c:Ljava/lang/String;

    .line 2058054
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2058055
    invoke-direct {p0, p1, v0, v0}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;Ljava/lang/String;Ljava/lang/String;)V

    .line 2058056
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2058045
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;-><init>()V

    .line 2058046
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    .line 2058047
    iput-object p2, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->b:Ljava/lang/String;

    .line 2058048
    iput-object p3, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->c:Ljava/lang/String;

    .line 2058049
    return-void
.end method


# virtual methods
.method public final a()LX/Dv3;
    .locals 1

    .prologue
    .line 2058044
    sget-object v0, LX/Dv3;->SINGLE_MEDIA:LX/Dv3;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2058039
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2058040
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2058041
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2058042
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2058043
    return-void
.end method
