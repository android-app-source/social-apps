.class public final Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

.field public final b:D

.field public final c:LX/Dvw;

.field public d:Z

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2059439
    new-instance v0, LX/Dvv;

    invoke-direct {v0}, LX/Dvv;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2059431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059432
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    .line 2059433
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->b:D

    .line 2059434
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    .line 2059435
    const-class v0, LX/Dvw;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Dvw;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    .line 2059436
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->e:Ljava/lang/String;

    .line 2059437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->f:Ljava/lang/String;

    .line 2059438
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;DLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2059440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059441
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    .line 2059442
    iput-wide p2, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->b:D

    .line 2059443
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    .line 2059444
    const p2, 0x3f8ccccd    # 1.1f

    .line 2059445
    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->F()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2059446
    const/4 v0, 0x0

    .line 2059447
    :goto_0
    move-object v0, v0

    .line 2059448
    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    .line 2059449
    iput-object p4, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->e:Ljava/lang/String;

    .line 2059450
    iput-object p5, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->f:Ljava/lang/String;

    .line 2059451
    return-void

    .line 2059452
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->F()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->F()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 2059453
    sget-object v0, LX/Dvw;->LANDSCAPE:LX/Dvw;

    goto :goto_0

    .line 2059454
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->F()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->F()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    .line 2059455
    sget-object v0, LX/Dvw;->PORTRAIT:LX/Dvw;

    goto :goto_0

    .line 2059456
    :cond_2
    sget-object v0, LX/Dvw;->SQUARE:LX/Dvw;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2059430
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2059423
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->a:Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2059424
    iget-wide v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->b:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2059425
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2059426
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->c:LX/Dvw;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2059427
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2059428
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2059429
    return-void
.end method
