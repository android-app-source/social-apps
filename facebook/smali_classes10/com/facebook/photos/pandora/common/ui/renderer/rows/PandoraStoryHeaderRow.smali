.class public Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraStoryHeaderRow;
.super Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraStoryHeaderRow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2059525
    new-instance v0, LX/Dw0;

    invoke-direct {v0}, LX/Dw0;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraStoryHeaderRow;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2059526
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;-><init>()V

    .line 2059527
    const-class v0, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraStoryHeaderRow;->a:Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    .line 2059528
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2059529
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2059530
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraStoryHeaderRow;->a:Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2059531
    return-void
.end method
