.class public Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final k:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dv9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DwK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DwJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation runtime Lcom/facebook/dialtone/common/IsDialtonePhotoFeatureEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/AmY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/26C;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AmR;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/DwA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:I

.field public final m:[LX/DwE;

.field public n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

.field public o:LX/DvW;

.field public p:Ljava/lang/String;

.field public q:Z

.field private r:Z

.field private s:Z

.field private t:LX/DwE;

.field private u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

.field private v:LX/Dw9;

.field private w:LX/Dw2;

.field private x:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2059835
    const-class v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    const-string v1, "photo_pandora"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2059832
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2059833
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getNumOfItems()I

    move-result v0

    new-array v0, v0, [LX/DwE;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    .line 2059834
    return-void
.end method

.method private a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;Z)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    .line 2059817
    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2059818
    if-eqz v0, :cond_0

    .line 2059819
    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->k()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2059820
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->D()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;

    move-result-object v0

    .line 2059821
    if-nez v0, :cond_2

    .line 2059822
    :goto_1
    return-object v1

    .line 2059823
    :cond_0
    if-eqz p2, :cond_1

    const v0, 0x7f080072

    .line 2059824
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2059825
    :cond_1
    const v0, 0x7f080073

    goto :goto_2

    .line 2059826
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11R;

    sget-object v2, LX/1lB;->EXACT_STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {p1}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->D()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;->d()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    .line 2059827
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2059828
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2059829
    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2059830
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v1, v0

    .line 2059831
    goto :goto_1
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLPhoto;Ljava/lang/String;)V
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2059815
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/Dv9;

    new-instance v0, LX/DvB;

    iget-object v3, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-object v4, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->o:LX/DvW;

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/DvB;-><init>(Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Lcom/facebook/graphql/model/GraphQLPhoto;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, LX/0b4;->a(LX/0b7;)V

    .line 2059816
    return-void
.end method

.method private static a(LX/DwE;)Z
    .locals 1

    .prologue
    .line 2059814
    if-eqz p0, :cond_0

    iget-object v0, p0, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2059810
    iget-object v1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2059811
    iget-object v3, v3, LX/DwE;->c:LX/1aX;

    invoke-virtual {v3}, LX/1aX;->d()V

    .line 2059812
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2059813
    :cond_0
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 2059806
    iget-object v1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2059807
    iget-object v3, v3, LX/DwE;->c:LX/1aX;

    invoke-virtual {v3}, LX/1aX;->f()V

    .line 2059808
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2059809
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 2059785
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v13

    move-object v3, p0

    check-cast v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;

    const/16 v4, 0x2e8a

    invoke-static {v13, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x509

    invoke-static {v13, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v13}, LX/DwK;->a(LX/0QB;)LX/DwK;

    move-result-object v6

    check-cast v6, LX/DwK;

    invoke-static {v13}, LX/DwJ;->a(LX/0QB;)LX/DwJ;

    move-result-object v7

    check-cast v7, LX/DwJ;

    const/16 v8, 0x1488

    invoke-static {v13, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v13}, LX/AmY;->a(LX/0QB;)LX/AmY;

    move-result-object v9

    check-cast v9, LX/AmY;

    invoke-static {v13}, LX/26C;->a(LX/0QB;)LX/26C;

    move-result-object v10

    check-cast v10, LX/26C;

    const/16 v11, 0x1d4d

    invoke-static {v13, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const-class v12, LX/DwA;

    invoke-interface {v13, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/DwA;

    const/16 v1, 0x2e4

    invoke-static {v13, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    iput-object v4, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->b:LX/0Or;

    iput-object v6, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c:LX/DwK;

    iput-object v7, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->d:LX/DwJ;

    iput-object v8, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->e:LX/0Or;

    iput-object v9, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->f:LX/AmY;

    iput-object v10, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->g:LX/26C;

    iput-object v11, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->h:LX/0Ot;

    iput-object v12, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->i:LX/DwA;

    iput-object v13, v3, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->j:LX/0Ot;

    .line 2059786
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2059787
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->l:I

    .line 2059788
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04bb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2059789
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->setWillNotCacheDrawing(Z)V

    .line 2059790
    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->setWillNotDraw(Z)V

    .line 2059791
    invoke-virtual {p0, p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2059792
    new-instance v2, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    const/16 v3, 0xc8

    .line 2059793
    iput v3, v2, LX/1Uo;->d:I

    .line 2059794
    move-object v2, v2

    .line 2059795
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2059796
    iput-object v3, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2059797
    move-object v1, v2

    .line 2059798
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getNumOfItems()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2059799
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v2

    .line 2059800
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v2, v3}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v2

    .line 2059801
    iget-object v3, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    new-instance v4, LX/DwE;

    invoke-direct {v4, v2}, LX/DwE;-><init>(LX/1aX;)V

    aput-object v4, v3, v0

    .line 2059802
    iget-object v2, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    aget-object v2, v2, v0

    iget-object v2, v2, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2059803
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2059804
    :cond_0
    new-instance v0, LX/Dw2;

    invoke-direct {v0, p0}, LX/Dw2;-><init>(Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->w:LX/Dw2;

    .line 2059805
    return-void
.end method

.method public final a(Landroid/graphics/Rect;Landroid/net/Uri;Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;ILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 2059712
    if-nez p2, :cond_0

    .line 2059713
    :goto_0
    return-void

    .line 2059714
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->q:Z

    if-eqz v0, :cond_1

    .line 2059715
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c:LX/DwK;

    const-string v2, "LoadImageThumbnail"

    invoke-virtual {v0, v2}, LX/DwK;->a(Ljava/lang/String;)V

    .line 2059716
    :cond_1
    new-instance v2, LX/Dw1;

    invoke-direct {v2, p0, p5}, LX/Dw1;-><init>(Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;Ljava/lang/String;)V

    .line 2059717
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    aget-object v3, v0, p4

    .line 2059718
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v4, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v4, v3, LX/DwE;->c:LX/1aX;

    .line 2059719
    iget-object v5, v4, LX/1aX;->f:LX/1aZ;

    move-object v4, v5

    .line 2059720
    invoke-virtual {v0, v4}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, v2}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 2059721
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->x:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;->a:LX/0Px;

    invoke-virtual {v0, p4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;

    .line 2059722
    iput-object p1, v3, LX/DwE;->b:Landroid/graphics/Rect;

    .line 2059723
    iget-object v4, v3, LX/DwE;->c:LX/1aX;

    invoke-virtual {v4, v2}, LX/1aX;->a(LX/1aZ;)V

    .line 2059724
    invoke-virtual {p3}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, LX/DwE;->d:Ljava/lang/String;

    .line 2059725
    iput-object p2, v3, LX/DwE;->e:Landroid/net/Uri;

    .line 2059726
    invoke-static {p3}, LX/8I6;->a(LX/8IH;)Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v2

    iput-object v2, v3, LX/DwE;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2059727
    const/4 v2, 0x0

    .line 2059728
    if-nez p3, :cond_4

    .line 2059729
    :cond_2
    :goto_1
    move-object v2, v2

    .line 2059730
    iput-object v2, v3, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 2059731
    iget-object v2, v3, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    iget-boolean v4, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->r:Z

    invoke-virtual {v2, v4, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 2059732
    iget-object v2, v3, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    iget v4, p1, Landroid/graphics/Rect;->left:I

    iget v5, p1, Landroid/graphics/Rect;->top:I

    iget v6, p1, Landroid/graphics/Rect;->right:I

    iget v7, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2059733
    iget-object v2, v3, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    if-nez v2, :cond_3

    :goto_2
    invoke-direct {p0, p3, v1}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;Z)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v3, LX/DwE;->h:Ljava/lang/CharSequence;

    .line 2059734
    iget-object v1, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->e:Ljava/lang/String;

    iput-object v1, v3, LX/DwE;->i:Ljava/lang/String;

    .line 2059735
    iget-object v0, v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow$PandoraMultiMediaStoryEntry;->f:Ljava/lang/String;

    iput-object v0, v3, LX/DwE;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 2059736
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 2059737
    :cond_4
    invoke-interface {p3}, LX/8IH;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    .line 2059738
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, 0x4ed245b

    if-ne v4, v5, :cond_2

    .line 2059739
    new-instance v2, LX/2oI;

    invoke-direct {v2}, LX/2oI;-><init>()V

    .line 2059740
    invoke-interface {p3}, LX/8IH;->l()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel$CreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2059741
    iput-object v4, v2, LX/2oI;->u:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2059742
    invoke-interface {p3}, LX/8IH;->m()LX/1VU;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1VU;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    .line 2059743
    iput-object v4, v2, LX/2oI;->A:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2059744
    invoke-interface {p3}, LX/8IH;->n()I

    move-result v4

    .line 2059745
    iput v4, v2, LX/2oI;->M:I

    .line 2059746
    invoke-interface {p3}, LX/8IH;->d()Ljava/lang/String;

    move-result-object v4

    .line 2059747
    iput-object v4, v2, LX/2oI;->N:Ljava/lang/String;

    .line 2059748
    invoke-interface {p3}, LX/8IH;->e()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059749
    iput-object v4, v2, LX/2oI;->O:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059750
    invoke-interface {p3}, LX/8IH;->aj_()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059751
    iput-object v4, v2, LX/2oI;->P:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059752
    invoke-interface {p3}, LX/8IH;->ai_()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059753
    iput-object v4, v2, LX/2oI;->T:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059754
    invoke-interface {p3}, LX/8IH;->j()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059755
    iput-object v4, v2, LX/2oI;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059756
    invoke-interface {p3}, LX/8IH;->o()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059757
    iput-object v4, v2, LX/2oI;->X:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059758
    invoke-interface {p3}, LX/8IH;->p()Z

    move-result v4

    .line 2059759
    iput-boolean v4, v2, LX/2oI;->al:Z

    .line 2059760
    invoke-interface {p3}, LX/8IH;->q()Z

    move-result v4

    .line 2059761
    iput-boolean v4, v2, LX/2oI;->am:Z

    .line 2059762
    invoke-interface {p3}, LX/8IH;->r()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059763
    iput-object v4, v2, LX/2oI;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059764
    invoke-interface {p3}, LX/8IH;->s()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059765
    iput-object v4, v2, LX/2oI;->at:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059766
    invoke-interface {p3}, LX/8IH;->t()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059767
    iput-object v4, v2, LX/2oI;->au:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059768
    invoke-interface {p3}, LX/8IH;->u()I

    move-result v4

    .line 2059769
    iput v4, v2, LX/2oI;->az:I

    .line 2059770
    invoke-interface {p3}, LX/8IH;->v()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059771
    iput-object v4, v2, LX/2oI;->aI:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059772
    invoke-interface {p3}, LX/8IH;->w()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059773
    iput-object v4, v2, LX/2oI;->aJ:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059774
    invoke-interface {p3}, LX/8IH;->y()I

    move-result v4

    .line 2059775
    iput v4, v2, LX/2oI;->aS:I

    .line 2059776
    invoke-interface {p3}, LX/8IH;->z()Ljava/lang/String;

    move-result-object v4

    .line 2059777
    iput-object v4, v2, LX/2oI;->aT:Ljava/lang/String;

    .line 2059778
    invoke-interface {p3}, LX/8IH;->A()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059779
    iput-object v4, v2, LX/2oI;->aX:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059780
    invoke-interface {p3}, LX/8IH;->B()LX/1Fb;

    move-result-object v4

    invoke-static {v4}, LX/8I6;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    .line 2059781
    iput-object v4, v2, LX/2oI;->bF:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2059782
    invoke-interface {p3}, LX/8IH;->C()I

    move-result v4

    .line 2059783
    iput v4, v2, LX/2oI;->cd:I

    .line 2059784
    invoke-virtual {v2}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    goto/16 :goto_1
.end method

.method public a(Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Ljava/lang/String;ZZZLX/Dw7;)V
    .locals 2
    .param p8    # LX/Dw7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2059697
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->x:Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiMediaRow;

    .line 2059698
    iput-object p2, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    .line 2059699
    iput-object p3, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->o:LX/DvW;

    .line 2059700
    iput-object p4, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->p:Ljava/lang/String;

    .line 2059701
    iput-boolean p5, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->q:Z

    .line 2059702
    iput-boolean p6, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->r:Z

    .line 2059703
    iput-boolean p7, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->s:Z

    .line 2059704
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->s:Z

    if-eqz v0, :cond_0

    if-eqz p8, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    if-nez v0, :cond_0

    .line 2059705
    new-instance v0, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    .line 2059706
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->addView(Landroid/view/View;)V

    .line 2059707
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->i:LX/DwA;

    .line 2059708
    new-instance p2, LX/Dw9;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p1

    check-cast p1, LX/03V;

    invoke-direct {p2, p8, v1, p1}, LX/Dw9;-><init>(LX/Dw7;Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;LX/03V;)V

    .line 2059709
    move-object v0, p2

    .line 2059710
    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->v:LX/Dw9;

    .line 2059711
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2059670
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->s:Z

    if-nez v0, :cond_1

    .line 2059671
    :cond_0
    :goto_0
    return-void

    .line 2059672
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    if-eqz v0, :cond_2

    .line 2059673
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->v:LX/Dw9;

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    .line 2059674
    invoke-static {v2}, LX/1aL;->a(Landroid/view/View;)LX/1Ra;

    .line 2059675
    iget-object v3, v0, LX/Dw9;->d:LX/1Rc;

    invoke-virtual {v3, v2}, LX/1Ra;->b(Landroid/view/View;)V

    .line 2059676
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    .line 2059677
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->setVisibility(I)V

    .line 2059678
    :cond_2
    iget-object v2, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 2059679
    iget-object v5, v4, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v5, :cond_4

    .line 2059680
    iput-object v4, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    .line 2059681
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    invoke-static {v0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(LX/DwE;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2059682
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->setVisibility(I)V

    .line 2059683
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v0, v0, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v1, v1, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-static {v1}, LX/6X7;->a(Lcom/facebook/graphql/model/GraphQLVideo;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 2059684
    iput-object v1, v0, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 2059685
    move-object v0, v0

    .line 2059686
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2059687
    iget-object v1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v1, v1, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2059688
    iget-object v1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->v:LX/Dw9;

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v2, v2, LX/DwE;->b:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v3, v3, LX/DwE;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    .line 2059689
    new-instance v5, LX/1Rc;

    new-instance v6, LX/3EE;

    const/4 v7, -0x1

    .line 2059690
    new-instance v8, LX/CDf;

    const/4 p0, 0x0

    invoke-direct {v8, v2, v3, p0}, LX/CDf;-><init>(III)V

    invoke-static {v8}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v8

    move-object v8, v8

    .line 2059691
    new-instance p0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-direct {v6, v0, v7, v8, p0}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    iget-object v7, v1, LX/Dw9;->a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-direct {v5, v6, v7}, LX/1Rc;-><init>(Ljava/lang/Object;LX/1Nt;)V

    iput-object v5, v1, LX/Dw9;->d:LX/1Rc;

    .line 2059692
    iget-object v5, v1, LX/Dw9;->d:LX/1Rc;

    iget-object v6, v1, LX/Dw9;->b:LX/Dw7;

    invoke-virtual {v5, v6}, LX/1Ra;->a(LX/1PW;)V

    .line 2059693
    iget-object v5, v1, LX/Dw9;->d:LX/1Rc;

    iget-object v6, v1, LX/Dw9;->c:LX/03V;

    invoke-static {v4, v5, v6}, LX/1aL;->a(Landroid/view/View;LX/1Ra;LX/03V;)V

    .line 2059694
    iget-object v5, v1, LX/Dw9;->d:LX/1Rc;

    invoke-virtual {v5, v4}, LX/1Ra;->a(Landroid/view/View;)V

    .line 2059695
    goto/16 :goto_0

    .line 2059696
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 2059836
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->e()V

    .line 2059837
    iget-object v2, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 2059838
    iget-object v5, v4, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v1, v1, v1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2059839
    iget-object v5, v4, LX/DwE;->c:LX/1aX;

    invoke-virtual {v5, v6}, LX/1aX;->a(LX/1aZ;)V

    .line 2059840
    const-string v5, ""

    iput-object v5, v4, LX/DwE;->d:Ljava/lang/String;

    .line 2059841
    iput-object v6, v4, LX/DwE;->e:Landroid/net/Uri;

    .line 2059842
    const-string v5, ""

    iput-object v5, v4, LX/DwE;->h:Ljava/lang/CharSequence;

    .line 2059843
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2059844
    :cond_0
    return-void
.end method

.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2059570
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->w:LX/Dw2;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->w:LX/Dw2;

    invoke-virtual {v0, p1}, LX/556;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2059571
    const/4 v0, 0x1

    .line 2059572
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 1

    .prologue
    .line 2059573
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->w:LX/Dw2;

    .line 2059574
    iget-object p0, v0, LX/3t3;->b:Ljava/lang/Object;

    move-object v0, p0

    .line 2059575
    check-cast v0, Landroid/view/accessibility/AccessibilityNodeProvider;

    return-object v0
.end method

.method public getNumOfItems()I
    .locals 1

    .prologue
    .line 2059576
    const/4 v0, 0x1

    return v0
.end method

.method public getRowHeight()I
    .locals 1

    .prologue
    .line 2059577
    const/4 v0, 0x0

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0xf436049

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2059578
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onAttachedToWindow()V

    .line 2059579
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->d()V

    .line 2059580
    const/16 v1, 0x2d

    const v2, 0x713308de

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x61e10a68

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2059581
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onDetachedFromWindow()V

    .line 2059582
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->e()V

    .line 2059583
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->c()V

    .line 2059584
    const/16 v1, 0x2d

    const v2, 0x799f4a7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    .line 2059585
    iget-object v2, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 2059586
    iget-object v0, v4, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 2059587
    iget-object v0, v4, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2059588
    iget-object v0, v4, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2059589
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->f:LX/AmY;

    iget-object v4, v4, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v0, p1, v4}, LX/AmY;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    .line 2059590
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2059591
    :cond_1
    iget-object v0, v4, LX/DwE;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    .line 2059592
    iget-object v0, v4, LX/DwE;->i:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2059593
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AmR;

    iget-object v5, v4, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    iget-object v6, v4, LX/DwE;->i:Ljava/lang/String;

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 2059594
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2059595
    :cond_2
    :goto_2
    iget-object v0, v4, LX/DwE;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->aw()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2059596
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->g:LX/26C;

    iget-object v4, v4, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v0, p1, v4}, LX/26C;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    goto :goto_1

    .line 2059597
    :cond_3
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 2059598
    return-void

    .line 2059599
    :cond_4
    iget-object v7, v0, LX/AmR;->a:Landroid/content/Context;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f030f3a

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/facebook/widget/CustomFrameLayout;

    .line 2059600
    const v8, 0x7f0d24e2

    invoke-virtual {v7, v8}, Lcom/facebook/widget/CustomFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/facebook/widget/text/BetterTextView;

    .line 2059601
    invoke-virtual {v8, v6}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2059602
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/facebook/widget/CustomFrameLayout;->measure(II)V

    .line 2059603
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v8

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v9

    invoke-virtual {v7, v10, v10, v8, v9}, Lcom/facebook/widget/CustomFrameLayout;->layout(IIII)V

    .line 2059604
    iget v8, v5, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    iget v9, v5, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    invoke-virtual {p1, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2059605
    invoke-virtual {v7, p1}, Lcom/facebook/widget/CustomFrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 2059606
    iget v7, v5, Landroid/graphics/Rect;->left:I

    neg-int v7, v7

    int-to-float v7, v7

    iget v8, v5, Landroid/graphics/Rect;->top:I

    neg-int v8, v8

    int-to-float v8, v8

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_2
.end method

.method public final onFinishTemporaryDetach()V
    .locals 0

    .prologue
    .line 2059607
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onFinishTemporaryDetach()V

    .line 2059608
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->d()V

    .line 2059609
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 2059610
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    invoke-static {v0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(LX/DwE;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2059611
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    iget-object v1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v1, v1, LX/DwE;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v2, v2, LX/DwE;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v3, v3, LX/DwE;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v4, v4, LX/DwE;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->layout(IIII)V

    .line 2059612
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 2059613
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    invoke-static {v0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(LX/DwE;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2059614
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->u:Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    iget-object v1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v1, v1, LX/DwE;->b:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->t:LX/DwE;

    iget-object v2, v2, LX/DwE;->b:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->measure(II)V

    .line 2059615
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v0, p2}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 2059616
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getRowHeight()I

    move-result v1

    .line 2059617
    if-ne v1, v0, :cond_1

    .line 2059618
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;->onMeasure(II)V

    .line 2059619
    :goto_0
    return-void

    .line 2059620
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 2059621
    invoke-virtual {p0, v0, v1}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public final onStartTemporaryDetach()V
    .locals 0

    .prologue
    .line 2059622
    invoke-super {p0}, Lcom/facebook/widget/CustomViewGroup;->onStartTemporaryDetach()V

    .line 2059623
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->e()V

    .line 2059624
    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 2059625
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eq v1, v2, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eq v1, v7, :cond_1

    .line 2059626
    :cond_0
    :goto_0
    return v0

    .line 2059627
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-eq v1, v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v7, :cond_5

    .line 2059628
    :cond_2
    iget-object v3, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 2059629
    iget-object v6, v5, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_3

    .line 2059630
    iget-object v5, v5, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2059631
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2059632
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->invalidate()V

    .line 2059633
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v7, :cond_6

    move v0, v2

    .line 2059634
    goto :goto_0

    .line 2059635
    :cond_6
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 2059636
    invoke-virtual {p0, v1}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->getLocationInWindow([I)V

    .line 2059637
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    aget v4, v1, v0

    int-to-float v4, v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    .line 2059638
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    aget v1, v1, v2

    int-to-float v1, v1

    sub-float v1, v4, v1

    float-to-int v1, v1

    .line 2059639
    const/4 v4, 0x0

    .line 2059640
    if-gez v1, :cond_7

    move v1, v4

    .line 2059641
    :cond_7
    if-gez v3, :cond_8

    move v3, v4

    .line 2059642
    :cond_8
    iget-object v6, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    array-length v7, v6

    move v5, v4

    :goto_2
    if-ge v5, v7, :cond_c

    aget-object v4, v6, v5

    .line 2059643
    iget-object p1, v4, LX/DwE;->b:Landroid/graphics/Rect;

    if-eqz p1, :cond_b

    iget-object p1, v4, LX/DwE;->b:Landroid/graphics/Rect;

    invoke-virtual {p1, v3, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result p1

    if-eqz p1, :cond_b

    .line 2059644
    :goto_3
    move-object v3, v4

    .line 2059645
    if-eqz v3, :cond_0

    .line 2059646
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2059647
    :goto_4
    iget-object v0, v3, LX/DwE;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, v3, LX/DwE;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v4, v3, LX/DwE;->b:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v3, v3, LX/DwE;->b:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v0, v1, v4, v3}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->invalidate(IIII)V

    move v0, v2

    .line 2059648
    goto :goto_0

    .line 2059649
    :pswitch_0
    iget-object v0, v3, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->l:I

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_4

    .line 2059650
    :pswitch_1
    iget-object v0, v3, LX/DwE;->c:LX/1aX;

    .line 2059651
    iget-object v1, v0, LX/1aX;->f:LX/1aZ;

    move-object v1, v1

    .line 2059652
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9

    move-object v0, v1

    check-cast v0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    .line 2059653
    iget-boolean v4, v0, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;->e:Z

    move v0, v4

    .line 2059654
    if-eqz v0, :cond_9

    .line 2059655
    check-cast v1, Lcom/facebook/drawee/fbpipeline/DegradableDraweeController;

    invoke-virtual {v1}, LX/1bp;->onClick()Z

    goto :goto_4

    .line 2059656
    :cond_9
    iget-object v0, v3, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    if-eqz v0, :cond_a

    .line 2059657
    iget-object v0, v3, LX/DwE;->d:Ljava/lang/String;

    iget-object v1, v3, LX/DwE;->g:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 2059658
    iget-object v4, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Dv9;

    new-instance v5, LX/DvB;

    iget-object v6, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->n:Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;

    iget-object v7, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->o:LX/DvW;

    invoke-direct {v5, v0, v6, v7, v1}, LX/DvB;-><init>(Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;LX/DvW;Lcom/facebook/graphql/model/GraphQLVideo;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2059659
    goto :goto_4

    .line 2059660
    :cond_a
    iget-object v0, v3, LX/DwE;->d:Ljava/lang/String;

    iget-object v1, v3, LX/DwE;->e:Landroid/net/Uri;

    iget-object v4, v3, LX/DwE;->f:Lcom/facebook/graphql/model/GraphQLPhoto;

    iget-object v5, v3, LX/DwE;->j:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v4, v5}, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->a(Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/graphql/model/GraphQLPhoto;Ljava/lang/String;)V

    goto :goto_4

    .line 2059661
    :cond_b
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_2

    .line 2059662
    :cond_c
    const/4 v4, 0x0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2059663
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v2

    .line 2059664
    if-eqz v2, :cond_1

    .line 2059665
    :cond_0
    :goto_0
    return v0

    .line 2059666
    :cond_1
    iget-object v3, p0, Lcom/facebook/photos/pandora/common/ui/views/BasePandoraMultiMediaRowView;->m:[LX/DwE;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2059667
    iget-object v5, v5, LX/DwE;->a:Landroid/graphics/drawable/Drawable;

    if-eq v5, p1, :cond_0

    .line 2059668
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2059669
    goto :goto_0
.end method
