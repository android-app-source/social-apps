.class public Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2059885
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2059886
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->a()V

    .line 2059887
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2059888
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2059889
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->a()V

    .line 2059890
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2059891
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2059892
    invoke-direct {p0}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->a()V

    .line 2059893
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2059894
    const v0, 0x7f030ef5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2059895
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 2059896
    invoke-virtual {p0, v0}, Lcom/facebook/photos/pandora/common/ui/views/PandoraBennyLoadingSpinnerView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2059897
    return-void
.end method
