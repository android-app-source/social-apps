.class public final Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

.field public final b:Lcom/facebook/graphql/model/GraphQLPhoto;

.field public final c:D

.field public d:Z

.field public e:LX/Dvz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2059480
    new-instance v0, LX/Dvy;

    invoke-direct {v0}, LX/Dvy;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2059481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059482
    const-class v0, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->a:Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    .line 2059483
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2059484
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->c:D

    .line 2059485
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->d:Z

    .line 2059486
    return-void
.end method

.method public constructor <init>(Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;Lcom/facebook/graphql/model/GraphQLPhoto;D)V
    .locals 1

    .prologue
    .line 2059487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059488
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->a:Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    .line 2059489
    iput-object p2, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 2059490
    iput-wide p3, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->c:D

    .line 2059491
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->d:Z

    .line 2059492
    return-void
.end method


# virtual methods
.method public final a()LX/Dvz;
    .locals 3

    .prologue
    const v2, 0x3f8ccccd    # 1.1f

    .line 2059493
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2059494
    :cond_0
    const/4 v0, 0x0

    .line 2059495
    :goto_0
    return-object v0

    .line 2059496
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->e:LX/Dvz;

    if-eqz v0, :cond_2

    .line 2059497
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->e:LX/Dvz;

    goto :goto_0

    .line 2059498
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    .line 2059499
    sget-object v0, LX/Dvz;->LANDSCAPE:LX/Dvz;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->e:LX/Dvz;

    .line 2059500
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->e:LX/Dvz;

    goto :goto_0

    .line 2059501
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_4

    .line 2059502
    sget-object v0, LX/Dvz;->PORTRAIT:LX/Dvz;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->e:LX/Dvz;

    .line 2059503
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->e:LX/Dvz;

    goto :goto_0

    .line 2059504
    :cond_4
    sget-object v0, LX/Dvz;->SQUARE:LX/Dvz;

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->e:LX/Dvz;

    .line 2059505
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->e:LX/Dvz;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2059506
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2059507
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->a:Lcom/facebook/photos/pandora/common/data/model/PandoraDataModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2059508
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->b:Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2059509
    iget-wide v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->c:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 2059510
    iget-boolean v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererMultiPhotosRow$PandoraMultiPhotoStoryEntry;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 2059511
    return-void
.end method
