.class public Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2059100
    new-instance v0, LX/Dve;

    invoke-direct {v0}, LX/Dve;-><init>()V

    sput-object v0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2059104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059105
    iput-object p1, p0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;->a:LX/0Px;

    .line 2059106
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2059107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2059108
    const-class v0, Lcom/facebook/photos/pandora/common/ui/renderer/rows/PandoraRendererRow;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;->a:LX/0Px;

    .line 2059109
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2059103
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2059101
    iget-object v0, p0, Lcom/facebook/photos/pandora/common/ui/renderer/PandoraRendererResult;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2059102
    return-void
.end method
