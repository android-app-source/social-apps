.class public Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:Lcom/facebook/common/callercontext/CallerContext;

.field public final b:I

.field public c:D

.field public d:D

.field public e:I

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;"
        }
    .end annotation
.end field

.field public h:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2057837
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2057838
    const-class v0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;

    const-string v1, "video"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2057839
    const v0, 0x7f0d0099

    iput v0, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->b:I

    .line 2057840
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->g:Ljava/util/List;

    .line 2057841
    new-instance v0, LX/Duu;

    invoke-direct {v0, p0}, LX/Duu;-><init>(Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;)V

    iput-object v0, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->h:Landroid/view/View$OnClickListener;

    .line 2057842
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;

    const/16 v0, 0x2e01

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->f:LX/0Ot;

    .line 2057843
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 2057844
    invoke-virtual {p0}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->getChildCount()I

    move-result v2

    .line 2057845
    sub-int v0, p4, p2

    .line 2057846
    int-to-double v0, v0

    iget v3, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->e:I

    int-to-double v4, v3

    iget-wide v6, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->c:D

    mul-double/2addr v4, v6

    iget v3, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->e:I

    add-int/lit8 v3, v3, -0x1

    int-to-double v6, v3

    iget-wide v8, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->d:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    sub-double/2addr v0, v4

    iget v3, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->e:I

    add-int/lit8 v3, v3, -0x1

    int-to-double v4, v3

    div-double/2addr v0, v4

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v0

    .line 2057847
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 2057848
    invoke-virtual {p0, v1}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2057849
    if-eqz v3, :cond_1

    .line 2057850
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 2057851
    const/4 v0, 0x0

    .line 2057852
    if-eqz v6, :cond_0

    .line 2057853
    iget-wide v8, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->d:D

    iget-wide v10, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->c:D

    add-double/2addr v8, v10

    int-to-double v10, v6

    mul-double/2addr v8, v10

    int-to-double v6, v6

    mul-double/2addr v6, v4

    add-double/2addr v6, v8

    double-to-int v0, v6

    .line 2057854
    :cond_0
    iget-wide v6, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->c:D

    double-to-int v6, v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 2057855
    iget-wide v8, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->c:D

    double-to-int v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 2057856
    invoke-virtual {v3, v6, v7}, Landroid/view/View;->measure(II)V

    .line 2057857
    const/4 v6, 0x0

    int-to-double v8, v0

    iget-wide v10, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->c:D

    add-double/2addr v8, v10

    double-to-int v7, v8

    iget-wide v8, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->c:D

    double-to-int v8, v8

    invoke-virtual {v3, v0, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 2057858
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2057859
    :cond_2
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    .line 2057860
    invoke-virtual {p0}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 2057861
    iget-wide v2, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->d:D

    iget-wide v4, p0, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->c:D

    add-double/2addr v2, v4

    double-to-int v1, v2

    .line 2057862
    invoke-virtual {p0, v0, v1}, Lcom/facebook/photos/albums/video/ui/VideoAlbumPermalinkRowView;->setMeasuredDimension(II)V

    .line 2057863
    return-void
.end method
