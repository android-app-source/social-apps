.class public Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/9bj;

.field private r:LX/D3w;

.field private s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1C2;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/121;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2057620
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2057621
    new-instance v0, LX/Duo;

    invoke-direct {v0, p0}, LX/Duo;-><init>(Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;)V

    iput-object v0, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->q:LX/9bj;

    .line 2057622
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Long;LX/Dup;LX/0JR;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2057623
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2057624
    const-string v1, "target_actor_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "target_actor_type"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "origin"

    iget-object v3, p3, LX/0JR;->value:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2057625
    return-object v0
.end method

.method private a(LX/0Ot;LX/D3w;LX/0Ot;LX/121;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;",
            "LX/D3w;",
            "LX/0Ot",
            "<",
            "LX/1C2;",
            ">;",
            "LX/121;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2057626
    iput-object p1, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->p:LX/0Ot;

    .line 2057627
    iput-object p2, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->r:LX/D3w;

    .line 2057628
    iput-object p3, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->s:LX/0Ot;

    .line 2057629
    iput-object p4, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->t:LX/121;

    .line 2057630
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;

    const/16 v0, 0x2e01

    invoke-static {v1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1}, LX/D3w;->b(LX/0QB;)LX/D3w;

    move-result-object v0

    check-cast v0, LX/D3w;

    const/16 v3, 0x1335

    invoke-static {v1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v1}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v1

    check-cast v1, LX/121;

    invoke-direct {p0, v2, v0, v3, v1}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->a(LX/0Ot;LX/D3w;LX/0Ot;LX/121;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;)V
    .locals 2

    .prologue
    .line 2057631
    invoke-virtual {p0}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "origin"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0JR;->asOriginType(Ljava/lang/String;)LX/0JR;

    move-result-object v0

    .line 2057632
    if-eqz v0, :cond_0

    sget-object v1, LX/0JR;->PAGE_VIDEO_HUB:LX/0JR;

    invoke-virtual {v0, v1}, LX/0JR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2057633
    iget-object v0, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->r:LX/D3w;

    sget-object v1, LX/04D;->PAGE_TIMELINE:LX/04D;

    invoke-virtual {v0, p1, p0, v1}, LX/D3w;->b(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;Landroid/content/Context;LX/04D;)V

    .line 2057634
    :goto_0
    return-void

    .line 2057635
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->r:LX/D3w;

    sget-object v1, LX/04D;->VIDEO_ALBUM_PERMALINK:LX/04D;

    invoke-virtual {v0, p1, p0, v1}, LX/D3w;->b(Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;Landroid/content/Context;LX/04D;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2057636
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2057637
    invoke-static {p0, p0}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2057638
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2057639
    invoke-virtual {p0}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Extras of the intent was expected to hold a long value for target id"

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2057640
    iget-object v0, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1C2;

    invoke-virtual {p0}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "origin"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0JR;->asOriginType(Ljava/lang/String;)LX/0JR;

    move-result-object v2

    const/4 v6, 0x0

    .line 2057641
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v5, LX/04A;->VIDEO_ALBUM_PERMALINK_ENTER:LX/04A;

    iget-object v5, v5, LX/04A;->value:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v5, LX/0JQ;->ALBUM_ORIGIN:LX/0JQ;

    iget-object v5, v5, LX/0JQ;->value:Ljava/lang/String;

    iget-object v7, v2, LX/0JR;->value:Ljava/lang/String;

    invoke-virtual {v4, v5, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2057642
    const/4 v8, 0x0

    move-object v4, v0

    move-object v7, v6

    move-object v9, v6

    move-object v10, v6

    invoke-static/range {v4 .. v10}, LX/1C2;->a(LX/1C2;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/0lF;ZLX/04D;LX/04G;)LX/1C2;

    .line 2057643
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2057644
    const v0, 0x7f03157d

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->setContentView(I)V

    .line 2057645
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2057646
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2057647
    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2057648
    new-instance v1, LX/Dum;

    invoke-direct {v1, p0}, LX/Dum;-><init>(Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2057649
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->t:LX/121;

    sget-object v1, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    const v2, 0x7f080e4a

    invoke-virtual {p0, v2}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Dun;

    invoke-direct {v3, p0}, LX/Dun;-><init>(Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;)V

    invoke-virtual {v0, v1, v2, v3}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 2057650
    invoke-virtual {p0}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "target_actor_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Dup;

    .line 2057651
    invoke-virtual {p0}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "target_actor_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2057652
    new-instance v1, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;

    invoke-direct {v1}, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;-><init>()V

    .line 2057653
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2057654
    const-string v5, "target_actor_id"

    invoke-virtual {v4, v5, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2057655
    const-string v5, "target_actor_type"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2057656
    invoke-virtual {v1, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2057657
    move-object v0, v1

    .line 2057658
    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2057659
    return-void

    :cond_1
    move v0, v1

    .line 2057660
    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x34ed763f    # -9603521.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2057661
    iget-object v0, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->q:LX/9bj;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2057662
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2057663
    const/16 v0, 0x23

    const v2, 0x6001f5e

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x61f8bb41

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2057664
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2057665
    iget-object v0, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkActivity;->q:LX/9bj;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2057666
    const/16 v0, 0x23

    const v2, -0x34f174b9    # -9341767.0f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
