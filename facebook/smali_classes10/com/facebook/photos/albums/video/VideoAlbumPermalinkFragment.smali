.class public Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Dut;

.field private b:LX/Dus;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2057810
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2057807
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2057808
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;

    const-class v0, LX/Dut;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/Dut;

    iput-object p1, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;->a:LX/Dut;

    .line 2057809
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x748ba43

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2057815
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2057816
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "The arguments for the fragment should have a long value for user id which is missing"

    invoke-static {v0, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2057817
    iget-object v2, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;->a:LX/Dut;

    .line 2057818
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2057819
    const-string v3, "target_actor_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2057820
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2057821
    const-string v4, "target_actor_type"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Dup;

    .line 2057822
    new-instance v6, LX/Dus;

    const/16 v7, 0x12b1

    invoke-static {v2, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v7, 0xafd

    invoke-static {v2, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v7, 0x1b

    invoke-static {v2, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    move-object v7, v3

    move-object v8, v0

    invoke-direct/range {v6 .. v11}, LX/Dus;-><init>(Ljava/lang/Long;LX/Dup;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2057823
    move-object v0, v6

    .line 2057824
    iput-object v0, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;->b:LX/Dus;

    .line 2057825
    new-instance v0, Landroid/widget/ListView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 2057826
    iget-object v2, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;->b:LX/Dus;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2057827
    iget-object v2, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;->b:LX/Dus;

    invoke-virtual {v2}, LX/Dus;->a()V

    .line 2057828
    const v2, 0x3208ff61

    invoke-static {v2, v1}, LX/02F;->f(II)V

    return-object v0

    .line 2057829
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2736cbbc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2057811
    iget-object v1, p0, Lcom/facebook/photos/albums/video/VideoAlbumPermalinkFragment;->b:LX/Dus;

    .line 2057812
    iget-object v2, v1, LX/Dus;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2057813
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2057814
    const/16 v1, 0x2b

    const v2, -0x374652f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
