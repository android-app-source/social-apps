.class public Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Dxl;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2063221
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x731c7297    # -3.5059996E-31f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2063222
    const v0, 0x7f0300ce

    invoke-virtual {p1, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2063223
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const v3, 0x7f0d00bc

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 2063224
    const v3, 0x7f0811e9

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2063225
    invoke-interface {v1, v5}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2063226
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2063227
    const-string v3, "arg_album"

    invoke-static {v1, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2063228
    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorsFragment;->a:LX/Dxl;

    if-nez v3, :cond_0

    .line 2063229
    new-instance v3, LX/Dxl;

    invoke-direct {v3}, LX/Dxl;-><init>()V

    iput-object v3, p0, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorsFragment;->a:LX/Dxl;

    .line 2063230
    :cond_0
    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorsFragment;->a:LX/Dxl;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v5

    .line 2063231
    iput-object v5, v3, LX/Dxl;->a:LX/0Px;

    .line 2063232
    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorsFragment;->a:LX/Dxl;

    const v5, -0x4a73882d

    invoke-static {v3, v5}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2063233
    const v1, 0x7f0d0519

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 2063234
    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/contributors/AlbumPermalinkContributorsFragment;->a:LX/Dxl;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2063235
    const/16 v1, 0x2b

    const v3, 0x22ccea5f

    invoke-static {v4, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method
