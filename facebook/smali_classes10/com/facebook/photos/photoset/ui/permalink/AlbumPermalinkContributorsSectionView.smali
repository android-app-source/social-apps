.class public Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2063287
    const-class v0, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2063284
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2063285
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a()V

    .line 2063286
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2063281
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2063282
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a()V

    .line 2063283
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2063278
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2063279
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a()V

    .line 2063280
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2063276
    const-class v0, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;

    invoke-static {v0, p0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2063277
    return-void
.end method

.method private a(LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x3

    const/4 v4, 0x0

    .line 2063239
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2063240
    new-array v2, v6, [I

    const v0, 0x7f0d2467

    aput v0, v2, v4

    const v0, 0x7f0d2468

    aput v0, v2, v1

    const/4 v0, 0x2

    const v1, 0x7f0d2469

    aput v1, v2, v0

    move v1, v4

    .line 2063241
    :goto_1
    if-ge v1, v6, :cond_1

    .line 2063242
    aget v0, v2, v1

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0, p1, v1}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/0Px;I)V

    .line 2063243
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    move v0, v4

    .line 2063244
    goto :goto_0

    .line 2063245
    :cond_1
    const v0, 0x7f0d246a

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2063246
    const v1, 0x7f0d246b

    invoke-virtual {p0, v1}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2063247
    const v2, 0x7f0d246c

    invoke-virtual {p0, v2}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 2063248
    const v3, 0x7f0d246d

    invoke-virtual {p0, v3}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2063249
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    if-lt v6, v5, :cond_2

    .line 2063250
    invoke-virtual {v0, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2063251
    :goto_2
    return-void

    .line 2063252
    :cond_2
    invoke-static {v3, p1, v6}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/0Px;I)V

    .line 2063253
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    const/4 v6, 0x4

    if-le v5, v6, :cond_3

    .line 2063254
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "+"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x4

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2063255
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2063256
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2063257
    :goto_3
    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2063258
    invoke-virtual {v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2063259
    invoke-virtual {v2}, Landroid/widget/ImageView;->bringToFront()V

    .line 2063260
    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->bringToFront()V

    goto :goto_2

    .line 2063261
    :cond_3
    invoke-virtual {v1, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2063262
    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3
.end method

.method private static a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/0Px;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 2063270
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 2063271
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2063272
    :cond_0
    :goto_0
    return-void

    .line 2063273
    :cond_1
    invoke-virtual {p1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2063274
    invoke-virtual {p1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2063275
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;

    const/16 v1, 0x2e01

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->b:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLAlbum;)V
    .locals 2

    .prologue
    .line 2063263
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->setVisibility(I)V

    .line 2063264
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2063265
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 2063266
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->setVisibility(I)V

    .line 2063267
    new-instance v0, LX/Dxm;

    invoke-direct {v0, p0, p1}, LX/Dxm;-><init>(Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;Lcom/facebook/graphql/model/GraphQLAlbum;)V

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2063268
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/AlbumPermalinkContributorsSectionView;->a(LX/0Px;)V

    .line 2063269
    :cond_0
    return-void
.end method
