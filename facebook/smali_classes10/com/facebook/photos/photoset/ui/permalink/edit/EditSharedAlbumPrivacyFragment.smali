.class public Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/Dy1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/listview/BetterListView;

.field private c:LX/Dy3;

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2063864
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;

    invoke-static {p0}, LX/Dy1;->a(LX/0QB;)LX/Dy1;

    move-result-object p0

    check-cast p0, LX/Dy1;

    iput-object p0, p1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->a:LX/Dy1;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2063865
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2063866
    const-class v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;

    invoke-static {v0, p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2063867
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2063868
    const-string v1, "input_album"

    invoke-static {v0, v1}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2063869
    if-nez v0, :cond_0

    .line 2063870
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should supply INPUT_ALBUM for editing privacy"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2063871
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 2063872
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063873
    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->a:LX/Dy1;

    invoke-virtual {v3, v0}, LX/Dy1;->a(LX/1oV;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2063874
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2063875
    :cond_2
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->d:LX/0Px;

    .line 2063876
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2063877
    const-string v1, "selected_album_privacy"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063878
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x1584f55e

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2063879
    const v0, 0x7f03045b

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2063880
    const v0, 0x7f0d04af

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    .line 2063881
    new-instance v0, LX/Dy3;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->d:LX/0Px;

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->a:LX/Dy1;

    invoke-direct {v0, v3, v4, v5}, LX/Dy3;-><init>(Landroid/content/Context;LX/0Px;LX/Dy1;)V

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->c:LX/Dy3;

    .line 2063882
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->c:LX/Dy3;

    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063883
    iput-object v3, v0, LX/Dy3;->e:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063884
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->c:LX/Dy3;

    new-instance v3, LX/Dy0;

    invoke-direct {v3, p0}, LX/Dy0;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;)V

    .line 2063885
    iput-object v3, v0, LX/Dy3;->c:LX/Dy0;

    .line 2063886
    new-instance v4, LX/Dy2;

    invoke-direct {v4, v0}, LX/Dy2;-><init>(LX/Dy3;)V

    iput-object v4, v0, LX/Dy3;->d:Landroid/view/View$OnClickListener;

    .line 2063887
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;->c:LX/Dy3;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2063888
    const/16 v0, 0x2b

    const v3, -0x36ad1190    # -863975.0f

    invoke-static {v6, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method
