.class public Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2063848
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2063849
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2063850
    const v0, 0x7f03045a

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyActivity;->setContentView(I)V

    .line 2063851
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063852
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2063853
    const v1, 0x7f08130b

    invoke-virtual {p0, v1}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2063854
    new-instance v1, LX/Dxz;

    invoke-direct {v1, p0}, LX/Dxz;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2063855
    :cond_0
    if-nez p1, :cond_1

    .line 2063856
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2063857
    new-instance v1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;

    invoke-direct {v1}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditSharedAlbumPrivacyFragment;-><init>()V

    .line 2063858
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2063859
    move-object v0, v1

    .line 2063860
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    .line 2063861
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2063862
    :cond_1
    return-void
.end method
