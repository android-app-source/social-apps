.class public Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final v:Ljava/lang/String;


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/view/View;

.field private D:Landroid/view/View;

.field private E:Landroid/view/View;

.field private F:Landroid/view/View;

.field private G:Landroid/view/View;

.field private H:Landroid/view/View;

.field private I:Landroid/view/View;

.field private J:Landroid/view/View;

.field public K:Lcom/facebook/widget/FbScrollView;

.field public final L:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljava/lang/String;

.field public N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field private O:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field private P:Ljava/lang/Boolean;

.field public Q:Lcom/facebook/graphql/model/GraphQLAlbum;

.field private R:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public T:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field public V:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public W:Z

.field private X:Z

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9fy;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dy1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Landroid/widget/EditText;

.field public x:Landroid/widget/EditText;

.field public y:Landroid/widget/CheckBox;

.field private z:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2063758
    const-class v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2063759
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2063760
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->L:Ljava/util/ArrayList;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLAlbum;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2063761
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2063762
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063763
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2063764
    const-string v0, "album"

    invoke-static {v1, v0, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2063765
    return-object v1

    .line 2063766
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLAlbum;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 2063767
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2063768
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2063769
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2063770
    invoke-static {v0}, LX/1xl;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2063771
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2063772
    :cond_1
    invoke-direct {p0, v2}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/ipc/model/FacebookProfile;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2063773
    iget-wide v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2063774
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2063775
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2063776
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2063777
    :cond_0
    invoke-direct {p0, v1}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2063778
    iget-boolean v2, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->X:Z

    if-nez v2, :cond_1

    move v2, v1

    .line 2063779
    :goto_0
    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2063780
    :goto_1
    if-eqz v1, :cond_0

    .line 2063781
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->q()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;LX/0Px;)V

    .line 2063782
    :cond_0
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->l()Z

    move-result v1

    invoke-static {p0, v1}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->b(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;Z)V

    .line 2063783
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->I:Landroid/view/View;

    if-eqz v2, :cond_3

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2063784
    return-void

    :cond_1
    move v2, v0

    .line 2063785
    goto :goto_0

    :cond_2
    move v1, v0

    .line 2063786
    goto :goto_1

    .line 2063787
    :cond_3
    const/16 v0, 0x8

    goto :goto_2
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPrivacyScope;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2063788
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    .line 2063789
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dy1;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/Dy1;->a(LX/1oV;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2063790
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->L:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2063791
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2063792
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->O:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063793
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2063794
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->r()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2063795
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->r()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;->j()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->O:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063796
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->O:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063797
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->F:Landroid/view/View;

    new-instance v1, LX/Dxy;

    invoke-direct {v1, p0}, LX/Dxy;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2063798
    return-void
.end method

.method private static a(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;",
            "LX/0Ot",
            "<",
            "LX/9fy;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dy1;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2063844
    iput-object p1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->p:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->q:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->r:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->s:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->t:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->u:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;

    const/16 v1, 0x2e5f

    invoke-static {v6, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x12b1

    invoke-static {v6, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x455

    invoke-static {v6, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2eb1

    invoke-static {v6, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x259

    invoke-static {v6, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v6}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;)V

    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2063799
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063800
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    .line 2063801
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;ZZ)V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2063802
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->U:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 2063803
    :goto_0
    iget-boolean v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->X:Z

    if-nez v3, :cond_4

    if-eqz p1, :cond_4

    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-gt v3, v1, :cond_0

    if-eqz v0, :cond_4

    :cond_0
    move v3, v1

    .line 2063804
    :goto_1
    iget-boolean v4, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->X:Z

    if-nez v4, :cond_5

    if-nez v3, :cond_5

    if-eqz p1, :cond_5

    if-eqz p2, :cond_5

    .line 2063805
    :goto_2
    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->G:Landroid/view/View;

    if-nez v3, :cond_1

    if-eqz v1, :cond_6

    :cond_1
    move v4, v2

    :goto_3
    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2063806
    iget-object v4, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->C:Landroid/view/View;

    if-eqz v1, :cond_7

    move v1, v2

    :goto_4
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2063807
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->D:Landroid/view/View;

    if-eqz v3, :cond_8

    :goto_5
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2063808
    if-eqz v3, :cond_2

    .line 2063809
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->U:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 2063810
    :goto_6
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2063811
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2063812
    return-void

    :cond_3
    move v0, v2

    .line 2063813
    goto :goto_0

    :cond_4
    move v3, v2

    .line 2063814
    goto :goto_1

    :cond_5
    move v1, v2

    .line 2063815
    goto :goto_2

    :cond_6
    move v4, v5

    .line 2063816
    goto :goto_3

    :cond_7
    move v1, v5

    .line 2063817
    goto :goto_4

    :cond_8
    move v2, v5

    .line 2063818
    goto :goto_5

    .line 2063819
    :cond_9
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-direct {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Lcom/facebook/graphql/model/GraphQLAlbum;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6
.end method

.method private b(Ljava/util/List;)Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2063820
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2063821
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2063822
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 2063823
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v1, :cond_1

    .line 2063824
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 2063825
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0088

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-virtual {v5, v6, v4, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2063826
    :goto_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 2063827
    goto :goto_0

    .line 2063828
    :cond_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2063829
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->u:Ljava/lang/String;

    invoke-static {v0, v1}, LX/Dxo;->b(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2063830
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2063831
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->x:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2063832
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->E:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 2063833
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->w:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2063834
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2063835
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->x:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2063836
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2063837
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->A:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2063838
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->P:Ljava/lang/Boolean;

    .line 2063839
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->E:Landroid/view/View;

    new-instance v1, LX/Dxu;

    invoke-direct {v1, p0}, LX/Dxu;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2063840
    return-void
.end method

.method public static synthetic b(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V
    .locals 0

    .prologue
    .line 2063841
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public static b(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;Z)V
    .locals 2

    .prologue
    .line 2063842
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->z:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->c(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2063843
    return-void
.end method

.method private c(Z)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2063725
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez v0, :cond_0

    .line 2063726
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dy1;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v1

    .line 2063727
    if-eqz p1, :cond_1

    .line 2063728
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {v0, v2}, LX/Dy1;->a(LX/Dy1;LX/1Fd;)Ljava/lang/String;

    move-result-object v2

    .line 2063729
    :goto_0
    move-object v0, v2

    .line 2063730
    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dy1;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063731
    if-eqz p1, :cond_2

    .line 2063732
    invoke-interface {v1}, LX/1oU;->b()LX/1Fd;

    move-result-object v2

    invoke-static {v0, v2}, LX/Dy1;->a(LX/Dy1;LX/1Fd;)Ljava/lang/String;

    move-result-object v2

    .line 2063733
    :goto_2
    move-object v0, v2

    .line 2063734
    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, v2, p0}, LX/Dy1;->a(LX/Dy1;LX/1Fd;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    invoke-interface {v1}, LX/1oU;->b()LX/1Fd;

    move-result-object v2

    invoke-interface {v1}, LX/1oU;->d()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, v2, p0}, LX/Dy1;->a(LX/Dy1;LX/1Fd;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method private l()V
    .locals 11

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2063735
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->u:Ljava/lang/String;

    invoke-static {v0, v3}, LX/Dxo;->b(Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2063736
    :goto_0
    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAlbum;->l()Z

    move-result v5

    .line 2063737
    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-le v3, v1, :cond_4

    move v3, v1

    .line 2063738
    :goto_1
    iget-boolean v6, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->X:Z

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2063739
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063740
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v9

    if-nez v9, :cond_8

    .line 2063741
    :cond_0
    :goto_2
    move v6, v7

    .line 2063742
    if-nez v6, :cond_5

    if-nez v0, :cond_1

    if-eqz v5, :cond_5

    if-eqz v3, :cond_5

    .line 2063743
    :cond_1
    :goto_3
    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->J:Landroid/view/View;

    if-eqz v1, :cond_6

    move v1, v2

    :goto_4
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2063744
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->H:Landroid/view/View;

    if-eqz v0, :cond_7

    :goto_5
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2063745
    invoke-static {p0, v5, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a$redex0(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;ZZ)V

    .line 2063746
    if-eqz v0, :cond_2

    .line 2063747
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->m()V

    .line 2063748
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 2063749
    goto :goto_0

    :cond_4
    move v3, v2

    .line 2063750
    goto :goto_1

    :cond_5
    move v1, v2

    .line 2063751
    goto :goto_3

    :cond_6
    move v1, v4

    .line 2063752
    goto :goto_4

    :cond_7
    move v2, v4

    .line 2063753
    goto :goto_5

    .line 2063754
    :cond_8
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAlbum;->v()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v9

    .line 2063755
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v10

    if-nez v10, :cond_9

    .line 2063756
    if-lez v9, :cond_0

    move v7, v8

    goto :goto_2

    .line 2063757
    :cond_9
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLAlbum;->C()Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLMediaSetMediaConnection;->a()I

    move-result v10

    if-eq v9, v10, :cond_0

    move v7, v8

    goto :goto_2
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2063589
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    new-instance v1, LX/Dxv;

    invoke-direct {v1, p0}, LX/Dxv;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2063590
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 2063591
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->H:Landroid/view/View;

    new-instance v1, LX/Dxw;

    invoke-direct {v1, p0}, LX/Dxw;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2063592
    new-instance v0, LX/Dxx;

    invoke-direct {v0, p0}, LX/Dxx;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V

    .line 2063593
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->C:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2063594
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->D:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2063595
    return-void
.end method

.method private n()Lcom/facebook/graphql/model/GraphQLActor;
    .locals 6

    .prologue
    .line 2063550
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2063551
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2063552
    :goto_1
    return-object v0

    .line 2063553
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2063554
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    goto :goto_1
.end method

.method public static synthetic n(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)Lcom/facebook/graphql/model/GraphQLActor;
    .locals 1

    .prologue
    .line 2063555
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->n()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    return-object v0
.end method

.method public static o(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 2063556
    new-instance v1, LX/4BY;

    invoke-direct {v1, p0}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 2063557
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0811f4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2063558
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/4BY;->setCancelable(Z)V

    .line 2063559
    invoke-virtual {v1}, LX/4BY;->show()V

    .line 2063560
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v6, LX/8HU;->NORMAL_TO_SHARED:LX/8HU;

    .line 2063561
    :goto_0
    sget-object v0, LX/8HU;->NORMAL_TO_SHARED:LX/8HU;

    if-ne v6, v0, :cond_4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->SHARED:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    .line 2063562
    :goto_1
    new-instance v11, LX/Dxp;

    invoke-direct {v11, p0, v1, v0}, LX/Dxp;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;LX/4BY;Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;)V

    .line 2063563
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->s()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2063564
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->M:Ljava/lang/String;

    if-nez v0, :cond_5

    const-string v0, "null"

    :goto_2
    move-object v3, v0

    .line 2063565
    :goto_3
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2063566
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f082451

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2063567
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fy;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->r()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->x:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_4
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->t()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->c()Ljava/lang/String;

    move-result-object v5

    :goto_5
    iget-object v7, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v7}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v8

    iget-object v9, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v9}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v9

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->R:Ljava/util/Set;

    :goto_6
    iget-object v12, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v12}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v12

    if-eqz v12, :cond_2

    iget-object v10, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->S:Ljava/util/Set;

    :cond_2
    invoke-virtual/range {v0 .. v10}, LX/9fy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8HU;Ljava/lang/Boolean;Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;Ljava/util/Set;Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2063568
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tasks-updatePhotoAlbum:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1, v11}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2063569
    return-void

    .line 2063570
    :cond_3
    sget-object v6, LX/8HU;->NO_CONVERSION:LX/8HU;

    goto/16 :goto_0

    .line 2063571
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v0

    goto/16 :goto_1

    .line 2063572
    :cond_5
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->M:Ljava/lang/String;

    goto/16 :goto_2

    :cond_6
    move-object v3, v10

    .line 2063573
    goto/16 :goto_3

    :cond_7
    move-object v4, v10

    .line 2063574
    goto :goto_4

    :cond_8
    move-object v5, v10

    goto :goto_5

    :cond_9
    move-object v9, v10

    goto :goto_6
.end method

.method private p()Z
    .locals 2

    .prologue
    .line 2063575
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->P:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()Z
    .locals 2

    .prologue
    .line 2063576
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->w:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Landroid/widget/TextView;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private r()Z
    .locals 2

    .prologue
    .line 2063577
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2063578
    :goto_0
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->x:Landroid/widget/EditText;

    invoke-static {v1, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Landroid/widget/TextView;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2063579
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->y()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2063580
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private s()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2063581
    iget-boolean v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->W:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 2063582
    :goto_0
    return v0

    .line 2063583
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->M:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v2

    .line 2063584
    goto :goto_0

    .line 2063585
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 2063586
    :goto_1
    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->M:Ljava/lang/String;

    invoke-static {v3, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    .line 2063587
    :cond_2
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->s()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2063588
    goto :goto_0
.end method

.method private t()Z
    .locals 2

    .prologue
    .line 2063596
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->p()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->O:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()Z
    .locals 1

    .prologue
    .line 2063549
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->R:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->R:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->S:Ljava/util/Set;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->S:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()Z
    .locals 2

    .prologue
    .line 2063597
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->l()Z

    move-result v0

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static w(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2063598
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063599
    :goto_0
    return-void

    .line 2063600
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->L:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2063601
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->L:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v2

    move v3, v2

    :goto_2
    if-ge v4, v5, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->L:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063602
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v6, v7, :cond_4

    .line 2063603
    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move v0, v1

    .line 2063604
    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_2

    :cond_1
    move v0, v2

    .line 2063605
    goto :goto_1

    .line 2063606
    :cond_2
    if-nez v3, :cond_3

    .line 2063607
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->v:Ljava/lang/String;

    const-string v3, "The \'Only Me\' option is missing in the privacy option list!"

    invoke-virtual {v0, v1, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2063608
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->L:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063609
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_3
.end method

.method private x()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2063610
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-nez v1, :cond_1

    .line 2063611
    :cond_0
    :goto_0
    return v0

    .line 2063612
    :cond_1
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v1

    .line 2063613
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FACEBOOK:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2063614
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/os/Bundle;)V

    .line 2063615
    invoke-static {p0, p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2063616
    return-void
.end method

.method public final a()Z
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 2063617
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->s()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->t()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->u()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2063618
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2063619
    const v0, 0x7f030445

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->setContentView(I)V

    .line 2063620
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2063621
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2063622
    const v2, 0x7f0811e5

    invoke-virtual {p0, v2}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 2063623
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v3, 0x7f0811e6

    invoke-virtual {p0, v3}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2063624
    iput-object v3, v2, LX/108;->g:Ljava/lang/String;

    .line 2063625
    move-object v2, v2

    .line 2063626
    const/4 v3, -0x2

    .line 2063627
    iput v3, v2, LX/108;->h:I

    .line 2063628
    move-object v2, v2

    .line 2063629
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 2063630
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 2063631
    new-instance v2, LX/Dxq;

    invoke-direct {v2, p0}, LX/Dxq;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V

    invoke-interface {v0, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 2063632
    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 2063633
    new-instance v2, LX/Dxr;

    invoke-direct {v2, p0}, LX/Dxr;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V

    invoke-interface {v0, v2}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 2063634
    :cond_0
    const v0, 0x7f0d0510

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->w:Landroid/widget/EditText;

    .line 2063635
    const v0, 0x7f0d0511

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->x:Landroid/widget/EditText;

    .line 2063636
    const v0, 0x7f0d0cf3

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->A:Landroid/widget/TextView;

    .line 2063637
    const v0, 0x7f0d0cfb

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    .line 2063638
    const v0, 0x7f0d0cf6

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->z:Landroid/widget/TextView;

    .line 2063639
    const v0, 0x7f0d0cfd

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->C:Landroid/view/View;

    .line 2063640
    const v0, 0x7f0d0cfe

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->D:Landroid/view/View;

    .line 2063641
    const v0, 0x7f0d0cff

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->B:Landroid/widget/TextView;

    .line 2063642
    const v0, 0x7f0d0cf2

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->E:Landroid/view/View;

    .line 2063643
    const v0, 0x7f0d0cf4

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->I:Landroid/view/View;

    .line 2063644
    const v0, 0x7f0d0cf5

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->F:Landroid/view/View;

    .line 2063645
    const v0, 0x7f0d0cf7

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->J:Landroid/view/View;

    .line 2063646
    const v0, 0x7f0d0cfc

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->G:Landroid/view/View;

    .line 2063647
    const v0, 0x7f0d0cf9

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->H:Landroid/view/View;

    .line 2063648
    const v0, 0x7f0d0cf1

    invoke-virtual {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbScrollView;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->K:Lcom/facebook/widget/FbScrollView;

    .line 2063649
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "album"

    invoke-static {v0, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2063650
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063651
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063652
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063653
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063654
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063655
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063656
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063657
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2063658
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x25d6af

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->X:Z

    .line 2063659
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->b()V

    .line 2063660
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)V

    .line 2063661
    invoke-direct {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->l()V

    .line 2063662
    return-void

    :cond_1
    move v0, v1

    .line 2063663
    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2063664
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2063665
    if-ne p1, v1, :cond_2

    .line 2063666
    if-ne p2, v10, :cond_2

    .line 2063667
    const-string v0, "privacy_option"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063668
    if-nez v0, :cond_1

    .line 2063669
    :cond_0
    :goto_0
    return-void

    .line 2063670
    :cond_1
    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->N:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2063671
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->y:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->b(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;Z)V

    .line 2063672
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_d

    if-ne p2, v10, :cond_d

    .line 2063673
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->R:Ljava/util/Set;

    if-nez v0, :cond_3

    .line 2063674
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->R:Ljava/util/Set;

    .line 2063675
    :goto_1
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->S:Ljava/util/Set;

    if-nez v0, :cond_4

    .line 2063676
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->S:Ljava/util/Set;

    .line 2063677
    :goto_2
    const-string v0, "full_profiles"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 2063678
    iput-object v4, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->U:Ljava/util/List;

    .line 2063679
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->T:Ljava/util/Set;

    if-nez v0, :cond_5

    .line 2063680
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->T:Ljava/util/Set;

    .line 2063681
    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    :goto_4
    if-ge v3, v5, :cond_6

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2063682
    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->T:Ljava/util/Set;

    iget-wide v8, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2063683
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 2063684
    :cond_3
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->R:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_1

    .line 2063685
    :cond_4
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->S:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_2

    .line 2063686
    :cond_5
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->T:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_3

    .line 2063687
    :cond_6
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 2063688
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2063689
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v3, v2

    :goto_5
    if-ge v3, v8, :cond_7

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2063690
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2063691
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 2063692
    :cond_7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v2

    :goto_6
    if-ge v3, v7, :cond_8

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 2063693
    invoke-static {v0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a(Lcom/facebook/ipc/model/FacebookProfile;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2063694
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 2063695
    :cond_8
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2063696
    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 2063697
    iget-object v7, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->R:Ljava/util/Set;

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2063698
    :cond_a
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2063699
    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->Q:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 2063700
    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->S:Ljava/util/Set;

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 2063701
    :cond_c
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v1

    :goto_9
    invoke-static {p0, v0, v1}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a$redex0(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;ZZ)V

    .line 2063702
    :cond_d
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    if-ne p2, v10, :cond_0

    .line 2063703
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2063704
    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->V:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2063705
    if-eqz v0, :cond_f

    .line 2063706
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->M:Ljava/lang/String;

    .line 2063707
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->A:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2063708
    iput-boolean v2, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->W:Z

    goto/16 :goto_0

    :cond_e
    move v0, v2

    .line 2063709
    goto :goto_9

    .line 2063710
    :cond_f
    iput-object v11, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->M:Ljava/lang/String;

    .line 2063711
    iput-object v11, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->V:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2063712
    iput-boolean v1, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->W:Z

    .line 2063713
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->A:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public final onBackPressed()V
    .locals 6

    .prologue
    .line 2063714
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2063715
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2063716
    :goto_0
    return-void

    .line 2063717
    :cond_0
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2063718
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0811e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->setTitle(Ljava/lang/CharSequence;)V

    .line 2063719
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0811f5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2063720
    new-instance v1, LX/Dxs;

    invoke-direct {v1, p0}, LX/Dxs;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;)V

    .line 2063721
    new-instance v2, LX/Dxt;

    invoke-direct {v2, p0, v0}, LX/Dxt;-><init>(Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;LX/2EJ;)V

    .line 2063722
    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0811f6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4, v1}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2063723
    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/permalink/edit/EditAlbumPermalinkActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0811e1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2063724
    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method
