.class public Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private A:LX/9bW;

.field private B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9bW;",
            ">;"
        }
    .end annotation
.end field

.field private C:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field private D:LX/3iH;

.field public E:LX/0SI;

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private G:LX/Dxg;

.field private H:Ljava/util/concurrent/Executor;

.field private final a:LX/DyK;

.field private final b:LX/DyL;

.field public c:Lcom/facebook/widget/listview/BetterListView;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field public f:LX/9bF;

.field public g:Ljava/lang/Long;

.field public h:Z

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field private l:Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

.field private m:I

.field public n:Z

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:Z

.field public r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

.field public s:Ljava/lang/String;

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9bq;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private w:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxa;",
            ">;"
        }
    .end annotation
.end field

.field public z:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2064191
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2064192
    new-instance v0, LX/DyK;

    invoke-direct {v0, p0}, LX/DyK;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a:LX/DyK;

    .line 2064193
    new-instance v0, LX/DyL;

    invoke-direct {v0, p0}, LX/DyL;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->b:LX/DyL;

    .line 2064194
    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    .line 2064195
    iput-boolean v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->h:Z

    .line 2064196
    iput-boolean v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->j:Z

    .line 2064197
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->m:I

    .line 2064198
    iput-boolean v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->n:Z

    .line 2064199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->o:Ljava/lang/String;

    .line 2064200
    iput-boolean v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->p:Z

    .line 2064201
    iput-boolean v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->q:Z

    .line 2064202
    return-void
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Z)Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;
    .locals 4

    .prologue
    .line 2064352
    new-instance v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    invoke-direct {v0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;-><init>()V

    .line 2064353
    if-nez p0, :cond_0

    .line 2064354
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2064355
    :cond_0
    const-string v1, "owner_id"

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2064356
    const-string v1, "is_page"

    invoke-virtual {p0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2064357
    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2064358
    return-object v0
.end method

.method private a(IILX/0zS;)V
    .locals 7

    .prologue
    .line 2064345
    new-instance v0, LX/5hS;

    invoke-direct {v0}, LX/5hS;-><init>()V

    move-object v0, v0

    .line 2064346
    const-string v1, "node_id"

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "image_width"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "image_height"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "count"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "media_type"

    sget-object v2, LX/0wF;->IMAGEWEBP:LX/0wF;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2064347
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2064348
    new-instance v3, LX/DyJ;

    invoke-direct {v3, p0}, LX/DyJ;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    .line 2064349
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v4, "fetchVideosUploadedQuery_%s-%s"

    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    aput-object v6, v5, v1

    const/4 v6, 0x1

    sget-object v1, LX/0zS;->b:LX/0zS;

    if-ne p3, v1, :cond_0

    const-string v1, "cache"

    :goto_0
    aput-object v1, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, LX/Dy8;

    invoke-direct {v4, p0, v2}, LX/Dy8;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v0, v1, v4, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2064350
    return-void

    .line 2064351
    :cond_0
    const-string v1, "network"

    goto :goto_0
.end method

.method private static a(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;LX/0Ot;LX/0Ot;LX/9bF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/auth/viewercontext/ViewerContext;LX/Dxg;Ljava/util/concurrent/Executor;LX/3iH;LX/0SI;)V
    .locals 2
    .param p12    # LX/Dxg;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9bq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/9bF;",
            "LX/0Ot",
            "<",
            "LX/9bY;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/intent/PhotosViewIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9bW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Dxa;",
            ">;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/Dxg;",
            "Ljava/util/concurrent/Executor;",
            "LX/3iH;",
            "LX/0SI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2064328
    invoke-virtual {p11}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->s:Ljava/lang/String;

    .line 2064329
    iput-object p1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->t:LX/0Ot;

    .line 2064330
    iput-object p5, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->v:LX/0Ot;

    .line 2064331
    iput-object p2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    .line 2064332
    iput-object p4, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->w:LX/0Ot;

    .line 2064333
    iput-object p3, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    .line 2064334
    iput-object p6, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->x:LX/0Ot;

    .line 2064335
    iput-object p7, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->F:LX/0Ot;

    .line 2064336
    iput-object p10, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->y:LX/0Ot;

    .line 2064337
    iput-object p11, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->z:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2064338
    iput-object p12, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->G:LX/Dxg;

    .line 2064339
    iput-object p13, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->H:Ljava/util/concurrent/Executor;

    .line 2064340
    iput-object p9, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->C:LX/0Ot;

    .line 2064341
    iput-object p8, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->B:LX/0Ot;

    .line 2064342
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->D:LX/3iH;

    .line 2064343
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->E:LX/0SI;

    .line 2064344
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 16

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;

    const/16 v1, 0x2e03

    invoke-static {v15, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x12b1

    invoke-static {v15, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v15}, LX/9bF;->a(LX/0QB;)LX/9bF;

    move-result-object v3

    check-cast v3, LX/9bF;

    const/16 v4, 0x2e01

    invoke-static {v15, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x455

    invoke-static {v15, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2e65

    invoke-static {v15, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x259

    invoke-static {v15, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2e00

    invoke-static {v15, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xafd

    invoke-static {v15, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2eaa

    invoke-static {v15, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {v15}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v11

    check-cast v11, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v15}, LX/Dxg;->b(LX/0QB;)LX/Dxg;

    move-result-object v12

    check-cast v12, LX/Dxg;

    invoke-static {v15}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, Ljava/util/concurrent/Executor;

    invoke-static {v15}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v14

    check-cast v14, LX/3iH;

    invoke-static {v15}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v15

    check-cast v15, LX/0SI;

    invoke-static/range {v0 .. v15}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;LX/0Ot;LX/0Ot;LX/9bF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/auth/viewercontext/ViewerContext;LX/Dxg;Ljava/util/concurrent/Executor;LX/3iH;LX/0SI;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;Lcom/facebook/graphql/model/GraphQLAlbumsConnection;Z)V
    .locals 2

    .prologue
    .line 2064322
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLAlbumsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2064323
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    .line 2064324
    iget-object v1, v0, LX/9bF;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2064325
    invoke-virtual {v0, p1, p2}, LX/9bF;->b(Lcom/facebook/graphql/model/GraphQLAlbumsConnection;Z)V

    .line 2064326
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->d()V

    .line 2064327
    return-void
.end method

.method public static e(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)Z
    .locals 2

    .prologue
    .line 2064317
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->i:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 2064318
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->s:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2064319
    :goto_0
    return v0

    .line 2064320
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2064321
    :cond_1
    new-instance v0, LX/8A4;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->i:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object v1, LX/8A3;->CREATE_CONTENT:LX/8A3;

    invoke-virtual {v0, v1}, LX/8A4;->a(LX/8A3;)Z

    move-result v0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V
    .locals 4

    .prologue
    .line 2064312
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->i:Ljava/util/ArrayList;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/8A4;->a(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2064313
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->D:LX/3iH;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3iH;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2064314
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pandora_fetch_viewer_context"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/DyA;

    invoke-direct {v3, p0}, LX/DyA;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2064315
    :goto_0
    return-void

    .line 2064316
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c()V

    goto :goto_0
.end method

.method public static n(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)Z
    .locals 2

    .prologue
    .line 2064307
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    invoke-virtual {v0}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2064308
    iget-boolean v1, v0, LX/Dxa;->a:Z

    move v0, v1

    .line 2064309
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    .line 2064310
    iget-boolean v1, v0, LX/Dxa;->b:Z

    move v0, v1

    .line 2064311
    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2064302
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2064303
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2064304
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bW;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->A:LX/9bW;

    .line 2064305
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxa;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Dxa;->a(Landroid/content/Intent;)V

    .line 2064306
    return-void
.end method

.method public final c()V
    .locals 15

    .prologue
    const/16 v5, 0x14

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v2, 0xfa

    .line 2064273
    iput-boolean v7, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->n:Z

    .line 2064274
    iput-object v4, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->o:Ljava/lang/String;

    .line 2064275
    iput-boolean v7, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->p:Z

    .line 2064276
    iput-boolean v7, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->q:Z

    .line 2064277
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bq;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/0zS;->b:LX/0zS;

    invoke-virtual/range {v0 .. v5}, LX/9bq;->a(Ljava/lang/String;ILX/0zS;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 2064278
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bq;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/0zS;->d:LX/0zS;

    invoke-virtual/range {v0 .. v5}, LX/9bq;->a(Ljava/lang/String;ILX/0zS;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2064279
    new-instance v3, LX/DyC;

    invoke-direct {v3, p0}, LX/DyC;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    .line 2064280
    new-instance v4, LX/DyD;

    invoke-direct {v4, p0}, LX/DyD;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    .line 2064281
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v5, "fetchInitialAlbumsListCache_%s"

    new-array v9, v6, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    aput-object v10, v9, v7

    invoke-static {v5, v9}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v9, LX/DyE;

    invoke-direct {v9, p0, v8}, LX/DyE;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v0, v5, v9, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2064282
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v3, "fetchInitialAlbumsListNetwork_%s"

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    aput-object v8, v5, v7

    invoke-static {v3, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, LX/DyF;

    invoke-direct {v5, p0, v1}, LX/DyF;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-virtual {v0, v3, v5, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2064283
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->A:LX/9bW;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->A:LX/9bW;

    iget-boolean v0, v0, LX/9bW;->a:Z

    if-eqz v0, :cond_2

    invoke-static {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->n(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v6

    .line 2064284
    :goto_0
    if-eqz v0, :cond_0

    .line 2064285
    sget-object v0, LX/0zS;->b:LX/0zS;

    invoke-direct {p0, v2, v2, v0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a(IILX/0zS;)V

    .line 2064286
    sget-object v0, LX/0zS;->d:LX/0zS;

    invoke-direct {p0, v2, v2, v0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a(IILX/0zS;)V

    .line 2064287
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->G:LX/Dxg;

    .line 2064288
    iget-object v1, v0, LX/Dxg;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 2064289
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->j:Z

    if-nez v0, :cond_1

    .line 2064290
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->G:LX/Dxg;

    sget-object v1, LX/0zS;->d:LX/0zS;

    iget-object v3, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    .line 2064291
    new-instance v11, LX/5g4;

    invoke-direct {v11}, LX/5g4;-><init>()V

    move-object v11, v11

    .line 2064292
    const-string v12, "targetId"

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v12

    const-string v13, "image_width"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v12

    const-string v13, "image_height"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2064293
    iget-object v12, v0, LX/Dxg;->a:LX/0se;

    invoke-virtual {v12, v11}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 2064294
    invoke-static {v11}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v11

    invoke-virtual {v11, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v12

    .line 2064295
    iget-object v11, v0, LX/Dxg;->e:LX/0Ot;

    invoke-interface {v11}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-virtual {v11, v12}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v11

    .line 2064296
    new-instance v12, LX/Dxe;

    invoke-direct {v12, v0, v3}, LX/Dxe;-><init>(LX/Dxg;Ljava/lang/Long;)V

    invoke-static {v11, v12}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v11

    .line 2064297
    new-instance v12, LX/Dxf;

    invoke-direct {v12, v0}, LX/Dxf;-><init>(LX/Dxg;)V

    invoke-static {v11, v12}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2064298
    move-object v0, v11

    .line 2064299
    new-instance v1, LX/DyG;

    invoke-direct {v1, p0}, LX/DyG;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->H:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2064300
    :cond_1
    return-void

    :cond_2
    move v0, v7

    .line 2064301
    goto/16 :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2064266
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    invoke-virtual {v0}, LX/9bF;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 2064267
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2064268
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2064269
    :cond_0
    :goto_0
    return-void

    .line 2064270
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2064271
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->e:Landroid/view/View;

    const v1, 0x7f0d0d65

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2064272
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->e:Landroid/view/View;

    const v1, 0x7f0d0d66

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x2da53487

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2064242
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2064243
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2064244
    if-eqz v1, :cond_0

    .line 2064245
    const-string v0, "extra_photo_tab_mode_params"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->r:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2064246
    const-string v0, "owner_id"

    invoke-virtual {v1, v0, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    .line 2064247
    const-string v0, "is_page"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->h:Z

    .line 2064248
    const-string v0, "disable_adding_photos_to_albums"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->k:Z

    .line 2064249
    const-string v0, "customized_res"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->l:Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

    .line 2064250
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->l:Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

    if-eqz v0, :cond_0

    .line 2064251
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->l:Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

    iget v1, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2064252
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->l:Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

    iget v1, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;->a:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setBackgroundResource(I)V

    .line 2064253
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->l:Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

    iget v1, v1, Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2064254
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->s:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2064255
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->s:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    .line 2064256
    :cond_1
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    if-eqz v0, :cond_2

    .line 2064257
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->h:Z

    const/4 v3, 0x1

    invoke-static {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->e(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)Z

    move-result v4

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->l:Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->l:Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;

    iget v5, v5, Lcom/facebook/photos/photoset/ui/photoset/PandoraCustomizedBackgroundConfig;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 2064258
    :goto_0
    iput-object v1, v0, LX/9bF;->d:Ljava/lang/String;

    .line 2064259
    iput-boolean v2, v0, LX/9bF;->g:Z

    .line 2064260
    iput-boolean v3, v0, LX/9bF;->h:Z

    .line 2064261
    iput-boolean v4, v0, LX/9bF;->e:Z

    .line 2064262
    iput-object v5, v0, LX/9bF;->i:Ljava/lang/Integer;

    .line 2064263
    const/4 v7, 0x0

    iput-boolean v7, v0, LX/9bF;->j:Z

    .line 2064264
    :cond_2
    const v0, -0x2a1e3d47

    invoke-static {v0, v6}, LX/02F;->f(II)V

    return-void

    .line 2064265
    :cond_3
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x7cd7c8b7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2064238
    const v0, 0x7f0300d3

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->e:Landroid/view/View;

    .line 2064239
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->e:Landroid/view/View;

    const v2, 0x102000a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    .line 2064240
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->e:Landroid/view/View;

    const v2, 0x7f0d0521

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->d:Landroid/view/View;

    .line 2064241
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->e:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x26dac2d6

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1be95463

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2064234
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2064235
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2064236
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2064237
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x8e85bc6

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x27bab8fd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2064230
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a:LX/DyK;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2064231
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->b:LX/DyL;

    invoke-virtual {v0, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2064232
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2064233
    const/16 v0, 0x2b

    const v2, 0xedeac40

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x4ca8108f    # 8.8114296E7f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2064212
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2064213
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->a:LX/DyK;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2064214
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9bY;

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->b:LX/DyL;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2064215
    iget-boolean v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->h:Z

    if-eqz v0, :cond_1

    .line 2064216
    iget-boolean v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->j:Z

    if-eqz v0, :cond_0

    .line 2064217
    invoke-static {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->m(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    .line 2064218
    :goto_0
    const v0, 0x220f7403

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 2064219
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v2, "fetchAlbumsPermissions_%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->t:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/9bq;

    iget-object v4, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->g:Ljava/lang/Long;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/0zS;->c:LX/0zS;

    .line 2064220
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v6, 0x1

    :goto_1
    invoke-static {v6}, LX/0PB;->checkState(Z)V

    .line 2064221
    new-instance v7, LX/5g6;

    invoke-direct {v7}, LX/5g6;-><init>()V

    .line 2064222
    const-string v6, "page_id"

    invoke-virtual {v7, v6, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v8, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v8

    const-string v9, "profile_pic_media_type"

    iget-object v6, v2, LX/9bq;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0rq;

    invoke-virtual {v6}, LX/0rq;->a()LX/0wF;

    move-result-object v6

    invoke-virtual {v8, v9, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2064223
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    .line 2064224
    iget-object v6, v2, LX/9bq;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-virtual {v6, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 2064225
    move-object v2, v6

    .line 2064226
    new-instance v4, LX/Dy9;

    invoke-direct {v4, p0}, LX/Dy9;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    invoke-virtual {v0, v3, v2, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2064227
    goto :goto_0

    .line 2064228
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c()V

    goto :goto_0

    .line 2064229
    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x261b1cdc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2064207
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2064208
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2064209
    if-eqz v0, :cond_0

    .line 2064210
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 2064211
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x7f19fc85

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2064203
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2064204
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->f:LX/9bF;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2064205
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;->c:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/DyB;

    invoke-direct {v1, p0}, LX/DyB;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PandoraAlbumsFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2064206
    return-void
.end method
