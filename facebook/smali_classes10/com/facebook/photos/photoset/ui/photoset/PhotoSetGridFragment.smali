.class public Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9fx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DxW;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Dxa;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;

.field public i:Lcom/facebook/widget/gridview/BetterGridView;

.field private j:Ljava/lang/String;

.field private k:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public l:LX/9av;

.field public m:Ljava/lang/String;

.field public n:Lcom/facebook/common/callercontext/CallerContext;

.field public o:LX/5hJ;

.field public p:I

.field private q:LX/74S;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2064476
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2064477
    new-instance v0, LX/9av;

    invoke-direct {v0}, LX/9av;-><init>()V

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    .line 2064478
    sget-object v0, LX/5hJ;->TAGGED_MEDIASET:LX/5hJ;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->o:LX/5hJ;

    .line 2064479
    return-void
.end method

.method public static a$redex0(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;J)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2064480
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_show_attribution"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 2064481
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    .line 2064482
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    invoke-virtual {v5}, LX/9av;->a()LX/0Px;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->q:LX/74S;

    invoke-virtual/range {v0 .. v7}, LX/Dxd;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLAlbum;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V

    .line 2064483
    :goto_0
    return-void

    .line 2064484
    :cond_0
    sget-object v0, LX/DyP;->a:[I

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->o:LX/5hJ;

    invoke-virtual {v1}, LX/5hJ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2064485
    const-string v2, "unknown photoset: %s, objectId: %s, setToken: %s"

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->o:LX/5hJ;

    invoke-virtual {v0}, LX/5hJ;->name()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->j:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->j:Ljava/lang/String;

    :goto_2
    invoke-static {v2, v3, v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_0

    .line 2064486
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->j:Ljava/lang/String;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    invoke-virtual {v5}, LX/9av;->a()LX/0Px;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->q:LX/74S;

    invoke-virtual/range {v0 .. v7}, LX/Dxd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V

    goto :goto_0

    .line 2064487
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    invoke-virtual {v0}, LX/9av;->a()LX/0Px;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->q:LX/74S;

    .line 2064488
    iget-object v0, v1, LX/Dxd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2064489
    new-instance v0, LX/9hE;

    const-class v8, LX/23a;

    new-instance p0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {p0, v3}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-static {v8, p0}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v8

    invoke-direct {v0, v8}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    move-object v0, v0

    .line 2064490
    invoke-virtual {v0, v5}, LX/9hE;->b(LX/0Px;)LX/9hE;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    .line 2064491
    iput-boolean v7, v0, LX/9hD;->m:Z

    .line 2064492
    move-object v0, v0

    .line 2064493
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v8

    .line 2064494
    iget-object v0, v1, LX/Dxd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    const/4 p0, 0x0

    invoke-interface {v0, v2, v8, p0}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2064495
    goto/16 :goto_0

    .line 2064496
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    invoke-virtual {v0}, LX/9av;->a()LX/0Px;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->q:LX/74S;

    .line 2064497
    iget-object v0, v1, LX/Dxd;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2064498
    new-instance v0, LX/9hE;

    const-class v8, LX/23b;

    new-instance p0, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-direct {p0, v3}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-static {v8, p0}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v8

    invoke-direct {v0, v8}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    move-object v0, v0

    .line 2064499
    invoke-virtual {v0, v5}, LX/9hE;->b(LX/0Px;)LX/9hE;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v0

    .line 2064500
    iput-boolean v7, v0, LX/9hD;->m:Z

    .line 2064501
    move-object v0, v0

    .line 2064502
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v8

    .line 2064503
    iget-object v0, v1, LX/Dxd;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    const/4 p0, 0x0

    invoke-interface {v0, v2, v8, p0}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2064504
    goto/16 :goto_0

    .line 2064505
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Dxd;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    invoke-virtual {v5}, LX/9av;->a()LX/0Px;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->q:LX/74S;

    invoke-virtual/range {v0 .. v7}, LX/Dxd;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;LX/0Px;LX/74S;Z)V

    goto/16 :goto_0

    .line 2064506
    :cond_1
    const-string v0, "NULL"

    goto/16 :goto_1

    :cond_2
    const-string v1, "NULL"

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static c(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V
    .locals 4

    .prologue
    .line 2064507
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    .line 2064508
    iget-boolean v1, v0, LX/9av;->b:Z

    move v0, v1

    .line 2064509
    if-nez v0, :cond_0

    .line 2064510
    :goto_0
    return-void

    .line 2064511
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    const-string v1, "task-fetchMediaset"

    .line 2064512
    new-instance v2, LX/DyN;

    invoke-direct {v2, p0}, LX/DyN;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V

    move-object v2, v2

    .line 2064513
    new-instance v3, LX/DyO;

    invoke-direct {v3, p0}, LX/DyO;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method

.method public static d(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2064514
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v0

    .line 2064515
    if-nez v1, :cond_0

    .line 2064516
    :goto_0
    return-void

    .line 2064517
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/gridview/BetterGridView;->setVisibility(I)V

    .line 2064518
    const v0, 0x7f0d0d65

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0811fe

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 2064519
    const v0, 0x1020004

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2064520
    const v0, 0x7f0d0d65

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2064521
    const v0, 0x7f0d0d66

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 2064522
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2064523
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    const/16 v3, 0x12b1

    invoke-static {p1, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2e5e

    invoke-static {p1, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2ea9

    invoke-static {p1, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2ead

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2eaa

    invoke-static {p1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p1}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const/16 v0, 0x2dff

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->b:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->c:LX/0Ot;

    iput-object v6, v2, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->e:LX/0Ot;

    iput-object v8, v2, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->f:Ljava/lang/String;

    iput-object p1, v2, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->g:LX/0Ot;

    .line 2064524
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/16 v9, 0x8

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x34e88b3d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2064525
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2064526
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2064527
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/gridview/BetterGridView;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    .line 2064528
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2064529
    if-nez v0, :cond_a

    .line 2064530
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v1, v0

    .line 2064531
    :goto_0
    new-instance v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;

    iget-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    invoke-direct {v0, v2}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;-><init>(LX/9av;)V

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->h:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;

    .line 2064532
    const-string v0, "fullscreen_gallery_source"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "fullscreen_gallery_source"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/74S;->valueOf(Ljava/lang/String;)LX/74S;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->q:LX/74S;

    .line 2064533
    const-string v0, "extra_caller_context"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    .line 2064534
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v0, :cond_0

    .line 2064535
    const-class v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;

    const-string v2, "unknown"

    invoke-static {v0, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->n:Lcom/facebook/common/callercontext/CallerContext;

    .line 2064536
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2064537
    const-string v2, "owner_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2064538
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2064539
    iput-object v2, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    .line 2064540
    :cond_1
    invoke-static {}, LX/5hJ;->values()[LX/5hJ;

    move-result-object v2

    const-string v3, "mediaset_type"

    sget-object v4, LX/5hJ;->TAGGED_MEDIASET:LX/5hJ;

    invoke-virtual {v4}, LX/5hJ;->ordinal()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    aget-object v0, v2, v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->o:LX/5hJ;

    .line 2064541
    const-string v0, "extra_photo_tab_mode_params"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2064542
    if-eqz v2, :cond_2

    .line 2064543
    iget-wide v10, v2, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->b:J

    move-wide v4, v10

    .line 2064544
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    .line 2064545
    :cond_2
    const-string v0, "extra_album_selected"

    invoke-static {v1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2064546
    const-string v0, "set_token"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->j:Ljava/lang/String;

    .line 2064547
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2064548
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAlbum;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    .line 2064549
    sget-object v0, LX/5hJ;->ALBUM_MEDIASET:LX/5hJ;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->o:LX/5hJ;

    .line 2064550
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2064551
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    .line 2064552
    :cond_4
    const-string v0, "mediaset_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "owner_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2064553
    invoke-static {}, LX/5hJ;->values()[LX/5hJ;

    move-result-object v0

    const-string v3, "mediaset_type"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->o:LX/5hJ;

    .line 2064554
    const-string v0, "owner_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    .line 2064555
    :cond_5
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    invoke-virtual {v0, v9}, Lcom/facebook/widget/gridview/BetterGridView;->setVisibility(I)V

    .line 2064556
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DxW;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->k:Lcom/facebook/graphql/model/GraphQLAlbum;

    const v3, 0x7f0d0513

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f0d00bc

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, LX/0h5;

    iget-object v5, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->f:Ljava/lang/String;

    const/16 v11, 0x8

    .line 2064557
    iput-object v1, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 2064558
    iput-object v2, v0, LX/DxW;->g:Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    .line 2064559
    iput-object v6, v0, LX/DxW;->h:Ljava/lang/String;

    .line 2064560
    invoke-virtual {v3, v11}, Landroid/view/View;->setVisibility(I)V

    .line 2064561
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v8

    if-eqz v8, :cond_b

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->B()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v8

    iget-object v10, v0, LX/DxW;->h:Ljava/lang/String;

    invoke-static {v8, v10}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_b

    const/4 v10, 0x0

    .line 2064562
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_10

    :cond_6
    move v8, v10

    .line 2064563
    :goto_3
    move v8, v8

    .line 2064564
    if-nez v8, :cond_b

    .line 2064565
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->h:Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/gridview/BetterGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2064566
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    new-instance v1, LX/DyR;

    invoke-direct {v1, p0}, LX/DyR;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/gridview/BetterGridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2064567
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/gridview/BetterGridView;->setClickable(Z)V

    .line 2064568
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    new-instance v1, LX/DyQ;

    invoke-direct {v1, p0}, LX/DyQ;-><init>(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/gridview/BetterGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2064569
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    invoke-virtual {v0, v9}, Lcom/facebook/widget/gridview/BetterGridView;->setVisibility(I)V

    .line 2064570
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0b3a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->p:I

    .line 2064571
    const v0, -0xdaadf5a

    invoke-static {v0, v7}, LX/02F;->f(II)V

    return-void

    .line 2064572
    :cond_8
    sget-object v0, LX/74S;->TIMELINE_PHOTOS_OF_USER:LX/74S;

    goto/16 :goto_1

    .line 2064573
    :cond_9
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->j:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 2064574
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->m:Ljava/lang/String;

    .line 2064575
    sget-object v0, LX/5hJ;->MEDIASET:LX/5hJ;

    iput-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->o:LX/5hJ;

    goto/16 :goto_2

    :cond_a
    move-object v1, v0

    goto/16 :goto_0

    .line 2064576
    :cond_b
    if-nez v1, :cond_c

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_c

    iget-object v8, v0, LX/DxW;->h:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2064577
    :cond_c
    new-instance v8, LX/DxR;

    invoke-direct {v8, v0}, LX/DxR;-><init>(LX/DxW;)V

    invoke-virtual {v3, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2064578
    const/4 v8, 0x0

    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2064579
    const v8, 0x7f0d0514

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 2064580
    invoke-virtual {v0}, LX/DxW;->b()Z

    move-result v10

    if-eqz v10, :cond_d

    .line 2064581
    const v10, 0x7f0811d8

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 2064582
    iget-object v10, v0, LX/DxW;->i:Landroid/content/res/Resources;

    const v11, 0x7f0811d8

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2064583
    if-eqz v4, :cond_7

    .line 2064584
    const v8, 0x7f081201

    invoke-interface {v4, v8}, LX/0h5;->setTitle(I)V

    goto/16 :goto_4

    .line 2064585
    :cond_d
    invoke-virtual {v0}, LX/DxW;->a()Z

    move-result v10

    if-eqz v10, :cond_e

    .line 2064586
    const v10, 0x7f0811d9

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 2064587
    iget-object v10, v0, LX/DxW;->i:Landroid/content/res/Resources;

    const v11, 0x7f0811d9

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2064588
    if-eqz v4, :cond_7

    .line 2064589
    const v8, 0x7f0811ff

    invoke-interface {v4, v8}, LX/0h5;->setTitle(I)V

    goto/16 :goto_4

    .line 2064590
    :cond_e
    const/4 p1, 0x0

    .line 2064591
    iget-object v10, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v10, :cond_13

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iget-object v12, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v12

    invoke-static {v10, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_13

    .line 2064592
    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2064593
    const v10, 0x7f0811d8

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 2064594
    new-instance v10, LX/DxS;

    invoke-direct {v10, v0}, LX/DxS;-><init>(LX/DxW;)V

    invoke-virtual {v3, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2064595
    :goto_5
    iget-object v10, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-nez v10, :cond_15

    .line 2064596
    const/4 v10, 0x0

    .line 2064597
    :goto_6
    move v10, v10

    .line 2064598
    if-nez v10, :cond_f

    iget-object v10, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v10, :cond_f

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->COVER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iget-object v12, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v12

    invoke-static {v10, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->PROFILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iget-object v12, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v12

    invoke-static {v10, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->MOBILE:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iget-object v12, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v12

    invoke-static {v10, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 2064599
    const/16 v10, 0x8

    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2064600
    :cond_f
    invoke-virtual {v0}, LX/DxW;->b()Z

    move-result v8

    if-nez v8, :cond_7

    invoke-virtual {v0}, LX/DxW;->a()Z

    move-result v8

    if-nez v8, :cond_7

    .line 2064601
    invoke-virtual {v3, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    .line 2064602
    :cond_10
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLAlbum;->q()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    move v12, v10

    :goto_7
    if-ge v12, v2, :cond_12

    invoke-virtual {p1, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/model/GraphQLActor;

    .line 2064603
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 2064604
    const/4 v8, 0x1

    goto/16 :goto_3

    .line 2064605
    :cond_11
    add-int/lit8 v8, v12, 0x1

    move v12, v8

    goto :goto_7

    :cond_12
    move v8, v10

    .line 2064606
    goto/16 :goto_3

    .line 2064607
    :cond_13
    iget-object v10, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v10, :cond_14

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;->COVER:Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    iget-object v12, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLAlbum;->k()Lcom/facebook/graphql/enums/GraphQLPhotosAlbumAPIType;

    move-result-object v12

    invoke-static {v10, v12}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_14

    .line 2064608
    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2064609
    const v10, 0x7f0811d9

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 2064610
    new-instance v10, LX/DxT;

    invoke-direct {v10, v0}, LX/DxT;-><init>(LX/DxW;)V

    invoke-virtual {v3, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_5

    .line 2064611
    :cond_14
    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2064612
    const v10, 0x7f0811d5

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setText(I)V

    .line 2064613
    new-instance v10, LX/DxU;

    invoke-direct {v10, v0}, LX/DxU;-><init>(LX/DxW;)V

    invoke-virtual {v3, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_5

    :cond_15
    iget-object v10, v0, LX/DxW;->f:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLAlbum;->o()Z

    move-result v10

    goto/16 :goto_6
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x7602893d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2064614
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030f4f

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x2b

    const v3, 0x3f43ef34    # 0.7653687f

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x34246324    # -2.878508E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2064615
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2064616
    iget-object v1, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->l:LX/9av;

    const/4 p0, 0x0

    .line 2064617
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/9av;->b:Z

    .line 2064618
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/9av;->c:Z

    .line 2064619
    iput-object p0, v1, LX/9av;->d:Ljava/lang/String;

    .line 2064620
    iput-object p0, v1, LX/9av;->e:Ljava/lang/String;

    .line 2064621
    iput-object p0, v1, LX/9av;->a:LX/0Px;

    .line 2064622
    const/16 v1, 0x2b

    const v2, -0x5ea5b404

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6311da64

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2064623
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/gridview/BetterGridView;->setEnabled(Z)V

    .line 2064624
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->a:LX/0Ot;

    if-eqz v0, :cond_0

    .line 2064625
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2064626
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->g:LX/0Ot;

    if-eqz v0, :cond_1

    .line 2064627
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    invoke-virtual {v0}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a()V

    .line 2064628
    :cond_1
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2064629
    const/16 v0, 0x2b

    const v2, 0x4a2c74fe    # 2825535.5f

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x482118df

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2064630
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2064631
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2064632
    if-eqz v0, :cond_0

    .line 2064633
    const v2, 0x7f081274

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2064634
    :cond_0
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->i:Lcom/facebook/widget/gridview/BetterGridView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/widget/gridview/BetterGridView;->setEnabled(Z)V

    .line 2064635
    invoke-static {p0}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;->c(Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridFragment;)V

    .line 2064636
    const/16 v0, 0x2b

    const v2, 0x5185cf90

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
