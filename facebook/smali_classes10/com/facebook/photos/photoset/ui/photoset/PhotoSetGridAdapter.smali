.class public Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:LX/9av;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2064401
    const-class v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;

    const-string v1, "photos_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/9av;)V
    .locals 0

    .prologue
    .line 2064398
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2064399
    iput-object p1, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->b:LX/9av;

    .line 2064400
    return-void
.end method

.method private a(I)Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;
    .locals 1

    .prologue
    .line 2064393
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->b:LX/9av;

    .line 2064394
    iget-object p0, v0, LX/9av;->a:LX/0Px;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/9av;->a:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    if-ltz p1, :cond_0

    iget-object p0, v0, LX/9av;->a:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p0

    if-lt p1, p0, :cond_1

    .line 2064395
    :cond_0
    const/4 p0, 0x0

    .line 2064396
    :goto_0
    move-object v0, p0

    .line 2064397
    return-object v0

    :cond_1
    iget-object p0, v0, LX/9av;->a:LX/0Px;

    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;

    goto :goto_0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2064388
    iget-object v0, p0, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->b:LX/9av;

    .line 2064389
    iget-object p0, v0, LX/9av;->a:LX/0Px;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/9av;->a:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2064390
    :cond_0
    const/4 p0, 0x0

    .line 2064391
    :goto_0
    move v0, p0

    .line 2064392
    return v0

    :cond_1
    iget-object p0, v0, LX/9av;->a:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2064387
    invoke-direct {p0, p1}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->a(I)Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2064384
    invoke-direct {p0, p1}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->a(I)Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2064385
    const-wide/16 v0, -0x1

    .line 2064386
    :goto_0
    return-wide v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->a(I)Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2064376
    if-eqz p2, :cond_1

    .line 2064377
    check-cast p2, LX/Dul;

    .line 2064378
    :goto_0
    invoke-direct {p0, p1}, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->a(I)Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;

    move-result-object v0

    .line 2064379
    invoke-virtual {v0}, Lcom/facebook/photos/albums/protocols/MediasetQueryModels$DefaultThumbnailImageModel;->k()LX/1Fb;

    move-result-object v0

    sget-object v1, Lcom/facebook/photos/photoset/ui/photoset/PhotoSetGridAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2064380
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 2064381
    :cond_0
    :goto_1
    return-object p2

    .line 2064382
    :cond_1
    new-instance p2, LX/Dul;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, LX/Dul;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2064383
    :cond_2
    invoke-static {v0}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p2, p0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 2064375
    const/4 v0, 0x1

    return v0
.end method
