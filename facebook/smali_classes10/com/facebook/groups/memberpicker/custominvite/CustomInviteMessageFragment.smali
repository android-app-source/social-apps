.class public Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbEditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2005884
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1351ad3c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2005885
    new-instance v1, Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    .line 2005886
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08308c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 2005887
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    const/16 v2, 0x30

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setGravity(I)V

    .line 2005888
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setBackgroundResource(I)V

    .line 2005889
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 2005890
    const/high16 v2, 0x41a00000    # 20.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 2005891
    iget-object v2, p0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2, v1, v1, v1, v1}, Lcom/facebook/resources/ui/FbEditText;->setPadding(IIII)V

    .line 2005892
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    .line 2005893
    new-instance v2, LX/DVs;

    invoke-direct {v2, p0}, LX/DVs;-><init>(Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;)V

    move-object v2, v2

    .line 2005894
    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2005895
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2005896
    if-eqz v1, :cond_0

    .line 2005897
    const-string v2, "groups_custom_invite_message"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2005898
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2005899
    iget-object v2, p0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2005900
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    const/16 v2, 0x2b

    const v3, -0x1c0a10e3

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2005901
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2005902
    if-eqz v0, :cond_0

    .line 2005903
    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 2005904
    const v3, 0x7f08308a

    invoke-interface {v0, v3}, LX/1ZF;->x_(I)V

    .line 2005905
    iget-object v3, p0, Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2005906
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08308b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2005907
    iput-object v5, v4, LX/108;->g:Ljava/lang/String;

    .line 2005908
    move-object v4, v4

    .line 2005909
    const/4 v5, -0x2

    .line 2005910
    iput v5, v4, LX/108;->h:I

    .line 2005911
    move-object v4, v4

    .line 2005912
    new-array v5, v1, [Ljava/lang/CharSequence;

    aput-object v3, v5, v2

    invoke-static {v5}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2005913
    :goto_0
    iput-boolean v1, v4, LX/108;->d:Z

    .line 2005914
    move-object v1, v4

    .line 2005915
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2005916
    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2005917
    new-instance v1, LX/DVt;

    invoke-direct {v1, p0}, LX/DVt;-><init>(Lcom/facebook/groups/memberpicker/custominvite/CustomInviteMessageFragment;)V

    invoke-interface {v0, v1}, LX/1ZF;->a(LX/63W;)V

    .line 2005918
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2005919
    return-void

    :cond_1
    move v1, v2

    .line 2005920
    goto :goto_0
.end method
