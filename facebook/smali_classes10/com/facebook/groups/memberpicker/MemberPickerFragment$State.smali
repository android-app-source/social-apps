.class public final Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2005113
    new-instance v0, LX/DVZ;

    invoke-direct {v0}, LX/DVZ;-><init>()V

    sput-object v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2005114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2005115
    iput-object p1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;->b:Ljava/lang/String;

    .line 2005116
    iput-object p2, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;->a:Ljava/util/List;

    .line 2005117
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2005118
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2005119
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2005120
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2005121
    return-void
.end method
