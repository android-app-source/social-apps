.class public final Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2010996
    const-class v0, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel;

    new-instance v1, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2010997
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2010998
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2010999
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2011000
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2011001
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2011002
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2011003
    if-eqz v2, :cond_0

    .line 2011004
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2011005
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2011006
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2011007
    if-eqz v2, :cond_1

    .line 2011008
    const-string p0, "group_invite_link"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2011009
    invoke-static {v1, v2, p1, p2}, LX/DXq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2011010
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2011011
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2011012
    check-cast p1, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel$Serializer;->a(Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupRegenerateInviteLinkMutationModels$GroupInviteLinkRegenerateFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
