.class public final Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkExpireMutationModels$GroupInviteLinkExpireFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkExpireMutationModels$GroupInviteLinkExpireFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2010624
    const-class v0, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkExpireMutationModels$GroupInviteLinkExpireFieldsModel;

    new-instance v1, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkExpireMutationModels$GroupInviteLinkExpireFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkExpireMutationModels$GroupInviteLinkExpireFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2010625
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2010626
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkExpireMutationModels$GroupInviteLinkExpireFieldsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2010627
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2010628
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2010629
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2010630
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2010631
    if-eqz v2, :cond_0

    .line 2010632
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2010633
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2010634
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2010635
    if-eqz v2, :cond_1

    .line 2010636
    const-string p0, "group_invite_link"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2010637
    invoke-static {v1, v2, p1, p2}, LX/DXq;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2010638
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2010639
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2010640
    check-cast p1, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkExpireMutationModels$GroupInviteLinkExpireFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkExpireMutationModels$GroupInviteLinkExpireFieldsModel$Serializer;->a(Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkExpireMutationModels$GroupInviteLinkExpireFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
