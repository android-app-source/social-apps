.class public Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/DXJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DXP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1dV;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2010216
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2010217
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2010218
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;

    invoke-static {v0}, LX/DXJ;->a(LX/0QB;)LX/DXJ;

    move-result-object v2

    check-cast v2, LX/DXJ;

    invoke-static {v0}, LX/DXP;->a(LX/0QB;)LX/DXP;

    move-result-object p1

    check-cast p1, LX/DXP;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v2, p0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->a:LX/DXJ;

    iput-object p1, p0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->b:LX/DXP;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->c:LX/0kL;

    .line 2010219
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x2120f989

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2010220
    new-instance v1, LX/1De;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 2010221
    new-instance v2, Lcom/facebook/components/ComponentView;

    invoke-direct {v2, v1}, Lcom/facebook/components/ComponentView;-><init>(LX/1De;)V

    .line 2010222
    iget-object v3, p0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->a:LX/DXJ;

    invoke-virtual {v3, v1}, LX/DXJ;->c(LX/1De;)LX/DXH;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/DXH;->a(Lcom/facebook/groups/memberpicker/sharelink/protocol/GroupInviteLinkModels$GroupInviteLinkFieldsModel;)LX/DXH;

    move-result-object v3

    invoke-static {v1, v3}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v1

    invoke-virtual {v1}, LX/1me;->b()LX/1dV;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->d:LX/1dV;

    .line 2010223
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->d:LX/1dV;

    invoke-virtual {v2, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 2010224
    const/16 v1, 0x2b

    const v3, 0x1f622e28

    invoke-static {v5, v1, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x320f160

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2010225
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->d:LX/1dV;

    .line 2010226
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2010227
    const/16 v1, 0x2b

    const v2, -0xd92629c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2010228
    const-string v0, ""

    .line 2010229
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2010230
    if-eqz v1, :cond_0

    .line 2010231
    const-string v0, "group_feed_id"

    const-string v2, ""

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2010232
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;->b:LX/DXP;

    new-instance v2, LX/DXQ;

    invoke-direct {v2, p0}, LX/DXQ;-><init>(Lcom/facebook/groups/memberpicker/sharelink/GroupShareLinkFragment;)V

    .line 2010233
    iput-object v0, v1, LX/DXP;->e:Ljava/lang/String;

    .line 2010234
    iput-object v2, v1, LX/DXP;->f:LX/0TF;

    .line 2010235
    new-instance v3, LX/DXf;

    invoke-direct {v3}, LX/DXf;-><init>()V

    move-object v3, v3

    .line 2010236
    const-string v4, "group_id"

    iget-object v5, v1, LX/DXP;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2010237
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/16 v5, 0x258

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 2010238
    iget-object v4, v1, LX/DXP;->a:LX/0Sh;

    iget-object v5, v1, LX/DXP;->b:LX/0tX;

    invoke-virtual {v5, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    new-instance v5, LX/DXL;

    invoke-direct {v5, v1}, LX/DXL;-><init>(LX/DXP;)V

    invoke-virtual {v4, v3, v5}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2010239
    return-void
.end method
