.class public Lcom/facebook/groups/memberpicker/MemberPickerToken;
.super Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/groups/memberpicker/MemberPickerToken;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private e:Ljava/lang/String;

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2005523
    new-instance v0, LX/DVc;

    invoke-direct {v0}, LX/DVc;-><init>()V

    sput-object v0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 2005524
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    const-class v1, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    .line 2005525
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->e:Ljava/lang/String;

    .line 2005526
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->f:Z

    .line 2005527
    return-void

    .line 2005528
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/user/model/User;)V
    .locals 3

    .prologue
    .line 2005529
    iget-object v0, p1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2005530
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    .line 2005531
    iget-object v2, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2005532
    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    .line 2005533
    iget-object v0, p1, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    move-object v0, v0

    .line 2005534
    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->e:Ljava/lang/String;

    .line 2005535
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->f:Z

    .line 2005536
    return-void
.end method

.method public constructor <init>(Lcom/facebook/user/model/User;Z)V
    .locals 3

    .prologue
    .line 2005537
    iget-object v0, p1, Lcom/facebook/user/model/User;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2005538
    invoke-virtual {p1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    .line 2005539
    iget-object v2, p1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v2, v2

    .line 2005540
    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    .line 2005541
    iget-object v0, p1, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    move-object v0, v0

    .line 2005542
    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->e:Ljava/lang/String;

    .line 2005543
    iput-boolean p2, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->f:Z

    .line 2005544
    return-void
.end method

.method public constructor <init>(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2005545
    invoke-direct {p0, p1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 2005546
    iput-object p2, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->e:Ljava/lang/String;

    .line 2005547
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->f:Z

    .line 2005548
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2005549
    const/4 v0, 0x0

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2005550
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2005551
    const/4 v0, 0x1

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2005552
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 2005553
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2005554
    invoke-virtual {p0}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2005555
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 2005556
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2005557
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2005558
    iget-boolean v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2005559
    return-void

    .line 2005560
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
