.class public Lcom/facebook/groups/memberpicker/MemberPickerFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;
.implements LX/DM8;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;",
        "LX/0fh;",
        "LX/0fj;",
        "LX/DM8",
        "<",
        "Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;",
        ">;"
    }
.end annotation


# static fields
.field public static u:Ljava/lang/String;


# instance fields
.field public A:LX/DVD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/DVI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/DVk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DVN;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DVL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/DVb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/groups/multicompany/bridge/InviteByEmailNuxController;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/DVw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/DVO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private N:LX/DVj;

.field public O:LX/DVJ;

.field public P:LX/DVn;

.field private Q:LX/DM5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DM5",
            "<",
            "Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;",
            ">;"
        }
    .end annotation
.end field

.field public R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field private U:Z

.field public V:Z

.field public W:Landroid/support/v4/app/DialogFragment;

.field private X:LX/DM7;

.field private Y:LX/621;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field private Z:Ljava/lang/String;

.field private aa:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public ab:Ljava/lang/String;

.field private ac:Lcom/facebook/fig/listitem/FigListItem;

.field public v:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/DWD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/DVo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2005280
    const-string v0, "invite_option_section"

    sput-object v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->u:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2005281
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    .line 2005282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ab:Ljava/lang/String;

    .line 2005283
    return-void
.end method

.method public static I(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V
    .locals 6

    .prologue
    .line 2005284
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v0

    .line 2005285
    const/4 v1, 0x0

    .line 2005286
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-nez v2, :cond_a

    .line 2005287
    :cond_0
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2005288
    iget-object v2, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->w:LX/0kL;

    new-instance v3, LX/27k;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f083082

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v3}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2005289
    :cond_1
    :goto_0
    move v1, v1

    .line 2005290
    if-nez v1, :cond_4

    .line 2005291
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f083083

    .line 2005292
    :goto_1
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->w:LX/0kL;

    new-instance v2, LX/27k;

    invoke-direct {v2, v0}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2005293
    :cond_2
    :goto_2
    return-void

    .line 2005294
    :cond_3
    const v0, 0x7f083082

    goto :goto_1

    .line 2005295
    :cond_4
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X:LX/DM7;

    sget-object v2, LX/DM7;->TYPE_CREATE_FLOW:LX/DM7;

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Q:LX/DM5;

    if-eqz v1, :cond_5

    .line 2005296
    new-instance v1, Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;

    invoke-static {v0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-direct {v1, v0, v2}, Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;-><init>(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_2

    .line 2005297
    :cond_5
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X:LX/DM7;

    sget-object v1, LX/DM7;->TYPE_EXISTING_GROUP:LX/DM7;

    if-ne v0, v1, :cond_2

    .line 2005298
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->W(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)LX/0Px;

    move-result-object v0

    .line 2005299
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->M:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->J:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8vO;

    invoke-virtual {v1}, LX/8vO;->a()Z

    move-result v1

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 2005300
    if-eqz v1, :cond_6

    .line 2005301
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8vO;

    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->W(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)LX/0Px;

    new-instance v1, LX/DVY;

    invoke-direct {v1, p0}, LX/DVY;-><init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    invoke-virtual {v0}, LX/8vO;->b()V

    .line 2005302
    goto :goto_2

    .line 2005303
    :cond_6
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2005304
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2005305
    iget-object v4, v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2005306
    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v4

    sget-object v5, LX/0XG;->FACEBOOK:LX/0XG;

    if-eq v4, v5, :cond_8

    .line 2005307
    iget-object v4, v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 2005308
    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v4

    sget-object v5, LX/0XG;->FACEBOOK_CONTACT:LX/0XG;

    if-ne v4, v5, :cond_7

    .line 2005309
    :cond_8
    iget-object v4, v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v1, v4

    .line 2005310
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 2005311
    :cond_9
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2005312
    invoke-direct {p0, v1, v0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->a(LX/0Px;LX/0Px;)V

    goto/16 :goto_2

    .line 2005313
    :cond_a
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2005314
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2005315
    instance-of v4, v2, Lcom/facebook/groups/memberpicker/MemberPickerToken;

    if-eqz v4, :cond_c

    check-cast v2, Lcom/facebook/groups/memberpicker/MemberPickerToken;

    .line 2005316
    iget-boolean v4, v2, Lcom/facebook/groups/memberpicker/MemberPickerToken;->f:Z

    move v2, v4

    .line 2005317
    if-nez v2, :cond_b

    .line 2005318
    :cond_c
    const/4 v2, 0x1

    .line 2005319
    :goto_5
    move v2, v2

    .line 2005320
    if-eqz v2, :cond_1

    .line 2005321
    :cond_d
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_e
    const/4 v2, 0x0

    goto :goto_5

    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_3
.end method

.method public static K(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V
    .locals 3

    .prologue
    .line 2005322
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X:LX/DM7;

    sget-object v1, LX/DM7;->TYPE_CREATE_FLOW:LX/DM7;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Q:LX/DM5;

    if-eqz v0, :cond_0

    .line 2005323
    new-instance v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcom/facebook/groups/memberpicker/MemberPickerFragment$State;-><init>(Ljava/lang/String;Ljava/util/List;)V

    .line 2005324
    :goto_0
    return-void

    .line 2005325
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    goto :goto_0
.end method

.method public static L(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V
    .locals 1

    .prologue
    .line 2005326
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    .line 2005327
    return-void
.end method

.method public static O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z
    .locals 2

    .prologue
    .line 2005277
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X:LX/DM7;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X:LX/DM7;

    sget-object v1, LX/DM7;->TYPE_CREATE_FLOW:LX/DM7;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private P()V
    .locals 1

    .prologue
    .line 2005328
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    if-eqz v0, :cond_0

    .line 2005329
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    invoke-interface {v0}, LX/DVJ;->a()V

    .line 2005330
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->P:LX/DVn;

    if-eqz v0, :cond_1

    .line 2005331
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->P:LX/DVn;

    invoke-virtual {v0}, LX/B1d;->e()V

    .line 2005332
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->N:LX/DVj;

    if-eqz v0, :cond_4

    .line 2005333
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->N:LX/DVj;

    .line 2005334
    iget-object p0, v0, LX/DVj;->d:LX/DVd;

    if-eqz p0, :cond_2

    .line 2005335
    iget-object p0, v0, LX/DVj;->d:LX/DVd;

    invoke-virtual {p0}, LX/B1d;->e()V

    .line 2005336
    :cond_2
    iget-object p0, v0, LX/DVj;->e:LX/DVf;

    if-eqz p0, :cond_3

    .line 2005337
    iget-object p0, v0, LX/DVj;->e:LX/DVf;

    invoke-virtual {p0}, LX/B1d;->e()V

    .line 2005338
    :cond_3
    const/4 p0, 0x0

    .line 2005339
    iput-object p0, v0, LX/DVj;->f:LX/0Px;

    .line 2005340
    iput-object p0, v0, LX/DVj;->g:LX/0Px;

    .line 2005341
    iput-object p0, v0, LX/DVj;->h:Ljava/util/Set;

    .line 2005342
    :cond_4
    return-void
.end method

.method public static R(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)LX/DVj;
    .locals 10

    .prologue
    .line 2005343
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->N:LX/DVj;

    if-nez v0, :cond_0

    .line 2005344
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->D:LX/DVk;

    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v3, LX/DVV;

    invoke-direct {v3, p0}, LX/DVV;-><init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005345
    new-instance v4, LX/DVj;

    const-class v5, LX/DVg;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/DVg;

    const-class v5, LX/DVe;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/DVe;

    move-object v5, v1

    move-object v6, v2

    move-object v7, v3

    invoke-direct/range {v4 .. v9}, LX/DVj;-><init>(Ljava/lang/String;Ljava/lang/Integer;LX/DVU;LX/DVg;LX/DVe;)V

    .line 2005346
    move-object v0, v4

    .line 2005347
    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->N:LX/DVj;

    .line 2005348
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->N:LX/DVj;

    return-object v0
.end method

.method public static W(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005349
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2005350
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 2005351
    iget-object v3, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 2005352
    invoke-virtual {v3}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v3

    sget-object v4, LX/0XG;->EMAIL:LX/0XG;

    if-ne v3, v4, :cond_0

    .line 2005353
    iget-object v3, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, v3

    .line 2005354
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2005355
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static X(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2005356
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->G:LX/0Uh;

    const/16 v2, 0x3c1

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->U:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private static a(LX/0Px;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2005359
    const-string v0, ","

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/0Px;LX/0Px;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2005408
    iget-boolean v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->V:Z

    if-eqz v0, :cond_0

    .line 2005409
    :goto_0
    return-void

    .line 2005410
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2005411
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "GroupID has not been set for member picker"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2005412
    :cond_1
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2005413
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->x:Landroid/content/res/Resources;

    const v1, 0x7f0f0155

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->W:Landroid/support/v4/app/DialogFragment;

    .line 2005414
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->W:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2005415
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->V:Z

    .line 2005416
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005417
    iget-object v2, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->y:LX/DWD;

    iget-object v3, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v6, "mobile_create_group"

    :goto_1
    iget-object v7, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ab:Ljava/lang/String;

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v2 .. v7}, LX/DWD;->a(Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2005418
    new-instance v3, LX/DVT;

    invoke-direct {v3, p0, p1, p2, p0}, LX/DVT;-><init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;LX/0Px;LX/0Px;Lcom/facebook/base/fragment/FbFragment;)V

    iget-object v4, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->v:LX/0TD;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2005419
    goto :goto_0

    .line 2005420
    :cond_3
    const-string v6, "mobile_add_members"

    goto :goto_1
.end method

.method private static a(Lcom/facebook/groups/memberpicker/MemberPickerFragment;LX/0TD;LX/0kL;Landroid/content/res/Resources;LX/DWD;LX/DVo;LX/DVD;LX/DVI;LX/0Or;LX/DVk;LX/0Ot;LX/0Ot;LX/0Uh;LX/DVb;LX/1g8;LX/0Ot;LX/DVw;LX/DVO;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/memberpicker/MemberPickerFragment;",
            "LX/0TD;",
            "LX/0kL;",
            "Landroid/content/res/Resources;",
            "LX/DWD;",
            "LX/DVo;",
            "LX/DVD;",
            "Lcom/facebook/groups/memberpicker/MemberPickerNavigationHandler;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/DVk;",
            "LX/0Ot",
            "<",
            "LX/DVN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DVL;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/DVb;",
            "LX/1g8;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/work/groups/multicompany/bridge/InviteByEmailNuxController;",
            ">;",
            "LX/DVw;",
            "LX/DVO;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2005407
    iput-object p1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->v:LX/0TD;

    iput-object p2, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->w:LX/0kL;

    iput-object p3, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->x:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->y:LX/DWD;

    iput-object p5, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->z:LX/DVo;

    iput-object p6, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->A:LX/DVD;

    iput-object p7, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->B:LX/DVI;

    iput-object p8, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->C:LX/0Or;

    iput-object p9, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->D:LX/DVk;

    iput-object p10, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->E:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->F:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->G:LX/0Uh;

    iput-object p13, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->H:LX/DVb;

    iput-object p14, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->I:LX/1g8;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->J:LX/0Ot;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->K:LX/DVw;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L:LX/DVO;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->M:Ljava/lang/Boolean;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 21

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v20

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/groups/memberpicker/MemberPickerFragment;

    invoke-static/range {v20 .. v20}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static/range {v20 .. v20}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static/range {v20 .. v20}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static/range {v20 .. v20}, LX/DWD;->a(LX/0QB;)LX/DWD;

    move-result-object v6

    check-cast v6, LX/DWD;

    const-class v7, LX/DVo;

    move-object/from16 v0, v20

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/DVo;

    invoke-static/range {v20 .. v20}, LX/Jas;->a(LX/0QB;)LX/Jas;

    move-result-object v8

    check-cast v8, LX/DVD;

    invoke-static/range {v20 .. v20}, LX/DVI;->a(LX/0QB;)LX/DVI;

    move-result-object v9

    check-cast v9, LX/DVI;

    const/16 v10, 0x15e7

    move-object/from16 v0, v20

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const-class v11, LX/DVk;

    move-object/from16 v0, v20

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/DVk;

    const/16 v12, 0x2475

    move-object/from16 v0, v20

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x2474

    move-object/from16 v0, v20

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v20 .. v20}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v14

    check-cast v14, LX/0Uh;

    invoke-static/range {v20 .. v20}, LX/DVb;->a(LX/0QB;)LX/DVb;

    move-result-object v15

    check-cast v15, LX/DVb;

    invoke-static/range {v20 .. v20}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v16

    check-cast v16, LX/1g8;

    const/16 v17, 0x38bf

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-static/range {v20 .. v20}, LX/DVw;->a(LX/0QB;)LX/DVw;

    move-result-object v18

    check-cast v18, LX/DVw;

    invoke-static/range {v20 .. v20}, LX/DVO;->a(LX/0QB;)LX/DVO;

    move-result-object v19

    check-cast v19, LX/DVO;

    invoke-static/range {v20 .. v20}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v20

    check-cast v20, Ljava/lang/Boolean;

    invoke-static/range {v2 .. v20}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->a(Lcom/facebook/groups/memberpicker/MemberPickerFragment;LX/0TD;LX/0kL;Landroid/content/res/Resources;LX/DWD;LX/DVo;LX/DVD;LX/DVI;LX/0Or;LX/DVk;LX/0Ot;LX/0Ot;LX/0Uh;LX/DVb;LX/1g8;LX/0Ot;LX/DVw;LX/DVO;Ljava/lang/Boolean;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2005383
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2005384
    if-eqz v0, :cond_2

    .line 2005385
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2005386
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->P:LX/DVn;

    if-nez v0, :cond_0

    .line 2005387
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->z:LX/DVo;

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->C:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, LX/DVX;

    invoke-direct {v2, p0}, LX/DVX;-><init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005388
    new-instance p1, LX/DVn;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p2

    check-cast p2, LX/0tX;

    invoke-direct {p1, v3, v0, p2, v2}, LX/DVn;-><init>(LX/1Ck;Ljava/lang/String;LX/0tX;LX/B1b;)V

    .line 2005389
    move-object v0, p1

    .line 2005390
    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->P:LX/DVn;

    .line 2005391
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->P:LX/DVn;

    move-object v0, v0

    .line 2005392
    invoke-virtual {v0}, LX/B1d;->f()V

    .line 2005393
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    if-eqz v0, :cond_1

    .line 2005394
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    invoke-interface {v0}, LX/DVJ;->a()V

    .line 2005395
    :cond_1
    :goto_1
    return-void

    .line 2005396
    :cond_2
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2005397
    if-eqz p2, :cond_7

    .line 2005398
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)LX/DVj;

    move-result-object v0

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, LX/DVj;->a(Z)V

    .line 2005399
    :cond_3
    :goto_2
    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 2005400
    :cond_5
    invoke-static {p0, p1}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->f(Lcom/facebook/groups/memberpicker/MemberPickerFragment;Ljava/lang/String;)LX/DVJ;

    move-result-object v0

    invoke-interface {v0, p1}, LX/DVJ;->a(Ljava/lang/String;)V

    .line 2005401
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->P:LX/DVn;

    if-eqz v0, :cond_1

    .line 2005402
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->P:LX/DVn;

    invoke-virtual {v0}, LX/B1d;->e()V

    goto :goto_1

    .line 2005403
    :cond_6
    invoke-static {p0, p1}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->f(Lcom/facebook/groups/memberpicker/MemberPickerFragment;Ljava/lang/String;)LX/DVJ;

    move-result-object v0

    invoke-interface {v0, p1}, LX/DVJ;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 2005404
    :cond_7
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)LX/DVj;

    move-result-object v0

    invoke-virtual {v0}, LX/DVj;->a()V

    .line 2005405
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    if-eqz v0, :cond_3

    .line 2005406
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    invoke-interface {v0}, LX/DVJ;->a()V

    goto :goto_2
.end method

.method public static f(Lcom/facebook/groups/memberpicker/MemberPickerFragment;Ljava/lang/String;)LX/DVJ;
    .locals 4

    .prologue
    .line 2005375
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    if-nez v0, :cond_0

    .line 2005376
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->F:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DVJ;

    :goto_0
    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    .line 2005377
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    invoke-interface {v0}, LX/DVJ;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2005378
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    invoke-interface {v0}, LX/DVJ;->a()V

    .line 2005379
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    invoke-interface {v0}, LX/DVJ;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2005380
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    new-instance v2, LX/DVW;

    invoke-direct {v2, p0}, LX/DVW;-><init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, LX/DVJ;->a(Ljava/lang/String;LX/DVU;Z)V

    .line 2005381
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O:LX/DVJ;

    return-object v0

    .line 2005382
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->E:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DVJ;

    goto :goto_0
.end method


# virtual methods
.method public final B()V
    .locals 4

    .prologue
    .line 2005366
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X:LX/DM7;

    sget-object v1, LX/DM7;->TYPE_EXISTING_GROUP:LX/DM7;

    if-ne v0, v1, :cond_0

    .line 2005367
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->I:LX/1g8;

    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    .line 2005368
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "groups_member_picker_navigation"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "add_member"

    .line 2005369
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2005370
    move-object v2, v2

    .line 2005371
    const-string v3, "group"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "type"

    const-string p0, "cancel"

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "source"

    const-string p0, "invite"

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2005372
    iget-object v3, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2005373
    invoke-static {v0, v2}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2005374
    :cond_0
    return-void
.end method

.method public final D()V
    .locals 1

    .prologue
    .line 2005362
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2005363
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->K:LX/DVw;

    invoke-virtual {v0, p0}, LX/DVw;->b(Landroid/support/v4/app/Fragment;)V

    .line 2005364
    :cond_0
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005365
    return-void
.end method

.method public final E()V
    .locals 2

    .prologue
    .line 2005360
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->a(Ljava/lang/String;Z)V

    .line 2005361
    return-void
.end method

.method public final G()V
    .locals 0

    .prologue
    .line 2005357
    return-void
.end method

.method public final H()Z
    .locals 1

    .prologue
    .line 2005358
    const/4 v0, 0x0

    return v0
.end method

.method public final S_()Z
    .locals 2

    .prologue
    .line 2005278
    goto :goto_1

    .line 2005279
    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)LX/44w;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;)",
            "LX/44w",
            "<",
            "LX/0Rf",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2005125
    sget-object v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005126
    new-instance v0, LX/44w;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Y:LX/621;

    invoke-direct {v0, v1, v2}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2005127
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/lang/String;Ljava/util/Map;)LX/44w;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/user/model/User;Ljava/lang/String;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
    .locals 2

    .prologue
    .line 2005186
    new-instance v0, Lcom/facebook/groups/memberpicker/MemberPickerToken;

    invoke-direct {v0, p1}, Lcom/facebook/groups/memberpicker/MemberPickerToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 2005187
    iget-object v1, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 2005188
    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/lang/String;)Z

    move-result v1

    .line 2005189
    iput-boolean v1, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 2005190
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "member_search_invite_result_section"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2005191
    iput-boolean v1, v0, Lcom/facebook/groups/memberpicker/MemberPickerToken;->f:Z

    .line 2005192
    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2005185
    const-string v0, "add_member"

    return-object v0
.end method

.method public final a(I)V
    .locals 6

    .prologue
    .line 2005166
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {v0, p1}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 2005167
    iget-object v1, v0, LX/8QL;->a:LX/8vA;

    sget-object v2, LX/8vA;->USER:LX/8vA;

    if-ne v1, v2, :cond_3

    move-object v1, v0

    .line 2005168
    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    move-object v1, v0

    .line 2005169
    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2005170
    invoke-virtual {v0}, LX/8QL;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2005171
    iget-object v2, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->I:LX/1g8;

    iget-object v3, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Z:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 2005172
    :goto_1
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "groups_member_picker_action"

    invoke-direct {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "add_member"

    .line 2005173
    iput-object v5, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2005174
    move-object v4, v4

    .line 2005175
    const-string v5, "group"

    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "position"

    invoke-virtual {v4, v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "item_type"

    const-string p0, "profile"

    invoke-virtual {v4, v5, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string p0, "selection_source"

    if-eqz v1, :cond_5

    const-string v4, "search"

    :goto_2
    invoke-virtual {v5, p0, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "source"

    const-string p0, "invite"

    invoke-virtual {v4, v5, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "invitee_id"

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2005176
    iget-object v5, v2, LX/1g8;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2005177
    invoke-static {v2, v4}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2005178
    :cond_0
    :goto_3
    return-void

    .line 2005179
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 2005180
    :cond_3
    iget-object v0, v0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->INVITE:LX/8vA;

    if-ne v0, v1, :cond_0

    .line 2005181
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->S:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 2005182
    const-string v0, "MemberPickerFragment"

    const-string v1, "group url is null for inviting users via share."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 2005183
    :cond_4
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    goto :goto_3

    .line 2005184
    :cond_5
    const-string v4, "alphabetical"

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2005145
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 2005146
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2005147
    sget-object v0, LX/DM7;->TYPE_EXISTING_GROUP:LX/DM7;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X:LX/DM7;

    .line 2005148
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2005149
    if-eqz v0, :cond_0

    .line 2005150
    const-string v1, "CreateFlowType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2005151
    sget-object v0, LX/DM7;->TYPE_CREATE_FLOW:LX/DM7;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X:LX/DM7;

    .line 2005152
    :cond_0
    :goto_0
    new-instance v0, LX/623;

    const-string v1, ""

    new-instance v2, LX/DVP;

    invoke-direct {v2}, LX/DVP;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Y:LX/621;

    .line 2005153
    return-void

    .line 2005154
    :cond_1
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    .line 2005155
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->values()[Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    const-string v2, "group_visibility"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2005156
    const-string v1, "group_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->S:Ljava/lang/String;

    .line 2005157
    const-string v1, "enable_email_invite"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->U:Z

    .line 2005158
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->I:LX/1g8;

    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    .line 2005159
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "groups_member_picker_navigation"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "add_member"

    .line 2005160
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2005161
    move-object v2, v2

    .line 2005162
    const-string v3, "group"

    invoke-virtual {v2, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "type"

    const-string p1, "start"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "source"

    const-string p1, "invite"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2005163
    iget-object v3, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2005164
    invoke-static {v0, v2}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2005165
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2005135
    const-string v0, "member_suggestions_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2005136
    const v0, 0x7f08307d

    .line 2005137
    :goto_0
    return v0

    .line 2005138
    :cond_0
    const-string v0, "member_picker_merged_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2005139
    const v0, 0x7f08307c

    goto :goto_0

    .line 2005140
    :cond_1
    const-string v0, "member_search_result_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2005141
    const v0, 0x7f08307e

    goto :goto_0

    .line 2005142
    :cond_2
    const-string v0, "member_search_invite_result_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2005143
    const v0, 0x7f08307f

    goto :goto_0

    .line 2005144
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2005131
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Landroid/os/Bundle;)V

    .line 2005132
    if-eqz p1, :cond_0

    .line 2005133
    const-string v0, "selectedTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->aa:Ljava/util/List;

    .line 2005134
    :cond_0
    return-void
.end method

.method public final c()LX/8RE;
    .locals 1

    .prologue
    .line 2005129
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->R:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    .line 2005130
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->A:LX/DVD;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2005128
    const-string v0, "member_search_result_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "member_search_invite_result_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2005122
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2005123
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->a(Ljava/lang/String;Z)V

    .line 2005124
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2005222
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->aa:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->aa:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2005223
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->aa:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/util/Set;)V

    .line 2005224
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->aa:Ljava/util/List;

    .line 2005225
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2005226
    const/16 v0, 0x457f

    if-eq p1, v0, :cond_0

    .line 2005227
    :goto_0
    return-void

    .line 2005228
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->K:LX/DVw;

    invoke-virtual {v0, p1, p2, p3}, LX/DVw;->a(IILandroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ab:Ljava/lang/String;

    .line 2005229
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ab:Ljava/lang/String;

    invoke-static {v0, p0}, LX/DVw;->a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0xfef158c

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2005230
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 2005231
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L:LX/DVO;

    iget-object v3, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v4, 0x0

    .line 2005232
    sget-object p1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->SECRET:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v3, p1, :cond_0

    iget-object p1, v0, LX/DVO;->a:LX/0ad;

    sget-short p2, LX/DVr;->a:S

    invoke-interface {p1, p2, v4}, LX/0ad;->a(SZ)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 v4, 0x1

    :cond_0
    move v0, v4

    .line 2005233
    if-eqz v0, :cond_1

    .line 2005234
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2005235
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2005236
    new-instance v3, Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ac:Lcom/facebook/fig/listitem/FigListItem;

    .line 2005237
    iget-object v3, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ac:Lcom/facebook/fig/listitem/FigListItem;

    const v4, 0x7f083095

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 2005238
    iget-object v3, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ac:Lcom/facebook/fig/listitem/FigListItem;

    new-instance v4, LX/DVQ;

    invoke-direct {v4, p0}, LX/DVQ;-><init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2005239
    iget-object v3, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ac:Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2005240
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2005241
    const/16 v1, 0x2b

    const v3, 0x39f78d37

    invoke-static {v5, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2005242
    :goto_0
    return-object v0

    :cond_1
    const v0, -0x5c43afe7

    invoke-static {v0, v2}, LX/02F;->f(II)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5ee66ae4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2005243
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->P()V

    .line 2005244
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onPause()V

    .line 2005245
    const/16 v1, 0x2b

    const v2, 0x42710940

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x7593c10d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2005246
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onResume()V

    .line 2005247
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005248
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ac:Lcom/facebook/fig/listitem/FigListItem;

    if-eqz v1, :cond_0

    .line 2005249
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L:LX/DVO;

    iget-object v2, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->ac:Lcom/facebook/fig/listitem/FigListItem;

    .line 2005250
    iget-object v4, v1, LX/DVO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/DZA;->g:LX/0Tn;

    const/4 p0, 0x0

    invoke-interface {v4, v5, p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2005251
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x41481658

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2005252
    :cond_1
    new-instance v4, LX/0hs;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 p0, 0x2

    invoke-direct {v4, v5, p0}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2005253
    const v5, 0x7f08308e

    invoke-virtual {v4, v5}, LX/0hs;->a(I)V

    .line 2005254
    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f08308f

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2005255
    sget-object v5, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v4, v5}, LX/0ht;->a(LX/3AV;)V

    .line 2005256
    invoke-virtual {v4, v2}, LX/0ht;->f(Landroid/view/View;)V

    .line 2005257
    iget-object v4, v1, LX/DVO;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/DZA;->g:LX/0Tn;

    const/4 p0, 0x1

    invoke-interface {v4, v5, p0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2005193
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2005194
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->B:LX/DVI;

    .line 2005195
    new-instance v1, LX/DVS;

    invoke-direct {v1, p0}, LX/DVS;-><init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    move-object v1, v1

    .line 2005196
    new-instance p2, LX/DVR;

    invoke-direct {p2, p0}, LX/DVR;-><init>(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005197
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->O(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)Z

    .line 2005198
    const-class v2, LX/1ZF;

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ZF;

    .line 2005199
    if-eqz v2, :cond_0

    .line 2005200
    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1ZF;->k_(Z)V

    .line 2005201
    const v3, 0x7f083081

    invoke-interface {v2, v3}, LX/1ZF;->x_(I)V

    .line 2005202
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v3

    iget-object v4, v0, LX/DVI;->a:Landroid/content/res/Resources;

    const p2, 0x7f08307b

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2005203
    iput-object v4, v3, LX/108;->g:Ljava/lang/String;

    .line 2005204
    move-object v3, v3

    .line 2005205
    const/4 v4, -0x2

    .line 2005206
    iput v4, v3, LX/108;->h:I

    .line 2005207
    move-object v3, v3

    .line 2005208
    invoke-virtual {v3}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v3

    .line 2005209
    invoke-interface {v2, v3}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2005210
    new-instance v3, LX/DVH;

    invoke-direct {v3, v0, v1}, LX/DVH;-><init>(LX/DVI;Landroid/view/View$OnClickListener;)V

    invoke-interface {v2, v3}, LX/1ZF;->a(LX/63W;)V

    .line 2005211
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->K:LX/DVw;

    invoke-virtual {v0, p0}, LX/DVw;->a(Landroid/support/v4/app/Fragment;)V

    .line 2005212
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->L(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005213
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v0, p1}, LX/BWl;->d(Landroid/view/View;)Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    move-result-object v0

    .line 2005214
    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->requestFocus()Z

    .line 2005215
    sget-object v1, LX/8uy;->PLAIN_TEXT:LX/8uy;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setTextMode(LX/8uy;)V

    .line 2005216
    iget-boolean v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->U:Z

    if-eqz v1, :cond_1

    .line 2005217
    const v1, 0x7f083086

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setHint(I)V

    .line 2005218
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v0, p1}, LX/BWl;->f(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 2005219
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 2005220
    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f083086

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 2005221
    :cond_1
    return-void
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 2005258
    const/16 v0, 0x32

    return v0
.end method

.method public final u()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2005259
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->X:LX/DM7;

    sget-object v1, LX/DM7;->TYPE_CREATE_FLOW:LX/DM7;

    if-ne v0, v1, :cond_0

    .line 2005260
    sget-object v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a:Ljava/lang/String;

    const-string v1, "member_search_result_section"

    const-string v2, "member_search_invite_result_section"

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2005261
    :goto_0
    return-object v0

    .line 2005262
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->S:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->T:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->CLOSED:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v0, v1, :cond_2

    .line 2005263
    :cond_1
    sget-object v0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->u:Ljava/lang/String;

    const-string v1, "member_suggestions_section"

    const-string v2, "member_picker_merged_section"

    const-string v3, "member_search_result_section"

    const-string v4, "member_search_invite_result_section"

    invoke-static {v0, v1, v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 2005264
    :cond_2
    const-string v0, "member_suggestions_section"

    const-string v1, "member_picker_merged_section"

    const-string v2, "member_search_result_section"

    const-string v3, "member_search_invite_result_section"

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final w()V
    .locals 2

    .prologue
    .line 2005265
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    const v1, -0x7303669d

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2005266
    return-void
.end method

.method public final x()V
    .locals 2

    .prologue
    .line 2005267
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 2005268
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Z:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2005269
    const-string v1, ""

    iput-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Z:Ljava/lang/String;

    .line 2005270
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Z:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2005271
    iput-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Z:Ljava/lang/String;

    .line 2005272
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    new-instance v1, LX/623;

    invoke-direct {v1}, LX/623;-><init>()V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8tB;->a(Ljava/util/List;)V

    .line 2005273
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->Z:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->a(Ljava/lang/String;Z)V

    .line 2005274
    :cond_1
    return-void
.end method

.method public final y()V
    .locals 0

    .prologue
    .line 2005275
    invoke-static {p0}, Lcom/facebook/groups/memberpicker/MemberPickerFragment;->I(Lcom/facebook/groups/memberpicker/MemberPickerFragment;)V

    .line 2005276
    return-void
.end method
