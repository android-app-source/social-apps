.class public final Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2006127
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    new-instance v1, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2006128
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2006129
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2006130
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2006131
    const/4 v2, 0x0

    .line 2006132
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_12

    .line 2006133
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2006134
    :goto_0
    move v1, v2

    .line 2006135
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2006136
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2006137
    new-instance v1, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;-><init>()V

    .line 2006138
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2006139
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2006140
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2006141
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2006142
    :cond_0
    return-object v1

    .line 2006143
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2006144
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_11

    .line 2006145
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2006146
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2006147
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 2006148
    const-string p0, "added_users"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2006149
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2006150
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_3

    .line 2006151
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_3

    .line 2006152
    invoke-static {p1, v0}, LX/DW6;->b(LX/15w;LX/186;)I

    move-result v11

    .line 2006153
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2006154
    :cond_3
    invoke-static {v10, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v10

    move v10, v10

    .line 2006155
    goto :goto_1

    .line 2006156
    :cond_4
    const-string p0, "already_added_users"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2006157
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2006158
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_5

    .line 2006159
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_5

    .line 2006160
    invoke-static {p1, v0}, LX/DW7;->b(LX/15w;LX/186;)I

    move-result v11

    .line 2006161
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2006162
    :cond_5
    invoke-static {v9, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v9

    move v9, v9

    .line 2006163
    goto :goto_1

    .line 2006164
    :cond_6
    const-string p0, "already_invited_users"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 2006165
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2006166
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_7

    .line 2006167
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_7

    .line 2006168
    invoke-static {p1, v0}, LX/DW8;->b(LX/15w;LX/186;)I

    move-result v11

    .line 2006169
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2006170
    :cond_7
    invoke-static {v8, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 2006171
    goto/16 :goto_1

    .line 2006172
    :cond_8
    const-string p0, "client_mutation_id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 2006173
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto/16 :goto_1

    .line 2006174
    :cond_9
    const-string p0, "failed_emails"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 2006175
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 2006176
    :cond_a
    const-string p0, "failed_users"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 2006177
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2006178
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_b

    .line 2006179
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_b

    .line 2006180
    invoke-static {p1, v0}, LX/DW9;->b(LX/15w;LX/186;)I

    move-result v11

    .line 2006181
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2006182
    :cond_b
    invoke-static {v5, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 2006183
    goto/16 :goto_1

    .line 2006184
    :cond_c
    const-string p0, "invited_emails"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_d

    .line 2006185
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2006186
    :cond_d
    const-string p0, "invited_users"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_f

    .line 2006187
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2006188
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_e

    .line 2006189
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_e

    .line 2006190
    invoke-static {p1, v0}, LX/DWA;->b(LX/15w;LX/186;)I

    move-result v11

    .line 2006191
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 2006192
    :cond_e
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2006193
    goto/16 :goto_1

    .line 2006194
    :cond_f
    const-string p0, "requested_users"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 2006195
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2006196
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_10

    .line 2006197
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_10

    .line 2006198
    invoke-static {p1, v0}, LX/DWB;->b(LX/15w;LX/186;)I

    move-result v11

    .line 2006199
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2006200
    :cond_10
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2006201
    goto/16 :goto_1

    .line 2006202
    :cond_11
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 2006203
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2006204
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2006205
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2006206
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2006207
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2006208
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2006209
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2006210
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2006211
    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2006212
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_12
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
