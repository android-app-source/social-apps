.class public final Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x717994b5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2006948
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2006954
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2006952
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2006953
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006949
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2006950
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2006951
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006946
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->g:Ljava/lang/String;

    .line 2006947
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2006936
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2006937
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2006938
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2006939
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2006940
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2006941
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2006942
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2006943
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2006944
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2006945
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2006928
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2006929
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2006930
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    .line 2006931
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2006932
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;

    .line 2006933
    iput-object v0, v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    .line 2006934
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2006935
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2006955
    new-instance v0, LX/DWJ;

    invoke-direct {v0, p1}, LX/DWJ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006927
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2006925
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2006926
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2006924
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2006917
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;-><init>()V

    .line 2006918
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2006919
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2006923
    const v0, 0x3e70af02

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2006922
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupInviteMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006920
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    .line 2006921
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    return-object v0
.end method
