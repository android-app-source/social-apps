.class public final Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2009195
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;

    new-instance v1, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2009196
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2009197
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2009198
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2009199
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2009200
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2009201
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2009202
    if-eqz v2, :cond_0

    .line 2009203
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2009204
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2009205
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2009206
    if-eqz v2, :cond_1

    .line 2009207
    const-string p0, "friends"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2009208
    invoke-static {v1, v2, p1, p2}, LX/DX5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2009209
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2009210
    if-eqz v2, :cond_2

    .line 2009211
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2009212
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2009213
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2009214
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2009215
    check-cast p1, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$Serializer;->a(Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
