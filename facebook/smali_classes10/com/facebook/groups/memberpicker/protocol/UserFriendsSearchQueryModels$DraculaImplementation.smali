.class public final Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2009505
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2009506
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2009503
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2009504
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2009487
    if-nez p1, :cond_0

    .line 2009488
    :goto_0
    return v0

    .line 2009489
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2009490
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2009491
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2009492
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2009493
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 2009494
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 2009495
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2009496
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2009497
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2009498
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2009499
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 2009500
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 2009501
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2009502
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2243ed7d
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2009486
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2009483
    packed-switch p0, :pswitch_data_0

    .line 2009484
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2009485
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x2243ed7d
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2009482
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2009480
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;->b(I)V

    .line 2009481
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2009475
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2009476
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2009477
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2009478
    iput p2, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;->b:I

    .line 2009479
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2009449
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2009474
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2009471
    iget v0, p0, LX/1vt;->c:I

    .line 2009472
    move v0, v0

    .line 2009473
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2009468
    iget v0, p0, LX/1vt;->c:I

    .line 2009469
    move v0, v0

    .line 2009470
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2009465
    iget v0, p0, LX/1vt;->b:I

    .line 2009466
    move v0, v0

    .line 2009467
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2009462
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2009463
    move-object v0, v0

    .line 2009464
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2009453
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2009454
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2009455
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2009456
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2009457
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2009458
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2009459
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2009460
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2009461
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2009450
    iget v0, p0, LX/1vt;->c:I

    .line 2009451
    move v0, v0

    .line 2009452
    return v0
.end method
