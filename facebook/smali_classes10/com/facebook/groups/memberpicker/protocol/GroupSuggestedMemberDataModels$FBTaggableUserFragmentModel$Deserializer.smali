.class public final Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2007237
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel;

    new-instance v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2007238
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2007239
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2007240
    invoke-static {p1}, LX/DWU;->a(LX/15w;)LX/15i;

    move-result-object v2

    .line 2007241
    new-instance v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel;-><init>()V

    .line 2007242
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2007243
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2007244
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2007245
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2007246
    :cond_0
    return-object v1
.end method
