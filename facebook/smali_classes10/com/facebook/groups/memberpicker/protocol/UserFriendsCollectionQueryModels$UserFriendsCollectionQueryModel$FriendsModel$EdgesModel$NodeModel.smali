.class public final Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/DWH;
.implements LX/DWG;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x47e88e54
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:D

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2009067
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2009068
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2009069
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2009070
    return-void
.end method

.method private n()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBylines"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2009071
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x4e067bed    # 5.6406714E8f

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->e:LX/3Sb;

    .line 2009072
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2009112
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2009113
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 2009073
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2009074
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->n()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 2009075
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2009076
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2009077
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 2009078
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x6c4b15a

    invoke-static {v2, v1, v3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2009079
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->o()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x5aeb5d93

    invoke-static {v2, v1, v3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2009080
    const/4 v1, 0x7

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2009081
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2009082
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->f:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2009083
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2009084
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2009085
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2009086
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2009087
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2009088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2009089
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2009090
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2009091
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->n()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2009092
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->n()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2009093
    if-eqz v1, :cond_3

    .line 2009094
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;

    .line 2009095
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->e:LX/3Sb;

    move-object v1, v0

    .line 2009096
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2009097
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6c4b15a

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2009098
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2009099
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;

    .line 2009100
    iput v3, v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->j:I

    move-object v1, v0

    .line 2009101
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2009102
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5aeb5d93

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2009103
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2009104
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;

    .line 2009105
    iput v3, v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->k:I

    move-object v1, v0

    .line 2009106
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2009107
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    .line 2009108
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2009109
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2009110
    goto :goto_1

    :cond_3
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2009111
    new-instance v0, LX/DX0;

    invoke-direct {v0, p1}, LX/DX0;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2009061
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2009062
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2009063
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->f:D

    .line 2009064
    const/4 v0, 0x5

    const v1, 0x6c4b15a

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->j:I

    .line 2009065
    const/4 v0, 0x6

    const v1, 0x5aeb5d93

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->k:I

    .line 2009066
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2009050
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2009051
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2009045
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2009046
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;-><init>()V

    .line 2009047
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2009048
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2009049
    const v0, -0x4b05a07a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2009060
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2009052
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    .line 2009053
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2009054
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 2009055
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2009056
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    .line 2009057
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture50"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2009058
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2009059
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsCollectionQueryModels$UserFriendsCollectionQueryModel$FriendsModel$EdgesModel$NodeModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
