.class public final Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2006032
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2006031
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2006029
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2006030
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2006023
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2006024
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2006025
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2006026
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2006027
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2006028
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2006008
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2006009
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2006010
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2006022
    new-instance v0, LX/DVz;

    invoke-direct {v0, p1}, LX/DVz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006021
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2006019
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2006020
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2006018
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2006015
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;-><init>()V

    .line 2006016
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2006017
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2006014
    const v0, -0x78004267

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2006013
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006011
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;->e:Ljava/lang/String;

    .line 2006012
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$AddedUsersModel;->e:Ljava/lang/String;

    return-object v0
.end method
