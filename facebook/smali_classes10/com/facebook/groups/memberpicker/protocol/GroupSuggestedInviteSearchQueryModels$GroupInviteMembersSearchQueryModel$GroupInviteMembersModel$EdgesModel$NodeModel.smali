.class public final Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/DWH;
.implements LX/DWG;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6db97322
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:D

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2006820
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2006819
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2006817
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2006818
    return-void
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006814
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2006815
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2006816
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private o()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBylines"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2006812
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->f:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x1

    const v4, 0x4e067bed    # 5.6406714E8f

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->f:LX/3Sb;

    .line 2006813
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->f:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private p()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStructuredName"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006810
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2006811
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 2006791
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2006792
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2006793
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->o()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 2006794
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2006795
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2006796
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->l()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v8

    .line 2006797
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x6c4b15a

    invoke-static {v3, v2, v4}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2006798
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->p()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x5aeb5d93

    invoke-static {v3, v2, v4}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2006799
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2006800
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2006801
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2006802
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->g:D

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2006803
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2006804
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2006805
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2006806
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2006807
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2006808
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2006809
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2006770
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2006771
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->o()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2006772
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->o()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2006773
    if-eqz v1, :cond_3

    .line 2006774
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    .line 2006775
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->f:LX/3Sb;

    move-object v1, v0

    .line 2006776
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2006777
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6c4b15a

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2006778
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2006779
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    .line 2006780
    iput v3, v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->k:I

    move-object v1, v0

    .line 2006781
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2006782
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->p()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5aeb5d93

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2006783
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2006784
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    .line 2006785
    iput v3, v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->l:I

    move-object v1, v0

    .line 2006786
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2006787
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    .line 2006788
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2006789
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2006790
    goto :goto_1

    :cond_3
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2006769
    new-instance v0, LX/DWI;

    invoke-direct {v0, p1}, LX/DWI;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006821
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2006764
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2006765
    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->g:D

    .line 2006766
    const/4 v0, 0x6

    const v1, 0x6c4b15a

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->k:I

    .line 2006767
    const/4 v0, 0x7

    const v1, 0x5aeb5d93

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->l:I

    .line 2006768
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2006762
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2006763
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2006761
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2006758
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;-><init>()V

    .line 2006759
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2006760
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2006757
    const v0, -0x5f2f034c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2006756
    const v0, 0x50c72189

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006754
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    .line 2006755
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006752
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    .line 2006753
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2006750
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->j:Ljava/util/List;

    .line 2006751
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture50"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006748
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2006749
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
