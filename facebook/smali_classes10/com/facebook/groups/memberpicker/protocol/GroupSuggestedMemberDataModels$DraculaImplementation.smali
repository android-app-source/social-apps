.class public final Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2007233
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2007234
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2007231
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2007232
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    .line 2007175
    if-nez p1, :cond_0

    .line 2007176
    const/4 v0, 0x0

    .line 2007177
    :goto_0
    return v0

    .line 2007178
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2007179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2007180
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2007181
    const v1, 0x3aaa24f8

    const/4 v3, 0x0

    .line 2007182
    if-nez v0, :cond_1

    move v2, v3

    .line 2007183
    :goto_1
    move v0, v2

    .line 2007184
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2007185
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2007186
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2007187
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 2007188
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2007189
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2007190
    :sswitch_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2007191
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(III)I

    move-result v1

    .line 2007192
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStructuredNamePart;

    move-result-object v2

    .line 2007193
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 2007194
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2007195
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v0, v4}, LX/186;->a(III)V

    .line 2007196
    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-virtual {p3, v0, v1, v3}, LX/186;->a(III)V

    .line 2007197
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 2007198
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2007199
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2007200
    const v1, 0x543d3c04

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 2007201
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2007202
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2007203
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2007204
    :sswitch_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2007205
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2007206
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2007207
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2007208
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2007209
    :sswitch_4
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2007210
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2007211
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2007212
    const/4 v2, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2007213
    const/4 v4, 0x3

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2007214
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2007215
    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-virtual {p0, p1, v4, v5}, LX/15i;->a(III)I

    move-result v7

    .line 2007216
    const/4 v4, 0x5

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2007217
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p3, v4, v0, v5}, LX/186;->a(III)V

    .line 2007218
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2007219
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2007220
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 2007221
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v7, v1}, LX/186;->a(III)V

    .line 2007222
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2007223
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v4

    .line 2007224
    if-nez v4, :cond_2

    const/4 v2, 0x0

    .line 2007225
    :goto_2
    if-ge v3, v4, :cond_3

    .line 2007226
    invoke-virtual {p0, v0, v3}, LX/15i;->q(II)I

    move-result v5

    .line 2007227
    invoke-static {p0, v5, v1, p3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v5

    aput v5, v2, v3

    .line 2007228
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2007229
    :cond_2
    new-array v2, v4, [I

    goto :goto_2

    .line 2007230
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p3, v2, v3}, LX/186;->a([IZ)I

    move-result v2

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x6c4b15a -> :sswitch_4
        0x3aaa24f8 -> :sswitch_1
        0x4e067bed -> :sswitch_2
        0x543d3c04 -> :sswitch_3
        0x5aeb5d93 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2007166
    if-nez p0, :cond_0

    move v0, v1

    .line 2007167
    :goto_0
    return v0

    .line 2007168
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 2007169
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 2007170
    :goto_1
    if-ge v1, v2, :cond_2

    .line 2007171
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 2007172
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2007173
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 2007174
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 2007159
    const/4 v7, 0x0

    .line 2007160
    const/4 v1, 0x0

    .line 2007161
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 2007162
    invoke-static {v2, v3, v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2007163
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 2007164
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2007165
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2007158
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2007145
    sparse-switch p2, :sswitch_data_0

    .line 2007146
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2007147
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2007148
    const v1, 0x3aaa24f8

    .line 2007149
    if-eqz v0, :cond_0

    .line 2007150
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2007151
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2007152
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2007153
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2007154
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2007155
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2007156
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2007157
    const v1, 0x543d3c04

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x6c4b15a -> :sswitch_1
        0x3aaa24f8 -> :sswitch_1
        0x4e067bed -> :sswitch_2
        0x543d3c04 -> :sswitch_1
        0x5aeb5d93 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2007139
    if-eqz p1, :cond_0

    .line 2007140
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v1

    .line 2007141
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    .line 2007142
    if-eq v0, v1, :cond_0

    .line 2007143
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2007144
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2007138
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2007235
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2007236
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2007133
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2007134
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2007135
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a:LX/15i;

    .line 2007136
    iput p2, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->b:I

    .line 2007137
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2007132
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2007131
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2007128
    iget v0, p0, LX/1vt;->c:I

    .line 2007129
    move v0, v0

    .line 2007130
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2007125
    iget v0, p0, LX/1vt;->c:I

    .line 2007126
    move v0, v0

    .line 2007127
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2007122
    iget v0, p0, LX/1vt;->b:I

    .line 2007123
    move v0, v0

    .line 2007124
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2007119
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2007120
    move-object v0, v0

    .line 2007121
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2007110
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2007111
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2007112
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2007113
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2007114
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2007115
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2007116
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2007117
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2007118
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2007107
    iget v0, p0, LX/1vt;->c:I

    .line 2007108
    move v0, v0

    .line 2007109
    return v0
.end method
