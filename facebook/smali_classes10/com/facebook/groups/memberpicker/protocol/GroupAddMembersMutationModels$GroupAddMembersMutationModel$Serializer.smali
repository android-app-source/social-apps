.class public final Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2006421
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    new-instance v1, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2006422
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2006420
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2006355
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2006356
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v5, 0x6

    const/4 v4, 0x4

    .line 2006357
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2006358
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2006359
    if-eqz v2, :cond_1

    .line 2006360
    const-string v3, "added_users"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2006361
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2006362
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 2006363
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/DW6;->a(LX/15i;ILX/0nX;)V

    .line 2006364
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2006365
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2006366
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2006367
    if-eqz v2, :cond_3

    .line 2006368
    const-string v3, "already_added_users"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2006369
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2006370
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_2

    .line 2006371
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/DW7;->a(LX/15i;ILX/0nX;)V

    .line 2006372
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2006373
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2006374
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2006375
    if-eqz v2, :cond_5

    .line 2006376
    const-string v3, "already_invited_users"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2006377
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2006378
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_4

    .line 2006379
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/DW8;->a(LX/15i;ILX/0nX;)V

    .line 2006380
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2006381
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2006382
    :cond_5
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2006383
    if-eqz v2, :cond_6

    .line 2006384
    const-string v3, "client_mutation_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2006385
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2006386
    :cond_6
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2006387
    if-eqz v2, :cond_7

    .line 2006388
    const-string v2, "failed_emails"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2006389
    invoke-virtual {v1, v0, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2006390
    :cond_7
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2006391
    if-eqz v2, :cond_9

    .line 2006392
    const-string v3, "failed_users"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2006393
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2006394
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_8

    .line 2006395
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    invoke-static {v1, v4, p1}, LX/DW9;->a(LX/15i;ILX/0nX;)V

    .line 2006396
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 2006397
    :cond_8
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2006398
    :cond_9
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2006399
    if-eqz v2, :cond_a

    .line 2006400
    const-string v2, "invited_emails"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2006401
    invoke-virtual {v1, v0, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2006402
    :cond_a
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2006403
    if-eqz v2, :cond_c

    .line 2006404
    const-string v3, "invited_users"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2006405
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2006406
    const/4 v3, 0x0

    :goto_4
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_b

    .line 2006407
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    invoke-static {v1, v4, p1}, LX/DWA;->a(LX/15i;ILX/0nX;)V

    .line 2006408
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 2006409
    :cond_b
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2006410
    :cond_c
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2006411
    if-eqz v2, :cond_e

    .line 2006412
    const-string v3, "requested_users"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2006413
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2006414
    const/4 v3, 0x0

    :goto_5
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_d

    .line 2006415
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    invoke-static {v1, v4, p1}, LX/DWB;->a(LX/15i;ILX/0nX;)V

    .line 2006416
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 2006417
    :cond_d
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2006418
    :cond_e
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2006419
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2006354
    check-cast p1, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel$Serializer;->a(Lcom/facebook/groups/memberpicker/protocol/GroupAddMembersMutationModels$GroupAddMembersMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
