.class public final Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b69198d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2008835
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2008834
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2008832
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2008833
    return-void
.end method

.method private a()Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2008830
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;->e:Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;->e:Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    .line 2008831
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;->e:Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2008824
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2008825
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;->a()Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2008826
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2008827
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2008828
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2008829
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2008816
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2008817
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;->a()Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2008818
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;->a()Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    .line 2008819
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;->a()Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2008820
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;

    .line 2008821
    iput-object v0, v1, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;->e:Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel$GroupMembersModel;

    .line 2008822
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2008823
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2008815
    new-instance v0, LX/DWs;

    invoke-direct {v0, p1}, LX/DWs;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2008813
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2008814
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2008812
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2008809
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberpicker/protocol/MemberPickerDataModels$GroupFriendsMemberPickerDataModel;-><init>()V

    .line 2008810
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2008811
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2008807
    const v0, -0x61cab010

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2008808
    const v0, 0x41e065f

    return v0
.end method
