.class public final Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x669a76aa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2007865
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2007864
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2007862
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2007863
    return-void
.end method

.method private j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2007860
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->e:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->e:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    .line 2007861
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->e:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2007866
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2007867
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2007868
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2007869
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2007870
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2007871
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2007852
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2007853
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2007854
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    .line 2007855
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2007856
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;

    .line 2007857
    iput-object v0, v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->e:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    .line 2007858
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2007859
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2007851
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2007848
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersCollectionQueryModels$GroupSuggestedMembersCollectionQueryModel$SuggestedMembersModel$EdgesModel;-><init>()V

    .line 2007849
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2007850
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2007847
    const v0, 0x2da10c10

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2007846
    const v0, 0x35a38452

    return v0
.end method
