.class public final Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x36375d15
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2006861
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2006860
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2006833
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2006834
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2006850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2006851
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2006852
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2006853
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2006854
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2006855
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2006856
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2006857
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2006858
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2006859
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2006842
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2006843
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2006844
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    .line 2006845
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2006846
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;

    .line 2006847
    iput-object v0, v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    .line 2006848
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2006849
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006840
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->e:Ljava/lang/String;

    .line 2006841
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2006837
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;-><init>()V

    .line 2006838
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2006839
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2006836
    const v0, 0x3d76161b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2006835
    const v0, 0x28ac430e

    return v0
.end method

.method public final j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006831
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    .line 2006832
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->f:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2006829
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->g:Ljava/lang/String;

    .line 2006830
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->g:Ljava/lang/String;

    return-object v0
.end method
