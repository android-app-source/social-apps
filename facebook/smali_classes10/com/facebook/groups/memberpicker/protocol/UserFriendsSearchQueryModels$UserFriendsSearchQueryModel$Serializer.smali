.class public final Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2009737
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;

    new-instance v1, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2009738
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2009739
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2009740
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2009741
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2009742
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2009743
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2009744
    if-eqz v2, :cond_0

    .line 2009745
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2009746
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2009747
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2009748
    if-eqz v2, :cond_1

    .line 2009749
    const-string p0, "friends"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2009750
    invoke-static {v1, v2, p1, p2}, LX/DXF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2009751
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2009752
    if-eqz v2, :cond_2

    .line 2009753
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2009754
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2009755
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2009756
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2009757
    check-cast p1, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel$Serializer;->a(Lcom/facebook/groups/memberpicker/protocol/UserFriendsSearchQueryModels$UserFriendsSearchQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
