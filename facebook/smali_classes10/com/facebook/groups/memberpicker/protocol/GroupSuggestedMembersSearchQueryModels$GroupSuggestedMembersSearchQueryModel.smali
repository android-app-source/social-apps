.class public final Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x62371e3a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2008460
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2008497
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2008495
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2008496
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2008492
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2008493
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2008494
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2008490
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->f:Ljava/lang/String;

    .line 2008491
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2008480
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2008481
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2008482
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2008483
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2008484
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2008485
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2008486
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2008487
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2008488
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2008489
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2008472
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2008473
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2008474
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    .line 2008475
    invoke-virtual {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2008476
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;

    .line 2008477
    iput-object v0, v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->g:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    .line 2008478
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2008479
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2008471
    new-instance v0, LX/DWk;

    invoke-direct {v0, p1}, LX/DWk;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2008470
    invoke-direct {p0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2008468
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2008469
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2008459
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2008465
    new-instance v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;-><init>()V

    .line 2008466
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2008467
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2008464
    const v0, -0xd7fa12a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2008463
    const v0, 0x252222

    return v0
.end method

.method public final j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSuggestedMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2008461
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->g:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    iput-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->g:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    .line 2008462
    iget-object v0, p0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel;->g:Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMembersSearchQueryModels$GroupSuggestedMembersSearchQueryModel$SuggestedMembersModel;

    return-object v0
.end method
