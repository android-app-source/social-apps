.class public final Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2007249
    const-class v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel;

    new-instance v1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2007250
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2007251
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 2007252
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2007253
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v6, 0x4

    const-wide/16 v4, 0x0

    .line 2007254
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2007255
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2007256
    if-eqz v2, :cond_0

    .line 2007257
    const-string v3, "bylines"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007258
    invoke-static {v1, v2, p1, p2}, LX/DWT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2007259
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 2007260
    cmpl-double v4, v2, v4

    if-eqz v4, :cond_1

    .line 2007261
    const-string v4, "communicationRank"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007262
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(D)V

    .line 2007263
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2007264
    if-eqz v2, :cond_2

    .line 2007265
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007266
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2007267
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2007268
    if-eqz v2, :cond_3

    .line 2007269
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007270
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2007271
    :cond_3
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 2007272
    if-eqz v2, :cond_4

    .line 2007273
    const-string v2, "name_search_tokens"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007274
    invoke-virtual {v1, v0, v6}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2007275
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2007276
    if-eqz v2, :cond_5

    .line 2007277
    const-string v3, "profilePicture50"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007278
    invoke-static {v1, v2, p1}, LX/DWV;->a(LX/15i;ILX/0nX;)V

    .line 2007279
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2007280
    if-eqz v2, :cond_6

    .line 2007281
    const-string v3, "structured_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2007282
    invoke-static {v1, v2, p1, p2}, LX/DWS;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2007283
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2007284
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2007285
    check-cast p1, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel$Serializer;->a(Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedMemberDataModels$FBTaggableUserFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
