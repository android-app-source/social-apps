.class public final Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2016047
    const-class v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;

    new-instance v1, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2016048
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2016049
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 2016050
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2016051
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x7

    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v4, 0x4

    .line 2016052
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2016053
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2016054
    if-eqz v2, :cond_0

    .line 2016055
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016056
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2016057
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2016058
    if-eqz v2, :cond_1

    .line 2016059
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016060
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2016061
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2016062
    if-eqz v2, :cond_2

    .line 2016063
    const-string v3, "possible_push_subscription_levels"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016064
    invoke-static {v1, v2, p1, p2}, LX/Dak;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2016065
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2016066
    if-eqz v2, :cond_3

    .line 2016067
    const-string v3, "possible_subscription_levels"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016068
    invoke-static {v1, v2, p1, p2}, LX/Dam;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2016069
    :cond_3
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2016070
    if-eqz v2, :cond_4

    .line 2016071
    const-string v2, "viewer_admin_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016072
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2016073
    :cond_4
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 2016074
    if-eqz v2, :cond_5

    .line 2016075
    const-string v2, "viewer_push_subscription_level"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016076
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2016077
    :cond_5
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 2016078
    if-eqz v2, :cond_6

    .line 2016079
    const-string v2, "viewer_request_to_join_subscription_level"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016080
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2016081
    :cond_6
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2016082
    if-eqz v2, :cond_7

    .line 2016083
    const-string v2, "viewer_subscription_level"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2016084
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2016085
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2016086
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2016087
    check-cast p1, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$Serializer;->a(Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
