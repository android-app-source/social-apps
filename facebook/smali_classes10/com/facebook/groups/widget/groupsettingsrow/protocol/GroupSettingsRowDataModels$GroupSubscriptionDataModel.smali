.class public final Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/DQn;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32cb0450
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2016190
    const-class v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2016189
    const-class v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2016126
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2016127
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V
    .locals 4

    .prologue
    .line 2016182
    iput-object p1, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 2016183
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2016184
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2016185
    if-eqz v0, :cond_0

    .line 2016186
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x5

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2016187
    :cond_0
    return-void

    .line 2016188
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V
    .locals 4

    .prologue
    .line 2016175
    iput-object p1, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 2016176
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2016177
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2016178
    if-eqz v0, :cond_0

    .line 2016179
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x7

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2016180
    :cond_0
    return-void

    .line 2016181
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2016169
    iput-object p1, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->f:Ljava/lang/String;

    .line 2016170
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2016171
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2016172
    if-eqz v0, :cond_0

    .line 2016173
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2016174
    :cond_0
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016167
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->e:Ljava/lang/String;

    .line 2016168
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016165
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->g:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->g:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    .line 2016166
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->g:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016163
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->h:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->h:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    .line 2016164
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->h:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 2016143
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2016144
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2016145
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2016146
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->l()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2016147
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->m()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2016148
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 2016149
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2016150
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->ky_()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 2016151
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2016152
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2016153
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 2016154
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2016155
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2016156
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2016157
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2016158
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2016159
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2016160
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2016161
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2016162
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2016130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2016131
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->l()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2016132
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->l()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    .line 2016133
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->l()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2016134
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;

    .line 2016135
    iput-object v0, v1, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->g:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    .line 2016136
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->m()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2016137
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->m()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    .line 2016138
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->m()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2016139
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;

    .line 2016140
    iput-object v0, v1, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->h:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    .line 2016141
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2016142
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2016129
    new-instance v0, LX/Dah;

    invoke-direct {v0, p1}, LX/Dah;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016128
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2016090
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2016091
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2016092
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2016093
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2016094
    :goto_0
    return-void

    .line 2016095
    :cond_0
    const-string v0, "viewer_push_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2016096
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2016097
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2016098
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2016099
    :cond_1
    const-string v0, "viewer_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2016100
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2016101
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2016102
    const/4 v0, 0x7

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2016103
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2016104
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2016105
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->a(Ljava/lang/String;)V

    .line 2016106
    :cond_0
    :goto_0
    return-void

    .line 2016107
    :cond_1
    const-string v0, "viewer_push_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2016108
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V

    goto :goto_0

    .line 2016109
    :cond_2
    const-string v0, "viewer_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2016110
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2016111
    new-instance v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;

    invoke-direct {v0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;-><init>()V

    .line 2016112
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2016113
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016114
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->f:Ljava/lang/String;

    .line 2016115
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016116
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->l()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2016117
    const v0, 0x1c67c4cd

    return v0
.end method

.method public final synthetic e()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016118
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->m()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2016119
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016120
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 2016121
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    return-object v0
.end method

.method public final kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016088
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 2016089
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    return-object v0
.end method

.method public final ky_()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016122
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 2016123
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->k:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016124
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2016125
    iget-object v0, p0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    return-object v0
.end method
