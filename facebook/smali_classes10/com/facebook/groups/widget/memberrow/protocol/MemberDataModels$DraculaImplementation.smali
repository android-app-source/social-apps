.class public final Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2016444
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2016445
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2016446
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2016447
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2016448
    if-nez p1, :cond_0

    .line 2016449
    :goto_0
    return v0

    .line 2016450
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2016451
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2016452
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 2016453
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2016454
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 2016455
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2016456
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2016457
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2016458
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2016459
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2016460
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7c5614aa -> :sswitch_1
        0x2d1cae2e -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2016465
    new-instance v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2016461
    sparse-switch p0, :sswitch_data_0

    .line 2016462
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2016463
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7c5614aa -> :sswitch_0
        0x2d1cae2e -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2016464
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2016437
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->b(I)V

    .line 2016438
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2016439
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2016440
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2016441
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->a:LX/15i;

    .line 2016442
    iput p2, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->b:I

    .line 2016443
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2016411
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2016412
    new-instance v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2016413
    iget v0, p0, LX/1vt;->c:I

    .line 2016414
    move v0, v0

    .line 2016415
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2016416
    iget v0, p0, LX/1vt;->c:I

    .line 2016417
    move v0, v0

    .line 2016418
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2016419
    iget v0, p0, LX/1vt;->b:I

    .line 2016420
    move v0, v0

    .line 2016421
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016422
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2016423
    move-object v0, v0

    .line 2016424
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2016428
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2016429
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2016430
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2016431
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2016432
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2016433
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2016434
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2016435
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2016436
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2016425
    iget v0, p0, LX/1vt;->c:I

    .line 2016426
    move v0, v0

    .line 2016427
    return v0
.end method
