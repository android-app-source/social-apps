.class public final Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/DUV;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x746af455
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2016555
    const-class v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2016554
    const-class v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2016552
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2016553
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2016535
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2016536
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2016537
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2016538
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->j()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x2d1cae2e

    invoke-static {v3, v2, v4}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2016539
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->kA_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2016540
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->k()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, -0x7c5614aa

    invoke-static {v5, v4, v6}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2016541
    const/16 v5, 0x8

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2016542
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2016543
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2016544
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2016545
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2016546
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2016547
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2016548
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2016549
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2016550
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2016551
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2016519
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2016520
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2016521
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2d1cae2e

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2016522
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2016523
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    .line 2016524
    iput v3, v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->j:I

    move-object v1, v0

    .line 2016525
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2016526
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7c5614aa

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2016527
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2016528
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    .line 2016529
    iput v3, v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->l:I

    move-object v1, v0

    .line 2016530
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2016531
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2016532
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2016533
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2016534
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2016518
    new-instance v0, LX/Das;

    invoke-direct {v0, p1}, LX/Das;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016517
    invoke-virtual {p0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2016510
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2016511
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->g:Z

    .line 2016512
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->h:Z

    .line 2016513
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->i:Z

    .line 2016514
    const/4 v0, 0x5

    const v1, 0x2d1cae2e

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->j:I

    .line 2016515
    const/4 v0, 0x7

    const v1, -0x7c5614aa

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->l:I

    .line 2016516
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2016508
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2016509
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2016507
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016556
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2016557
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2016558
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2016504
    new-instance v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    invoke-direct {v0}, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;-><init>()V

    .line 2016505
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2016506
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016502
    iget-object v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->f:Ljava/lang/String;

    .line 2016503
    iget-object v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2016500
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2016501
    iget-boolean v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->g:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2016499
    const v0, -0x3cf9af38

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2016497
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2016498
    iget-boolean v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->h:Z

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2016496
    const v0, 0x50c72189

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getLastActiveMessagesStatus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016494
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2016495
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016492
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2016493
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final kA_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2016490
    iget-object v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->k:Ljava/lang/String;

    .line 2016491
    iget-object v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final kz_()Z
    .locals 2

    .prologue
    .line 2016488
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2016489
    iget-boolean v0, p0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;->i:Z

    return v0
.end method
