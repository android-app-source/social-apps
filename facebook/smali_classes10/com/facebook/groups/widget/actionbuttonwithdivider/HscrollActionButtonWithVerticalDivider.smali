.class public Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;
.super Lcom/facebook/fbui/glyph/GlyphButton;
.source ""


# instance fields
.field private b:I

.field private c:I

.field private d:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2014908
    invoke-direct {p0, p1}, Lcom/facebook/fbui/glyph/GlyphButton;-><init>(Landroid/content/Context;)V

    .line 2014909
    invoke-direct {p0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->a()V

    .line 2014910
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2014905
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/glyph/GlyphButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2014906
    invoke-direct {p0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->a()V

    .line 2014907
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2014881
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/glyph/GlyphButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2014882
    invoke-direct {p0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->a()V

    .line 2014883
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2014899
    invoke-virtual {p0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2014900
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->d:Landroid/graphics/Paint;

    .line 2014901
    iget-object v1, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->d:Landroid/graphics/Paint;

    const v2, 0x7f0a0115

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2014902
    const v1, 0x7f0b007f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->b:I

    .line 2014903
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->c:I

    .line 2014904
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2014894
    invoke-super {p0, p1}, Lcom/facebook/fbui/glyph/GlyphButton;->onDraw(Landroid/graphics/Canvas;)V

    .line 2014895
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->b:I

    sub-int/2addr v0, v1

    .line 2014896
    :goto_0
    int-to-float v1, v0

    iget v2, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->c:I

    int-to-float v2, v2

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->getHeight()I

    move-result v0

    iget v4, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->c:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2014897
    return-void

    .line 2014898
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDividerColor(I)V
    .locals 1

    .prologue
    .line 2014891
    iget-object v0, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2014892
    invoke-virtual {p0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->invalidate()V

    .line 2014893
    return-void
.end method

.method public setDividerMarginPx(I)V
    .locals 0

    .prologue
    .line 2014888
    iput p1, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->c:I

    .line 2014889
    invoke-virtual {p0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->invalidate()V

    .line 2014890
    return-void
.end method

.method public setDividerThicknessPx(I)V
    .locals 2

    .prologue
    .line 2014884
    iput p1, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->b:I

    .line 2014885
    iget-object v0, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2014886
    invoke-virtual {p0}, Lcom/facebook/groups/widget/actionbuttonwithdivider/HscrollActionButtonWithVerticalDivider;->invalidate()V

    .line 2014887
    return-void
.end method
