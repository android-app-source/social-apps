.class public Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2015182
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 2015183
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->a()V

    .line 2015184
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2015185
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2015186
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->a()V

    .line 2015187
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2015188
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2015189
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->a()V

    .line 2015190
    return-void
.end method

.method private a(Lcom/facebook/events/model/Event;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2015191
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->ap()Ljava/lang/String;

    move-result-object v0

    .line 2015192
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2015193
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f083070

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 2015194
    :cond_0
    :goto_0
    return-object p2

    .line 2015195
    :cond_1
    iget v0, p1, Lcom/facebook/events/model/Event;->ae:I

    move v0, v0

    .line 2015196
    if-lez v0, :cond_3

    .line 2015197
    iget v0, p1, Lcom/facebook/events/model/Event;->ae:I

    move v0, v0

    .line 2015198
    if-ne v0, v5, :cond_2

    .line 2015199
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083078

    new-array v2, v5, [Ljava/lang/Object;

    .line 2015200
    iget-object v3, p1, Lcom/facebook/events/model/Event;->ad:Ljava/lang/String;

    move-object v3, v3

    .line 2015201
    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 2015202
    :cond_2
    iget v0, p1, Lcom/facebook/events/model/Event;->ae:I

    move v0, v0

    .line 2015203
    add-int/lit8 v0, v0, -0x1

    .line 2015204
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0154

    new-array v3, v3, [Ljava/lang/Object;

    .line 2015205
    iget-object v4, p1, Lcom/facebook/events/model/Event;->ad:Ljava/lang/String;

    move-object v4, v4

    .line 2015206
    aput-object v4, v3, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 2015207
    :cond_3
    iget v0, p1, Lcom/facebook/events/model/Event;->ag:I

    move v0, v0

    .line 2015208
    if-lez v0, :cond_5

    .line 2015209
    iget v0, p1, Lcom/facebook/events/model/Event;->ag:I

    move v0, v0

    .line 2015210
    if-ne v0, v5, :cond_4

    .line 2015211
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083077

    new-array v2, v5, [Ljava/lang/Object;

    .line 2015212
    iget-object v3, p1, Lcom/facebook/events/model/Event;->af:Ljava/lang/String;

    move-object v3, v3

    .line 2015213
    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 2015214
    :cond_4
    iget v0, p1, Lcom/facebook/events/model/Event;->ag:I

    move v0, v0

    .line 2015215
    add-int/lit8 v0, v0, -0x1

    .line 2015216
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0153

    new-array v3, v3, [Ljava/lang/Object;

    .line 2015217
    iget-object v4, p1, Lcom/facebook/events/model/Event;->af:Ljava/lang/String;

    move-object v4, v4

    .line 2015218
    aput-object v4, v3, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 2015219
    :cond_5
    iget-object v0, p1, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    move-object v0, v0

    .line 2015220
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2015221
    invoke-direct {p0, p1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->b(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 2015222
    :cond_6
    if-nez p2, :cond_0

    .line 2015223
    const-string p2, ""

    goto/16 :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2015224
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setTextColor(I)V

    .line 2015225
    return-void
.end method

.method private b(Lcom/facebook/events/model/Event;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2015226
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083076

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 2015227
    iget-object v4, p1, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    move-object v4, v4

    .line 2015228
    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 2015229
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    .line 2015230
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->INVITED:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v1, :cond_3

    .line 2015231
    invoke-virtual {p0, v4}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    .line 2015232
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->ap()Ljava/lang/String;

    move-result-object v0

    .line 2015233
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2015234
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f083070

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setText(Ljava/lang/CharSequence;)V

    .line 2015235
    :goto_0
    return-void

    .line 2015236
    :cond_0
    iget-object v0, p1, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    move-object v0, v0

    .line 2015237
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2015238
    invoke-direct {p0, p1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->b(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2015239
    :cond_1
    invoke-direct {p0, p1, v3}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2015240
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2015241
    invoke-virtual {p0, v2}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    goto :goto_0

    .line 2015242
    :cond_2
    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2015243
    :cond_3
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v1, :cond_4

    .line 2015244
    invoke-virtual {p0, v2}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    goto :goto_0

    .line 2015245
    :cond_4
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v1, :cond_5

    .line 2015246
    invoke-virtual {p0, v4}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    .line 2015247
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f083073

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2015248
    :cond_5
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v1, :cond_6

    .line 2015249
    invoke-virtual {p0, v4}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    .line 2015250
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f083071

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2015251
    :cond_6
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->HOST:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne v0, v1, :cond_7

    .line 2015252
    invoke-virtual {p0, v4}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    .line 2015253
    const v0, 0x7f08306f

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setText(I)V

    goto :goto_0

    .line 2015254
    :cond_7
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->E:Z

    move v0, v0

    .line 2015255
    if-eqz v0, :cond_8

    .line 2015256
    invoke-virtual {p0, v4}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    .line 2015257
    const v0, 0x7f083072

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setText(I)V

    goto :goto_0

    .line 2015258
    :cond_8
    invoke-direct {p0, p1, v3}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->a(Lcom/facebook/events/model/Event;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2015259
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2015260
    invoke-virtual {p0, v2}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2015261
    :cond_9
    invoke-virtual {p0, v4}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setVisibility(I)V

    .line 2015262
    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventSocialContextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
