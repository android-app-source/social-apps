.class public Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/BiT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DMP;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:[Lcom/facebook/resources/ui/FbTextView;

.field private g:Landroid/view/View$OnClickListener;

.field private h:Landroid/view/View$OnClickListener;

.field private i:Landroid/view/View$OnClickListener;

.field private j:Landroid/view/View$OnClickListener;

.field private k:Landroid/view/View$OnClickListener;

.field private l:Landroid/view/View$OnClickListener;

.field public m:Lcom/facebook/events/model/Event;

.field private n:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2015072
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2015073
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a()V

    .line 2015074
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2015146
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2015147
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a()V

    .line 2015148
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2015143
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2015144
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a()V

    .line 2015145
    return-void
.end method

.method private a(LX/BiU;)Landroid/view/View$OnClickListener;
    .locals 2

    .prologue
    .line 2015135
    sget-object v0, LX/DaV;->a:[I

    invoke-virtual {p1}, LX/BiU;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2015136
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2015137
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->g:Landroid/view/View$OnClickListener;

    goto :goto_0

    .line 2015138
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->h:Landroid/view/View$OnClickListener;

    goto :goto_0

    .line 2015139
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->i:Landroid/view/View$OnClickListener;

    goto :goto_0

    .line 2015140
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->j:Landroid/view/View$OnClickListener;

    goto :goto_0

    .line 2015141
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->k:Landroid/view/View$OnClickListener;

    goto :goto_0

    .line 2015142
    :pswitch_5
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->l:Landroid/view/View$OnClickListener;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2015134
    new-instance v0, LX/DaS;

    invoke-direct {v0, p0, p1}, LX/DaS;-><init>(Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2015133
    new-instance v0, LX/DaT;

    invoke-direct {v0, p0, p1}, LX/DaT;-><init>(Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2015119
    const-class v0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    invoke-static {v0, p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2015120
    const v0, 0x7f030805

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2015121
    const v0, 0x7f0d1524

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->n:Landroid/view/View;

    .line 2015122
    const v0, 0x7f0d1525

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2015123
    const v0, 0x7f0d1526

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2015124
    const v0, 0x7f0d1527

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2015125
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->c:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->d:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->e:Lcom/facebook/resources/ui/FbTextView;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->f:[Lcom/facebook/resources/ui/FbTextView;

    .line 2015126
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->g:Landroid/view/View$OnClickListener;

    .line 2015127
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->h:Landroid/view/View$OnClickListener;

    .line 2015128
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->NOT_GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->i:Landroid/view/View$OnClickListener;

    .line 2015129
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->j:Landroid/view/View$OnClickListener;

    .line 2015130
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->k:Landroid/view/View$OnClickListener;

    .line 2015131
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->l:Landroid/view/View$OnClickListener;

    .line 2015132
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;

    invoke-static {v0}, LX/BiT;->a(LX/0QB;)LX/BiT;

    move-result-object v0

    check-cast v0, LX/BiT;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a:LX/BiT;

    return-void
.end method

.method public static b(Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;)V
    .locals 4

    .prologue
    .line 2015114
    new-instance v0, LX/2qr;

    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->n:Landroid/view/View;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/2qr;-><init>(Landroid/view/View;I)V

    .line 2015115
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, LX/2qr;->setDuration(J)V

    .line 2015116
    new-instance v1, LX/DaU;

    invoke-direct {v1, p0}, LX/DaU;-><init>(Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;)V

    invoke-virtual {v0, v1}, LX/2qr;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2015117
    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2015118
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2015110
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2015111
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->clearAnimation()V

    .line 2015112
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->c(I)V

    .line 2015113
    return-void
.end method

.method private c(I)V
    .locals 1

    .prologue
    .line 2015107
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2015108
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->requestLayout()V

    .line 2015109
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;LX/DMP;)V
    .locals 5

    .prologue
    .line 2015079
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->c()V

    .line 2015080
    iput-object p1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->m:Lcom/facebook/events/model/Event;

    .line 2015081
    invoke-static {p1}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/events/model/Event;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2015082
    const/4 v0, 0x1

    move v0, v0

    .line 2015083
    if-eqz v0, :cond_0

    .line 2015084
    invoke-static {}, LX/BiT;->b()LX/0Px;

    move-result-object v2

    .line 2015085
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->f:[Lcom/facebook/resources/ui/FbTextView;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 2015086
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->f:[Lcom/facebook/resources/ui/FbTextView;

    aget-object v3, v0, v1

    .line 2015087
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BiU;

    .line 2015088
    sget-object v4, LX/DBY;->a:[I

    invoke-virtual {v0}, LX/BiU;->ordinal()I

    move-result p1

    aget v4, v4, p1

    packed-switch v4, :pswitch_data_0

    .line 2015089
    const/4 v4, 0x0

    :goto_1
    move v4, v4

    .line 2015090
    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2015091
    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->a(LX/BiU;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2015092
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2015093
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->c:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f08306c

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2015094
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->c:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2015095
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->d:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f08306d

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2015096
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2015097
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->e:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f08306e

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2015098
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2015099
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->b:LX/DMP;

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 2015100
    iput-object p2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->b:LX/DMP;

    .line 2015101
    :cond_2
    return-void

    .line 2015102
    :pswitch_0
    const v4, 0x7f08129b

    goto :goto_1

    .line 2015103
    :pswitch_1
    const v4, 0x7f08129c

    goto :goto_1

    .line 2015104
    :pswitch_2
    const v4, 0x7f08129d

    goto :goto_1

    .line 2015105
    :pswitch_3
    const v4, 0x7f08129e

    goto :goto_1

    .line 2015106
    :pswitch_4
    const v4, 0x7f0812a0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setHorizontalMargin(I)V
    .locals 3

    .prologue
    .line 2015075
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->n:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2015076
    :goto_0
    return-void

    .line 2015077
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusButtonsView;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2015078
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, p1, v1, p1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_0
.end method
