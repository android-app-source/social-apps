.class public Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/1aX;

.field private f:Landroid/net/Uri;

.field private g:Ljava/lang/String;

.field private h:Landroid/text/style/MetricAffectingSpan;

.field private i:Landroid/text/style/MetricAffectingSpan;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2014952
    const-class v0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;

    const-string v1, "group_events"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2014959
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 2014960
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a()V

    .line 2014961
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2014956
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2014957
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a()V

    .line 2014958
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2014953
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2014954
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a()V

    .line 2014955
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2014911
    const-class v0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;

    invoke-static {v0, p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2014912
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a0856

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2014913
    new-instance v1, LX/1Uo;

    iget-object v2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->c:Landroid/content/res/Resources;

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v1, v0}, LX/1Uo;->f(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v0

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2014914
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->e:LX/1aX;

    .line 2014915
    invoke-virtual {v0}, LX/1af;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V

    .line 2014916
    return-void
.end method

.method private static a(Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;LX/6RZ;LX/0Or;Landroid/content/res/Resources;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;",
            "LX/6RZ;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2014951
    iput-object p1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a:LX/6RZ;

    iput-object p2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->c:Landroid/content/res/Resources;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;

    invoke-static {v1}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v0

    check-cast v0, LX/6RZ;

    const/16 v2, 0x509

    invoke-static {v1, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a(Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;LX/6RZ;LX/0Or;Landroid/content/res/Resources;)V

    return-void
.end method

.method private setBackgroundWithPadding(Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 2014962
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->getPaddingLeft()I

    move-result v0

    .line 2014963
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->getPaddingTop()I

    move-result v1

    .line 2014964
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->getPaddingRight()I

    move-result v2

    .line 2014965
    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->getPaddingBottom()I

    move-result v3

    .line 2014966
    invoke-virtual {p0, p1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2014967
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->setPadding(IIII)V

    .line 2014968
    return-void
.end method

.method private setProfilePictureUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 2014946
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->f:Landroid/net/Uri;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2014947
    :goto_0
    return-void

    .line 2014948
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->f:Landroid/net/Uri;

    .line 2014949
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2014950
    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->e:LX/1aX;

    invoke-virtual {v1, v0}, LX/1aX;->a(LX/1aZ;)V

    goto :goto_0
.end method

.method private setStartDate(Ljava/util/Date;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v6, 0x11

    .line 2014934
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a:LX/6RZ;

    invoke-virtual {v0, p1}, LX/6RZ;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2014935
    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->a:LX/6RZ;

    invoke-virtual {v1, p1}, LX/6RZ;->d(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 2014936
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2014937
    iget-object v2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2014938
    :goto_0
    return-void

    .line 2014939
    :cond_0
    iput-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->g:Ljava/lang/String;

    .line 2014940
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0adf

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->i:Landroid/text/style/MetricAffectingSpan;

    .line 2014941
    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0ade

    invoke-direct {v2, v3, v4}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->h:Landroid/text/style/MetricAffectingSpan;

    .line 2014942
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2014943
    iget-object v3, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->h:Landroid/text/style/MetricAffectingSpan;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2014944
    iget-object v3, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->i:Landroid/text/style/MetricAffectingSpan;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v3, v0, v1, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2014945
    invoke-virtual {p0, v2}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 1

    .prologue
    .line 2014930
    iget-object v0, p1, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    move-object v0, v0

    .line 2014931
    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->setProfilePictureUri(Landroid/net/Uri;)V

    .line 2014932
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->setStartDate(Ljava/util/Date;)V

    .line 2014933
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x112fcd4a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2014927
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onAttachedToWindow()V

    .line 2014928
    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->e:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 2014929
    const/16 v1, 0x2d

    const v2, 0x3a70df35

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x43063804

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2014924
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onDetachedFromWindow()V

    .line 2014925
    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->e:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 2014926
    const/16 v1, 0x2d

    const v2, -0x61a10216

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2014921
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onFinishTemporaryDetach()V

    .line 2014922
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2014923
    return-void
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2014918
    invoke-super {p0}, Lcom/facebook/widget/text/BetterTextView;->onStartTemporaryDetach()V

    .line 2014919
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2014920
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 2014917
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventProfilePictureView;->e:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
