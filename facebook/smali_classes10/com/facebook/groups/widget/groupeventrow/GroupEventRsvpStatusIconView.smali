.class public Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;
.super Landroid/widget/ImageView;
.source ""

# interfaces
.implements LX/Bni;


# instance fields
.field public a:LX/38v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/DMP;

.field private c:Lcom/facebook/events/model/Event;

.field private d:LX/Bne;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2015179
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 2015180
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->a()V

    .line 2015181
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2015176
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2015177
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->a()V

    .line 2015178
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2015173
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2015174
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->a()V

    .line 2015175
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2015170
    const-class v0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    invoke-static {v0, p0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2015171
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->a:LX/38v;

    invoke-virtual {v0, p0}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->d:LX/Bne;

    .line 2015172
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;

    const-class v1, LX/38v;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/38v;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->a:LX/38v;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;LX/DMP;)V
    .locals 4

    .prologue
    .line 2015155
    iput-object p1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->c:Lcom/facebook/events/model/Event;

    .line 2015156
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->b:LX/DMP;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 2015157
    iput-object p2, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->b:LX/DMP;

    .line 2015158
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->d:LX/Bne;

    .line 2015159
    iget-object v1, p1, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v1, v1

    .line 2015160
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    .line 2015161
    iget-object v3, p1, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v3, v3

    .line 2015162
    invoke-virtual {v0, v1, v2, v3}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v0

    .line 2015163
    if-nez v0, :cond_1

    .line 2015164
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setVisibility(I)V

    .line 2015165
    :goto_0
    return-void

    .line 2015166
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setVisibility(I)V

    .line 2015167
    iget-object v1, v0, LX/BnW;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2015168
    iget-object v1, v0, LX/BnW;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2015169
    iget-object v0, v0, LX/BnW;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 2

    .prologue
    .line 2015152
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->b:LX/DMP;

    if-eqz v0, :cond_0

    .line 2015153
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->b:LX/DMP;

    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->c:Lcom/facebook/events/model/Event;

    invoke-interface {v0, v1, p2}, LX/DMP;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    .line 2015154
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 2

    .prologue
    .line 2015149
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->b:LX/DMP;

    if-eqz v0, :cond_0

    .line 2015150
    iget-object v0, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->b:LX/DMP;

    iget-object v1, p0, Lcom/facebook/groups/widget/groupeventrow/GroupEventRsvpStatusIconView;->c:Lcom/facebook/events/model/Event;

    invoke-interface {v0, v1, p2}, LX/DMP;->a(Lcom/facebook/events/model/Event;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    .line 2015151
    :cond_0
    return-void
.end method
