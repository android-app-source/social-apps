.class public Lcom/facebook/groups/widget/remotepogview/RemotePogView;
.super Lcom/facebook/drawee/view/DraweeView;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/drawee/view/DraweeView",
        "<",
        "LX/1af;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:I

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:Z

.field private f:Landroid/graphics/Bitmap;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2016873
    const-class v0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;

    const-string v1, "group_info"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2016870
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;)V

    .line 2016871
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a(Landroid/util/AttributeSet;II)V

    .line 2016872
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    .prologue
    .line 2016867
    invoke-direct {p0, p1}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;)V

    .line 2016868
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2, p3}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a(Landroid/util/AttributeSet;II)V

    .line 2016869
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2016865
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2016866
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2016862
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/view/DraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2016863
    invoke-direct {p0, p2, v0, v0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a(Landroid/util/AttributeSet;II)V

    .line 2016864
    return-void
.end method

.method private a(Landroid/util/AttributeSet;II)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2016835
    const-class v1, Lcom/facebook/groups/widget/remotepogview/RemotePogView;

    invoke-static {v1, p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2016836
    if-eqz p1, :cond_5

    .line 2016837
    invoke-virtual {p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/03r;->RemotePogAttrs:[I

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 2016838
    if-eqz v2, :cond_5

    .line 2016839
    const/16 v1, 0x0

    invoke-virtual {v2, v1, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2016840
    invoke-virtual {p0, v1}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->setPogSize(I)V

    .line 2016841
    const/16 v1, 0x3

    invoke-virtual {v2, v1, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 2016842
    if-eqz v1, :cond_0

    .line 2016843
    invoke-virtual {p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->d:Landroid/graphics/drawable/Drawable;

    .line 2016844
    :cond_0
    const/16 v1, 0x1

    invoke-virtual {v2, v1, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 2016845
    if-lez v1, :cond_1

    .line 2016846
    invoke-virtual {p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a0841

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2016847
    const/16 v3, 0x2

    invoke-virtual {v2, v3, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 2016848
    :cond_1
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    move v5, v1

    move v1, v0

    move v0, v5

    .line 2016849
    :goto_0
    iget-object v2, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->d:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_2

    .line 2016850
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a004f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v2, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->d:Landroid/graphics/drawable/Drawable;

    .line 2016851
    :cond_2
    if-eqz p2, :cond_3

    .line 2016852
    :goto_1
    if-eqz p3, :cond_4

    .line 2016853
    :goto_2
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2016854
    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v1

    int-to-float v2, p3

    invoke-virtual {v1, p2, v2}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v1

    .line 2016855
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 2016856
    move-object v0, v0

    .line 2016857
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2016858
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2016859
    return-void

    :cond_3
    move p2, v1

    .line 2016860
    goto :goto_1

    :cond_4
    move p3, v0

    .line 2016861
    goto :goto_2

    :cond_5
    move v1, v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a:LX/1Ad;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2016830
    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2016831
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2016832
    iget-object v0, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a:LX/1Ad;

    sget-object v1, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2016833
    invoke-virtual {p0, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2016834
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2016827
    iput-boolean p2, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->e:Z

    .line 2016828
    invoke-direct {p0, p1}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->a(Ljava/lang/String;)V

    .line 2016829
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2016818
    invoke-super {p0, p1}, Lcom/facebook/drawee/view/DraweeView;->onDraw(Landroid/graphics/Canvas;)V

    .line 2016819
    iget-boolean v0, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->e:Z

    if-eqz v0, :cond_1

    .line 2016820
    iget-object v0, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->f:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 2016821
    invoke-virtual {p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0219a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 2016822
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->f:Landroid/graphics/Bitmap;

    .line 2016823
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 2016824
    invoke-virtual {p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2016825
    iget-object v2, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->f:Landroid/graphics/Bitmap;

    int-to-float v0, v0

    int-to-float v1, v1

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2016826
    :cond_1
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 2016816
    iget v0, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->b:I

    iget v1, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->setMeasuredDimension(II)V

    .line 2016817
    return-void
.end method

.method public setPogSize(I)V
    .locals 1

    .prologue
    .line 2016810
    iget v0, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->b:I

    if-eq p1, v0, :cond_0

    .line 2016811
    iput p1, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->b:I

    .line 2016812
    iget v0, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->b:I

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->setMinimumHeight(I)V

    .line 2016813
    iget v0, p0, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->b:I

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->setMinimumWidth(I)V

    .line 2016814
    invoke-virtual {p0}, Lcom/facebook/groups/widget/remotepogview/RemotePogView;->requestLayout()V

    .line 2016815
    :cond_0
    return-void
.end method
