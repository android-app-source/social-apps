.class public Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field public a:LX/8tu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;

.field private d:Landroid/text/style/ClickableSpan;

.field private e:Landroid/text/SpannableString;

.field public f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2016392
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 2016393
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->c:Landroid/content/res/Resources;

    .line 2016394
    invoke-direct {p0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a()V

    .line 2016395
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2016388
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2016389
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->c:Landroid/content/res/Resources;

    .line 2016390
    invoke-direct {p0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a()V

    .line 2016391
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2016384
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2016385
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->c:Landroid/content/res/Resources;

    .line 2016386
    invoke-direct {p0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a()V

    .line 2016387
    return-void
.end method

.method private a(Ljava/lang/String;II)Landroid/text/Spannable;
    .locals 4

    .prologue
    .line 2016381
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2016382
    iget-object v1, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->d:Landroid/text/style/ClickableSpan;

    add-int v2, p2, p3

    const/16 v3, 0x21

    invoke-interface {v0, v1, p2, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2016383
    return-object v0
.end method

.method private static a(Ljava/lang/CharSequence;Landroid/text/Spannable;)Ljava/lang/CharSequence;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x3

    const/4 v1, 0x0

    .line 2016372
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    add-int/lit16 v0, v0, 0x96

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2016373
    new-array v0, v5, [Ljava/lang/CharSequence;

    const/16 v2, 0x96

    invoke-interface {p0, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, " "

    aput-object v1, v0, v6

    aput-object p1, v0, v7

    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p0

    .line 2016374
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    move v0, v1

    move v2, v1

    .line 2016375
    :goto_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2016376
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    const/16 v4, 0xa

    if-ne v3, v4, :cond_2

    .line 2016377
    add-int/lit8 v2, v2, 0x1

    .line 2016378
    :cond_2
    if-lt v2, v5, :cond_3

    .line 2016379
    new-array v2, v5, [Ljava/lang/CharSequence;

    invoke-interface {p0, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v2, v1

    const-string v0, " "

    aput-object v0, v2, v6

    aput-object p1, v2, v7

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0

    .line 2016380
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2016369
    const-class v0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-static {v0, p0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2016370
    invoke-direct {p0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->getClickableSpan()Landroid/text/style/ClickableSpan;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->d:Landroid/text/style/ClickableSpan;

    .line 2016371
    return-void
.end method

.method private a(Landroid/text/Spannable;)V
    .locals 1

    .prologue
    .line 2016332
    invoke-virtual {p0, p1}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->setText(Ljava/lang/CharSequence;)V

    .line 2016333
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/7Gw;->a(Landroid/widget/TextView;I)Z

    .line 2016334
    iget-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a:LX/8tu;

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2016335
    return-void
.end method

.method private static a(Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;LX/8tu;LX/17W;)V
    .locals 0

    .prologue
    .line 2016368
    iput-object p1, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a:LX/8tu;

    iput-object p2, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->b:LX/17W;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;

    invoke-static {v1}, LX/8tu;->a(LX/0QB;)LX/8tu;

    move-result-object v0

    check-cast v0, LX/8tu;

    invoke-static {v1}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-static {p0, v0, v1}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a(Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;LX/8tu;LX/17W;)V

    return-void
.end method

.method public static c$redex0(Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;)V
    .locals 2

    .prologue
    .line 2016355
    iget-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->j:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2016356
    :goto_0
    return-void

    .line 2016357
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->f:Z

    if-eqz v0, :cond_1

    .line 2016358
    iget-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->setText(Ljava/lang/CharSequence;)V

    .line 2016359
    :goto_1
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/7Gw;->a(Landroid/widget/TextView;I)Z

    .line 2016360
    iget-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a:LX/8tu;

    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    goto :goto_0

    .line 2016361
    :cond_1
    new-instance v0, Landroid/text/SpannableString;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2016362
    iget-boolean v1, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->h:Z

    if-eqz v1, :cond_2

    .line 2016363
    invoke-direct {p0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->getSpannable()Landroid/text/Spannable;

    move-result-object v0

    .line 2016364
    :cond_2
    iget-boolean v1, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->g:Z

    if-eqz v1, :cond_3

    .line 2016365
    iget-object v1, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->j:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a(Ljava/lang/CharSequence;Landroid/text/Spannable;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2016366
    :goto_2
    invoke-virtual {p0, v0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2016367
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->j:Ljava/lang/String;

    goto :goto_2
.end method

.method private getClickableSpan()Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 2016351
    iget-boolean v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->i:Z

    if-eqz v0, :cond_0

    .line 2016352
    new-instance v0, LX/Dao;

    invoke-direct {v0, p0}, LX/Dao;-><init>(Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;)V

    .line 2016353
    :goto_0
    return-object v0

    .line 2016354
    :cond_0
    new-instance v0, LX/Dap;

    invoke-direct {v0, p0}, LX/Dap;-><init>(Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;)V

    goto :goto_0
.end method

.method private getSpannable()Landroid/text/Spannable;
    .locals 5

    .prologue
    .line 2016347
    iget-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->e:Landroid/text/SpannableString;

    if-nez v0, :cond_0

    .line 2016348
    iget-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->c:Landroid/content/res/Resources;

    const v1, 0x7f083079

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/SpannableString;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->e:Landroid/text/SpannableString;

    .line 2016349
    iget-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->e:Landroid/text/SpannableString;

    iget-object v1, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->d:Landroid/text/style/ClickableSpan;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->e:Landroid/text/SpannableString;

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2016350
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->e:Landroid/text/SpannableString;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 2016341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->g:Z

    .line 2016342
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->i:Z

    .line 2016343
    iput-object p2, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->k:Ljava/lang/String;

    .line 2016344
    invoke-direct {p0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->getClickableSpan()Landroid/text/style/ClickableSpan;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->d:Landroid/text/style/ClickableSpan;

    .line 2016345
    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a(Ljava/lang/String;II)Landroid/text/Spannable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->a(Landroid/text/Spannable;)V

    .line 2016346
    return-void
.end method

.method public final a(Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 2016336
    iput-object p1, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->j:Ljava/lang/String;

    .line 2016337
    iput-boolean p2, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->g:Z

    .line 2016338
    iput-boolean p3, p0, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->h:Z

    .line 2016339
    invoke-static {p0}, Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;->c$redex0(Lcom/facebook/groups/widget/infoview/GroupsDescriptionSpannableView;)V

    .line 2016340
    return-void
.end method
