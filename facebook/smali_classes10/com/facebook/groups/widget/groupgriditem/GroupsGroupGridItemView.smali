.class public Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;
.super LX/DaX;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final g:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private h:Landroid/view/ViewStub;

.field public i:Landroid/widget/ImageView;

.field public j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2015468
    const-class v0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;

    const-string v1, "landing"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->g:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2015465
    invoke-direct {p0, p1}, LX/DaX;-><init>(Landroid/content/Context;)V

    .line 2015466
    const v0, 0x7f020ec3

    iput v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->j:I

    .line 2015467
    return-void
.end method

.method public static a(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V
    .locals 2

    .prologue
    .line 2015462
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 2015463
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2015464
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2015451
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 2015452
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2015453
    :cond_0
    :goto_0
    return-void

    .line 2015454
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->h:Landroid/view/ViewStub;

    if-nez v0, :cond_2

    .line 2015455
    const v0, 0x7f0d2658

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->d(I)LX/0am;

    move-result-object v0

    .line 2015456
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2015457
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->h:Landroid/view/ViewStub;

    .line 2015458
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2015459
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    .line 2015460
    :goto_1
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 2015461
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->h:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->getRootView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    goto :goto_1
.end method

.method private static b(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;Z)V
    .locals 0

    .prologue
    .line 2015447
    if-eqz p1, :cond_0

    .line 2015448
    invoke-static {p0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->b(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V

    .line 2015449
    :goto_0
    return-void

    .line 2015450
    :cond_0
    invoke-static {p0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->a(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2015446
    const/4 v0, 0x0

    invoke-static {v0}, LX/Dae;->a(Z)I

    move-result v0

    return v0
.end method

.method public final a(LX/Gkq;D)V
    .locals 10

    .prologue
    .line 2015433
    iget-boolean v0, p1, LX/Gkq;->n:Z

    move v0, v0

    .line 2015434
    if-eqz v0, :cond_1

    .line 2015435
    invoke-static {p0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->b(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V

    .line 2015436
    :goto_0
    iget-object v0, p1, LX/Gkq;->a:Ljava/lang/String;

    move-object v2, v0

    .line 2015437
    iget-object v0, p1, LX/Gkq;->c:Ljava/lang/String;

    move-object v3, v0

    .line 2015438
    iget-boolean v0, p1, LX/Gkq;->n:Z

    move v0, v0

    .line 2015439
    if-eqz v0, :cond_0

    const/4 v4, 0x0

    .line 2015440
    :goto_1
    iget-object v0, p1, LX/Gkq;->b:Ljava/lang/String;

    move-object v5, v0

    .line 2015441
    sget-object v8, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->g:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, p0

    move-wide v6, p2

    invoke-virtual/range {v1 .. v8}, LX/DaX;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;DLcom/facebook/common/callercontext/CallerContext;)V

    .line 2015442
    return-void

    .line 2015443
    :cond_0
    iget v0, p1, LX/Gkq;->f:I

    move v4, v0

    .line 2015444
    goto :goto_1

    .line 2015445
    :cond_1
    invoke-static {p0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->a(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;D)V
    .locals 9

    .prologue
    .line 2015431
    sget-object v8, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->g:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-virtual/range {v1 .. v8}, LX/DaX;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;DLcom/facebook/common/callercontext/CallerContext;)V

    .line 2015432
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;DZ)V
    .locals 11

    .prologue
    .line 2015418
    move/from16 v0, p7

    invoke-static {p0, v0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->b(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;Z)V

    .line 2015419
    sget-object v10, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->g:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move-object v7, p4

    move-wide/from16 v8, p5

    invoke-virtual/range {v3 .. v10}, LX/DaX;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;DLcom/facebook/common/callercontext/CallerContext;)V

    .line 2015420
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2015426
    const v0, 0x7f020c4a

    iput v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->j:I

    .line 2015427
    if-eqz p1, :cond_0

    .line 2015428
    invoke-static {p0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->b(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V

    .line 2015429
    :goto_0
    return-void

    .line 2015430
    :cond_0
    invoke-static {p0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->a(Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;)V

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2015421
    invoke-super {p0, p1}, LX/DaX;->draw(Landroid/graphics/Canvas;)V

    .line 2015422
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2015423
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->j:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2015424
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->i:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->getDrawingTime()J

    move-result-wide v2

    invoke-virtual {p0, p1, v0, v2, v3}, Lcom/facebook/groups/widget/groupgriditem/GroupsGroupGridItemView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 2015425
    :cond_0
    return-void
.end method
