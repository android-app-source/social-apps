.class public final Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2015542
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2015543
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2015540
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2015541
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2015517
    if-nez p1, :cond_0

    .line 2015518
    :goto_0
    return v0

    .line 2015519
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2015520
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2015521
    :sswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2015522
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2015523
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2015524
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2015525
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2015526
    const v2, -0x4b792c95

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2015527
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2015528
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2015529
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2015530
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2015531
    const v2, 0x399461ad

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2015532
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2015533
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2015534
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2015535
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2015536
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2015537
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2015538
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2015539
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x4b792c95 -> :sswitch_2
        -0x267b0ee8 -> :sswitch_0
        0x399461ad -> :sswitch_3
        0x3ec49b28 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2015516
    new-instance v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2015469
    sparse-switch p2, :sswitch_data_0

    .line 2015470
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2015471
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2015472
    const v1, -0x4b792c95

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2015473
    :goto_0
    :sswitch_1
    return-void

    .line 2015474
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2015475
    const v1, 0x399461ad

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x4b792c95 -> :sswitch_2
        -0x267b0ee8 -> :sswitch_1
        0x399461ad -> :sswitch_1
        0x3ec49b28 -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2015510
    if-eqz p1, :cond_0

    .line 2015511
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;

    move-result-object v1

    .line 2015512
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;

    .line 2015513
    if-eq v0, v1, :cond_0

    .line 2015514
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2015515
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2015509
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2015507
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2015508
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2015502
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2015503
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2015504
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a:LX/15i;

    .line 2015505
    iput p2, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->b:I

    .line 2015506
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2015501
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2015500
    new-instance v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2015497
    iget v0, p0, LX/1vt;->c:I

    .line 2015498
    move v0, v0

    .line 2015499
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2015494
    iget v0, p0, LX/1vt;->c:I

    .line 2015495
    move v0, v0

    .line 2015496
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2015491
    iget v0, p0, LX/1vt;->b:I

    .line 2015492
    move v0, v0

    .line 2015493
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2015488
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2015489
    move-object v0, v0

    .line 2015490
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2015479
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2015480
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2015481
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2015482
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2015483
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2015484
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2015485
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2015486
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2015487
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2015476
    iget v0, p0, LX/1vt;->c:I

    .line 2015477
    move v0, v0

    .line 2015478
    return v0
.end method
