.class public final Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x739b99f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2015682
    const-class v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2015613
    const-class v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2015680
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2015681
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2015674
    iput-object p1, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->i:Ljava/lang/String;

    .line 2015675
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2015676
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2015677
    if-eqz v0, :cond_0

    .line 2015678
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2015679
    :cond_0
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2015672
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 2015673
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupItemCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2015670
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2015671
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupFeed"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2015668
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2015669
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2015666
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->h:Ljava/lang/String;

    .line 2015667
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2015664
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->i:Ljava/lang/String;

    .line 2015665
    iget-object v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2015650
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2015651
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2015652
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x3ec49b28

    invoke-static {v2, v1, v3}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2015653
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x267b0ee8

    invoke-static {v3, v2, v4}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2015654
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2015655
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2015656
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2015657
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2015658
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2015659
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2015660
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2015661
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2015662
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2015663
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2015634
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2015635
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 2015636
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x3ec49b28

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2015637
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2015638
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;

    .line 2015639
    iput v3, v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->f:I

    move-object v1, v0

    .line 2015640
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2015641
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x267b0ee8

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2015642
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2015643
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;

    .line 2015644
    iput v3, v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->g:I

    move-object v1, v0

    .line 2015645
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2015646
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 2015647
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2015648
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move-object p0, v1

    .line 2015649
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2015633
    new-instance v0, LX/DaY;

    invoke-direct {v0, p1}, LX/DaY;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2015632
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2015628
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2015629
    const/4 v0, 0x1

    const v1, 0x3ec49b28

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->f:I

    .line 2015630
    const/4 v0, 0x2

    const v1, -0x267b0ee8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->g:I

    .line 2015631
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2015622
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2015623
    invoke-direct {p0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2015624
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2015625
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 2015626
    :goto_0
    return-void

    .line 2015627
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2015619
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2015620
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;->a(Ljava/lang/String;)V

    .line 2015621
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2015616
    new-instance v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;

    invoke-direct {v0}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;-><init>()V

    .line 2015617
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2015618
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2015615
    const v0, 0x71bff988

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2015614
    const v0, 0x41e065f

    return v0
.end method
