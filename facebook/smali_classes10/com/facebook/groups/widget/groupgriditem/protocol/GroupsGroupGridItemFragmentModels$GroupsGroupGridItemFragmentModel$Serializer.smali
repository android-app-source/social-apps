.class public final Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2015610
    const-class v0, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;

    new-instance v1, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2015611
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2015612
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2015585
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2015586
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 2015587
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2015588
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2015589
    if-eqz v2, :cond_0

    .line 2015590
    const-string v2, "community_category"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2015591
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2015592
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2015593
    if-eqz v2, :cond_1

    .line 2015594
    const-string p0, "groupItemCoverPhoto"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2015595
    invoke-static {v1, v2, p1, p2}, LX/Dab;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2015596
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2015597
    if-eqz v2, :cond_2

    .line 2015598
    const-string p0, "group_feed"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2015599
    invoke-static {v1, v2, p1}, LX/Daa;->a(LX/15i;ILX/0nX;)V

    .line 2015600
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2015601
    if-eqz v2, :cond_3

    .line 2015602
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2015603
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2015604
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2015605
    if-eqz v2, :cond_4

    .line 2015606
    const-string p0, "name"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2015607
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2015608
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2015609
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2015584
    check-cast p1, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel$Serializer;->a(Lcom/facebook/groups/widget/groupgriditem/protocol/GroupsGroupGridItemFragmentModels$GroupsGroupGridItemFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
