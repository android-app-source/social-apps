.class public final Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2014667
    iput-object p1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iput-object p2, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2014668
    :try_start_0
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v0, v0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->c:LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v0

    .line 2014669
    new-instance v1, LX/Da6;

    iget-object v2, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v2, v2, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->w:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v3, v3, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->C:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    invoke-virtual {v3}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->e()LX/0Px;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/Da6;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2014670
    iget-object v2, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v2, v2, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->d:LX/Da5;

    invoke-static {v2, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    const-string v2, "createGroupSideConversation"

    .line 2014671
    iput-object v2, v1, LX/2Vk;->c:Ljava/lang/String;

    .line 2014672
    move-object v1, v1

    .line 2014673
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 2014674
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v1, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v1, :cond_0

    .line 2014675
    new-instance v1, LX/Da8;

    const-string v2, "{result=createGroupSideConversation:$.id}"

    iget-object v3, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v3, v3, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {v1, v2, v3}, LX/Da8;-><init>(Ljava/lang/String;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 2014676
    iget-object v2, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v2, v2, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->f:LX/Da7;

    invoke-static {v2, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    const-string v2, "createGroupSideConversation"

    .line 2014677
    iput-object v2, v1, LX/2Vk;->d:Ljava/lang/String;

    .line 2014678
    move-object v1, v1

    .line 2014679
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 2014680
    :cond_0
    new-instance v1, LX/Da4;

    iget-object v2, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v2, v2, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->u:Ljava/lang/String;

    const-string v3, "{result=createGroupSideConversation:$.id}"

    invoke-direct {v1, v2, v3}, LX/Da4;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2014681
    iget-object v2, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v2, v2, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->e:LX/Da3;

    invoke-static {v2, v1}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v1

    const-string v2, "createGroupSideConversation"

    .line 2014682
    iput-object v2, v1, LX/2Vk;->d:Ljava/lang/String;

    .line 2014683
    move-object v1, v1

    .line 2014684
    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 2014685
    const-string v1, "createSideConversationBatch"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2014686
    const-string v1, "createGroupSideConversation"

    invoke-interface {v0, v1}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2014687
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v2, -0x7433a816

    invoke-static {v1, v0, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2014688
    :goto_0
    return-void

    .line 2014689
    :catch_0
    move-exception v0

    .line 2014690
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->b:Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    iget-object v1, v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->t:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2014691
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment$2;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
