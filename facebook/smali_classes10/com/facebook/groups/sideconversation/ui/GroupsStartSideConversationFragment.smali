.class public Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0fj;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final q:[Ljava/lang/String;

.field public static final r:[Ljava/lang/String;

.field private static final s:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Lcom/facebook/ui/media/attachments/MediaResource;

.field public B:Landroid/support/v4/app/DialogFragment;

.field public C:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

.field private D:Z

.field private final E:Landroid/view/View$OnClickListener;

.field public a:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/18V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Da5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Da3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Da7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Da0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2Ib;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/Da2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0i5;

.field public final t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field private v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public w:Lcom/facebook/resources/ui/FbEditText;

.field public x:Landroid/widget/ImageView;

.field public y:Lcom/facebook/drawee/view/DraweeView;

.field public z:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2014861
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v3

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->q:[Ljava/lang/String;

    .line 2014862
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->r:[Ljava/lang/String;

    .line 2014863
    const-class v0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    const-string v1, "group_mall_side_conversation"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->s:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2014849
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2014850
    const-class v0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->t:Ljava/lang/String;

    .line 2014851
    new-instance v0, LX/DaD;

    invoke-direct {v0, p0}, LX/DaD;-><init>(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->E:Landroid/view/View$OnClickListener;

    .line 2014852
    return-void
.end method

.method private static a(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;LX/1g8;Landroid/view/inputmethod/InputMethodManager;LX/18V;LX/Da5;LX/Da3;LX/Da7;Ljava/util/concurrent/ExecutorService;LX/1Ck;LX/Da0;Lcom/facebook/content/SecureContextHelper;LX/2Ib;LX/0kL;LX/1Ad;LX/Da2;LX/0i4;)V
    .locals 0

    .prologue
    .line 2014853
    iput-object p1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->a:LX/1g8;

    iput-object p2, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    iput-object p3, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->c:LX/18V;

    iput-object p4, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->d:LX/Da5;

    iput-object p5, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->e:LX/Da3;

    iput-object p6, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->f:LX/Da7;

    iput-object p7, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->g:Ljava/util/concurrent/ExecutorService;

    iput-object p8, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->h:LX/1Ck;

    iput-object p9, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->i:LX/Da0;

    iput-object p10, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->j:Lcom/facebook/content/SecureContextHelper;

    iput-object p11, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->k:LX/2Ib;

    iput-object p12, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->l:LX/0kL;

    iput-object p13, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->m:LX/1Ad;

    iput-object p14, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->n:LX/Da2;

    iput-object p15, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->o:LX/0i4;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 17

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;

    invoke-static {v15}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v1

    check-cast v1, LX/1g8;

    invoke-static {v15}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v15}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v3

    check-cast v3, LX/18V;

    invoke-static {v15}, LX/Da5;->a(LX/0QB;)LX/Da5;

    move-result-object v4

    check-cast v4, LX/Da5;

    invoke-static {v15}, LX/Da3;->a(LX/0QB;)LX/Da3;

    move-result-object v5

    check-cast v5, LX/Da3;

    invoke-static {v15}, LX/Da7;->a(LX/0QB;)LX/Da7;

    move-result-object v6

    check-cast v6, LX/Da7;

    invoke-static {v15}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, Ljava/util/concurrent/ExecutorService;

    invoke-static {v15}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-static {v15}, LX/Da0;->a(LX/0QB;)LX/Da0;

    move-result-object v9

    check-cast v9, LX/Da0;

    invoke-static {v15}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v10

    check-cast v10, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v15}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v11

    check-cast v11, LX/2Ib;

    invoke-static {v15}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-static {v15}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v13

    check-cast v13, LX/1Ad;

    invoke-static {v15}, LX/Da2;->a(LX/0QB;)LX/Da2;

    move-result-object v14

    check-cast v14, LX/Da2;

    const-class v16, LX/0i4;

    invoke-interface/range {v15 .. v16}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/0i4;

    invoke-static/range {v0 .. v15}, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->a(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;LX/1g8;Landroid/view/inputmethod/InputMethodManager;LX/18V;LX/Da5;LX/Da3;LX/Da7;Ljava/util/concurrent/ExecutorService;LX/1Ck;LX/Da0;Lcom/facebook/content/SecureContextHelper;LX/2Ib;LX/0kL;LX/1Ad;LX/Da2;LX/0i4;)V

    return-void
.end method

.method public static c(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V
    .locals 1

    .prologue
    .line 2014829
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->D:Z

    .line 2014830
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2014831
    return-void
.end method

.method public static d$redex0(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V
    .locals 3

    .prologue
    .line 2014854
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->b:Landroid/view/inputmethod/InputMethodManager;

    .line 2014855
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2014856
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2014857
    return-void
.end method

.method public static k(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V
    .locals 1

    .prologue
    .line 2014858
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->B:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    .line 2014859
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->B:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 2014860
    :cond_0
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 2014832
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2014833
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->m:LX/1Ad;

    sget-object v1, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->s:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v1, v1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 2014834
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->y:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2014835
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->x:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2014836
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->y:Lcom/facebook/drawee/view/DraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setVisibility(I)V

    .line 2014837
    :cond_0
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2014838
    iget-boolean v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->D:Z

    if-eqz v1, :cond_1

    .line 2014839
    :cond_0
    :goto_0
    return v0

    .line 2014840
    :cond_1
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->w:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->C:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    invoke-virtual {v1}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->e()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gtz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    if-eqz v1, :cond_0

    .line 2014841
    :cond_2
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2014842
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    .line 2014843
    const v1, 0x7f081b6c

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2014844
    const v1, 0x7f081b6d

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2014845
    const v1, 0x7f081b6e

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/DaG;

    invoke-direct {v2, p0}, LX/DaG;-><init>(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2014846
    const v1, 0x7f081b6f

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/DaH;

    invoke-direct {v2, p0}, LX/DaH;-><init>(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2014847
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2014848
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2014774
    const-string v0, "group_mall_side_conversation"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2014764
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2014765
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2014766
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2014767
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->u:Ljava/lang/String;

    .line 2014768
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2014769
    const-string v1, "PRE_SELECT_MEMBERS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->v:Ljava/util/ArrayList;

    .line 2014770
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2014771
    const-string v1, "group_feed_id"

    iget-object v2, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2014772
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->D:Z

    .line 2014773
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2014738
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2014739
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2014740
    packed-switch p1, :pswitch_data_0

    .line 2014741
    :cond_0
    :goto_0
    return-void

    .line 2014742
    :pswitch_0
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->z:Landroid/net/Uri;

    .line 2014743
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2014744
    move-object v0, v0

    .line 2014745
    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    .line 2014746
    iput-object v1, v0, LX/5zn;->c:LX/2MK;

    .line 2014747
    move-object v0, v0

    .line 2014748
    sget-object v1, LX/5zj;->CAMERA:LX/5zj;

    .line 2014749
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 2014750
    move-object v0, v0

    .line 2014751
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2014752
    invoke-direct {p0}, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->o()V

    goto :goto_0

    .line 2014753
    :pswitch_1
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 2014754
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2014755
    move-object v0, v0

    .line 2014756
    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    .line 2014757
    iput-object v1, v0, LX/5zn;->c:LX/2MK;

    .line 2014758
    move-object v0, v0

    .line 2014759
    sget-object v1, LX/5zj;->GALLERY:LX/5zj;

    .line 2014760
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 2014761
    move-object v0, v0

    .line 2014762
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2014763
    invoke-direct {p0}, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->o()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3f2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x50793614

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2014737
    const v1, 0x7f0313ba

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4f15db0f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6de7d59d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2014775
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2014776
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    const v2, 0x7f0d2d8a

    invoke-virtual {v1, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2014777
    if-eqz v1, :cond_0

    .line 2014778
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->c()I

    .line 2014779
    :cond_0
    const/16 v1, 0x2b

    const v2, 0xb51d6fa

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2014780
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2014781
    const-string v0, "savedThreadName"

    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->w:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2014782
    const-string v0, "savedThreadPhoto"

    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2014783
    const-string v0, "tmp_image"

    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->z:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2014784
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x522fb1b6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2014785
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2014786
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->v:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 2014787
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->C:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    iget-object v2, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->v:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->a(Ljava/util/ArrayList;)V

    .line 2014788
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->v:Ljava/util/ArrayList;

    .line 2014789
    :cond_0
    const/16 v1, 0x2b

    const v2, -0xeb2f160

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2014790
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2014791
    const v0, 0x7f0d2d89

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->w:Lcom/facebook/resources/ui/FbEditText;

    .line 2014792
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->w:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a05f0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2014793
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->w:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->requestFocus()Z

    .line 2014794
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->w:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/DaE;

    invoke-direct {v1, p0}, LX/DaE;-><init>(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2014795
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->n:LX/Da2;

    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->E:Landroid/view/View$OnClickListener;

    .line 2014796
    const-class v2, LX/1ZF;

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1ZF;

    .line 2014797
    if-eqz v2, :cond_0

    .line 2014798
    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1ZF;->k_(Z)V

    .line 2014799
    iget-object v3, v0, LX/Da2;->a:Landroid/content/res/Resources;

    const v4, 0x7f081b5d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 2014800
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v3

    iget-object v4, v0, LX/Da2;->a:Landroid/content/res/Resources;

    const v5, 0x7f081b5f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2014801
    iput-object v4, v3, LX/108;->g:Ljava/lang/String;

    .line 2014802
    move-object v3, v3

    .line 2014803
    const/4 v4, -0x2

    .line 2014804
    iput v4, v3, LX/108;->h:I

    .line 2014805
    move-object v3, v3

    .line 2014806
    invoke-virtual {v3}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v3

    .line 2014807
    invoke-interface {v2, v3}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2014808
    new-instance v3, LX/Da1;

    invoke-direct {v3, v0, v1}, LX/Da1;-><init>(LX/Da2;Landroid/view/View$OnClickListener;)V

    invoke-interface {v2, v3}, LX/1ZF;->a(LX/63W;)V

    .line 2014809
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->o:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->p:LX/0i5;

    .line 2014810
    const v0, 0x7f0d2d87

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->x:Landroid/widget/ImageView;

    .line 2014811
    const v0, 0x7f0d2d88

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/view/DraweeView;

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->y:Lcom/facebook/drawee/view/DraweeView;

    .line 2014812
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d2d8a

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->C:Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    .line 2014813
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v1

    .line 2014814
    iput-object v1, v0, LX/1Uo;->u:LX/4Ab;

    .line 2014815
    move-object v0, v0

    .line 2014816
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 2014817
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->y:Lcom/facebook/drawee/view/DraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2014818
    if-eqz p2, :cond_3

    .line 2014819
    const-string v0, "savedThreadName"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2014820
    iget-object v1, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->w:Lcom/facebook/resources/ui/FbEditText;

    const-string v0, "savedThreadName"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2014821
    :cond_1
    const-string v0, "savedThreadPhoto"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2014822
    const-string v0, "savedThreadPhoto"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2014823
    :cond_2
    const-string v0, "tmp_image"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2014824
    const-string v0, "tmp_image"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->z:Landroid/net/Uri;

    .line 2014825
    :cond_3
    const v0, 0x7f0d2d86

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 2014826
    new-instance v1, LX/DaF;

    invoke-direct {v1, p0, v0}, LX/DaF;-><init>(Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2014827
    invoke-direct {p0}, Lcom/facebook/groups/sideconversation/ui/GroupsStartSideConversationFragment;->o()V

    .line 2014828
    return-void
.end method
