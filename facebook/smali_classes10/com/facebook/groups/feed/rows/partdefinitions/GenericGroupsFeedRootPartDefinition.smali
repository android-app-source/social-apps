.class public Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/1TR;

.field private final c:LX/DD3;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 4
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;*-TE;>;>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;*-TE;>;>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;",
            ">;*-TE;*>;>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/api/feed/data/EndOfFeedSentinel$EndOfFeedSentinelFeedUnit;",
            ">;*-TE;*>;>;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/hidden/HiddenUnitGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/egolistview/rows/GroupYouShouldJoinComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gysc/partdefinitions/GroupsYouShouldCreateHScrollRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gysc/components/GroupsYouShouldCreateComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/feed/rows/components/GroupsSectionHeaderComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/storyset/rows/StorySetPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/gpymi/rows/GroupsPeopleYouMayInviteComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1TR;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DD3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1992906
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1992907
    invoke-interface/range {p14 .. p14}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1TR;

    iput-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->b:LX/1TR;

    .line 1992908
    invoke-interface/range {p15 .. p15}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DD3;

    iput-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->c:LX/DD3;

    .line 1992909
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/HideableUnit;

    invoke-static {p5}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->a:LX/1T5;

    .line 1992910
    if-eqz p2, :cond_0

    .line 1992911
    iget-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->a:LX/1T5;

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v2, p2}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    .line 1992912
    :cond_0
    if-eqz p3, :cond_3

    .line 1992913
    iget-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->a:LX/1T5;

    const-class v2, Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    invoke-virtual {v1, v2, p3}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    .line 1992914
    :goto_0
    iget-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->a:LX/1T5;

    const-class v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v2, p1}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/api/feed/data/EndOfFeedSentinel$EndOfFeedSentinelFeedUnit;

    invoke-virtual {v1, v2, p4}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldJoinFeedUnit;

    invoke-static {p6}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupsYouShouldCreateFeedUnit;

    iget-object v3, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->b:LX/1TR;

    invoke-virtual {v3}, LX/1TR;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p8}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object p7

    :cond_1
    invoke-virtual {v1, v2, p7}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/GraphQLGroupsSectionHeaderUnit;

    invoke-static {p9}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-static {p10}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    .line 1992915
    iget-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->c:LX/DD3;

    invoke-virtual {v1}, LX/DD3;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1992916
    iget-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->a:LX/1T5;

    const-class v2, Lcom/facebook/graphql/model/GraphQLPaginatedGroupsPeopleYouMayInviteFeedUnit;

    invoke-static {p11}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    .line 1992917
    :cond_2
    iget-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->a:LX/1T5;

    const-class v2, Ljava/lang/Object;

    invoke-static/range {p12 .. p12}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    .line 1992918
    return-void

    .line 1992919
    :cond_3
    iget-object v1, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->a:LX/1T5;

    const-class v2, Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    move-object/from16 v0, p13

    invoke-virtual {v1, v2, v0}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1992920
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    .line 1992921
    iget-object v0, p0, Lcom/facebook/groups/feed/rows/partdefinitions/GenericGroupsFeedRootPartDefinition;->a:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    .line 1992922
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1992923
    const/4 v0, 0x1

    return v0
.end method
