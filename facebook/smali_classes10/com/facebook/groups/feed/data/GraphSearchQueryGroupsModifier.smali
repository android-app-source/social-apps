.class public Lcom/facebook/groups/feed/data/GraphSearchQueryGroupsModifier;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/groups/feed/data/GraphSearchQueryGroupsModifier;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lcom/facebook/graphql/enums/GraphQLGroupCategory;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1991891
    new-instance v0, LX/DNc;

    invoke-direct {v0}, LX/DNc;-><init>()V

    sput-object v0, Lcom/facebook/groups/feed/data/GraphSearchQueryGroupsModifier;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/DNd;)V
    .locals 1

    .prologue
    .line 1991888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1991889
    iget-object v0, p1, LX/DNd;->a:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/groups/feed/data/GraphSearchQueryGroupsModifier;->a:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1991890
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1991892
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1991886
    iget-object v0, p0, Lcom/facebook/groups/feed/data/GraphSearchQueryGroupsModifier;->a:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1991887
    return-void
.end method
