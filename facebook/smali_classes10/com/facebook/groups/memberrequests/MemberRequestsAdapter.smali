.class public Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;
.super LX/1Cv;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/DML;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DML",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/DML;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DML",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/DML;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DML",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/DML;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DML",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/DML;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DML",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/DML;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DML",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/DML;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DML",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/DML;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DML",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/DML;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/DML",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/DYU;

.field private l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field

.field public m:Landroid/content/res/Resources;

.field public final n:LX/17Y;

.field public final o:Lcom/facebook/content/SecureContextHelper;

.field private final p:LX/11R;

.field public final q:LX/DXz;

.field private final r:Z

.field private final s:Z

.field private final t:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DML",
            "<+",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2011495
    const-class v0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    const-string v1, "member_requests"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/DYU;ZLX/DXz;LX/17Y;LX/11R;Lcom/facebook/content/SecureContextHelper;Landroid/content/res/Resources;Ljava/lang/Boolean;)V
    .locals 10
    .param p1    # LX/DYU;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2011474
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2011475
    new-instance v1, LX/DYD;

    invoke-direct {v1, p0}, LX/DYD;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->b:LX/DML;

    .line 2011476
    new-instance v1, LX/DYE;

    invoke-direct {v1, p0}, LX/DYE;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->c:LX/DML;

    .line 2011477
    new-instance v1, LX/DYF;

    invoke-direct {v1, p0}, LX/DYF;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->d:LX/DML;

    .line 2011478
    new-instance v1, LX/DYG;

    invoke-direct {v1, p0}, LX/DYG;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->e:LX/DML;

    .line 2011479
    new-instance v1, LX/DYH;

    invoke-direct {v1, p0}, LX/DYH;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->f:LX/DML;

    .line 2011480
    new-instance v1, LX/DYI;

    invoke-direct {v1, p0}, LX/DYI;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->g:LX/DML;

    .line 2011481
    new-instance v1, LX/DYJ;

    invoke-direct {v1, p0}, LX/DYJ;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->h:LX/DML;

    .line 2011482
    new-instance v1, LX/DYK;

    invoke-direct {v1, p0}, LX/DYK;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->i:LX/DML;

    .line 2011483
    new-instance v1, LX/DYL;

    invoke-direct {v1, p0}, LX/DYL;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;)V

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->j:LX/DML;

    .line 2011484
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->l:LX/0Px;

    .line 2011485
    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->f:LX/DML;

    iget-object v2, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->b:LX/DML;

    iget-object v3, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->g:LX/DML;

    iget-object v4, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->i:LX/DML;

    iget-object v5, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->d:LX/DML;

    iget-object v6, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->e:LX/DML;

    iget-object v7, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->c:LX/DML;

    iget-object v8, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->h:LX/DML;

    iget-object v9, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->j:LX/DML;

    invoke-static/range {v1 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->t:LX/0Px;

    .line 2011486
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->o:Lcom/facebook/content/SecureContextHelper;

    .line 2011487
    iput-object p4, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->n:LX/17Y;

    .line 2011488
    iput-object p1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->k:LX/DYU;

    .line 2011489
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    .line 2011490
    invoke-virtual/range {p8 .. p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->s:Z

    .line 2011491
    iput-object p5, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->p:LX/11R;

    .line 2011492
    iput-object p3, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->q:LX/DXz;

    .line 2011493
    iput-boolean p2, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->r:Z

    .line 2011494
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;Landroid/content/Context;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2011473
    new-instance v0, LX/DY8;

    invoke-direct {v0, p0, p1, p2}, LX/DY8;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;Landroid/view/View;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    const-wide/16 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2011438
    const v0, 0x7f0d15ca

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2011439
    iget-boolean v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->s:Z

    if-nez v1, :cond_0

    .line 2011440
    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 2011441
    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->n()J

    move-result-wide v4

    .line 2011442
    :goto_0
    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    .line 2011443
    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->p:LX/11R;

    sget-object v6, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    mul-long/2addr v4, v8

    invoke-virtual {v1, v6, v4, v5}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 2011444
    iget-object v4, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v5, 0x7f0830dc

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v1, v6, v3

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2011445
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2011446
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v1, 0x7f081b4d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2011447
    invoke-virtual {p1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;->j()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    move v1, v2

    .line 2011448
    :goto_1
    const v0, 0x7f0d15c9

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2011449
    if-eqz v1, :cond_3

    .line 2011450
    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->o()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->o()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel;->a()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel$WorkCommunityModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->o()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel;->a()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel$WorkCommunityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel$WorkCommunityModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2011451
    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v4, 0x7f0830e6

    new-array v5, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->o()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel;->a()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel$WorkCommunityModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel$WorkInfoModel$WorkCommunityModel;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2011452
    :cond_1
    :goto_2
    const v0, 0x7f0d15cb

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2011453
    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_7

    .line 2011454
    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2011455
    invoke-virtual {v4, v1, v3}, LX/15i;->j(II)I

    move-result v1

    if-lez v1, :cond_6

    move v1, v2

    :goto_3
    if-eqz v1, :cond_8

    .line 2011456
    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2011457
    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->j()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2011458
    iget-object v7, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v8, 0x7f0f015d

    invoke-virtual {v4, v1, v3}, LX/15i;->j(II)I

    move-result v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v6, v5, v3}, LX/15i;->j(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v7, v8, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2011459
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2011460
    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2011461
    :goto_4
    return-void

    :cond_2
    move v1, v3

    .line 2011462
    goto/16 :goto_1

    .line 2011463
    :cond_3
    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->p:LX/11R;

    sget-object v5, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->m()J

    move-result-wide v6

    mul-long/2addr v6, v8

    invoke-virtual {v1, v5, v6, v7}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 2011464
    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2011465
    iget-object v5, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v6, 0x7f0830dd

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;->k()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2011466
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2011467
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->j()I

    move-result v5

    if-lez v5, :cond_5

    .line 2011468
    iget-object v5, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->m:Landroid/content/res/Resources;

    const v6, 0x7f0f015c

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->j()I

    move-result v7

    new-array v8, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->j()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2011469
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 2011470
    :cond_5
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_6
    move v1, v3

    .line 2011471
    goto/16 :goto_3

    :cond_7
    move v1, v3

    goto/16 :goto_3

    .line 2011472
    :cond_8
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_9
    move-wide v4, v6

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2011437
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->t:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2011496
    check-cast p2, LX/DMB;

    .line 2011497
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 2011498
    return-void
.end method

.method public final a(Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;LX/0Px;LX/DYT;ZZ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;",
            ">;",
            "LX/DYT;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 2011398
    if-nez p2, :cond_0

    .line 2011399
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2011400
    iput-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->l:LX/0Px;

    .line 2011401
    :goto_0
    return-void

    .line 2011402
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2011403
    iget-boolean v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->r:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 2011404
    new-instance v0, LX/DY9;

    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->j:LX/DML;

    invoke-direct {v0, p0, v1, p1}, LX/DY9;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;LX/DML;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;)V

    move-object v0, v0

    .line 2011405
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2011406
    new-instance v0, LX/DaP;

    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->h:LX/DML;

    invoke-direct {v0, v1}, LX/DaP;-><init>(LX/DML;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2011407
    :cond_1
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p4, :cond_2

    .line 2011408
    new-instance v0, LX/DaP;

    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->g:LX/DML;

    invoke-direct {v0, v1}, LX/DaP;-><init>(LX/DML;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2011409
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->l:LX/0Px;

    .line 2011410
    const v0, 0x62a532e3

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0

    .line 2011411
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 2011412
    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

    .line 2011413
    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2011414
    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2011415
    iget-object v4, p3, LX/DYT;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p3, LX/DYT;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DYS;

    sget-object v5, LX/DYS;->MEMBER_REQUEST_ACCEPTED:LX/DYS;

    invoke-virtual {v4, v5}, LX/DYS;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    const/4 v4, 0x1

    :goto_2
    move v3, v4

    .line 2011416
    if-eqz v3, :cond_4

    .line 2011417
    new-instance v3, LX/DYA;

    iget-object v4, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->i:LX/DML;

    invoke-direct {v3, p0, v4, v0}, LX/DYA;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;LX/DML;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;)V

    move-object v0, v3

    .line 2011418
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2011419
    :cond_3
    :goto_3
    new-instance v0, LX/DaP;

    iget-object v3, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->h:LX/DML;

    invoke-direct {v0, v3}, LX/DaP;-><init>(LX/DML;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2011420
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2011421
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2011422
    iget-object v4, p3, LX/DYT;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v4, p3, LX/DYT;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DYS;

    sget-object v5, LX/DYS;->MEMBER_REQUEST_IGNORED:LX/DYS;

    invoke-virtual {v4, v5}, LX/DYS;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v4, 0x1

    :goto_4
    move v3, v4

    .line 2011423
    if-eqz v3, :cond_5

    .line 2011424
    new-instance v3, LX/DYC;

    iget-object v4, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->d:LX/DML;

    invoke-direct {v3, p0, v4, v0}, LX/DYC;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;LX/DML;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;)V

    move-object v0, v3

    .line 2011425
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2011426
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2011427
    iget-object v4, p3, LX/DYT;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p3, LX/DYT;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/DYS;

    sget-object v5, LX/DYS;->MEMBER_REQUEST_BLOCKED:LX/DYS;

    invoke-virtual {v4, v5}, LX/DYS;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const/4 v4, 0x1

    :goto_5
    move v3, v4

    .line 2011428
    if-eqz v3, :cond_6

    .line 2011429
    new-instance v3, LX/DYB;

    iget-object v4, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->e:LX/DML;

    invoke-direct {v3, p0, v4, v0}, LX/DYB;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;LX/DML;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;)V

    move-object v0, v3

    .line 2011430
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2011431
    :cond_6
    new-instance v4, LX/DY7;

    iget-object v6, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->c:LX/DML;

    move-object v5, p0

    move-object v7, v0

    move-object v8, p1

    move v9, p5

    invoke-direct/range {v4 .. v9}, LX/DY7;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;LX/DML;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;Z)V

    move-object v0, v4

    .line 2011432
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2011433
    :cond_7
    if-eqz p4, :cond_8

    .line 2011434
    new-instance v0, LX/DaP;

    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->b:LX/DML;

    invoke-direct {v0, v1}, LX/DaP;-><init>(LX/DML;)V

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2011435
    :cond_8
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->l:LX/0Px;

    .line 2011436
    const v0, 0x3abae045

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_0

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_a
    const/4 v4, 0x0

    goto :goto_4

    :cond_b
    const/4 v4, 0x0

    goto :goto_5
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2011396
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    .line 2011397
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->l:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2011395
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->l:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2011394
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2011393
    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->t:LX/0Px;

    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->l:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2011392
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
