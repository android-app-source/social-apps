.class public Lcom/facebook/groups/memberrequests/MemberRequestsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/DYM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DXz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DYT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DKo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

.field public h:Z

.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;

.field public k:Ljava/lang/String;

.field public l:LX/DYU;

.field public m:Z

.field private n:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2011638
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2011618
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->b:LX/DXz;

    iget-object v2, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->k:Ljava/lang/String;

    .line 2011619
    iput-object v2, v0, LX/DXz;->c:Ljava/lang/String;

    .line 2011620
    const-class v3, LX/1ZF;

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1ZF;

    .line 2011621
    if-eqz v3, :cond_0

    .line 2011622
    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1ZF;->k_(Z)V

    .line 2011623
    const v4, 0x7f0830e4

    invoke-interface {v3, v4}, LX/1ZF;->x_(I)V

    .line 2011624
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f02020c

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 2011625
    iput-object v5, v4, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2011626
    move-object v4, v4

    .line 2011627
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f0830e5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2011628
    iput-object v5, v4, LX/108;->j:Ljava/lang/String;

    .line 2011629
    move-object v4, v4

    .line 2011630
    invoke-virtual {v4}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v4

    .line 2011631
    invoke-interface {v3, v4}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2011632
    new-instance v4, LX/DXw;

    invoke-direct {v4, v0, p0}, LX/DXw;-><init>(LX/DXz;Lcom/facebook/base/fragment/FbFragment;)V

    invoke-interface {v3, v4}, LX/1ZF;->a(LX/63W;)V

    .line 2011633
    :cond_0
    const v0, 0x7f0d15d2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/widget/listview/BetterListView;

    .line 2011634
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->g:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    iget-object v3, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->c:LX/DYT;

    const/4 v4, 0x1

    iget-boolean v5, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->h:Z

    move-object v2, v1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;->a(Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel;LX/0Px;LX/DYT;ZZ)V

    .line 2011635
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->g:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    invoke-virtual {v6, v0}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2011636
    new-instance v0, LX/DYW;

    invoke-direct {v0, p0}, LX/DYW;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsFragment;)V

    invoke-virtual {v6, v0}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2011637
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2011617
    const-string v0, "member_requests"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2011611
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2011612
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;

    const-class v3, LX/DYM;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DYM;

    invoke-static {v0}, LX/DXz;->b(LX/0QB;)LX/DXz;

    move-result-object v4

    check-cast v4, LX/DXz;

    invoke-static {v0}, LX/DYT;->b(LX/0QB;)LX/DYT;

    move-result-object v5

    check-cast v5, LX/DYT;

    invoke-static {v0}, LX/DKo;->b(LX/0QB;)LX/DKo;

    move-result-object v6

    check-cast v6, LX/DKo;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p1

    check-cast p1, LX/0tX;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v0

    check-cast v0, LX/17W;

    iput-object v3, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->a:LX/DYM;

    iput-object v4, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->b:LX/DXz;

    iput-object v5, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->c:LX/DYT;

    iput-object v6, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->d:LX/DKo;

    iput-object p1, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->e:LX/0tX;

    iput-object v0, v2, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->f:LX/17W;

    .line 2011613
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v0

    .line 2011614
    const-string v0, "group_feed_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->k:Ljava/lang/String;

    .line 2011615
    const-string v0, "groups_single_page_mode"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->n:Z

    .line 2011616
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x5b41b8d9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2011595
    const v1, 0x7f03085d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x314b4238

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x55c3747f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2011608
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2011609
    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->d:LX/DKo;

    invoke-virtual {v1}, LX/DKo;->c()V

    .line 2011610
    const/16 v1, 0x2b

    const v2, -0x46b414e3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2011596
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2011597
    new-instance v0, LX/DYU;

    invoke-direct {v0, p0}, LX/DYU;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->l:LX/DYU;

    .line 2011598
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->d:LX/DKo;

    new-instance v1, LX/DYV;

    invoke-direct {v1, p0}, LX/DYV;-><init>(Lcom/facebook/groups/memberrequests/MemberRequestsFragment;)V

    invoke-virtual {v0, v1}, LX/DKo;->a(LX/DKm;)V

    .line 2011599
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->d:LX/DKo;

    invoke-virtual {v0}, LX/DKo;->a()V

    .line 2011600
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2011601
    const-string v1, "group_request_member_header_visible"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2011602
    iget-object v1, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->a:LX/DYM;

    iget-object v2, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->l:LX/DYU;

    .line 2011603
    new-instance v3, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    invoke-static {v1}, LX/DXz;->b(LX/0QB;)LX/DXz;

    move-result-object v6

    check-cast v6, LX/DXz;

    invoke-static {v1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v1}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v8

    check-cast v8, LX/11R;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    invoke-static {v1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    move-object v4, v2

    move v5, v0

    invoke-direct/range {v3 .. v11}, Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;-><init>(LX/DYU;ZLX/DXz;LX/17Y;LX/11R;Lcom/facebook/content/SecureContextHelper;Landroid/content/res/Resources;Ljava/lang/Boolean;)V

    .line 2011604
    move-object v0, v3

    .line 2011605
    iput-object v0, p0, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->g:Lcom/facebook/groups/memberrequests/MemberRequestsAdapter;

    .line 2011606
    invoke-direct {p0, p1}, Lcom/facebook/groups/memberrequests/MemberRequestsFragment;->b(Landroid/view/View;)V

    .line 2011607
    return-void
.end method
