.class public final Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2f48bd0c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2012146
    const-class v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2012145
    const-class v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2012143
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2012144
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2012133
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2012134
    invoke-virtual {p0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2012135
    invoke-virtual {p0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2012136
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2012137
    iget v2, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->e:I

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 2012138
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2012139
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2012140
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->h:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2012141
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2012142
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2012120
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2012121
    invoke-virtual {p0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2012122
    invoke-virtual {p0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    .line 2012123
    invoke-virtual {p0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2012124
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

    .line 2012125
    iput-object v0, v1, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->f:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    .line 2012126
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2012127
    invoke-virtual {p0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    .line 2012128
    invoke-virtual {p0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2012129
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

    .line 2012130
    iput-object v0, v1, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->g:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    .line 2012131
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2012132
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2012119
    invoke-virtual {p0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2012115
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2012116
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->e:I

    .line 2012117
    const/4 v0, 0x3

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->h:J

    .line 2012118
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2012102
    new-instance v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;-><init>()V

    .line 2012103
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2012104
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2012114
    const v0, -0x42836234

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2012113
    const v0, -0x33f6e2

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2012111
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2012112
    iget v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->e:I

    return v0
.end method

.method public final k()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2012109
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->f:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    iput-object v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->f:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    .line 2012110
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->f:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$InviterModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2012107
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->g:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->g:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    .line 2012108
    iget-object v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->g:Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final m()J
    .locals 2

    .prologue
    .line 2012105
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2012106
    iget-wide v0, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$FetchMemberRequestsModel$AdminAwareGroupModel$GroupPendingMembersModel$EdgesModel;->h:J

    return-wide v0
.end method
