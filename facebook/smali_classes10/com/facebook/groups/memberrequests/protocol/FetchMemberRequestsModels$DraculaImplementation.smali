.class public final Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2011739
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2011740
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2011791
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2011792
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2011742
    if-nez p1, :cond_0

    .line 2011743
    :goto_0
    return v0

    .line 2011744
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2011745
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2011746
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2011747
    const v2, -0x6e004eb6

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2011748
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2011749
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2011750
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2011751
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2011752
    const v2, 0x461eacb4

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2011753
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2011754
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2011755
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2011756
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2011757
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2011758
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2011759
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2011760
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2011761
    :sswitch_3
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2011762
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2011763
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2011764
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2011765
    :sswitch_4
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2011766
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2011767
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2011768
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2011769
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2011770
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2011771
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2011772
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2011773
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2011774
    :sswitch_6
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2011775
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2011776
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 2011777
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 2011778
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2011779
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2011780
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2011781
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2011782
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 2011783
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 2011784
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 2011785
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2011786
    :sswitch_7
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2011787
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2011788
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2011789
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2011790
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6e004eb6 -> :sswitch_1
        -0x6ce9a97e -> :sswitch_3
        -0x51c07e92 -> :sswitch_5
        -0x3bfc2f87 -> :sswitch_7
        -0x394a70a7 -> :sswitch_4
        -0x34dcea97 -> :sswitch_6
        -0x31779e83 -> :sswitch_0
        0x461eacb4 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2011741
    new-instance v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2011732
    sparse-switch p2, :sswitch_data_0

    .line 2011733
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2011734
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2011735
    const v1, -0x6e004eb6

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 2011736
    :goto_0
    :sswitch_1
    return-void

    .line 2011737
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2011738
    const v1, 0x461eacb4

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6e004eb6 -> :sswitch_2
        -0x6ce9a97e -> :sswitch_1
        -0x51c07e92 -> :sswitch_1
        -0x3bfc2f87 -> :sswitch_1
        -0x394a70a7 -> :sswitch_1
        -0x34dcea97 -> :sswitch_1
        -0x31779e83 -> :sswitch_0
        0x461eacb4 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2011726
    if-eqz p1, :cond_0

    .line 2011727
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;

    move-result-object v1

    .line 2011728
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;

    .line 2011729
    if-eq v0, v1, :cond_0

    .line 2011730
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2011731
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2011725
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2011723
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2011724
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2011793
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2011794
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2011795
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->a:LX/15i;

    .line 2011796
    iput p2, p0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->b:I

    .line 2011797
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2011704
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2011703
    new-instance v0, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2011700
    iget v0, p0, LX/1vt;->c:I

    .line 2011701
    move v0, v0

    .line 2011702
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2011697
    iget v0, p0, LX/1vt;->c:I

    .line 2011698
    move v0, v0

    .line 2011699
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2011720
    iget v0, p0, LX/1vt;->b:I

    .line 2011721
    move v0, v0

    .line 2011722
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2011705
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2011706
    move-object v0, v0

    .line 2011707
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2011708
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2011709
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2011710
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2011711
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2011712
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2011713
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2011714
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2011715
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberrequests/protocol/FetchMemberRequestsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2011716
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2011717
    iget v0, p0, LX/1vt;->c:I

    .line 2011718
    move v0, v0

    .line 2011719
    return v0
.end method
