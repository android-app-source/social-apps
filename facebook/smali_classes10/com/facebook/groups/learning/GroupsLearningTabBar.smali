.class public Lcom/facebook/groups/learning/GroupsLearningTabBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/DRb;

.field private b:LX/5O6;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1998395
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1998396
    invoke-direct {p0}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->a()V

    .line 1998397
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1998392
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1998393
    invoke-direct {p0}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->a()V

    .line 1998394
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1998389
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1998390
    invoke-direct {p0}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->a()V

    .line 1998391
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1998381
    const v0, 0x7f03081b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1998382
    new-instance v0, LX/5O6;

    invoke-virtual {p0}, Lcom/facebook/groups/learning/GroupsLearningTabBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/5O6;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->b:LX/5O6;

    .line 1998383
    const v0, 0x7f0d1543

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->c:Landroid/view/View;

    .line 1998384
    iget-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->c:Landroid/view/View;

    new-instance v1, LX/DRY;

    invoke-direct {v1, p0}, LX/DRY;-><init>(Lcom/facebook/groups/learning/GroupsLearningTabBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1998385
    const v0, 0x7f0d1544

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->d:Landroid/view/View;

    .line 1998386
    iget-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->d:Landroid/view/View;

    new-instance v1, LX/DRZ;

    invoke-direct {v1, p0}, LX/DRZ;-><init>(Lcom/facebook/groups/learning/GroupsLearningTabBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1998387
    iget-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->c:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1998388
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1998377
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1998378
    iget-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->b:LX/5O6;

    invoke-virtual {v0, p1}, LX/5O6;->a(Landroid/graphics/Canvas;)V

    .line 1998379
    iget-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->b:LX/5O6;

    invoke-virtual {v0, p1}, LX/5O6;->b(Landroid/graphics/Canvas;)V

    .line 1998380
    return-void
.end method

.method public setOnTabChangeListener(LX/DRb;)V
    .locals 0

    .prologue
    .line 1998369
    iput-object p1, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->a:LX/DRb;

    .line 1998370
    return-void
.end method

.method public setSelectedTab(LX/DRa;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1998371
    sget-object v0, LX/DRa;->LEARNING_UNITS:LX/DRa;

    if-ne p1, v0, :cond_0

    .line 1998372
    iget-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 1998373
    iget-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1998374
    :goto_0
    return-void

    .line 1998375
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1998376
    iget-object v0, p0, Lcom/facebook/groups/learning/GroupsLearningTabBar;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0
.end method
