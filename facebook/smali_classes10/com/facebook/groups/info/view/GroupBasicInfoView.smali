.class public Lcom/facebook/groups/info/view/GroupBasicInfoView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public l:Lcom/facebook/widget/text/BetterTextView;

.field public m:Lcom/facebook/widget/text/BetterTextView;

.field public n:Landroid/widget/ImageView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1998274
    const-class v0, Lcom/facebook/groups/info/view/GroupBasicInfoView;

    const-string v1, "group_info"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1998275
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1998276
    const p1, 0x7f0307f7

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1998277
    const p1, 0x7f0d14f3

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1998278
    const p1, 0x7f0d14f5

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 1998279
    const p1, 0x7f0d14f6

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 1998280
    const p1, 0x7f0d14f4

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/facebook/groups/info/view/GroupBasicInfoView;->n:Landroid/widget/ImageView;

    .line 1998281
    return-void
.end method
