.class public Lcom/facebook/groups/info/GroupInfoFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0hF;


# instance fields
.field public a:LX/DZ7;
    .annotation runtime Lcom/facebook/groups/info/navigation/GroupInfoNavHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DQZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DQP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DQG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DZH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/common/shortcuts/InstallShortcutHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DMZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/DPp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DKH;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final m:LX/DQQ;

.field private final n:LX/DMP;

.field private final o:LX/DMR;

.field public p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

.field private q:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field private r:Ljava/lang/String;

.field public s:Lcom/facebook/groups/info/GroupInfoAdapter;

.field private t:Lcom/facebook/widget/listview/BetterListView;

.field private u:Landroid/view/View;

.field private v:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1994630
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1994631
    new-instance v0, LX/DQR;

    invoke-direct {v0, p0}, LX/DQR;-><init>(Lcom/facebook/groups/info/GroupInfoFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->m:LX/DQQ;

    .line 1994632
    new-instance v0, LX/DQS;

    invoke-direct {v0, p0}, LX/DQS;-><init>(Lcom/facebook/groups/info/GroupInfoFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->n:LX/DMP;

    .line 1994633
    new-instance v0, LX/DQT;

    invoke-direct {v0, p0}, LX/DQT;-><init>(Lcom/facebook/groups/info/GroupInfoFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->o:LX/DMR;

    return-void
.end method

.method private a(Z)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 1994604
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mDetached:Z

    move v0, v0

    .line 1994605
    if-nez v0, :cond_2

    .line 1994606
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->f:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    invoke-virtual {v0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    .line 1994607
    if-eqz p1, :cond_3

    sget-object v7, LX/DO3;->a:LX/0zS;

    .line 1994608
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->c:LX/DQP;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->r:Ljava/lang/String;

    const-string v2, "2"

    const-string v4, "6"

    new-instance v8, LX/DQU;

    invoke-direct {v8, p0}, LX/DQU;-><init>(Lcom/facebook/groups/info/GroupInfoFragment;)V

    move-object v5, v3

    .line 1994609
    new-instance v9, LX/DQj;

    invoke-direct {v9}, LX/DQj;-><init>()V

    move-object v9, v9

    .line 1994610
    const-string v10, "group_id"

    invoke-virtual {v9, v10, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1994611
    const-string v10, "group_events_page_size"

    invoke-virtual {v9, v10, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1994612
    const-string v10, "group_mediaset_page_size"

    invoke-virtual {v9, v10, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1994613
    const-string v10, "photo_for_launcher_shortcut_size"

    invoke-virtual {v9, v10, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1994614
    const-string v10, "cover_photo_size"

    iget-object v11, v0, LX/DQP;->d:Landroid/content/res/Resources;

    const v12, 0x7f0b202c

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1994615
    const-string v10, "profile_image_size"

    iget-object v11, v0, LX/DQP;->d:Landroid/content/res/Resources;

    const v12, 0x7f0b2032

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1994616
    if-eqz v3, :cond_0

    .line 1994617
    const-string v10, "group_events_page_cursor"

    invoke-virtual {v9, v10, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1994618
    :cond_0
    if-eqz v5, :cond_1

    .line 1994619
    const-string v10, "group_mediaset_page_cursor"

    invoke-virtual {v9, v10, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1994620
    :cond_1
    const-string v10, "automatic_photo_captioning_enabled"

    iget-object v11, v0, LX/DQP;->e:LX/0sX;

    invoke-virtual {v11}, LX/0sX;->a()Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1994621
    invoke-static {v9}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v10

    invoke-virtual {v10, v7}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v10

    const/4 v11, 0x1

    .line 1994622
    iput-boolean v11, v10, LX/0zO;->p:Z

    .line 1994623
    move-object v10, v10

    .line 1994624
    const-wide/32 v11, 0x93a80

    invoke-virtual {v10, v11, v12}, LX/0zO;->a(J)LX/0zO;

    move-result-object v10

    .line 1994625
    iget-object v11, v0, LX/DQP;->c:LX/0se;

    invoke-virtual {v11, v9}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 1994626
    iget-object v9, v0, LX/DQP;->a:LX/1My;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "GROUP_INFO_DATA_FETCHER_"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v8, v11}, LX/1My;->a(LX/0zO;LX/0TF;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    .line 1994627
    iget-object v10, v0, LX/DQP;->b:LX/0Sh;

    invoke-virtual {v10, v9, v8}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1994628
    :cond_2
    return-void

    .line 1994629
    :cond_3
    sget-object v7, LX/0zS;->d:LX/0zS;

    goto/16 :goto_0
.end method

.method public static a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;LX/7vF;Z)V
    .locals 8

    .prologue
    .line 1994417
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1994418
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v2

    .line 1994419
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1994420
    invoke-virtual {v2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;->j()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;

    .line 1994421
    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, LX/7vF;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1994422
    new-instance v6, LX/DQc;

    invoke-direct {v6}, LX/DQc;-><init>()V

    .line 1994423
    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v7

    iput-object v7, v6, LX/DQc;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1994424
    move-object v6, v6

    .line 1994425
    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/7vF;->a(Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    .line 1994426
    iput-object v0, v6, LX/DQc;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1994427
    move-object v0, v6

    .line 1994428
    invoke-virtual {v0}, LX/DQc;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994429
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1994430
    :cond_0
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1994431
    :cond_1
    new-instance v0, LX/DQb;

    invoke-direct {v0}, LX/DQb;-><init>()V

    invoke-static {v2}, LX/DQb;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;)LX/DQb;

    move-result-object v0

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1994432
    iput-object v1, v0, LX/DQb;->b:LX/0Px;

    .line 1994433
    move-object v0, v0

    .line 1994434
    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {v1}, LX/DQq;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DQq;

    move-result-object v1

    invoke-virtual {v0}, LX/DQb;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v0

    .line 1994435
    iput-object v0, v1, LX/DQq;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    .line 1994436
    move-object v0, v1

    .line 1994437
    invoke-virtual {v0}, LX/DQq;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/facebook/groups/info/GroupInfoFragment;->a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Z)V

    .line 1994438
    :cond_2
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V
    .locals 1

    .prologue
    .line 1994602
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/facebook/groups/info/GroupInfoFragment;->a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Z)V

    .line 1994603
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/info/GroupInfoFragment;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Z)V
    .locals 3

    .prologue
    .line 1994597
    iput-object p1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1994598
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->b:LX/DQZ;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/DQZ;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Landroid/content/Context;)V

    .line 1994599
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->a:LX/DZ7;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->b:LX/DQZ;

    invoke-interface {v0, p0, v1}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;LX/DLO;)V

    .line 1994600
    invoke-direct {p0, p2}, Lcom/facebook/groups/info/GroupInfoFragment;->b(Z)V

    .line 1994601
    return-void
.end method

.method private b(Z)V
    .locals 12

    .prologue
    .line 1994479
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->s:Lcom/facebook/groups/info/GroupInfoAdapter;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1994480
    if-nez v1, :cond_1

    .line 1994481
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1994482
    iput-object v2, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->r:LX/0Px;

    .line 1994483
    const v2, 0x641ad0bc

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1994484
    :cond_0
    :goto_0
    return-void

    .line 1994485
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1994486
    invoke-static {v0, v1, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->i(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/0Pz;)V

    .line 1994487
    invoke-static {v0, v1, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/0Pz;)V

    .line 1994488
    new-instance v3, LX/DQD;

    sget-object v4, LX/DQN;->j:LX/DML;

    invoke-direct {v3, v0, v4, v1}, LX/DQD;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994489
    iget-boolean v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->n:Z

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->L()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1994490
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994491
    new-instance v3, LX/DQE;

    sget-object v4, LX/DQN;->j:LX/DML;

    invoke-direct {v3, v0, v4, v1}, LX/DQE;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994492
    :cond_2
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994493
    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v3, v4, :cond_4

    const/4 v3, 0x0

    .line 1994494
    if-nez v1, :cond_12

    .line 1994495
    :cond_3
    :goto_1
    move v3, v3

    .line 1994496
    if-eqz v3, :cond_11

    .line 1994497
    :cond_4
    :goto_2
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1994498
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v6

    if-eq v3, v6, :cond_5

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v6

    if-ne v3, v6, :cond_13

    :cond_5
    iget-object v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->s:LX/5QT;

    sget-object v6, LX/5QT;->WITH_TABS:LX/5QT;

    if-eq v3, v6, :cond_13

    move v3, v4

    .line 1994499
    :goto_3
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v7

    if-ne v6, v7, :cond_14

    .line 1994500
    :goto_4
    if-eqz v3, :cond_6

    .line 1994501
    const/4 v6, 0x0

    move v5, v6

    .line 1994502
    if-nez v5, :cond_15

    :cond_6
    if-nez v4, :cond_15

    .line 1994503
    :cond_7
    :goto_5
    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    if-eqz v3, :cond_9

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v3, v4, :cond_9

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->L()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-boolean v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->o:Z

    if-eqz v3, :cond_9

    :cond_8
    iget-object v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->q:LX/88k;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/88k;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 1994504
    :cond_9
    :goto_6
    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    if-eqz v3, :cond_a

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v3, v4, :cond_a

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eq v3, v4, :cond_18

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eq v3, v4, :cond_18

    .line 1994505
    :cond_a
    :goto_7
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v3, v4, :cond_b

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eq v3, v4, :cond_19

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eq v3, v4, :cond_19

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INFORMAL:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-eq v3, v4, :cond_19

    .line 1994506
    :cond_b
    :goto_8
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    if-eqz v3, :cond_c

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v3, v4, :cond_c

    iget-object v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->q:LX/88k;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/88k;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;)Z

    move-result v3

    if-nez v3, :cond_1a

    .line 1994507
    :cond_c
    :goto_9
    iget-object v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->s:LX/5QT;

    sget-object v4, LX/5QT;->WITH_TABS:LX/5QT;

    if-eq v3, v4, :cond_f

    .line 1994508
    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    .line 1994509
    :cond_d
    :goto_a
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1994510
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v6

    if-eq v5, v6, :cond_21

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v6

    if-eq v5, v6, :cond_21

    .line 1994511
    :cond_e
    :goto_b
    move v3, v3

    .line 1994512
    if-eqz v3, :cond_20

    .line 1994513
    :goto_c
    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v5

    .line 1994514
    if-nez v5, :cond_24

    .line 1994515
    :cond_f
    :goto_d
    const/4 v3, 0x1

    .line 1994516
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v5

    if-eq v4, v5, :cond_2a

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v5

    if-eq v4, v5, :cond_2a

    .line 1994517
    :cond_10
    :goto_e
    move v3, v3

    .line 1994518
    if-eqz v3, :cond_29

    .line 1994519
    :goto_f
    invoke-static {v0, v1, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->h(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/0Pz;)V

    .line 1994520
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->r:LX/0Px;

    .line 1994521
    if-eqz p1, :cond_0

    .line 1994522
    const v2, 0x5732b0d3

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto/16 :goto_0

    .line 1994523
    :cond_11
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994524
    new-instance v3, LX/DQ2;

    sget-object v4, LX/DQN;->n:LX/DML;

    invoke-direct {v3, v0, v4, v1}, LX/DQ2;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994525
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2

    .line 1994526
    :cond_12
    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->E()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v4

    .line 1994527
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1994528
    invoke-virtual {v4}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/groups/grouppurposes/protocol/FetchGroupPurposesQueryModels$GroupPurposeFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->name()Ljava/lang/String;

    move-result-object v3

    .line 1994529
    const-string v4, "REAL_WORLD"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto/16 :goto_1

    :cond_13
    move v3, v5

    .line 1994530
    goto/16 :goto_3

    :cond_14
    move v4, v5

    .line 1994531
    goto/16 :goto_4

    .line 1994532
    :cond_15
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994533
    goto :goto_10

    .line 1994534
    :goto_10
    if-eqz v4, :cond_7

    .line 1994535
    iget-object v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v4, 0x7f08305c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/DQO;->ADD_TO_HOME_SCREEN:LX/DQO;

    invoke-static {v0, v3, v4, v1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994536
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_5

    .line 1994537
    :cond_16
    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1994538
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994539
    iget-object v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v4, 0x7f081b4e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/DQO;->CREATE_GROUP_CHAT:LX/DQO;

    invoke-static {v0, v3, v4, v1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994540
    :cond_17
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_6

    .line 1994541
    :cond_18
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994542
    iget-object v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v4, 0x7f081b50

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/DQO;->CREATE_SUBGROUP:LX/DQO;

    invoke-static {v0, v3, v4, v1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994543
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_7

    .line 1994544
    :cond_19
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994545
    iget-object v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v4, 0x7f081b51

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/DQO;->BROWSE_ALL_SUBGROUPS:LX/DQO;

    invoke-static {v0, v3, v4, v1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994546
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_8

    .line 1994547
    :cond_1a
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994548
    iget-object v3, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v4, 0x7f081b52

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/DQO;->CHANNELS:LX/DQO;

    invoke-static {v0, v3, v4, v1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994549
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_9

    .line 1994550
    :cond_1b
    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    move-result-object v4

    .line 1994551
    if-eqz v4, :cond_d

    .line 1994552
    invoke-virtual {v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;->j()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v3

    if-nez v3, :cond_1d

    const/4 v3, 0x0

    .line 1994553
    :goto_11
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994554
    sget-object v5, LX/DQO;->PHOTOS:LX/DQO;

    invoke-static {v0, v5, v1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a$redex0(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 1994555
    if-eqz v3, :cond_1c

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 1994556
    :cond_1c
    new-instance v3, LX/DPw;

    sget-object v4, LX/DQN;->o:LX/DML;

    invoke-direct {v3, v0, v4, v5}, LX/DPw;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994557
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_a

    .line 1994558
    :cond_1d
    invoke-virtual {v4}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;->j()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel;->a()LX/0Px;

    move-result-object v3

    goto :goto_11

    .line 1994559
    :cond_1e
    new-instance v6, LX/DPx;

    sget-object v7, LX/DQN;->o:LX/DML;

    invoke-direct {v6, v0, v7, v5}, LX/DPx;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994560
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994561
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_1f

    .line 1994562
    new-instance v5, LX/DPy;

    .line 1994563
    sget-object v7, LX/DQN;->d:LX/DML;

    move-object v6, v7

    .line 1994564
    invoke-direct {v5, v0, v6, v3, v4}, LX/DPy;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;LX/0Px;Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994565
    :cond_1f
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_a

    .line 1994566
    :cond_20
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994567
    new-instance v3, LX/DPr;

    sget-object v4, LX/DQN;->o:LX/DML;

    invoke-direct {v3, v0, v4}, LX/DPr;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994568
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_c

    .line 1994569
    :cond_21
    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-nez v5, :cond_22

    move v5, v3

    :goto_12
    if-nez v5, :cond_e

    iget-object v5, v0, Lcom/facebook/groups/info/GroupInfoAdapter;->l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v5, :cond_e

    move v3, v4

    .line 1994570
    goto/16 :goto_b

    .line 1994571
    :cond_22
    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 1994572
    invoke-virtual {v6, v5, v4}, LX/15i;->j(II)I

    move-result v5

    if-nez v5, :cond_23

    move v5, v3

    goto :goto_12

    :cond_23
    move v5, v4

    goto :goto_12

    .line 1994573
    :cond_24
    invoke-virtual {v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;->j()LX/0Px;

    move-result-object v11

    .line 1994574
    sget-object v6, LX/DQO;->EVENTS:LX/DQO;

    invoke-static {v0, v6, v1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a$redex0(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v6

    .line 1994575
    new-instance v7, LX/DPs;

    sget-object v8, LX/DQN;->o:LX/DML;

    invoke-direct {v7, v0, v8, v6}, LX/DPs;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Landroid/view/View$OnClickListener;)V

    .line 1994576
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994577
    invoke-virtual {v11}, LX/0Px;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_25

    .line 1994578
    invoke-virtual {v2, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994579
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_d

    .line 1994580
    :cond_25
    invoke-virtual {v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;->a()I

    move-result v5

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v8

    if-le v5, v8, :cond_27

    .line 1994581
    new-instance v5, LX/DPt;

    sget-object v7, LX/DQN;->o:LX/DML;

    invoke-direct {v5, v0, v7, v6}, LX/DPt;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994582
    :goto_13
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994583
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->c(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994584
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v10

    .line 1994585
    const/4 v9, 0x0

    :goto_14
    if-ge v9, v10, :cond_28

    .line 1994586
    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;

    .line 1994587
    if-eqz v8, :cond_26

    .line 1994588
    new-instance v5, LX/DPv;

    sget-object v7, LX/DQN;->m:LX/DML;

    move-object v6, v0

    invoke-direct/range {v5 .. v10}, LX/DPv;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;II)V

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994589
    :cond_26
    add-int/lit8 v9, v9, 0x1

    goto :goto_14

    .line 1994590
    :cond_27
    invoke-virtual {v2, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_13

    .line 1994591
    :cond_28
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_d

    .line 1994592
    :cond_29
    invoke-static {v0, v2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994593
    new-instance v3, LX/DQF;

    sget-object v4, LX/DQN;->o:LX/DML;

    invoke-direct {v3, v0, v4, v1}, LX/DQF;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994594
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_f

    .line 1994595
    :cond_2a
    invoke-virtual {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1994596
    const/4 v3, 0x0

    goto/16 :goto_e
.end method

.method public static c(Lcom/facebook/groups/info/GroupInfoFragment;Z)V
    .locals 2

    .prologue
    .line 1994475
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->u:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1994476
    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->u:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1994477
    :cond_0
    return-void

    .line 1994478
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1994474
    const-string v0, "group_info"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 1994465
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1994466
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/info/GroupInfoFragment;

    invoke-static {v0}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object v3

    check-cast v3, LX/DZ7;

    new-instance v7, LX/DQa;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v7, v4, v5, v6}, LX/DQa;-><init>(LX/0Zb;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;)V

    move-object v4, v7

    check-cast v4, LX/DQZ;

    invoke-static {v0}, LX/DQP;->b(LX/0QB;)LX/DQP;

    move-result-object v5

    check-cast v5, LX/DQP;

    const-class v6, LX/DQG;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/DQG;

    invoke-static {v0}, LX/DZI;->b(LX/0QB;)LX/DZI;

    move-result-object v7

    check-cast v7, LX/DZH;

    invoke-static {v0}, Lcom/facebook/common/shortcuts/InstallShortcutHelper;->b(LX/0QB;)Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    move-result-object v8

    check-cast v8, Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    invoke-static {v0}, LX/DMZ;->b(LX/0QB;)LX/DMZ;

    move-result-object v9

    check-cast v9, LX/DMZ;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    invoke-static {v0}, LX/DPp;->b(LX/0QB;)LX/DPp;

    move-result-object v11

    check-cast v11, LX/DPp;

    const/16 v12, 0x2390

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 p1, 0x12c4

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v0

    check-cast v0, LX/1g8;

    iput-object v3, v2, Lcom/facebook/groups/info/GroupInfoFragment;->a:LX/DZ7;

    iput-object v4, v2, Lcom/facebook/groups/info/GroupInfoFragment;->b:LX/DQZ;

    iput-object v5, v2, Lcom/facebook/groups/info/GroupInfoFragment;->c:LX/DQP;

    iput-object v6, v2, Lcom/facebook/groups/info/GroupInfoFragment;->d:LX/DQG;

    iput-object v7, v2, Lcom/facebook/groups/info/GroupInfoFragment;->e:LX/DZH;

    iput-object v8, v2, Lcom/facebook/groups/info/GroupInfoFragment;->f:Lcom/facebook/common/shortcuts/InstallShortcutHelper;

    iput-object v9, v2, Lcom/facebook/groups/info/GroupInfoFragment;->g:LX/DMZ;

    iput-object v10, v2, Lcom/facebook/groups/info/GroupInfoFragment;->h:Landroid/content/res/Resources;

    iput-object v11, v2, Lcom/facebook/groups/info/GroupInfoFragment;->i:LX/DPp;

    iput-object v12, v2, Lcom/facebook/groups/info/GroupInfoFragment;->j:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/groups/info/GroupInfoFragment;->k:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/groups/info/GroupInfoFragment;->l:LX/1g8;

    .line 1994467
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1994468
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->r:Ljava/lang/String;

    .line 1994469
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1994470
    const-string v1, "group_mall_type"

    sget-object v2, LX/5QT;->WITHOUT_TABS_LEGACY:LX/5QT;

    invoke-virtual {v2}, LX/5QT;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->v:I

    .line 1994471
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1994472
    const-string v1, "group_feed_model"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->q:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1994473
    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1994458
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->r:Ljava/lang/String;

    .line 1994459
    if-nez v0, :cond_0

    .line 1994460
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1994461
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1994462
    :cond_0
    if-nez v0, :cond_1

    .line 1994463
    const/4 v0, 0x0

    .line 1994464
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "group_id"

    invoke-static {v1, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1994451
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1994452
    const/16 v0, 0x7aa

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1994453
    :cond_0
    :goto_0
    return-void

    .line 1994454
    :cond_1
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1994455
    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/DKH;

    iget-object v2, p0, Lcom/facebook/groups/info/GroupInfoFragment;->r:Ljava/lang/String;

    .line 1994456
    iget-object p0, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, p0

    .line 1994457
    invoke-virtual {v1, v2, v0}, LX/DKH;->a(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2ab6c979

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1994450
    const v1, 0x7f030810

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x745e63cc

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x71893e39

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1994446
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1994447
    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->c:LX/DQP;

    .line 1994448
    iget-object v2, v1, LX/DQP;->a:LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 1994449
    const/16 v1, 0x2b

    const v2, -0x6a0f6a4a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5a3a51f2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1994443
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1994444
    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->g:LX/DMZ;

    iget-object v2, p0, Lcom/facebook/groups/info/GroupInfoFragment;->o:LX/DMR;

    invoke-virtual {v1, v2}, LX/DMZ;->b(LX/DMR;)V

    .line 1994445
    const/16 v1, 0x2b

    const v2, -0x728fec84

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5f783ae7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1994439
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1994440
    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->c:LX/DQP;

    .line 1994441
    iget-object v2, v1, LX/DQP;->a:LX/1My;

    invoke-virtual {v2}, LX/1My;->d()V

    .line 1994442
    const/16 v1, 0x2b

    const v2, 0x30a710b4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x14b7a4b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1994413
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1994414
    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->c:LX/DQP;

    .line 1994415
    iget-object v2, v1, LX/DQP;->a:LX/1My;

    invoke-virtual {v2}, LX/1My;->e()V

    .line 1994416
    const/16 v1, 0x2b

    const v2, -0xbbf3b3e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1994389
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1994390
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->g:LX/DMZ;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->o:LX/DMR;

    .line 1994391
    iput-object v1, v0, LX/DMZ;->f:LX/DMR;

    .line 1994392
    const v0, 0x7f0d04af

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    .line 1994393
    const v0, 0x7f0d01f8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->u:Landroid/view/View;

    .line 1994394
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->e:LX/DZH;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->m:LX/DQQ;

    .line 1994395
    iput-object v1, v0, LX/DZH;->h:LX/DQQ;

    .line 1994396
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->b:LX/DQZ;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->p:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/DQZ;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;Landroid/content/Context;)V

    .line 1994397
    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->a:LX/DZ7;

    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->q:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->q:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1994398
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COLLEGE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->H()Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$GroupHeaderInformationModel$AdminAwareGroupModel;->b()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->CITY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 1994399
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->h:Landroid/content/res/Resources;

    const v2, 0x7f08305e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/facebook/groups/info/GroupInfoFragment;->b:LX/DQZ;

    invoke-interface {v1, p0, v0, v2}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 1994400
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->d:LX/DQG;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/groups/info/GroupInfoFragment;->n:LX/DMP;

    iget-object v4, p0, Lcom/facebook/groups/info/GroupInfoFragment;->i:LX/DPp;

    invoke-static {}, LX/5QT;->values()[LX/5QT;

    move-result-object v5

    iget v6, p0, Lcom/facebook/groups/info/GroupInfoFragment;->v:I

    aget-object v5, v5, v6

    iget-object v6, p0, Lcom/facebook/groups/info/GroupInfoFragment;->q:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual/range {v0 .. v6}, LX/DQG;->a(Lcom/facebook/widget/listview/BetterListView;LX/0gc;LX/DMP;LX/DPp;LX/5QT;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;)Lcom/facebook/groups/info/GroupInfoAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->s:Lcom/facebook/groups/info/GroupInfoAdapter;

    .line 1994401
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->s:Lcom/facebook/groups/info/GroupInfoAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1994402
    invoke-static {p0, v7}, Lcom/facebook/groups/info/GroupInfoFragment;->c(Lcom/facebook/groups/info/GroupInfoFragment;Z)V

    .line 1994403
    invoke-direct {p0, v7}, Lcom/facebook/groups/info/GroupInfoFragment;->b(Z)V

    .line 1994404
    invoke-direct {p0, v7}, Lcom/facebook/groups/info/GroupInfoFragment;->a(Z)V

    .line 1994405
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->l:LX/1g8;

    iget-object v1, p0, Lcom/facebook/groups/info/GroupInfoFragment;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/info/GroupInfoFragment;->q:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    invoke-virtual {v2}, Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    .line 1994406
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "admin_panel_group_info_view"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "group_info"

    .line 1994407
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1994408
    move-object v3, v3

    .line 1994409
    const-string v4, "group_id"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "viewer_admin_type"

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1994410
    iget-object v4, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1994411
    return-void

    .line 1994412
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoFragment;->h:Landroid/content/res/Resources;

    const v2, 0x7f08305d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
