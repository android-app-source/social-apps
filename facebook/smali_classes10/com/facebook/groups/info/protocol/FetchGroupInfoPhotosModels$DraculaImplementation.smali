.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1997629
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1997630
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1997631
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1997632
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1997633
    if-nez p1, :cond_0

    .line 1997634
    :goto_0
    return v0

    .line 1997635
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1997636
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1997637
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1997638
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1997639
    invoke-virtual {p0, p1, v6}, LX/15i;->b(II)Z

    move-result v2

    .line 1997640
    invoke-virtual {p0, p1, v7}, LX/15i;->b(II)Z

    move-result v3

    .line 1997641
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1997642
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1997643
    const/4 v5, 0x4

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1997644
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1997645
    invoke-virtual {p3, v6, v2}, LX/186;->a(IZ)V

    .line 1997646
    invoke-virtual {p3, v7, v3}, LX/186;->a(IZ)V

    .line 1997647
    invoke-virtual {p3, v8, v4}, LX/186;->b(II)V

    .line 1997648
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1654130f
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1997649
    new-instance v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1997653
    packed-switch p0, :pswitch_data_0

    .line 1997654
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1997655
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x1654130f
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1997650
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1997651
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;->b(I)V

    .line 1997652
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1997624
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1997625
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1997626
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;->a:LX/15i;

    .line 1997627
    iput p2, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;->b:I

    .line 1997628
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1997623
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1997598
    new-instance v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1997620
    iget v0, p0, LX/1vt;->c:I

    .line 1997621
    move v0, v0

    .line 1997622
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1997617
    iget v0, p0, LX/1vt;->c:I

    .line 1997618
    move v0, v0

    .line 1997619
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1997614
    iget v0, p0, LX/1vt;->b:I

    .line 1997615
    move v0, v0

    .line 1997616
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1997611
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1997612
    move-object v0, v0

    .line 1997613
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1997602
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1997603
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1997604
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1997605
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1997606
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1997607
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1997608
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1997609
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1997610
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1997599
    iget v0, p0, LX/1vt;->c:I

    .line 1997600
    move v0, v0

    .line 1997601
    return v0
.end method
