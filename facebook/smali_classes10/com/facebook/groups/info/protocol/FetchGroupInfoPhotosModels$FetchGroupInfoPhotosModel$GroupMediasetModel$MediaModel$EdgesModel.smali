.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xee61028
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1997858
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1997857
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1997855
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1997856
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1997849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1997850
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1997851
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1997852
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1997853
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1997854
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1997841
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1997842
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1997843
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    .line 1997844
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1997845
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;

    .line 1997846
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    .line 1997847
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1997848
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1997839
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    .line 1997840
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1997834
    new-instance v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel;-><init>()V

    .line 1997835
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1997836
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1997838
    const v0, 0x19c17918    # 2.0004642E-23f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1997837
    const v0, 0x44113f63

    return v0
.end method
