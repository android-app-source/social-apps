.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/5A1;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/9Mz;
.implements LX/DQo;
.implements LX/DQl;
.implements LX/DQp;
.implements LX/DQm;
.implements LX/DQn;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5a3b092d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$Serializer;
.end annotation


# instance fields
.field private A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$GroupTopicTagsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Z

.field private C:Z

.field private D:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private E:Z

.field private F:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private G:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:Z

.field private Q:Z

.field private R:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private U:Z

.field private V:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private W:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private X:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Y:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Z:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ab:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ac:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ad:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1996516
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1996596
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1996594
    const/16 v0, 0x34

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1996595
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1996591
    const/16 v0, 0x34

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1996592
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1996593
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 1996585
    iput-wide p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->f:J

    .line 1996586
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996587
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996588
    if-eqz v0, :cond_0

    .line 1996589
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 1996590
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V
    .locals 4

    .prologue
    .line 1996578
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 1996579
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996580
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996581
    if-eqz v0, :cond_0

    .line 1996582
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x1b

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1996583
    :cond_0
    return-void

    .line 1996584
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 1996571
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->W:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1996572
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996573
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996574
    if-eqz v0, :cond_0

    .line 1996575
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x2c

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1996576
    :cond_0
    return-void

    .line 1996577
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V
    .locals 4

    .prologue
    .line 1996564
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 1996565
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996566
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996567
    if-eqz v0, :cond_0

    .line 1996568
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x24

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1996569
    :cond_0
    return-void

    .line 1996570
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V
    .locals 4

    .prologue
    .line 1996557
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Z:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 1996558
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996559
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996560
    if-eqz v0, :cond_0

    .line 1996561
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x2f

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1996562
    :cond_0
    return-void

    .line 1996563
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V
    .locals 4

    .prologue
    .line 1996503
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ab:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 1996504
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996505
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996506
    if-eqz v0, :cond_0

    .line 1996507
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x31

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1996508
    :cond_0
    return-void

    .line 1996509
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 1996548
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ac:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1996549
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996550
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996551
    if-eqz v0, :cond_0

    .line 1996552
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x32

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1996553
    :cond_0
    return-void

    .line 1996554
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 1996541
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1996542
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996543
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996544
    if-eqz v0, :cond_0

    .line 1996545
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x27

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1996546
    :cond_0
    return-void

    .line 1996547
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1996535
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->o:Ljava/lang/String;

    .line 1996536
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996537
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996538
    if-eqz v0, :cond_0

    .line 1996539
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1996540
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1996529
    iput-boolean p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B:Z

    .line 1996530
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996531
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996532
    if-eqz v0, :cond_0

    .line 1996533
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1996534
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1996523
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->G:Ljava/lang/String;

    .line 1996524
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996525
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996526
    if-eqz v0, :cond_0

    .line 1996527
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1996528
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1996517
    iput-boolean p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->P:Z

    .line 1996518
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1996519
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1996520
    if-eqz v0, :cond_0

    .line 1996521
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0x25

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1996522
    :cond_0
    return-void
.end method


# virtual methods
.method public final A()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupMediaset"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996514
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->t:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    const/16 v1, 0xf

    const-class v2, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->t:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    .line 1996515
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->t:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    return-object v0
.end method

.method public final B()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996512
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996513
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final C()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupPendingMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996510
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996511
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final D()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupPendingStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x2

    .line 1996601
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996602
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final E()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996599
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->x:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->x:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1996600
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->x:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    return-object v0
.end method

.method public final F()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupReportedStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996633
    const/4 v0, 0x2

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996634
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final G()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupSellConfig"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996631
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1996632
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    return-object v0
.end method

.method public final H()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$GroupTopicTagsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1996629
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A:Ljava/util/List;

    const/16 v1, 0x16

    const-class v2, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$GroupTopicTagsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A:Ljava/util/List;

    .line 1996630
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final I()Z
    .locals 2

    .prologue
    .line 1996627
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996628
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B:Z

    return v0
.end method

.method public final J()Z
    .locals 2

    .prologue
    .line 1996625
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996626
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->C:Z

    return v0
.end method

.method public final K()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996623
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->D:Ljava/lang/String;

    const/16 v1, 0x19

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->D:Ljava/lang/String;

    .line 1996624
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->D:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Z
    .locals 2

    .prologue
    .line 1996621
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996622
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->E:Z

    return v0
.end method

.method public final M()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996619
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    const/16 v1, 0x1b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 1996620
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    return-object v0
.end method

.method public final N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996635
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->H:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    const/16 v1, 0x1d

    const-class v2, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->H:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    .line 1996636
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->H:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    return-object v0
.end method

.method public final O()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhotoForLauncherShortcut"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996617
    const/4 v0, 0x3

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996618
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->I:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final P()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPossibleJoinApprovalSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996615
    const/4 v0, 0x3

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996616
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->J:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final Q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPossiblePostPermissionSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996613
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996614
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final R()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996611
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->L:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    const/16 v1, 0x21

    const-class v2, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->L:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    .line 1996612
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->L:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    return-object v0
.end method

.method public final S()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996609
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->M:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->M:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    .line 1996610
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->M:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    return-object v0
.end method

.method public final T()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPossibleVisibilitySettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996607
    const/4 v0, 0x4

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996608
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final U()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996605
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 1996606
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    return-object v0
.end method

.method public final V()Z
    .locals 2

    .prologue
    .line 1996603
    const/4 v0, 0x4

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996604
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->P:Z

    return v0
.end method

.method public final W()Z
    .locals 2

    .prologue
    .line 1996555
    const/4 v0, 0x4

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996556
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Q:Z

    return v0
.end method

.method public final X()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996597
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1996598
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method

.method public final Y()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSuggestedPurpose"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996462
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996463
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->S:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final Z()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996460
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->T:Ljava/lang/String;

    const/16 v1, 0x29

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->T:Ljava/lang/String;

    .line 1996461
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->T:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 46

    .prologue
    .line 1996365
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1996366
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->k()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1996367
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1996368
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x64b842c9

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1996369
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->t()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1996370
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1996371
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->x()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x1f84a18

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1996372
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1996373
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x7bbc1ce5

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1996374
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 1996375
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x18d312bf

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1996376
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->C()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x4509b6d8

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1996377
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->D()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x56796cf0

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v18

    .line 1996378
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->E()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1996379
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0xf69c7a9

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v20

    .line 1996380
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->G()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v21

    .line 1996381
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->H()LX/0Px;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v22

    .line 1996382
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1996383
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    .line 1996384
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 1996385
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v26

    .line 1996386
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x51ecfa16

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v27

    .line 1996387
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->P()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x4a0c7f15

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v28

    .line 1996388
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Q()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x44e15961

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1996389
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1996390
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->S()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1996391
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->T()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x22a1d027    # -1.00064086E18f

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v32

    .line 1996392
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->U()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v33

    .line 1996393
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->X()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v34

    .line 1996394
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Y()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x6da0c94

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 1996395
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Z()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v36

    .line 1996396
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v37

    .line 1996397
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v38

    .line 1996398
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ab()Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v39

    .line 1996399
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ac()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v40

    .line 1996400
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v41

    .line 1996401
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ky_()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v42

    .line 1996402
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v43

    .line 1996403
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v44

    .line 1996404
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ae()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x657dce4b

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v45

    .line 1996405
    const/16 v3, 0x34

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1996406
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1996407
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->f:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1996408
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->g:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996409
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->h:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996410
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->i:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996411
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996412
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->k:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996413
    const/4 v2, 0x7

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->l:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996414
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1996415
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1996416
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1996417
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1996418
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1996419
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1996420
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1996421
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1996422
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996423
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996424
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996425
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996426
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996427
    const/16 v2, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996428
    const/16 v2, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996429
    const/16 v2, 0x17

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996430
    const/16 v2, 0x18

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->C:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996431
    const/16 v2, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996432
    const/16 v2, 0x1a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->E:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996433
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996434
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996435
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996436
    const/16 v2, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996437
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996438
    const/16 v2, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996439
    const/16 v2, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996440
    const/16 v2, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996441
    const/16 v2, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996442
    const/16 v2, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996443
    const/16 v2, 0x25

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->P:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996444
    const/16 v2, 0x26

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Q:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996445
    const/16 v2, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996446
    const/16 v2, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996447
    const/16 v2, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996448
    const/16 v2, 0x2a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->U:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1996449
    const/16 v2, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996450
    const/16 v2, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996451
    const/16 v2, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996452
    const/16 v2, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996453
    const/16 v2, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996454
    const/16 v2, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996455
    const/16 v2, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996456
    const/16 v2, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996457
    const/16 v2, 0x33

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1996458
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1996459
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1996233
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1996234
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->k()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1996235
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->k()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    .line 1996236
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->k()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1996237
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996238
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    .line 1996239
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1996240
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x64b842c9

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1996241
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1996242
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996243
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->n:I

    move-object v1, v0

    .line 1996244
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1996245
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 1996246
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1996247
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996248
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->p:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 1996249
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->x()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1996250
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->x()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x1f84a18

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1996251
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->x()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1996252
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996253
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->q:I

    move-object v1, v0

    .line 1996254
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1996255
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    .line 1996256
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1996257
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996258
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    .line 1996259
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 1996260
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7bbc1ce5

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1996261
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1996262
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996263
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s:I

    move-object v1, v0

    .line 1996264
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1996265
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    .line 1996266
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A()Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1996267
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996268
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->t:Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;

    .line 1996269
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 1996270
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x18d312bf

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1996271
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1996272
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996273
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u:I

    move-object v1, v0

    .line 1996274
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->C()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_8

    .line 1996275
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->C()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4509b6d8

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1996276
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->C()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1996277
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996278
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v:I

    move-object v1, v0

    .line 1996279
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->D()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_9

    .line 1996280
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->D()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x56796cf0

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 1996281
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->D()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1996282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996283
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w:I

    move-object v1, v0

    .line 1996284
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->E()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1996285
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->E()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1996286
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->E()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    if-eq v2, v0, :cond_a

    .line 1996287
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996288
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->x:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1996289
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_b

    .line 1996290
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0xf69c7a9

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 1996291
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1996292
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996293
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y:I

    move-object v1, v0

    .line 1996294
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->G()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1996295
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->G()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1996296
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->G()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v2

    if-eq v2, v0, :cond_c

    .line 1996297
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996298
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->z:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1996299
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->H()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1996300
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->H()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1996301
    if-eqz v2, :cond_d

    .line 1996302
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996303
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->A:Ljava/util/List;

    move-object v1, v0

    .line 1996304
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1996305
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    .line 1996306
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_e

    .line 1996307
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996308
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->H:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    .line 1996309
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_f

    .line 1996310
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x51ecfa16

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 1996311
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->O()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1996312
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996313
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->I:I

    move-object v1, v0

    .line 1996314
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->P()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_10

    .line 1996315
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->P()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4a0c7f15

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    .line 1996316
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->P()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_10

    .line 1996317
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996318
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->J:I

    move-object v1, v0

    .line 1996319
    :cond_10
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_11

    .line 1996320
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x44e15961

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    .line 1996321
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1996322
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996323
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K:I

    move-object v1, v0

    .line 1996324
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1996325
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    .line 1996326
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v2

    if-eq v2, v0, :cond_12

    .line 1996327
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996328
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->L:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    .line 1996329
    :cond_12
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->S()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1996330
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->S()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    .line 1996331
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->S()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 1996332
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996333
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->M:Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    .line 1996334
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->T()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_14

    .line 1996335
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->T()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x22a1d027    # -1.00064086E18f

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    .line 1996336
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->T()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_14

    .line 1996337
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996338
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N:I

    move-object v1, v0

    .line 1996339
    :cond_14
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Y()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_15

    .line 1996340
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Y()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6da0c94

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_b

    .line 1996341
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Y()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_15

    .line 1996342
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996343
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->S:I

    move-object v1, v0

    .line 1996344
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ae()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_16

    .line 1996345
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ae()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x657dce4b

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_c
    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_c

    .line 1996346
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ae()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_16

    .line 1996347
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1996348
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad:I

    move-object v1, v0

    .line 1996349
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1996350
    if-nez v1, :cond_17

    :goto_0
    return-object p0

    .line 1996351
    :catchall_0
    move-exception v0

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    throw v0

    .line 1996352
    :catchall_1
    move-exception v0

    :try_start_e
    monitor-exit v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    throw v0

    .line 1996353
    :catchall_2
    move-exception v0

    :try_start_f
    monitor-exit v4
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    throw v0

    .line 1996354
    :catchall_3
    move-exception v0

    :try_start_10
    monitor-exit v4
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    throw v0

    .line 1996355
    :catchall_4
    move-exception v0

    :try_start_11
    monitor-exit v4
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_4

    throw v0

    .line 1996356
    :catchall_5
    move-exception v0

    :try_start_12
    monitor-exit v4
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    throw v0

    .line 1996357
    :catchall_6
    move-exception v0

    :try_start_13
    monitor-exit v4
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    throw v0

    .line 1996358
    :catchall_7
    move-exception v0

    :try_start_14
    monitor-exit v4
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_7

    throw v0

    .line 1996359
    :catchall_8
    move-exception v0

    :try_start_15
    monitor-exit v4
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    throw v0

    .line 1996360
    :catchall_9
    move-exception v0

    :try_start_16
    monitor-exit v4
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_9

    throw v0

    .line 1996361
    :catchall_a
    move-exception v0

    :try_start_17
    monitor-exit v4
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_a

    throw v0

    .line 1996362
    :catchall_b
    move-exception v0

    :try_start_18
    monitor-exit v4
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_b

    throw v0

    .line 1996363
    :catchall_c
    move-exception v0

    :try_start_19
    monitor-exit v4
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_c

    throw v0

    :cond_17
    move-object p0, v1

    .line 1996364
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1996232
    new-instance v0, LX/DQs;

    invoke-direct {v0, p1}, LX/DQs;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996231
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1996203
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1996204
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->f:J

    .line 1996205
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->g:Z

    .line 1996206
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->h:Z

    .line 1996207
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->i:Z

    .line 1996208
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->j:Z

    .line 1996209
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->k:Z

    .line 1996210
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->l:Z

    .line 1996211
    const/16 v0, 0x9

    const v1, -0x64b842c9

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->n:I

    .line 1996212
    const/16 v0, 0xc

    const v1, -0x1f84a18

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->q:I

    .line 1996213
    const/16 v0, 0xe

    const v1, 0x7bbc1ce5

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s:I

    .line 1996214
    const/16 v0, 0x10

    const v1, -0x18d312bf

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u:I

    .line 1996215
    const/16 v0, 0x11

    const v1, 0x4509b6d8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v:I

    .line 1996216
    const/16 v0, 0x12

    const v1, 0x56796cf0

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w:I

    .line 1996217
    const/16 v0, 0x14

    const v1, -0xf69c7a9

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->y:I

    .line 1996218
    const/16 v0, 0x17

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->B:Z

    .line 1996219
    const/16 v0, 0x18

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->C:Z

    .line 1996220
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->E:Z

    .line 1996221
    const/16 v0, 0x1e

    const v1, 0x51ecfa16

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->I:I

    .line 1996222
    const/16 v0, 0x1f

    const v1, -0x4a0c7f15

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->J:I

    .line 1996223
    const/16 v0, 0x20

    const v1, 0x44e15961

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->K:I

    .line 1996224
    const/16 v0, 0x23

    const v1, -0x22a1d027    # -1.00064086E18f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->N:I

    .line 1996225
    const/16 v0, 0x25

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->P:Z

    .line 1996226
    const/16 v0, 0x26

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Q:Z

    .line 1996227
    const/16 v0, 0x28

    const v1, 0x6da0c94

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->S:I

    .line 1996228
    const/16 v0, 0x2a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->U:Z

    .line 1996229
    const/16 v0, 0x33

    const v1, 0x657dce4b

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad:I

    .line 1996230
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1996153
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1996154
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996155
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996156
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1996157
    :goto_0
    return-void

    .line 1996158
    :cond_0
    const-string v0, "description"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1996159
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996160
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996161
    const/16 v0, 0xa

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1996162
    :cond_1
    const-string v0, "has_viewer_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1996163
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->I()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996164
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996165
    const/16 v0, 0x17

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1996166
    :cond_2
    const-string v0, "join_approval_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1996167
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->M()Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996168
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996169
    const/16 v0, 0x1b

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1996170
    :cond_3
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1996171
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996172
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996173
    const/16 v0, 0x1c

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1996174
    :cond_4
    const-string v0, "post_permission_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1996175
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->U()Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996176
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996177
    const/16 v0, 0x24

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1996178
    :cond_5
    const-string v0, "requires_post_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1996179
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->V()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996180
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996181
    const/16 v0, 0x25

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1996182
    :cond_6
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1996183
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->X()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996184
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996185
    const/16 v0, 0x27

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1996186
    :cond_7
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1996187
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996188
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996189
    const/16 v0, 0x2c

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1996190
    :cond_8
    const-string v0, "viewer_push_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1996191
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996192
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996193
    const/16 v0, 0x2f

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1996194
    :cond_9
    const-string v0, "viewer_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1996195
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996197
    const/16 v0, 0x31

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1996198
    :cond_a
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1996199
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1996200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1996201
    const/16 v0, 0x32

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1996202
    :cond_b
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1996128
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1996129
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(J)V

    .line 1996130
    :cond_0
    :goto_0
    return-void

    .line 1996131
    :cond_1
    const-string v0, "description"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1996132
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1996133
    :cond_2
    const-string v0, "has_viewer_favorited"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1996134
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(Z)V

    goto :goto_0

    .line 1996135
    :cond_3
    const-string v0, "join_approval_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1996136
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V

    goto :goto_0

    .line 1996137
    :cond_4
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1996138
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1996139
    :cond_5
    const-string v0, "post_permission_setting"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1996140
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V

    goto :goto_0

    .line 1996141
    :cond_6
    const-string v0, "requires_post_approval"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1996142
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->b(Z)V

    goto :goto_0

    .line 1996143
    :cond_7
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1996144
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    goto :goto_0

    .line 1996145
    :cond_8
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1996146
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0

    .line 1996147
    :cond_9
    const-string v0, "viewer_push_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1996148
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V

    goto/16 :goto_0

    .line 1996149
    :cond_a
    const-string v0, "viewer_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1996150
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V

    goto/16 :goto_0

    .line 1996151
    :cond_b
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1996152
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto/16 :goto_0
.end method

.method public final aa()Z
    .locals 2

    .prologue
    .line 1996126
    const/4 v0, 0x5

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996127
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->U:Z

    return v0
.end method

.method public final ab()Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996124
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->X:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->X:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    .line 1996125
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->X:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    return-object v0
.end method

.method public final ac()Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996122
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Y:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    const/16 v1, 0x2e

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Y:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    .line 1996123
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Y:Lcom/facebook/graphql/enums/GraphQLGroupPostStatus;

    return-object v0
.end method

.method public final ad()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996120
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ac:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/16 v1, 0x32

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ac:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1996121
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ac:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method

.method public final ae()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVisibilitySentence"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996118
    const/4 v0, 0x6

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996119
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ad:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1996115
    new-instance v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-direct {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;-><init>()V

    .line 1996116
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1996117
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996113
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->G:Ljava/lang/String;

    const/16 v1, 0x1c

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->G:Ljava/lang/String;

    .line 1996114
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->G:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1996111
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996112
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->i:Z

    return v0
.end method

.method public final synthetic d()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996110
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->R()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossiblePushSubscriptionLevelsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1996109
    const v0, -0x419bad9a

    return v0
.end method

.method public final synthetic e()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996108
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->S()Lcom/facebook/groups/widget/groupsettingsrow/protocol/GroupSettingsRowDataModels$GroupSubscriptionDataModel$PossibleSubscriptionLevelsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1996484
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996501
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ab:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    const/16 v1, 0x31

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ab:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 1996502
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->ab:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    return-object v0
.end method

.method public final k()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996499
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    .line 1996500
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    return-object v0
.end method

.method public final kx_()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996497
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Z:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const/16 v1, 0x2f

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Z:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 1996498
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->Z:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    return-object v0
.end method

.method public final ky_()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996466
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->aa:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    const/16 v1, 0x30

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->aa:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 1996467
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->aa:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    return-object v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 1996495
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996496
    iget-wide v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->f:J

    return-wide v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 1996493
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996494
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->g:Z

    return v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1996491
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996492
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->h:Z

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1996489
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996490
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->j:Z

    return v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 1996487
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996488
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->k:Z

    return v0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 1996485
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996486
    iget-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->l:Z

    return v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996464
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1996465
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final s()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1996482
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996483
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996480
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->o:Ljava/lang/String;

    const/16 v1, 0xa

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->o:Ljava/lang/String;

    .line 1996481
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996478
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->V:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    const/16 v1, 0x2b

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->V:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1996479
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->V:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    return-object v0
.end method

.method public final v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996476
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->p:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->p:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    .line 1996477
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->p:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    return-object v0
.end method

.method public final w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996474
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->W:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/16 v1, 0x2c

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->W:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1996475
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->W:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final x()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupDocsAndFiles"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996472
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996473
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->q:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final y()Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupEvents"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996470
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    const/16 v1, 0xd

    const-class v2, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    .line 1996471
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r:Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel;

    return-object v0
.end method

.method public final z()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupForsaleAvailableStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996468
    const/4 v0, 0x1

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996469
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
