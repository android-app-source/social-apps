.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1996695
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;

    new-instance v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1996696
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1996697
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1996698
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1996699
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1996700
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1996701
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1996702
    :goto_0
    move v1, v2

    .line 1996703
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1996704
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1996705
    new-instance v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;

    invoke-direct {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;-><init>()V

    .line 1996706
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1996707
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1996708
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1996709
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1996710
    :cond_0
    return-object v1

    .line 1996711
    :cond_1
    const-string p0, "user_might_be_selling"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1996712
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v4, v1

    move v1, v3

    .line 1996713
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_7

    .line 1996714
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1996715
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1996716
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v9, :cond_2

    .line 1996717
    const-string p0, "admin_aware_group"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1996718
    invoke-static {p1, v0}, LX/DRE;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1996719
    :cond_3
    const-string p0, "group_purposes"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1996720
    invoke-static {p1, v0}, LX/9Tf;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1996721
    :cond_4
    const-string p0, "group_sell_config"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1996722
    invoke-static {p1, v0}, LX/9LH;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1996723
    :cond_5
    const-string p0, "suggested_purpose"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1996724
    invoke-static {p1, v0}, LX/9Tg;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1996725
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1996726
    :cond_7
    const/4 v9, 0x5

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1996727
    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1996728
    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1996729
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1996730
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1996731
    if-eqz v1, :cond_8

    .line 1996732
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4}, LX/186;->a(IZ)V

    .line 1996733
    :cond_8
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto :goto_1
.end method
