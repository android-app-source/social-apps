.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3578b80
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1995716
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1995715
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1995713
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1995714
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1995707
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->g:Ljava/lang/String;

    .line 1995708
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1995709
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1995710
    if-eqz v0, :cond_0

    .line 1995711
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1995712
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1995697
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1995698
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1995699
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1995700
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1995701
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1995702
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1995703
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1995704
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1995705
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1995706
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1995694
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1995695
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1995696
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1995693
    new-instance v0, LX/DQt;

    invoke-direct {v0, p1}, LX/DQt;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1995672
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1995687
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1995688
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1995689
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1995690
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1995691
    :goto_0
    return-void

    .line 1995692
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1995684
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1995685
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->a(Ljava/lang/String;)V

    .line 1995686
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1995681
    new-instance v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;-><init>()V

    .line 1995682
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1995683
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1995680
    const v0, 0x4fe5c3c5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1995679
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1995677
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    .line 1995678
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1995675
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->f:Ljava/lang/String;

    .line 1995676
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1995673
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->g:Ljava/lang/String;

    .line 1995674
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$ParentGroupModel;->g:Ljava/lang/String;

    return-object v0
.end method
