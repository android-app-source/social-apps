.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2641c656
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1994963
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1994962
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1994960
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1994961
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1994957
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1994958
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1994959
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1994951
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1994952
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1994953
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1994954
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1994955
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1994956
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1994943
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1994944
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1994945
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1994946
    invoke-virtual {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1994947
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;

    .line 1994948
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1994949
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1994950
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1994936
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    .line 1994937
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;->e:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1994940
    new-instance v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoEventsModels$FetchGroupInfoEventsModel$GroupEventsModel$EdgesModel;-><init>()V

    .line 1994941
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1994942
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1994939
    const v0, -0x2b19e1db

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1994938
    const v0, 0x462f4635

    return v0
.end method
