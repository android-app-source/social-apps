.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1995400
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1995401
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1995398
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1995399
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1995294
    if-nez p1, :cond_0

    move v0, v1

    .line 1995295
    :goto_0
    return v0

    .line 1995296
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1995297
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1995298
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1995299
    const v2, -0x6e0a5310

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1995300
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995301
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995302
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1995303
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1995304
    const v2, -0x2eccb4ab

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1995305
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995306
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995307
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1995308
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1995309
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1995310
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995311
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995312
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1995313
    :sswitch_3
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1995314
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995315
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1995316
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1995317
    :sswitch_4
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1995318
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995319
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1995320
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1995321
    :sswitch_5
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1995322
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995323
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1995324
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1995325
    :sswitch_6
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1995326
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995327
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1995328
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995329
    :sswitch_7
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1995330
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995331
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1995332
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995333
    :sswitch_8
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1995334
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995335
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1995336
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995337
    :sswitch_9
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    .line 1995338
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1995339
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995340
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995341
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995342
    :sswitch_a
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1995343
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1995344
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995345
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995346
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995347
    :sswitch_b
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1995348
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1995349
    const v3, -0x2db7ab8c

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 1995350
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1995351
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1995352
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1995353
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995354
    :sswitch_c
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1995355
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1995356
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v2

    .line 1995357
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1995358
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1995359
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995360
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1995361
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995362
    :sswitch_d
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1995363
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1995364
    const v3, 0x7be47f12

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v2

    .line 1995365
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1995366
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1995367
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1995368
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995369
    :sswitch_e
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1995370
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1995371
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v2

    .line 1995372
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1995373
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1995374
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995375
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1995376
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995377
    :sswitch_f
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1995378
    const v2, 0x5ff7ba5f

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->b(LX/15i;IILX/186;)I

    move-result v0

    .line 1995379
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1995380
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995381
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995382
    :sswitch_10
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1995383
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1995384
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    .line 1995385
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1995386
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1995387
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995388
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1995389
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1995390
    :sswitch_11
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$VisibilitySentenceModel$RangesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1995391
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1995392
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1995393
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1995394
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1995395
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1995396
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1995397
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6e0a5310 -> :sswitch_1
        -0x64b842c9 -> :sswitch_0
        -0x4a0c7f15 -> :sswitch_b
        -0x2eccb4ab -> :sswitch_2
        -0x2db7ab8c -> :sswitch_c
        -0x22a1d027 -> :sswitch_f
        -0x18d312bf -> :sswitch_5
        -0xf69c7a9 -> :sswitch_8
        -0x1f84a18 -> :sswitch_3
        0x44e15961 -> :sswitch_d
        0x4509b6d8 -> :sswitch_6
        0x461983bb -> :sswitch_a
        0x51ecfa16 -> :sswitch_9
        0x56796cf0 -> :sswitch_7
        0x5ff7ba5f -> :sswitch_10
        0x657dce4b -> :sswitch_11
        0x7bbc1ce5 -> :sswitch_4
        0x7be47f12 -> :sswitch_e
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1995293
    new-instance v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1995289
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1995290
    if-eqz v0, :cond_0

    .line 1995291
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1995292
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1995284
    if-eqz p0, :cond_0

    .line 1995285
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1995286
    if-eq v0, p0, :cond_0

    .line 1995287
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1995288
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1995267
    sparse-switch p2, :sswitch_data_0

    .line 1995268
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1995269
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1995270
    const v1, -0x6e0a5310

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1995271
    :goto_0
    :sswitch_1
    return-void

    .line 1995272
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1995273
    const v1, -0x2eccb4ab

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1995274
    :sswitch_3
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$PhotoForLauncherShortcutModel$PhotoModel;

    .line 1995275
    invoke-static {v0, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1995276
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1995277
    const v1, -0x2db7ab8c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1995278
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1995279
    const v1, 0x7be47f12

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1995280
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1995281
    const v1, 0x5ff7ba5f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1995282
    :sswitch_7
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$VisibilitySentenceModel$RangesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1995283
    invoke-static {v0, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6e0a5310 -> :sswitch_2
        -0x64b842c9 -> :sswitch_0
        -0x4a0c7f15 -> :sswitch_4
        -0x2eccb4ab -> :sswitch_1
        -0x2db7ab8c -> :sswitch_1
        -0x22a1d027 -> :sswitch_6
        -0x18d312bf -> :sswitch_1
        -0xf69c7a9 -> :sswitch_1
        -0x1f84a18 -> :sswitch_1
        0x44e15961 -> :sswitch_5
        0x4509b6d8 -> :sswitch_1
        0x461983bb -> :sswitch_1
        0x51ecfa16 -> :sswitch_3
        0x56796cf0 -> :sswitch_1
        0x5ff7ba5f -> :sswitch_1
        0x657dce4b -> :sswitch_7
        0x7bbc1ce5 -> :sswitch_1
        0x7be47f12 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1995257
    if-nez p1, :cond_0

    move v0, v1

    .line 1995258
    :goto_0
    return v0

    .line 1995259
    :cond_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v2

    .line 1995260
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1995261
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1995262
    invoke-virtual {p0, p1, v1}, LX/15i;->q(II)I

    move-result v3

    .line 1995263
    invoke-static {p0, v3, p2, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    aput v3, v0, v1

    .line 1995264
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1995265
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1995266
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 1995250
    if-eqz p1, :cond_0

    .line 1995251
    invoke-virtual {p0, p1}, LX/15i;->d(I)I

    move-result v1

    .line 1995252
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1995253
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v2

    .line 1995254
    invoke-static {p0, v2, p2, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1995255
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1995256
    :cond_0
    return-void
.end method

.method private static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1995210
    if-eqz p1, :cond_0

    .line 1995211
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v1

    .line 1995212
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    .line 1995213
    if-eq v0, v1, :cond_0

    .line 1995214
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1995215
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1995249
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1995247
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1995248
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1995242
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1995243
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1995244
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a:LX/15i;

    .line 1995245
    iput p2, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->b:I

    .line 1995246
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1995241
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1995240
    new-instance v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1995237
    iget v0, p0, LX/1vt;->c:I

    .line 1995238
    move v0, v0

    .line 1995239
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1995234
    iget v0, p0, LX/1vt;->c:I

    .line 1995235
    move v0, v0

    .line 1995236
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1995231
    iget v0, p0, LX/1vt;->b:I

    .line 1995232
    move v0, v0

    .line 1995233
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1995228
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1995229
    move-object v0, v0

    .line 1995230
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1995219
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1995220
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1995221
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1995222
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1995223
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1995224
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1995225
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1995226
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1995227
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1995216
    iget v0, p0, LX/1vt;->c:I

    .line 1995217
    move v0, v0

    .line 1995218
    return v0
.end method
