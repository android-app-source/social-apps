.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1995772
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    new-instance v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1995773
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1995774
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/0nX;LX/0my;)V
    .locals 11

    .prologue
    .line 1995775
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1995776
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 v10, 0x2b

    const/16 v9, 0x27

    const/16 v8, 0x24

    const/16 v7, 0x1b

    const/16 v6, 0x8

    .line 1995777
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995778
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995779
    if-eqz v2, :cond_0

    .line 1995780
    const-string v3, "admin_aware_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995781
    invoke-static {v1, v2, p1}, LX/DRE;->a(LX/15i;ILX/0nX;)V

    .line 1995782
    :cond_0
    const/4 v2, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1995783
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 1995784
    const-string v4, "archived_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995785
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 1995786
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995787
    if-eqz v2, :cond_2

    .line 1995788
    const-string v3, "can_viewer_change_cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995789
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995790
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995791
    if-eqz v2, :cond_3

    .line 1995792
    const-string v3, "can_viewer_change_name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995793
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995794
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995795
    if-eqz v2, :cond_4

    .line 1995796
    const-string v3, "can_viewer_pin_post"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995797
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995798
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995799
    if-eqz v2, :cond_5

    .line 1995800
    const-string v3, "can_viewer_report"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995801
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995802
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995803
    if-eqz v2, :cond_6

    .line 1995804
    const-string v3, "can_viewer_see_admin_activity_log"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995805
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995806
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995807
    if-eqz v2, :cond_7

    .line 1995808
    const-string v3, "can_viewer_see_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995809
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995810
    :cond_7
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 1995811
    if-eqz v2, :cond_8

    .line 1995812
    const-string v2, "community_category"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995813
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995814
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995815
    if-eqz v2, :cond_c

    .line 1995816
    const-string v3, "cover_photo"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995817
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995818
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1995819
    if-eqz v3, :cond_b

    .line 1995820
    const-string v4, "photo"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995821
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995822
    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1995823
    if-eqz v4, :cond_a

    .line 1995824
    const-string v2, "image"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995825
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995826
    const/4 v2, 0x0

    invoke-virtual {v1, v4, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1995827
    if-eqz v2, :cond_9

    .line 1995828
    const-string v3, "uri"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995829
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995830
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995831
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995832
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995833
    :cond_c
    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1995834
    if-eqz v2, :cond_d

    .line 1995835
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995836
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995837
    :cond_d
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995838
    if-eqz v2, :cond_e

    .line 1995839
    const-string v3, "group_configs"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995840
    invoke-static {v1, v2, p1, p2}, LX/5A9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995841
    :cond_e
    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995842
    if-eqz v2, :cond_10

    .line 1995843
    const-string v3, "group_docs_and_files"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995844
    const/4 v3, 0x0

    .line 1995845
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995846
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1995847
    if-eqz v3, :cond_f

    .line 1995848
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995849
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1995850
    :cond_f
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995851
    :cond_10
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995852
    if-eqz v2, :cond_11

    .line 1995853
    const-string v3, "group_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995854
    invoke-static {v1, v2, p1, p2}, LX/DQh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995855
    :cond_11
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995856
    if-eqz v2, :cond_13

    .line 1995857
    const-string v3, "group_forsale_available_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995858
    const/4 v3, 0x0

    .line 1995859
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995860
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1995861
    if-eqz v3, :cond_12

    .line 1995862
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995863
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1995864
    :cond_12
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995865
    :cond_13
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995866
    if-eqz v2, :cond_14

    .line 1995867
    const-string v3, "group_mediaset"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995868
    invoke-static {v1, v2, p1, p2}, LX/DRO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995869
    :cond_14
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995870
    if-eqz v2, :cond_16

    .line 1995871
    const-string v3, "group_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995872
    const/4 v3, 0x0

    .line 1995873
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995874
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1995875
    if-eqz v3, :cond_15

    .line 1995876
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995877
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1995878
    :cond_15
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995879
    :cond_16
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995880
    if-eqz v2, :cond_18

    .line 1995881
    const-string v3, "group_pending_members"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995882
    const/4 v3, 0x0

    .line 1995883
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995884
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1995885
    if-eqz v3, :cond_17

    .line 1995886
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995887
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1995888
    :cond_17
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995889
    :cond_18
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995890
    if-eqz v2, :cond_1a

    .line 1995891
    const-string v3, "group_pending_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995892
    const/4 v3, 0x0

    .line 1995893
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995894
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1995895
    if-eqz v3, :cond_19

    .line 1995896
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995897
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1995898
    :cond_19
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995899
    :cond_1a
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995900
    if-eqz v2, :cond_1b

    .line 1995901
    const-string v3, "group_purposes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995902
    invoke-static {v1, v2, p1, p2}, LX/9Tf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995903
    :cond_1b
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995904
    if-eqz v2, :cond_1d

    .line 1995905
    const-string v3, "group_reported_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995906
    const/4 v3, 0x0

    .line 1995907
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995908
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1995909
    if-eqz v3, :cond_1c

    .line 1995910
    const-string v4, "count"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995911
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1995912
    :cond_1c
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995913
    :cond_1d
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995914
    if-eqz v2, :cond_1e

    .line 1995915
    const-string v3, "group_sell_config"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995916
    invoke-static {v1, v2, p1, p2}, LX/9LH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995917
    :cond_1e
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995918
    if-eqz v2, :cond_20

    .line 1995919
    const-string v3, "group_topic_tags"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995920
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1995921
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_1f

    .line 1995922
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    invoke-static {v1, v4, p1}, LX/DR4;->a(LX/15i;ILX/0nX;)V

    .line 1995923
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1995924
    :cond_1f
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1995925
    :cond_20
    const/16 v2, 0x17

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995926
    if-eqz v2, :cond_21

    .line 1995927
    const-string v3, "has_viewer_favorited"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995928
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995929
    :cond_21
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995930
    if-eqz v2, :cond_22

    .line 1995931
    const-string v3, "has_viewer_hidden"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995932
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995933
    :cond_22
    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1995934
    if-eqz v2, :cond_23

    .line 1995935
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995936
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995937
    :cond_23
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995938
    if-eqz v2, :cond_24

    .line 1995939
    const-string v3, "is_multi_company_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995940
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995941
    :cond_24
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    .line 1995942
    if-eqz v2, :cond_25

    .line 1995943
    const-string v2, "join_approval_setting"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995944
    invoke-virtual {v1, v0, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995945
    :cond_25
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1995946
    if-eqz v2, :cond_26

    .line 1995947
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995948
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995949
    :cond_26
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995950
    if-eqz v2, :cond_27

    .line 1995951
    const-string v3, "parent_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995952
    invoke-static {v1, v2, p1}, LX/DR5;->a(LX/15i;ILX/0nX;)V

    .line 1995953
    :cond_27
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995954
    if-eqz v2, :cond_29

    .line 1995955
    const-string v3, "photoForLauncherShortcut"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995956
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1995957
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1995958
    if-eqz v3, :cond_28

    .line 1995959
    const-string v4, "photo"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995960
    invoke-static {v1, v3, p1, p2}, LX/DR6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995961
    :cond_28
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1995962
    :cond_29
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995963
    if-eqz v2, :cond_2a

    .line 1995964
    const-string v3, "possible_join_approval_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995965
    invoke-static {v1, v2, p1, p2}, LX/DR8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995966
    :cond_2a
    const/16 v2, 0x20

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995967
    if-eqz v2, :cond_2b

    .line 1995968
    const-string v3, "possible_post_permission_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995969
    invoke-static {v1, v2, p1, p2}, LX/DR9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995970
    :cond_2b
    const/16 v2, 0x21

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995971
    if-eqz v2, :cond_2c

    .line 1995972
    const-string v3, "possible_push_subscription_levels"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995973
    invoke-static {v1, v2, p1, p2}, LX/Dak;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995974
    :cond_2c
    const/16 v2, 0x22

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995975
    if-eqz v2, :cond_2d

    .line 1995976
    const-string v3, "possible_subscription_levels"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995977
    invoke-static {v1, v2, p1, p2}, LX/Dam;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995978
    :cond_2d
    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995979
    if-eqz v2, :cond_2e

    .line 1995980
    const-string v3, "possible_visibility_settings"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995981
    invoke-static {v1, v2, p1, p2}, LX/DRA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1995982
    :cond_2e
    invoke-virtual {v1, v0, v8}, LX/15i;->g(II)I

    move-result v2

    .line 1995983
    if-eqz v2, :cond_2f

    .line 1995984
    const-string v2, "post_permission_setting"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995985
    invoke-virtual {v1, v0, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995986
    :cond_2f
    const/16 v2, 0x25

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995987
    if-eqz v2, :cond_30

    .line 1995988
    const-string v3, "requires_post_approval"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995989
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995990
    :cond_30
    const/16 v2, 0x26

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1995991
    if-eqz v2, :cond_31

    .line 1995992
    const-string v3, "should_show_notif_settings_transition_nux"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995993
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1995994
    :cond_31
    invoke-virtual {v1, v0, v9}, LX/15i;->g(II)I

    move-result v2

    .line 1995995
    if-eqz v2, :cond_32

    .line 1995996
    const-string v2, "subscribe_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1995997
    invoke-virtual {v1, v0, v9}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1995998
    :cond_32
    const/16 v2, 0x28

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1995999
    if-eqz v2, :cond_33

    .line 1996000
    const-string v3, "suggested_purpose"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996001
    invoke-static {v1, v2, p1}, LX/9Tg;->a(LX/15i;ILX/0nX;)V

    .line 1996002
    :cond_33
    const/16 v2, 0x29

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1996003
    if-eqz v2, :cond_34

    .line 1996004
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996005
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996006
    :cond_34
    const/16 v2, 0x2a

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1996007
    if-eqz v2, :cond_35

    .line 1996008
    const-string v3, "user_might_be_selling"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996009
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1996010
    :cond_35
    invoke-virtual {v1, v0, v10}, LX/15i;->g(II)I

    move-result v2

    .line 1996011
    if-eqz v2, :cond_36

    .line 1996012
    const-string v2, "viewer_admin_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996013
    invoke-virtual {v1, v0, v10}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996014
    :cond_36
    const/16 v2, 0x2c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996015
    if-eqz v2, :cond_37

    .line 1996016
    const-string v2, "viewer_join_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996017
    const/16 v2, 0x2c

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996018
    :cond_37
    const/16 v2, 0x2d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996019
    if-eqz v2, :cond_38

    .line 1996020
    const-string v2, "viewer_leave_scenario"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996021
    const/16 v2, 0x2d

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996022
    :cond_38
    const/16 v2, 0x2e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996023
    if-eqz v2, :cond_39

    .line 1996024
    const-string v2, "viewer_post_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996025
    const/16 v2, 0x2e

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996026
    :cond_39
    const/16 v2, 0x2f

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996027
    if-eqz v2, :cond_3a

    .line 1996028
    const-string v2, "viewer_push_subscription_level"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996029
    const/16 v2, 0x2f

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996030
    :cond_3a
    const/16 v2, 0x30

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996031
    if-eqz v2, :cond_3b

    .line 1996032
    const-string v2, "viewer_request_to_join_subscription_level"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996033
    const/16 v2, 0x30

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996034
    :cond_3b
    const/16 v2, 0x31

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996035
    if-eqz v2, :cond_3c

    .line 1996036
    const-string v2, "viewer_subscription_level"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996037
    const/16 v2, 0x31

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996038
    :cond_3c
    const/16 v2, 0x32

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996039
    if-eqz v2, :cond_3d

    .line 1996040
    const-string v2, "visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996041
    const/16 v2, 0x32

    invoke-virtual {v1, v0, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996042
    :cond_3d
    const/16 v2, 0x33

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996043
    if-eqz v2, :cond_41

    .line 1996044
    const-string v3, "visibility_sentence"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996045
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1996046
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1996047
    if-eqz v3, :cond_3f

    .line 1996048
    const-string v4, "ranges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996049
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1996050
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_3e

    .line 1996051
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    invoke-static {v1, v5, p1}, LX/DRB;->a(LX/15i;ILX/0nX;)V

    .line 1996052
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1996053
    :cond_3e
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1996054
    :cond_3f
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1996055
    if-eqz v3, :cond_40

    .line 1996056
    const-string v4, "text"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996057
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1996058
    :cond_40
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1996059
    :cond_41
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1996060
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1996061
    check-cast p1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel$Serializer;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
