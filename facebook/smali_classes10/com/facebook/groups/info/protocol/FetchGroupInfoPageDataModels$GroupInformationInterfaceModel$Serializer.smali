.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1996763
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;

    new-instance v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1996764
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1996762
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1996736
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1996737
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1996738
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1996739
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996740
    if-eqz v2, :cond_0

    .line 1996741
    const-string p0, "admin_aware_group"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996742
    invoke-static {v1, v2, p1}, LX/DRE;->a(LX/15i;ILX/0nX;)V

    .line 1996743
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996744
    if-eqz v2, :cond_1

    .line 1996745
    const-string p0, "group_purposes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996746
    invoke-static {v1, v2, p1, p2}, LX/9Tf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1996747
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996748
    if-eqz v2, :cond_2

    .line 1996749
    const-string p0, "group_sell_config"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996750
    invoke-static {v1, v2, p1, p2}, LX/9LH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1996751
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1996752
    if-eqz v2, :cond_3

    .line 1996753
    const-string p0, "suggested_purpose"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996754
    invoke-static {v1, v2, p1}, LX/9Tg;->a(LX/15i;ILX/0nX;)V

    .line 1996755
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1996756
    if-eqz v2, :cond_4

    .line 1996757
    const-string p0, "user_might_be_selling"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1996758
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1996759
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1996760
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1996761
    check-cast p1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Serializer;->a(Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;LX/0nX;LX/0my;)V

    return-void
.end method
