.class public final Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;
.implements LX/DQo;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1f8dcb75
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1996802
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1996801
    const-class v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1996799
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1996800
    return-void
.end method

.method private a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996797
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    .line 1996798
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    return-object v0
.end method

.method private j()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996795
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->f:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->f:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1996796
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->f:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    return-object v0
.end method

.method private k()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupSellConfig"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996793
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->g:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    iput-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->g:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1996794
    iget-object v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->g:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSuggestedPurpose"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1996791
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1996792
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1996778
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1996779
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1996780
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->j()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1996781
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->k()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1996782
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x6da0c94

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1996783
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1996784
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1996785
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1996786
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1996787
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1996788
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1996789
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1996790
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1996803
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1996804
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1996805
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    .line 1996806
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->a()Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1996807
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;

    .line 1996808
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->e:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel$AdminAwareGroupModel;

    .line 1996809
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->j()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1996810
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->j()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1996811
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->j()Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1996812
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;

    .line 1996813
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->f:Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$GroupPurposesInformationModel$GroupPurposesModel;

    .line 1996814
    :cond_1
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->k()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1996815
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->k()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1996816
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->k()Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1996817
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;

    .line 1996818
    iput-object v0, v1, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->g:Lcom/facebook/groupcommerce/protocol/GroupSellInformationGraphQLModels$GroupSellInformationModel$GroupSellConfigModel;

    .line 1996819
    :cond_2
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1996820
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6da0c94

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/grouppurposes/protocol/GroupPurposesInformationGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1996821
    invoke-direct {p0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1996822
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;

    .line 1996823
    iput v3, v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->h:I

    move-object v1, v0

    .line 1996824
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1996825
    if-nez v1, :cond_4

    :goto_0
    return-object p0

    .line 1996826
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    move-object p0, v1

    .line 1996827
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1996777
    new-instance v0, LX/DQv;

    invoke-direct {v0, p1}, LX/DQv;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1996773
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1996774
    const/4 v0, 0x3

    const v1, 0x6da0c94

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->h:I

    .line 1996775
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;->i:Z

    .line 1996776
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1996771
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1996772
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1996770
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1996767
    new-instance v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;

    invoke-direct {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$GroupInformationInterfaceModel;-><init>()V

    .line 1996768
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1996769
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1996766
    const v0, -0x31265e3e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1996765
    const v0, 0x41e065f

    return v0
.end method
