.class public Lcom/facebook/groups/info/GroupInfoAdapter;
.super LX/1Cv;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/widget/listview/BetterListView;

.field public final c:LX/DMP;

.field public final d:Landroid/content/res/Resources;

.field public final e:LX/0ad;

.field public final f:LX/23R;

.field public final g:LX/DRS;

.field public final h:LX/Bm1;

.field public final i:LX/0gc;

.field public final j:LX/DPp;

.field public final k:LX/DPo;

.field public final l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

.field private final m:LX/0Uh;

.field public final n:Z

.field public final o:Z

.field private final p:LX/3my;

.field public final q:LX/88k;

.field public r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/5QT;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1994257
    const-class v0, Lcom/facebook/groups/info/GroupInfoAdapter;

    const-string v1, "group_info"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/info/GroupInfoAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/widget/listview/BetterListView;LX/0gc;LX/DMP;LX/DPp;LX/5QT;Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;Landroid/content/res/Resources;LX/23R;LX/Bm1;LX/DPo;LX/DRS;LX/0ad;LX/0Uh;Ljava/lang/Boolean;Ljava/lang/Boolean;LX/3my;LX/88k;)V
    .locals 2
    .param p1    # Lcom/facebook/widget/listview/BetterListView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/DMP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/DPp;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/5QT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/work/groups/multicompany/gk/IsWorkMultiCompanyGroupsEnabledGk;
        .end annotation
    .end param
    .param p15    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/work/groups/multicompany/gk/IsMultiCompanyChatEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1994237
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1994238
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->r:LX/0Px;

    .line 1994239
    iput-object p2, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->i:LX/0gc;

    .line 1994240
    iput-object p4, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->j:LX/DPp;

    .line 1994241
    iput-object p7, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    .line 1994242
    iput-object p1, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->b:Lcom/facebook/widget/listview/BetterListView;

    .line 1994243
    iput-object p3, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->c:LX/DMP;

    .line 1994244
    iput-object p5, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->s:LX/5QT;

    .line 1994245
    iput-object p8, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->f:LX/23R;

    .line 1994246
    iput-object p9, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->h:LX/Bm1;

    .line 1994247
    iput-object p10, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->k:LX/DPo;

    .line 1994248
    iput-object p11, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->g:LX/DRS;

    .line 1994249
    iput-object p6, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->l:Lcom/facebook/groups/feed/protocol/FetchGroupInformationGraphQLModels$FetchGroupInformationModel;

    .line 1994250
    iput-object p12, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->e:LX/0ad;

    .line 1994251
    iput-object p13, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->m:LX/0Uh;

    .line 1994252
    invoke-virtual/range {p14 .. p14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->n:Z

    .line 1994253
    invoke-virtual/range {p15 .. p15}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->o:Z

    .line 1994254
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->p:LX/3my;

    .line 1994255
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->q:LX/88k;

    .line 1994256
    return-void
.end method

.method private static a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;ILX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;
    .locals 7

    .prologue
    .line 1994236
    new-instance v0, LX/DQ1;

    sget-object v2, LX/DQN;->i:LX/DML;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/DQ1;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Ljava/lang/String;ILX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;
    .locals 6

    .prologue
    .line 1994234
    new-instance v0, LX/DQ3;

    sget-object v2, LX/DQN;->j:LX/DML;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/DQ3;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    .line 1994235
    return-object v0
.end method

.method public static a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1994232
    goto :goto_0

    .line 1994233
    :goto_0
    return-void
.end method

.method public static a(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/0Pz;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1994199
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->C()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1994200
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->F()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1994201
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->D()LX/1vs;

    move-result-object v3

    iget-object v6, v3, LX/1vs;->a:LX/15i;

    iget v7, v3, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1994202
    if-eqz v0, :cond_4

    invoke-virtual {v2, v0, v1}, LX/15i;->j(II)I

    move-result v0

    move v3, v0

    .line 1994203
    :goto_0
    if-eqz v5, :cond_5

    invoke-virtual {v4, v5, v1}, LX/15i;->j(II)I

    move-result v0

    move v2, v0

    .line 1994204
    :goto_1
    if-eqz v7, :cond_6

    invoke-virtual {v6, v7, v1}, LX/15i;->j(II)I

    move-result v0

    if-lez v0, :cond_6

    const/4 v0, 0x1

    .line 1994205
    :goto_2
    iget-object v4, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v5, 0x7f08305f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, LX/DQO;->MEMBER_REQUESTS:LX/DQO;

    invoke-static {p0, v4, v3, v5, p1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;ILX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994206
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994207
    iget-object v3, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v4, 0x7f083061

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/DQO;->REPORTED_POSTS:LX/DQO;

    invoke-static {p0, v3, v2, v4, p1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;ILX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994208
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994209
    if-eqz v0, :cond_0

    .line 1994210
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v2, 0x7f083060

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v7, v1}, LX/15i;->j(II)I

    move-result v1

    sget-object v2, LX/DQO;->PENDING_POSTS:LX/DQO;

    invoke-static {p0, v0, v1, v2, p1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;ILX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994211
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994212
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1994213
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v1, 0x7f083050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/DQO;->EDIT_GROUP_SETTINGS:LX/DQO;

    invoke-static {p0, v0, v1, p1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994214
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994215
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1994216
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->s()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1994217
    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v1, 0x7f083065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1994218
    :goto_3
    sget-object v1, LX/DQO;->COVER_PHOTO:LX/DQO;

    invoke-static {p0, v0, v1, p1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994219
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994220
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->p()Z

    move-result v0

    move v0, v0

    .line 1994221
    if-eqz v0, :cond_3

    .line 1994222
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v1, 0x7f083051

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/DQO;->ADMIN_ACTIVITY:LX/DQO;

    invoke-static {p0, v0, v1, p1}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;Ljava/lang/String;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)LX/DMB;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994223
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994224
    :cond_3
    return-void

    .line 1994225
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1994226
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1994227
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_4
    move v3, v1

    .line 1994228
    goto/16 :goto_0

    :cond_5
    move v2, v1

    .line 1994229
    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 1994230
    goto/16 :goto_2

    .line 1994231
    :cond_7
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v1, 0x7f083066

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static a$redex0(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1994198
    new-instance v0, LX/DQ4;

    invoke-direct {v0, p0, p1, p2}, LX/DQ4;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DQO;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;Ljava/util/List;Ljava/util/List;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosInterfaces$FetchGroupInfoPhotos$$GroupMediaset$;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1994183
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ne p4, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1994184
    const-class v0, LX/23Y;

    new-instance v2, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/photos/mediafetcher/query/param/IdQueryParam;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;->a(Ljava/lang/Class;Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;)Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    move-result-object v2

    .line 1994185
    new-instance v6, LX/DPz;

    invoke-direct {v6, p0, p4, p2, p3}, LX/DPz;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;ILjava/util/List;Ljava/util/List;)V

    move v8, v1

    .line 1994186
    :goto_1
    if-ge v8, p4, :cond_2

    .line 1994187
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 1994188
    invoke-interface {p3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1994189
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 1994190
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1994191
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPhotosModels$FetchGroupInfoPhotosModel$GroupMediasetModel$MediaModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1994192
    :goto_2
    invoke-virtual {v7, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1994193
    new-instance v0, LX/DQ0;

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, LX/DQ0;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;LX/9hN;)V

    invoke-virtual {v7, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1994194
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 1994195
    goto :goto_0

    .line 1994196
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->d:Landroid/content/res/Resources;

    const v1, 0x7f080072

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1994197
    :cond_2
    return-void
.end method

.method public static b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1994181
    goto :goto_0

    .line 1994182
    :goto_0
    return-void
.end method

.method public static c()LX/DMB;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/DMB",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1994180
    new-instance v0, LX/DaP;

    sget-object v1, LX/DQN;->l:LX/DML;

    invoke-direct {v0, v1}, LX/DaP;-><init>(LX/DML;)V

    return-object v0
.end method

.method public static c(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1994119
    goto :goto_0

    .line 1994120
    :goto_0
    return-void
.end method

.method public static h(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/0Pz;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1994157
    invoke-static {p1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1994158
    invoke-static {p2}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1994159
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994160
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->p:LX/3my;

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->r()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/3my;->d(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v3

    .line 1994161
    if-nez v3, :cond_0

    .line 1994162
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->X()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    .line 1994163
    sget-object v4, LX/DQ6;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :cond_0
    move v0, v2

    .line 1994164
    :goto_0
    iget-object v4, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->m:LX/0Uh;

    const/16 v5, 0x4f3

    invoke-virtual {v4, v5, v2}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1994165
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v7

    if-eq v6, v7, :cond_1

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->MODERATOR:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v7

    if-ne v6, v7, :cond_2

    .line 1994166
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->l()J

    move-result-wide v10

    .line 1994167
    new-instance v7, LX/DQ5;

    sget-object v9, LX/DQN;->j:LX/DML;

    move-object v8, p0

    move-object v12, p1

    invoke-direct/range {v7 .. v12}, LX/DQ5;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;JLcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {p2, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994168
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->o()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v4

    if-eq v2, v4, :cond_4

    .line 1994169
    if-eqz v0, :cond_3

    .line 1994170
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994171
    :cond_3
    new-instance v0, LX/DQ8;

    sget-object v2, LX/DQN;->h:LX/DML;

    invoke-direct {v0, p0, v2, v3, p1}, LX/DQ8;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;ZLcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    .line 1994172
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->w()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v1, v2, :cond_6

    .line 1994173
    if-eqz v0, :cond_5

    .line 1994174
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->b(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994175
    :cond_5
    new-instance v0, LX/DQA;

    sget-object v1, LX/DQN;->h:LX/DML;

    invoke-direct {v0, p0, v1, v3, p1}, LX/DQA;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;ZLcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994176
    :cond_6
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994177
    return-void

    .line 1994178
    :pswitch_0
    new-instance v4, LX/DQ7;

    sget-object v5, LX/DQN;->g:LX/DML;

    invoke-direct {v4, p0, v5, v0, p1}, LX/DQ7;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    invoke-virtual {p2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    .line 1994179
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static i(Lcom/facebook/groups/info/GroupInfoAdapter;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;LX/0Pz;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;",
            "LX/0Pz",
            "<",
            "LX/DMB;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1994137
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->g:LX/DRS;

    .line 1994138
    new-instance v1, LX/DRR;

    sget-object v2, LX/DQN;->a:LX/DML;

    invoke-direct {v1, v0, v2, p1}, LX/DRR;-><init>(LX/DRS;LX/DML;Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;)V

    move-object v0, v1

    .line 1994139
    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994140
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994141
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->t()Ljava/lang/String;

    move-result-object v1

    .line 1994142
    invoke-virtual {p1}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->H()LX/0Px;

    move-result-object v2

    .line 1994143
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1994144
    new-instance v0, LX/DQB;

    .line 1994145
    sget-object v4, LX/DQN;->b:LX/DML;

    move-object v3, v4

    .line 1994146
    invoke-direct {v0, p0, v3, v1}, LX/DQB;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994147
    :cond_0
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 1994148
    :goto_0
    if-eqz v0, :cond_1

    .line 1994149
    new-instance v3, LX/DQC;

    .line 1994150
    sget-object p1, LX/DQN;->c:LX/DML;

    move-object v4, p1

    .line 1994151
    invoke-direct {v3, p0, v4, v2}, LX/DQC;-><init>(Lcom/facebook/groups/info/GroupInfoAdapter;LX/DML;LX/0Px;)V

    invoke-virtual {p2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994152
    :cond_1
    if-nez v0, :cond_2

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1994153
    :cond_2
    invoke-static {}, Lcom/facebook/groups/info/GroupInfoAdapter;->c()LX/DMB;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1994154
    invoke-static {p0, p2}, Lcom/facebook/groups/info/GroupInfoAdapter;->a(Lcom/facebook/groups/info/GroupInfoAdapter;LX/0Pz;)V

    .line 1994155
    :cond_3
    return-void

    .line 1994156
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1994131
    sget-object v1, LX/DQN;->p:LX/0Px;

    move-object v0, v1

    .line 1994132
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1994133
    iget-object v2, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->g:LX/DRS;

    .line 1994134
    sget-object p0, LX/DQN;->p:LX/0Px;

    move-object v0, p0

    .line 1994135
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-virtual {v2, v1, v0}, LX/DRS;->a(Landroid/view/View;LX/DML;)V

    .line 1994136
    return-object v1
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1994128
    check-cast p2, LX/DMB;

    .line 1994129
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 1994130
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1994127
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->r:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1994126
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->r:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1994125
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1994123
    sget-object v1, LX/DQN;->p:LX/0Px;

    move-object v1, v1

    .line 1994124
    iget-object v0, p0, Lcom/facebook/groups/info/GroupInfoAdapter;->r:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1994121
    sget-object p0, LX/DQN;->p:LX/0Px;

    move-object v0, p0

    .line 1994122
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
