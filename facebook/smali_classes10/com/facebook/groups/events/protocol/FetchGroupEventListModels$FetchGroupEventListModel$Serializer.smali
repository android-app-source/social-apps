.class public final Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1990450
    const-class v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;

    new-instance v1, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1990451
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1990449
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1990432
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1990433
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x2

    const/4 v4, 0x1

    .line 1990434
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1990435
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1990436
    if-eqz v2, :cond_0

    .line 1990437
    const-string v3, "group_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1990438
    invoke-static {v1, v2, p1, p2}, LX/DMt;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1990439
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1990440
    if-eqz v2, :cond_1

    .line 1990441
    const-string v2, "viewer_join_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1990442
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1990443
    :cond_1
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1990444
    if-eqz v2, :cond_2

    .line 1990445
    const-string v2, "visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1990446
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1990447
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1990448
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1990431
    check-cast p1, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$Serializer;->a(Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;LX/0nX;LX/0my;)V

    return-void
.end method
