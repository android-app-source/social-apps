.class public final Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4fcef936
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1990487
    const-class v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1990488
    const-class v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1990489
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1990490
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 1990508
    iput-object p1, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1990509
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1990510
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1990511
    if-eqz v0, :cond_0

    .line 1990512
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1990513
    :cond_0
    return-void

    .line 1990514
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 1990491
    iput-object p1, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1990492
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1990493
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1990494
    if-eqz v0, :cond_0

    .line 1990495
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1990496
    :cond_0
    return-void

    .line 1990497
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1990498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1990499
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->a()Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1990500
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1990501
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1990502
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1990503
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1990504
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1990505
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1990506
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1990507
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1990478
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1990479
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->a()Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1990480
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->a()Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    .line 1990481
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->a()Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1990482
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;

    .line 1990483
    iput-object v0, v1, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->e:Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    .line 1990484
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1990485
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1990486
    new-instance v0, LX/DMq;

    invoke-direct {v0, p1}, LX/DMq;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupEvents"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1990476
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->e:Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    iput-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->e:Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    .line 1990477
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->e:Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel$GroupEventsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1990466
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1990467
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1990468
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1990469
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1990470
    :goto_0
    return-void

    .line 1990471
    :cond_0
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1990472
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1990473
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1990474
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1990475
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1990461
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1990462
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    .line 1990463
    :cond_0
    :goto_0
    return-void

    .line 1990464
    :cond_1
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1990465
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1990458
    new-instance v0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;

    invoke-direct {v0}, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;-><init>()V

    .line 1990459
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1990460
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1990457
    const v0, -0x5acb94a6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1990456
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1990454
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1990455
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1990452
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1990453
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupEventListModels$FetchGroupEventListModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method
