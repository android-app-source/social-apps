.class public final Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xaaf555a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1990766
    const-class v0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1990767
    const-class v0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1990768
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1990769
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 1990776
    iput-object p1, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1990777
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1990778
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1990779
    if-eqz v0, :cond_0

    .line 1990780
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1990781
    :cond_0
    return-void

    .line 1990782
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1990770
    iput-object p1, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->f:Ljava/lang/String;

    .line 1990771
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1990772
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1990773
    if-eqz v0, :cond_0

    .line 1990774
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1990775
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1990791
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1990792
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1990793
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->k()Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1990794
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1990795
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1990796
    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->e:Z

    invoke-virtual {p1, v3, v4}, LX/186;->a(IZ)V

    .line 1990797
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1990798
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1990799
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1990800
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1990801
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1990783
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1990784
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->k()Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1990785
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->k()Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    .line 1990786
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->k()Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1990787
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    .line 1990788
    iput-object v0, v1, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->g:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    .line 1990789
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1990790
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1990762
    new-instance v0, LX/DMx;

    invoke-direct {v0, p1}, LX/DMx;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1990763
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1990764
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->e:Z

    .line 1990765
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1990752
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1990753
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1990754
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1990755
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1990756
    :goto_0
    return-void

    .line 1990757
    :cond_0
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1990758
    invoke-virtual {p0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1990759
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1990760
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1990761
    :cond_1
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1990747
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1990748
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->a(Ljava/lang/String;)V

    .line 1990749
    :cond_0
    :goto_0
    return-void

    .line 1990750
    :cond_1
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1990751
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1990745
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1990746
    iget-boolean v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1990742
    new-instance v0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    invoke-direct {v0}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;-><init>()V

    .line 1990743
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1990744
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1990741
    const v0, -0x3f2a86d4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1990740
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1990738
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->f:Ljava/lang/String;

    .line 1990739
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1990736
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->g:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->g:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    .line 1990737
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->g:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$ParentGroupModel;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1990734
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1990735
    iget-object v0, p0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method
