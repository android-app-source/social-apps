.class public final Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1990732
    const-class v0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    new-instance v1, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1990733
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1990731
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1990710
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1990711
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    .line 1990712
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1990713
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1990714
    if-eqz v2, :cond_0

    .line 1990715
    const-string v3, "can_viewer_create_event"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1990716
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1990717
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1990718
    if-eqz v2, :cond_1

    .line 1990719
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1990720
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1990721
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1990722
    if-eqz v2, :cond_2

    .line 1990723
    const-string v3, "parent_group"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1990724
    invoke-static {v1, v2, p1}, LX/DN0;->a(LX/15i;ILX/0nX;)V

    .line 1990725
    :cond_2
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1990726
    if-eqz v2, :cond_3

    .line 1990727
    const-string v2, "visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1990728
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1990729
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1990730
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1990709
    check-cast p1, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Serializer;->a(Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;LX/0nX;LX/0my;)V

    return-void
.end method
