.class public final Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1990613
    const-class v0, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    new-instance v1, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1990614
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1990615
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1990616
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1990617
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1990618
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1990619
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1990620
    :goto_0
    move v1, v2

    .line 1990621
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1990622
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1990623
    new-instance v1, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

    invoke-direct {v1}, Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;-><init>()V

    .line 1990624
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1990625
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1990626
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1990627
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1990628
    :cond_0
    return-object v1

    .line 1990629
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_6

    .line 1990630
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1990631
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1990632
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v8, :cond_1

    .line 1990633
    const-string p0, "can_viewer_create_event"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1990634
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v7, v1

    move v1, v3

    goto :goto_1

    .line 1990635
    :cond_2
    const-string p0, "name"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1990636
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1990637
    :cond_3
    const-string p0, "parent_group"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1990638
    invoke-static {p1, v0}, LX/DN0;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1990639
    :cond_4
    const-string p0, "visibility"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1990640
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 1990641
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1990642
    :cond_6
    const/4 v8, 0x4

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1990643
    if-eqz v1, :cond_7

    .line 1990644
    invoke-virtual {v0, v2, v7}, LX/186;->a(IZ)V

    .line 1990645
    :cond_7
    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1990646
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1990647
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1990648
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_1
.end method
