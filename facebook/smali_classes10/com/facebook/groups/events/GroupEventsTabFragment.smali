.class public Lcom/facebook/groups/events/GroupEventsTabFragment;
.super Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;
.source ""

# interfaces
.implements LX/0fh;


# static fields
.field public static final h:Ljava/lang/Class;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DZ8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/DMg;

.field public j:Lcom/facebook/groups/events/protocol/FetchGroupNameInfoModels$FetchGroupNameInfoModel;

.field private k:Landroid/support/v4/view/ViewPager;

.field private l:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public m:LX/DLO;

.field public n:Ljava/lang/String;

.field private o:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1990183
    const-class v0, Lcom/facebook/groups/events/GroupEventsTabFragment;

    sput-object v0, Lcom/facebook/groups/events/GroupEventsTabFragment;->h:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1990181
    invoke-direct {p0}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;-><init>()V

    .line 1990182
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1990148
    const-string v0, "group_events"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1990174
    invoke-super {p0, p1}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;->a(Landroid/os/Bundle;)V

    .line 1990175
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/events/GroupEventsTabFragment;

    const/16 v3, 0x259

    invoke-static {p1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object v6

    check-cast v6, LX/DZ8;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    const/16 v0, 0x15e7

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/groups/events/GroupEventsTabFragment;->a:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/groups/events/GroupEventsTabFragment;->b:Landroid/content/res/Resources;

    iput-object v5, v2, Lcom/facebook/groups/events/GroupEventsTabFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v2, Lcom/facebook/groups/events/GroupEventsTabFragment;->d:LX/DZ8;

    iput-object v7, v2, Lcom/facebook/groups/events/GroupEventsTabFragment;->e:LX/1Ck;

    iput-object v8, v2, Lcom/facebook/groups/events/GroupEventsTabFragment;->f:LX/0tX;

    iput-object p1, v2, Lcom/facebook/groups/events/GroupEventsTabFragment;->g:LX/0Ot;

    .line 1990176
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1990177
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->n:Ljava/lang/String;

    .line 1990178
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1990179
    const-string v1, "group_mall_type"

    sget-object v2, LX/5QT;->WITHOUT_TABS_LEGACY:LX/5QT;

    invoke-virtual {v2}, LX/5QT;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->o:I

    .line 1990180
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x7805446a

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1990169
    invoke-super {p0, p1}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1990170
    new-instance v1, LX/DMg;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->n:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->b:Landroid/content/res/Resources;

    invoke-direct {v1, v2, v3, v4}, LX/DMg;-><init>(LX/0gc;Ljava/lang/String;Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->i:LX/DMg;

    .line 1990171
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->k:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->i:LX/DMg;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1990172
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->l:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v2, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 1990173
    const/16 v1, 0x2b

    const v2, 0x6a8e24a1

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1990160
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1990161
    const/16 v0, 0xd7

    if-ne p1, v0, :cond_0

    .line 1990162
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->i:LX/DMg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/DK1;->e(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1990163
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/groups/events/GroupUpcomingEventsFragment;

    if-eqz v1, :cond_0

    .line 1990164
    check-cast v0, Lcom/facebook/groups/events/GroupUpcomingEventsFragment;

    .line 1990165
    iget-object v1, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    if-eqz v1, :cond_0

    .line 1990166
    iget-object v1, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    invoke-virtual {v1}, LX/DMe;->c()V

    .line 1990167
    iget-object v1, v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    invoke-virtual {v1}, LX/DMe;->a()V

    .line 1990168
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4945240e    # 807488.9f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1990159
    const v1, 0x7f030807

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2b5745e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x310f9add

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1990156
    invoke-super {p0}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;->onDestroyView()V

    .line 1990157
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->e:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1990158
    const/16 v1, 0x2b

    const v2, -0x3522b875    # -7250885.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1990149
    invoke-super {p0, p1, p2}, Lcom/facebook/groups/draggable/AbstractGroupsDraggableViewPagerContainerFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1990150
    const v0, 0x7f0d152c

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->k:Landroid/support/v4/view/ViewPager;

    .line 1990151
    const v0, 0x7f0d152b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->l:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 1990152
    new-instance v0, LX/DMh;

    invoke-direct {v0, p0}, LX/DMh;-><init>(Lcom/facebook/groups/events/GroupEventsTabFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->m:LX/DLO;

    .line 1990153
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->d:LX/DZ8;

    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->b:Landroid/content/res/Resources;

    const v2, 0x7f0830b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->m:LX/DLO;

    invoke-virtual {v0, p0, v1, v2}, LX/DZ8;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 1990154
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsTabFragment;->e:LX/1Ck;

    sget-object v1, LX/DMk;->CREATE_GROUP_EVENT:LX/DMk;

    new-instance v2, LX/DMi;

    invoke-direct {v2, p0}, LX/DMi;-><init>(Lcom/facebook/groups/events/GroupEventsTabFragment;)V

    new-instance p1, LX/DMj;

    invoke-direct {p1, p0}, LX/DMj;-><init>(Lcom/facebook/groups/events/GroupEventsTabFragment;)V

    invoke-virtual {v0, v1, v2, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1990155
    return-void
.end method
