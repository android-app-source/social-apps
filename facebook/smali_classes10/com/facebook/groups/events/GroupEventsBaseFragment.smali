.class public abstract Lcom/facebook/groups/events/GroupEventsBaseFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/DMf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DMK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DMZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:LX/DMV;

.field public g:Z

.field private h:Ljava/lang/String;

.field public i:LX/DMe;

.field public j:LX/DMJ;

.field private final k:LX/DMP;

.field private final l:LX/DMR;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1989919
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1989920
    new-instance v0, LX/DMQ;

    invoke-direct {v0, p0}, LX/DMQ;-><init>(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->k:LX/DMP;

    .line 1989921
    new-instance v0, LX/DMS;

    invoke-direct {v0, p0}, LX/DMS;-><init>(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->l:LX/DMR;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/events/GroupEventsBaseFragment;

    const-class v1, LX/DMf;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/DMf;

    const-class v2, LX/DMK;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/DMK;

    invoke-static {p0}, LX/DMZ;->b(LX/0QB;)LX/DMZ;

    move-result-object p0

    check-cast p0, LX/DMZ;

    iput-object v1, p1, Lcom/facebook/groups/events/GroupEventsBaseFragment;->a:LX/DMf;

    iput-object v2, p1, Lcom/facebook/groups/events/GroupEventsBaseFragment;->b:LX/DMK;

    iput-object p0, p1, Lcom/facebook/groups/events/GroupEventsBaseFragment;->c:LX/DMZ;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/events/GroupEventsBaseFragment;Z)V
    .locals 4

    .prologue
    .line 1989909
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->d:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1989910
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v0}, LX/DMV;->d()I

    move-result v2

    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v0}, LX/DMV;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1989911
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v0}, LX/DMV;->e()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->d:Landroid/view/View;

    .line 1989912
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v0}, LX/DMV;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 1989913
    :cond_0
    if-eqz p1, :cond_1

    .line 1989914
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v1}, LX/DMV;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1989915
    :goto_0
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1989916
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v0}, LX/DMV;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1989917
    return-void

    .line 1989918
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v1}, LX/DMV;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    goto :goto_0
.end method

.method public static l(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V
    .locals 1

    .prologue
    .line 1989907
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    invoke-virtual {v0}, LX/DMe;->a()V

    .line 1989908
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 1989896
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1989897
    const-class v0, Lcom/facebook/groups/events/GroupEventsBaseFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/events/GroupEventsBaseFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1989898
    invoke-virtual {p0}, Lcom/facebook/groups/events/GroupEventsBaseFragment;->b()LX/DMV;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    .line 1989899
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1989900
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->h:Ljava/lang/String;

    .line 1989901
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->b:LX/DMK;

    invoke-virtual {p0}, Lcom/facebook/groups/events/GroupEventsBaseFragment;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->k:LX/DMP;

    .line 1989902
    new-instance v5, LX/DMJ;

    const-class v6, LX/38v;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/38v;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v9

    check-cast v9, Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0xc

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-static {v0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v11

    check-cast v11, LX/6RZ;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v12

    check-cast v12, Landroid/content/res/Resources;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v13

    check-cast v13, LX/17W;

    move-object v6, v1

    move-object v8, v2

    invoke-direct/range {v5 .. v13}, LX/DMJ;-><init>(Ljava/lang/String;LX/38v;LX/DMP;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/6RZ;Landroid/content/res/Resources;LX/17W;)V

    .line 1989903
    move-object v0, v5

    .line 1989904
    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->j:LX/DMJ;

    .line 1989905
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->a:LX/DMf;

    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->h:Ljava/lang/String;

    sget-object v2, LX/DMe;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-instance v3, LX/DMT;

    invoke-direct {v3, p0}, LX/DMT;-><init>(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V

    invoke-virtual {p0}, Lcom/facebook/groups/events/GroupEventsBaseFragment;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/DMf;->a(Ljava/lang/String;ILX/DMT;Ljava/lang/String;)LX/DMe;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    .line 1989906
    return-void
.end method

.method public abstract b()LX/DMV;
.end method

.method public abstract c()Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/EventTimeframe;
    .end annotation
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x10565304

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1989895
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v1}, LX/DMV;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2f95e4d2

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x76d65b30

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1989891
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1989892
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    .line 1989893
    iget-object v2, v1, LX/DMe;->n:LX/1My;

    invoke-virtual {v2}, LX/1My;->a()V

    .line 1989894
    const/16 v1, 0x2b

    const v2, 0x1a402183

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4438dcb3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1989883
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1989884
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->c:LX/DMZ;

    iget-object v2, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->l:LX/DMR;

    invoke-virtual {v1, v2}, LX/DMZ;->b(LX/DMR;)V

    .line 1989885
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    .line 1989886
    invoke-virtual {v1}, LX/DMe;->c()V

    .line 1989887
    iget-object v2, v1, LX/DMe;->d:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1989888
    iput-object v4, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->d:Landroid/view/View;

    .line 1989889
    iput-object v4, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 1989890
    const/16 v1, 0x2b

    const v2, -0x61cf7d4f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1d20dc86

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1989879
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1989880
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    .line 1989881
    iget-object v2, v1, LX/DMe;->n:LX/1My;

    invoke-virtual {v2}, LX/1My;->d()V

    .line 1989882
    const/16 v1, 0x2b

    const v2, -0x300fa7b8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5d4ad91e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1989864
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1989865
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->i:LX/DMe;

    .line 1989866
    iget-object v2, v1, LX/DMe;->n:LX/1My;

    invoke-virtual {v2}, LX/1My;->e()V

    .line 1989867
    const/16 v1, 0x2b

    const v2, -0x663b045d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1989870
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1989871
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->c:LX/DMZ;

    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->l:LX/DMR;

    .line 1989872
    iput-object v1, v0, LX/DMZ;->f:LX/DMR;

    .line 1989873
    iget-object v0, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->f:LX/DMV;

    invoke-interface {v0}, LX/DMV;->b()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1989874
    iget-object v1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->j:LX/DMJ;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1989875
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 1989876
    new-instance v1, LX/DMU;

    invoke-direct {v1, p0}, LX/DMU;-><init>(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1989877
    invoke-static {p0}, Lcom/facebook/groups/events/GroupEventsBaseFragment;->l(Lcom/facebook/groups/events/GroupEventsBaseFragment;)V

    .line 1989878
    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 0

    .prologue
    .line 1989868
    iput-boolean p1, p0, Lcom/facebook/groups/events/GroupEventsBaseFragment;->g:Z

    .line 1989869
    return-void
.end method
