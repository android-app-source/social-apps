.class public final Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4a312e79
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013672
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013631
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2013670
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2013671
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V
    .locals 4

    .prologue
    .line 2013663
    iput-object p1, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 2013664
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2013665
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2013666
    if-eqz v0, :cond_0

    .line 2013667
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2013668
    :cond_0
    return-void

    .line 2013669
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013661
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->e:Ljava/lang/String;

    .line 2013662
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013659
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    .line 2013660
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2013651
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013652
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2013653
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2013654
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2013655
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2013656
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2013657
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013658
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2013648
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013650
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2013647
    new-instance v0, LX/DZn;

    invoke-direct {v0, p1}, LX/DZn;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013646
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2013640
    const-string v0, "viewer_push_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2013641
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2013642
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2013643
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2013644
    :goto_0
    return-void

    .line 2013645
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2013637
    const-string v0, "viewer_push_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2013638
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupPushSubscriptionLevel;)V

    .line 2013639
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2013634
    new-instance v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;-><init>()V

    .line 2013635
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2013636
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2013633
    const v0, -0x596bb52c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2013632
    const v0, 0x41e065f

    return v0
.end method
