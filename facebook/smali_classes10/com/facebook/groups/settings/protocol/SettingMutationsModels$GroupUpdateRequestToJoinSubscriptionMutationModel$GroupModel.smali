.class public final Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x596ebddf
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013787
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013786
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2013784
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2013785
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013782
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;->e:Ljava/lang/String;

    .line 2013783
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013780
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    .line 2013781
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2013772
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013773
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2013774
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupRequestToJoinSubscriptionLevel;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2013775
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2013776
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2013777
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2013778
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013779
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2013788
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013789
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013790
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2013771
    new-instance v0, LX/DZo;

    invoke-direct {v0, p1}, LX/DZo;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013770
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2013768
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2013769
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2013767
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2013764
    new-instance v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateRequestToJoinSubscriptionMutationModel$GroupModel;-><init>()V

    .line 2013765
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2013766
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2013763
    const v0, 0x74baf2dc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2013762
    const v0, 0x41e065f

    return v0
.end method
