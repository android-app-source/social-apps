.class public final Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x35c3678c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013704
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013703
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2013701
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2013702
    return-void
.end method

.method private a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013699
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;->e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;->e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    .line 2013700
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;->e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2013705
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013706
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;->a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2013707
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2013708
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2013709
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013710
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2013691
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013692
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;->a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2013693
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;->a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    .line 2013694
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;->a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2013695
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;

    .line 2013696
    iput-object v0, v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;->e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel$GroupModel;

    .line 2013697
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013698
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2013688
    new-instance v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdatePushSubscriptionMutationModel;-><init>()V

    .line 2013689
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2013690
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2013687
    const v0, 0x7f9b6013

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2013686
    const v0, -0x1a59b1f8

    return v0
.end method
