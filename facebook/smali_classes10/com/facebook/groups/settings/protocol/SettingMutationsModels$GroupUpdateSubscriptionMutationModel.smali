.class public final Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xcaf5d4f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013960
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013967
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2013965
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2013966
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013963
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->e:Ljava/lang/String;

    .line 2013964
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013961
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    .line 2013962
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2013968
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013969
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2013970
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2013971
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2013972
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2013973
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2013974
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013975
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2013947
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013948
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2013949
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    .line 2013950
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2013951
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;

    .line 2013952
    iput-object v0, v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;->f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    .line 2013953
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013954
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2013955
    new-instance v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel;-><init>()V

    .line 2013956
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2013957
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2013958
    const v0, 0x52f89920

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2013959
    const v0, -0x67012932

    return v0
.end method
