.class public final Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2ca7fa74
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013929
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2013928
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2013926
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2013927
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V
    .locals 4

    .prologue
    .line 2013919
    iput-object p1, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 2013920
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2013921
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2013922
    if-eqz v0, :cond_0

    .line 2013923
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2013924
    :cond_0
    return-void

    .line 2013925
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013917
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->e:Ljava/lang/String;

    .line 2013918
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013915
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 2013916
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2013907
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013908
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2013909
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2013910
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2013911
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2013912
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2013913
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013914
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2013904
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2013905
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2013906
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2013888
    new-instance v0, LX/DZp;

    invoke-direct {v0, p1}, LX/DZp;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2013903
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2013897
    const-string v0, "viewer_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2013898
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2013899
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2013900
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2013901
    :goto_0
    return-void

    .line 2013902
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2013894
    const-string v0, "viewer_subscription_level"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2013895
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-direct {p0, p2}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V

    .line 2013896
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2013891
    new-instance v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$GroupUpdateSubscriptionMutationModel$GroupModel;-><init>()V

    .line 2013892
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2013893
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2013890
    const v0, -0xdd218e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2013889
    const v0, 0x41e065f

    return v0
.end method
