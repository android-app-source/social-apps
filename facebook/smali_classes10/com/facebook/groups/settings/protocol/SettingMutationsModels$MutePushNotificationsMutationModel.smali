.class public final Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x21d3bd8f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2014217
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2014216
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2014214
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2014215
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2014212
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->e:Ljava/lang/String;

    .line 2014213
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2014210
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    .line 2014211
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2014189
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2014190
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2014191
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2014192
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2014193
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2014194
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2014195
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2014196
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2014202
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2014203
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2014204
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    .line 2014205
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2014206
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;

    .line 2014207
    iput-object v0, v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;->f:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    .line 2014208
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2014209
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2014199
    new-instance v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;-><init>()V

    .line 2014200
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2014201
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2014198
    const v0, 0x2e7ae38a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2014197
    const v0, -0x148fe643

    return v0
.end method
