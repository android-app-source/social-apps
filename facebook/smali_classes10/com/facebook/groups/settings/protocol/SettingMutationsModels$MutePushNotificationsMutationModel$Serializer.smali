.class public final Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2014172
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;

    new-instance v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2014173
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2014174
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2014175
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2014176
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2014177
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2014178
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2014179
    if-eqz v2, :cond_0

    .line 2014180
    const-string p0, "client_mutation_id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2014181
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2014182
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2014183
    if-eqz v2, :cond_1

    .line 2014184
    const-string p0, "push_token"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2014185
    invoke-static {v1, v2, p1, p2}, LX/DZx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2014186
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2014187
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2014188
    check-cast p1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$Serializer;->a(Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
