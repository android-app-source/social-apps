.class public final Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2f26e3f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2014166
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2014167
    const-class v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2014170
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2014171
    return-void
.end method

.method private a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2014168
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    .line 2014169
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    return-object v0
.end method

.method private j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2014164
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->g:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->g:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    .line 2014165
    iget-object v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->g:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2014134
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2014135
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2014136
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2014137
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2014138
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2014139
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2014140
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2014141
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2014142
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2014151
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2014152
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2014153
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    .line 2014154
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->a()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2014155
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    .line 2014156
    iput-object v0, v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->e:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$ApplicationModel;

    .line 2014157
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2014158
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    .line 2014159
    invoke-direct {p0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->j()Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2014160
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    .line 2014161
    iput-object v0, v1, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->g:Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel$OwnerModel;

    .line 2014162
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2014163
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2014148
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2014149
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;->f:J

    .line 2014150
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2014145
    new-instance v0, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;

    invoke-direct {v0}, Lcom/facebook/groups/settings/protocol/SettingMutationsModels$MutePushNotificationsMutationModel$PushTokenModel;-><init>()V

    .line 2014146
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2014147
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2014144
    const v0, 0x3bea777f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2014143
    const v0, 0x49049adf

    return v0
.end method
