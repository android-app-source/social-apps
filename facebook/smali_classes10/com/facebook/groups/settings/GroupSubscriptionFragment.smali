.class public Lcom/facebook/groups/settings/GroupSubscriptionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:LX/DZW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DZ7;
    .annotation runtime Lcom/facebook/groups/settings/annotation/GroupSubscriptionsNavHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DZg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1g8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/widget/listview/BetterListView;

.field private i:LX/DZf;

.field private j:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2013499
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static b(Lcom/facebook/groups/settings/GroupSubscriptionFragment;)V
    .locals 4

    .prologue
    .line 2013496
    iget-object v0, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v2, LX/27k;

    iget-object v1, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    const v3, 0x7f082fe7

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2013497
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2013498
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2013495
    const-string v0, "group_notification_settings"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2013468
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;

    new-instance v5, LX/DZW;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/DZI;->b(LX/0QB;)LX/DZI;

    move-result-object v4

    check-cast v4, LX/DZH;

    invoke-direct {v5, v3, v4}, LX/DZW;-><init>(Landroid/content/res/Resources;LX/DZH;)V

    move-object v3, v5

    check-cast v3, LX/DZW;

    invoke-static {v0}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object v4

    check-cast v4, LX/DZ7;

    const-class v5, LX/DZg;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/DZg;

    const/16 v6, 0x12c4

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1b

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v0

    check-cast v0, LX/1g8;

    iput-object v3, v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->a:LX/DZW;

    iput-object v4, v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->b:LX/DZ7;

    iput-object v5, v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->c:LX/DZg;

    iput-object v6, v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->d:LX/0Ot;

    iput-object v7, v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->e:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->f:LX/1g8;

    .line 2013469
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2013470
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4fbb9c7e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2013494
    const v1, 0x7f030830

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7d29dbdf

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x36da0fe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2013490
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2013491
    iget-object v1, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->i:LX/DZf;

    .line 2013492
    iget-object v2, v1, LX/DZf;->a:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2013493
    const/16 v1, 0x2b

    const v2, 0x22475808

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2013471
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2013472
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2013473
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->g:Ljava/lang/String;

    .line 2013474
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2013475
    const-string v1, "group_admin_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->j:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2013476
    const v0, 0x7f0d1566

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->h:Lcom/facebook/widget/listview/BetterListView;

    .line 2013477
    iget-object v0, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->c:LX/DZg;

    iget-object v1, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->g:Ljava/lang/String;

    new-instance v2, LX/DZb;

    invoke-direct {v2, p0}, LX/DZb;-><init>(Lcom/facebook/groups/settings/GroupSubscriptionFragment;)V

    .line 2013478
    new-instance p2, LX/DZf;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p1

    check-cast p1, LX/0tX;

    invoke-direct {p2, v3, p1, v1, v2}, LX/DZf;-><init>(LX/1Ck;LX/0tX;Ljava/lang/String;LX/DZb;)V

    .line 2013479
    move-object v0, p2

    .line 2013480
    iput-object v0, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->i:LX/DZf;

    .line 2013481
    iget-object v0, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->i:LX/DZf;

    .line 2013482
    iget-object v1, v0, LX/DZf;->a:LX/1Ck;

    const-string v2, "fetch_group_settings_row"

    new-instance v3, LX/DZd;

    invoke-direct {v3, v0}, LX/DZd;-><init>(LX/DZf;)V

    new-instance p1, LX/DZe;

    invoke-direct {p1, v0}, LX/DZe;-><init>(LX/DZf;)V

    invoke-virtual {v1, v2, v3, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2013483
    iget-object v0, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->f:LX/1g8;

    iget-object v1, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/settings/GroupSubscriptionFragment;->j:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2013484
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "admin_panel_request_notif_view"

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "group_notification_settings"

    .line 2013485
    iput-object p0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2013486
    move-object v3, v3

    .line 2013487
    const-string p0, "group_id"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "viewer_admin_type"

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2013488
    iget-object p0, v0, LX/1g8;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2013489
    return-void
.end method
