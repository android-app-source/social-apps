.class public final Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic d:I

.field public final synthetic e:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;I)V
    .locals 0

    .prologue
    .line 1987629
    iput-object p1, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->e:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iput-object p2, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->c:Lcom/google/common/util/concurrent/SettableFuture;

    iput p5, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1987630
    new-instance v0, LX/DKv;

    invoke-direct {v0, p0}, LX/DKv;-><init>(Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;)V

    .line 1987631
    new-instance v1, LX/34X;

    iget-object v2, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, v2, v0, v3}, LX/34X;-><init>(Landroid/net/Uri;LX/1uy;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1987632
    :try_start_0
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->e:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    iget-object v0, v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->e:LX/DLX;

    invoke-virtual {v0, v1}, LX/3AP;->a(LX/34X;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 1987633
    if-nez v0, :cond_0

    .line 1987634
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->c:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, LX/DKx;

    iget v2, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->d:I

    const-string v3, "Empty resource"

    invoke-direct {v1, v2, v3}, LX/DKx;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1987635
    :goto_0
    return-void

    .line 1987636
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->c:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, LX/DKy;

    iget v3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->d:I

    invoke-direct {v2, v3, v0}, LX/DKy;-><init>(ILjava/io/File;)V

    const v0, 0x141d969b

    invoke-static {v1, v2, v0}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1987637
    :catch_0
    move-exception v0

    .line 1987638
    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->c:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, LX/DKx;

    iget v3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;->d:I

    invoke-direct {v2, v3, v0}, LX/DKx;-><init>(ILjava/lang/Exception;)V

    invoke-virtual {v1, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
