.class public Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/DLX;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/1Ck;

.field public final i:LX/DM4;

.field public final j:Landroid/app/NotificationManager;

.field public final k:Landroid/content/Context;

.field public final l:LX/DLT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1987680
    const-class v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->a:Ljava/lang/String;

    .line 1987681
    const-class v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/DLX;LX/0Ot;LX/0Ot;LX/1Ck;LX/DM4;Landroid/app/NotificationManager;Landroid/content/Context;LX/DLT;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p9    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # LX/DLT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/DLX;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/1Ck;",
            "Lcom/facebook/groups/docsandfiles/view/GroupDocsAndFilesViewFactory;",
            "Landroid/app/NotificationManager;",
            "Landroid/content/Context;",
            "Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$GroupsDocsAndFilesDownloadControllerListener;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1987682
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1987683
    iput-object p1, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->c:Ljava/util/concurrent/ExecutorService;

    .line 1987684
    iput-object p2, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->d:LX/0Ot;

    .line 1987685
    iput-object p3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->e:LX/DLX;

    .line 1987686
    iput-object p4, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->f:LX/0Ot;

    .line 1987687
    iput-object p5, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->g:LX/0Ot;

    .line 1987688
    iput-object p6, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->h:LX/1Ck;

    .line 1987689
    iput-object p7, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->i:LX/DM4;

    .line 1987690
    iput-object p8, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->j:Landroid/app/NotificationManager;

    .line 1987691
    iput-object p9, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->k:Landroid/content/Context;

    .line 1987692
    iput-object p10, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->l:LX/DLT;

    .line 1987693
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 11

    .prologue
    .line 1987694
    const-string v0, " "

    const-string v1, "%20"

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 1987695
    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->h:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DOWNLOAD_GROUP_FILES_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1987696
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v8

    .line 1987697
    iget-object v10, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;

    move-object v5, p0

    move-object v6, p1

    move-object v7, v0

    move v9, p3

    invoke-direct/range {v4 .. v9}, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController$1;-><init>(Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;Ljava/lang/String;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;I)V

    const v5, 0x66397a07

    invoke-static {v10, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1987698
    move-object v0, v8

    .line 1987699
    new-instance v3, LX/DKw;

    invoke-direct {v3, p0}, LX/DKw;-><init>(Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;)V

    move-object v3, v3

    .line 1987700
    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1987701
    return-void
.end method
