.class public final Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/DL8;


# direct methods
.method public constructor <init>(LX/DL8;Ljava/io/File;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1987736
    iput-object p1, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->c:LX/DL8;

    iput-object p2, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->a:Ljava/io/File;

    iput-object p3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1987737
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->a:Ljava/io/File;

    .line 1987738
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/DM3;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1987739
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1987740
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1987741
    :goto_0
    move-object v1, v1

    .line 1987742
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->c:LX/DL8;

    iget-object v0, v0, LX/DL8;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7zS;

    .line 1987743
    iget-object v2, v0, LX/7zS;->a:LX/7z2;

    move-object v0, v2

    .line 1987744
    new-instance v2, LX/7yy;

    iget-object v3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->a:Ljava/io/File;

    invoke-direct {v2, v3, v1}, LX/7yy;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1987745
    new-instance v1, LX/7yw;

    sget-object v3, LX/7yt;->GROUPS:LX/7yt;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v5, LX/7yu;

    invoke-direct {v5}, LX/7yu;-><init>()V

    invoke-direct {v1, v3, v4, v5}, LX/7yw;-><init>(LX/7yt;Ljava/util/Map;LX/7yu;)V

    .line 1987746
    iget-object v3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->c:LX/DL8;

    iget-object v3, v3, LX/DL8;->m:LX/7yp;

    if-nez v3, :cond_0

    .line 1987747
    iget-object v3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->c:LX/DL8;

    new-instance v4, LX/DL3;

    invoke-direct {v4}, LX/DL3;-><init>()V

    .line 1987748
    iput-object v4, v3, LX/DL8;->m:LX/7yp;

    .line 1987749
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->a:Ljava/io/File;

    invoke-static {v3}, LX/DL8;->d(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    .line 1987750
    iget-object v4, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->c:LX/DL8;

    iget-object v4, v4, LX/DL8;->m:LX/7yp;

    invoke-virtual {v0, v2, v1, v4}, LX/7z2;->a(LX/7yy;LX/7yw;LX/7yp;)LX/7z0;

    move-result-object v1

    .line 1987751
    iget-object v2, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->c:LX/DL8;

    invoke-static {v2, v3, v1}, LX/DL8;->a$redex0(LX/DL8;Ljava/lang/String;LX/7z0;)V

    .line 1987752
    invoke-virtual {v0, v1}, LX/7z2;->c(LX/7z0;)LX/7zL;

    move-result-object v0

    iget-object v0, v0, LX/7zL;->a:Ljava/lang/String;

    .line 1987753
    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->c:LX/DL8;

    const/4 v2, 0x0

    invoke-static {v1, v3, v2}, LX/DL8;->a$redex0(LX/DL8;Ljava/lang/String;LX/7z0;)V

    .line 1987754
    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->c:LX/DL8;

    iget-object v2, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->a:Ljava/io/File;

    iget-object v3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-static {v1, v2, v0, v3}, LX/DL8;->a$redex0(LX/DL8;Ljava/io/File;Ljava/lang/String;Lcom/google/common/util/concurrent/SettableFuture;)V
    :try_end_0
    .catch LX/7zB; {:try_start_0 .. :try_end_0} :catch_0

    .line 1987755
    :goto_1
    return-void

    .line 1987756
    :catch_0
    move-exception v0

    .line 1987757
    iget-boolean v0, v0, LX/7zB;->mIsCancellation:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/DL5;->SEGMENT_UPLOADING_CANCELLATION:LX/DL5;

    .line 1987758
    :goto_2
    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v2, LX/DL2;

    iget-object v3, p0, Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesUploadController$2;->a:Ljava/io/File;

    invoke-direct {v2, v0, v3}, LX/DL2;-><init>(LX/DL5;Ljava/io/File;)V

    invoke-virtual {v1, v2}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_1

    .line 1987759
    :cond_1
    sget-object v0, LX/DL5;->SEGMENT_UPLOADING_FAILURE:LX/DL5;

    goto :goto_2

    :cond_2
    const-string v1, "application/octet-stream"

    goto :goto_0
.end method
