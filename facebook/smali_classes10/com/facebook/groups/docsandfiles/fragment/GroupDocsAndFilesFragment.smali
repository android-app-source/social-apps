.class public Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# static fields
.field private static final l:[Ljava/lang/String;

.field public static final m:[Ljava/lang/String;

.field private static final n:Ljava/lang/String;


# instance fields
.field public A:Z

.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DLc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DKz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DL9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DKu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DZ7;
    .annotation runtime Lcom/facebook/groups/docsandfiles/annotation/DocsAndFilesNavigationHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/DM4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/DKt;

.field public p:LX/DLb;

.field public q:Lcom/facebook/groups/docsandfiles/controller/GroupsDocsAndFilesDownloadController;

.field public r:LX/DL8;

.field private s:Ljava/lang/String;

.field private t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public u:LX/DLO;

.field public v:LX/1P0;

.field private w:Landroid/widget/LinearLayout;

.field private x:Lcom/facebook/widget/text/BetterButton;

.field public y:LX/0i5;

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1988308
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v2

    sput-object v0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->l:[Ljava/lang/String;

    .line 1988309
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v2

    sput-object v0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->m:[Ljava/lang/String;

    .line 1988310
    const-class v0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->n:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1988307
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1988191
    if-nez p1, :cond_1

    .line 1988192
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->w:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 1988193
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1988194
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1988195
    :goto_0
    return-void

    .line 1988196
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->w:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    .line 1988197
    const v0, 0x7f0d1533

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1988198
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->w:Landroid/widget/LinearLayout;

    .line 1988199
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->w:Landroid/widget/LinearLayout;

    const v1, 0x7f0d15b1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterButton;

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->x:Lcom/facebook/widget/text/BetterButton;

    .line 1988200
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->x:Lcom/facebook/widget/text/BetterButton;

    new-instance v1, LX/DLR;

    invoke-direct {v1, p0}, LX/DLR;-><init>(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1988201
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1988202
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V
    .locals 3

    .prologue
    .line 1988305
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->y:LX/0i5;

    sget-object v1, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->l:[Ljava/lang/String;

    new-instance v2, LX/DLQ;

    invoke-direct {v2, p0}, LX/DLQ;-><init>(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1988306
    return-void
.end method

.method public static d(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)Z
    .locals 1

    .prologue
    .line 1988302
    iget-boolean v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->A:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->r:LX/DL8;

    .line 1988303
    iget-object p0, v0, LX/DL8;->l:Ljava/util/Map;

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 1988304
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static e$redex0(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V
    .locals 5

    .prologue
    .line 1988295
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->p:LX/DLb;

    .line 1988296
    iget-boolean v1, v0, LX/DLb;->h:Z

    if-nez v1, :cond_0

    .line 1988297
    iget-object v1, v0, LX/DLb;->c:LX/1Ck;

    sget-object v2, LX/DLa;->FETCH_GROUP_DOCS_AND_FILES:LX/DLa;

    .line 1988298
    iget-object v3, v0, LX/DLb;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/groups/docsandfiles/loader/GroupDocsAndFilesPagedListLoader$2;

    invoke-direct {v4, v0}, Lcom/facebook/groups/docsandfiles/loader/GroupDocsAndFilesPagedListLoader$2;-><init>(LX/DLb;)V

    const p0, 0x43882366

    invoke-static {v3, v4, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1988299
    new-instance v3, LX/DLZ;

    invoke-direct {v3, v0}, LX/DLZ;-><init>(LX/DLb;)V

    move-object v3, v3

    .line 1988300
    new-instance v4, LX/DLY;

    invoke-direct {v4, v0}, LX/DLY;-><init>(LX/DLb;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1988301
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1988294
    const-string v0, "group_files_and_docs"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 1988277
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1988278
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const-class v4, LX/DLc;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DLc;

    const-class v5, LX/DKz;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/DKz;

    const-class v6, LX/DL9;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/DL9;

    const-class v7, LX/DKu;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/DKu;

    const-class v8, LX/0i4;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/0i4;

    invoke-static {v0}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object v9

    check-cast v9, LX/DZ7;

    invoke-static {v0}, LX/DM4;->b(LX/0QB;)LX/DM4;

    move-result-object v10

    check-cast v10, LX/DM4;

    const/16 v11, 0x12c4

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 p1, 0x259

    invoke-static {v0, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v3, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->a:Landroid/content/res/Resources;

    iput-object v4, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->b:LX/DLc;

    iput-object v5, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->c:LX/DKz;

    iput-object v6, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->d:LX/DL9;

    iput-object v7, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->e:LX/DKu;

    iput-object v8, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->f:LX/0i4;

    iput-object v9, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->g:LX/DZ7;

    iput-object v10, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->h:LX/DM4;

    iput-object v11, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->i:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->j:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->k:Lcom/facebook/content/SecureContextHelper;

    .line 1988279
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1988280
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->s:Ljava/lang/String;

    .line 1988281
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1988282
    const-string v1, "groups_launch_file_selector"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->z:Z

    .line 1988283
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->e:LX/DKu;

    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->s:Ljava/lang/String;

    .line 1988284
    new-instance v3, LX/DKt;

    const-class v2, LX/DLK;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/DLK;

    invoke-direct {v3, v1, v2}, LX/DKt;-><init>(Ljava/lang/String;LX/DLK;)V

    .line 1988285
    move-object v0, v3

    .line 1988286
    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    .line 1988287
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    .line 1988288
    iput-object p0, v0, LX/DKt;->d:Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;

    .line 1988289
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->b:LX/DLc;

    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->s:Ljava/lang/String;

    new-instance v2, LX/DLL;

    invoke-direct {v2, p0}, LX/DLL;-><init>(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V

    .line 1988290
    new-instance v3, LX/DLb;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    move-object v7, v1

    move-object v8, v2

    invoke-direct/range {v3 .. v8}, LX/DLb;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/1Ck;Ljava/lang/String;LX/DLL;)V

    .line 1988291
    move-object v0, v3

    .line 1988292
    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->p:LX/DLb;

    .line 1988293
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 11

    .prologue
    .line 1988227
    packed-switch p1, :pswitch_data_0

    .line 1988228
    :cond_0
    :goto_0
    return-void

    .line 1988229
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1988230
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 1988231
    if-nez v1, :cond_5

    .line 1988232
    :cond_1
    :goto_1
    move-object v0, v5

    .line 1988233
    if-nez v0, :cond_2

    .line 1988234
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid file path. Original uri is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1988235
    :cond_2
    if-eqz v0, :cond_f

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 1988236
    const/4 v1, 0x1

    .line 1988237
    :goto_2
    move v1, v1

    .line 1988238
    if-eqz v1, :cond_4

    .line 1988239
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1988240
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->r:LX/DL8;

    if-nez v0, :cond_3

    .line 1988241
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->d:LX/DL9;

    iget-object v2, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->s:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, LX/DLN;

    invoke-direct {v4, p0}, LX/DLN;-><init>(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V

    invoke-virtual {v0, v2, v3, v4}, LX/DL9;->a(Ljava/lang/String;Landroid/content/Context;LX/DLN;)LX/DL8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->r:LX/DL8;

    .line 1988242
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->r:LX/DL8;

    invoke-virtual {v0, v1}, LX/DL8;->a(Ljava/io/File;)V

    goto :goto_0

    .line 1988243
    :cond_4
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Non local file is selected. Original uri is: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1988244
    :cond_5
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x13

    if-lt v6, v9, :cond_6

    move v6, v7

    .line 1988245
    :goto_3
    if-eqz v6, :cond_c

    invoke-static {v0, v1}, Landroid/provider/DocumentsContract;->isDocumentUri(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1988246
    invoke-static {v1}, Landroid/provider/DocumentsContract;->getDocumentId(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    .line 1988247
    const-string v9, "com.android.externalstorage.documents"

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    move v9, v9

    .line 1988248
    if-eqz v9, :cond_7

    .line 1988249
    const-string v9, ":"

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1988250
    aget-object v8, v6, v8

    .line 1988251
    const-string v9, "primary"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1988252
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_6
    move v6, v8

    .line 1988253
    goto :goto_3

    .line 1988254
    :cond_7
    const-string v9, "com.android.providers.downloads.documents"

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    move v9, v9

    .line 1988255
    if-eqz v9, :cond_8

    .line 1988256
    const-string v7, "content://downloads/public_downloads"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v7, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 1988257
    invoke-static {v0, v6, v5, v5}, LX/DM3;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 1988258
    :cond_8
    const-string v9, "com.android.providers.media.documents"

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    move v9, v9

    .line 1988259
    if-eqz v9, :cond_1

    .line 1988260
    const-string v9, ":"

    invoke-virtual {v6, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1988261
    aget-object v9, v6, v8

    .line 1988262
    const-string v10, "image"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1988263
    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 1988264
    :cond_9
    :goto_4
    new-array v9, v7, [Ljava/lang/String;

    aget-object v6, v6, v7

    aput-object v6, v9, v8

    .line 1988265
    const-string v6, "_id=?"

    invoke-static {v0, v5, v6, v9}, LX/DM3;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 1988266
    :cond_a
    const-string v10, "video"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1988267
    sget-object v5, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_4

    .line 1988268
    :cond_b
    const-string v10, "audio"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1988269
    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_4

    .line 1988270
    :cond_c
    const-string v6, "content"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 1988271
    const-string v6, "com.google.android.apps.photos.content"

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    move v6, v6

    .line 1988272
    if-eqz v6, :cond_d

    .line 1988273
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 1988274
    :cond_d
    invoke-static {v0, v1, v5, v5}, LX/DM3;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    .line 1988275
    :cond_e
    const-string v6, "file"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1988276
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_1

    :cond_f
    const/4 v1, 0x0

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x40beecef

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1988226
    const v1, 0x7f03080a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x20360e66

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5b5e8b24

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1988217
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1988218
    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->p:LX/DLb;

    .line 1988219
    const/4 v2, 0x0

    .line 1988220
    iput-object v2, v1, LX/DLb;->g:Ljava/lang/String;

    .line 1988221
    iput-object v2, v1, LX/DLb;->e:LX/0Px;

    .line 1988222
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/DLb;->h:Z

    .line 1988223
    iget-object v2, v1, LX/DLb;->c:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1988224
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->w:Landroid/widget/LinearLayout;

    .line 1988225
    const/16 v1, 0x2b

    const v2, -0x62dad228

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1988203
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1988204
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->f:LX/0i4;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->y:LX/0i5;

    .line 1988205
    new-instance v0, LX/DLP;

    invoke-direct {v0, p0}, LX/DLP;-><init>(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->u:LX/DLO;

    .line 1988206
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->g:LX/DZ7;

    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->a:Landroid/content/res/Resources;

    const v2, 0x7f0830a0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->u:LX/DLO;

    invoke-interface {v0, p0, v1, v2}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 1988207
    const v0, 0x7f0d1532

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1988208
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->o:LX/DKt;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1988209
    new-instance v0, LX/1P0;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P0;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->v:LX/1P0;

    .line 1988210
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->v:LX/1P0;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1988211
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->t:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, LX/DLM;

    invoke-direct {v1, p0}, LX/DLM;-><init>(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1988212
    invoke-static {p0}, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->e$redex0(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V

    .line 1988213
    iget-boolean v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->z:Z

    if-eqz v0, :cond_0

    .line 1988214
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->z:Z

    .line 1988215
    invoke-static {p0}, Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;->c$redex0(Lcom/facebook/groups/docsandfiles/fragment/GroupDocsAndFilesFragment;)V

    .line 1988216
    :cond_0
    return-void
.end method
