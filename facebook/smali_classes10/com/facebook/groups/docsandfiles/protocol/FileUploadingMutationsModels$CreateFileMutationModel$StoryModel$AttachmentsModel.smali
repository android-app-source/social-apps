.class public final Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x146a8e64
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1988599
    const-class v0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1988602
    const-class v0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1988600
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1988601
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1988587
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->g:Ljava/lang/String;

    .line 1988588
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1988589
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1988590
    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1988591
    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1988592
    invoke-direct {p0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1988593
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1988594
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1988595
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1988596
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1988597
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1988598
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1988603
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1988604
    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1988605
    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    .line 1988606
    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->a()Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1988607
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;

    .line 1988608
    iput-object v0, v1, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->e:Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    .line 1988609
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1988610
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1988578
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->e:Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->e:Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    .line 1988579
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->e:Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel$TargetModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1988580
    new-instance v0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;

    invoke-direct {v0}, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;-><init>()V

    .line 1988581
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1988582
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1988583
    const v0, -0x1e876e2b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1988584
    const v0, -0x4b900828

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1988585
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->f:Ljava/lang/String;

    .line 1988586
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/FileUploadingMutationsModels$CreateFileMutationModel$StoryModel$AttachmentsModel;->f:Ljava/lang/String;

    return-object v0
.end method
