.class public final Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xaa92313
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1989407
    const-class v0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1989408
    const-class v0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1989415
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1989416
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1989409
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1989410
    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1989411
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1989412
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1989413
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1989414
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1989397
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1989398
    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1989399
    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    .line 1989400
    invoke-virtual {p0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1989401
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;

    .line 1989402
    iput-object v0, v1, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->e:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    .line 1989403
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1989404
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1989396
    new-instance v0, LX/DLw;

    invoke-direct {v0, p1}, LX/DLw;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupDocsAndFiles"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1989405
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->e:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    iput-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->e:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    .line 1989406
    iget-object v0, p0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;->e:Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel$GroupDocsAndFilesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1989394
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1989395
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1989393
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1989390
    new-instance v0, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;

    invoke-direct {v0}, Lcom/facebook/groups/docsandfiles/protocol/GroupDocsAndFilesModels$GroupDocsAndFilesQueryModel;-><init>()V

    .line 1989391
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1989392
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1989389
    const v0, -0x12676fbf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1989388
    const v0, 0x41e065f

    return v0
.end method
