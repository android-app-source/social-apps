.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x355575bd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2004010
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2004028
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2004029
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2004030
    return-void
.end method

.method private a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupMentions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2004031
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    .line 2004032
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2004033
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2004034
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2004035
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2004036
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2004037
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2004038
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2004019
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2004020
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2004021
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    .line 2004022
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2004023
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;

    .line 2004024
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    .line 2004025
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2004026
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2004027
    new-instance v0, LX/DUl;

    invoke-direct {v0, p1}, LX/DUl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2004017
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2004018
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2004016
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2004013
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel;-><init>()V

    .line 2004014
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2004015
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2004012
    const v0, -0x452c88e1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2004011
    const v0, 0x41e065f

    return v0
.end method
