.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x57fed3de
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001230
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001231
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2001238
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2001239
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2001232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001233
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2001234
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2001235
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2001236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001237
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2001221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001222
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2001223
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    .line 2001224
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2001225
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;

    .line 2001226
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    .line 2001227
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001228
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2001229
    new-instance v0, LX/DTy;

    invoke-direct {v0, p1}, LX/DTy;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupAdminProfiles"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001211
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    .line 2001212
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2001219
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2001220
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2001213
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2001214
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel;-><init>()V

    .line 2001215
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2001216
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2001217
    const v0, 0x1978bdd8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2001218
    const v0, 0x41e065f

    return v0
.end method
