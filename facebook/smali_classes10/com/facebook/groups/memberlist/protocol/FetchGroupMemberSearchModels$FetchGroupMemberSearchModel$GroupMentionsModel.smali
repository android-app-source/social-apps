.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1e3f5a85
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2003970
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2003971
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2003972
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2003973
    return-void
.end method

.method private a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMemberSection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2003974
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    .line 2003975
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2003976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2003977
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2003978
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2003979
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2003980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2003981
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2003982
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2003983
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2003984
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    .line 2003985
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2003986
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    .line 2003987
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel;

    .line 2003988
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2003989
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2003990
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel;-><init>()V

    .line 2003991
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2003992
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2003993
    const v0, -0x241a053f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2003994
    const v0, -0x1eab8e89

    return v0
.end method
