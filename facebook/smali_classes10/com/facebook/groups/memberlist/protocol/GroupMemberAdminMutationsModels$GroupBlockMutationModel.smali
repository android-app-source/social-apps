.class public final Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2004658
    const-class v0, Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2004653
    const-class v0, Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2004656
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2004657
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2004654
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel;->e:Ljava/lang/String;

    .line 2004655
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2004659
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2004660
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2004661
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2004662
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2004663
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2004664
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2004648
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2004649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2004650
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2004645
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/GroupMemberAdminMutationsModels$GroupBlockMutationModel;-><init>()V

    .line 2004646
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2004647
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2004651
    const v0, -0x5ec19245

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2004652
    const v0, -0xadbb941

    return v0
.end method
