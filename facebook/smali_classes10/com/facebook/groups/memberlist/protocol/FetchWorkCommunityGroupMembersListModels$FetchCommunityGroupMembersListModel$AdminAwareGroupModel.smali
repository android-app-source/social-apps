.class public final Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3458cc18
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2004249
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2004206
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2004247
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2004248
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 2004240
    iput-object p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2004241
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2004242
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2004243
    if-eqz v0, :cond_0

    .line 2004244
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2004245
    :cond_0
    return-void

    .line 2004246
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2004232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2004233
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->a()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2004234
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2004235
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2004236
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2004237
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2004238
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2004239
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2004224
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2004225
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2004226
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2004227
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2004228
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    .line 2004229
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2004230
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2004231
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2004223
    new-instance v0, LX/DUt;

    invoke-direct {v0, p1}, LX/DUt;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2004250
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2004251
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2004217
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2004218
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->a()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2004219
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2004220
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 2004221
    :goto_0
    return-void

    .line 2004222
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2004214
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2004215
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    .line 2004216
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2004211
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;-><init>()V

    .line 2004212
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2004213
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2004210
    const v0, -0x353ff2c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2004209
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getWorkMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2004207
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2004208
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    return-object v0
.end method
