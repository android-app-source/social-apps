.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2002359
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    new-instance v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2002360
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2002361
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2002362
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2002363
    const/4 v2, 0x0

    .line 2002364
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_9

    .line 2002365
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2002366
    :goto_0
    move v1, v2

    .line 2002367
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2002368
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2002369
    new-instance v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    invoke-direct {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;-><init>()V

    .line 2002370
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2002371
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2002372
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2002373
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2002374
    :cond_0
    return-object v1

    .line 2002375
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2002376
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_8

    .line 2002377
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 2002378
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2002379
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 2002380
    const-string p0, "id"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2002381
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2002382
    :cond_3
    const-string p0, "name"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2002383
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 2002384
    :cond_4
    const-string p0, "url"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2002385
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 2002386
    :cond_5
    const-string p0, "viewer_admin_type"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 2002387
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2002388
    :cond_6
    const-string p0, "viewer_join_state"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 2002389
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 2002390
    :cond_7
    const-string p0, "visibility"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2002391
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto/16 :goto_1

    .line 2002392
    :cond_8
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2002393
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2002394
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2002395
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 2002396
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2002397
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2002398
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2002399
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto/16 :goto_1
.end method
