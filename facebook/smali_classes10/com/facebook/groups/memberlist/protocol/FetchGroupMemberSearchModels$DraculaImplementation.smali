.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2003788
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2003789
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2003813
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2003814
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 2003794
    if-nez p1, :cond_0

    .line 2003795
    :goto_0
    return v0

    .line 2003796
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2003797
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2003798
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2003799
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2003800
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2003801
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2003802
    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v3

    .line 2003803
    invoke-virtual {p0, p1, v9}, LX/15i;->b(II)Z

    move-result v4

    .line 2003804
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2003805
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2003806
    const/4 v6, 0x5

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2003807
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2003808
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 2003809
    invoke-virtual {p3, v8, v3}, LX/186;->a(IZ)V

    .line 2003810
    invoke-virtual {p3, v9, v4}, LX/186;->a(IZ)V

    .line 2003811
    invoke-virtual {p3, v10, v5}, LX/186;->b(II)V

    .line 2003812
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x12689677
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2003793
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2003790
    packed-switch p0, :pswitch_data_0

    .line 2003791
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2003792
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x12689677
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2003787
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2003785
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;->b(I)V

    .line 2003786
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2003815
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2003816
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2003817
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;->a:LX/15i;

    .line 2003818
    iput p2, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;->b:I

    .line 2003819
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2003784
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2003783
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2003780
    iget v0, p0, LX/1vt;->c:I

    .line 2003781
    move v0, v0

    .line 2003782
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2003759
    iget v0, p0, LX/1vt;->c:I

    .line 2003760
    move v0, v0

    .line 2003761
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2003777
    iget v0, p0, LX/1vt;->b:I

    .line 2003778
    move v0, v0

    .line 2003779
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2003774
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2003775
    move-object v0, v0

    .line 2003776
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2003765
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2003766
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2003767
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2003768
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2003769
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2003770
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2003771
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2003772
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2003773
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2003762
    iget v0, p0, LX/1vt;->c:I

    .line 2003763
    move v0, v0

    .line 2003764
    return v0
.end method
