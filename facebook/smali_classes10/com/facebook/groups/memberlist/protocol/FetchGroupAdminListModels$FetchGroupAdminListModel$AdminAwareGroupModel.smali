.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46e60cbd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001725
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001724
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2001722
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2001723
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2001716
    iput-boolean p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->e:Z

    .line 2001717
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2001718
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2001719
    if-eqz v0, :cond_0

    .line 2001720
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 2001721
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2001707
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001708
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2001709
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2001710
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2001711
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2001712
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2001713
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2001714
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001715
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2001670
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001671
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2001672
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    .line 2001673
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2001674
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    .line 2001675
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    .line 2001676
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2001677
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    .line 2001678
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2001679
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    .line 2001680
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    .line 2001681
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001682
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2001706
    new-instance v0, LX/DU8;

    invoke-direct {v0, p1}, LX/DU8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2001703
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2001704
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->e:Z

    .line 2001705
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2001697
    const-string v0, "can_viewer_claim_adminship"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2001698
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2001699
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2001700
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 2001701
    :goto_0
    return-void

    .line 2001702
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2001694
    const-string v0, "can_viewer_claim_adminship"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2001695
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->a(Z)V

    .line 2001696
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2001692
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2001693
    iget-boolean v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2001689
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;-><init>()V

    .line 2001690
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2001691
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2001688
    const v0, -0x15c43567

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2001687
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupAdminProfiles"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001685
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    .line 2001686
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getGroupModerators"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001683
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    .line 2001684
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$FetchGroupAdminListModel$AdminAwareGroupModel;->g:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel;

    return-object v0
.end method
