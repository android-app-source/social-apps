.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2002402
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    new-instance v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2002403
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2002405
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2002406
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2002407
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x5

    const/4 p0, 0x4

    const/4 v4, 0x3

    .line 2002408
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2002409
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2002410
    if-eqz v2, :cond_0

    .line 2002411
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2002412
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2002413
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2002414
    if-eqz v2, :cond_1

    .line 2002415
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2002416
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2002417
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2002418
    if-eqz v2, :cond_2

    .line 2002419
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2002420
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2002421
    :cond_2
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 2002422
    if-eqz v2, :cond_3

    .line 2002423
    const-string v2, "viewer_admin_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2002424
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2002425
    :cond_3
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 2002426
    if-eqz v2, :cond_4

    .line 2002427
    const-string v2, "viewer_join_state"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2002428
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2002429
    :cond_4
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 2002430
    if-eqz v2, :cond_5

    .line 2002431
    const-string v2, "visibility"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2002432
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2002433
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2002434
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2002404
    check-cast p1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Serializer;->a(Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;LX/0nX;LX/0my;)V

    return-void
.end method
