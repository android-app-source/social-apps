.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x26a4b959
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2003914
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2003893
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2003915
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2003916
    return-void
.end method

.method private a()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2003906
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;->e:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;->e:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    .line 2003907
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;->e:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2003908
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2003909
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;->a()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2003910
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2003911
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2003912
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2003913
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2003898
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2003899
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;->a()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2003900
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;->a()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    .line 2003901
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;->a()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2003902
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;

    .line 2003903
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;->e:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    .line 2003904
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2003905
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2003895
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberSearchModels$FetchGroupMemberSearchModel$GroupMentionsModel$MemberSectionModel$EdgesModel;-><init>()V

    .line 2003896
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2003897
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2003894
    const v0, -0x3062f404

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2003892
    const v0, -0x392ad321

    return v0
.end method
