.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7befca10
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2002916
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2002915
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2002892
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2002893
    return-void
.end method

.method private a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002913
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    .line 2002914
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2002907
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2002908
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2002909
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2002910
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2002911
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2002912
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2002899
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2002900
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2002901
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    .line 2002902
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2002903
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;

    .line 2002904
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel$InviteeModel;

    .line 2002905
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2002906
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2002896
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$InviteeDataModel;-><init>()V

    .line 2002897
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2002898
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2002895
    const v0, -0x33931a7c    # -6.2101008E7f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2002894
    const v0, 0x58017a73

    return v0
.end method
