.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2002965
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2002966
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2002963
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2002964
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2002955
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2002956
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2002957
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2002958
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2002959
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2002960
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2002961
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2002962
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2002952
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2002953
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2002954
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2002951
    new-instance v0, LX/DUZ;

    invoke-direct {v0, p1}, LX/DUZ;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002967
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2002949
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2002950
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2002948
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2002945
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;-><init>()V

    .line 2002946
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2002947
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2002944
    const v0, -0x1bf5e424

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2002943
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002939
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->e:Ljava/lang/String;

    .line 2002940
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002941
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->f:Ljava/lang/String;

    .line 2002942
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$AddedByModel;->f:Ljava/lang/String;

    return-object v0
.end method
