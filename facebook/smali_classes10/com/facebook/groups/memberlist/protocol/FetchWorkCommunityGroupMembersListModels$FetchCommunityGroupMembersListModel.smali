.class public final Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x57212f6a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2004319
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2004293
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2004317
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2004318
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2004311
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2004312
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2004313
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2004314
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2004315
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2004316
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2004303
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2004304
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2004305
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    .line 2004306
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2004307
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;

    .line 2004308
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    .line 2004309
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2004310
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2004302
    new-instance v0, LX/DUu;

    invoke-direct {v0, p1}, LX/DUu;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdminAwareGroup"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2004320
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    .line 2004321
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel$AdminAwareGroupModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2004300
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2004301
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2004299
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2004296
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchWorkCommunityGroupMembersListModels$FetchCommunityGroupMembersListModel;-><init>()V

    .line 2004297
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2004298
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2004295
    const v0, 0x249871

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2004294
    const v0, 0x41e065f

    return v0
.end method
