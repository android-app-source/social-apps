.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2002575
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2002576
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2002596
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2002597
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 2002577
    if-nez p1, :cond_0

    .line 2002578
    :goto_0
    return v0

    .line 2002579
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2002580
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2002581
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2002582
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2002583
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2002584
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2002585
    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v3

    .line 2002586
    invoke-virtual {p0, p1, v9}, LX/15i;->b(II)Z

    move-result v4

    .line 2002587
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2002588
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2002589
    const/4 v6, 0x5

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2002590
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2002591
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 2002592
    invoke-virtual {p3, v8, v3}, LX/186;->a(IZ)V

    .line 2002593
    invoke-virtual {p3, v9, v4}, LX/186;->a(IZ)V

    .line 2002594
    invoke-virtual {p3, v10, v5}, LX/186;->b(II)V

    .line 2002595
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x74ec7da3
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2002574
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2002571
    packed-switch p0, :pswitch_data_0

    .line 2002572
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2002573
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x74ec7da3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2002570
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2002568
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;->b(I)V

    .line 2002569
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2002598
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2002599
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2002600
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;->a:LX/15i;

    .line 2002601
    iput p2, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;->b:I

    .line 2002602
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2002567
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2002566
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2002563
    iget v0, p0, LX/1vt;->c:I

    .line 2002564
    move v0, v0

    .line 2002565
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2002560
    iget v0, p0, LX/1vt;->c:I

    .line 2002561
    move v0, v0

    .line 2002562
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2002557
    iget v0, p0, LX/1vt;->b:I

    .line 2002558
    move v0, v0

    .line 2002559
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002542
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2002543
    move-object v0, v0

    .line 2002544
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2002548
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2002549
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2002550
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2002551
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2002552
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2002553
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2002554
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2002555
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2002556
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2002545
    iget v0, p0, LX/1vt;->c:I

    .line 2002546
    move v0, v0

    .line 2002547
    return v0
.end method
