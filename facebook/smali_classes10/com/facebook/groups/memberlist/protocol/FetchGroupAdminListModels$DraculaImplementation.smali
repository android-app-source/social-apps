.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2001646
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2001647
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2001644
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2001645
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 2001625
    if-nez p1, :cond_0

    .line 2001626
    :goto_0
    return v0

    .line 2001627
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 2001628
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2001629
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2001630
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2001631
    invoke-virtual {p0, p1, v7}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2001632
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2001633
    invoke-virtual {p0, p1, v8}, LX/15i;->b(II)Z

    move-result v3

    .line 2001634
    invoke-virtual {p0, p1, v9}, LX/15i;->b(II)Z

    move-result v4

    .line 2001635
    invoke-virtual {p0, p1, v10}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2001636
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2001637
    const/4 v6, 0x5

    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 2001638
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2001639
    invoke-virtual {p3, v7, v2}, LX/186;->b(II)V

    .line 2001640
    invoke-virtual {p3, v8, v3}, LX/186;->a(IZ)V

    .line 2001641
    invoke-virtual {p3, v9, v4}, LX/186;->a(IZ)V

    .line 2001642
    invoke-virtual {p3, v10, v5}, LX/186;->b(II)V

    .line 2001643
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4187197
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2001624
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 2001621
    packed-switch p0, :pswitch_data_0

    .line 2001622
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2001623
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x4187197
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2001587
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 2001619
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;->b(I)V

    .line 2001620
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2001614
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2001615
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2001616
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;->a:LX/15i;

    .line 2001617
    iput p2, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;->b:I

    .line 2001618
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2001613
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2001612
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2001609
    iget v0, p0, LX/1vt;->c:I

    .line 2001610
    move v0, v0

    .line 2001611
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2001606
    iget v0, p0, LX/1vt;->c:I

    .line 2001607
    move v0, v0

    .line 2001608
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2001603
    iget v0, p0, LX/1vt;->b:I

    .line 2001604
    move v0, v0

    .line 2001605
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001600
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2001601
    move-object v0, v0

    .line 2001602
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2001591
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2001592
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2001593
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2001594
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2001595
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2001596
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2001597
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2001598
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2001599
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2001588
    iget v0, p0, LX/1vt;->c:I

    .line 2001589
    move v0, v0

    .line 2001590
    return v0
.end method
