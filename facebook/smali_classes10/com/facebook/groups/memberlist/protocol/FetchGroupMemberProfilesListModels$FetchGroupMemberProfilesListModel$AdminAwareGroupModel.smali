.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x27f90a29
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2002721
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2002720
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2002718
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2002719
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 2002712
    iput-wide p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->e:J

    .line 2002713
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2002714
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2002715
    if-eqz v0, :cond_0

    .line 2002716
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 2002717
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 2002705
    iput-object p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2002706
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2002707
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2002708
    if-eqz v0, :cond_0

    .line 2002709
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x8

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2002710
    :cond_0
    return-void

    .line 2002711
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2002699
    iput-object p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j:Ljava/lang/String;

    .line 2002700
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2002701
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2002702
    if-eqz v0, :cond_0

    .line 2002703
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2002704
    :cond_0
    return-void
.end method

.method private r()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2002697
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2002698
    iget-wide v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->e:J

    return-wide v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 2002676
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2002677
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2002678
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2002679
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2002680
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2002681
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2002682
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2002683
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v12

    .line 2002684
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->q()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 2002685
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2002686
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2002687
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2002688
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2002689
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 2002690
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 2002691
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 2002692
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 2002693
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 2002694
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 2002695
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2002696
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2002658
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2002659
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2002660
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2002661
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2002662
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    .line 2002663
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2002664
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2002665
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2002666
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2002667
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    .line 2002668
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->i:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2002669
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2002670
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2002671
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2002672
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    .line 2002673
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2002674
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2002675
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2002657
    new-instance v0, LX/DUW;

    invoke-direct {v0, p1}, LX/DUW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002656
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2002625
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2002626
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->e:J

    .line 2002627
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 2002722
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2002723
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->r()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2002724
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2002725
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 2002726
    :goto_0
    return-void

    .line 2002727
    :cond_0
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2002728
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2002729
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2002730
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2002731
    :cond_1
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2002732
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->q()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2002733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2002734
    const/16 v0, 0x8

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2002735
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 2002649
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2002650
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->a(J)V

    .line 2002651
    :cond_0
    :goto_0
    return-void

    .line 2002652
    :cond_1
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2002653
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2002654
    :cond_2
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2002655
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2002646
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;-><init>()V

    .line 2002647
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2002648
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2002645
    const v0, -0x2f9a9e60

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2002644
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriendMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002642
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2002643
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002640
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->g:Ljava/lang/String;

    .line 2002641
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002638
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->h:Ljava/lang/String;

    .line 2002639
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getInvitedMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002636
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->i:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->i:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2002637
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->i:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002634
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j:Ljava/lang/String;

    .line 2002635
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOtherMembers"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002632
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    .line 2002633
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesConnectionDefaultFieldsModel;

    return-object v0
.end method

.method public final p()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002630
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->l:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->l:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2002631
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->l:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002628
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2002629
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$FetchGroupMemberProfilesListModel$AdminAwareGroupModel;->m:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method
