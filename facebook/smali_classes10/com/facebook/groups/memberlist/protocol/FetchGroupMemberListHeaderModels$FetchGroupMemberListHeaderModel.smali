.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7ade352
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2002516
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2002515
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2002513
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2002514
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 2002506
    iput-object p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2002507
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2002508
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2002509
    if-eqz v0, :cond_0

    .line 2002510
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2002511
    :cond_0
    return-void

    .line 2002512
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 2002499
    iput-object p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2002500
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2002501
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2002502
    if-eqz v0, :cond_0

    .line 2002503
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x5

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 2002504
    :cond_0
    return-void

    .line 2002505
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2002493
    iput-object p1, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->f:Ljava/lang/String;

    .line 2002494
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 2002495
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 2002496
    if-eqz v0, :cond_0

    .line 2002497
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 2002498
    :cond_0
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002491
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->e:Ljava/lang/String;

    .line 2002492
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002489
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->f:Ljava/lang/String;

    .line 2002490
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2002473
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2002474
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2002475
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2002476
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2002477
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2002478
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 2002479
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->m()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2002480
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2002481
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 2002482
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2002483
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2002484
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2002485
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2002486
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2002487
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2002488
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2002470
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2002471
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2002472
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2002517
    new-instance v0, LX/DUP;

    invoke-direct {v0, p1}, LX/DUP;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002469
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->n()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 2002455
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2002456
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2002457
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2002458
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 2002459
    :goto_0
    return-void

    .line 2002460
    :cond_0
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2002461
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2002462
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2002463
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2002464
    :cond_1
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2002465
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->m()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 2002466
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 2002467
    const/4 v0, 0x5

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 2002468
    :cond_2
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 2002448
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2002449
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->a(Ljava/lang/String;)V

    .line 2002450
    :cond_0
    :goto_0
    return-void

    .line 2002451
    :cond_1
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2002452
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_0

    .line 2002453
    :cond_2
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2002454
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2002445
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;-><init>()V

    .line 2002446
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2002447
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2002444
    const v0, 0x3d3a5cff

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2002443
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002441
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->g:Ljava/lang/String;

    .line 2002442
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002439
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2002440
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->h:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002437
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2002438
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->i:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2002435
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2002436
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;->j:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method
