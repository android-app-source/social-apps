.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x395a0eb7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001117
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001145
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2001143
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2001144
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2001135
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001136
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->a()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 2001137
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2001138
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2001139
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2001140
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2001141
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001142
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2001127
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001128
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2001129
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    .line 2001130
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2001131
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;

    .line 2001132
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    .line 2001133
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001134
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001125
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2001126
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2001122
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;-><init>()V

    .line 2001123
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2001124
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2001121
    const v0, 0x65ae55db

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2001120
    const v0, -0x585ebae9

    return v0
.end method

.method public final j()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001118
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    .line 2001119
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel;->f:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminIdsModels$FetchGroupAdminIdsModel$GroupAdminProfilesModel$EdgesModel$NodeModel;

    return-object v0
.end method
