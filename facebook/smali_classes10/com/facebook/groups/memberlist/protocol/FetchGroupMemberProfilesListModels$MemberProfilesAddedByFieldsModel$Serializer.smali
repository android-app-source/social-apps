.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2002978
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel;

    new-instance v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2002979
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2002980
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 2002981
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2002982
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const-wide/16 v4, 0x0

    .line 2002983
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2002984
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2002985
    if-eqz v2, :cond_0

    .line 2002986
    const-string v3, "added_by"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2002987
    invoke-static {v1, v2, p1}, LX/DUf;->a(LX/15i;ILX/0nX;)V

    .line 2002988
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 2002989
    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 2002990
    const-string v4, "added_time"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2002991
    invoke-virtual {p1, v2, v3}, LX/0nX;->a(J)V

    .line 2002992
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2002993
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2002994
    check-cast p1, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel$Serializer;->a(Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberProfilesListModels$MemberProfilesAddedByFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
