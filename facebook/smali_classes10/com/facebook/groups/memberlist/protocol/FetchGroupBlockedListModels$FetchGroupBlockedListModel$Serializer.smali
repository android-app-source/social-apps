.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2002208
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;

    new-instance v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2002209
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2002197
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2002199
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2002200
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2002201
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2002202
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2002203
    if-eqz v2, :cond_0

    .line 2002204
    const-string p0, "group_blocked_profiles"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2002205
    invoke-static {v1, v2, p1, p2}, LX/DUL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2002206
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2002207
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2002198
    check-cast p1, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel$Serializer;->a(Lcom/facebook/groups/memberlist/protocol/FetchGroupBlockedListModels$FetchGroupBlockedListModel;LX/0nX;LX/0my;)V

    return-void
.end method
