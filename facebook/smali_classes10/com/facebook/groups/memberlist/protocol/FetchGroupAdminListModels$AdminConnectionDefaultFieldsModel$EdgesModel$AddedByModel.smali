.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x46ac8a11
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001476
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001475
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2001473
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2001474
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2001465
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001466
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2001467
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2001468
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2001469
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2001470
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2001471
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001472
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2001462
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001463
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001464
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2001461
    new-instance v0, LX/DU7;

    invoke-direct {v0, p1}, LX/DU7;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001448
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2001459
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2001460
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2001458
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2001455
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;-><init>()V

    .line 2001456
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2001457
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2001454
    const v0, 0x64bc1f6d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2001453
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001451
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->e:Ljava/lang/String;

    .line 2001452
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001449
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->f:Ljava/lang/String;

    .line 2001450
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;->f:Ljava/lang/String;

    return-object v0
.end method
