.class public final Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x137c4f98
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001498
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2001497
    const-class v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2001536
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2001537
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 2001525
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001526
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2001527
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 2001528
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->l()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2001529
    const/4 v1, 0x4

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2001530
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2001531
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2001532
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2001533
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2001534
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001535
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2001512
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2001513
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2001514
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    .line 2001515
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2001516
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;

    .line 2001517
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    .line 2001518
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->l()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2001519
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->l()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    .line 2001520
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->l()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2001521
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;

    .line 2001522
    iput-object v0, v1, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->h:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    .line 2001523
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2001524
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001510
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    .line 2001511
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->e:Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel$AddedByModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2001507
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2001508
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->f:J

    .line 2001509
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2001538
    new-instance v0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;-><init>()V

    .line 2001539
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2001540
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2001506
    const v0, 0x68a49436

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2001505
    const v0, -0x585ebae9

    return v0
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 2001503
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2001504
    iget-wide v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->f:J

    return-wide v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001501
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2001502
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    return-object v0
.end method

.method public final l()Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2001499
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->h:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->h:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    .line 2001500
    iget-object v0, p0, Lcom/facebook/groups/memberlist/protocol/FetchGroupAdminListModels$AdminConnectionDefaultFieldsModel$EdgesModel;->h:Lcom/facebook/groups/widget/memberrow/protocol/MemberDataModels$GroupMemberDataModel;

    return-object v0
.end method
