.class public Lcom/facebook/groups/memberlist/GroupBlockedListFragment;
.super Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;
.source ""


# instance fields
.field public a:LX/DRz;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DTa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/DRv;

.field private d:LX/DRr;

.field public e:LX/DTZ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1999043
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;-><init>()V

    return-void
.end method

.method public static E(Lcom/facebook/groups/memberlist/GroupBlockedListFragment;)V
    .locals 0

    .prologue
    .line 1999029
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->p()V

    .line 1999030
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->C()V

    .line 1999031
    return-void
.end method

.method private e(Z)V
    .locals 2

    .prologue
    .line 1999032
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 1999033
    :goto_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1999034
    if-eqz v1, :cond_0

    .line 1999035
    const v1, 0x7f0d04ed

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1999036
    :cond_0
    return-void

    .line 1999037
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/B1d;
    .locals 11

    .prologue
    .line 1998972
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->a:LX/DRz;

    .line 1998973
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1998974
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->D()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1998975
    iget-object v3, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->y:LX/B1b;

    move-object v3, v3

    .line 1998976
    new-instance v4, LX/DRy;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    move-object v6, v1

    move-object v7, p1

    move-object v8, v2

    move-object v10, v3

    invoke-direct/range {v4 .. v10}, LX/DRy;-><init>(LX/1Ck;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/0tX;LX/B1b;)V

    .line 1998977
    move-object v0, v4

    .line 1998978
    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1999038
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(LX/0Px;)V

    .line 1999039
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1999040
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    invoke-virtual {v0, p1}, LX/DSB;->b(LX/0Px;)V

    .line 1999041
    :goto_0
    return-void

    .line 1999042
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    invoke-virtual {v0, p1}, LX/DSB;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1999024
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Landroid/os/Bundle;)V

    .line 1999025
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;

    const-class v2, LX/DRz;

    invoke-interface {p1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/DRz;

    const-class v0, LX/DTa;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/DTa;

    iput-object v2, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->a:LX/DRz;

    iput-object p1, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->b:LX/DTa;

    .line 1999026
    return-void
.end method

.method public final a(Landroid/view/View;LX/DSf;)V
    .locals 3

    .prologue
    .line 1999027
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e:LX/DTZ;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->q:Z

    invoke-virtual {v0, p1, p2, v1, v2}, LX/DTZ;->onClick(Landroid/view/View;LX/DSf;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Z)V

    .line 1999028
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1999014
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1999015
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e(Z)V

    .line 1999016
    :goto_0
    return-void

    .line 1999017
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Z)V

    .line 1999018
    if-eqz p1, :cond_2

    const/16 v0, 0x8

    .line 1999019
    :goto_1
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1999020
    if-eqz v1, :cond_1

    .line 1999021
    const v1, 0x7f0d1b69

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1999022
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e(Z)V

    goto :goto_0

    .line 1999023
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final k()Landroid/view/View;
    .locals 4

    .prologue
    .line 1999008
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1999009
    const v2, 0x7f03085f

    move v2, v2

    .line 1999010
    const v0, 0x7f0d1b68

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1999011
    const v0, 0x7f0d15d3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1999012
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->h:Landroid/content/res/Resources;

    const v2, 0x7f082fce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1999013
    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1999007
    const/4 v0, 0x0

    return v0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 1998995
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->b:LX/DTa;

    .line 1998996
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1998997
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object v2, v2

    .line 1998998
    invoke-virtual {v0, v1, v2}, LX/DTa;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)LX/DTZ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e:LX/DTZ;

    .line 1998999
    new-instance v0, LX/DRw;

    invoke-direct {v0, p0}, LX/DRw;-><init>(Lcom/facebook/groups/memberlist/GroupBlockedListFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->c:LX/DRv;

    .line 1999000
    new-instance v0, LX/DRx;

    invoke-direct {v0, p0}, LX/DRx;-><init>(Lcom/facebook/groups/memberlist/GroupBlockedListFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->d:LX/DRr;

    .line 1999001
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e:LX/DTZ;

    .line 1999002
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->g:LX/DSE;

    move-object v1, v1

    .line 1999003
    iput-object v1, v0, LX/DTZ;->k:LX/DSE;

    .line 1999004
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->c:LX/DRv;

    invoke-virtual {v0, v1}, LX/DTZ;->a(LX/DRo;)V

    .line 1999005
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->d:LX/DRr;

    invoke-virtual {v0, v1}, LX/DTZ;->a(LX/DRo;)V

    .line 1999006
    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x503e7f81

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1998989
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e:LX/DTZ;

    .line 1998990
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->g:LX/DSE;

    move-object v2, v2

    .line 1998991
    invoke-virtual {v1, v2}, LX/DTZ;->b(LX/DSE;)V

    .line 1998992
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->e:LX/DTZ;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupBlockedListFragment;->c:LX/DRv;

    invoke-virtual {v1, v2}, LX/DTZ;->b(LX/DRo;)V

    .line 1998993
    invoke-super {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->onDestroyView()V

    .line 1998994
    const/16 v1, 0x2b

    const v2, 0x784baed0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 1998979
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->setUserVisibleHint(Z)V

    .line 1998980
    iget-boolean v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->p:Z

    if-eqz v0, :cond_1

    .line 1998981
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    move-object v0, v0

    .line 1998982
    if-eqz v0, :cond_1

    .line 1998983
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    move-object v0, v0

    .line 1998984
    invoke-virtual {v0}, LX/B1d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1998985
    :goto_0
    if-eqz v0, :cond_0

    .line 1998986
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o()V

    .line 1998987
    :cond_0
    return-void

    .line 1998988
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
