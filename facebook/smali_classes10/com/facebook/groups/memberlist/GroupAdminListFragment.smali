.class public Lcom/facebook/groups/memberlist/GroupAdminListFragment;
.super Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DRu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DT7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DTp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DTa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DSq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DS7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/DRt;

.field private t:LX/DRp;

.field private u:LX/DRr;

.field public v:LX/DTZ;

.field private w:Lcom/facebook/widget/text/BetterTextView;

.field private x:Lcom/facebook/widget/text/BetterTextView;

.field public y:LX/DSp;

.field public z:LX/DSY;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1998824
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;-><init>()V

    return-void
.end method

.method private E()V
    .locals 9

    .prologue
    .line 1998817
    new-instance v0, LX/DSY;

    invoke-direct {v0}, LX/DSY;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->z:LX/DSY;

    .line 1998818
    new-instance v2, LX/DSZ;

    invoke-direct {v2}, LX/DSZ;-><init>()V

    .line 1998819
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->g:LX/DS7;

    new-instance v1, LX/DRh;

    invoke-direct {v1, p0}, LX/DRh;-><init>(Lcom/facebook/groups/memberlist/GroupAdminListFragment;)V

    new-instance v3, LX/DRj;

    invoke-direct {v3, p0}, LX/DRj;-><init>(Lcom/facebook/groups/memberlist/GroupAdminListFragment;)V

    new-instance v4, LX/DRl;

    invoke-direct {v4, p0}, LX/DRl;-><init>(Lcom/facebook/groups/memberlist/GroupAdminListFragment;)V

    .line 1998820
    iget-object v5, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1998821
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->r()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, LX/DS7;->a(LX/DRg;LX/DSZ;LX/DRi;LX/DRk;Ljava/lang/String;Z)LX/DS6;

    move-result-object v6

    .line 1998822
    iget-object v3, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->f:LX/DSq;

    iget-object v4, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->z:LX/DSY;

    iget-object v5, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->z:LX/DSY;

    new-instance v8, LX/DS3;

    invoke-direct {v8}, LX/DS3;-><init>()V

    move-object v7, v2

    invoke-virtual/range {v3 .. v8}, LX/DSq;->a(LX/BWf;LX/DSX;LX/DS5;LX/DSZ;LX/BWd;)LX/DSp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->y:LX/DSp;

    .line 1998823
    return-void
.end method

.method private e(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1998808
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->w:Lcom/facebook/widget/text/BetterTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    if-nez v0, :cond_1

    .line 1998809
    :cond_0
    :goto_0
    return-void

    .line 1998810
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->s:LX/DRt;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->s:LX/DRt;

    .line 1998811
    iget-boolean p1, v0, LX/DRt;->i:Z

    move v0, p1

    .line 1998812
    if-eqz v0, :cond_2

    .line 1998813
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->w:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1998814
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0

    .line 1998815
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->w:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1998816
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private f(Z)V
    .locals 2

    .prologue
    .line 1998802
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 1998803
    :goto_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1998804
    if-eqz v1, :cond_0

    .line 1998805
    const v1, 0x7f0d04ed

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1998806
    :cond_0
    return-void

    .line 1998807
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/B1d;
    .locals 11

    .prologue
    .line 1998794
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->b:LX/DRu;

    .line 1998795
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1998796
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->D()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 1998797
    iget-object v3, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->y:LX/B1b;

    move-object v3, v3

    .line 1998798
    new-instance v4, LX/DRt;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    move-object v6, v1

    move-object v7, p1

    move-object v8, v2

    move-object v10, v3

    invoke-direct/range {v4 .. v10}, LX/DRt;-><init>(LX/1Ck;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/0tX;LX/B1b;)V

    .line 1998799
    move-object v0, v4

    .line 1998800
    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->s:LX/DRt;

    .line 1998801
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->s:LX/DRt;

    return-object v0
.end method

.method public final a(I)LX/DSf;
    .locals 1

    .prologue
    .line 1998793
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->y:LX/DSp;

    invoke-virtual {v0, p1}, LX/BWh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DSf;

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1998787
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->z:LX/DSY;

    .line 1998788
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    move-object v1, v1

    .line 1998789
    invoke-virtual {v1}, LX/B1d;->d()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/DSY;->a(LX/0Px;LX/0P1;)V

    .line 1998790
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->y:LX/DSp;

    const v1, -0x6cd72788

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1998791
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(LX/0Px;)V

    .line 1998792
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1998784
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Landroid/os/Bundle;)V

    .line 1998785
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/memberlist/GroupAdminListFragment;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const-class v4, LX/DRu;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DRu;

    invoke-static {p1}, LX/DT7;->b(LX/0QB;)LX/DT7;

    move-result-object v5

    check-cast v5, LX/DT7;

    invoke-static {p1}, LX/DTp;->a(LX/0QB;)LX/DTp;

    move-result-object v6

    check-cast v6, LX/DTp;

    const-class v7, LX/DTa;

    invoke-interface {p1, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/DTa;

    const-class v8, LX/DSq;

    invoke-interface {p1, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/DSq;

    const-class v0, LX/DS7;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/DS7;

    iput-object v3, v2, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v4, v2, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->b:LX/DRu;

    iput-object v5, v2, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->c:LX/DT7;

    iput-object v6, v2, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->d:LX/DTp;

    iput-object v7, v2, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->e:LX/DTa;

    iput-object v8, v2, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->f:LX/DSq;

    iput-object p1, v2, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->g:LX/DS7;

    .line 1998786
    return-void
.end method

.method public final a(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1998779
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->z:LX/DSY;

    invoke-virtual {v0}, LX/DSY;->c()V

    .line 1998780
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->y:LX/DSp;

    const v1, 0x6b0cefb8

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1998781
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Landroid/text/Editable;)V

    .line 1998782
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o()V

    .line 1998783
    return-void
.end method

.method public final a(Landroid/view/View;LX/DSf;)V
    .locals 3

    .prologue
    .line 1998825
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->q:Z

    invoke-virtual {v0, p1, p2, v1, v2}, LX/DTZ;->onClick(Landroid/view/View;LX/DSf;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Z)V

    .line 1998826
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1998768
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1998769
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->f(Z)V

    .line 1998770
    :goto_0
    return-void

    .line 1998771
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Z)V

    .line 1998772
    invoke-direct {p0, p1}, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->e(Z)V

    .line 1998773
    if-eqz p1, :cond_2

    const/16 v0, 0x8

    .line 1998774
    :goto_1
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1998775
    if-eqz v1, :cond_1

    .line 1998776
    const v1, 0x7f0d1b69

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1998777
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->f(Z)V

    goto :goto_0

    .line 1998778
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1998767
    const/4 v0, 0x0

    return v0
.end method

.method public final c()LX/1Cv;
    .locals 1

    .prologue
    .line 1998764
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->y:LX/DSp;

    if-nez v0, :cond_0

    .line 1998765
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->E()V

    .line 1998766
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->y:LX/DSp;

    return-object v0
.end method

.method public final d()LX/DSA;
    .locals 1

    .prologue
    .line 1998761
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->y:LX/DSp;

    if-nez v0, :cond_0

    .line 1998762
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->E()V

    .line 1998763
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->y:LX/DSp;

    return-object v0
.end method

.method public final k()Landroid/view/View;
    .locals 4

    .prologue
    .line 1998749
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300b8

    const v0, 0x7f0d1b68

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1998750
    const v0, 0x7f0d04eb

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1998751
    const v1, 0x7f0d04ec

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 1998752
    const v3, 0x7f03085f

    move v2, v3

    .line 1998753
    invoke-virtual {v1, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1998754
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 1998755
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->h:Landroid/content/res/Resources;

    const v3, 0x7f082fc5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1998756
    const v1, 0x7f0d04ee

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->w:Lcom/facebook/widget/text/BetterTextView;

    .line 1998757
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->w:Lcom/facebook/widget/text/BetterTextView;

    new-instance v2, LX/DRm;

    invoke-direct {v2, p0}, LX/DRm;-><init>(Lcom/facebook/groups/memberlist/GroupAdminListFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1998758
    const v1, 0x7f0d04ef

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    .line 1998759
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->x:Lcom/facebook/widget/text/BetterTextView;

    new-instance v2, LX/DRn;

    invoke-direct {v2, p0}, LX/DRn;-><init>(Lcom/facebook/groups/memberlist/GroupAdminListFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1998760
    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1998748
    const/4 v0, 0x0

    return v0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 1998734
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->e:LX/DTa;

    .line 1998735
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1998736
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object v2, v2

    .line 1998737
    invoke-virtual {v0, v1, v2}, LX/DTa;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)LX/DTZ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    .line 1998738
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    invoke-virtual {v0}, LX/DTZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c(Z)V

    .line 1998739
    new-instance v0, LX/DRq;

    invoke-direct {v0, p0}, LX/DRq;-><init>(Lcom/facebook/groups/memberlist/GroupAdminListFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->t:LX/DRp;

    .line 1998740
    new-instance v0, LX/DRs;

    invoke-direct {v0, p0}, LX/DRs;-><init>(Lcom/facebook/groups/memberlist/GroupAdminListFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->u:LX/DRr;

    .line 1998741
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    .line 1998742
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->g:LX/DSE;

    move-object v1, v1

    .line 1998743
    iput-object v1, v0, LX/DTZ;->k:LX/DSE;

    .line 1998744
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->t:LX/DRp;

    invoke-virtual {v0, v1}, LX/DTZ;->a(LX/DRo;)V

    .line 1998745
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->u:LX/DRr;

    invoke-virtual {v0, v1}, LX/DTZ;->a(LX/DRo;)V

    .line 1998746
    return-void

    .line 1998747
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4f0f6653

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1998727
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    .line 1998728
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->g:LX/DSE;

    move-object v2, v2

    .line 1998729
    invoke-virtual {v1, v2}, LX/DTZ;->b(LX/DSE;)V

    .line 1998730
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->t:LX/DRp;

    invoke-virtual {v1, v2}, LX/DTZ;->b(LX/DRo;)V

    .line 1998731
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->v:LX/DTZ;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupAdminListFragment;->u:LX/DRr;

    invoke-virtual {v1, v2}, LX/DTZ;->b(LX/DRo;)V

    .line 1998732
    invoke-super {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->onDestroyView()V

    .line 1998733
    const/16 v1, 0x2b

    const v2, -0x15018b38

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 1998717
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->setUserVisibleHint(Z)V

    .line 1998718
    iget-boolean v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->p:Z

    if-eqz v0, :cond_1

    .line 1998719
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    move-object v0, v0

    .line 1998720
    if-eqz v0, :cond_1

    .line 1998721
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    move-object v0, v0

    .line 1998722
    invoke-virtual {v0}, LX/B1d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1998723
    :goto_0
    if-eqz v0, :cond_0

    .line 1998724
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o()V

    .line 1998725
    :cond_0
    return-void

    .line 1998726
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
