.class public Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;
.super Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;
.source ""


# instance fields
.field public a:LX/DSv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DT7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DZ7;
    .annotation runtime Lcom/facebook/groups/memberlist/annotation/GroupSuggestAdminNavHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2000231
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;

    const-class v1, LX/DSv;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/DSv;

    invoke-static {p0}, LX/DT7;->b(LX/0QB;)LX/DT7;

    move-result-object v2

    check-cast v2, LX/DT7;

    invoke-static {p0}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object v3

    check-cast v3, LX/DZ7;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object v1, p1, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->a:LX/DSv;

    iput-object v2, p1, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->b:LX/DT7;

    iput-object v3, p1, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->c:LX/DZ7;

    iput-object p0, p1, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->d:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/B1d;
    .locals 8

    .prologue
    .line 2000237
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->a:LX/DSv;

    .line 2000238
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2000239
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->e()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->D()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 2000240
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->y:LX/B1b;

    move-object v6, v2

    .line 2000241
    const/4 v7, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, LX/DSv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;LX/B1b;Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;)LX/DSu;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2000232
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(LX/0Px;)V

    .line 2000233
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2000234
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    invoke-virtual {v0, p1}, LX/DSB;->b(LX/0Px;)V

    .line 2000235
    :goto_0
    return-void

    .line 2000236
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    invoke-virtual {v0, p1}, LX/DSB;->a(LX/0Px;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2000205
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Landroid/os/Bundle;)V

    .line 2000206
    const-class v0, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2000207
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c(Z)V

    .line 2000208
    return-void
.end method

.method public final a(Landroid/view/View;LX/DSf;)V
    .locals 8

    .prologue
    .line 2000209
    if-eqz p2, :cond_0

    .line 2000210
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->b:LX/DT7;

    .line 2000211
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2000212
    iget-object v2, p2, LX/DSf;->d:LX/DUV;

    move-object v2, v2

    .line 2000213
    invoke-interface {v2}, LX/DUU;->c()Ljava/lang/String;

    move-result-object v2

    .line 2000214
    iget-object v3, p2, LX/DSf;->d:LX/DUV;

    move-object v3, v3

    .line 2000215
    invoke-interface {v3}, LX/DUU;->kA_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2000216
    new-instance v5, LX/0ju;

    invoke-direct {v5, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2000217
    iget-object v6, v0, LX/DT7;->c:Landroid/content/res/Resources;

    const v7, 0x7f082fc8

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v3, p1, p2

    invoke-virtual {v6, v7, p1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v5

    const v6, 0x7f080016

    new-instance v7, LX/DT5;

    invoke-direct {v7, v0}, LX/DT5;-><init>(LX/DT7;)V

    invoke-virtual {v5, v6, v7}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5}, LX/0ju;->a()LX/2EJ;

    move-result-object v5

    invoke-virtual {v5}, LX/2EJ;->show()V

    .line 2000218
    new-instance v5, LX/4G6;

    invoke-direct {v5}, LX/4G6;-><init>()V

    iget-object v6, v0, LX/DT7;->a:Ljava/lang/String;

    .line 2000219
    const-string v7, "actor_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000220
    move-object v5, v5

    .line 2000221
    const-string v6, "group_id"

    invoke-virtual {v5, v6, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000222
    move-object v5, v5

    .line 2000223
    const-string v6, "user_id"

    invoke-virtual {v5, v6, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2000224
    move-object v5, v5

    .line 2000225
    new-instance v6, LX/DV7;

    invoke-direct {v6}, LX/DV7;-><init>()V

    move-object v6, v6

    .line 2000226
    const-string v7, "input"

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2000227
    iget-object v5, v0, LX/DT7;->d:LX/0tX;

    invoke-static {v6}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2000228
    new-instance v6, LX/DT6;

    invoke-direct {v6, v0}, LX/DT6;-><init>(LX/DT7;)V

    iget-object v7, v0, LX/DT7;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2000229
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    .line 2000230
    :cond_0
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2000204
    const/4 v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2000203
    const/4 v0, 0x1

    return v0
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 2000202
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2000199
    invoke-super {p0, p1, p2}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2000200
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->c:LX/DZ7;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupSuggestAdminMemberListFragment;->d:Landroid/content/res/Resources;

    const v2, 0x7f082fc7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 2000201
    return-void
.end method
