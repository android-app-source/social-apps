.class public Lcom/facebook/groups/memberlist/GroupMemberListFragment;
.super Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;
.source ""


# static fields
.field public static final x:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:LX/DRv;

.field private B:LX/DSR;

.field public C:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public D:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public E:Ljava/lang/String;

.field public F:LX/DTZ;

.field public G:LX/DSp;

.field private H:LX/DSY;

.field public I:LX/0Yb;

.field public a:LX/DSq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/DTa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DS7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DSv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DS1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/DZ7;
    .annotation runtime Lcom/facebook/groups/memberlist/annotation/GroupMembershipNavHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5QP;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/CGV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private y:LX/DRp;

.field private z:LX/DRr;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1999629
    const-class v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    sput-object v0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->x:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1999625
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;-><init>()V

    .line 1999626
    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    .line 1999627
    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    .line 1999628
    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->E:Ljava/lang/String;

    return-void
.end method

.method private F()V
    .locals 9

    .prologue
    .line 1999618
    new-instance v0, LX/DSY;

    invoke-direct {v0}, LX/DSY;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->H:LX/DSY;

    .line 1999619
    new-instance v2, LX/DSZ;

    invoke-direct {v2}, LX/DSZ;-><init>()V

    .line 1999620
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->d:LX/DS7;

    new-instance v1, LX/DSL;

    invoke-direct {v1, p0}, LX/DSL;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    new-instance v3, LX/DSM;

    invoke-direct {v3, p0}, LX/DSM;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    new-instance v4, LX/DSN;

    invoke-direct {v4, p0}, LX/DSN;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    .line 1999621
    iget-object v5, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1999622
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->r()Z

    move-result v6

    invoke-virtual/range {v0 .. v6}, LX/DS7;->a(LX/DRg;LX/DSZ;LX/DRi;LX/DRk;Ljava/lang/String;Z)LX/DS6;

    move-result-object v6

    .line 1999623
    iget-object v3, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->a:LX/DSq;

    iget-object v4, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->H:LX/DSY;

    iget-object v5, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->H:LX/DSY;

    new-instance v8, LX/DS3;

    invoke-direct {v8}, LX/DS3;-><init>()V

    move-object v7, v2

    invoke-virtual/range {v3 .. v8}, LX/DSq;->a(LX/BWf;LX/DSX;LX/DS5;LX/DSZ;LX/BWd;)LX/DSp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    .line 1999624
    return-void
.end method

.method public static G(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V
    .locals 4

    .prologue
    .line 1999610
    new-instance v0, LX/DTv;

    invoke-direct {v0}, LX/DTv;-><init>()V

    move-object v0, v0

    .line 1999611
    const-string v1, "group_id"

    .line 1999612
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1999613
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "member_count"

    const-string v3, "50"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "end_cursor"

    iget-object v3, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->E:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "fetch_admin_type"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1999614
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1999615
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->j:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1999616
    new-instance v1, LX/DST;

    invoke-direct {v1, p0}, LX/DST;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1999617
    return-void
.end method

.method public static H(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V
    .locals 3

    .prologue
    .line 1999593
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    if-eqz v0, :cond_1

    .line 1999594
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    if-eqz v0, :cond_0

    .line 1999595
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    .line 1999596
    iput-object v1, v0, LX/DTZ;->i:Ljava/util/Set;

    .line 1999597
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    invoke-virtual {v0}, LX/DSp;->a()LX/DSZ;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->C:Ljava/util/Set;

    .line 1999598
    iget-object v2, v0, LX/DSZ;->a:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 1999599
    iget-object v2, v0, LX/DSZ;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1999600
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    if-eqz v0, :cond_3

    .line 1999601
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    if-eqz v0, :cond_2

    .line 1999602
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    .line 1999603
    iput-object v1, v0, LX/DTZ;->j:Ljava/util/Set;

    .line 1999604
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    invoke-virtual {v0}, LX/DSp;->a()LX/DSZ;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->D:Ljava/util/Set;

    .line 1999605
    iget-object v2, v0, LX/DSZ;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 1999606
    iget-object v2, v0, LX/DSZ;->b:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1999607
    :cond_3
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    invoke-virtual {v0}, LX/DTZ;->a()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c(Z)V

    .line 1999608
    return-void

    .line 1999609
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/groups/memberlist/GroupMemberListFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1999587
    invoke-virtual {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->b(Ljava/lang/String;)V

    .line 1999588
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1999589
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->H:LX/DSY;

    invoke-virtual {v0}, LX/DSY;->c()V

    .line 1999590
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    const v1, -0x7a24a1ae

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1999591
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Z)V

    .line 1999592
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/B1d;
    .locals 8

    .prologue
    .line 1999574
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1999575
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1999576
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->f:LX/DS1;

    .line 1999577
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1999578
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1999579
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->D()I

    move-result v4

    .line 1999580
    iget-object v3, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->y:LX/B1b;

    move-object v5, v3

    .line 1999581
    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, LX/DS1;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/B1b;)LX/DS0;

    move-result-object v0

    .line 1999582
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->e:LX/DSv;

    .line 1999583
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1999584
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->e()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->D()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 1999585
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->y:LX/B1b;

    move-object v6, v2

    .line 1999586
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->r:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->r:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v2}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v7

    :goto_1
    move-object v2, p1

    invoke-virtual/range {v0 .. v7}, LX/DSv;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Integer;LX/B1b;Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;)LX/DSu;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public final a(I)LX/DSf;
    .locals 2

    .prologue
    .line 1999570
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    invoke-virtual {v0, p1}, LX/BWh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1999571
    instance-of v1, v0, LX/DSf;

    if-eqz v1, :cond_0

    .line 1999572
    check-cast v0, LX/DSf;

    .line 1999573
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1999564
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->H:LX/DSY;

    .line 1999565
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    move-object v1, v1

    .line 1999566
    invoke-virtual {v1}, LX/B1d;->d()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, LX/DSY;->a(LX/0Px;LX/0P1;)V

    .line 1999567
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    const v1, -0x65a9b437

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1999568
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(LX/0Px;)V

    .line 1999569
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    .line 1999630
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Landroid/os/Bundle;)V

    .line 1999631
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;

    const-class v3, LX/DSq;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/DSq;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const-class v5, LX/DTa;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/DTa;

    const-class v6, LX/DS7;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/DS7;

    const-class v7, LX/DSv;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/DSv;

    const-class v8, LX/DS1;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/DS1;

    invoke-static {v0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v9

    check-cast v9, LX/0iA;

    invoke-static {v0}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object v10

    check-cast v10, LX/DZ7;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v11

    check-cast v11, LX/0Xl;

    const/16 v12, 0x238f

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 p1, 0x455

    invoke-static {v0, p1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v0}, LX/CGV;->a(LX/0QB;)LX/CGV;

    move-result-object v0

    check-cast v0, LX/CGV;

    iput-object v3, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->a:LX/DSq;

    iput-object v4, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v5, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->c:LX/DTa;

    iput-object v6, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->d:LX/DS7;

    iput-object v7, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->e:LX/DSv;

    iput-object v8, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->f:LX/DS1;

    iput-object v9, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->g:LX/0iA;

    iput-object v10, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->s:LX/DZ7;

    iput-object v11, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->t:LX/0Xl;

    iput-object v12, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->u:LX/0Ot;

    iput-object p1, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->v:LX/0Ot;

    iput-object v0, v2, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->w:LX/CGV;

    .line 1999632
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c(Z)V

    .line 1999633
    return-void
.end method

.method public final a(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1999559
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->H:LX/DSY;

    invoke-virtual {v0}, LX/DSY;->c()V

    .line 1999560
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    const v1, 0x3718cf7

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1999561
    invoke-super {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Landroid/text/Editable;)V

    .line 1999562
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o()V

    .line 1999563
    return-void
.end method

.method public final a(Landroid/view/View;LX/DSf;)V
    .locals 3

    .prologue
    .line 1999555
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    .line 1999556
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->x:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-object v1, v1

    .line 1999557
    iget-boolean v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->q:Z

    invoke-virtual {v0, p1, p2, v1, v2}, LX/DTZ;->onClick(Landroid/view/View;LX/DSf;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Z)V

    .line 1999558
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1999554
    const/4 v0, 0x0

    return v0
.end method

.method public final c()LX/1Cv;
    .locals 1

    .prologue
    .line 1999551
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    if-nez v0, :cond_0

    .line 1999552
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F()V

    .line 1999553
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    return-object v0
.end method

.method public final d()LX/DSA;
    .locals 1

    .prologue
    .line 1999548
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    if-nez v0, :cond_0

    .line 1999549
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F()V

    .line 1999550
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G:LX/DSp;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1999547
    const/4 v0, 0x0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1999546
    const/4 v0, 0x1

    return v0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 1999528
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->c:LX/DTa;

    .line 1999529
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1999530
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object v2, v2

    .line 1999531
    invoke-virtual {v0, v1, v2}, LX/DTa;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupAdminType;)LX/DTZ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    .line 1999532
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    invoke-virtual {v0}, LX/DTZ;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c(Z)V

    .line 1999533
    new-instance v0, LX/DSO;

    invoke-direct {v0, p0}, LX/DSO;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->y:LX/DRp;

    .line 1999534
    new-instance v0, LX/DSP;

    invoke-direct {v0, p0}, LX/DSP;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->z:LX/DRr;

    .line 1999535
    new-instance v0, LX/DSQ;

    invoke-direct {v0, p0}, LX/DSQ;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->A:LX/DRv;

    .line 1999536
    new-instance v0, LX/DSS;

    invoke-direct {v0, p0}, LX/DSS;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->B:LX/DSR;

    .line 1999537
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    .line 1999538
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->g:LX/DSE;

    move-object v1, v1

    .line 1999539
    iput-object v1, v0, LX/DTZ;->k:LX/DSE;

    .line 1999540
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->y:LX/DRp;

    invoke-virtual {v0, v1}, LX/DTZ;->a(LX/DRo;)V

    .line 1999541
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->z:LX/DRr;

    invoke-virtual {v0, v1}, LX/DTZ;->a(LX/DRo;)V

    .line 1999542
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->A:LX/DRv;

    invoke-virtual {v0, v1}, LX/DTZ;->a(LX/DRo;)V

    .line 1999543
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->B:LX/DSR;

    invoke-virtual {v0, v1}, LX/DTZ;->a(LX/DRo;)V

    .line 1999544
    return-void

    .line 1999545
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x25b7ccd9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1999519
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    .line 1999520
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->g:LX/DSE;

    move-object v2, v2

    .line 1999521
    invoke-virtual {v1, v2}, LX/DTZ;->b(LX/DSE;)V

    .line 1999522
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->y:LX/DRp;

    invoke-virtual {v1, v2}, LX/DTZ;->b(LX/DRo;)V

    .line 1999523
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->z:LX/DRr;

    invoke-virtual {v1, v2}, LX/DTZ;->b(LX/DRo;)V

    .line 1999524
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->F:LX/DTZ;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->A:LX/DRv;

    invoke-virtual {v1, v2}, LX/DTZ;->b(LX/DRo;)V

    .line 1999525
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->I:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 1999526
    invoke-super {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->onDestroyView()V

    .line 1999527
    const/16 v1, 0x2b

    const v2, -0x504589bf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1999496
    invoke-super {p0, p1, p2}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1999497
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1999498
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1999499
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->s:LX/DZ7;

    .line 1999500
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1999501
    const/4 v2, 0x0

    invoke-interface {v0, p0, v1, v2}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 1999502
    :cond_0
    const v0, 0x7f0d1b6a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    .line 1999503
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->ADMIN:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1999504
    iget-object v2, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object v2, v2

    .line 1999505
    if-ne v0, v2, :cond_1

    .line 1999506
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->g:LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->GROUP_ADD_MODERATOR:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/DTu;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/DTu;

    .line 1999507
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 1999508
    if-nez v1, :cond_2

    .line 1999509
    :goto_0
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->g:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    invoke-virtual {v0}, LX/DTu;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1999510
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->t:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.STREAM_PUBLISH_COMPLETE"

    new-instance v2, LX/DSU;

    invoke-direct {v2, p0}, LX/DSU;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->I:LX/0Yb;

    .line 1999511
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->I:LX/0Yb;

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 1999512
    invoke-static {p0}, Lcom/facebook/groups/memberlist/GroupMemberListFragment;->G(Lcom/facebook/groups/memberlist/GroupMemberListFragment;)V

    .line 1999513
    return-void

    .line 1999514
    :cond_2
    new-instance v2, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 p1, 0x2

    invoke-direct {v2, v3, p1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1999515
    const/4 v3, -0x1

    .line 1999516
    iput v3, v2, LX/0hs;->t:I

    .line 1999517
    const v3, 0x7f082fd7

    invoke-virtual {v2, v3}, LX/0hs;->a(I)V

    .line 1999518
    invoke-virtual {v2, v1}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0
.end method
