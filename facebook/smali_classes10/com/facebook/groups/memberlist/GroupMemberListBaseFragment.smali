.class public abstract Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/widget/listview/BetterListView;

.field public g:LX/DSE;

.field public h:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/DSC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/DVC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/DSB;

.field public p:Z

.field public q:Z

.field public r:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

.field public s:LX/B1d;

.field public t:Landroid/widget/EditText;

.field public u:Landroid/widget/ImageButton;

.field public v:Landroid/view/View;

.field public w:Ljava/lang/String;

.field public x:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field public final y:LX/B1b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1998666
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1998667
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1998668
    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->e:LX/0Px;

    .line 1998669
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->w:Ljava/lang/String;

    .line 1998670
    new-instance v0, LX/DSD;

    invoke-direct {v0, p0}, LX/DSD;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->y:LX/B1b;

    return-void
.end method

.method private H()V
    .locals 6

    .prologue
    .line 1998671
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->i:LX/DSC;

    new-instance v1, LX/DSJ;

    invoke-direct {v1, p0}, LX/DSJ;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;)V

    new-instance v2, LX/DSZ;

    invoke-direct {v2}, LX/DSZ;-><init>()V

    iget-object v3, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    .line 1998672
    new-instance v5, LX/DSB;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v5, v1, v2, v3, v4}, LX/DSB;-><init>(LX/DRg;LX/DSZ;Ljava/lang/String;Ljava/lang/String;)V

    .line 1998673
    move-object v0, v5

    .line 1998674
    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    .line 1998675
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1998676
    if-eqz p1, :cond_0

    .line 1998677
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->k:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1998678
    :cond_0
    return-void
.end method


# virtual methods
.method public final C()V
    .locals 1

    .prologue
    .line 1998679
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d()LX/DSA;

    move-result-object v0

    invoke-interface {v0}, LX/DSA;->b()V

    .line 1998680
    return-void
.end method

.method public final D()I
    .locals 2

    .prologue
    .line 1998681
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->h:Landroid/content/res/Resources;

    const v1, 0x7f0b1f9e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public abstract a(Ljava/lang/String;)LX/B1d;
.end method

.method public a(I)LX/DSf;
    .locals 1

    .prologue
    .line 1998682
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    invoke-virtual {v0, p1}, LX/DSB;->a(I)LX/DSf;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/DSf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1998683
    iput-object p1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->e:LX/0Px;

    .line 1998684
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1998685
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1998686
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const-class v4, LX/DSC;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/DSC;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v7

    check-cast v7, LX/0Sh;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {v0}, LX/DVC;->b(LX/0QB;)LX/DVC;

    move-result-object v0

    check-cast v0, LX/DVC;

    iput-object v3, v2, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->h:Landroid/content/res/Resources;

    iput-object v4, v2, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->i:LX/DSC;

    iput-object v5, v2, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->j:LX/0tX;

    iput-object v6, v2, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->k:Landroid/view/inputmethod/InputMethodManager;

    iput-object v7, v2, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->l:LX/0Sh;

    iput-object p1, v2, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->m:Ljava/lang/String;

    iput-object v0, v2, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->n:LX/DVC;

    .line 1998687
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1998688
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a:Ljava/lang/String;

    .line 1998689
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1998690
    const-string v1, "community_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->b:Ljava/lang/String;

    .line 1998691
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1998692
    const-string v1, "group_feed_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c:Ljava/lang/String;

    .line 1998693
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1998694
    const-string v1, "is_archived"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->q:Z

    .line 1998695
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1998696
    const-string v1, "group_admin_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    .line 1998697
    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 1998698
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1998699
    const-string v1, "group_info_data_model"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->r:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    .line 1998700
    return-void
.end method

.method public a(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 1998701
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->C()V

    .line 1998702
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->w:Ljava/lang/String;

    .line 1998703
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->p()V

    .line 1998704
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1998705
    const-string p1, ""

    invoke-virtual {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Ljava/lang/String;)LX/B1d;

    move-result-object p1

    .line 1998706
    :goto_0
    move-object v0, p1

    .line 1998707
    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    .line 1998708
    return-void

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Ljava/lang/String;)LX/B1d;

    move-result-object p1

    goto :goto_0
.end method

.method public abstract a(Landroid/view/View;LX/DSf;)V
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 1998709
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    .line 1998710
    :goto_0
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->v:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1998711
    return-void

    .line 1998712
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1998713
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d()LX/DSA;

    move-result-object v0

    .line 1998714
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    move-object v1, v1

    .line 1998715
    invoke-interface {v0, p1, v1}, LX/DSA;->a(Ljava/lang/String;Lcom/facebook/widget/listview/BetterListView;)V

    .line 1998716
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1998660
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d()LX/DSA;

    move-result-object v0

    invoke-interface {v0}, LX/DSA;->a()LX/DSZ;

    move-result-object v0

    .line 1998661
    iget-boolean v1, v0, LX/DSZ;->d:Z

    if-eq p1, v1, :cond_0

    .line 1998662
    iput-boolean p1, v0, LX/DSZ;->d:Z

    .line 1998663
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c()LX/1Cv;

    move-result-object v0

    const v1, 0xeac7b35

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1998664
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 1998665
    const/4 v0, 0x1

    return v0
.end method

.method public c()LX/1Cv;
    .locals 1

    .prologue
    .line 1998593
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    if-nez v0, :cond_0

    .line 1998594
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->H()V

    .line 1998595
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    return-object v0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 1998596
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d()LX/DSA;

    move-result-object v0

    invoke-interface {v0}, LX/DSA;->a()LX/DSZ;

    move-result-object v0

    .line 1998597
    iput-boolean p1, v0, LX/DSZ;->f:Z

    .line 1998598
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c()LX/1Cv;

    move-result-object v0

    const v1, 0x4f396f1d    # 3.11106688E9f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1998599
    return-void
.end method

.method public d()LX/DSA;
    .locals 1

    .prologue
    .line 1998600
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    if-nez v0, :cond_0

    .line 1998601
    invoke-direct {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->H()V

    .line 1998602
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o:LX/DSB;

    return-object v0
.end method

.method public k()Landroid/view/View;
    .locals 1

    .prologue
    .line 1998603
    const v0, 0x7f0d04ed

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public abstract l()Z
.end method

.method public abstract m()V
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 1998604
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    invoke-virtual {v0}, LX/B1d;->f()V

    .line 1998605
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x190bf163

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1998606
    new-instance v1, LX/DSE;

    invoke-direct {v1, p0}, LX/DSE;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;)V

    iput-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->g:LX/DSE;

    .line 1998607
    const-string v1, ""

    invoke-virtual {p0, v1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Ljava/lang/String;)LX/B1d;

    move-result-object v1

    move-object v1, v1

    .line 1998608
    iput-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    .line 1998609
    const v1, 0x7f030ab8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1f353123

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6f0379ee

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1998610
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->p()V

    .line 1998611
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d()LX/DSA;

    move-result-object v1

    invoke-interface {v1}, LX/DSA;->a()LX/DSZ;

    move-result-object v1

    invoke-virtual {v1}, LX/DSZ;->a()V

    .line 1998612
    iget-object v1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->t:Landroid/widget/EditText;

    invoke-static {p0, v1}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a$redex0(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;Landroid/view/View;)V

    .line 1998613
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1998614
    const/16 v1, 0x2b

    const v2, 0x6ab2797c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1998615
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1998616
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->b(Z)V

    .line 1998617
    const v0, 0x7f0d14f1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1998618
    const p2, 0x7f03085f

    move p1, p2

    .line 1998619
    invoke-virtual {v0, p1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1998620
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1998621
    iget-object p1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->h:Landroid/content/res/Resources;

    const p2, 0x7f082fcf

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1998622
    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1998623
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->k()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->v:Landroid/view/View;

    .line 1998624
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a(Z)V

    .line 1998625
    const v0, 0x7f0d1b6a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    .line 1998626
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c()LX/1Cv;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1998627
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d()LX/DSA;

    move-result-object v0

    invoke-interface {v0}, LX/DSA;->a()LX/DSZ;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->l()Z

    move-result p1

    .line 1998628
    iput-boolean p1, v0, LX/DSZ;->e:Z

    .line 1998629
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->c()LX/1Cv;

    move-result-object v0

    const p1, 0x2264d289

    invoke-static {v0, p1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1998630
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance p1, LX/DSF;

    invoke-direct {p1, p0}, LX/DSF;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1998631
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->m()V

    .line 1998632
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->f:Lcom/facebook/widget/listview/BetterListView;

    new-instance p1, LX/DSG;

    invoke-direct {p1, p0}, LX/DSG;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;)V

    invoke-virtual {v0, p1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1998633
    const v0, 0x7f0d1547

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->u:Landroid/widget/ImageButton;

    .line 1998634
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->u:Landroid/widget/ImageButton;

    iget-object p1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->n:LX/DVC;

    invoke-virtual {p1}, LX/DVC;->c()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1998635
    const v0, 0x7f0d1546

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->t:Landroid/widget/EditText;

    .line 1998636
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->t:Landroid/widget/EditText;

    iget-object p1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->h:Landroid/content/res/Resources;

    const p2, 0x7f082fbc

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1998637
    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    iget-object p1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->h:Landroid/content/res/Resources;

    const p2, 0x7f0a05f2

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    sget-object p2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1998638
    iget-object p1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->n:LX/DVC;

    invoke-virtual {p1}, LX/DVC;->c()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1998639
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1998640
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1998641
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->u:Landroid/widget/ImageButton;

    new-instance p1, LX/DSH;

    invoke-direct {p1, p0}, LX/DSH;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1998642
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->t:Landroid/widget/EditText;

    new-instance p1, LX/DSI;

    invoke-direct {p1, p0}, LX/DSI;-><init>(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;)V

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1998643
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->o()V

    .line 1998644
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 1998645
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    invoke-virtual {v0}, LX/B1d;->e()V

    .line 1998646
    return-void
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 1998647
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->r:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    if-nez v0, :cond_0

    .line 1998648
    const/4 v0, 0x0

    .line 1998649
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->r:Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/info/protocol/FetchGroupInfoPageDataModels$FetchGroupInfoPageDataModel;->v()Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;

    move-result-object v0

    const-string v1, "gk_groups_member_bios"

    invoke-static {v0, v1}, LX/88m;->a(Lcom/facebook/api/graphql/feed/NewsFeedDefaultsStoryAttachmentGraphQLModels$GroupConfigsModel$GroupConfigsGroupConfigsModel;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 1998650
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->w:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 1998651
    iput-boolean p1, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->p:Z

    .line 1998652
    if-nez p1, :cond_0

    .line 1998653
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->t:Landroid/widget/EditText;

    invoke-static {p0, v0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->a$redex0(Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;Landroid/view/View;)V

    .line 1998654
    :cond_0
    return-void
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 1998655
    iget-object v0, p0, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->s:LX/B1d;

    move-object v0, v0

    .line 1998656
    invoke-virtual {v0}, LX/B1d;->i()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, v0, LX/B1d;->b:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1998657
    if-nez v0, :cond_0

    .line 1998658
    invoke-virtual {p0}, Lcom/facebook/groups/memberlist/GroupMemberListBaseFragment;->d()LX/DSA;

    move-result-object v0

    invoke-interface {v0}, LX/DSA;->c()Z

    move-result v0

    move v0, v0

    .line 1998659
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
