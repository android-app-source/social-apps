.class public Lcom/facebook/groups/memberlist/MembershipTabsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/DTh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/DZ7;
    .annotation runtime Lcom/facebook/groups/memberlist/annotation/GroupMembershipNavHandler;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/DVC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/DVF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/groups/memberlist/protocol/FetchGroupMemberListHeaderModels$FetchGroupMemberListHeaderModel;

.field public l:LX/DLO;

.field private m:LX/DRr;

.field public n:LX/DTb;

.field private o:Landroid/support/v4/view/ViewPager;

.field private p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2000693
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method public static b(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V
    .locals 4

    .prologue
    .line 2000694
    new-instance v0, LX/DTb;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    .line 2000695
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2000696
    iget-object v3, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->a:Landroid/content/res/Resources;

    invoke-direct {v0, v1, v2, v3}, LX/DTb;-><init>(LX/0gc;Landroid/os/Bundle;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->n:LX/DTb;

    .line 2000697
    iget-object v0, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->o:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->n:LX/DTb;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2000698
    iget-object v0, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iget-object v1, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->o:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2000699
    return-void
.end method

.method public static e$redex0(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V
    .locals 4

    .prologue
    .line 2000700
    iget-object v0, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->a:Landroid/content/res/Resources;

    const v3, 0x7f081b54

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2000701
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2000702
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2000703
    const-string v0, "group_mall_membership_tabs"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 2000704
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2000705
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v2, p0

    check-cast v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;

    invoke-static {p1}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {p1}, LX/DTh;->a(LX/0QB;)LX/DTh;

    move-result-object v6

    check-cast v6, LX/DTh;

    invoke-static {p1}, LX/DZ8;->a(LX/0QB;)LX/DZ8;

    move-result-object v7

    check-cast v7, LX/DZ7;

    invoke-static {p1}, LX/DVC;->b(LX/0QB;)LX/DVC;

    move-result-object v8

    check-cast v8, LX/DVC;

    invoke-static {p1}, LX/Jaq;->b(LX/0QB;)LX/Jaq;

    move-result-object v9

    check-cast v9, LX/DVF;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    const/16 v0, 0x12c4

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v3, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->a:Landroid/content/res/Resources;

    iput-object v4, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v5, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->c:Ljava/lang/String;

    iput-object v6, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->d:LX/DTh;

    iput-object v7, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->e:LX/DZ7;

    iput-object v8, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->f:LX/DVC;

    iput-object v9, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->g:LX/DVF;

    iput-object v10, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->h:LX/1Ck;

    iput-object v11, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->i:LX/0tX;

    iput-object p1, v2, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->j:LX/0Ot;

    .line 2000706
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2000707
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->q:Ljava/lang/String;

    .line 2000708
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2074611b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2000709
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2000710
    new-instance v1, LX/DTc;

    invoke-direct {v1, p0}, LX/DTc;-><init>(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V

    iput-object v1, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->l:LX/DLO;

    .line 2000711
    iget-object v1, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->e:LX/DZ7;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->a:Landroid/content/res/Resources;

    const v3, 0x7f082fc0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->l:LX/DLO;

    invoke-interface {v1, p0, v2, v3}, LX/DZ7;->a(Lcom/facebook/base/fragment/FbFragment;Ljava/lang/String;LX/DLO;)V

    .line 2000712
    new-instance v1, LX/DTd;

    invoke-direct {v1, p0}, LX/DTd;-><init>(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V

    iput-object v1, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->m:LX/DRr;

    .line 2000713
    const/16 v1, 0x2b

    const v2, -0x5070f134

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x34a2e810    # -1.4489584E7f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2000714
    const v1, 0x7f030abd

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x30abacb4

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x208eee0e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2000715
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2000716
    iget-object v1, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->h:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2000717
    const/16 v1, 0x2b

    const v2, -0x482f4a4b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x23ef6acb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2000718
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2000719
    iget-object v1, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->d:LX/DTh;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->m:LX/DRr;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2000720
    const/16 v1, 0x2b

    const v2, -0x7a784116

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3bfbe39e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2000721
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2000722
    iget-object v1, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->d:LX/DTh;

    iget-object v2, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->m:LX/DRr;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2000723
    const/16 v1, 0x2b

    const v2, 0x41562281

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2000724
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2000725
    const v0, 0x7f0d1b74

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->o:Landroid/support/v4/view/ViewPager;

    .line 2000726
    const v0, 0x7f0d1b73

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    iput-object v0, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->p:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

    .line 2000727
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2000728
    const-string p1, "group_admin_type"

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2000729
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2000730
    const-string p1, "is_viewer_joined"

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2000731
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2000732
    const-string p1, "group_visibility"

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2000733
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2000734
    const-string p1, "group_url"

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2000735
    if-eqz v0, :cond_1

    .line 2000736
    iget-object v0, p0, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->h:LX/1Ck;

    const-string v1, "fetch_memberlist_header"

    new-instance p1, LX/DTe;

    invoke-direct {p1, p0}, LX/DTe;-><init>(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V

    new-instance p2, LX/DTf;

    invoke-direct {p2, p0}, LX/DTf;-><init>(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V

    invoke-virtual {v0, v1, p1, p2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2000737
    :goto_1
    return-void

    .line 2000738
    :cond_1
    invoke-static {p0}, Lcom/facebook/groups/memberlist/MembershipTabsFragment;->b(Lcom/facebook/groups/memberlist/MembershipTabsFragment;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
