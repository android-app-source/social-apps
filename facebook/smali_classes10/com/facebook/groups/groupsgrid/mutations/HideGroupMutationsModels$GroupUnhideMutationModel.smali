.class public final Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1993640
    const-class v0, Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1993643
    const-class v0, Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1993641
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1993642
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1993632
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel;->e:Ljava/lang/String;

    .line 1993633
    iget-object v0, p0, Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1993634
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1993635
    invoke-direct {p0}, Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1993636
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1993637
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1993638
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1993639
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1993624
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1993625
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1993626
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1993629
    new-instance v0, Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/groupsgrid/mutations/HideGroupMutationsModels$GroupUnhideMutationModel;-><init>()V

    .line 1993630
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1993631
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1993627
    const v0, 0x76936864

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1993628
    const v0, -0x7a17abed

    return v0
.end method
