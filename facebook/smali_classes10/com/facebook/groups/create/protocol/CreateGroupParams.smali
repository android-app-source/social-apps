.class public Lcom/facebook/groups/create/protocol/CreateGroupParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/groups/create/protocol/CreateGroupParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1986993
    new-instance v0, LX/DKR;

    invoke-direct {v0}, LX/DKR;-><init>()V

    sput-object v0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1986994
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1986995
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->a:Ljava/lang/String;

    .line 1986996
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->b:Ljava/lang/String;

    .line 1986997
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->c:Ljava/lang/String;

    .line 1986998
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->d:Ljava/lang/String;

    .line 1986999
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->e:Ljava/lang/String;

    .line 1987000
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->f:Ljava/lang/String;

    .line 1987001
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->g:Ljava/lang/String;

    .line 1987002
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->h:Ljava/lang/String;

    .line 1987003
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->i:Ljava/lang/String;

    .line 1987004
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1987005
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1987006
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987007
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987008
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->c:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987009
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->d:Ljava/lang/String;

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987010
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->e:Ljava/lang/String;

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987011
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->f:Ljava/lang/String;

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987012
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->g:Ljava/lang/String;

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987013
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->h:Ljava/lang/String;

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987014
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/groups/create/protocol/CreateGroupParams;->i:Ljava/lang/String;

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987015
    return-void

    .line 1987016
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 1987017
    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 1987018
    :cond_2
    const-string v0, ""

    goto :goto_2

    .line 1987019
    :cond_3
    const-string v0, ""

    goto :goto_3

    .line 1987020
    :cond_4
    const-string v0, ""

    goto :goto_4

    .line 1987021
    :cond_5
    const-string v0, ""

    goto :goto_5

    .line 1987022
    :cond_6
    const-string v0, ""

    goto :goto_6
.end method
