.class public final Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1987105
    const-class v0, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1987104
    const-class v0, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1987102
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1987103
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1987096
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1987097
    invoke-virtual {p0}, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1987098
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1987099
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1987100
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1987101
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1987093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1987094
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1987095
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1987092
    new-instance v0, LX/DKU;

    invoke-direct {v0, p1}, LX/DKU;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1987091
    invoke-virtual {p0}, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1987089
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1987090
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1987081
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1987086
    new-instance v0, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;-><init>()V

    .line 1987087
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1987088
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1987085
    const v0, 0x1ca9545a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1987084
    const v0, 0x41e065f

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1987082
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;->e:Ljava/lang/String;

    .line 1987083
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;->e:Ljava/lang/String;

    return-object v0
.end method
