.class public final Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/4Fi;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/F4e;

.field public final synthetic d:LX/0Px;

.field public final synthetic e:LX/0Px;

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Z

.field public final synthetic h:Landroid/net/Uri;

.field public final synthetic i:LX/DKa;


# direct methods
.method public constructor <init>(LX/DKa;LX/4Fi;Lcom/google/common/util/concurrent/SettableFuture;LX/F4e;LX/0Px;LX/0Px;Ljava/lang/String;ZLandroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1987201
    iput-object p1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->i:LX/DKa;

    iput-object p2, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->a:LX/4Fi;

    iput-object p3, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p4, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->c:LX/F4e;

    iput-object p5, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->d:LX/0Px;

    iput-object p6, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->e:LX/0Px;

    iput-object p7, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->f:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->g:Z

    iput-object p9, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->h:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1987202
    :try_start_0
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->i:LX/DKa;

    iget-object v0, v0, LX/DKa;->e:LX/DKb;

    iget-object v1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->a:LX/4Fi;

    iget-object v2, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->i:LX/DKa;

    iget-object v2, v2, LX/DKa;->i:LX/0SI;

    invoke-interface {v2}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 1987203
    new-instance v3, LX/DKS;

    invoke-direct {v3}, LX/DKS;-><init>()V

    move-object v3, v3

    .line 1987204
    const-string v4, "input"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1987205
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1987206
    iput-object v2, v3, LX/399;->e:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1987207
    move-object v3, v3

    .line 1987208
    iget-object v4, v0, LX/DKb;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 1987209
    const/4 v2, 0x0

    .line 1987210
    const v1, -0x51d62d49

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1987211
    if-eqz v0, :cond_1

    .line 1987212
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1987213
    if-eqz v1, :cond_1

    .line 1987214
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1987215
    check-cast v1, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel;->a()Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1987216
    iget-object v1, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v1

    .line 1987217
    check-cast v0, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel;->a()Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/create/protocol/GroupCreateMutationModels$GroupCreateMutationFieldsModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1987218
    :goto_0
    if-nez v1, :cond_0

    .line 1987219
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Create group failure: Couldn\'t get group id from creation"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1987220
    :goto_1
    return-void

    .line 1987221
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->i:LX/DKa;

    sget-object v2, LX/DKF;->INVITE_MEMBERS:LX/DKF;

    iget-object v3, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->c:LX/F4e;

    invoke-static {v0, v2, v3}, LX/DKa;->a$redex0(LX/DKa;LX/DKF;LX/F4e;)V

    .line 1987222
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->i:LX/DKa;

    iget-object v0, v0, LX/DKa;->f:LX/DWD;

    iget-object v2, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->d:LX/0Px;

    iget-object v3, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->e:LX/0Px;

    const-string v4, "mobile_create_group"

    iget-object v5, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->f:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/DWD;->a(Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1987223
    new-instance v2, LX/DKY;

    invoke-direct {v2, p0, v1}, LX/DKY;-><init>(Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->i:LX/DKa;

    iget-object v1, v1, LX/DKa;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v2, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1987224
    :catch_0
    move-exception v0

    .line 1987225
    iget-object v1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$2;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_1

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method
