.class public Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1987336
    new-instance v0, LX/DKd;

    invoke-direct {v0}, LX/DKd;-><init>()V

    sput-object v0, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1987332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1987333
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;->a:Ljava/lang/String;

    .line 1987334
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;->b:Ljava/lang/String;

    .line 1987335
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1987337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1987338
    iput-object p1, p0, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;->a:Ljava/lang/String;

    .line 1987339
    iput-object p2, p0, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;->b:Ljava/lang/String;

    .line 1987340
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1987331
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1987328
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987329
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1987330
    return-void
.end method
