.class public final Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;

.field public final synthetic b:Z

.field public final synthetic c:LX/F4e;

.field public final synthetic d:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/DKa;


# direct methods
.method public constructor <init>(LX/DKa;Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;ZLX/F4e;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1987253
    iput-object p1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->f:LX/DKa;

    iput-object p2, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->a:Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;

    iput-boolean p3, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->b:Z

    iput-object p4, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->c:LX/F4e;

    iput-object p5, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->d:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p6, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1987245
    :try_start_0
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->f:LX/DKa;

    iget-object v0, v0, LX/DKa;->b:LX/18V;

    iget-object v1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->f:LX/DKa;

    iget-object v1, v1, LX/DKa;->g:LX/DKc;

    iget-object v2, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->a:Lcom/facebook/groups/create/protocol/SetAsCoverPhotoParams;

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1987246
    iget-object v1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->f:LX/DKa;

    iget-object v1, v1, LX/DKa;->d:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4$1;

    invoke-direct {v2, p0}, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4$1;-><init>(Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;)V

    const v3, -0x69ef5b3d

    invoke-static {v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1987247
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1987248
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->d:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->e:Ljava/lang/String;

    const v2, -0x7faae96

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1987249
    :goto_0
    return-void

    .line 1987250
    :catch_0
    move-exception v0

    .line 1987251
    :try_start_1
    sget-object v1, LX/DKa;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1987252
    iget-object v0, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->d:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->e:Ljava/lang/String;

    const v2, 0x4995ba18    # 1226563.0f

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->d:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v2, p0, Lcom/facebook/groups/create/protocol/GroupCreationActionHandler$4;->e:Ljava/lang/String;

    const v3, -0x67d8ed1e

    invoke-static {v1, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    throw v0
.end method
