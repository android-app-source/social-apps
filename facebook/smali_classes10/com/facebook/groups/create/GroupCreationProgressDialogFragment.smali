.class public Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source ""


# instance fields
.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1986793
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 1986794
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 1986795
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1986796
    const-string v1, "message_res_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1986797
    const-string v2, "is_indeterminate"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 1986798
    const-string v3, "is_cancelable"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 1986799
    const-string v4, "dismiss_on_pause"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;->j:Z

    .line 1986800
    new-instance v0, LX/4BY;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 1986801
    const/4 v4, 0x0

    .line 1986802
    iput v4, v0, LX/4BY;->d:I

    .line 1986803
    invoke-virtual {v0, v2}, LX/4BY;->a(Z)V

    .line 1986804
    invoke-virtual {v0, v3}, LX/4BY;->setCancelable(Z)V

    .line 1986805
    invoke-virtual {p0, v3}, Landroid/support/v4/app/DialogFragment;->d_(Z)V

    .line 1986806
    if-lez v1, :cond_0

    .line 1986807
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1986808
    :cond_0
    return-object v0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5f2313de

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1986809
    iget-boolean v1, p0, Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;->j:Z

    if-eqz v1, :cond_0

    .line 1986810
    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1986811
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onPause()V

    .line 1986812
    const/16 v1, 0x2b

    const v2, -0x6f8be402

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
