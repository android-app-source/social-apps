.class public Lcom/facebook/rtc/models/RtcVoicemailInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/rtc/models/RtcVoicemailInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:J

.field public e:J

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2097013
    new-instance v0, LX/EGH;

    invoke-direct {v0}, LX/EGH;-><init>()V

    sput-object v0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2097014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2097015
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2097002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2097003
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->a:Ljava/lang/String;

    .line 2097004
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->b:Ljava/lang/String;

    .line 2097005
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->c:Ljava/lang/String;

    .line 2097006
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->d:J

    .line 2097007
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->e:J

    .line 2097008
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->f:Ljava/lang/String;

    .line 2097009
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->g:Ljava/lang/String;

    .line 2097010
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->h:Z

    .line 2097011
    return-void

    .line 2097012
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2097001
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2096991
    iget-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096992
    iget-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096993
    iget-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096994
    iget-wide v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2096995
    iget-wide v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2096996
    iget-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096997
    iget-object v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096998
    iget-boolean v0, p0, Lcom/facebook/rtc/models/RtcVoicemailInfo;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2096999
    return-void

    .line 2097000
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
