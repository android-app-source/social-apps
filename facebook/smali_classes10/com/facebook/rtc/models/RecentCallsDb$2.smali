.class public final Lcom/facebook/rtc/models/RecentCallsDb$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:LX/3Le;


# direct methods
.method public constructor <init>(LX/3Le;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2096743
    iput-object p1, p0, Lcom/facebook/rtc/models/RecentCallsDb$2;->c:LX/3Le;

    iput-object p2, p0, Lcom/facebook/rtc/models/RecentCallsDb$2;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/facebook/rtc/models/RecentCallsDb$2;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2096732
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$2;->c:LX/3Le;

    iget-object v0, v0, LX/3Le;->c:LX/3Lf;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2096733
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2096734
    :cond_0
    sget-object v0, LX/3Le;->a:Ljava/lang/Class;

    const-string v1, "Unable to acquire db for markConferenceCallEnded"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2096735
    :cond_1
    :goto_0
    return-void

    .line 2096736
    :cond_2
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2096737
    const-string v2, "on_going"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2096738
    const-string v2, "thread_id = ?"

    .line 2096739
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$2;->a:Ljava/lang/String;

    aput-object v4, v3, v5

    .line 2096740
    const-string v4, "person_summary"

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2096741
    if-lez v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$2;->b:Z

    if-eqz v0, :cond_1

    .line 2096742
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$2;->c:LX/3Le;

    invoke-static {v0}, LX/3Le;->d(LX/3Le;)V

    goto :goto_0
.end method
