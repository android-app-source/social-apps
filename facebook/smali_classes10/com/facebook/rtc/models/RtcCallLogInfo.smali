.class public Lcom/facebook/rtc/models/RtcCallLogInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/rtc/models/RtcCallLogInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:J

.field public b:Lcom/facebook/user/model/UserKey;

.field public c:J

.field public d:J

.field public e:Z

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z

.field public j:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2096859
    new-instance v0, LX/EGB;

    invoke-direct {v0}, LX/EGB;-><init>()V

    sput-object v0, Lcom/facebook/rtc/models/RtcCallLogInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2096857
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096858
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2096822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096823
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->a:J

    .line 2096824
    const-class v0, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    iput-object v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->b:Lcom/facebook/user/model/UserKey;

    .line 2096825
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->c:J

    .line 2096826
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->d:J

    .line 2096827
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->e:Z

    .line 2096828
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->f:I

    .line 2096829
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->g:I

    .line 2096830
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->h:Z

    .line 2096831
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->i:Z

    .line 2096832
    const-class v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iput-object v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->j:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2096833
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->k:Z

    .line 2096834
    return-void

    :cond_0
    move v0, v2

    .line 2096835
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2096836
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2096837
    goto :goto_2

    :cond_3
    move v1, v2

    .line 2096838
    goto :goto_3
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2096856
    iget v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2096855
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2096839
    iget-wide v4, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->a:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 2096840
    iget-object v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2096841
    iget-wide v4, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->c:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 2096842
    iget-wide v4, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->d:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 2096843
    iget-boolean v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2096844
    iget v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2096845
    iget v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2096846
    iget-boolean v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->h:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2096847
    iget-boolean v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->i:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2096848
    iget-object v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->j:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2096849
    iget-boolean v0, p0, Lcom/facebook/rtc/models/RtcCallLogInfo;->k:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2096850
    return-void

    :cond_0
    move v0, v2

    .line 2096851
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2096852
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2096853
    goto :goto_2

    :cond_3
    move v1, v2

    .line 2096854
    goto :goto_3
.end method
