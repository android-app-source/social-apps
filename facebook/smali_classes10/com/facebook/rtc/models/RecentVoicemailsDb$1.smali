.class public final Lcom/facebook/rtc/models/RecentVoicemailsDb$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:J

.field public final synthetic e:J

.field public final synthetic f:Ljava/lang/String;

.field public final synthetic g:Ljava/lang/String;

.field public final synthetic h:Z

.field public final synthetic i:LX/3Lj;


# direct methods
.method public constructor <init>(LX/3Lj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2096794
    iput-object p1, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->i:LX/3Lj;

    iput-object p2, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->c:Ljava/lang/String;

    iput-wide p5, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->d:J

    iput-wide p7, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->e:J

    iput-object p9, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->f:Ljava/lang/String;

    iput-object p10, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->g:Ljava/lang/String;

    iput-boolean p11, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->h:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 2096795
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->i:LX/3Lj;

    iget-object v0, v0, LX/3Lj;->b:LX/3Lk;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2096796
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2096797
    :cond_0
    :goto_0
    return-void

    .line 2096798
    :cond_1
    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2096799
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2096800
    const-string v2, "caller_id"

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096801
    const-string v2, "callee_id"

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096802
    const-string v2, "call_id"

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096803
    const-string v2, "voicemail_time"

    iget-wide v4, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2096804
    const-string v2, "voicemail_duration"

    iget-wide v4, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->e:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2096805
    const-string v2, "download_uri"

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096806
    const-string v2, "message_id"

    iget-object v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096807
    const-string v2, "voicemail_played"

    iget-boolean v3, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2096808
    const-string v2, "voicemail_summary"

    const/4 v3, 0x0

    const v4, 0x41d40e35

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const v2, -0x1d536f27

    invoke-static {v2}, LX/03h;->a(I)V

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 2096809
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->i:LX/3Lj;

    const-wide/16 v12, 0x64

    .line 2096810
    iget-object v6, v0, LX/3Lj;->b:LX/3Lk;

    invoke-virtual {v6}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 2096811
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v7

    if-nez v7, :cond_3

    .line 2096812
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentVoicemailsDb$1;->i:LX/3Lj;

    .line 2096813
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/3Lj;->a(LX/3Lj;LX/0Px;)V

    .line 2096814
    const/4 v1, -0x1

    invoke-static {v0, v1}, LX/3Lj;->a(LX/3Lj;I)V

    .line 2096815
    goto/16 :goto_0

    .line 2096816
    :cond_3
    const-string v7, "voicemail_summary"

    invoke-static {v6, v7}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v8

    .line 2096817
    cmp-long v7, v8, v12

    if-lez v7, :cond_2

    .line 2096818
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v10, "DELETE FROM voicemail_summary WHERE ROWID IN (SELECT ROWID FROM voicemail_summary ORDER BY voicemail_time ASC LIMIT "

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v8, v12

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const v8, -0xf00f253

    invoke-static {v8}, LX/03h;->a(I)V

    invoke-virtual {v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v6, 0x5eb73ae2

    invoke-static {v6}, LX/03h;->a(I)V

    goto :goto_1
.end method
