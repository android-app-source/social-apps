.class public final Lcom/facebook/rtc/models/RecentCallsDb$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:J

.field public final synthetic f:J

.field public final synthetic g:Z

.field public final synthetic h:Z

.field public final synthetic i:Z

.field public final synthetic j:Z

.field public final synthetic k:Z

.field public final synthetic l:LX/3Le;


# direct methods
.method public constructor <init>(LX/3Le;ZZLjava/lang/String;Ljava/lang/String;JJZZZZZ)V
    .locals 0

    .prologue
    .line 2096744
    iput-object p1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->l:LX/3Le;

    iput-boolean p2, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->a:Z

    iput-boolean p3, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->b:Z

    iput-object p4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->d:Ljava/lang/String;

    iput-wide p6, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->e:J

    iput-wide p8, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->f:J

    iput-boolean p10, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->g:Z

    iput-boolean p11, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->h:Z

    iput-boolean p12, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->i:Z

    iput-boolean p13, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->j:Z

    iput-boolean p14, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->k:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 2096745
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->l:LX/3Le;

    iget-object v0, v0, LX/3Le;->c:LX/3Lf;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 2096746
    iget-boolean v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->a:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 2096747
    :goto_0
    const/16 v4, 0x9

    new-array v6, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->c:Ljava/lang/String;

    if-nez v4, :cond_4

    const-string v4, ""

    :goto_1
    aput-object v4, v6, v7

    iget-object v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->d:Ljava/lang/String;

    if-nez v4, :cond_5

    const-string v4, ""

    :goto_2
    aput-object v4, v6, v3

    iget-wide v8, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->e:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v2

    iget-wide v8, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v1

    const/4 v1, 0x4

    iget-boolean v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->g:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v6, v1

    const/4 v1, 0x5

    iget-boolean v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->h:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v6, v1

    const/4 v1, 0x6

    iget-boolean v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->b:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v6, v1

    const/4 v1, 0x7

    iget-boolean v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->i:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v6, v1

    const/16 v1, 0x8

    iget-boolean v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->j:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v6, v1

    .line 2096748
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2096749
    const-string v6, "user_id"

    iget-object v1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->c:Ljava/lang/String;

    if-nez v1, :cond_6

    const-string v1, ""

    :goto_3
    invoke-virtual {v4, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096750
    const-string v6, "thread_id"

    iget-object v1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->d:Ljava/lang/String;

    if-nez v1, :cond_7

    const-string v1, ""

    :goto_4
    invoke-virtual {v4, v6, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2096751
    const-string v1, "last_call_time"

    iget-wide v6, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2096752
    const-string v1, "duration"

    iget-wide v6, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2096753
    const-string v1, "answered"

    iget-boolean v6, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->g:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2096754
    const-string v1, "direction"

    iget-boolean v6, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->h:Z

    if-eqz v6, :cond_8

    :goto_5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2096755
    const-string v1, "call_type"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2096756
    const-string v0, "acknowledged"

    iget-boolean v1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->i:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2096757
    const-string v0, "seen"

    iget-boolean v1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->j:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2096758
    const-string v0, "on_going"

    iget-boolean v1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->k:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2096759
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2096760
    const-string v0, "person_summary"

    const v1, -0x7fa3bd79

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {v5, v0, v10, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    const v2, 0x5bee07b1    # 1.33999002E17f

    invoke-static {v2}, LX/03h;->a(I)V

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 2096761
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->l:LX/3Le;

    invoke-static {v0, v10}, LX/3Le;->b(LX/3Le;LX/0Px;)V

    .line 2096762
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->l:LX/3Le;

    invoke-static {v0}, LX/3Le;->c$redex0(LX/3Le;)V

    .line 2096763
    iget-boolean v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->h:Z

    if-eqz v0, :cond_1

    .line 2096764
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->l:LX/3Le;

    iget-object v1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->c:Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 2096765
    iget-object v3, v0, LX/3Le;->d:LX/0Sh;

    const-string v4, "Recent Calls DB accessed from UI Thread"

    invoke-virtual {v3, v4}, LX/0Sh;->b(Ljava/lang/String;)V

    .line 2096766
    iget-object v3, v0, LX/3Le;->c:LX/3Lf;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2096767
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v4

    if-nez v4, :cond_a

    .line 2096768
    :cond_1
    :goto_6
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->l:LX/3Le;

    iget-object v1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->l:LX/3Le;

    invoke-static {v1}, LX/3Le;->f(LX/3Le;)I

    move-result v1

    .line 2096769
    iput v1, v0, LX/3Le;->k:I

    .line 2096770
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->l:LX/3Le;

    invoke-static {v0}, LX/3Le;->d(LX/3Le;)V

    .line 2096771
    iget-object v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->l:LX/3Le;

    .line 2096772
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2096773
    const-string v2, "com.facebook.rtc.fbwebrtc.CALL_LOG_BADGE_UPDATED"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2096774
    const-string v2, "CALL_LOG_EXTRA_UNSEEN_COUNT"

    iget v3, v0, LX/3Le;->k:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2096775
    iget-object v2, v0, LX/3Le;->j:LX/0Xl;

    invoke-interface {v2, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2096776
    :cond_2
    return-void

    .line 2096777
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->b:Z

    if-eqz v0, :cond_9

    move v0, v2

    .line 2096778
    goto/16 :goto_0

    .line 2096779
    :cond_4
    iget-object v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->c:Ljava/lang/String;

    goto/16 :goto_1

    :cond_5
    iget-object v4, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->d:Ljava/lang/String;

    goto/16 :goto_2

    .line 2096780
    :cond_6
    iget-object v1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->c:Ljava/lang/String;

    goto/16 :goto_3

    .line 2096781
    :cond_7
    iget-object v1, p0, Lcom/facebook/rtc/models/RecentCallsDb$3;->d:Ljava/lang/String;

    goto/16 :goto_4

    :cond_8
    move v2, v3

    .line 2096782
    goto/16 :goto_5

    :cond_9
    move v0, v3

    goto/16 :goto_0

    .line 2096783
    :cond_a
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2096784
    const-string v5, "seen"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2096785
    if-nez v1, :cond_b

    .line 2096786
    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "0"

    aput-object v6, v5, v2

    .line 2096787
    const-string v2, "person_summary"

    const-string v6, "seen = ?"

    invoke-virtual {v3, v2, v4, v6, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_6

    .line 2096788
    :cond_b
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "0"

    aput-object v6, v5, v2

    aput-object v1, v5, v7

    .line 2096789
    const-string v2, "person_summary"

    const-string v6, "seen = ? AND user_id = ?"

    invoke-virtual {v3, v2, v4, v6, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_6
.end method
