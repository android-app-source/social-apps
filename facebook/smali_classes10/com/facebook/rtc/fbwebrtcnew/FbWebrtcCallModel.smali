.class public Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/EEz;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/EEu;

.field public final e:LX/EEs;

.field public final f:LX/EEt;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:Z

.field public final l:Z

.field public final m:Z

.field public final n:Z

.field public final o:Z

.field public final p:J

.field public final q:J

.field public final r:LX/EEy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2094393
    new-instance v0, LX/EEr;

    invoke-direct {v0}, LX/EEr;-><init>()V

    sput-object v0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/EEv;)V
    .locals 2

    .prologue
    .line 2094394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2094395
    iget-wide v0, p1, LX/EEv;->a:J

    iput-wide v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->a:J

    .line 2094396
    iget-object v0, p1, LX/EEv;->b:LX/0Px;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->b:LX/0Px;

    .line 2094397
    iget-object v0, p1, LX/EEv;->c:LX/0P1;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->c:LX/0P1;

    .line 2094398
    iget-object v0, p1, LX/EEv;->d:LX/EEu;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->d:LX/EEu;

    .line 2094399
    iget-object v0, p1, LX/EEv;->e:LX/EEs;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->e:LX/EEs;

    .line 2094400
    iget-object v0, p1, LX/EEv;->f:LX/EEt;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->f:LX/EEt;

    .line 2094401
    iget-object v0, p1, LX/EEv;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->g:Ljava/lang/String;

    .line 2094402
    iget-object v0, p1, LX/EEv;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->h:Ljava/lang/String;

    .line 2094403
    iget-object v0, p1, LX/EEv;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->i:Ljava/lang/String;

    .line 2094404
    iget-boolean v0, p1, LX/EEv;->j:Z

    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->j:Z

    .line 2094405
    iget-boolean v0, p1, LX/EEv;->k:Z

    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->k:Z

    .line 2094406
    iget-boolean v0, p1, LX/EEv;->l:Z

    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->l:Z

    .line 2094407
    iget-boolean v0, p1, LX/EEv;->m:Z

    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->m:Z

    .line 2094408
    iget-boolean v0, p1, LX/EEv;->n:Z

    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->n:Z

    .line 2094409
    iget-boolean v0, p1, LX/EEv;->o:Z

    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->o:Z

    .line 2094410
    iget-wide v0, p1, LX/EEv;->p:J

    iput-wide v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->p:J

    .line 2094411
    iget-wide v0, p1, LX/EEv;->q:J

    iput-wide v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->q:J

    .line 2094412
    iget-object v0, p1, LX/EEv;->r:LX/EEy;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->r:LX/EEy;

    .line 2094413
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2094414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2094415
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->a:J

    .line 2094416
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->b:LX/0Px;

    .line 2094417
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0P1;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->c:LX/0P1;

    .line 2094418
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/EEu;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->d:LX/EEu;

    .line 2094419
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/EEs;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->e:LX/EEs;

    .line 2094420
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/EEt;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->f:LX/EEt;

    .line 2094421
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->g:Ljava/lang/String;

    .line 2094422
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->h:Ljava/lang/String;

    .line 2094423
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->i:Ljava/lang/String;

    .line 2094424
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->j:Z

    .line 2094425
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->k:Z

    .line 2094426
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->l:Z

    .line 2094427
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->m:Z

    .line 2094428
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->n:Z

    .line 2094429
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->o:Z

    .line 2094430
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->p:J

    .line 2094431
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->q:J

    .line 2094432
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/EEy;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->r:LX/EEy;

    .line 2094433
    return-void

    :cond_0
    move v0, v2

    .line 2094434
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2094435
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2094436
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2094437
    goto :goto_3

    :cond_4
    move v0, v2

    .line 2094438
    goto :goto_4

    :cond_5
    move v1, v2

    .line 2094439
    goto :goto_5
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2094440
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2094441
    iget-wide v4, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->a:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 2094442
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2094443
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->c:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2094444
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->d:LX/EEu;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2094445
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->e:LX/EEs;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2094446
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->f:LX/EEt;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2094447
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2094448
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2094449
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2094450
    iget-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->j:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2094451
    iget-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->k:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2094452
    iget-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->l:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2094453
    iget-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->m:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2094454
    iget-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->n:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2094455
    iget-boolean v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->o:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2094456
    iget-wide v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->p:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2094457
    iget-wide v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->q:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2094458
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtcnew/FbWebrtcCallModel;->r:LX/EEy;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2094459
    return-void

    :cond_0
    move v0, v2

    .line 2094460
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2094461
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2094462
    goto :goto_2

    :cond_3
    move v0, v2

    .line 2094463
    goto :goto_3

    :cond_4
    move v0, v2

    .line 2094464
    goto :goto_4

    :cond_5
    move v1, v2

    .line 2094465
    goto :goto_5
.end method
