.class public Lcom/facebook/rtc/views/CountdownView;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field public b:Lcom/facebook/widget/FacebookProgressCircleView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/animation/ValueAnimator;

.field private e:I

.field public f:I

.field public g:LX/EGm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2098437
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2098438
    iput-object p1, p0, Lcom/facebook/rtc/views/CountdownView;->a:Landroid/content/Context;

    .line 2098439
    invoke-direct {p0}, Lcom/facebook/rtc/views/CountdownView;->d()V

    .line 2098440
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2098433
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2098434
    iput-object p1, p0, Lcom/facebook/rtc/views/CountdownView;->a:Landroid/content/Context;

    .line 2098435
    invoke-direct {p0}, Lcom/facebook/rtc/views/CountdownView;->d()V

    .line 2098436
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 2098417
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/rtc/views/CountdownView;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->c:Landroid/widget/TextView;

    .line 2098418
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->c:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2098419
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->c:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v3, v3, v3, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 2098420
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->c:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/CountdownView;->addView(Landroid/view/View;)V

    .line 2098421
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2098422
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->c:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 2098423
    new-instance v0, Lcom/facebook/widget/FacebookProgressCircleView;

    iget-object v1, p0, Lcom/facebook/rtc/views/CountdownView;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/widget/FacebookProgressCircleView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->b:Lcom/facebook/widget/FacebookProgressCircleView;

    .line 2098424
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->b:Lcom/facebook/widget/FacebookProgressCircleView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FacebookProgressCircleView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2098425
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->b:Lcom/facebook/widget/FacebookProgressCircleView;

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/CountdownView;->addView(Landroid/view/View;)V

    .line 2098426
    const/4 v0, 0x1

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/facebook/rtc/views/CountdownView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/facebook/rtc/views/CountdownView;->e:I

    .line 2098427
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->b:Lcom/facebook/widget/FacebookProgressCircleView;

    iget v1, p0, Lcom/facebook/rtc/views/CountdownView;->e:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgressCircleStrokeWidth(I)V

    .line 2098428
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->d:Landroid/animation/ValueAnimator;

    .line 2098429
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->d:Landroid/animation/ValueAnimator;

    new-instance v1, LX/EGl;

    invoke-direct {v1, p0}, LX/EGl;-><init>(Lcom/facebook/rtc/views/CountdownView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2098430
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->d:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2098431
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/CountdownView;->setDuration(I)V

    .line 2098432
    return-void

    nop

    :array_0
    .array-data 4
        0x64
        0x0
    .end array-data
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2098415
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2098416
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2098410
    if-nez p1, :cond_1

    .line 2098411
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->g:LX/EGm;

    if-eqz v0, :cond_0

    .line 2098412
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->g:LX/EGm;

    invoke-interface {v0}, LX/EGm;->a()V

    .line 2098413
    :cond_0
    :goto_0
    return-void

    .line 2098414
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->c:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2098441
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2098442
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2098409
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    return v0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 2098404
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 2098405
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->b:Lcom/facebook/widget/FacebookProgressCircleView;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/CountdownView;->getMeasuredWidth()I

    move-result v1

    iget v2, p0, Lcom/facebook/rtc/views/CountdownView;->e:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FacebookProgressCircleView;->setSize(I)V

    .line 2098406
    invoke-virtual {p0}, Lcom/facebook/rtc/views/CountdownView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/rtc/views/CountdownView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    .line 2098407
    iget-object v1, p0, Lcom/facebook/rtc/views/CountdownView;->c:Landroid/widget/TextView;

    const/4 v2, 0x2

    int-to-float v0, v0

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v0, v3

    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2098408
    return-void
.end method

.method public setDuration(I)V
    .locals 4

    .prologue
    .line 2098401
    iput p1, p0, Lcom/facebook/rtc/views/CountdownView;->f:I

    .line 2098402
    iget-object v0, p0, Lcom/facebook/rtc/views/CountdownView;->d:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/facebook/rtc/views/CountdownView;->f:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2098403
    return-void
.end method

.method public setListener(LX/EGm;)V
    .locals 0

    .prologue
    .line 2098399
    iput-object p1, p0, Lcom/facebook/rtc/views/CountdownView;->g:LX/EGm;

    .line 2098400
    return-void
.end method
