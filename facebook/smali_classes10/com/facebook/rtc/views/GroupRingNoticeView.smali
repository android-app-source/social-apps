.class public Lcom/facebook/rtc/views/GroupRingNoticeView;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:LX/EGo;

.field private b:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/resources/ui/FbButton;

.field private e:LX/EGp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2098451
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2098452
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->e:LX/EGp;

    .line 2098453
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2098491
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2098492
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->e:LX/EGp;

    .line 2098493
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2098488
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2098489
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->e:LX/EGp;

    .line 2098490
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const-wide/16 v8, 0xc8

    const-wide/16 v6, 0x190

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 2098477
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->b:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 2098478
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 2098479
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2098480
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 2098481
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setAlpha(F)V

    .line 2098482
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    .line 2098483
    iget-object v1, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTranslationY(F)V

    .line 2098484
    iget-object v1, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->d:Lcom/facebook/resources/ui/FbButton;

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setTranslationY(F)V

    .line 2098485
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2098486
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2098487
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2098476
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->a:LX/EGo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1e3983f8

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2098473
    iget-object v1, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->a:LX/EGo;

    if-eqz v1, :cond_0

    .line 2098474
    iget-object v1, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->a:LX/EGo;

    invoke-interface {v1}, LX/EGo;->a()V

    .line 2098475
    :cond_0
    const v1, 0x3983a8a3

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x58dfdaa4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2098467
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 2098468
    const v0, 0x7f0d2aca

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 2098469
    const v0, 0x7f0d2acb

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2098470
    const v0, 0x7f0d2acc

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->d:Lcom/facebook/resources/ui/FbButton;

    .line 2098471
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098472
    const/16 v0, 0x2d

    const v2, -0x119a55d9

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setListener(LX/EGo;)V
    .locals 0

    .prologue
    .line 2098465
    iput-object p1, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->a:LX/EGo;

    .line 2098466
    return-void
.end method

.method public setModeAndShow(LX/EGp;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2098454
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->e:LX/EGp;

    if-ne v0, p1, :cond_0

    .line 2098455
    :goto_0
    return-void

    .line 2098456
    :cond_0
    sget-object v0, LX/EGn;->a:[I

    invoke-virtual {p1}, LX/EGp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2098457
    :goto_1
    iput-object p1, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->e:LX/EGp;

    .line 2098458
    invoke-virtual {p0, v3}, Lcom/facebook/rtc/views/GroupRingNoticeView;->setVisibility(I)V

    goto :goto_0

    .line 2098459
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08080e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2098460
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_1

    .line 2098461
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08080f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2098462
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->d:Lcom/facebook/resources/ui/FbButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_1

    .line 2098463
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/GroupRingNoticeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080810

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2098464
    iget-object v0, p0, Lcom/facebook/rtc/views/GroupRingNoticeView;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
