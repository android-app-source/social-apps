.class public Lcom/facebook/rtc/views/RtcGridView;
.super Landroid/widget/LinearLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Landroid/widget/LinearLayout$LayoutParams;

.field private static final c:Landroid/widget/LinearLayout$LayoutParams;


# instance fields
.field private d:Landroid/view/LayoutInflater;

.field private e:Landroid/widget/LinearLayout;

.field public f:LX/EHI;

.field private g:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/EIC;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/view/View;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 2099387
    const-class v0, Lcom/facebook/rtc/views/RtcGridView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/views/RtcGridView;->a:Ljava/lang/String;

    .line 2099388
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    sput-object v0, Lcom/facebook/rtc/views/RtcGridView;->b:Landroid/widget/LinearLayout$LayoutParams;

    .line 2099389
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v2, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    sput-object v0, Lcom/facebook/rtc/views/RtcGridView;->c:Landroid/widget/LinearLayout$LayoutParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2099405
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2099406
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->f:LX/EHI;

    .line 2099407
    invoke-direct {p0, p1}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/content/Context;)V

    .line 2099408
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2099401
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2099402
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->f:LX/EHI;

    .line 2099403
    invoke-direct {p0, p1}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/content/Context;)V

    .line 2099404
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2099397
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2099398
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->f:LX/EHI;

    .line 2099399
    invoke-direct {p0, p1}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/content/Context;)V

    .line 2099400
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2099390
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->d:Landroid/view/LayoutInflater;

    .line 2099391
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f031238

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2099392
    const v0, 0x7f0d2ac8

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcGridView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    .line 2099393
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->g:Ljava/util/LinkedHashMap;

    .line 2099394
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    .line 2099395
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v0, v1}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/widget/LinearLayout;I)V

    .line 2099396
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 2099383
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 2099384
    sget-object v0, Lcom/facebook/rtc/views/RtcGridView;->c:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2099385
    :goto_0
    return-void

    .line 2099386
    :cond_0
    sget-object v0, Lcom/facebook/rtc/views/RtcGridView;->b:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private static a(Landroid/widget/LinearLayout;I)V
    .locals 1

    .prologue
    .line 2099379
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 2099380
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2099381
    :goto_0
    return-void

    .line 2099382
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0
.end method

.method private a(Landroid/widget/LinearLayout;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2099361
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 2099362
    :cond_0
    :goto_0
    return v0

    .line 2099363
    :cond_1
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2099364
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2099365
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->getSize()I

    move-result v2

    const/4 v3, 0x2

    if-gt v2, v3, :cond_0

    .line 2099366
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2099367
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2099368
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2099369
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-le v4, v1, :cond_2

    .line 2099370
    :goto_1
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-le v4, v1, :cond_2

    .line 2099371
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcGridView;->getNewRow()Landroid/widget/LinearLayout;

    move-result-object v4

    .line 2099372
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2099373
    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2099374
    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2099375
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2099376
    iget-object v5, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 2099377
    :cond_3
    iput-object v2, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    move v0, v1

    .line 2099378
    goto :goto_0
.end method

.method private static b(Landroid/widget/LinearLayout;I)V
    .locals 1

    .prologue
    .line 2099355
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 2099356
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2099357
    sget-object v0, Lcom/facebook/rtc/views/RtcGridView;->b:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2099358
    :goto_0
    return-void

    .line 2099359
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 2099360
    sget-object v0, Lcom/facebook/rtc/views/RtcGridView;->c:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private getNewRow()Landroid/widget/LinearLayout;
    .locals 2

    .prologue
    .line 2099409
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2099410
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v0, v1}, Lcom/facebook/rtc/views/RtcGridView;->b(Landroid/widget/LinearLayout;I)V

    .line 2099411
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2099348
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->h:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2099349
    :cond_0
    :goto_0
    return-void

    .line 2099350
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2099351
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcGridView;->h:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v2

    if-ltz v2, :cond_2

    .line 2099352
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcGridView;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2099353
    invoke-direct {p0, v0}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/widget/LinearLayout;)Z

    .line 2099354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->h:Landroid/view/View;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2099339
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2099340
    :cond_0
    :goto_0
    return-void

    .line 2099341
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2099342
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EIC;

    .line 2099343
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 2099344
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v3

    if-ltz v3, :cond_2

    .line 2099345
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 2099346
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2099347
    invoke-direct {p0, v1}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/widget/LinearLayout;)Z

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2099321
    if-nez p1, :cond_0

    .line 2099322
    const/4 v0, 0x0

    .line 2099323
    :goto_0
    return v0

    .line 2099324
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    move v0, v1

    .line 2099325
    goto :goto_0

    .line 2099326
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/view/View;I)V

    .line 2099327
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2099328
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2099329
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    .line 2099330
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2099331
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcGridView;->h:Landroid/view/View;

    move v0, v1

    .line 2099332
    goto :goto_0

    .line 2099333
    :cond_2
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcGridView;->getNewRow()Landroid/widget/LinearLayout;

    move-result-object v0

    .line 2099334
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2099335
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2099336
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2099337
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcGridView;->h:Landroid/view/View;

    move v0, v1

    .line 2099338
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/EIC;)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2099292
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2099293
    :cond_0
    :goto_0
    return v3

    .line 2099294
    :cond_1
    if-eqz p2, :cond_0

    .line 2099295
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v3, v2

    .line 2099296
    goto :goto_0

    .line 2099297
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->getSize()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 2099298
    const/4 v4, 0x0

    .line 2099299
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v6, :cond_5

    .line 2099300
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcGridView;->getNewRow()Landroid/widget/LinearLayout;

    move-result-object v0

    .line 2099301
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2099302
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 2099303
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    iget-object v4, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v1, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2099304
    :cond_3
    :goto_1
    if-eqz v0, :cond_0

    .line 2099305
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p2, v1}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/view/View;I)V

    .line 2099306
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->b()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_9

    .line 2099307
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, p2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 2099308
    :goto_2
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v3, v2

    .line 2099309
    goto :goto_0

    .line 2099310
    :cond_4
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2099311
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2099312
    :cond_5
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    :goto_3
    if-ltz v5, :cond_a

    .line 2099313
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2099314
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v5, v1, :cond_7

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGridView;->b()Z

    move-result v1

    if-nez v1, :cond_7

    .line 2099315
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-gtz v1, :cond_6

    move v1, v2

    .line 2099316
    :goto_4
    if-nez v1, :cond_3

    .line 2099317
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_3

    :cond_6
    move v1, v3

    .line 2099318
    goto :goto_4

    .line 2099319
    :cond_7
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v1, v6, :cond_8

    move v1, v2

    goto :goto_4

    :cond_8
    move v1, v3

    goto :goto_4

    .line 2099320
    :cond_9
    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_a
    move-object v0, v4

    goto/16 :goto_1
.end method

.method public final b(Ljava/lang/String;)LX/EIC;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2099289
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2099290
    const/4 v0, 0x0

    .line 2099291
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EIC;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2099275
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAllRemoteViews()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LX/EIC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2099288
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getSize()I
    .locals 2

    .prologue
    .line 2099287
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->h:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .prologue
    .line 2099278
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2099279
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->e:Landroid/widget/LinearLayout;

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v0, v1}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/widget/LinearLayout;I)V

    .line 2099280
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGridView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 2099281
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v0, v1}, Lcom/facebook/rtc/views/RtcGridView;->b(Landroid/widget/LinearLayout;I)V

    .line 2099282
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2099283
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2099284
    iget v4, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v3, v4}, Lcom/facebook/rtc/views/RtcGridView;->a(Landroid/view/View;I)V

    .line 2099285
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2099286
    :cond_1
    return-void
.end method

.method public setViewChangedListener(LX/EHI;)V
    .locals 0

    .prologue
    .line 2099276
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcGridView;->f:LX/EHI;

    .line 2099277
    return-void
.end method
