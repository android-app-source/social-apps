.class public Lcom/facebook/rtc/views/RtcSpringDragView;
.super Landroid/widget/RelativeLayout;
.source ""


# static fields
.field private static final q:LX/0wT;


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/EHi;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:I

.field private g:I

.field public h:I

.field public i:LX/0wd;

.field public j:LX/0wd;

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field private r:Z

.field private s:Landroid/view/SurfaceView;

.field private t:LX/ECB;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2100399
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/views/RtcSpringDragView;->q:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 2100340
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2100341
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2100342
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->c:LX/0Ot;

    .line 2100343
    new-instance v0, LX/EHi;

    invoke-direct {v0}, LX/EHi;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    .line 2100344
    const-class v0, Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2100345
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2100346
    const v1, 0x7f0315eb

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2100347
    const v0, 0x7f0d313a

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->e:Landroid/view/View;

    .line 2100348
    return-void
.end method

.method private static a(III)I
    .locals 2

    .prologue
    .line 2100349
    sub-int v0, p0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sub-int v1, p0, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2100350
    :goto_0
    return p1

    :cond_0
    move p1, p2

    goto :goto_0
.end method

.method private a(LX/EHi;)LX/EHm;
    .locals 4

    .prologue
    .line 2100351
    new-instance v1, LX/EHm;

    invoke-direct {v1}, LX/EHm;-><init>()V

    .line 2100352
    sget-object v2, LX/EHh;->a:[I

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2100353
    iget-object v3, v0, LX/EDx;->ck:LX/EDt;

    move-object v0, v3

    .line 2100354
    invoke-virtual {v0}, LX/EDt;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    .line 2100355
    :goto_0
    return-object v1

    .line 2100356
    :pswitch_0
    iget v0, p1, LX/EHi;->a:I

    iput v0, v1, LX/EHm;->a:I

    .line 2100357
    iget v0, p1, LX/EHi;->c:I

    iget v2, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->h:I

    add-int/2addr v0, v2

    iput v0, v1, LX/EHm;->b:I

    goto :goto_0

    .line 2100358
    :pswitch_1
    iget v0, p1, LX/EHi;->b:I

    iput v0, v1, LX/EHm;->a:I

    .line 2100359
    iget v0, p1, LX/EHi;->c:I

    iput v0, v1, LX/EHm;->b:I

    goto :goto_0

    .line 2100360
    :pswitch_2
    iget v0, p1, LX/EHi;->b:I

    iput v0, v1, LX/EHm;->a:I

    .line 2100361
    iget v0, p1, LX/EHi;->d:I

    iput v0, v1, LX/EHm;->b:I

    goto :goto_0

    .line 2100362
    :pswitch_3
    iget v0, p1, LX/EHi;->a:I

    iput v0, v1, LX/EHm;->a:I

    .line 2100363
    iget v0, p1, LX/EHi;->d:I

    iput v0, v1, LX/EHm;->b:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static synthetic a(Lcom/facebook/rtc/views/RtcSpringDragView;LX/EHi;)LX/EHm;
    .locals 1

    .prologue
    .line 2100364
    invoke-direct {p0, p1}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(LX/EHi;)LX/EHm;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/rtc/views/RtcSpringDragView;LX/0Ot;LX/0wW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rtc/views/RtcSpringDragView;",
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;",
            "LX/0wW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2100365
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->c:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->a:LX/0wW;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rtc/views/RtcSpringDragView;

    const/16 v1, 0x3257

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-static {p0, v1, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(Lcom/facebook/rtc/views/RtcSpringDragView;LX/0Ot;LX/0wW;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;IIZ)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 2100400
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getBounds(Lcom/facebook/rtc/views/RtcSpringDragView;)LX/EHi;

    move-result-object v3

    .line 2100401
    new-instance v4, LX/EHm;

    invoke-direct {v4}, LX/EHm;-><init>()V

    .line 2100402
    iget v1, v3, LX/EHi;->a:I

    iget v5, v3, LX/EHi;->b:I

    invoke-static {p1, v1, v5}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(III)I

    move-result v1

    iput v1, v4, LX/EHm;->a:I

    .line 2100403
    iget v1, v3, LX/EHi;->c:I

    iget v5, v3, LX/EHi;->d:I

    invoke-static {p2, v1, v5}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(III)I

    move-result v1

    iput v1, v4, LX/EHm;->b:I

    .line 2100404
    iget v1, v4, LX/EHm;->a:I

    iget v5, v3, LX/EHi;->a:I

    if-ne v1, v5, :cond_2

    move v1, v0

    .line 2100405
    :goto_0
    iget v4, v4, LX/EHm;->b:I

    iget v3, v3, LX/EHi;->c:I

    if-ne v4, v3, :cond_0

    move v2, v0

    .line 2100406
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-static {v1, v2}, LX/EDt;->getCorner(ZZ)LX/EDt;

    move-result-object v1

    .line 2100407
    iput-object v1, v0, LX/EDx;->ck:LX/EDt;

    .line 2100408
    if-eqz p3, :cond_1

    .line 2100409
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->e$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    .line 2100410
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 2100411
    goto :goto_0
.end method

.method private b(F)V
    .locals 4

    .prologue
    .line 2100366
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->i:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 2100367
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->j:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 2100368
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getScaleX()F

    move-result v0

    .line 2100369
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput v0, v1, v2

    const/4 v0, 0x1

    aput p1, v1, v0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 2100370
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2100371
    new-instance v1, LX/EHf;

    invoke-direct {v1, p0}, LX/EHf;-><init>(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2100372
    new-instance v1, LX/EHg;

    invoke-direct {v1, p0}, LX/EHg;-><init>(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2100373
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2100374
    return-void
.end method

.method public static b(Lcom/facebook/rtc/views/RtcSpringDragView;II)V
    .locals 2

    .prologue
    const/16 v1, -0xfa

    .line 2100375
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->t:LX/ECB;

    invoke-interface {v0, p1, p2, v1, v1}, LX/ECB;->a(IIII)V

    .line 2100376
    return-void
.end method

.method public static e$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;)V
    .locals 6

    .prologue
    .line 2100377
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2100378
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getBounds(Lcom/facebook/rtc/views/RtcSpringDragView;)LX/EHi;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(LX/EHi;)LX/EHm;

    move-result-object v1

    .line 2100379
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 2100380
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->i:LX/0wd;

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    int-to-double v4, v3

    invoke-virtual {v2, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 2100381
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->i:LX/0wd;

    iget v3, v1, LX/EHm;->a:I

    int-to-double v4, v3

    invoke-virtual {v2, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 2100382
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->j:LX/0wd;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    int-to-double v4, v0

    invoke-virtual {v2, v4, v5}, LX/0wd;->a(D)LX/0wd;

    .line 2100383
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->j:LX/0wd;

    iget v1, v1, LX/EHm;->b:I

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 2100384
    :goto_0
    return-void

    .line 2100385
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->i:LX/0wd;

    iget v2, v1, LX/EHm;->a:I

    int-to-double v2, v2

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 2100386
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->j:LX/0wd;

    iget v1, v1, LX/EHm;->b:I

    int-to-double v2, v1

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    goto :goto_0
.end method

.method public static f(Lcom/facebook/rtc/views/RtcSpringDragView;)V
    .locals 2

    .prologue
    .line 2100387
    iget v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->o:I

    iget v1, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->p:I

    invoke-static {p0, v0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->b(Lcom/facebook/rtc/views/RtcSpringDragView;II)V

    .line 2100388
    return-void
.end method

.method public static getBounds(Lcom/facebook/rtc/views/RtcSpringDragView;)LX/EHi;
    .locals 4

    .prologue
    .line 2100389
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 2100390
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    .line 2100391
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getWidth()I

    move-result v2

    sub-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getScaleX()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v0, v2

    .line 2100392
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getHeight()I

    move-result v2

    sub-int/2addr v2, v1

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getScaleY()F

    move-result v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    .line 2100393
    new-instance v2, LX/EHi;

    invoke-direct {v2}, LX/EHi;-><init>()V

    .line 2100394
    iget-object v3, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iget v3, v3, LX/EHi;->c:I

    iput v3, v2, LX/EHi;->c:I

    .line 2100395
    iget v3, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->g:I

    sub-int v1, v3, v1

    iget-object v3, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iget v3, v3, LX/EHi;->d:I

    sub-int/2addr v1, v3

    iput v1, v2, LX/EHi;->d:I

    .line 2100396
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iget v1, v1, LX/EHi;->a:I

    iput v1, v2, LX/EHi;->a:I

    .line 2100397
    iget v1, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->f:I

    sub-int v0, v1, v0

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iget v1, v1, LX/EHi;->b:I

    sub-int/2addr v0, v1

    iput v0, v2, LX/EHi;->b:I

    .line 2100398
    return-object v2
.end method

.method public static setNearestCorner(Lcom/facebook/rtc/views/RtcSpringDragView;Z)V
    .locals 2

    .prologue
    .line 2100335
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2100336
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-static {p0, v1, v0, p1}, Lcom/facebook/rtc/views/RtcSpringDragView;->a$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;IIZ)V

    .line 2100337
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2100338
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2100339
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 2100255
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getScaleX()F

    move-result v0

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 2100256
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 2100257
    invoke-virtual {p0, p1}, Lcom/facebook/rtc/views/RtcSpringDragView;->setScaleX(F)V

    .line 2100258
    invoke-virtual {p0, p1}, Lcom/facebook/rtc/views/RtcSpringDragView;->setScaleY(F)V

    .line 2100259
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->e$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    .line 2100260
    :cond_0
    :goto_0
    return-void

    .line 2100261
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/rtc/views/RtcSpringDragView;->b(F)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 2100331
    iput p1, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->f:I

    .line 2100332
    iput p2, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->g:I

    .line 2100333
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->e$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    .line 2100334
    return-void
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 2100262
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iget v0, v0, LX/EHi;->a:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iget v0, v0, LX/EHi;->b:I

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iget v0, v0, LX/EHi;->c:I

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iget v0, v0, LX/EHi;->d:I

    if-eq v0, p4, :cond_1

    .line 2100263
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iput p1, v0, LX/EHi;->a:I

    .line 2100264
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iput p2, v0, LX/EHi;->b:I

    .line 2100265
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iput p3, v0, LX/EHi;->c:I

    .line 2100266
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->b:LX/EHi;

    iput p4, v0, LX/EHi;->d:I

    .line 2100267
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->e$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    .line 2100268
    :cond_1
    return-void
.end method

.method public final a(IILandroid/view/View;)V
    .locals 2

    .prologue
    .line 2100269
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2100270
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 2100271
    invoke-virtual {p0, p1, p2}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(II)V

    .line 2100272
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2100273
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->removeView(Landroid/view/View;)V

    .line 2100274
    :cond_0
    iput-object p3, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    .line 2100275
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->addView(Landroid/view/View;I)V

    .line 2100276
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->s:Landroid/view/SurfaceView;

    if-eqz v0, :cond_1

    .line 2100277
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->s:Landroid/view/SurfaceView;

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->removeView(Landroid/view/View;)V

    .line 2100278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->s:Landroid/view/SurfaceView;

    .line 2100279
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->r:Z

    .line 2100280
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->setPivotX(F)V

    .line 2100281
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getPaddingTop()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->setPivotY(F)V

    .line 2100282
    return-void
.end method

.method public final a(LX/ECB;)V
    .locals 6

    .prologue
    const-wide v2, 0x3fd3333333333333L    # 0.3

    .line 2100283
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/rtc/views/RtcSpringDragView;->q:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 2100284
    iput-wide v2, v0, LX/0wd;->k:D

    .line 2100285
    move-object v0, v0

    .line 2100286
    iput-wide v2, v0, LX/0wd;->l:D

    .line 2100287
    move-object v0, v0

    .line 2100288
    new-instance v1, LX/EHl;

    invoke-direct {v1, p0}, LX/EHl;-><init>(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->i:LX/0wd;

    .line 2100289
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/rtc/views/RtcSpringDragView;->q:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 2100290
    iput-wide v2, v0, LX/0wd;->k:D

    .line 2100291
    move-object v0, v0

    .line 2100292
    iput-wide v2, v0, LX/0wd;->l:D

    .line 2100293
    move-object v0, v0

    .line 2100294
    new-instance v1, LX/EHl;

    invoke-direct {v1, p0}, LX/EHl;-><init>(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->j:LX/0wd;

    .line 2100295
    if-nez p1, :cond_0

    .line 2100296
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must pass a valid marginChangedCallback to support dragging behavior"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2100297
    :cond_0
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->t:LX/ECB;

    .line 2100298
    new-instance v0, LX/EHj;

    invoke-direct {v0, p0}, LX/EHj;-><init>(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2100299
    return-void
.end method

.method public final a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 2100300
    iget-boolean v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2100301
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    if-eq v0, p1, :cond_0

    .line 2100302
    :goto_0
    return-void

    .line 2100303
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/rtc/views/RtcSpringDragView;->removeView(Landroid/view/View;)V

    .line 2100304
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2100305
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(F)V

    .line 2100306
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2100307
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2100308
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2100309
    :goto_0
    return-void

    .line 2100310
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public getChildView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2100311
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 2100312
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2100313
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->e$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    .line 2100314
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 2100315
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 2100316
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2100317
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 2100318
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 2100319
    iget v2, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->f:I

    if-ne v2, v1, :cond_0

    iget v2, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->g:I

    if-eq v2, v0, :cond_1

    .line 2100320
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(II)V

    .line 2100321
    :cond_1
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x3b81f935

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2100322
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 2100323
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->e$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    .line 2100324
    const/16 v1, 0x2d

    const v2, 0x2acde483

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setTopLeftOffset(I)V
    .locals 0

    .prologue
    .line 2100325
    iput p1, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->h:I

    .line 2100326
    return-void
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 2100327
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2100328
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2100329
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSpringDragView;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2100330
    :cond_0
    return-void
.end method
