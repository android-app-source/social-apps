.class public Lcom/facebook/rtc/views/RtcIncomingCallButtons;
.super LX/EH2;
.source ""


# instance fields
.field public a:LX/00H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/fbui/glyph/GlyphButton;

.field private c:Lcom/facebook/fbui/glyph/GlyphButton;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field public g:LX/EBm;

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2099515
    invoke-direct {p0, p1, p2}, LX/EH2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2099516
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2099517
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->h:LX/0Ot;

    .line 2099518
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->c()V

    .line 2099519
    return-void
.end method

.method private static a(Lcom/facebook/rtc/views/RtcIncomingCallButtons;LX/00H;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rtc/views/RtcIncomingCallButtons;",
            "LX/00H;",
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2099520
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->a:LX/00H;

    iput-object p2, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->h:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;

    const-class v0, LX/00H;

    invoke-interface {v1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/00H;

    const/16 v2, 0x3257

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->a(Lcom/facebook/rtc/views/RtcIncomingCallButtons;LX/00H;LX/0Ot;)V

    return-void
.end method

.method public static b(Lcom/facebook/rtc/views/RtcIncomingCallButtons;)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2099521
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2099522
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->c:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2099523
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2099524
    :goto_0
    return-void

    .line 2099525
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->c:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2099526
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2099527
    const-class v0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2099528
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2099529
    const v1, 0x7f03123d

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2099530
    const v0, 0x7f0d2ad1

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->e:Landroid/view/View;

    .line 2099531
    const v0, 0x7f0d2ad2

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->f:Landroid/view/View;

    .line 2099532
    const v0, 0x7f0d2ad3

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->d:Landroid/view/View;

    .line 2099533
    const v0, 0x7f0d2ad4

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2099534
    const v0, 0x7f0d2ad5

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->c:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2099535
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EHO;

    invoke-direct {v1, p0}, LX/EHO;-><init>(Lcom/facebook/rtc/views/RtcIncomingCallButtons;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099536
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->c:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EHP;

    invoke-direct {v1, p0}, LX/EHP;-><init>(Lcom/facebook/rtc/views/RtcIncomingCallButtons;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099537
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->d:Landroid/view/View;

    new-instance v1, LX/EHQ;

    invoke-direct {v1, p0}, LX/EHQ;-><init>(Lcom/facebook/rtc/views/RtcIncomingCallButtons;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099538
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->d()V

    .line 2099539
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->b(Lcom/facebook/rtc/views/RtcIncomingCallButtons;)V

    .line 2099540
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2099541
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->e:Landroid/view/View;

    new-instance v1, LX/EHR;

    invoke-direct {v1, p0}, LX/EHR;-><init>(Lcom/facebook/rtc/views/RtcIncomingCallButtons;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099542
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->f:Landroid/view/View;

    new-instance v1, LX/EHS;

    invoke-direct {v1, p0}, LX/EHS;-><init>(Lcom/facebook/rtc/views/RtcIncomingCallButtons;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099543
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2099544
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2099545
    return-void
.end method


# virtual methods
.method public setButtonsEnabled(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x0

    .line 2099546
    new-array v1, v4, [Landroid/view/View;

    iget-object v2, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    aput-object v2, v1, v0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->c:Lcom/facebook/fbui/glyph/GlyphButton;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->d:Landroid/view/View;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->e:Landroid/view/View;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->f:Landroid/view/View;

    aput-object v3, v1, v2

    .line 2099547
    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v2, v1, v0

    .line 2099548
    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 2099549
    invoke-static {v2, p1}, LX/EH2;->a(Landroid/view/View;Z)V

    .line 2099550
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2099551
    :cond_0
    return-void
.end method

.method public setListener(LX/EBm;)V
    .locals 0

    .prologue
    .line 2099552
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->g:LX/EBm;

    .line 2099553
    return-void
.end method
