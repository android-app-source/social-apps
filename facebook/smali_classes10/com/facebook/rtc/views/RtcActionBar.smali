.class public Lcom/facebook/rtc/views/RtcActionBar;
.super Landroid/widget/RelativeLayout;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/EBv;

.field private C:LX/EC2;

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:Z

.field public f:Landroid/widget/LinearLayout;

.field private g:Lcom/facebook/resources/ui/FbTextView;

.field private h:Landroid/widget/ImageView;

.field private i:I

.field private j:I

.field private k:F

.field private l:I

.field private m:I

.field private n:I

.field private o:LX/EHD;

.field private p:Lcom/facebook/fbui/glyph/GlyphButton;

.field private q:Lcom/facebook/fbui/glyph/GlyphButton;

.field private r:Lcom/facebook/fbui/glyph/GlyphButton;

.field private s:Lcom/facebook/fbui/glyph/GlyphButton;

.field private t:Lcom/facebook/fbui/glyph/GlyphButton;

.field private u:Landroid/widget/ImageView;

.field private v:Lcom/facebook/resources/ui/FbTextView;

.field private w:Z

.field private x:LX/0hs;

.field private y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2098884
    const-class v0, Lcom/facebook/rtc/views/RtcActionBar;

    sput-object v0, Lcom/facebook/rtc/views/RtcActionBar;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2098944
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2098945
    sget-object v0, LX/EHD;->VOICE:LX/EHD;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    .line 2098946
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2098947
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    .line 2098948
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->RtcActionBar:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2098949
    :try_start_0
    const/16 v0, 0x0

    const v2, 0x7f021773

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->i:I

    .line 2098950
    const/16 v0, 0x6

    const v2, 0x7f020f67

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->j:I

    .line 2098951
    const/16 v0, 0x7

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->k:F

    .line 2098952
    const/16 v0, 0x1

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0194

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->l:I

    .line 2098953
    const/16 v0, 0x2

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0190

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->m:I

    .line 2098954
    const/16 v0, 0x3

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a02f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->c:I

    .line 2098955
    const/16 v0, 0x4

    const v2, 0x7f021781

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->d:I

    .line 2098956
    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->e:Z

    .line 2098957
    const/16 v0, 0x5

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->n:I

    .line 2098958
    const/16 v0, 0x9

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 2098959
    invoke-static {}, LX/EHD;->values()[LX/EHD;

    move-result-object v2

    aget-object v0, v2, v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2098960
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2098961
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2098962
    const v1, 0x7f031231

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2098963
    const v0, 0x7f0d2aa4

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->f:Landroid/widget/LinearLayout;

    .line 2098964
    const v0, 0x7f0d2aa6

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 2098965
    const v0, 0x7f0d2aa5

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->h:Landroid/widget/ImageView;

    .line 2098966
    const v0, 0x7f0d2aa2

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->p:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2098967
    const v0, 0x7f0d2aa1

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->s:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2098968
    const v0, 0x7f0d2aa3

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->u:Landroid/widget/ImageView;

    .line 2098969
    const v0, 0x7f0d2a9f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2098970
    const v0, 0x7f0d2aa7

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->v:Lcom/facebook/resources/ui/FbTextView;

    .line 2098971
    const v0, 0x7f0d2a9e

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->q:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2098972
    const v0, 0x7f0d2aa0

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->r:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 2098973
    const-class v0, Lcom/facebook/rtc/views/RtcActionBar;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcActionBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2098974
    return-void

    .line 2098975
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private static a(Lcom/facebook/rtc/views/RtcActionBar;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rtc/views/RtcActionBar;",
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2098943
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/rtc/views/RtcActionBar;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p3, p0, Lcom/facebook/rtc/views/RtcActionBar;->A:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/RtcActionBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/rtc/views/RtcActionBar;

    const/16 v0, 0x1564

    invoke-static {v1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->b:LX/0Or;

    const/16 v0, 0x3257

    invoke-static {v1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0, v2, v0, v1}, Lcom/facebook/rtc/views/RtcActionBar;->a(Lcom/facebook/rtc/views/RtcActionBar;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0ad;)V

    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 2098916
    const-class v0, Lcom/facebook/rtc/views/RtcActionBar;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcActionBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2098917
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcActionBar;->setType(LX/EHD;)V

    .line 2098918
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->f:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 2098919
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->f:Landroid/widget/LinearLayout;

    new-instance v1, LX/EH5;

    invoke-direct {v1, p0}, LX/EH5;-><init>(Lcom/facebook/rtc/views/RtcActionBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098920
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->g:Lcom/facebook/resources/ui/FbTextView;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->l:I

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2098921
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->h:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/facebook/rtc/views/RtcActionBar;->j:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2098922
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->h:Landroid/widget/ImageView;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->k:F

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 2098923
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->v:Lcom/facebook/resources/ui/FbTextView;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->m:I

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2098924
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcActionBar;->n(Lcom/facebook/rtc/views/RtcActionBar;)V

    .line 2098925
    new-instance v0, LX/EH6;

    invoke-direct {v0, p0}, LX/EH6;-><init>(Lcom/facebook/rtc/views/RtcActionBar;)V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->C:LX/EC2;

    .line 2098926
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->C:LX/EC2;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EC2;)V

    .line 2098927
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->p:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 2098928
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->p:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setBackgroundResource(I)V

    .line 2098929
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->p:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EH7;

    invoke-direct {v1, p0}, LX/EH7;-><init>(Lcom/facebook/rtc/views/RtcActionBar;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098930
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->s:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 2098931
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->s:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setBackgroundResource(I)V

    .line 2098932
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->s:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EH8;

    invoke-direct {v1, p0}, LX/EH8;-><init>(Lcom/facebook/rtc/views/RtcActionBar;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098933
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->r:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 2098934
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->r:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setBackgroundResource(I)V

    .line 2098935
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->r:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EH9;

    invoke-direct {v1, p0}, LX/EH9;-><init>(Lcom/facebook/rtc/views/RtcActionBar;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098936
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2098937
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->u:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2098938
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->q:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->n:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 2098939
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->q:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EHA;

    invoke-direct {v1, p0}, LX/EHA;-><init>(Lcom/facebook/rtc/views/RtcActionBar;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098940
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->n:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(I)V

    .line 2098941
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/EHB;

    invoke-direct {v1, p0}, LX/EHB;-><init>(Lcom/facebook/rtc/views/RtcActionBar;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2098942
    return-void
.end method

.method private j()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2098913
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v1, LX/EHD;->VIDEO:LX/EHD;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v1, LX/EHD;->VIDEO_CONFERENCE:LX/EHD;

    if-ne v0, v1, :cond_4

    :cond_0
    move v1, v3

    .line 2098914
    :goto_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-nez v0, :cond_3

    if-nez v1, :cond_3

    :cond_2
    move v2, v3

    :cond_3
    return v2

    :cond_4
    move v1, v2

    .line 2098915
    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    .line 2098899
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/EDJ;->i:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098900
    iget v1, v0, LX/EDx;->am:I

    move v0, v1

    .line 2098901
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcActionBar;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    .line 2098902
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 2098903
    if-eqz v0, :cond_1

    .line 2098904
    :cond_0
    :goto_0
    return-void

    .line 2098905
    :cond_1
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    .line 2098906
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    const/16 v1, 0x1f40

    .line 2098907
    iput v1, v0, LX/0hs;->t:I

    .line 2098908
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    const v1, 0x7f080804

    invoke-virtual {v0, v1}, LX/0hs;->a(I)V

    .line 2098909
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    const v1, 0x7f080805

    invoke-virtual {v0, v1}, LX/0hs;->b(I)V

    .line 2098910
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->s:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 2098911
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2098912
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/EDJ;->i:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 2098886
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/EDJ;->j:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    .line 2098887
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->r:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1}, Lcom/facebook/fbui/glyph/GlyphButton;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcActionBar;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    .line 2098888
    iget-boolean v2, v1, LX/0ht;->r:Z

    move v1, v2

    .line 2098889
    if-nez v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->w:Z

    if-nez v1, :cond_1

    .line 2098890
    new-instance v1, LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    .line 2098891
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    const/16 v2, 0x1f40

    .line 2098892
    iput v2, v1, LX/0hs;->t:I

    .line 2098893
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    const v2, 0x7f080806

    invoke-virtual {v1, v2}, LX/0hs;->a(I)V

    .line 2098894
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    iget-object v2, p0, Lcom/facebook/rtc/views/RtcActionBar;->r:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v1, v2}, LX/0ht;->c(Landroid/view/View;)V

    .line 2098895
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    invoke-virtual {v1}, LX/0ht;->d()V

    .line 2098896
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->w:Z

    .line 2098897
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/EDJ;->j:LX/0Tn;

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2098898
    :cond_1
    return-void
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 2098885
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v1, LX/EHD;->AUDIO_CONFERENCE:LX/EHD;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v1, LX/EHD;->VIDEO_CONFERENCE:LX/EHD;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v1, LX/EHD;->ROSTER_CONFERENCE:LX/EHD;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Lcom/facebook/rtc/views/RtcActionBar;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2098867
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2098868
    :cond_0
    :goto_0
    return-void

    .line 2098869
    :cond_1
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcActionBar;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2098870
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2098871
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->g:Lcom/facebook/resources/ui/FbTextView;

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aA()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2098872
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2098873
    const-string v1, ""

    .line 2098874
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2098875
    const-string v0, "."

    move-object v1, v0

    .line 2098876
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098877
    iget-boolean v2, v0, LX/EDx;->cc:Z

    move v0, v2

    .line 2098878
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v2, LX/EHD;->VIDEO:LX/EHD;

    if-ne v0, v2, :cond_4

    .line 2098879
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2098880
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v2, LX/EHD;->VIDEO:LX/EHD;

    if-ne v0, v2, :cond_5

    .line 2098881
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcActionBar;->g:Lcom/facebook/resources/ui/FbTextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ay()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2098882
    :cond_5
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v2, LX/EHD;->VOICE:LX/EHD;

    if-ne v0, v2, :cond_0

    .line 2098883
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->g:Lcom/facebook/resources/ui/FbTextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080796

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2098865
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->C:LX/EC2;

    invoke-virtual {v0, v1}, LX/EDx;->b(LX/EC2;)V

    .line 2098866
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2098976
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcActionBar;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2098977
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->v:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2098978
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->v:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2098979
    :goto_0
    return-void

    .line 2098980
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->v:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2098798
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aI()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->e:Z

    if-eqz v0, :cond_2

    .line 2098799
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2098800
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->p:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021a86

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2098801
    :goto_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->p:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2098802
    :goto_1
    return-void

    .line 2098803
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2098804
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->p:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021a8d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2098805
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->p:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021a8b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2098806
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->p:Lcom/facebook/fbui/glyph/GlyphButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2098812
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v1, LX/EHD;->ROSTER_CONFERENCE:LX/EHD;

    if-eq v0, v1, :cond_0

    .line 2098813
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->q:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2098814
    :goto_0
    return-void

    .line 2098815
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->q:Lcom/facebook/fbui/glyph/GlyphButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2098816
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098817
    iget v1, v0, LX/EDx;->am:I

    move v0, v1

    .line 2098818
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2098819
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->A:LX/0ad;

    sget-short v1, LX/3Dx;->ae:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2098820
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->s:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2098821
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcActionBar;->k()V

    .line 2098822
    :cond_0
    :goto_0
    return-void

    .line 2098823
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->s:Lcom/facebook/fbui/glyph/GlyphButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 2098807
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bk()I

    move-result v1

    .line 2098808
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->br()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v2, LX/EHD;->ROSTER_CONFERENCE:LX/EHD;

    if-eq v0, v2, :cond_0

    const/4 v0, 0x3

    if-lt v1, v0, :cond_0

    const/4 v0, 0x4

    if-gt v1, v0, :cond_0

    .line 2098809
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2098810
    :goto_0
    return-void

    .line 2098811
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2098824
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->N()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    sget-object v1, LX/EHD;->ROSTER_CONFERENCE:LX/EHD;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aZ()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->af()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2098825
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->W()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2098826
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->r:Lcom/facebook/fbui/glyph/GlyphButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2098827
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcActionBar;->l()V

    .line 2098828
    :cond_1
    :goto_0
    return-void

    .line 2098829
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->r:Lcom/facebook/fbui/glyph/GlyphButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2098830
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    if-eqz v0, :cond_0

    .line 2098831
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2098832
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->x:LX/0hs;

    .line 2098833
    :cond_0
    return-void
.end method

.method public getMinimizeButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 2098834
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->f:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 2098835
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2098836
    :goto_0
    return-void

    .line 2098837
    :cond_0
    sget-object v1, LX/EHC;->a:[I

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2098838
    iget-object v2, v0, LX/EDx;->cm:LX/EDr;

    move-object v0, v2

    .line 2098839
    invoke-virtual {v0}, LX/EDr;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2098840
    sget-object v0, Lcom/facebook/rtc/views/RtcActionBar;->a:Ljava/lang/Class;

    const-string v1, "Unknown connection quality type"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 2098841
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021832

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2098842
    :goto_1
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcActionBar;->u:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2098843
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021835

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 2098844
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021833

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 2098845
    :pswitch_3
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f021834

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x774d41ef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2098846
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 2098847
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcActionBar;->i()V

    .line 2098848
    const/16 v1, 0x2d

    const v2, 0x6f33dbc4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setListener(LX/EBv;)V
    .locals 0

    .prologue
    .line 2098849
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcActionBar;->B:LX/EBv;

    .line 2098850
    return-void
.end method

.method public setMultiViewModeSwitchMode(LX/EIT;)V
    .locals 2

    .prologue
    .line 2098851
    sget-object v0, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    if-ne p1, v0, :cond_1

    .line 2098852
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    const v1, 0x7f020d03

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    .line 2098853
    :cond_0
    :goto_0
    return-void

    .line 2098854
    :cond_1
    sget-object v0, LX/EIT;->GRID_VIEW:LX/EIT;

    if-ne p1, v0, :cond_0

    .line 2098855
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->t:Lcom/facebook/fbui/glyph/GlyphButton;

    const v1, 0x7f020cea

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setImageResource(I)V

    goto :goto_0
.end method

.method public setRosterButtonVisibility(I)V
    .locals 1

    .prologue
    .line 2098856
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->q:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphButton;->setVisibility(I)V

    .line 2098857
    return-void
.end method

.method public setType(LX/EHD;)V
    .locals 1

    .prologue
    .line 2098858
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcActionBar;->o:LX/EHD;

    .line 2098859
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcActionBar;->n(Lcom/facebook/rtc/views/RtcActionBar;)V

    .line 2098860
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcActionBar;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ac()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcActionBar;->a(Ljava/lang/String;)V

    .line 2098861
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->c()V

    .line 2098862
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->e()V

    .line 2098863
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcActionBar;->f()V

    .line 2098864
    return-void
.end method
