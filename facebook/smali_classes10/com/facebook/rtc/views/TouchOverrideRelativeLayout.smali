.class public Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;
.super Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;
.source ""

# interfaces
.implements LX/EGk;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public b:Z

.field private c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2S7;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/2EJ;

.field public f:LX/EBo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/rtc/views/ChildLockBanner;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/facebook/rtc/views/ChildLockBanner;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2101377
    const-class v0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2101369
    invoke-direct {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 2101370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    .line 2101371
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101372
    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->c:LX/0Ot;

    .line 2101373
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101374
    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->d:LX/0Ot;

    .line 2101375
    invoke-direct {p0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->f()V

    .line 2101376
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2101361
    invoke-direct {p0, p1, p2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2101362
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    .line 2101363
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101364
    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->c:LX/0Ot;

    .line 2101365
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101366
    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->d:LX/0Ot;

    .line 2101367
    invoke-direct {p0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->f()V

    .line 2101368
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2101353
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2101354
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    .line 2101355
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101356
    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->c:LX/0Ot;

    .line 2101357
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101358
    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->d:LX/0Ot;

    .line 2101359
    invoke-direct {p0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->f()V

    .line 2101360
    return-void
.end method

.method private static a(Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;",
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2S7;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2101352
    iput-object p1, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->c:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->d:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    const/16 v1, 0x3257

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x1111

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->a(Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 2101349
    const-class v0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2101350
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->g:Ljava/util/ArrayList;

    .line 2101351
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 2101378
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->e:LX/2EJ;

    if-nez v0, :cond_0

    .line 2101379
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080802

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080803

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    new-instance v2, LX/EIF;

    invoke-direct {v2, p0}, LX/EIF;-><init>(Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->e:LX/2EJ;

    .line 2101380
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2S7;

    const-string v1, "cl_clicked"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Z)V

    .line 2101381
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->e:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2101382
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2101346
    invoke-virtual {p0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->d()V

    .line 2101347
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2S7;

    const-string v1, "cl_disabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Z)V

    .line 2101348
    return-void
.end method

.method public final a(Lcom/facebook/rtc/views/ChildLockBanner;)V
    .locals 1

    .prologue
    .line 2101290
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2101291
    iput-object p0, p1, Lcom/facebook/rtc/views/ChildLockBanner;->c:LX/EGk;

    .line 2101292
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2101339
    iget-boolean v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    if-eqz v0, :cond_0

    .line 2101340
    :goto_0
    return-void

    .line 2101341
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101342
    iget v1, v0, LX/EDx;->am:I

    move v0, v1

    .line 2101343
    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 2101344
    sget-object v0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->a:Ljava/lang/String;

    const-string v1, "Can only block touch events in an established call"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2101345
    :cond_1
    invoke-direct {p0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->g()V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2101329
    iget-boolean v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101330
    iget v1, v0, LX/EDx;->am:I

    move v0, v1

    .line 2101331
    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 2101332
    :cond_0
    :goto_0
    return-void

    .line 2101333
    :cond_1
    iput-boolean v2, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    .line 2101334
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101335
    iput-boolean v2, v0, LX/EDx;->cf:Z

    .line 2101336
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->f:LX/EBo;

    if-eqz v0, :cond_2

    .line 2101337
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->f:LX/EBo;

    invoke-interface {v0}, LX/EBo;->a()V

    .line 2101338
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2S7;

    const-string v1, "cl_enabled"

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2101322
    iget-boolean v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    if-eqz v0, :cond_0

    .line 2101323
    iput-boolean v1, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    .line 2101324
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101325
    iput-boolean v1, v0, LX/EDx;->cf:Z

    .line 2101326
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->f:LX/EBo;

    if-eqz v0, :cond_0

    .line 2101327
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->f:LX/EBo;

    invoke-interface {v0}, LX/EBo;->b()V

    .line 2101328
    :cond_0
    return-void
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 2101319
    iget-boolean v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    if-nez v0, :cond_0

    .line 2101320
    invoke-super {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 2101321
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 2101298
    iget-boolean v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    if-nez v0, :cond_0

    .line 2101299
    invoke-super {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2101300
    :goto_0
    return v0

    .line 2101301
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2101302
    if-nez v0, :cond_2

    .line 2101303
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    .line 2101304
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    .line 2101305
    const/4 v0, 0x2

    new-array v6, v0, [I

    .line 2101306
    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v2, v3

    :goto_1
    if-ge v2, v7, :cond_4

    iget-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/ChildLockBanner;

    .line 2101307
    invoke-virtual {v0}, Lcom/facebook/rtc/views/ChildLockBanner;->isShown()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2101308
    invoke-virtual {v0, v6}, Lcom/facebook/rtc/views/ChildLockBanner;->getLocationOnScreen([I)V

    .line 2101309
    aget v8, v6, v3

    int-to-float v8, v8

    cmpl-float v8, v4, v8

    if-ltz v8, :cond_1

    aget v8, v6, v3

    invoke-virtual {v0}, Lcom/facebook/rtc/views/ChildLockBanner;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    int-to-float v8, v8

    cmpg-float v8, v4, v8

    if-gtz v8, :cond_1

    aget v8, v6, v1

    int-to-float v8, v8

    cmpl-float v8, v5, v8

    if-ltz v8, :cond_1

    aget v8, v6, v1

    invoke-virtual {v0}, Lcom/facebook/rtc/views/ChildLockBanner;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    int-to-float v8, v8

    cmpg-float v8, v5, v8

    if-gtz v8, :cond_1

    .line 2101310
    invoke-virtual {v0, p1}, Lcom/facebook/rtc/views/ChildLockBanner;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2101311
    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->h:Lcom/facebook/rtc/views/ChildLockBanner;

    move v0, v1

    .line 2101312
    goto :goto_0

    .line 2101313
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2101314
    :cond_2
    iget-object v2, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->h:Lcom/facebook/rtc/views/ChildLockBanner;

    if-eqz v2, :cond_4

    .line 2101315
    iget-object v2, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->h:Lcom/facebook/rtc/views/ChildLockBanner;

    invoke-virtual {v2, p1}, Lcom/facebook/rtc/views/ChildLockBanner;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2101316
    if-eq v0, v1, :cond_3

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4

    .line 2101317
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->h:Lcom/facebook/rtc/views/ChildLockBanner;

    :cond_4
    move v0, v1

    .line 2101318
    goto :goto_0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2101295
    iget-boolean v0, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    if-nez v0, :cond_0

    .line 2101296
    invoke-super {p0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2101297
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setChildLockListener(LX/EBo;)V
    .locals 0

    .prologue
    .line 2101293
    iput-object p1, p0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->f:LX/EBo;

    .line 2101294
    return-void
.end method
