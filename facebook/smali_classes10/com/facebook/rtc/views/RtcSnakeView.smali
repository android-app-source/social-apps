.class public Lcom/facebook/rtc/views/RtcSnakeView;
.super Landroid/view/View;
.source ""


# static fields
.field private static final k:[I


# instance fields
.field private a:LX/EHe;

.field private b:Ljava/nio/ByteBuffer;

.field private c:LX/3rW;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Paint;

.field private j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/os/Handler;

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2100168
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/rtc/views/RtcSnakeView;->k:[I

    return-void

    nop

    :array_0
    .array-data 4
        -0x10000
        -0xffff01
        -0xff0100
        -0xff01
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2099974
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2099975
    sget-object v0, LX/EHe;->NOT_START:LX/EHe;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    .line 2099976
    iput-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->b:Ljava/nio/ByteBuffer;

    .line 2099977
    iput-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->c:LX/3rW;

    .line 2099978
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->d:Landroid/graphics/Paint;

    .line 2099979
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->e:Landroid/graphics/Paint;

    .line 2099980
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->f:Landroid/graphics/Paint;

    .line 2099981
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->g:Landroid/graphics/Paint;

    .line 2099982
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    .line 2099983
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->i:Landroid/graphics/Paint;

    .line 2099984
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->j:Ljava/util/HashMap;

    .line 2099985
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->l:Landroid/os/Handler;

    .line 2099986
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2099987
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->m:LX/0Ot;

    .line 2099988
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->b()V

    .line 2099989
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2100152
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2100153
    sget-object v0, LX/EHe;->NOT_START:LX/EHe;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    .line 2100154
    iput-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->b:Ljava/nio/ByteBuffer;

    .line 2100155
    iput-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->c:LX/3rW;

    .line 2100156
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->d:Landroid/graphics/Paint;

    .line 2100157
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->e:Landroid/graphics/Paint;

    .line 2100158
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->f:Landroid/graphics/Paint;

    .line 2100159
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->g:Landroid/graphics/Paint;

    .line 2100160
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    .line 2100161
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->i:Landroid/graphics/Paint;

    .line 2100162
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->j:Ljava/util/HashMap;

    .line 2100163
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->l:Landroid/os/Handler;

    .line 2100164
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2100165
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->m:LX/0Ot;

    .line 2100166
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->b()V

    .line 2100167
    return-void
.end method

.method private a(J)I
    .locals 5

    .prologue
    .line 2100147
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->j:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2100148
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->j:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2100149
    :goto_0
    return v0

    .line 2100150
    :cond_0
    sget-object v0, Lcom/facebook/rtc/views/RtcSnakeView;->k:[I

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    aget v0, v0, v1

    .line 2100151
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->j:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;B)V
    .locals 6

    .prologue
    .line 2100130
    const/4 v0, 0x0

    .line 2100131
    if-nez p2, :cond_2

    .line 2100132
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0807f7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2100133
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    const v2, -0xff0100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2100134
    :cond_0
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2100135
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 2100136
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 2100137
    iget-object v3, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v0, v4, v5, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2100138
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    .line 2100139
    iget-object v3, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2100140
    :cond_1
    return-void

    .line 2100141
    :cond_2
    const/4 v1, 0x1

    if-ne p2, v1, :cond_3

    .line 2100142
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0807f8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2100143
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 2100144
    :cond_3
    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    .line 2100145
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0807f9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2100146
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    const/16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;FFF)V
    .locals 10

    .prologue
    const/16 v9, 0x1e

    const/4 v6, 0x1

    .line 2100119
    const/high16 v0, 0x41f00000    # 30.0f

    div-float v8, p4, v0

    .line 2100120
    add-float v3, p2, p4

    add-float v4, p3, p4

    iget-object v5, p0, Lcom/facebook/rtc/views/RtcSnakeView;->d:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p2

    move v2, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move v7, v6

    .line 2100121
    :goto_0
    if-ge v7, v9, :cond_0

    .line 2100122
    int-to-float v0, v7

    mul-float/2addr v0, v8

    add-float v1, p2, v0

    .line 2100123
    add-float v4, p3, p4

    iget-object v5, p0, Lcom/facebook/rtc/views/RtcSnakeView;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, p3

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2100124
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 2100125
    :cond_0
    :goto_1
    if-ge v6, v9, :cond_1

    .line 2100126
    int-to-float v0, v6

    mul-float/2addr v0, v8

    add-float v2, p3, v0

    .line 2100127
    add-float v3, p2, p4

    iget-object v5, p0, Lcom/facebook/rtc/views/RtcSnakeView;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2100128
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 2100129
    :cond_1
    return-void
.end method

.method private a(Landroid/graphics/Canvas;FFFFLX/EGh;)V
    .locals 9

    .prologue
    .line 2100091
    invoke-virtual {p6}, LX/EGh;->b()LX/EGf;

    move-result-object v0

    .line 2100092
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/EGf;->a()S

    move-result v1

    if-ltz v1, :cond_0

    invoke-virtual {v0}, LX/EGf;->b()S

    move-result v0

    if-ltz v0, :cond_0

    .line 2100093
    invoke-virtual {p6}, LX/EGh;->b()LX/EGf;

    move-result-object v0

    invoke-virtual {v0}, LX/EGf;->a()S

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p4

    add-float v1, p2, v0

    .line 2100094
    invoke-virtual {p6}, LX/EGh;->b()LX/EGf;

    move-result-object v0

    invoke-virtual {v0}, LX/EGf;->b()S

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p5

    add-float v2, p3, v0

    .line 2100095
    add-float v3, v1, p4

    add-float v4, v2, p5

    iget-object v5, p0, Lcom/facebook/rtc/views/RtcSnakeView;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2100096
    :cond_0
    const/4 v0, 0x0

    move v6, v0

    .line 2100097
    :goto_0
    const/4 v0, 0x4

    invoke-virtual {p6, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p6, v0}, LX/0eW;->d(I)I

    move-result v0

    :goto_1
    move v0, v0

    .line 2100098
    if-ge v6, v0, :cond_3

    .line 2100099
    new-instance v0, LX/EGg;

    invoke-direct {v0}, LX/EGg;-><init>()V

    .line 2100100
    const/4 v1, 0x4

    invoke-virtual {p6, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p6, v1}, LX/0eW;->e(I)I

    move-result v1

    mul-int/lit8 v2, v6, 0x4

    add-int/2addr v1, v2

    invoke-virtual {p6, v1}, LX/0eW;->b(I)I

    move-result v1

    iget-object v2, p6, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 2100101
    iput v1, v0, LX/EGg;->a:I

    iput-object v2, v0, LX/EGg;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 2100102
    :goto_2
    move-object v0, v1

    .line 2100103
    move-object v8, v0

    .line 2100104
    invoke-virtual {v8}, LX/EGg;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/rtc/views/RtcSnakeView;->a(J)I

    move-result v0

    .line 2100105
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->g:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2100106
    const/4 v0, 0x0

    move v7, v0

    .line 2100107
    :goto_3
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v8, v0}, LX/0eW;->d(I)I

    move-result v0

    :goto_4
    move v0, v0

    .line 2100108
    if-ge v7, v0, :cond_2

    .line 2100109
    new-instance v0, LX/EGf;

    invoke-direct {v0}, LX/EGf;-><init>()V

    .line 2100110
    const/16 v1, 0x8

    invoke-virtual {v8, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v8, v1}, LX/0eW;->e(I)I

    move-result v1

    mul-int/lit8 v2, v7, 0x4

    add-int/2addr v1, v2

    iget-object v2, v8, LX/0eW;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1, v2}, LX/EGf;->a(ILjava/nio/ByteBuffer;)LX/EGf;

    move-result-object v1

    :goto_5
    move-object v0, v1

    .line 2100111
    move-object v0, v0

    .line 2100112
    if-eqz v0, :cond_1

    .line 2100113
    invoke-virtual {v0}, LX/EGf;->a()S

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p4

    add-float/2addr v1, p2

    .line 2100114
    invoke-virtual {v0}, LX/EGf;->b()S

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p5

    add-float v2, p3, v0

    .line 2100115
    add-float v3, v1, p4

    add-float v4, v2, p5

    iget-object v5, p0, Lcom/facebook/rtc/views/RtcSnakeView;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2100116
    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_3

    .line 2100117
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 2100118
    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    const/4 v1, 0x0

    goto :goto_5
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .locals 6

    .prologue
    .line 2100081
    if-lez p2, :cond_1

    .line 2100082
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 2100083
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2100084
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 2100085
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 2100086
    iget-object v3, p0, Lcom/facebook/rtc/views/RtcSnakeView;->i:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v0, v4, v5, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2100087
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    .line 2100088
    iget-object v3, p0, Lcom/facebook/rtc/views/RtcSnakeView;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2100089
    :cond_0
    return-void

    .line 2100090
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0807f6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/RtcSnakeView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rtc/views/RtcSnakeView;

    const/16 v1, 0x3257

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->m:LX/0Ot;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/rtc/views/RtcSnakeView;B)V
    .locals 3

    .prologue
    .line 2100063
    new-instance v0, LX/0eX;

    invoke-direct {v0}, LX/0eX;-><init>()V

    .line 2100064
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0eX;->b(I)V

    .line 2100065
    const/4 v1, 0x0

    .line 2100066
    invoke-virtual {v0, v1, p1, v1}, LX/0eX;->a(IBI)V

    .line 2100067
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v1

    .line 2100068
    move v1, v1

    .line 2100069
    invoke-virtual {v0, v1}, LX/0eX;->c(I)V

    .line 2100070
    new-instance v1, LX/0eX;

    invoke-direct {v1}, LX/0eX;-><init>()V

    .line 2100071
    invoke-virtual {v0}, LX/0eX;->e()[B

    move-result-object v0

    const/4 p1, 0x1

    .line 2100072
    array-length v2, v0

    invoke-virtual {v1, p1, v2, p1}, LX/0eX;->a(III)V

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    :goto_0
    if-ltz v2, :cond_0

    aget-byte p1, v0, v2

    invoke-virtual {v1, p1}, LX/0eX;->a(B)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, LX/0eX;->b()I

    move-result v2

    move v0, v2

    .line 2100073
    invoke-static {v1}, LX/ECS;->a(LX/0eX;)V

    .line 2100074
    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/ECS;->a(LX/0eX;B)V

    .line 2100075
    const/4 v2, 0x1

    const/4 p1, 0x0

    invoke-virtual {v1, v2, v0, p1}, LX/0eX;->c(III)V

    .line 2100076
    invoke-static {v1}, LX/ECS;->b(LX/0eX;)I

    move-result v0

    .line 2100077
    invoke-virtual {v1, v0}, LX/0eX;->c(I)V

    .line 2100078
    invoke-virtual {v1}, LX/0eX;->e()[B

    move-result-object v1

    .line 2100079
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v1}, LX/EDx;->a([B)V

    .line 2100080
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 2100041
    const-class v0, Lcom/facebook/rtc/views/RtcSnakeView;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcSnakeView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2100042
    new-instance v0, LX/3rW;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, LX/EHd;

    invoke-direct {v2, p0}, LX/EHd;-><init>(Lcom/facebook/rtc/views/RtcSnakeView;)V

    invoke-direct {v0, v1, v2}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->c:LX/3rW;

    .line 2100043
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->c:LX/3rW;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3rW;->a(Z)V

    .line 2100044
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2100045
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2100046
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->d:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2100047
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->e:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2100048
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->e:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2100049
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->f:Landroid/graphics/Paint;

    const/16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2100050
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2100051
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2100052
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->g:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2100053
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->g:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2100054
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2100055
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2100056
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 2100057
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->h:Landroid/graphics/Paint;

    const/high16 v1, 0x42c00000    # 96.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2100058
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2100059
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 2100060
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 2100061
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->i:Landroid/graphics/Paint;

    const/high16 v1, 0x43400000    # 192.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2100062
    return-void
.end method

.method public static c(Lcom/facebook/rtc/views/RtcSnakeView;)V
    .locals 2

    .prologue
    .line 2100036
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->b:Ljava/nio/ByteBuffer;

    .line 2100037
    sget-object v0, LX/EHe;->NOT_START:LX/EHe;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    .line 2100038
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    sget-object v1, LX/EHe;->NOT_START:LX/EHe;

    if-ne v0, v1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcSnakeView;->setVisibility(I)V

    .line 2100039
    return-void

    .line 2100040
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2100028
    new-instance v0, LX/0eX;

    invoke-direct {v0}, LX/0eX;-><init>()V

    .line 2100029
    invoke-static {v0}, LX/ECS;->a(LX/0eX;)V

    .line 2100030
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/ECS;->a(LX/0eX;B)V

    .line 2100031
    invoke-static {v0}, LX/ECS;->b(LX/0eX;)I

    move-result v1

    .line 2100032
    invoke-virtual {v0, v1}, LX/0eX;->c(I)V

    .line 2100033
    invoke-virtual {v0}, LX/0eX;->e()[B

    move-result-object v1

    .line 2100034
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v1}, LX/EDx;->a([B)V

    .line 2100035
    return-void
.end method

.method public final a([B)V
    .locals 5

    .prologue
    .line 2100013
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 2100014
    :cond_0
    :goto_0
    return-void

    .line 2100015
    :cond_1
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->b:Ljava/nio/ByteBuffer;

    .line 2100016
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->b:Ljava/nio/ByteBuffer;

    invoke-static {v0}, LX/EGj;->a(Ljava/nio/ByteBuffer;)LX/EGj;

    move-result-object v0

    .line 2100017
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_3

    iget-object v2, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v3, v0, LX/0eW;->a:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    :goto_1
    move v0, v1

    .line 2100018
    packed-switch v0, :pswitch_data_0

    .line 2100019
    :goto_2
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    sget-object v1, LX/EHe;->NOT_START:LX/EHe;

    if-ne v0, v1, :cond_2

    const/16 v0, 0x8

    :goto_3
    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcSnakeView;->setVisibility(I)V

    goto :goto_0

    .line 2100020
    :pswitch_0
    sget-object v0, LX/EHe;->COUNT_DOWN:LX/EHe;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    .line 2100021
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->invalidate()V

    goto :goto_2

    .line 2100022
    :pswitch_1
    sget-object v0, LX/EHe;->IN_GAME:LX/EHe;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    .line 2100023
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->invalidate()V

    goto :goto_2

    .line 2100024
    :pswitch_2
    sget-object v0, LX/EHe;->GAME_OVER:LX/EHe;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    .line 2100025
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcSnakeView;->invalidate()V

    .line 2100026
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->l:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/rtc/views/RtcSnakeView$2;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/views/RtcSnakeView$2;-><init>(Lcom/facebook/rtc/views/RtcSnakeView;)V

    const-wide/16 v2, 0x1388

    const v4, -0x443edbae

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_2

    .line 2100027
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 2099991
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->b:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_1

    .line 2099992
    :cond_0
    :goto_0
    return-void

    .line 2099993
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->b:Ljava/nio/ByteBuffer;

    invoke-static {v0}, LX/EGj;->a(Ljava/nio/ByteBuffer;)LX/EGj;

    move-result-object v7

    .line 2099994
    if-eqz v7, :cond_0

    .line 2099995
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0xa

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, -0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    .line 2099996
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, v0

    div-float v2, v1, v3

    .line 2099997
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, v0

    div-float v3, v1, v3

    .line 2099998
    const/high16 v1, 0x41f00000    # 30.0f

    div-float v4, v0, v1

    .line 2099999
    invoke-direct {p0, p1, v2, v3, v0}, Lcom/facebook/rtc/views/RtcSnakeView;->a(Landroid/graphics/Canvas;FFF)V

    .line 2100000
    new-instance v0, LX/EGh;

    invoke-direct {v0}, LX/EGh;-><init>()V

    .line 2100001
    const/4 v1, 0x6

    invoke-virtual {v7, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_4

    iget v5, v7, LX/0eW;->a:I

    add-int/2addr v1, v5

    invoke-virtual {v7, v1}, LX/0eW;->b(I)I

    move-result v1

    iget-object v5, v7, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 2100002
    iput v1, v0, LX/EGh;->a:I

    iput-object v5, v0, LX/EGh;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 2100003
    :goto_1
    move-object v0, v1

    .line 2100004
    move-object v6, v0

    .line 2100005
    if-eqz v6, :cond_2

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    .line 2100006
    invoke-direct/range {v0 .. v6}, Lcom/facebook/rtc/views/RtcSnakeView;->a(Landroid/graphics/Canvas;FFFFLX/EGh;)V

    .line 2100007
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    sget-object v1, LX/EHe;->COUNT_DOWN:LX/EHe;

    if-ne v0, v1, :cond_3

    .line 2100008
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v1, v7, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, v7, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->getShort(I)S

    move-result v0

    :goto_2
    move v0, v0

    .line 2100009
    invoke-direct {p0, p1, v0}, Lcom/facebook/rtc/views/RtcSnakeView;->a(Landroid/graphics/Canvas;I)V

    goto :goto_0

    .line 2100010
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcSnakeView;->a:LX/EHe;

    sget-object v1, LX/EHe;->GAME_OVER:LX/EHe;

    if-ne v0, v1, :cond_0

    .line 2100011
    const/16 v0, 0xa

    invoke-virtual {v7, v0}, LX/0eW;->a(I)I

    move-result v0

    if-eqz v0, :cond_6

    iget-object v1, v7, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v2, v7, LX/0eW;->a:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    :goto_3
    move v0, v0

    .line 2100012
    invoke-direct {p0, p1, v0}, Lcom/facebook/rtc/views/RtcSnakeView;->a(Landroid/graphics/Canvas;B)V

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x60161d3a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2099990
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcSnakeView;->c:LX/3rW;

    invoke-virtual {v1, p1}, LX/3rW;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x34fcd480    # -8596352.0f

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
