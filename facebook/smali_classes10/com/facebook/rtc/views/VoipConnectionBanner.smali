.class public Lcom/facebook/rtc/views/VoipConnectionBanner;
.super LX/EH2;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:LX/EIH;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2101391
    invoke-direct {p0, p1, p2}, LX/EH2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2101392
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2101393
    iput-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->a:LX/0Ot;

    .line 2101394
    sget-object v0, LX/EIH;->VOICE:LX/EIH;

    iput-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->e:LX/EIH;

    .line 2101395
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->WebrtcLinearLayout:[I

    invoke-virtual {v0, p2, v1, v2, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2101396
    :try_start_0
    invoke-static {}, LX/EIH;->values()[LX/EIH;

    move-result-object v0

    const/16 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->e:LX/EIH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2101397
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2101398
    invoke-direct {p0, p1}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a(Landroid/content/Context;)V

    .line 2101399
    return-void

    .line 2101400
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method private a(III)V
    .locals 3

    .prologue
    .line 2101401
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->b:Landroid/view/View;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/VoipConnectionBanner;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2101402
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->c:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 2101403
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 2101404
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2101405
    const-class v0, Lcom/facebook/rtc/views/VoipConnectionBanner;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2101406
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2101407
    const v1, 0x7f0315e9

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->b:Landroid/view/View;

    .line 2101408
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->b:Landroid/view/View;

    const v1, 0x7f0d3138

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->c:Landroid/view/View;

    .line 2101409
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->b:Landroid/view/View;

    const v1, 0x7f0d3139

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2101410
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rtc/views/VoipConnectionBanner;

    const/16 v1, 0x3257

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2101411
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2101412
    :goto_0
    return-void

    .line 2101413
    :cond_0
    sget-object v1, LX/EIG;->a:[I

    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101414
    iget-object v5, v0, LX/EDx;->cl:LX/EDs;

    move-object v0, v5

    .line 2101415
    invoke-virtual {v0}, LX/EDs;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2101416
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->e:LX/EIH;

    sget-object v1, LX/EIH;->VOICE:LX/EIH;

    if-ne v0, v1, :cond_3

    .line 2101417
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2101418
    :pswitch_0
    const v0, 0x7f0a02f0

    const v1, 0x7f08076c

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a(III)V

    .line 2101419
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2101420
    :pswitch_1
    const v0, 0x7f0a02f1

    const v1, 0x7f08076d

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a(III)V

    goto :goto_1

    .line 2101421
    :pswitch_2
    const v0, 0x7f0a02f2

    const v1, 0x7f08076e

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a(III)V

    goto :goto_1

    .line 2101422
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->e:LX/EIH;

    sget-object v1, LX/EIH;->VOICE:LX/EIH;

    if-ne v0, v1, :cond_1

    .line 2101423
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2101424
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->e:LX/EIH;

    sget-object v1, LX/EIH;->CONFERENCE:LX/EIH;

    if-ne v0, v1, :cond_2

    .line 2101425
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2101426
    :cond_2
    const v0, 0x7f0a02f0

    const v1, 0x7f08076c

    invoke-direct {p0, v0, v2, v1}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a(III)V

    goto :goto_1

    .line 2101427
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setTheme(LX/EIH;)V
    .locals 0

    .prologue
    .line 2101428
    iput-object p1, p0, Lcom/facebook/rtc/views/VoipConnectionBanner;->e:LX/EIH;

    .line 2101429
    invoke-virtual {p0}, Lcom/facebook/rtc/views/VoipConnectionBanner;->a()V

    .line 2101430
    return-void
.end method
