.class public Lcom/facebook/rtc/views/ChildLockBanner;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# instance fields
.field private a:LX/EFU;

.field public b:LX/EFV;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/EGk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2098391
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2098392
    invoke-direct {p0}, Lcom/facebook/rtc/views/ChildLockBanner;->a()V

    .line 2098393
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2098388
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2098389
    invoke-direct {p0}, Lcom/facebook/rtc/views/ChildLockBanner;->a()V

    .line 2098390
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2098385
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2098386
    invoke-direct {p0}, Lcom/facebook/rtc/views/ChildLockBanner;->a()V

    .line 2098387
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 2098376
    const-class v0, Lcom/facebook/rtc/views/ChildLockBanner;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/ChildLockBanner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2098377
    invoke-virtual {p0}, Lcom/facebook/rtc/views/ChildLockBanner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2098378
    const v1, 0x7f030901

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2098379
    iget-object v0, p0, Lcom/facebook/rtc/views/ChildLockBanner;->b:LX/EFV;

    const/4 v1, 0x3

    const-wide/16 v2, 0xbb8

    .line 2098380
    new-instance v5, LX/EFU;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v4

    check-cast v4, LX/0So;

    invoke-direct {v5, v1, v2, v3, v4}, LX/EFU;-><init>(IJLX/0So;)V

    .line 2098381
    move-object v0, v5

    .line 2098382
    iput-object v0, p0, Lcom/facebook/rtc/views/ChildLockBanner;->a:LX/EFU;

    .line 2098383
    const v0, 0x7f0d1726

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/ChildLockBanner;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/views/ChildLockBanner;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 2098384
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/ChildLockBanner;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rtc/views/ChildLockBanner;

    const-class v1, LX/EFV;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/EFV;

    iput-object v0, p0, Lcom/facebook/rtc/views/ChildLockBanner;->b:LX/EFV;

    return-void
.end method


# virtual methods
.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x6fac86ba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2098359
    invoke-virtual {p0}, Lcom/facebook/rtc/views/ChildLockBanner;->isShown()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2098360
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, 0x55038cbc

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2098361
    :goto_0
    return v0

    .line 2098362
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 2098363
    iget-object v2, p0, Lcom/facebook/rtc/views/ChildLockBanner;->a:LX/EFU;

    .line 2098364
    iget-object v4, v2, LX/EFU;->c:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    .line 2098365
    iget-object v6, v2, LX/EFU;->a:[J

    iget v7, v2, LX/EFU;->d:I

    aput-wide v4, v6, v7

    .line 2098366
    iget v6, v2, LX/EFU;->d:I

    add-int/lit8 v6, v6, 0x1

    iget-object v7, v2, LX/EFU;->a:[J

    array-length v7, v7

    rem-int/2addr v6, v7

    iput v6, v2, LX/EFU;->d:I

    .line 2098367
    iget-object v6, v2, LX/EFU;->a:[J

    iget v7, v2, LX/EFU;->d:I

    aget-wide v6, v6, v7

    sub-long/2addr v4, v6

    iget-wide v6, v2, LX/EFU;->b:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    .line 2098368
    const/4 v4, 0x0

    .line 2098369
    :goto_1
    move v2, v4

    .line 2098370
    if-eqz v2, :cond_1

    .line 2098371
    iget-object v2, p0, Lcom/facebook/rtc/views/ChildLockBanner;->c:LX/EGk;

    if-eqz v2, :cond_1

    .line 2098372
    iget-object v2, p0, Lcom/facebook/rtc/views/ChildLockBanner;->c:LX/EGk;

    invoke-interface {v2}, LX/EGk;->a()V

    .line 2098373
    :cond_1
    const v2, -0xc60746d

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 2098374
    :cond_2
    invoke-static {v2}, LX/EFU;->b(LX/EFU;)V

    .line 2098375
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public setChildLockBannerListener(LX/EGk;)V
    .locals 0

    .prologue
    .line 2098357
    iput-object p1, p0, Lcom/facebook/rtc/views/ChildLockBanner;->c:LX/EGk;

    .line 2098358
    return-void
.end method

.method public setColorScheme(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2098348
    if-eqz p1, :cond_1

    const v0, 0x7f0a00f9

    move v1, v0

    .line 2098349
    :goto_0
    if-eqz p1, :cond_2

    const v0, 0x7f0a0310

    .line 2098350
    :goto_1
    iget-object v2, p0, Lcom/facebook/rtc/views/ChildLockBanner;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/ChildLockBanner;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2098351
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/ChildLockBanner;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/facebook/rtc/views/ChildLockBanner;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2098352
    if-eqz p1, :cond_0

    .line 2098353
    iget-object v0, p0, Lcom/facebook/rtc/views/ChildLockBanner;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v4, v4, v4, v4}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2098354
    :cond_0
    return-void

    .line 2098355
    :cond_1
    const v0, 0x7f0a00d5

    move v1, v0

    goto :goto_0

    .line 2098356
    :cond_2
    const v0, 0x7f0a02f9

    goto :goto_1
.end method
