.class public Lcom/facebook/rtc/views/RtcFloatingSelfView;
.super LX/EHF;
.source ""


# static fields
.field private static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserKey;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/view/TextureView;

.field private f:Lcom/facebook/user/tiles/UserTileView;

.field private g:Landroid/graphics/Path;

.field private h:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2099270
    const-class v0, Lcom/facebook/rtc/views/RtcFloatingSelfView;

    sput-object v0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2099262
    invoke-direct {p0, p1}, LX/EHF;-><init>(Landroid/content/Context;)V

    .line 2099263
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->g:Landroid/graphics/Path;

    .line 2099264
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    .line 2099265
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2099266
    invoke-direct {p0, p1, p2}, LX/EHF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2099267
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->g:Landroid/graphics/Path;

    .line 2099268
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    .line 2099269
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;

    const/16 v1, 0x12cd

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->a:LX/0Or;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2099271
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 2099272
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->f:Lcom/facebook/user/tiles/UserTileView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2099273
    return-void
.end method

.method public final a(Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    .line 2099237
    iget-object v0, p0, LX/EHF;->a:Landroid/graphics/Point;

    move-object v0, v0

    .line 2099238
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 2099239
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2099240
    const/4 v2, 0x0

    iput v2, p1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iput v2, p1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v2, p1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iput v2, p1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2099241
    if-ne v1, v5, :cond_1

    .line 2099242
    int-to-float v2, v0

    iget v3, p0, LX/EHF;->c:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2099243
    iput v0, p1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2099244
    :goto_0
    iget v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 2099245
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 2099246
    if-ne v1, v5, :cond_3

    .line 2099247
    iget v1, p0, LX/EHF;->c:F

    iget v2, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 2099248
    iget v1, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    iget v2, p0, LX/EHF;->c:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 2099249
    :goto_1
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setTransform(Landroid/graphics/Matrix;)V

    .line 2099250
    :cond_0
    return-void

    .line 2099251
    :cond_1
    iput v0, p1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2099252
    int-to-float v0, v0

    iget v2, p0, LX/EHF;->c:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2099253
    goto :goto_0

    .line 2099254
    :cond_2
    iget v1, p0, LX/EHF;->c:F

    iget v2, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    goto :goto_1

    .line 2099255
    :cond_3
    iget v1, p0, LX/EHF;->c:F

    iget v2, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 2099256
    iget v1, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    iget v2, p0, LX/EHF;->c:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    goto :goto_1

    .line 2099257
    :cond_4
    iget v1, p0, LX/EHF;->c:F

    iget v2, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    div-float/2addr v1, v2

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2099258
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->a()V

    .line 2099259
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setAlpha(F)V

    .line 2099260
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 2099261
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2099233
    invoke-virtual {p0}, LX/EHF;->j()V

    .line 2099234
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 2099235
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setAlpha(F)V

    .line 2099236
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2099225
    const-class v0, Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2099226
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2099227
    const v1, 0x7f031237

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2099228
    const v0, 0x7f0d2ac7

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    .line 2099229
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setOpaque(Z)V

    .line 2099230
    const v0, 0x7f0d2ac6

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->f:Lcom/facebook/user/tiles/UserTileView;

    .line 2099231
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->f:Lcom/facebook/user/tiles/UserTileView;

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2099232
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2099223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->g:Landroid/graphics/Path;

    .line 2099224
    return-void
.end method

.method public getOtherViews()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2099222
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->f:Lcom/facebook/user/tiles/UserTileView;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public getSelfTextureView()Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 2099217
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    return-object v0
.end method

.method public getVideoView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2099221
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->e:Landroid/view/TextureView;

    return-object v0
.end method

.method public setCaptureVideoPortraitRatio(F)V
    .locals 0

    .prologue
    .line 2099218
    iput p1, p0, Lcom/facebook/rtc/views/RtcFloatingSelfView;->h:F

    .line 2099219
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->requestLayout()V

    .line 2099220
    return-void
.end method
