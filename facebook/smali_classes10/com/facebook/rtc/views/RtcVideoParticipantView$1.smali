.class public final Lcom/facebook/rtc/views/RtcVideoParticipantView$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/EIC;


# direct methods
.method public constructor <init>(LX/EIC;)V
    .locals 0

    .prologue
    .line 2101097
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcVideoParticipantView$1;->a:LX/EIC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2101092
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoParticipantView$1;->a:LX/EIC;

    iget-object v0, v0, LX/EIC;->f:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->getLastRedrawTime()J

    move-result-wide v2

    .line 2101093
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoParticipantView$1;->a:LX/EIC;

    iget-object v0, v0, LX/EIC;->d:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v0

    sub-long/2addr v0, v2

    const-wide/16 v4, 0x1388

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 2101094
    :goto_0
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcVideoParticipantView$1;->a:LX/EIC;

    iget-object v1, v1, LX/EIC;->c:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/facebook/rtc/views/RtcVideoParticipantView$1$1;

    invoke-direct {v4, p0, v2, v3, v0}, Lcom/facebook/rtc/views/RtcVideoParticipantView$1$1;-><init>(Lcom/facebook/rtc/views/RtcVideoParticipantView$1;JZ)V

    const v0, -0x53116bc5

    invoke-static {v1, v4, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2101095
    return-void

    .line 2101096
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
