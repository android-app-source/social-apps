.class public Lcom/facebook/rtc/views/RtcLevelTileView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:I

.field private b:I

.field public c:I

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/animation/ValueAnimator;

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2099591
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2099592
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/rtc/views/RtcLevelTileView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2099593
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2099588
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2099589
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/rtc/views/RtcLevelTileView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2099590
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2099559
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2099560
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/rtc/views/RtcLevelTileView;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2099561
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2099594
    sget-object v0, LX/03r;->RtcLevelTileView:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2099595
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    .line 2099596
    iget v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    iput v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->c:I

    .line 2099597
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->d:Landroid/graphics/Paint;

    .line 2099598
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2099599
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->d:Landroid/graphics/Paint;

    const/16 v2, 0x7f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2099600
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcLevelTileView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a030f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2099601
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2099602
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    .line 2099577
    iget v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->f:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->b:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcLevelTileView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->b:I

    iget v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    if-gt v0, v1, :cond_1

    .line 2099578
    :cond_0
    :goto_0
    return-void

    .line 2099579
    :cond_1
    int-to-double v0, p1

    cmpl-double v0, v0, v4

    if-lez v0, :cond_2

    .line 2099580
    const/16 p1, 0xa

    .line 2099581
    :cond_2
    iget v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->b:I

    iget v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    sub-int/2addr v0, v1

    int-to-double v0, v0

    int-to-double v2, p1

    div-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iget v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    add-int/2addr v0, v1

    .line 2099582
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget v3, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->c:I

    aput v3, v1, v2

    const/4 v2, 0x1

    aput v0, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->e:Landroid/animation/ValueAnimator;

    .line 2099583
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->e:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2099584
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->e:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2099585
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->e:Landroid/animation/ValueAnimator;

    new-instance v1, LX/EHT;

    invoke-direct {v1, p0}, LX/EHT;-><init>(Lcom/facebook/rtc/views/RtcLevelTileView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2099586
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2099587
    iput p1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->f:I

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2099571
    iget v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->c:I

    iget v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    if-gt v0, v1, :cond_0

    .line 2099572
    :goto_0
    return-void

    .line 2099573
    :cond_0
    iget v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->c:I

    iget v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 2099574
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2099575
    iget v0, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 2099576
    iget v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->g:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->h:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x49475983

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2099562
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 2099563
    const/16 v1, 0x2d

    const v2, -0xdbb200c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2099564
    :goto_0
    return-void

    .line 2099565
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcLevelTileView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcLevelTileView;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->b:I

    .line 2099566
    iget v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->b:I

    iget v2, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    if-ge v1, v2, :cond_1

    .line 2099567
    iget v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->b:I

    iput v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->a:I

    .line 2099568
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcLevelTileView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->g:I

    .line 2099569
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcLevelTileView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/facebook/rtc/views/RtcLevelTileView;->h:I

    .line 2099570
    const v1, 0x5e2fdae4

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method
