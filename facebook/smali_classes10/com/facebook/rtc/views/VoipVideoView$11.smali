.class public final Lcom/facebook/rtc/views/VoipVideoView$11;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/EIU;


# direct methods
.method public constructor <init>(LX/EIU;)V
    .locals 0

    .prologue
    .line 2101457
    iput-object p1, p0, Lcom/facebook/rtc/views/VoipVideoView$11;->a:LX/EIU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    .line 2101458
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipVideoView$11;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2101459
    iget-object v2, v0, LX/EDx;->af:LX/EGE;

    move-object v2, v2

    .line 2101460
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipVideoView$11;->a:LX/EIU;

    .line 2101461
    iget-object v12, v0, LX/EIU;->u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v12}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->getLastRedrawTime()J

    move-result-wide v12

    move-wide v4, v12

    .line 2101462
    cmp-long v0, v4, v10

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rtc/views/VoipVideoView$11;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->I:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    invoke-virtual {v0}, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->now()J

    move-result-wide v6

    sub-long/2addr v6, v4

    const-wide/16 v8, 0x1388

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    const/4 v0, 0x1

    .line 2101463
    :goto_0
    cmp-long v3, v4, v10

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/facebook/rtc/views/VoipVideoView$11;->a:LX/EIU;

    iget-boolean v3, v3, LX/EIU;->aj:Z

    if-eqz v3, :cond_1

    :cond_0
    if-eqz v0, :cond_5

    .line 2101464
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipVideoView$11;->a:LX/EIU;

    iget-object v0, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2101465
    if-eqz v2, :cond_2

    iget-boolean v0, v2, LX/EGE;->i:Z

    if-eqz v0, :cond_2

    .line 2101466
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipVideoView$11;->a:LX/EIU;

    const v2, 0x7f08078b

    invoke-static {v0, v1, v2}, LX/EIU;->a$redex0(LX/EIU;II)V

    .line 2101467
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    .line 2101468
    goto :goto_0

    .line 2101469
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipVideoView$11;->a:LX/EIU;

    const v2, 0x7f08078b

    invoke-static {v0, v1, v2}, LX/EIU;->a$redex0(LX/EIU;II)V

    goto :goto_1

    .line 2101470
    :cond_5
    iget-object v0, p0, Lcom/facebook/rtc/views/VoipVideoView$11;->a:LX/EIU;

    const/16 v2, 0x8

    invoke-static {v0, v2, v1}, LX/EIU;->a$redex0(LX/EIU;II)V

    goto :goto_1
.end method
