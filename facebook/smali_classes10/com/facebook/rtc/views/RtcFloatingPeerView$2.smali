.class public final Lcom/facebook/rtc/views/RtcFloatingPeerView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:Z

.field public final synthetic d:Lcom/facebook/rtc/views/RtcFloatingPeerView;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/views/RtcFloatingPeerView;ZZZ)V
    .locals 0

    .prologue
    .line 2099000
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->d:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iput-boolean p2, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->a:Z

    iput-boolean p3, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->b:Z

    iput-boolean p4, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 2099001
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->d:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2099002
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->d:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 2099003
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->d:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v3, v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->g:Landroid/view/View;

    iget-boolean v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->a:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2099004
    iget-boolean v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->b:Z

    if-nez v0, :cond_4

    .line 2099005
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->d:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v0, v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->d:Landroid/view/View;

    iget-boolean v3, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->c:Z

    if-eqz v3, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2099006
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->d:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-boolean v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->c:Z

    .line 2099007
    iput-boolean v1, v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->n:Z

    .line 2099008
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 2099009
    goto :goto_0

    :cond_3
    move v1, v2

    .line 2099010
    goto :goto_1

    .line 2099011
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->d:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-boolean v0, v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->n:Z

    iget-boolean v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->c:Z

    if-eq v0, v1, :cond_1

    .line 2099012
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->d:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-boolean v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->c:Z

    .line 2099013
    iput-boolean v1, v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->n:Z

    .line 2099014
    iget-boolean v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->c:Z

    if-eqz v0, :cond_5

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2099015
    :goto_3
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2099016
    new-instance v1, LX/EHE;

    invoke-direct {v1, p0}, LX/EHE;-><init>(Lcom/facebook/rtc/views/RtcFloatingPeerView$2;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2099017
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;->d:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v1, v1, Lcom/facebook/rtc/views/RtcFloatingPeerView;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    .line 2099018
    :cond_5
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v5, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    goto :goto_3
.end method
