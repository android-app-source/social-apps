.class public Lcom/facebook/rtc/views/RtcFloatingPeerView;
.super LX/EHF;
.source ""


# instance fields
.field public a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

.field private f:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public g:Landroid/view/View;

.field private h:Lcom/facebook/user/tiles/UserTileView;

.field private i:Landroid/view/View;

.field private j:Lcom/facebook/widget/tiles/ThreadTileView;

.field private k:Lcom/facebook/resources/ui/FbTextView;

.field private l:Lcom/facebook/resources/ui/FbTextView;

.field private m:Landroid/graphics/drawable/Drawable;

.field public n:Z

.field private o:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/EFm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2099153
    invoke-direct {p0, p1}, LX/EHF;-><init>(Landroid/content/Context;)V

    .line 2099154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->n:Z

    .line 2099155
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2099156
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->r:LX/0Ot;

    .line 2099157
    const-class v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2099158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2099159
    invoke-direct {p0, p1, p2}, LX/EHF;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2099160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->n:Z

    .line 2099161
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2099162
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->r:LX/0Ot;

    .line 2099163
    const-class v0, Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2099164
    return-void
.end method

.method private static a(Lcom/facebook/rtc/views/RtcFloatingPeerView;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;LX/EFm;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rtc/views/RtcFloatingPeerView;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            "LX/EFm;",
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2099165
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->o:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->p:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object p3, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->q:LX/EFm;

    iput-object p4, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->r:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-static {v3}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v3}, LX/0um;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v3}, LX/EFm;->b(LX/0QB;)LX/EFm;

    move-result-object v2

    check-cast v2, LX/EFm;

    const/16 v4, 0x3257

    invoke-static {v3, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a(Lcom/facebook/rtc/views/RtcFloatingPeerView;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;LX/EFm;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/rtc/views/RtcFloatingPeerView;ZZZ)V
    .locals 3

    .prologue
    .line 2099166
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->o:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;

    invoke-direct {v1, p0, p3, p2, p1}, Lcom/facebook/rtc/views/RtcFloatingPeerView$2;-><init>(Lcom/facebook/rtc/views/RtcFloatingPeerView;ZZZ)V

    const v2, -0x417d85d0

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2099167
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 2099168
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->c()V

    .line 2099169
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2099170
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2099171
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2099172
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->f:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2099173
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->f:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2099174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->f:Ljava/util/concurrent/ScheduledFuture;

    .line 2099175
    :cond_0
    invoke-static {p0, v2, v2, v2}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a$redex0(Lcom/facebook/rtc/views/RtcFloatingPeerView;ZZZ)V

    .line 2099176
    return-void
.end method

.method private m()V
    .locals 7

    .prologue
    .line 2099177
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->l()V

    .line 2099178
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->p:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/views/RtcFloatingPeerView$1;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView$1;-><init>(Lcom/facebook/rtc/views/RtcFloatingPeerView;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1f4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->f:Ljava/util/concurrent/ScheduledFuture;

    .line 2099179
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2099180
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->c()V

    .line 2099181
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2099182
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->j:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tiles/ThreadTileView;->setVisibility(I)V

    .line 2099183
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2099184
    invoke-virtual {p0, v2}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setTimeOverlayColor(Z)V

    .line 2099185
    return-void
.end method

.method public final a(Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 2099186
    iget-object v0, p0, LX/EHF;->a:Landroid/graphics/Point;

    move-object v0, v0

    .line 2099187
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2099188
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2099189
    iget v1, p0, LX/EHF;->c:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 2099190
    iget v1, p0, LX/EHF;->c:F

    cmpg-float v1, v1, v3

    if-gez v1, :cond_1

    .line 2099191
    iget v1, v0, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iget v2, p0, LX/EHF;->c:F

    div-float v2, v3, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 2099192
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->a(II)V

    .line 2099193
    return-void

    .line 2099194
    :cond_1
    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, p0, LX/EHF;->c:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2099195
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2099196
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->l:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2099197
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2099198
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->c()V

    .line 2099199
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2099200
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->j:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->setVisibility(I)V

    .line 2099201
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setTimeOverlayColor(Z)V

    .line 2099202
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2099203
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2099204
    :goto_0
    iget-object v2, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    if-eqz p1, :cond_1

    const/16 v0, 0x3c

    invoke-virtual {p0, v0}, LX/EHF;->a(I)I

    move-result v0

    :goto_1
    invoke-virtual {v2, v1, v1, v1, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 2099205
    return-void

    .line 2099206
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2099207
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h()V

    .line 2099208
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aA()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setPeerName(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2099209
    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2099147
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->c()V

    .line 2099148
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2099149
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->j:Lcom/facebook/widget/tiles/ThreadTileView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->setVisibility(I)V

    .line 2099150
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    invoke-virtual {v0, v2}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2099151
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setTimeOverlayColor(Z)V

    .line 2099152
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 2099210
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->a()V

    .line 2099211
    if-eqz p1, :cond_0

    .line 2099212
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->k()V

    .line 2099213
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    move-object v0, v0

    .line 2099214
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2099215
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->m()V

    .line 2099216
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2099075
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    move-object v0, v0

    .line 2099076
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2099077
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->l()V

    .line 2099078
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->b()V

    .line 2099079
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2099080
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2099081
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    move-object v0, v0

    .line 2099082
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2099083
    invoke-direct {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->m()V

    .line 2099084
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2099085
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2099086
    const v1, 0x7f031236

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2099087
    sget-object v0, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v0, v0

    .line 2099088
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->e:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    .line 2099089
    const v0, 0x7f0d2abf

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    .line 2099090
    const v0, 0x7f0d2abe

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    .line 2099091
    const v0, 0x7f0d2ac2

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    .line 2099092
    const v0, 0x7f0d2ac3

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->j:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2099093
    const v0, 0x7f0d2ac4

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 2099094
    const v0, 0x7f0d2ac5

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 2099095
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->m:Landroid/graphics/drawable/Drawable;

    .line 2099096
    const v0, 0x7f0d2ac0

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->d:Landroid/view/View;

    .line 2099097
    const v0, 0x7f0d2ac1

    invoke-virtual {p0, v0}, LX/EHF;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->g:Landroid/view/View;

    .line 2099098
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 2099099
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2099100
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->k:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2099101
    return-void
.end method

.method public getLastRedrawTime()J
    .locals 2

    .prologue
    .line 2099102
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v0}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->getLastRedrawTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getOtherViews()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2099103
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public getPeerRenderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2099104
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    return-object v0
.end method

.method public getVideoView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2099105
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    move-object v0, v0

    .line 2099106
    return-object v0
.end method

.method public final h()V
    .locals 6

    .prologue
    .line 2099107
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->q:LX/EFm;

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->j:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v0, v1}, LX/EFm;->a(Lcom/facebook/widget/tiles/ThreadTileView;)V

    .line 2099108
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2099109
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2099110
    iget-wide v4, v0, LX/EDx;->ak:J

    move-wide v2, v4

    .line 2099111
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2099112
    :goto_0
    return-void

    .line 2099113
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final i()V
    .locals 4

    .prologue
    .line 2099114
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->q:LX/EFm;

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->j:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2099115
    iget-object v2, v0, LX/EFm;->c:LX/EDx;

    invoke-virtual {v2}, LX/EDx;->j()Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2099116
    iget-object v2, v0, LX/EFm;->c:LX/EDx;

    .line 2099117
    iget-object v3, v2, LX/EDx;->af:LX/EGE;

    move-object v2, v3

    .line 2099118
    if-nez v2, :cond_1

    .line 2099119
    const/4 v2, 0x0

    .line 2099120
    :goto_0
    move v0, v2

    .line 2099121
    if-eqz v0, :cond_0

    .line 2099122
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2099123
    iget-object v1, v0, LX/EDx;->af:LX/EGE;

    move-object v0, v1

    .line 2099124
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2099125
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    iget-object v0, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v0

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2099126
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->h:Lcom/facebook/user/tiles/UserTileView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 2099127
    :goto_1
    return-void

    .line 2099128
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->q:LX/EFm;

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->j:Lcom/facebook/widget/tiles/ThreadTileView;

    invoke-virtual {v0, v1}, LX/EFm;->a(Lcom/facebook/widget/tiles/ThreadTileView;)V

    goto :goto_1

    .line 2099129
    :cond_1
    iget-object v2, v2, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v2, v3, v1}, LX/EFm;->a(LX/EFm;JLcom/facebook/widget/tiles/ThreadTileView;)V

    .line 2099130
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public setOneShotDrawListener(LX/7fD;)V
    .locals 1

    .prologue
    .line 2099131
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    .line 2099132
    iput-object p1, v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->o:LX/7fD;

    .line 2099133
    return-void
.end method

.method public setPeerName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2099134
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2099135
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->k:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2099136
    return-void
.end method

.method public setTimeOverlayColor(Z)V
    .locals 2

    .prologue
    .line 2099137
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2099138
    :goto_0
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2099139
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2099140
    iget-object v1, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->i:Landroid/view/View;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->m:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2099141
    return-void

    .line 2099142
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    .line 2099143
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setVideoSizeChangedListener(LX/7fE;)V
    .locals 1

    .prologue
    .line 2099144
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcFloatingPeerView;->a:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    .line 2099145
    iput-object p1, v0, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->p:LX/7fE;

    .line 2099146
    return-void
.end method
