.class public Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;
.super LX/EHN;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Z

.field private c:Landroid/widget/ImageButton;

.field public d:Landroid/widget/ImageButton;

.field private e:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/rtc/views/CountdownView;

.field public g:LX/EHM;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2099478
    invoke-direct {p0, p1}, LX/EHN;-><init>(Landroid/content/Context;)V

    .line 2099479
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2099480
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a:LX/0Ot;

    .line 2099481
    const-class v0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2099482
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2099473
    invoke-direct {p0, p1, p2}, LX/EHN;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2099474
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2099475
    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a:LX/0Ot;

    .line 2099476
    const-class v0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;

    invoke-static {v0, p0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2099477
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;

    const/16 v1, 0x3266

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a:LX/0Ot;

    return-void
.end method

.method public static e(Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;)V
    .locals 2

    .prologue
    .line 2099468
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->e:Lcom/facebook/resources/ui/FbTextView;

    if-nez v0, :cond_0

    .line 2099469
    :goto_0
    return-void

    .line 2099470
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2099471
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->e:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0807e6

    invoke-virtual {p0, v1}, LX/EHN;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2099472
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->e:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f0807e7

    invoke-virtual {p0, v1}, LX/EHN;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2099483
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->f:Lcom/facebook/rtc/views/CountdownView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/CountdownView;->b()V

    .line 2099484
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2099462
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79G;

    .line 2099463
    const-string v1, "UI_COUNTDOWN_STARTED"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/79G;->a(LX/79G;Ljava/lang/String;LX/1rQ;)V

    .line 2099464
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 2099465
    invoke-static {p0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->e(Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;)V

    .line 2099466
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->f:Lcom/facebook/rtc/views/CountdownView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/CountdownView;->a()V

    .line 2099467
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2099457
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a()V

    .line 2099458
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099459
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->f:Lcom/facebook/rtc/views/CountdownView;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/CountdownView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099460
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099461
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 2099441
    iget-boolean v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->b:Z

    if-eqz v0, :cond_0

    .line 2099442
    :goto_0
    return-void

    .line 2099443
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->b:Z

    .line 2099444
    invoke-virtual {p0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2099445
    if-eqz p1, :cond_1

    const v0, 0x7f03123c

    .line 2099446
    :goto_1
    invoke-virtual {v1, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2099447
    const v0, 0x7f0d2ad0

    invoke-virtual {p0, v0}, LX/EHN;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->c:Landroid/widget/ImageButton;

    .line 2099448
    const v0, 0x7f0d2ace

    invoke-virtual {p0, v0}, LX/EHN;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->d:Landroid/widget/ImageButton;

    .line 2099449
    const v0, 0x7f0d2acf

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 2099450
    const v0, 0x7f0d2acd

    invoke-virtual {p0, v0}, LX/EHN;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/CountdownView;

    iput-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->f:Lcom/facebook/rtc/views/CountdownView;

    .line 2099451
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->c:Landroid/widget/ImageButton;

    new-instance v1, LX/EHJ;

    invoke-direct {v1, p0}, LX/EHJ;-><init>(Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099452
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->d:Landroid/widget/ImageButton;

    new-instance v1, LX/EHK;

    invoke-direct {v1, p0}, LX/EHK;-><init>(Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2099453
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->f:Lcom/facebook/rtc/views/CountdownView;

    new-instance v1, LX/EHL;

    invoke-direct {v1, p0}, LX/EHL;-><init>(Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;)V

    .line 2099454
    iput-object v1, v0, Lcom/facebook/rtc/views/CountdownView;->g:LX/EGm;

    .line 2099455
    goto :goto_0

    .line 2099456
    :cond_1
    const v0, 0x7f031242

    goto :goto_1
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2099440
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->f:Lcom/facebook/rtc/views/CountdownView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/CountdownView;->c()Z

    move-result v0

    return v0
.end method

.method public setDuration(I)V
    .locals 1

    .prologue
    .line 2099438
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->f:Lcom/facebook/rtc/views/CountdownView;

    invoke-virtual {v0, p1}, Lcom/facebook/rtc/views/CountdownView;->setDuration(I)V

    .line 2099439
    return-void
.end method

.method public setListener(LX/EHM;)V
    .locals 0

    .prologue
    .line 2099436
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->g:LX/EHM;

    .line 2099437
    return-void
.end method
