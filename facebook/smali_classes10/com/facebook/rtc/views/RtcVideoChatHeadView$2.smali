.class public final Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/EI9;


# direct methods
.method public constructor <init>(LX/EI9;)V
    .locals 0

    .prologue
    .line 2100495
    iput-object p1, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 2100496
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    invoke-virtual {v0}, LX/EI9;->getViewType()LX/EI8;

    move-result-object v0

    .line 2100497
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2100498
    sget-object v1, LX/EHw;->a:[I

    invoke-virtual {v0}, LX/EI8;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2100499
    :cond_0
    :goto_0
    return-void

    .line 2100500
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    .line 2100501
    iget-boolean v1, v0, LX/EI9;->I:Z

    move v0, v1

    .line 2100502
    if-eqz v0, :cond_0

    .line 2100503
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    iget-object v0, v0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->c()V

    goto :goto_0

    .line 2100504
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    invoke-static {v0}, LX/EI9;->q(LX/EI9;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2100505
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    iget-object v0, v0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->d()V

    goto :goto_0

    .line 2100506
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    iget-object v0, v0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setOneShotDrawListener(LX/7fD;)V

    goto :goto_0

    .line 2100507
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    invoke-static {v0}, LX/EI9;->q(LX/EI9;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2100508
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    iget-object v0, v0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->d()V

    .line 2100509
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    .line 2100510
    iget-boolean v1, v0, LX/EI9;->I:Z

    move v0, v1

    .line 2100511
    if-eqz v0, :cond_0

    .line 2100512
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    iget-object v0, v0, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->c()V

    goto :goto_0

    .line 2100513
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    iget-object v0, v0, LX/EI9;->g:Lcom/facebook/rtc/views/RtcFloatingPeerView;

    iget-object v1, p0, Lcom/facebook/rtc/views/RtcVideoChatHeadView$2;->a:LX/EI9;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcFloatingPeerView;->setOneShotDrawListener(LX/7fD;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
