.class public Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/ECr;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/rtc/expression/RtcVideoExpressionLoader$Listener;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/EGF;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AMO;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/EHZ;

.field private g:LX/0ad;

.field public h:LX/ECw;

.field public i:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2090458
    const-class v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/ECw;LX/0ad;LX/EHZ;Landroid/net/ConnectivityManager;)V
    .locals 8
    .param p2    # LX/ECw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0ad;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/EHZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/net/ConnectivityManager;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2090459
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2090460
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2090461
    iput-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->d:LX/0Ot;

    .line 2090462
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2090463
    iput-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->e:LX/0Ot;

    .line 2090464
    iput-object p1, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->b:Landroid/content/Context;

    .line 2090465
    iput-object p3, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->g:LX/0ad;

    .line 2090466
    iput-object p4, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->f:LX/EHZ;

    .line 2090467
    iput-object p2, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->h:LX/ECw;

    .line 2090468
    invoke-virtual {p0, v1}, LX/1OM;->a(Z)V

    .line 2090469
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->g:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v5, LX/3Dx;->eq:I

    const/4 v6, 0x5

    invoke-interface {v0, v3, v4, v5, v6}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    .line 2090470
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->g:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget v6, LX/3Dx;->er:I

    const/4 v7, 0x3

    invoke-interface {v0, v4, v5, v6, v7}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v4

    .line 2090471
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->g:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-object v6, LX/0c1;->Off:LX/0c1;

    sget v7, LX/3Dx;->es:I

    invoke-interface {v0, v5, v6, v7, v2}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    .line 2090472
    :goto_0
    invoke-virtual {p5, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 2090473
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move v0, v2

    .line 2090474
    :cond_1
    iget-object v1, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->h:LX/ECw;

    .line 2090475
    iget-object v2, v1, LX/ECw;->c:Ljava/util/List;

    invoke-interface {v2, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2090476
    iget-object v1, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->h:LX/ECw;

    const/4 v2, 0x4

    const/16 p0, 0x2c

    .line 2090477
    iget-object v5, v1, LX/ECw;->d:LX/1Zp;

    if-eqz v5, :cond_3

    .line 2090478
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 2090479
    goto :goto_0

    .line 2090480
    :cond_3
    iput-boolean v0, v1, LX/ECw;->f:Z

    .line 2090481
    new-instance v5, LX/EFI;

    invoke-direct {v5}, LX/EFI;-><init>()V

    move-object v5, v5

    .line 2090482
    const-string v6, "limit"

    const/16 v7, 0xa

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2090483
    const-string v6, "thumbnail_height"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2090484
    const-string v6, "thumbnail_width"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2090485
    const-string v6, "thumbnail_scale"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2090486
    const-string v6, "mask_sdk_version"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2090487
    const-string v6, "mask_model_version"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2090488
    const-string v6, "rtc_expression_version"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2090489
    const-string v6, "mask_supported_capabilities"

    invoke-static {}, LX/8Cg;->a()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2090490
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    .line 2090491
    iget-object v6, v1, LX/ECw;->a:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v6

    .line 2090492
    new-instance v7, LX/ECu;

    invoke-direct {v7, v1}, LX/ECu;-><init>(LX/ECw;)V

    iget-object v5, v1, LX/ECw;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v6, v7, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_1
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/0Px;)LX/BA6;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/AMT;",
            ">;)",
            "LX/BA6;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 2090493
    if-nez p4, :cond_0

    move-object v0, v1

    .line 2090494
    :goto_0
    return-object v0

    .line 2090495
    :cond_0
    new-instance v2, LX/BA5;

    invoke-direct {v2}, LX/BA5;-><init>()V

    .line 2090496
    iput-object p1, v2, LX/BA5;->a:Landroid/content/Context;

    .line 2090497
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMO;

    .line 2090498
    iget-object v3, v0, LX/AMO;->c:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    .line 2090499
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2090500
    invoke-virtual {p4}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v6, v4

    :goto_1
    if-ge v6, v8, :cond_3

    invoke-virtual {p4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/AMT;

    .line 2090501
    iget-object p1, v4, LX/AMT;->b:LX/AMS;

    move-object p1, p1

    .line 2090502
    sget-object v0, LX/AMS;->EFFECT:LX/AMS;

    if-eq p1, v0, :cond_1

    .line 2090503
    iget-object p1, v3, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->d:LX/AMf;

    .line 2090504
    iget-boolean v0, v4, LX/AMT;->e:Z

    move v0, v0

    .line 2090505
    if-eqz v0, :cond_2

    .line 2090506
    iget-object v0, v4, LX/AMT;->f:Ljava/lang/String;

    move-object v4, v0

    .line 2090507
    :goto_2
    invoke-virtual {p1, v4}, LX/AMf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2090508
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 2090509
    new-instance p1, Ljava/io/File;

    invoke-direct {p1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2090510
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2090511
    :cond_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_1

    .line 2090512
    :cond_2
    iget-object v0, v4, LX/AMT;->c:Ljava/lang/String;

    move-object v4, v0

    .line 2090513
    goto :goto_2

    .line 2090514
    :cond_3
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/AN2;->a(LX/0Px;)LX/AN2;

    move-result-object v4

    move-object v3, v4

    .line 2090515
    move-object v3, v3

    .line 2090516
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AMO;

    .line 2090517
    iget-object v4, v0, LX/AMO;->c:Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;

    .line 2090518
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p0

    .line 2090519
    invoke-virtual {p4}, LX/0Px;->size()I

    move-result p1

    const/4 v6, 0x0

    move v8, v6

    :goto_3
    if-ge v8, p1, :cond_6

    invoke-virtual {p4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/AMT;

    .line 2090520
    iget-object v7, v6, LX/AMT;->b:LX/AMS;

    move-object v7, v7

    .line 2090521
    sget-object v0, LX/AMS;->EFFECT:LX/AMS;

    if-ne v7, v0, :cond_4

    .line 2090522
    iget-object v0, v4, Lcom/facebook/cameracore/assets/fetch/MsqrdAssetDownloader;->d:LX/AMf;

    .line 2090523
    iget-boolean v7, v6, LX/AMT;->e:Z

    move v7, v7

    .line 2090524
    if-eqz v7, :cond_5

    .line 2090525
    iget-object v7, v6, LX/AMT;->f:Ljava/lang/String;

    move-object v7, v7

    .line 2090526
    :goto_4
    invoke-virtual {v0, v7}, LX/AMf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2090527
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2090528
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2090529
    iget-object v7, v6, LX/AMT;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2090530
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/AN1;->a(Ljava/lang/String;Ljava/lang/String;)LX/AN1;

    move-result-object v6

    .line 2090531
    if-eqz v6, :cond_4

    .line 2090532
    invoke-virtual {p0, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2090533
    :cond_4
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    goto :goto_3

    .line 2090534
    :cond_5
    iget-object v7, v6, LX/AMT;->c:Ljava/lang/String;

    move-object v7, v7

    .line 2090535
    goto :goto_4

    .line 2090536
    :cond_6
    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v4, v6

    .line 2090537
    move-object v0, v4

    .line 2090538
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    if-nez v4, :cond_7

    move-object v0, v1

    .line 2090539
    goto/16 :goto_0

    .line 2090540
    :cond_7
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AN1;

    .line 2090541
    iget-object v1, v0, LX/AN1;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2090542
    iput-object p2, v2, LX/BA5;->m:Ljava/lang/String;

    .line 2090543
    iput-object p3, v2, LX/BA5;->n:Ljava/lang/String;

    .line 2090544
    iput-object v0, v2, LX/BA5;->f:Ljava/lang/String;

    .line 2090545
    iget-object v0, v3, LX/AN2;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2090546
    iget-object v1, v3, LX/AN2;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2090547
    iget-object v4, v3, LX/AN2;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2090548
    invoke-virtual {v2, v0, v1, v3, v5}, LX/BA5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/BA5;

    .line 2090549
    invoke-virtual {v2}, LX/BA5;->a()LX/BA6;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2090550
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGF;

    invoke-virtual {v0}, LX/EGF;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2090551
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031235

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2090552
    new-instance v1, LX/ECr;

    invoke-direct {v1, p0, v0}, LX/ECr;-><init>(Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public final bridge synthetic a(LX/1a1;)V
    .locals 0

    .prologue
    .line 2090553
    return-void
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2090554
    check-cast p1, LX/ECr;

    .line 2090555
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGF;

    const/16 p0, 0x8

    const/4 v1, 0x0

    .line 2090556
    iput-object v0, p1, LX/ECr;->u:LX/EGF;

    .line 2090557
    const-class v2, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    .line 2090558
    iget-object v3, p1, LX/ECr;->u:LX/EGF;

    iget-object v3, v3, LX/EGF;->e:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2090559
    iget-object v3, p1, LX/ECr;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, p1, LX/ECr;->u:LX/EGF;

    iget-object v4, v4, LX/EGF;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2090560
    iget-object v2, p1, LX/ECr;->p:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setVisibility(I)V

    .line 2090561
    iget-object v2, p1, LX/ECr;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2090562
    :goto_0
    iput p2, p1, LX/ECr;->v:I

    .line 2090563
    iget-object v2, p1, LX/ECr;->l:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    iget-object v2, v2, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->f:LX/EHZ;

    .line 2090564
    iget v3, v2, LX/EHZ;->r:I

    move v2, v3

    .line 2090565
    iget v3, p1, LX/ECr;->v:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, p1, LX/ECr;->t:Z

    .line 2090566
    invoke-static {p1}, LX/ECr;->x(LX/ECr;)V

    .line 2090567
    iget-object v1, p1, LX/ECr;->m:Landroid/view/View;

    new-instance v2, LX/ECq;

    invoke-direct {v2, p1}, LX/ECq;-><init>(LX/ECr;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2090568
    return-void

    .line 2090569
    :cond_1
    iget-object v3, p1, LX/ECr;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2090570
    iget-object v2, p1, LX/ECr;->p:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2090571
    iget-object v2, p1, LX/ECr;->o:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 2090572
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->h:LX/ECw;

    .line 2090573
    iget-object p1, v0, LX/ECw;->c:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2090574
    return-void
.end method

.method public final e(I)V
    .locals 5

    .prologue
    .line 2090575
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EGF;

    .line 2090576
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->h:LX/ECw;

    invoke-virtual {v1, v0}, LX/ECw;->a(LX/EGF;)LX/ECv;

    move-result-object v1

    sget-object v2, LX/ECv;->COMPLETED:LX/ECv;

    if-ne v1, v2, :cond_1

    .line 2090577
    :cond_0
    iget-object v1, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->f:LX/EHZ;

    iget-object v2, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->b:Landroid/content/Context;

    iget-object v3, v0, LX/EGF;->b:Ljava/lang/String;

    iget-object v4, v0, LX/EGF;->c:Ljava/lang/String;

    iget-object v0, v0, LX/EGF;->a:LX/0Px;

    invoke-direct {p0, v2, v3, v4, v0}, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;LX/0Px;)LX/BA6;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, LX/EHZ;->a(ILX/BA6;)V

    .line 2090578
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bh()V

    .line 2090579
    :goto_0
    return-void

    .line 2090580
    :cond_1
    sget-object v0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->a:Ljava/lang/String;

    const-string v1, "Attempting to apply effect that is not downloaded."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2090581
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    if-eqz v0, :cond_0

    .line 2090582
    iget-object v0, p0, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2090583
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
