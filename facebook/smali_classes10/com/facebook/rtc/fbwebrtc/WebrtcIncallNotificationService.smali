.class public Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;
.super LX/0te;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Z

.field private d:Ljava/lang/String;

.field public e:LX/0aU;

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2091149
    const-class v0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->a:Ljava/lang/String;

    .line 2091150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FAIL_TO_FETCH_PEER_NAME_FOR_INCALL_NOTIFICATION"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2091215
    invoke-direct {p0}, LX/0te;-><init>()V

    .line 2091216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->c:Z

    .line 2091217
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2091218
    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->f:LX/0Ot;

    return-void
.end method

.method private a()Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 2091212
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->e:LX/0aU;

    const-string v1, "RTC_SHOW_CALL_UI"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2091213
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2091214
    const/4 v0, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v0, v1, v2}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;LX/0Ot;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;",
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2091211
    iput-object p1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->f:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->g:LX/03V;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;

    invoke-static {v1}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v0

    check-cast v0, LX/0aU;

    iput-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->e:LX/0aU;

    const/16 v0, 0x3257

    invoke-static {v1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0, v2, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->a(Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;LX/0Ot;LX/03V;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2091202
    if-nez p1, :cond_0

    .line 2091203
    sget-object v0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->b:Ljava/lang/String;

    const-string v1, "Peer name is empty for incall notification."

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 2091204
    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->g:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 2091205
    const v0, 0x7f080725

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2091206
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2091207
    :cond_1
    const/16 v0, 0x4e22

    invoke-direct {p0, p1}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->b(Ljava/lang/String;)Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->startForeground(ILandroid/app/Notification;)V

    .line 2091208
    iput-object p1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->d:Ljava/lang/String;

    .line 2091209
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->c:Z

    .line 2091210
    :cond_2
    return-void
.end method

.method private b(Ljava/lang/String;)Landroid/app/Notification;
    .locals 5

    .prologue
    .line 2091179
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->a()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 2091180
    invoke-direct {p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->c()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V

    .line 2091181
    invoke-direct {p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->b()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->cancel()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2091182
    :goto_0
    invoke-direct {p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->a()Landroid/app/PendingIntent;

    move-result-object v3

    .line 2091183
    const v0, 0x7f08070d

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2091184
    const v0, 0x7f0806f6

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2091185
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2091186
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2091187
    const v0, 0x7f08070f

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2091188
    const v0, 0x7f0806f7

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2091189
    :goto_1
    new-instance v2, LX/2HB;

    invoke-direct {v2, p0}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, p1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v1

    .line 2091190
    iput-object v3, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 2091191
    move-object v1, v1

    .line 2091192
    const v2, 0x7f021a9a

    invoke-direct {p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->b()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/2HB;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)LX/2HB;

    .line 2091193
    const/4 v0, 0x2

    .line 2091194
    iput v0, v1, LX/2HB;->j:I

    .line 2091195
    move-object v0, v1

    .line 2091196
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/2HB;->a(J)LX/2HB;

    .line 2091197
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-le v0, v2, :cond_1

    .line 2091198
    const v0, 0x7f021abe

    invoke-virtual {v1, v0}, LX/2HB;->a(I)LX/2HB;

    .line 2091199
    :goto_2
    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 2091200
    :cond_0
    const v0, 0x7f08070e

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_1

    .line 2091201
    :cond_1
    const v0, 0x7f021ab9

    invoke-virtual {v1, v0}, LX/2HB;->a(I)LX/2HB;

    goto :goto_2

    :catch_0
    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method private b()Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 2091176
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->e:LX/0aU;

    const-string v1, "RTC_END_CALL_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2091177
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2091178
    const/4 v0, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v0, v1, v2}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private c()Landroid/app/PendingIntent;
    .locals 8

    .prologue
    .line 2091164
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->e:LX/0aU;

    const-string v1, "RTC_SHOW_THREAD_VIEW_ACTION"

    invoke-virtual {v0, v1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2091165
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2091166
    const-string v2, "IS_CONFERENCE_CALL"

    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2091167
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2091168
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2091169
    iget-object v2, v0, LX/EDx;->al:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v2

    .line 2091170
    if-eqz v0, :cond_0

    .line 2091171
    const-string v2, "THREAD_ID"

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2091172
    :cond_0
    :goto_0
    const/4 v0, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v0, v1, v2}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0

    .line 2091173
    :cond_1
    const-string v2, "CONTACT_ID"

    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2091174
    iget-wide v6, v0, LX/EDx;->ak:J

    move-wide v4, v6

    .line 2091175
    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2091163
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onFbCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x5f213ec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2091160
    invoke-super {p0}, LX/0te;->onFbCreate()V

    .line 2091161
    invoke-static {p0, p0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2091162
    const/16 v1, 0x25

    const v2, 0xaf16ce5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFbStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x34db7801    # -1.0782719E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2091151
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2091152
    iget v2, v0, LX/EDx;->am:I

    move v0, v2

    .line 2091153
    if-nez v0, :cond_0

    .line 2091154
    invoke-virtual {p0, p3}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->stopSelfResult(I)Z

    .line 2091155
    const/16 v0, 0x25

    const v2, 0x69c789f1

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2091156
    :goto_0
    return v3

    .line 2091157
    :cond_0
    const-string v0, "CONTACT_NAME"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2091158
    invoke-direct {p0, v0}, Lcom/facebook/rtc/fbwebrtc/WebrtcIncallNotificationService;->a(Ljava/lang/String;)V

    .line 2091159
    const v0, 0x3afcb973

    invoke-static {v0, v1}, LX/02F;->d(II)V

    goto :goto_0
.end method
