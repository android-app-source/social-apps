.class public final Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$36;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;)V
    .locals 0

    .prologue
    .line 2091641
    iput-object p1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$36;->a:LX/EDx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2091642
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$36;->a:LX/EDx;

    const/4 v2, 0x1

    .line 2091643
    iget-boolean v1, v0, LX/EDx;->bu:Z

    if-eqz v1, :cond_0

    .line 2091644
    sget-object v1, LX/03R;->YES:LX/03R;

    iput-object v1, v0, LX/EDx;->bv:LX/03R;

    .line 2091645
    invoke-virtual {v0}, LX/EDx;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2091646
    iput-boolean v2, v0, LX/EDx;->bz:Z

    .line 2091647
    :cond_0
    invoke-virtual {v0}, LX/EDx;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v1, :cond_1

    .line 2091648
    iget-object v1, v0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2091649
    const/4 v1, 0x0

    iput-object v1, v0, LX/EDx;->bT:Ljava/util/concurrent/ScheduledFuture;

    .line 2091650
    :cond_1
    iget-object v1, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_2

    iget-object v1, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EC0;

    .line 2091651
    invoke-virtual {v1}, LX/EC0;->m()V

    .line 2091652
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2091653
    :cond_2
    return-void
.end method
