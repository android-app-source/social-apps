.class public final Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/webrtc/ConferenceCall;

.field public final synthetic b:[J

.field public final synthetic c:[Ljava/lang/String;

.field public final synthetic d:[Ljava/lang/String;

.field public final synthetic e:[I

.field public final synthetic f:[Z

.field public final synthetic g:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;Lcom/facebook/webrtc/ConferenceCall;[J[Ljava/lang/String;[Ljava/lang/String;[I[Z)V
    .locals 0

    .prologue
    .line 2091804
    iput-object p1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->g:LX/EDx;

    iput-object p2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->a:Lcom/facebook/webrtc/ConferenceCall;

    iput-object p3, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->b:[J

    iput-object p4, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->c:[Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->d:[Ljava/lang/String;

    iput-object p6, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->e:[I

    iput-object p7, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->f:[Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 2091805
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->g:LX/EDx;

    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->a:Lcom/facebook/webrtc/ConferenceCall;

    iget-object v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->b:[J

    iget-object v3, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->c:[Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->d:[Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->e:[I

    iget-object v6, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$52;->f:[Z

    .line 2091806
    iget-boolean v7, v0, LX/EDx;->ao:Z

    move v7, v7

    .line 2091807
    if-eqz v7, :cond_0

    invoke-virtual {v0}, LX/EDx;->h()Z

    move-result v7

    if-nez v7, :cond_1

    .line 2091808
    :cond_0
    :goto_0
    return-void

    .line 2091809
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/webrtc/ConferenceCall;->a()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2091810
    iget-object v7, v0, LX/EDx;->ae:Ljava/util/Map;

    if-eqz v7, :cond_0

    .line 2091811
    iget-object v7, v0, LX/EDx;->cy:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/79G;

    .line 2091812
    iget-boolean v8, v7, LX/79G;->d:Z

    if-eqz v8, :cond_1c

    .line 2091813
    :goto_1
    const/4 v8, 0x0

    .line 2091814
    const/4 v7, 0x0

    move v9, v8

    move v8, v7

    :goto_2
    array-length v7, v2

    if-ge v8, v7, :cond_7

    .line 2091815
    iget-object v7, v0, LX/EDx;->ae:Ljava/util/Map;

    aget-object v10, v3, v8

    invoke-interface {v7, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/EGE;

    .line 2091816
    if-eqz v7, :cond_3

    .line 2091817
    iget-object v10, v0, LX/EDx;->i:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2091818
    iget-object v11, v10, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v10, v11

    .line 2091819
    iget-object v11, v7, LX/EGE;->b:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 2091820
    aget v10, v5, v8

    const/4 v11, 0x1

    if-ne v10, v11, :cond_6

    .line 2091821
    aget-boolean v10, v6, v8

    if-eqz v10, :cond_4

    aget-object v10, v4, v8

    invoke-static {v10}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    const/4 v10, 0x1

    .line 2091822
    :goto_3
    if-nez v9, :cond_2

    if-eqz v10, :cond_5

    :cond_2
    const/4 v9, 0x1

    .line 2091823
    :goto_4
    const/4 v11, 0x1

    iput-boolean v11, v0, LX/EDx;->ap:Z

    .line 2091824
    aget-wide v11, v2, v8

    iput-wide v11, v7, LX/EGE;->h:J

    .line 2091825
    aget-object v11, v4, v8

    iput-object v11, v7, LX/EGE;->g:Ljava/lang/String;

    .line 2091826
    iput-boolean v10, v7, LX/EGE;->i:Z

    .line 2091827
    :cond_3
    :goto_5
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_2

    .line 2091828
    :cond_4
    const/4 v10, 0x0

    goto :goto_3

    .line 2091829
    :cond_5
    const/4 v9, 0x0

    goto :goto_4

    .line 2091830
    :cond_6
    aget v10, v5, v8

    if-nez v10, :cond_3

    .line 2091831
    aget-boolean v10, v6, v8

    iput-boolean v10, v7, LX/EGE;->j:Z

    goto :goto_5

    .line 2091832
    :cond_7
    iget-object v7, v0, LX/EDx;->af:LX/EGE;

    if-eqz v7, :cond_8

    .line 2091833
    iget-object v7, v0, LX/EDx;->af:LX/EGE;

    iget-wide v7, v7, LX/EGE;->h:J

    iput-wide v7, v0, LX/EDx;->aw:J

    .line 2091834
    :cond_8
    if-nez v9, :cond_9

    iget-boolean v7, v0, LX/EDx;->bX:Z

    if-eqz v7, :cond_b

    :cond_9
    const/4 v7, 0x1

    :goto_6
    iput-boolean v7, v0, LX/EDx;->bX:Z

    .line 2091835
    iput-boolean v9, v0, LX/EDx;->as:Z

    .line 2091836
    invoke-virtual {v0}, LX/EDx;->N()Z

    move-result v7

    if-nez v7, :cond_a

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v7

    if-eqz v7, :cond_c

    :cond_a
    const/4 v7, 0x1

    :goto_7
    invoke-static {v7}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v7

    iput-object v7, v0, LX/EDx;->bv:LX/03R;

    .line 2091837
    iget-object v7, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    const/4 v7, 0x0

    move v8, v7

    :goto_8
    if-ge v8, v9, :cond_0

    iget-object v7, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v7, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/EC0;

    .line 2091838
    invoke-virtual {v7}, LX/EC0;->u()V

    .line 2091839
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_8

    .line 2091840
    :cond_b
    const/4 v7, 0x0

    goto :goto_6

    .line 2091841
    :cond_c
    const/4 v7, 0x0

    goto :goto_7

    .line 2091842
    :cond_d
    const/4 v7, 0x0

    .line 2091843
    array-length v9, v2

    const/4 v8, 0x0

    :goto_9
    if-ge v8, v9, :cond_e

    aget-wide v11, v2, v8

    .line 2091844
    iget-wide v13, v0, LX/EDx;->aw:J

    cmp-long v10, v11, v13

    if-nez v10, :cond_13

    .line 2091845
    const/4 v7, 0x1

    .line 2091846
    :cond_e
    if-nez v7, :cond_f

    .line 2091847
    const-wide/16 v7, 0x0

    iput-wide v7, v0, LX/EDx;->aw:J

    .line 2091848
    :cond_f
    iget-boolean v10, v0, LX/EDx;->as:Z

    .line 2091849
    const/4 v9, 0x0

    .line 2091850
    const/4 v8, 0x0

    .line 2091851
    const/4 v7, 0x0

    :goto_a
    array-length v11, v2

    if-ge v7, v11, :cond_1b

    .line 2091852
    aget v11, v5, v7

    const/4 v12, 0x1

    if-ne v11, v12, :cond_14

    .line 2091853
    iget-wide v11, v0, LX/EDx;->aw:J

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-eqz v11, :cond_10

    iget-wide v11, v0, LX/EDx;->aw:J

    aget-wide v13, v2, v7

    cmp-long v11, v11, v13

    if-nez v11, :cond_14

    .line 2091854
    :cond_10
    iget-wide v11, v0, LX/EDx;->aw:J

    const-wide/16 v13, 0x0

    cmp-long v8, v11, v13

    if-nez v8, :cond_11

    .line 2091855
    aget-wide v11, v2, v7

    iput-wide v11, v0, LX/EDx;->aw:J

    .line 2091856
    const/4 v8, 0x1

    iput-boolean v8, v0, LX/EDx;->ap:Z

    .line 2091857
    aget-object v8, v4, v7

    iput-object v8, v0, LX/EDx;->av:Ljava/lang/String;

    .line 2091858
    :cond_11
    aget-boolean v8, v6, v7

    .line 2091859
    const/4 v7, 0x1

    .line 2091860
    :goto_b
    if-eqz v7, :cond_0

    .line 2091861
    if-eqz v8, :cond_17

    .line 2091862
    const/4 v7, 0x1

    iput-boolean v7, v0, LX/EDx;->as:Z

    .line 2091863
    const/4 v7, 0x1

    iput-boolean v7, v0, LX/EDx;->bX:Z

    .line 2091864
    iget-object v7, v0, LX/EDx;->bL:Landroid/view/View;

    if-eqz v7, :cond_12

    .line 2091865
    iget-wide v7, v0, LX/EDx;->aw:J

    iget-object v9, v0, LX/EDx;->bL:Landroid/view/View;

    invoke-virtual {v1, v7, v8, v9}, Lcom/facebook/webrtc/ConferenceCall;->setRendererWindow(JLandroid/view/View;)V

    .line 2091866
    :cond_12
    iget-object v7, v0, LX/EDx;->bv:LX/03R;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, LX/03R;->asBoolean(Z)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 2091867
    iget-object v7, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    const/4 v7, 0x0

    move v8, v7

    :goto_c
    if-ge v8, v9, :cond_0

    iget-object v7, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v7, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/EC0;

    .line 2091868
    invoke-virtual {v7}, LX/EC0;->k()V

    .line 2091869
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_c

    .line 2091870
    :cond_13
    add-int/lit8 v8, v8, 0x1

    goto :goto_9

    .line 2091871
    :cond_14
    add-int/lit8 v7, v7, 0x1

    goto :goto_a

    .line 2091872
    :cond_15
    iget-boolean v7, v0, LX/EDx;->bt:Z

    if-eqz v7, :cond_16

    .line 2091873
    const/4 v7, 0x1

    invoke-static {v0, v7}, LX/EDx;->v(LX/EDx;Z)V

    goto/16 :goto_0

    .line 2091874
    :cond_16
    const/4 v7, 0x1

    invoke-static {v0, v7}, LX/EDx;->u(LX/EDx;Z)V

    goto/16 :goto_0

    .line 2091875
    :cond_17
    const/4 v7, 0x0

    iput-boolean v7, v0, LX/EDx;->as:Z

    .line 2091876
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, LX/EDx;->b(Landroid/view/View;)V

    .line 2091877
    iget-object v7, v0, LX/EDx;->bv:LX/03R;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, LX/03R;->asBoolean(Z)Z

    move-result v7

    if-eqz v7, :cond_18

    .line 2091878
    iget-object v7, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    const/4 v7, 0x0

    move v8, v7

    :goto_d
    if-ge v8, v9, :cond_0

    iget-object v7, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v7, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/EC0;

    .line 2091879
    invoke-virtual {v7}, LX/EC0;->l()V

    .line 2091880
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_d

    .line 2091881
    :cond_18
    if-nez v10, :cond_19

    iget-boolean v7, v0, LX/EDx;->bt:Z

    if-eqz v7, :cond_19

    .line 2091882
    const/4 v7, 0x0

    invoke-static {v0, v7}, LX/EDx;->v(LX/EDx;Z)V

    goto/16 :goto_0

    .line 2091883
    :cond_19
    if-eqz v10, :cond_1a

    iget-boolean v7, v0, LX/EDx;->bt:Z

    if-nez v7, :cond_1a

    .line 2091884
    const/4 v7, 0x0

    invoke-static {v0, v7}, LX/EDx;->u(LX/EDx;Z)V

    goto/16 :goto_0

    .line 2091885
    :cond_1a
    const-string v7, "WebrtcUiHandler"

    const-string v8, "Unexpected state transition: isPreviousOn: %b, mRequestingVideo: %b"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v9, v11

    const/4 v10, 0x1

    iget-boolean v11, v0, LX/EDx;->bt:Z

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v8, v9}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_1b
    move v7, v8

    move v8, v9

    goto/16 :goto_b

    .line 2091886
    :cond_1c
    const-string v8, "RTC_ENGINE_IN_CALL"

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, LX/79G;->a(LX/79G;Ljava/lang/String;LX/1rQ;)V

    .line 2091887
    const/4 v8, 0x1

    iput-boolean v8, v7, LX/79G;->d:Z

    goto/16 :goto_1
.end method
