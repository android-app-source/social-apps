.class public final Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$42;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:J

.field public final synthetic d:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 2091684
    iput-object p1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$42;->d:LX/EDx;

    iput-object p2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$42;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$42;->b:Ljava/lang/String;

    iput-wide p4, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$42;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2091685
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$42;->d:LX/EDx;

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 2091686
    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2091687
    :cond_0
    return-void

    .line 2091688
    :cond_1
    iget-boolean v2, v0, LX/EDx;->as:Z

    if-eqz v2, :cond_0

    .line 2091689
    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2091690
    iget-boolean v2, v0, LX/EDx;->aB:Z

    move v2, v2

    .line 2091691
    if-nez v2, :cond_2

    invoke-virtual {v0}, LX/EDx;->t()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2091692
    sget-object v2, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    invoke-virtual {v0, v2}, LX/EDx;->a(LX/7TQ;)V

    .line 2091693
    sget-object v2, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, LX/EDx;->a$redex0(LX/EDx;LX/7TQ;Z)V

    .line 2091694
    :cond_2
    iput-boolean v1, v0, LX/EDx;->as:Z

    .line 2091695
    iput-object v5, v0, LX/EDx;->av:Ljava/lang/String;

    .line 2091696
    const-wide/16 v3, 0x0

    iput-wide v3, v0, LX/EDx;->aw:J

    .line 2091697
    invoke-virtual {v0, v5}, LX/EDx;->b(Landroid/view/View;)V

    .line 2091698
    iget-object v2, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v1, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EC0;

    .line 2091699
    invoke-virtual {v1}, LX/EC0;->l()V

    .line 2091700
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method
