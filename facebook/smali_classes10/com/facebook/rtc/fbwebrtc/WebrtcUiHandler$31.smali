.class public final Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:J

.field public final synthetic d:LX/EDx;


# direct methods
.method public constructor <init>(LX/EDx;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 2091611
    iput-object p1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;->d:LX/EDx;

    iput-object p2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;->b:Ljava/lang/String;

    iput-wide p4, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2091612
    iget-object v0, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;->d:LX/EDx;

    iget-object v1, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;->b:Ljava/lang/String;

    iget-wide v2, p0, Lcom/facebook/rtc/fbwebrtc/WebrtcUiHandler$31;->c:J

    const/4 v5, 0x1

    .line 2091613
    invoke-virtual {v0}, LX/EDx;->aG()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2091614
    sget-object v4, LX/03R;->YES:LX/03R;

    iput-object v4, v0, LX/EDx;->bv:LX/03R;

    .line 2091615
    :cond_0
    iget-boolean v4, v0, LX/EDx;->ao:Z

    move v4, v4

    .line 2091616
    if-eqz v4, :cond_2

    .line 2091617
    iget-boolean v4, v0, LX/EDx;->aB:Z

    move v4, v4

    .line 2091618
    if-eqz v4, :cond_1

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2091619
    sget-object v4, LX/EDu;->RECIPROCATED:LX/EDu;

    iput-object v4, v0, LX/EDx;->bR:LX/EDu;

    .line 2091620
    :cond_1
    iput-boolean v5, v0, LX/EDx;->as:Z

    .line 2091621
    iput-boolean v5, v0, LX/EDx;->bX:Z

    .line 2091622
    iput-boolean v5, v0, LX/EDx;->ap:Z

    .line 2091623
    iput-object v1, v0, LX/EDx;->av:Ljava/lang/String;

    .line 2091624
    iput-wide v2, v0, LX/EDx;->aw:J

    .line 2091625
    iget-object v4, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_2

    iget-object v4, v0, LX/EDx;->P:LX/0Px;

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EC0;

    .line 2091626
    invoke-virtual {v4}, LX/EC0;->k()V

    .line 2091627
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2091628
    :cond_2
    return-void
.end method
