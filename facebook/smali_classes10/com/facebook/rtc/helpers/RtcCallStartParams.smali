.class public Lcom/facebook/rtc/helpers/RtcCallStartParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/rtc/helpers/RtcCallStartParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:Z

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:LX/EFe;

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final l:J

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2096123
    new-instance v0, LX/EFd;

    invoke-direct {v0}, LX/EFd;-><init>()V

    sput-object v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(JJLjava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/EFe;JLX/0Px;LX/0Px;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/EFe;",
            "J",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2096107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096108
    iput-wide p1, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    .line 2096109
    iput-wide p3, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->b:J

    .line 2096110
    iput-object p5, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c:Ljava/lang/String;

    .line 2096111
    iput-object p6, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    .line 2096112
    iput-wide p7, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->e:J

    .line 2096113
    iput-boolean p9, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    .line 2096114
    iput-object p10, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->g:Ljava/lang/String;

    .line 2096115
    iput-object p11, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->h:Ljava/lang/String;

    .line 2096116
    iput-object p12, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->i:Ljava/lang/String;

    .line 2096117
    iput-object p13, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    .line 2096118
    iput-wide p14, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->l:J

    .line 2096119
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->m:LX/0Px;

    .line 2096120
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->n:LX/0Px;

    .line 2096121
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->o:Ljava/lang/String;

    .line 2096122
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2096080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2096081
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    .line 2096082
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->b:J

    .line 2096083
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c:Ljava/lang/String;

    .line 2096084
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    .line 2096085
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->e:J

    .line 2096086
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    .line 2096087
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->g:Ljava/lang/String;

    .line 2096088
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->h:Ljava/lang/String;

    .line 2096089
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->i:Ljava/lang/String;

    .line 2096090
    const-class v0, LX/EFe;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/EFe;

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    .line 2096091
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2096092
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 2096093
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2096094
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->k:LX/0Px;

    .line 2096095
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->l:J

    .line 2096096
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 2096097
    if-nez v0, :cond_2

    .line 2096098
    iput-object v2, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->m:LX/0Px;

    .line 2096099
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 2096100
    if-nez v0, :cond_3

    .line 2096101
    iput-object v2, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->n:LX/0Px;

    .line 2096102
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->o:Ljava/lang/String;

    .line 2096103
    return-void

    .line 2096104
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2096105
    :cond_2
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->m:LX/0Px;

    goto :goto_1

    .line 2096106
    :cond_3
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->n:LX/0Px;

    goto :goto_2
.end method

.method public static a(JLjava/lang/String;JZLjava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;
    .locals 20

    .prologue
    .line 2096079
    new-instance v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    sget-object v14, LX/EFe;->REGULAR:LX/EFe;

    const-wide/16 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-wide/from16 v2, p0

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move/from16 v10, p5

    move-object/from16 v13, p6

    invoke-direct/range {v1 .. v19}, Lcom/facebook/rtc/helpers/RtcCallStartParams;-><init>(JJLjava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/EFe;JLX/0Px;LX/0Px;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(JLjava/lang/String;JZLjava/lang/String;Ljava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;
    .locals 20

    .prologue
    .line 2096077
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p0 .. p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v17

    .line 2096078
    new-instance v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const/4 v12, 0x0

    sget-object v14, LX/EFe;->REGULAR:LX/EFe;

    const-wide/16 v15, 0x0

    const/16 v19, 0x0

    move-wide/from16 v2, p0

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move/from16 v10, p5

    move-object/from16 v11, p7

    move-object/from16 v13, p6

    move-object/from16 v18, v17

    invoke-direct/range {v1 .. v19}, Lcom/facebook/rtc/helpers/RtcCallStartParams;-><init>(JJLjava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/EFe;JLX/0Px;LX/0Px;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(JLjava/lang/String;Z)Lcom/facebook/rtc/helpers/RtcCallStartParams;
    .locals 20

    .prologue
    .line 2096076
    new-instance v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-string v13, ""

    sget-object v14, LX/EFe;->REGULAR:LX/EFe;

    const-wide/16 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-wide/from16 v2, p0

    move-object/from16 v7, p2

    move/from16 v10, p3

    invoke-direct/range {v1 .. v19}, Lcom/facebook/rtc/helpers/RtcCallStartParams;-><init>(JJLjava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/EFe;JLX/0Px;LX/0Px;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Lcom/facebook/rtc/helpers/RtcCallStartParams;Ljava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;
    .locals 22

    .prologue
    .line 2096075
    new-instance v3, Lcom/facebook/rtc/helpers/RtcCallStartParams;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->b:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->e:J

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->l:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->m:LX/0Px;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->n:LX/0Px;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->o:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v9, p1

    invoke-direct/range {v3 .. v21}, Lcom/facebook/rtc/helpers/RtcCallStartParams;-><init>(JJLjava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/EFe;JLX/0Px;LX/0Px;Ljava/lang/String;)V

    return-object v3
.end method

.method public static a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/rtc/helpers/RtcCallStartParams;"
        }
    .end annotation

    .prologue
    .line 2096074
    new-instance v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;

    const-wide/16 v2, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    const-string v13, ""

    sget-object v14, LX/EFe;->GROUP_CALL_JOIN:LX/EFe;

    const-wide/16 v15, 0x0

    move-object/from16 v7, p0

    move/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object/from16 v17, p4

    move-object/from16 v18, p5

    move-object/from16 v19, p6

    invoke-direct/range {v1 .. v19}, Lcom/facebook/rtc/helpers/RtcCallStartParams;-><init>(JJLjava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/EFe;JLX/0Px;LX/0Px;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/0Px;Ljava/lang/String;)Lcom/facebook/rtc/helpers/RtcCallStartParams;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/rtc/helpers/RtcCallStartParams;"
        }
    .end annotation

    .prologue
    .line 2096073
    new-instance v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;

    const-wide/16 v2, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const-wide/16 v8, 0x0

    sget-object v14, LX/EFe;->GROUP_CALL_START:LX/EFe;

    const-wide/16 v15, 0x0

    move-object/from16 v7, p0

    move/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    move-object/from16 v17, p5

    move-object/from16 v18, p6

    move-object/from16 v19, p7

    invoke-direct/range {v1 .. v19}, Lcom/facebook/rtc/helpers/RtcCallStartParams;-><init>(JJLjava/lang/String;Ljava/lang/String;JZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/EFe;JLX/0Px;LX/0Px;Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 2096072
    iget-wide v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2096052
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    sget-object v1, LX/EFe;->INSTANT_VIDEO:LX/EFe;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2096071
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    sget-object v1, LX/EFe;->PSTN_UPSELL:LX/EFe;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2096070
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2096053
    iget-wide v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2096054
    iget-wide v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2096055
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096056
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096057
    iget-wide v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2096058
    iget-boolean v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2096059
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096060
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096061
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096062
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->j:LX/EFe;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 2096063
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->k:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2096064
    iget-wide v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->l:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2096065
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->m:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2096066
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->n:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2096067
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2096068
    return-void

    .line 2096069
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
