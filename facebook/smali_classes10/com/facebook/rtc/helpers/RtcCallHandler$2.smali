.class public final Lcom/facebook/rtc/helpers/RtcCallHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/helpers/RtcCallStartParams;

.field public final synthetic b:LX/3A0;


# direct methods
.method public constructor <init>(LX/3A0;Lcom/facebook/rtc/helpers/RtcCallStartParams;)V
    .locals 0

    .prologue
    .line 2096008
    iput-object p1, p0, Lcom/facebook/rtc/helpers/RtcCallHandler$2;->b:LX/3A0;

    iput-object p2, p0, Lcom/facebook/rtc/helpers/RtcCallHandler$2;->a:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 2096009
    iget-object v0, p0, Lcom/facebook/rtc/helpers/RtcCallHandler$2;->b:LX/3A0;

    iget-object v0, v0, LX/3A0;->A:LX/3E7;

    iget-object v1, p0, Lcom/facebook/rtc/helpers/RtcCallHandler$2;->a:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-wide v2, v1, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a:J

    .line 2096010
    invoke-static {v0}, LX/3E7;->c(LX/3E7;)V

    .line 2096011
    iget-object v4, v0, LX/3E7;->k:Ljava/util/Random;

    const v5, 0x7fffffff

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    .line 2096012
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "{\"type\":\"ping\",\"call_id\":0,\"msg_id\":"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",\"version\":1}"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 2096013
    iget-object v5, v0, LX/3E7;->f:LX/3E8;

    const-wide/16 v8, 0x0

    int-to-long v10, v4

    move-wide v6, v2

    invoke-virtual/range {v5 .. v12}, LX/3E8;->sendToPeer(JJJLjava/lang/String;)Z

    .line 2096014
    return-void
.end method
