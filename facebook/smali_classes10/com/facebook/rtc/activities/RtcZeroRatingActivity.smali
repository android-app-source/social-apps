.class public Lcom/facebook/rtc/activities/RtcZeroRatingActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final t:Ljava/lang/Class;


# instance fields
.field public p:LX/3A0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2S7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/121;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/6c4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2087733
    const-class v0, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    sput-object v0, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->t:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2087734
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2087735
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2087736
    iput-object v0, p0, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->u:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2087737
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2087738
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v1, p0

    check-cast v1, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    invoke-static {p1}, LX/3A0;->a(LX/0QB;)LX/3A0;

    move-result-object v2

    check-cast v2, LX/3A0;

    invoke-static {p1}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object v3

    check-cast v3, LX/2S7;

    invoke-static {p1}, LX/128;->b(LX/0QB;)LX/128;

    move-result-object v5

    check-cast v5, LX/121;

    invoke-static {p1}, LX/6c4;->a(LX/0QB;)LX/6c4;

    move-result-object v6

    check-cast v6, LX/6c4;

    const/16 v0, 0x3257

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, v1, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->p:LX/3A0;

    iput-object v3, v1, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->q:LX/2S7;

    iput-object v5, v1, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->r:LX/121;

    iput-object v6, v1, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->s:LX/6c4;

    iput-object p1, v1, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->u:LX/0Ot;

    .line 2087739
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_DIRECT_VIDEO"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 2087740
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_AFTER_INCOMING_CALL_SCREEN"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 2087741
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_ENABLE_SELF_VIEW"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 2087742
    if-eqz v3, :cond_0

    .line 2087743
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x80080

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2087744
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->s:LX/6c4;

    invoke-virtual {v0}, LX/6c4;->b()V

    .line 2087745
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_CALL_PARAMS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;

    .line 2087746
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 2087747
    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2087748
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2087749
    :cond_1
    const/4 v1, 0x0

    .line 2087750
    const-string v6, "ACTION_START_CALL"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2087751
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/rtc/helpers/RtcCallStartParams;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2087752
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2087753
    :cond_3
    new-instance v1, LX/EBd;

    invoke-direct {v1, p0, v0}, LX/EBd;-><init>(Lcom/facebook/rtc/activities/RtcZeroRatingActivity;Lcom/facebook/rtc/helpers/RtcCallStartParams;)V

    move-object v0, v1

    .line 2087754
    :goto_0
    iget-object v1, p0, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->r:LX/121;

    sget-object v2, LX/0yY;->VOIP_CALL_INTERSTITIAL:LX/0yY;

    .line 2087755
    const v3, 0x7f0807d7

    invoke-virtual {p0, v3}, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2087756
    const v4, 0x7f080787

    invoke-virtual {p0, v4}, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 2087757
    invoke-virtual {v1, v2, v3, v4, v0}, LX/121;->a(LX/0yY;Ljava/lang/String;Ljava/lang/String;LX/39A;)LX/121;

    .line 2087758
    iget-object v0, p0, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->r:LX/121;

    sget-object v1, LX/0yY;->VOIP_CALL_INTERSTITIAL:LX/0yY;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/121;->a(LX/0yY;LX/0gc;)V

    .line 2087759
    return-void

    .line 2087760
    :cond_4
    const-string v0, "ACTION_INCOMING_CALL"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2087761
    if-eqz v3, :cond_5

    .line 2087762
    new-instance v0, LX/EBe;

    invoke-direct {v0, p0, v4}, LX/EBe;-><init>(Lcom/facebook/rtc/activities/RtcZeroRatingActivity;Z)V

    goto :goto_0

    .line 2087763
    :cond_5
    iget-object v0, p0, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v2}, LX/EDx;->i(Z)V

    .line 2087764
    new-instance v0, LX/EBf;

    invoke-direct {v0, p0, v2}, LX/EBf;-><init>(Lcom/facebook/rtc/activities/RtcZeroRatingActivity;Z)V

    goto :goto_0

    .line 2087765
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    move-object v0, v1

    goto :goto_0
.end method
