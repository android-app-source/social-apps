.class public Lcom/facebook/rtc/activities/RtcCallPermissionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field public static final t:[Ljava/lang/String;

.field public static final u:[Ljava/lang/String;


# instance fields
.field public p:LX/3A0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/2S7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/6c4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Lcom/facebook/rtc/helpers/RtcCallStartParams;

.field private w:[Ljava/lang/String;

.field private x:[Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:LX/0i5;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2087689
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v2

    sput-object v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->t:[Ljava/lang/String;

    .line 2087690
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v2

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->u:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2087691
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2087692
    iput-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->w:[Ljava/lang/String;

    .line 2087693
    iput-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->x:[Ljava/lang/String;

    .line 2087694
    iput-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->y:Ljava/lang/String;

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 2087681
    iget-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->v:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-boolean v1, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->f:Z

    .line 2087682
    iget-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->v:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iget-object v2, v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;->g:Ljava/lang/String;

    .line 2087683
    iget-object v3, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->z:LX/0i5;

    if-eqz v1, :cond_1

    sget-object v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->u:[Ljava/lang/String;

    :goto_0
    invoke-virtual {v3, v0}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2087684
    iget-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->r:LX/6c4;

    invoke-virtual {v0}, LX/6c4;->b()V

    .line 2087685
    :cond_0
    iget-object v3, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->z:LX/0i5;

    if-eqz v1, :cond_2

    sget-object v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->u:[Ljava/lang/String;

    :goto_1
    invoke-direct {p0, v1}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->b(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->c(Z)Ljava/lang/String;

    move-result-object v1

    new-instance v5, LX/EBa;

    invoke-direct {v5, p0, v2}, LX/EBa;-><init>(Lcom/facebook/rtc/activities/RtcCallPermissionActivity;Ljava/lang/String;)V

    invoke-virtual {v3, v0, v4, v1, v5}, LX/0i5;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Zx;)V

    .line 2087686
    return-void

    .line 2087687
    :cond_1
    sget-object v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->t:[Ljava/lang/String;

    goto :goto_0

    .line 2087688
    :cond_2
    sget-object v0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->t:[Ljava/lang/String;

    goto :goto_1
.end method

.method private static a(Lcom/facebook/rtc/activities/RtcCallPermissionActivity;LX/3A0;LX/2S7;LX/6c4;LX/0i4;)V
    .locals 0

    .prologue
    .line 2087676
    iput-object p1, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->p:LX/3A0;

    iput-object p2, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->q:LX/2S7;

    iput-object p3, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->r:LX/6c4;

    iput-object p4, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->s:LX/0i4;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;

    invoke-static {v3}, LX/3A0;->a(LX/0QB;)LX/3A0;

    move-result-object v0

    check-cast v0, LX/3A0;

    invoke-static {v3}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object v1

    check-cast v1, LX/2S7;

    invoke-static {v3}, LX/6c4;->a(LX/0QB;)LX/6c4;

    move-result-object v2

    check-cast v2, LX/6c4;

    const-class v4, LX/0i4;

    invoke-interface {v3, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/0i4;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->a(Lcom/facebook/rtc/activities/RtcCallPermissionActivity;LX/3A0;LX/2S7;LX/6c4;LX/0i4;)V

    return-void
.end method

.method private b(Z)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2087677
    if-eqz p1, :cond_0

    const v0, 0x7f0807db

    .line 2087678
    :goto_0
    const v1, 0x7f080011

    invoke-virtual {p0, v1}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2087679
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2087680
    :cond_0
    const v0, 0x7f0807d8

    goto :goto_0
.end method

.method private c(Z)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2087672
    if-eqz p1, :cond_0

    const v0, 0x7f0807dd

    .line 2087673
    :goto_0
    const v1, 0x7f080011

    invoke-virtual {p0, v1}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2087674
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2087675
    :cond_0
    const v0, 0x7f0807da

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2087660
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2087661
    invoke-static {p0, p0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2087662
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "StartParams"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/helpers/RtcCallStartParams;

    iput-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->v:Lcom/facebook/rtc/helpers/RtcCallStartParams;

    .line 2087663
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ConferenceParticipants"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2087664
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ConferenceParticipants"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->w:[Ljava/lang/String;

    .line 2087665
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ConferenceParticipantsToRing"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2087666
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ConferenceParticipantsToRing"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->x:[Ljava/lang/String;

    .line 2087667
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ConferenceServerInfo"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2087668
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ConferenceServerInfo"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->y:Ljava/lang/String;

    .line 2087669
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->s:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->z:LX/0i5;

    .line 2087670
    invoke-direct {p0}, Lcom/facebook/rtc/activities/RtcCallPermissionActivity;->a()V

    .line 2087671
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x15195af2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2087658
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2087659
    const/16 v1, 0x23

    const v2, -0x41d979f2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
