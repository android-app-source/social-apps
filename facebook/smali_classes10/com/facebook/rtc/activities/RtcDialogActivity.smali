.class public Lcom/facebook/rtc/activities/RtcDialogActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2087703
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2087704
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/RtcDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2087705
    if-nez v0, :cond_1

    .line 2087706
    :cond_0
    :goto_0
    return-void

    .line 2087707
    :cond_1
    const-string v1, "com.facebook.rtc.activities.intent.action.ACTION_DIALOG"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2087708
    new-instance v1, LX/31Y;

    invoke-direct {v1, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    const-string v2, "TITLE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const-string v2, "MESSAGE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080016

    invoke-virtual {p0, v2}, Lcom/facebook/rtc/activities/RtcDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance p1, LX/EBc;

    invoke-direct {p1, p0}, LX/EBc;-><init>(Lcom/facebook/rtc/activities/RtcDialogActivity;)V

    invoke-virtual {v1, v2, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    new-instance v2, LX/EBb;

    invoke-direct {v2, p0}, LX/EBb;-><init>(Lcom/facebook/rtc/activities/RtcDialogActivity;)V

    invoke-virtual {v1, v2}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    .line 2087709
    invoke-virtual {v1}, LX/2EJ;->show()V

    goto :goto_0
.end method
