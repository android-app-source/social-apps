.class public Lcom/facebook/rtc/activities/WebrtcIncallActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/15n;
.implements LX/EC9;
.implements LX/ECA;
.implements LX/ECB;
.implements LX/ECD;


# static fields
.field public static final M:[Ljava/lang/String;

.field public static final N:[Ljava/lang/String;

.field public static final Y:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EG7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/ECj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/EG4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/EG8;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/3RT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0yH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private O:Z

.field public P:Z

.field private Q:Z

.field public R:Z

.field public S:Z

.field public T:Z

.field private U:LX/EC2;

.field private V:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public W:Z

.field private X:LX/EC8;

.field public Z:J

.field public aA:Landroid/widget/LinearLayout;

.field public aB:Lcom/facebook/rtc/views/RtcSpringDragView;

.field public aC:LX/EIE;

.field private aD:Landroid/view/View$OnLayoutChangeListener;

.field public aE:Lcom/facebook/rtc/views/RtcActionBar;

.field public aF:LX/EBz;

.field public aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

.field private aH:Lcom/facebook/widget/tiles/ThreadTileView;

.field public aI:Lcom/facebook/rtc/views/RtcLevelTileView;

.field private aJ:Landroid/widget/TextView;

.field private aK:Landroid/widget/TextView;

.field public aL:Lcom/facebook/rtc/views/ChildLockBanner;

.field public aM:Lcom/facebook/rtc/views/VoipConnectionBanner;

.field private aN:Landroid/view/View;

.field private aO:Landroid/view/View;

.field public aP:Landroid/support/v4/view/ViewPager;

.field public aQ:LX/ED6;

.field public aR:Landroid/widget/FrameLayout;

.field public aS:LX/EIU;

.field public aT:I

.field private aU:I

.field private aV:J

.field private aW:Z

.field private aX:Z

.field private aY:Z

.field public aZ:J

.field public aa:Z

.field public ab:LX/EDO;

.field private ac:LX/EDP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ad:LX/ECi;

.field private ae:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public af:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ag:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EFj;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ah:LX/3Eb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ai:LX/ED7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aj:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ECw;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ak:LX/ECs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private al:Landroid/net/ConnectivityManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private am:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

.field private ao:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public ap:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public aq:LX/2EJ;

.field private ar:LX/EC0;

.field public as:LX/EIZ;

.field public at:LX/EIY;

.field public au:LX/EIh;

.field public av:LX/EIg;

.field public aw:LX/EId;

.field public ax:LX/EIk;

.field private ay:Landroid/view/View;

.field public az:Lcom/facebook/rtc/views/RtcSnakeView;

.field private ba:Z

.field private bb:Z

.field public bc:Z

.field public bd:LX/EC6;

.field public be:Z

.field private bf:Z

.field public bg:LX/0i5;

.field public p:Landroid/media/AudioManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/00H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1r1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/3A0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/2S7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Landroid/view/WindowManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/EG3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ka;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2089123
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v2

    sput-object v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->M:[Ljava/lang/String;

    .line 2089124
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v2

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->N:[Ljava/lang/String;

    .line 2089125
    const-class v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    sput-object v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Y:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2089246
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2089247
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2089248
    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    .line 2089249
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2089250
    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag:LX/0Ot;

    .line 2089251
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2089252
    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aj:LX/0Ot;

    .line 2089253
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2089254
    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->am:LX/0Ot;

    .line 2089255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ap:Ljava/util/concurrent/Future;

    .line 2089256
    iput v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aT:I

    .line 2089257
    iput-boolean v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bb:Z

    .line 2089258
    sget-object v0, LX/EC6;->NONE:LX/EC6;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    .line 2089259
    return-void
.end method

.method public static B(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 3

    .prologue
    .line 2089239
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->f(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2089240
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2089241
    :cond_0
    :goto_0
    return-void

    .line 2089242
    :cond_1
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089243
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B:LX/0ad;

    sget-short v1, LX/3Dx;->z:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2089244
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2089245
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/EIY;->setVoicemailButtonsVisible(Z)V

    goto :goto_0
.end method

.method public static C(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2089238
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    .line 2089215
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    if-eqz v0, :cond_0

    .line 2089216
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    invoke-virtual {v0}, LX/EIY;->a()V

    .line 2089217
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    if-eqz v0, :cond_1

    .line 2089218
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    .line 2089219
    iget-object v1, v0, LX/EIh;->b:LX/EH3;

    invoke-virtual {v1}, LX/EH3;->a()V

    .line 2089220
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_3

    .line 2089221
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->j()V

    .line 2089222
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089223
    iget-object v1, v0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/RtcActionBar;->b()V

    .line 2089224
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089225
    iget-object v1, v0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/RtcActionBar;->c()V

    .line 2089226
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089227
    iget-object v1, v0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/RtcActionBar;->d()V

    .line 2089228
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->C(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2089229
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->q()V

    .line 2089230
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->p()V

    .line 2089231
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    if-eqz v0, :cond_4

    .line 2089232
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->d()V

    .line 2089233
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->C(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2089234
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->f()V

    .line 2089235
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    if-eqz v0, :cond_5

    .line 2089236
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->d()V

    .line 2089237
    :cond_5
    return-void
.end method

.method public static E(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    .line 2089207
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aw()V

    .line 2089208
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2089209
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->I$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089210
    :goto_0
    return-void

    .line 2089211
    :cond_0
    const v0, 0x7f08073a

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2089212
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/7TQ;->CallEndIgnoreCall:LX/7TQ;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/7TQ;)V

    .line 2089213
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->x()V

    .line 2089214
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->I$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0
.end method

.method public static I$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 5

    .prologue
    .line 2089203
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ao:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 2089204
    :goto_0
    return-void

    .line 2089205
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->l(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2089206
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity$12;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$12;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    const-wide/16 v2, 0x7d0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ao:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public static J$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 3

    .prologue
    .line 2089193
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->D()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 2089194
    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2089195
    if-eqz v1, :cond_1

    sget-object v0, LX/EDq;->SPEAKERPHONE:LX/EDq;

    move-object v2, v0

    .line 2089196
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v2}, LX/EDx;->a(LX/EDq;)V

    .line 2089197
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089198
    iput-boolean v1, v0, LX/EDx;->bd:Z

    .line 2089199
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089200
    return-void

    .line 2089201
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 2089202
    :cond_1
    sget-object v0, LX/EDq;->EARPIECE:LX/EDq;

    move-object v2, v0

    goto :goto_1
.end method

.method public static N$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 7

    .prologue
    .line 2089185
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->G()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2089186
    :goto_0
    return-void

    .line 2089187
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2089188
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bg:LX/0i5;

    sget-object v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->N:[Ljava/lang/String;

    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Ljava/lang/String;

    move-result-object v2

    .line 2089189
    const v3, 0x7f080011

    invoke-virtual {p0, v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2089190
    const v4, 0x7f0807df

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2089191
    new-instance v4, LX/EBi;

    invoke-direct {v4, p0}, LX/EBi;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/0i5;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Zx;)V

    goto :goto_0

    .line 2089192
    :cond_1
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->O(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0
.end method

.method public static O(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2089159
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-nez v0, :cond_1

    move v3, v1

    .line 2089160
    :goto_0
    if-eqz v3, :cond_3

    .line 2089161
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089162
    iget-object v4, v0, LX/EDx;->M:LX/0Uh;

    const/16 v5, 0x5f6

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    move v0, v4

    .line 2089163
    if-eqz v0, :cond_2

    .line 2089164
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->am:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79G;

    .line 2089165
    const-string v4, "RTC_SHARE_VIDEO"

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, LX/79G;->a(LX/79G;Ljava/lang/String;LX/1rQ;)V

    .line 2089166
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v4, LX/03R;->YES:LX/03R;

    .line 2089167
    iput-object v4, v0, LX/EDx;->bv:LX/03R;

    .line 2089168
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089169
    iput-boolean v2, v0, LX/EDx;->aq:Z

    .line 2089170
    invoke-static {p0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2089171
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089172
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    if-nez v3, :cond_4

    .line 2089173
    :goto_2
    iput-boolean v1, v0, LX/EDx;->aq:Z

    .line 2089174
    invoke-static {p0, v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2089175
    return-void

    :cond_1
    move v3, v2

    .line 2089176
    goto :goto_0

    .line 2089177
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2089178
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v1}, LX/EDx;->l(Z)V

    .line 2089179
    iput-boolean v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    .line 2089180
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0807a2

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    goto :goto_1

    .line 2089181
    :cond_3
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    if-eqz v0, :cond_0

    .line 2089182
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v2}, LX/EDx;->l(Z)V

    .line 2089183
    iput-boolean v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    goto :goto_1

    :cond_4
    move v1, v2

    .line 2089184
    goto :goto_2
.end method

.method public static P(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 4

    .prologue
    .line 2089146
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    if-nez v0, :cond_0

    .line 2089147
    new-instance v0, LX/EIE;

    invoke-direct {v0, p0}, LX/EIE;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    .line 2089148
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089149
    iget v1, v0, LX/EDx;->au:I

    move v0, v1

    .line 2089150
    if-eqz v0, :cond_0

    .line 2089151
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089152
    iget v2, v0, LX/EDx;->at:I

    move v0, v2

    .line 2089153
    int-to-float v2, v0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089154
    iget v3, v0, LX/EDx;->au:I

    move v0, v3

    .line 2089155
    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, LX/EIE;->a(FF)V

    .line 2089156
    :cond_0
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->X$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089157
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->U$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089158
    return-void
.end method

.method public static Q(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 4

    .prologue
    .line 2088988
    sget-object v0, LX/EBu;->a:[I

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    invoke-virtual {v1}, LX/EC6;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2088989
    :cond_0
    :goto_0
    sget-object v0, LX/EC6;->NONE:LX/EC6;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    .line 2088990
    return-void

    .line 2088991
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    .line 2088992
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    .line 2088993
    if-nez v1, :cond_2

    .line 2088994
    :goto_1
    goto :goto_0

    .line 2088995
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->b(Landroid/view/View;)V

    .line 2088996
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->setVisibility(I)V

    goto :goto_0

    .line 2088997
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    .line 2088998
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2088999
    iget-object v1, v0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/RtcGridView;->a()V

    .line 2089000
    iget-object v1, v0, LX/EIU;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2089001
    iget-object v1, v0, LX/EIU;->S:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2089002
    iget-object v1, v0, LX/EIU;->Q:LX/EH4;

    if-eqz v1, :cond_1

    .line 2089003
    iget-object v1, v0, LX/EIU;->Q:LX/EH4;

    .line 2089004
    iget-object v2, v1, LX/EH4;->b:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 2089005
    iget-object v2, v1, LX/EH4;->b:Landroid/view/View;

    invoke-virtual {v1, v2}, LX/EH4;->removeView(Landroid/view/View;)V

    .line 2089006
    const/4 v2, 0x0

    iput-object v2, v1, LX/EH4;->b:Landroid/view/View;

    .line 2089007
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, LX/EIU;->Q:LX/EH4;

    .line 2089008
    goto :goto_0

    .line 2089009
    :cond_2
    iget-object v2, v0, LX/EIU;->V:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->removeView(Landroid/view/View;)V

    .line 2089010
    iget-object v2, v0, LX/EIU;->V:Lcom/facebook/resources/ui/FbFrameLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static U$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 3

    .prologue
    .line 2089131
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089132
    iget-boolean v1, v0, LX/EDx;->aq:Z

    move v0, v1

    .line 2089133
    if-eqz v0, :cond_1

    .line 2089134
    :cond_0
    :goto_0
    return-void

    .line 2089135
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aX()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2089136
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    if-eqz v0, :cond_0

    .line 2089137
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    invoke-virtual {v0}, LX/EIE;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2089138
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->X()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2089139
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    invoke-virtual {v0}, LX/EIE;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2089140
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EDv;->STARTED:LX/EDv;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EDv;)V

    .line 2089141
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->az()V

    .line 2089142
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    invoke-virtual {v0}, LX/EIE;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2089143
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    new-instance v1, LX/EC7;

    invoke-direct {v1, p0}, LX/EC7;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    invoke-virtual {v0, v1}, LX/EIE;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    goto :goto_0

    .line 2089144
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->V()LX/EHZ;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    invoke-virtual {v0, v1}, LX/EHZ;->a(Landroid/view/TextureView;)V

    goto :goto_1
.end method

.method public static W$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2089126
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-eqz v0, :cond_0

    .line 2089127
    :goto_0
    return-void

    .line 2089128
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->getHeight()I

    move-result v0

    if-lez v0, :cond_1

    .line 2089129
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/RtcActionBar;->getHeight()I

    move-result v1

    invoke-virtual {v0, v2, v2, v1, v2}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(IIII)V

    goto :goto_0

    .line 2089130
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/EBj;

    invoke-direct {v1, p0}, LX/EBj;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public static X$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 11

    .prologue
    .line 2089112
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    if-nez v0, :cond_0

    .line 2089113
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ak:LX/ECs;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aj:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ECw;

    iget-object v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B:LX/0ad;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->V()LX/EHZ;

    move-result-object v1

    iget-object v4, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->al:Landroid/net/ConnectivityManager;

    .line 2089114
    new-instance v5, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    const-class v6, Landroid/content/Context;

    invoke-interface {v2, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    move-object v7, v0

    move-object v8, v3

    move-object v9, v1

    move-object v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;-><init>(Landroid/content/Context;LX/ECw;LX/0ad;LX/EHZ;Landroid/net/ConnectivityManager;)V

    .line 2089115
    const/16 v6, 0x3257

    invoke-static {v2, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x187f

    invoke-static {v2, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    .line 2089116
    iput-object v6, v5, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->d:LX/0Ot;

    iput-object v7, v5, Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;->e:LX/0Ot;

    .line 2089117
    move-object v0, v5

    .line 2089118
    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    .line 2089119
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089120
    iget-object v1, v0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v1

    .line 2089121
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->an:Lcom/facebook/rtc/expression/RtcExpressionEffectsAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2089122
    :cond_0
    return-void
.end method

.method private Z()V
    .locals 2

    .prologue
    .line 2089107
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2089108
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089109
    iget-object p0, v1, LX/EIU;->u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    move-object v1, p0

    .line 2089110
    invoke-virtual {v0, v1}, LX/EDx;->b(Landroid/view/View;)V

    .line 2089111
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2089045
    invoke-static {p0, p1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2089046
    :goto_0
    return-void

    .line 2089047
    :cond_0
    const/4 v0, 0x0

    .line 2089048
    sget-object v1, LX/EBu;->c:[I

    invoke-virtual {p1}, LX/EC8;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2089049
    :goto_1
    if-nez v0, :cond_8

    .line 2089050
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D:LX/03V;

    const-string v1, "missingView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t find a view for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2089051
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    if-nez v0, :cond_1

    .line 2089052
    new-instance v0, LX/EIZ;

    invoke-direct {v0, p0}, LX/EIZ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    .line 2089053
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    new-instance v1, LX/EBm;

    invoke-direct {v1, p0}, LX/EBm;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089054
    iget-object v2, v0, LX/EIZ;->d:Lcom/facebook/rtc/views/RtcIncomingCallButtons;

    .line 2089055
    iput-object v1, v2, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->g:LX/EBm;

    .line 2089056
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/view/View;)V

    .line 2089057
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2089058
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    goto :goto_1

    .line 2089059
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2089060
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    if-nez v0, :cond_2

    .line 2089061
    new-instance v0, LX/EIh;

    invoke-direct {v0, p0}, LX/EIh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    .line 2089062
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aF:LX/EBz;

    .line 2089063
    iget-object v2, v0, LX/EIh;->b:LX/EH3;

    .line 2089064
    iput-object v1, v2, LX/EH3;->u:LX/EBz;

    .line 2089065
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/view/View;)V

    .line 2089066
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2089067
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    goto :goto_1

    .line 2089068
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    if-nez v0, :cond_4

    .line 2089069
    new-instance v0, LX/EIY;

    invoke-direct {v0, p0}, LX/EIY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    .line 2089070
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aF:LX/EBz;

    .line 2089071
    iget-object v2, v0, LX/EIY;->d:LX/EH3;

    .line 2089072
    iput-object v1, v2, LX/EH3;->u:LX/EBz;

    .line 2089073
    iget-object v2, v0, LX/EIY;->e:LX/EH3;

    .line 2089074
    iput-object v1, v2, LX/EH3;->u:LX/EBz;

    .line 2089075
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/view/View;)V

    .line 2089076
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2089077
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    goto/16 :goto_1

    .line 2089078
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw:LX/EId;

    if-nez v0, :cond_5

    .line 2089079
    new-instance v0, LX/EId;

    invoke-direct {v0, p0}, LX/EId;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw:LX/EId;

    .line 2089080
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw:LX/EId;

    new-instance v1, LX/EBn;

    invoke-direct {v1, p0}, LX/EBn;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089081
    iput-object v1, v0, LX/EId;->d:LX/EBn;

    .line 2089082
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw:LX/EId;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/view/View;)V

    .line 2089083
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw:LX/EId;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2089084
    :cond_5
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw:LX/EId;

    goto/16 :goto_1

    .line 2089085
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av:LX/EIg;

    if-nez v0, :cond_6

    .line 2089086
    new-instance v0, LX/EIg;

    invoke-direct {v0, p0}, LX/EIg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av:LX/EIg;

    .line 2089087
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av:LX/EIg;

    new-instance v1, LX/EBq;

    invoke-direct {v1, p0}, LX/EBq;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089088
    iput-object v1, v0, LX/EIg;->d:LX/EBq;

    .line 2089089
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av:LX/EIg;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/view/View;)V

    .line 2089090
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av:LX/EIg;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2089091
    :cond_6
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av:LX/EIg;

    goto/16 :goto_1

    .line 2089092
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ax:LX/EIk;

    if-nez v0, :cond_7

    .line 2089093
    new-instance v0, LX/EIk;

    invoke-direct {v0, p0}, LX/EIk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ax:LX/EIk;

    .line 2089094
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ax:LX/EIk;

    new-instance v1, LX/EBs;

    invoke-direct {v1, p0}, LX/EBs;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089095
    iput-object v1, v0, LX/EIk;->c:LX/EBs;

    .line 2089096
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ax:LX/EIk;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/view/View;)V

    .line 2089097
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ax:LX/EIk;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2089098
    :cond_7
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ax:LX/EIk;

    goto/16 :goto_1

    .line 2089099
    :cond_8
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ay:Landroid/view/View;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ay:Landroid/view/View;

    if-eq v1, v0, :cond_9

    .line 2089100
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ay:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2089101
    :cond_9
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2089102
    sget-object v1, LX/EC8;->REDIAL:LX/EC8;

    invoke-virtual {p1, v1}, LX/EC8;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2089103
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aK:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2089104
    :goto_2
    iput-object p1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->X:LX/EC8;

    .line 2089105
    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ay:Landroid/view/View;

    goto/16 :goto_0

    .line 2089106
    :cond_a
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aK:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/media/AudioManager;LX/00H;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1r1;LX/3A0;LX/2S7;LX/EDP;Landroid/view/WindowManager;LX/EG3;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0i4;LX/03V;LX/ECj;LX/EG4;LX/EG8;LX/0SG;LX/3RT;LX/0yH;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/0Or;LX/0Ot;LX/0Ot;LX/3Eb;LX/ED7;LX/0Ot;LX/ECs;Landroid/net/ConnectivityManager;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/rtc/activities/WebrtcIncallActivity;",
            "Landroid/media/AudioManager;",
            "LX/00H;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/1r1;",
            "LX/3A0;",
            "LX/2S7;",
            "LX/EDP;",
            "Landroid/view/WindowManager;",
            "LX/EG3;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ka;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EG7;",
            ">;",
            "LX/0ad;",
            "LX/0i4;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/ECj;",
            "LX/EG4;",
            "LX/EG8;",
            "LX/0SG;",
            "Lcom/facebook/rtc/interfaces/VoipNotificationPreferences;",
            "Lcom/facebook/iorg/common/zero/interfaces/ZeroFeatureVisibilityHelper;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EFj;",
            ">;",
            "LX/3Eb;",
            "LX/ED7;",
            "LX/0Ot",
            "<",
            "LX/ECw;",
            ">;",
            "LX/ECs;",
            "Landroid/net/ConnectivityManager;",
            "LX/0Ot",
            "<",
            "LX/79G;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2089145
    iput-object p1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->p:Landroid/media/AudioManager;

    iput-object p2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->q:LX/00H;

    iput-object p3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p4, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->s:LX/1r1;

    iput-object p5, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    iput-object p6, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    iput-object p7, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ac:LX/EDP;

    iput-object p8, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->v:Landroid/view/WindowManager;

    iput-object p9, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->w:LX/EG3;

    iput-object p10, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->x:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->y:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->z:LX/0Ot;

    iput-object p13, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->A:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B:LX/0ad;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->C:LX/0i4;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D:LX/03V;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->E:LX/ECj;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->F:LX/EG4;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->G:LX/EG8;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->H:LX/0SG;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->I:LX/3RT;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->J:LX/0yH;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->K:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->L:LX/0Uh;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ae:LX/0Or;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag:LX/0Ot;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ah:LX/3Eb;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ai:LX/ED7;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aj:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ak:LX/ECs;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->al:Landroid/net/ConnectivityManager;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->am:LX/0Ot;

    return-void
.end method

.method public static a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 2089039
    if-nez p1, :cond_0

    .line 2089040
    :goto_0
    return-void

    .line 2089041
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    invoke-virtual {v0}, LX/EIh;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 2089042
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2089043
    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2089044
    :cond_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x41b80000    # 23.0f

    invoke-direct {v0, v3, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;[Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 2

    .prologue
    .line 2089034
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aq:LX/2EJ;

    if-eqz v0, :cond_0

    .line 2089035
    :goto_0
    return-void

    .line 2089036
    :cond_0
    new-instance v0, LX/EBh;

    invoke-direct {v0, p0, p3}, LX/EBh;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2089037
    new-instance v1, LX/31Y;

    invoke-direct {v1, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p1, p2}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aq:LX/2EJ;

    .line 2089038
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aq:LX/2EJ;

    invoke-virtual {v1, v0}, LX/2EJ;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 37

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v35

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/rtc/activities/WebrtcIncallActivity;

    invoke-static/range {v35 .. v35}, LX/19T;->a(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    const-class v4, LX/00H;

    move-object/from16 v0, v35

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/00H;

    invoke-static/range {v35 .. v35}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v35 .. v35}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v6

    check-cast v6, LX/1r1;

    invoke-static/range {v35 .. v35}, LX/3A0;->a(LX/0QB;)LX/3A0;

    move-result-object v7

    check-cast v7, LX/3A0;

    invoke-static/range {v35 .. v35}, LX/2S7;->a(LX/0QB;)LX/2S7;

    move-result-object v8

    check-cast v8, LX/2S7;

    const-class v9, LX/EDP;

    move-object/from16 v0, v35

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/EDP;

    invoke-static/range {v35 .. v35}, LX/0q4;->a(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v10

    check-cast v10, Landroid/view/WindowManager;

    invoke-static/range {v35 .. v35}, LX/EBT;->a(LX/0QB;)LX/EG3;

    move-result-object v11

    check-cast v11, LX/EG3;

    const/16 v12, 0x245

    move-object/from16 v0, v35

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x4b9

    move-object/from16 v0, v35

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x1430

    move-object/from16 v0, v35

    invoke-static {v0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x3263

    move-object/from16 v0, v35

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v35 .. v35}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v16

    check-cast v16, LX/0ad;

    const-class v17, LX/0i4;

    move-object/from16 v0, v35

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/0i4;

    invoke-static/range {v35 .. v35}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v18

    check-cast v18, LX/03V;

    const-class v19, LX/ECj;

    move-object/from16 v0, v35

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/ECj;

    invoke-static/range {v35 .. v35}, LX/EBU;->a(LX/0QB;)LX/EG4;

    move-result-object v20

    check-cast v20, LX/EG4;

    invoke-static/range {v35 .. v35}, LX/EBY;->a(LX/0QB;)LX/EG8;

    move-result-object v21

    check-cast v21, LX/EG8;

    invoke-static/range {v35 .. v35}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v22

    check-cast v22, LX/0SG;

    invoke-static/range {v35 .. v35}, LX/EBW;->a(LX/0QB;)LX/3RT;

    move-result-object v23

    check-cast v23, LX/3RT;

    invoke-static/range {v35 .. v35}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v24

    check-cast v24, LX/0yH;

    invoke-static/range {v35 .. v35}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v25

    check-cast v25, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v35 .. v35}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v26

    check-cast v26, LX/0Uh;

    const/16 v27, 0x15e8

    move-object/from16 v0, v35

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v27

    const/16 v28, 0x3257

    move-object/from16 v0, v35

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v29, 0x325a

    move-object/from16 v0, v35

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v29

    invoke-static/range {v35 .. v35}, LX/3Eb;->a(LX/0QB;)LX/3Eb;

    move-result-object v30

    check-cast v30, LX/3Eb;

    const-class v31, LX/ED7;

    move-object/from16 v0, v35

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v31

    check-cast v31, LX/ED7;

    const/16 v32, 0x3251

    move-object/from16 v0, v35

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v32

    const-class v33, LX/ECs;

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v33

    check-cast v33, LX/ECs;

    invoke-static/range {v35 .. v35}, LX/0nI;->a(LX/0QB;)Landroid/net/ConnectivityManager;

    move-result-object v34

    check-cast v34, Landroid/net/ConnectivityManager;

    const/16 v36, 0x3266

    invoke-static/range {v35 .. v36}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v35

    invoke-static/range {v2 .. v35}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Landroid/media/AudioManager;LX/00H;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1r1;LX/3A0;LX/2S7;LX/EDP;Landroid/view/WindowManager;LX/EG3;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/0i4;LX/03V;LX/ECj;LX/EG4;LX/EG8;LX/0SG;LX/3RT;LX/0yH;Lcom/facebook/content/SecureContextHelper;LX/0Uh;LX/0Or;LX/0Ot;LX/0Ot;LX/3Eb;LX/ED7;LX/0Ot;LX/ECs;Landroid/net/ConnectivityManager;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2089031
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    .line 2089032
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0, p1, p2}, LX/EIU;->a(Ljava/lang/String;Z)V

    .line 2089033
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/7TQ;Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2089021
    sget-object v0, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089022
    iget p1, v0, LX/EDx;->ah:I

    move v0, p1

    .line 2089023
    if-gt v0, v1, :cond_1

    :cond_0
    move v0, v2

    .line 2089024
    :goto_0
    return v0

    .line 2089025
    :cond_1
    iput-boolean p2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Q:Z

    .line 2089026
    iput v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aU:I

    .line 2089027
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->O:Z

    if-nez v0, :cond_2

    .line 2089028
    iput-boolean v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->P:Z

    move v0, v1

    .line 2089029
    goto :goto_0

    .line 2089030
    :cond_2
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab()Z

    move-result v0

    goto :goto_0
.end method

.method private aA()V
    .locals 2

    .prologue
    .line 2089011
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2089012
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aM()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089013
    invoke-virtual {v0}, LX/EDx;->M()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/EDx;->N()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2089014
    if-eqz v0, :cond_2

    .line 2089015
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av()V

    .line 2089016
    :cond_1
    :goto_1
    return-void

    .line 2089017
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    sget-object v1, LX/EC6;->NONE:LX/EC6;

    if-eq v0, v1, :cond_3

    .line 2089018
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Q(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089019
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->setVisibility(I)V

    .line 2089020
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private aB()V
    .locals 1

    .prologue
    .line 2089393
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    .line 2089394
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->d()V

    .line 2089395
    :cond_0
    return-void
.end method

.method public static aC(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2089425
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-nez v0, :cond_0

    .line 2089426
    :goto_0
    return-void

    .line 2089427
    :cond_0
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2089428
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->r$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089429
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2089430
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aR:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2089431
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_2

    .line 2089432
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2089433
    iget-boolean v1, v0, LX/EIU;->G:Z

    if-nez v1, :cond_4

    .line 2089434
    :cond_2
    :goto_1
    iput-boolean v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    .line 2089435
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2089436
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->o$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089437
    :goto_2
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089438
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bc:Z

    if-eqz v0, :cond_5

    .line 2089439
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aL(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089440
    :goto_3
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0

    .line 2089441
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aN:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 2089442
    :cond_4
    iget-object v1, v0, LX/EIU;->C:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2089443
    iget-object v1, v0, LX/EIU;->u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v1, v4}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setVisibility(I)V

    .line 2089444
    iget-object v1, v0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    invoke-virtual {v1, v4}, Lcom/facebook/rtc/views/RtcGridView;->setVisibility(I)V

    .line 2089445
    invoke-virtual {v0}, LX/EIU;->l()V

    .line 2089446
    iput-boolean v3, v0, LX/EIU;->G:Z

    .line 2089447
    invoke-static {v0}, LX/EIU;->aa(LX/EIU;)V

    .line 2089448
    invoke-virtual {v0}, LX/EIU;->k()V

    .line 2089449
    invoke-static {v0}, LX/EIU;->V(LX/EIU;)V

    goto :goto_1

    .line 2089450
    :cond_5
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->setRequestedOrientation(I)V

    .line 2089451
    goto :goto_3
.end method

.method private aE()V
    .locals 1

    .prologue
    .line 2089452
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2089453
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089454
    iget-object p0, v0, LX/EDx;->p:LX/ECx;

    invoke-virtual {p0}, LX/ECx;->b()V

    .line 2089455
    :cond_0
    return-void
.end method

.method public static aG(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    .line 2089456
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE()V

    .line 2089457
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2089458
    return-void
.end method

.method public static aH(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    .line 2089459
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-nez v0, :cond_0

    sget-object v0, LX/EC8;->INCALL:LX/EC8;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    invoke-virtual {v0}, LX/EDO;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->P:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2089460
    if-eqz v0, :cond_1

    .line 2089461
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089462
    :goto_1
    return-void

    .line 2089463
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ai()V

    .line 2089464
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 2089465
    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aI()V
    .locals 2

    .prologue
    .line 2089466
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE()V

    .line 2089467
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 2089468
    return-void
.end method

.method private aJ()Z
    .locals 1

    .prologue
    .line 2089469
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089470
    iget-object p0, v0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    move-object v0, p0

    .line 2089471
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static aL(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 3

    .prologue
    .line 2089472
    const/4 v0, 0x0

    .line 2089473
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 2089474
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v0, v0

    .line 2089475
    if-eqz v0, :cond_1

    .line 2089476
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->setRequestedOrientation(I)V

    .line 2089477
    :goto_0
    return-void

    .line 2089478
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public static aP(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2089491
    const v0, 0x7f080011

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2089492
    const v1, 0x7f0807db

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private aa()V
    .locals 2

    .prologue
    .line 2089493
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2089494
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089495
    iget-object p0, v1, LX/EIU;->u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    move-object v1, p0

    .line 2089496
    invoke-virtual {v0, v1}, LX/EDx;->a(Landroid/view/View;)V

    .line 2089497
    :cond_0
    return-void
.end method

.method private ab()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2089482
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    .line 2089483
    iget v3, v0, LX/2S7;->u:I

    move v0, v3

    .line 2089484
    if-eqz v0, :cond_0

    move v0, v1

    .line 2089485
    :goto_0
    iget-object v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    iget-boolean v4, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Q:Z

    invoke-virtual {v3, v0, v4}, LX/EDO;->a(ZZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2089486
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    const-string v2, "rating_shown"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2089487
    const-wide/32 v2, 0xea60

    invoke-static {p0, v2, v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->c(Lcom/facebook/rtc/activities/WebrtcIncallActivity;J)V

    .line 2089488
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 2089489
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2089490
    goto :goto_1
.end method

.method public static ac(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 3

    .prologue
    .line 2089420
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->s:LX/1r1;

    const v1, 0x30000006

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    .line 2089421
    if-eqz v0, :cond_0

    .line 2089422
    invoke-virtual {v0}, LX/1ql;->c()V

    .line 2089423
    invoke-virtual {v0}, LX/1ql;->d()V

    .line 2089424
    :cond_0
    return-void
.end method

.method public static af(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 0

    .prologue
    .line 2089479
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089480
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    .line 2089481
    return-void
.end method

.method public static ag$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    .line 2089416
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ao:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 2089417
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ao:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2089418
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ao:Ljava/util/concurrent/Future;

    .line 2089419
    :cond_0
    return-void
.end method

.method private ah()V
    .locals 10

    .prologue
    .line 2089406
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->L:LX/0Uh;

    const/16 v1, 0x60a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2089407
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aH:Lcom/facebook/widget/tiles/ThreadTileView;

    new-instance v1, LX/EBl;

    invoke-direct {v1, p0}, LX/EBl;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tiles/ThreadTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2089408
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->F:LX/EG4;

    if-nez v0, :cond_1

    .line 2089409
    :goto_0
    return-void

    .line 2089410
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2089411
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aH:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->F:LX/EG4;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089412
    invoke-interface {v2}, LX/EG4;->a()LX/8Vc;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    goto :goto_0

    .line 2089413
    :cond_2
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aH:Lcom/facebook/widget/tiles/ThreadTileView;

    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->F:LX/EG4;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089414
    iget-wide v8, v0, LX/EDx;->ak:J

    move-wide v4, v8

    .line 2089415
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ae:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v2}, LX/EG4;->a()LX/8Vc;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    goto :goto_0
.end method

.method private ai()V
    .locals 1

    .prologue
    .line 2089403
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    if-nez v0, :cond_0

    .line 2089404
    :goto_0
    return-void

    .line 2089405
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    invoke-virtual {v0}, LX/EIY;->b()V

    goto :goto_0
.end method

.method public static ak(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 4

    .prologue
    .line 2089401
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aU()J

    move-result-wide v0

    const-wide/16 v2, 0x2bc

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aV:J

    .line 2089402
    return-void
.end method

.method public static al(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Z
    .locals 4

    .prologue
    .line 2089400
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aU()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aV:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private am()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2089396
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    invoke-virtual {v1}, LX/EDO;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2089397
    iput-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bb:Z

    .line 2089398
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    invoke-virtual {v1}, LX/EDO;->a()V

    .line 2089399
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static au(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 1

    .prologue
    .line 2089262
    sget-object v0, LX/EC8;->VIDEO_REQUEST:LX/EC8;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)V

    .line 2089263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->R:Z

    .line 2089264
    const v0, 0x7f08079f

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2089265
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089266
    return-void
.end method

.method private av()V
    .locals 11

    .prologue
    .line 2089310
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aM()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aZ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2089311
    :cond_0
    :goto_0
    return-void

    .line 2089312
    :cond_1
    const/4 v4, -0x1

    .line 2089313
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v2, :cond_2

    .line 2089314
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089315
    iget-object v1, v0, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/EIQ;

    invoke-direct {v2, v0}, LX/EIQ;-><init>(LX/EIU;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2089316
    iget-object v1, v0, LX/EIU;->u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v1}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->a()V

    .line 2089317
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2089318
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aR:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2089319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    .line 2089320
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const/4 v3, 0x0

    const/16 v6, 0x8

    .line 2089321
    iget-boolean v2, v0, LX/EIU;->G:Z

    if-eqz v2, :cond_6

    .line 2089322
    :goto_2
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089323
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/EBt;

    invoke-direct {v1, p0}, LX/EBt;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2089324
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->j()V

    .line 2089325
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0

    .line 2089326
    :cond_2
    new-instance v2, LX/EIU;

    invoke-direct {v2, p0}, LX/EIU;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089327
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    const/4 v10, 0x0

    .line 2089328
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v2, v6}, LX/EIU;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2089329
    iget-object v5, v2, LX/EIU;->u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v5, v10}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setScaleType(I)V

    .line 2089330
    iget-object v5, v2, LX/EIU;->e:LX/ECj;

    sget-object v6, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    new-instance v7, LX/EIL;

    invoke-direct {v7, v2}, LX/EIL;-><init>(LX/EIU;)V

    invoke-virtual {v5, v6, v7}, LX/ECj;->a(Ljava/lang/Boolean;LX/EBx;)LX/ECi;

    move-result-object v5

    iput-object v5, v2, LX/EIU;->s:LX/ECi;

    .line 2089331
    sget-object v5, Lcom/facebook/common/time/AwakeTimeSinceBootClock;->INSTANCE:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-object v5, v5

    .line 2089332
    iput-object v5, v2, LX/EIU;->I:Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    .line 2089333
    invoke-static {v2}, LX/EIU;->H(LX/EIU;)V

    .line 2089334
    invoke-static {v2}, LX/EIU;->ab(LX/EIU;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2089335
    invoke-virtual {v2}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f08072c

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v2, v5, v6}, LX/EIU;->a(Ljava/lang/String;Z)V

    .line 2089336
    invoke-virtual {v2}, LX/EIU;->c()V

    .line 2089337
    iget-object v5, v2, LX/EIU;->a:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v6, Lcom/facebook/rtc/views/VoipVideoView$2;

    invoke-direct {v6, v2}, Lcom/facebook/rtc/views/VoipVideoView$2;-><init>(LX/EIU;)V

    invoke-virtual {v2}, LX/EIU;->getDeferredMillisecondsForVideoCall()I

    move-result v7

    int-to-long v7, v7

    sget-object v9, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v6, v7, v8, v9}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2089338
    :goto_3
    new-instance v5, LX/EIM;

    invoke-direct {v5, v2}, LX/EIM;-><init>(LX/EIU;)V

    iput-object v5, v2, LX/EIU;->ak:LX/EC2;

    .line 2089339
    iget-object v5, v2, LX/EIU;->n:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/EDx;

    iget-object v6, v2, LX/EIU;->ak:LX/EC2;

    invoke-virtual {v5, v6}, LX/EDx;->a(LX/EC2;)V

    .line 2089340
    invoke-static {v2}, LX/EIU;->O(LX/EIU;)V

    .line 2089341
    iget-object v5, v2, LX/EIU;->K:Lcom/facebook/rtc/views/ChildLockBanner;

    invoke-virtual {v5}, Lcom/facebook/rtc/views/ChildLockBanner;->bringToFront()V

    .line 2089342
    iget-object v5, v2, LX/EIU;->K:Lcom/facebook/rtc/views/ChildLockBanner;

    invoke-virtual {v5, v10}, Lcom/facebook/rtc/views/ChildLockBanner;->setColorScheme(Z)V

    .line 2089343
    iget-object v5, v2, LX/EIU;->H:Lcom/facebook/rtc/views/VoipConnectionBanner;

    invoke-virtual {v5}, Lcom/facebook/rtc/views/VoipConnectionBanner;->bringToFront()V

    .line 2089344
    iget-object v5, v2, LX/EIU;->n:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/EDx;

    invoke-virtual {v5}, LX/EDx;->j()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2089345
    new-instance v5, LX/EH3;

    invoke-virtual {v2}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v6

    sget-object v7, LX/EH1;->CONFERENCE_VIDEO:LX/EH1;

    invoke-direct {v5, v6, v7}, LX/EH3;-><init>(Landroid/content/Context;LX/EH1;)V

    iput-object v5, v2, LX/EIU;->z:LX/EH3;

    .line 2089346
    :goto_4
    iget-object v5, v2, LX/EIU;->A:Landroid/widget/FrameLayout;

    iget-object v6, v2, LX/EIU;->z:LX/EH3;

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2089347
    iget-object v5, v2, LX/EIU;->b:Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    .line 2089348
    iget-object v5, v2, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2089349
    invoke-virtual {v6}, Landroid/view/Display;->getWidth()I

    move-result v7

    invoke-virtual {v6}, Landroid/view/Display;->getHeight()I

    move-result v6

    invoke-static {v7, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 2089350
    iget-object v6, v2, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v5}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2089351
    const/16 v5, 0x1388

    invoke-static {v2, v5}, LX/EIU;->c(LX/EIU;I)V

    .line 2089352
    new-instance v5, LX/EIN;

    invoke-direct {v5, v2}, LX/EIN;-><init>(LX/EIU;)V

    invoke-virtual {v2, v5}, LX/EIU;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2089353
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    iget-object v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aF:LX/EBz;

    .line 2089354
    iput-object v3, v2, LX/EIU;->ah:LX/EBz;

    .line 2089355
    iget-object v5, v2, LX/EIU;->z:LX/EH3;

    .line 2089356
    iput-object v3, v5, LX/EH3;->u:LX/EBz;

    .line 2089357
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    new-instance v3, LX/EBk;

    invoke-direct {v3, p0}, LX/EBk;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2089358
    iput-object v3, v2, LX/EIU;->y:LX/EBk;

    .line 2089359
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aR:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2089360
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, LX/EIU;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2089361
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    iget-object v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2089362
    iget-object v4, v3, LX/EIU;->K:Lcom/facebook/rtc/views/ChildLockBanner;

    move-object v3, v4

    .line 2089363
    invoke-virtual {v2, v3}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->a(Lcom/facebook/rtc/views/ChildLockBanner;)V

    .line 2089364
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    .line 2089365
    iget-boolean v3, v2, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->b:Z

    move v2, v3

    .line 2089366
    if-eqz v2, :cond_3

    .line 2089367
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v2}, LX/EIU;->C()V

    .line 2089368
    :cond_3
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->X$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto/16 :goto_1

    .line 2089369
    :cond_4
    invoke-virtual {v2}, LX/EIU;->e()V

    goto/16 :goto_3

    .line 2089370
    :cond_5
    new-instance v5, LX/EH3;

    invoke-virtual {v2}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v6

    sget-object v7, LX/EH1;->VIDEO:LX/EH1;

    invoke-direct {v5, v6, v7}, LX/EH3;-><init>(Landroid/content/Context;LX/EH1;)V

    iput-object v5, v2, LX/EIU;->z:LX/EH3;

    goto/16 :goto_4

    .line 2089371
    :cond_6
    iget-object v2, v0, LX/EIU;->C:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2089372
    iget-object v2, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->j()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2089373
    iget-object v2, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->bs()V

    .line 2089374
    iget-object v2, v0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    invoke-virtual {v2, v3}, Lcom/facebook/rtc/views/RtcGridView;->setVisibility(I)V

    .line 2089375
    iget-object v4, v0, LX/EIU;->o:LX/ED2;

    iget-object v2, v0, LX/EIU;->m:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v0}, LX/EIU;->getConferenceCall(LX/EIU;)LX/EFy;

    move-result-object v5

    iget-object v3, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->bj()LX/0Px;

    move-result-object v3

    .line 2089376
    new-instance v7, LX/ED1;

    invoke-direct {v7, v2, v5, v3}, LX/ED1;-><init>(Ljava/lang/String;LX/EFy;LX/0Px;)V

    .line 2089377
    const/16 v8, 0x3257

    invoke-static {v4, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2089378
    iput-object v8, v7, LX/ED1;->c:LX/0Ot;

    .line 2089379
    move-object v2, v7

    .line 2089380
    iput-object v2, v0, LX/EIU;->O:LX/ED1;

    .line 2089381
    iget-object v2, v0, LX/EIU;->P:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v0, LX/EIU;->O:LX/ED1;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2089382
    invoke-static {v0}, LX/EIU;->getMultiViewMode(LX/EIU;)LX/EIT;

    move-result-object v2

    iput-object v2, v0, LX/EIU;->an:LX/EIT;

    .line 2089383
    iget-object v2, v0, LX/EIU;->u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v2, v6}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setVisibility(I)V

    .line 2089384
    invoke-virtual {v0, v1}, LX/EIU;->b(LX/0gc;)V

    .line 2089385
    invoke-virtual {v0}, LX/EIU;->z()V

    .line 2089386
    :goto_5
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/EIU;->G:Z

    .line 2089387
    invoke-virtual {v0}, LX/EIU;->j()V

    .line 2089388
    const/16 v2, 0x1388

    invoke-static {v0, v2}, LX/EIU;->c(LX/EIU;I)V

    .line 2089389
    iget-object v2, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    invoke-virtual {v2}, LX/EDx;->aX()Z

    move-result v2

    iput-boolean v2, v0, LX/EIU;->aj:Z

    .line 2089390
    invoke-virtual {v0}, LX/EIU;->d()V

    goto/16 :goto_2

    .line 2089391
    :cond_7
    iget-object v2, v0, LX/EIU;->v:Lcom/facebook/rtc/views/RtcGridView;

    invoke-virtual {v2, v6}, Lcom/facebook/rtc/views/RtcGridView;->setVisibility(I)V

    .line 2089392
    iget-object v2, v0, LX/EIU;->u:Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;

    invoke-virtual {v2, v3}, Lorg/webrtc/videoengine/ViEAndroidGLES20SurfaceView;->setVisibility(I)V

    goto :goto_5
.end method

.method public static aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 1

    .prologue
    .line 2089304
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av()V

    .line 2089305
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aa()V

    .line 2089306
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB()V

    .line 2089307
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    .line 2089308
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ay()V

    .line 2089309
    :cond_0
    return-void
.end method

.method public static ax(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 0

    .prologue
    .line 2089299
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA()V

    .line 2089300
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Z()V

    .line 2089301
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB()V

    .line 2089302
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->az()V

    .line 2089303
    return-void
.end method

.method private ay()V
    .locals 9

    .prologue
    .line 2089270
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->M()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    .line 2089271
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2089272
    iget-object p0, v0, LX/EDx;->af:LX/EGE;

    move-object v0, p0

    .line 2089273
    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 2089274
    if-nez v0, :cond_1

    .line 2089275
    :cond_0
    :goto_0
    return-void

    .line 2089276
    :cond_1
    iget-object v2, v1, LX/EIU;->U:LX/EIX;

    if-nez v2, :cond_2

    .line 2089277
    new-instance v2, LX/EIX;

    invoke-virtual {v1}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/EIX;-><init>(Landroid/content/Context;)V

    iput-object v2, v1, LX/EIU;->U:LX/EIX;

    .line 2089278
    iget-object v2, v1, LX/EIU;->U:LX/EIX;

    new-instance v3, LX/EIJ;

    invoke-direct {v3, v1}, LX/EIJ;-><init>(LX/EIU;)V

    .line 2089279
    iput-object v3, v2, LX/EIX;->c:LX/EIJ;

    .line 2089280
    :cond_2
    iget-object v2, v1, LX/EIU;->T:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 2089281
    iget-object v2, v1, LX/EIU;->T:Landroid/widget/FrameLayout;

    iget-object v3, v1, LX/EIU;->U:LX/EIX;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 2089282
    iget-object v2, v1, LX/EIU;->T:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2089283
    iget-object v2, v1, LX/EIU;->B:Landroid/view/View;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1}, LX/EIU;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a02fb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2089284
    iget-object v2, v1, LX/EIU;->B:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2089285
    iget-object v2, v0, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v1, v2, v3}, LX/EIU;->setProfileImageForUser(LX/EIU;J)V

    .line 2089286
    iget-object v2, v1, LX/EIU;->F:Landroid/widget/TextView;

    iget-object v3, v0, LX/EGE;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2089287
    iget-object v2, v1, LX/EIU;->F:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2089288
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2089289
    invoke-virtual {v1}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080808

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2089290
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2089291
    invoke-virtual {v1}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080809

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2089292
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v8}, LX/EIU;->a(Ljava/lang/String;Z)V

    .line 2089293
    iget-object v2, v1, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v7}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2089294
    iget-object v2, v1, LX/EIU;->M:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v7}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    .line 2089295
    invoke-static {v1, v6}, LX/EIU;->e(LX/EIU;I)V

    .line 2089296
    iput-boolean v8, v1, LX/EIU;->am:Z

    .line 2089297
    iget-object v2, v1, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v2, v7}, Lcom/facebook/rtc/views/RtcActionBar;->setRosterButtonVisibility(I)V

    .line 2089298
    iget-object v2, v1, LX/EIU;->R:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private az()V
    .locals 1

    .prologue
    .line 2089267
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    .line 2089268
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->w()V

    .line 2089269
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2089260
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aK:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2089261
    return-void
.end method

.method public static b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)Z
    .locals 1

    .prologue
    .line 2088455
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->X:LX/EC8;

    invoke-virtual {p1, v0}, LX/EC8;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;J)V
    .locals 9

    .prologue
    .line 2088626
    const-wide/16 v0, 0x7530

    invoke-static {p0, v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->c(Lcom/facebook/rtc/activities/WebrtcIncallActivity;J)V

    .line 2088627
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->l(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088628
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088629
    sget-object v0, LX/EC8;->REDIAL:LX/EC8;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)V

    .line 2088630
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    iget-wide v4, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Z:J

    const-string v6, "redial_offered"

    const-string v7, ""

    move-wide v2, p1

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 2088631
    return-void
.end method

.method public static b$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V
    .locals 9

    .prologue
    .line 2088598
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    .line 2088599
    iget-object v1, v0, LX/3A0;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aU()J

    move-result-wide v1

    sget-object v3, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v0, v3}, LX/3A0;->a(LX/0c1;)I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v1, v3

    iput-wide v1, v0, LX/3A0;->I:J

    .line 2088600
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    .line 2088601
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->c()V

    .line 2088602
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2088603
    const/4 v2, 0x0

    .line 2088604
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short v5, LX/3Dx;->b:S

    invoke-interface {v1, v3, v4, v5, v2}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    .line 2088605
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aJ()Z

    move-result v1

    sget-object v4, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v3, v1, v4}, LX/3A0;->a(ZLX/0c1;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2088606
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aU()J

    move-result-wide v3

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 2088607
    iget-wide v7, v1, LX/EDx;->aK:J

    move-wide v5, v7

    .line 2088608
    sub-long/2addr v3, v5

    .line 2088609
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v1, v5}, LX/3A0;->a(LX/0c1;)I

    move-result v1

    int-to-long v5, v1

    cmp-long v1, v3, v5

    if-gtz v1, :cond_5

    const/4 v1, 0x1

    .line 2088610
    :goto_0
    if-eqz v1, :cond_1

    .line 2088611
    const v1, 0x7f080793

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2088612
    :cond_1
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->al(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p1, :cond_3

    .line 2088613
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2088614
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->c$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088615
    :cond_3
    :goto_1
    return-void

    .line 2088616
    :cond_4
    sget-object v0, LX/EC8;->NO_ANSWER:LX/EC8;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2088617
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    .line 2088618
    :goto_2
    goto :goto_1

    :cond_5
    move v1, v2

    .line 2088619
    goto :goto_0

    .line 2088620
    :cond_6
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2088621
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->I$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_2

    .line 2088622
    :cond_7
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/7TQ;->CallEndHangupCall:LX/7TQ;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/7TQ;)V

    .line 2088623
    const v0, 0x7f08073a

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088624
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->x()V

    .line 2088625
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->I$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_2
.end method

.method private c(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 2088532
    iput-boolean v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aa:Z

    .line 2088533
    const/4 v1, 0x0

    .line 2088534
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2088535
    const-string v2, "com.facebook.rtc.fbwebrtc.intent.action.SHOW_UI"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.facebook.rtc.fbwebrtc.intent.action.INCOMING_CALL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2088536
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    if-nez v0, :cond_e

    .line 2088537
    sget-object v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Y:Ljava/lang/Class;

    const-string v2, "Call is already finished."

    invoke-static {v0, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move v0, v1

    .line 2088538
    :goto_0
    move v0, v0

    .line 2088539
    if-nez v0, :cond_1

    .line 2088540
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    .line 2088541
    :goto_1
    return-void

    .line 2088542
    :cond_1
    invoke-direct {p0, v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Z)V

    .line 2088543
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088544
    iget-wide v5, v0, LX/EDx;->aL:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-lez v5, :cond_10

    .line 2088545
    :goto_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088546
    iget-wide v5, v0, LX/EDx;->ak:J

    move-wide v0, v5

    .line 2088547
    iput-wide v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Z:J

    .line 2088548
    new-instance v0, LX/EC3;

    invoke-direct {v0, p0}, LX/EC3;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->U:LX/EC2;

    .line 2088549
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->U:LX/EC2;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EC2;)V

    .line 2088550
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ah()V

    .line 2088551
    invoke-direct {p0, p1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Landroid/content/Intent;)V

    .line 2088552
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2088553
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2088554
    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2088555
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aU:I

    .line 2088556
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088557
    iget v1, v0, LX/EDx;->am:I

    move v0, v1

    .line 2088558
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088559
    iget v1, v0, LX/EDx;->am:I

    move v0, v1

    .line 2088560
    if-ne v4, v0, :cond_3

    .line 2088561
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    invoke-virtual {v0}, LX/2S7;->h()V

    .line 2088562
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v1

    .line 2088563
    if-eqz v1, :cond_4

    .line 2088564
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    sget-object v2, LX/EHD;->AUDIO_CONFERENCE:LX/EHD;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/RtcActionBar;->setType(LX/EHD;)V

    .line 2088565
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aS()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2088566
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088567
    iget-boolean v2, v0, LX/EDx;->aB:Z

    move v0, v2

    .line 2088568
    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    .line 2088569
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Z)V

    .line 2088570
    :goto_3
    invoke-virtual {p0, v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->setVolumeControlStream(I)V

    .line 2088571
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    sget-object v2, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v1, v0, v2}, LX/3A0;->a(ZLX/0c1;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2088572
    const v0, 0x7f08072c

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088573
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity$7;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$7;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v2, v3}, LX/3A0;->a(LX/0c1;)I

    move-result v2

    int-to-long v2, v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2088574
    :cond_5
    :goto_4
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto/16 :goto_1

    .line 2088575
    :cond_6
    invoke-direct {p0, v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Z)V

    .line 2088576
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->v(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_3

    .line 2088577
    :cond_7
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_4

    .line 2088578
    :cond_8
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aZ()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2088579
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->v(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088580
    invoke-virtual {p0, v4}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->setVolumeControlStream(I)V

    .line 2088581
    if-eqz v1, :cond_a

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->az()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 2088582
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->w$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088583
    :cond_9
    :goto_5
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ai()V

    goto :goto_4

    .line 2088584
    :cond_a
    if-nez v1, :cond_9

    .line 2088585
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EFj;

    invoke-virtual {v0}, LX/EFj;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    goto :goto_5

    .line 2088586
    :cond_b
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->af()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2088587
    if-nez v1, :cond_c

    .line 2088588
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->v(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088589
    :cond_c
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2088590
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->au()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2088591
    const v0, 0x7f080790

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2088592
    :cond_d
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EFj;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    .line 2088593
    iget-object v2, v1, LX/EDx;->bl:LX/7TQ;

    move-object v1, v2

    .line 2088594
    sget-object v2, LX/EFi;->Activity:LX/EFi;

    invoke-virtual {v0, v1, v2}, LX/EFj;->a(LX/7TQ;LX/EFi;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 2088595
    :cond_e
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_f
    move v0, v1

    .line 2088596
    goto/16 :goto_0

    .line 2088597
    :cond_10
    invoke-virtual {v0}, LX/EDx;->aU()J

    move-result-wide v5

    iput-wide v5, v0, LX/EDx;->aL:J

    goto/16 :goto_2
.end method

.method private static c(Lcom/facebook/rtc/activities/WebrtcIncallActivity;J)V
    .locals 3

    .prologue
    .line 2088529
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088530
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity$16;

    invoke-direct {v1, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity$16;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ao:Ljava/util/concurrent/Future;

    .line 2088531
    return-void
.end method

.method public static c$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V
    .locals 2

    .prologue
    .line 2088524
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088525
    iget-object v1, v0, LX/EDx;->bb:LX/EIq;

    move-object v0, v1

    .line 2088526
    invoke-virtual {v0, p1}, LX/EIq;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088527
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    .line 2088528
    :cond_0
    return-void
.end method

.method private e(Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2088519
    const-string v0, "AUTO_ACCEPT"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aW:Z

    .line 2088520
    const-string v0, "SHOW_EXPRESSION_UI"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aX:Z

    .line 2088521
    const-string v0, "SHOW_GROUP_CALL_ROSTER"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aY:Z

    .line 2088522
    const-string v0, "END_CALL"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ba:Z

    .line 2088523
    return-void
.end method

.method public static e(Lcom/facebook/rtc/activities/WebrtcIncallActivity;I)V
    .locals 3

    .prologue
    .line 2088500
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av()V

    .line 2088501
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bj()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2088502
    :cond_0
    sget-object v0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Y:Ljava/lang/Class;

    const-string v1, "unable to start group call countdown"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2088503
    :goto_0
    return-void

    .line 2088504
    :cond_1
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088505
    iget-boolean p0, v0, LX/EDx;->ai:Z

    move v0, p0

    .line 2088506
    iget-object v2, v1, LX/EIU;->W:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->e()V

    .line 2088507
    iget-object v2, v1, LX/EIU;->W:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;

    .line 2088508
    iget-boolean p0, v2, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->b:Z

    move p0, p0

    .line 2088509
    if-nez p0, :cond_3

    .line 2088510
    const/4 p0, 0x1

    invoke-virtual {v2, p0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->b(Z)V

    .line 2088511
    invoke-virtual {v2, p1}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->setDuration(I)V

    .line 2088512
    new-instance p0, LX/EIP;

    invoke-direct {p0, v1}, LX/EIP;-><init>(LX/EIU;)V

    .line 2088513
    iput-object p0, v2, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->g:LX/EHM;

    .line 2088514
    invoke-virtual {v2, v0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a(Z)V

    .line 2088515
    :cond_2
    :goto_1
    invoke-virtual {v1}, LX/EIU;->v()V

    .line 2088516
    goto :goto_0

    .line 2088517
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->d()Z

    move-result p0

    if-nez p0, :cond_2

    .line 2088518
    invoke-virtual {v2, v0}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a(Z)V

    goto :goto_1
.end method

.method private e(Z)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 2088482
    if-eqz p1, :cond_1

    .line 2088483
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-nez v0, :cond_0

    .line 2088484
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aN:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2088485
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aO:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2088486
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->o$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088487
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088488
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aM:Lcom/facebook/rtc/views/VoipConnectionBanner;

    sget-object v1, LX/EIH;->CONFERENCE:LX/EIH;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/VoipConnectionBanner;->setTheme(LX/EIH;)V

    .line 2088489
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aM:Lcom/facebook/rtc/views/VoipConnectionBanner;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/VoipConnectionBanner;->setVisibility(I)V

    .line 2088490
    :goto_0
    return-void

    .line 2088491
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-nez v0, :cond_2

    .line 2088492
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aN:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2088493
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aO:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2088494
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    if-eqz v0, :cond_3

    .line 2088495
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    invoke-virtual {v0}, LX/ED6;->f()V

    .line 2088496
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    .line 2088497
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2088498
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aM:Lcom/facebook/rtc/views/VoipConnectionBanner;

    invoke-virtual {v0, v2}, Lcom/facebook/rtc/views/VoipConnectionBanner;->setVisibility(I)V

    goto :goto_0

    .line 2088499
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aM:Lcom/facebook/rtc/views/VoipConnectionBanner;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/VoipConnectionBanner;->setVisibility(I)V

    goto :goto_0
.end method

.method public static f(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2088472
    if-eqz p1, :cond_0

    sget-object v0, LX/EC8;->INCALL_GROUP_ESCALATED:LX/EC8;

    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)V

    .line 2088473
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088474
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aH(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088475
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088476
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aJ:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2088477
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aZ()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2088478
    invoke-direct {p0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Z)V

    .line 2088479
    :goto_1
    return-void

    .line 2088480
    :cond_0
    sget-object v0, LX/EC8;->INCALL:LX/EC8;

    goto :goto_0

    .line 2088481
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Z)V

    goto :goto_1
.end method

.method public static h(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V
    .locals 1

    .prologue
    .line 2088466
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088467
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ba()V

    .line 2088468
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088469
    invoke-static {p0, p1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088470
    :goto_0
    return-void

    .line 2088471
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->bb()V

    goto :goto_0
.end method

.method public static i$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V
    .locals 4

    .prologue
    .line 2088456
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->J:LX/0yH;

    sget-object v1, LX/0yY;->VOIP_INCOMING_CALL_INTERSTITIAL:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yH;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B:LX/0ad;

    sget-object v1, LX/0c0;->Cached:LX/0c0;

    sget-short v2, LX/3Dx;->fm:S

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088457
    const/4 v2, 0x1

    .line 2088458
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    const-string v1, "zero_rating_shown"

    invoke-virtual {v0, v1, v2}, LX/2S7;->b(Ljava/lang/String;Z)Z

    .line 2088459
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/rtc/activities/RtcZeroRatingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2088460
    const-string v1, "ACTION_INCOMING_CALL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2088461
    const-string v1, "EXTRA_AFTER_INCOMING_CALL_SCREEN"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2088462
    const-string v1, "EXTRA_ENABLE_SELF_VIEW"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2088463
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->K:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2088464
    :goto_0
    return-void

    .line 2088465
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->j(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    goto :goto_0
.end method

.method public static j(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V
    .locals 8

    .prologue
    .line 2088259
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aw()V

    .line 2088260
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2088261
    :goto_0
    return-void

    .line 2088262
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_2

    .line 2088263
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 2088264
    :goto_1
    iget-object v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bg:LX/0i5;

    if-eqz v0, :cond_4

    sget-object v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->N:[Ljava/lang/String;

    move-object v2, v1

    :goto_2
    if-eqz v0, :cond_5

    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)Ljava/lang/String;

    move-result-object v1

    .line 2088265
    :goto_3
    if-eqz v0, :cond_6

    const v4, 0x7f0807dc

    .line 2088266
    :goto_4
    const v5, 0x7f080011

    invoke-virtual {p0, v5}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2088267
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    invoke-virtual {p0, v4, v6}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 2088268
    new-instance v4, LX/EC4;

    invoke-direct {v4, p0, p1}, LX/EC4;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    invoke-virtual {v3, v2, v1, v0, v4}, LX/0i5;->a([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/6Zx;)V

    .line 2088269
    goto :goto_0

    .line 2088270
    :cond_2
    invoke-static {p0, p1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->h(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    goto :goto_0

    .line 2088271
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2088272
    :cond_4
    sget-object v1, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->M:[Ljava/lang/String;

    move-object v2, v1

    goto :goto_2

    .line 2088273
    :cond_5
    const v1, 0x7f080011

    invoke-virtual {p0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2088274
    const v4, 0x7f0807d8

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2088275
    goto :goto_3

    .line 2088276
    :cond_6
    const v4, 0x7f0807d9

    goto :goto_4
.end method

.method public static k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V
    .locals 5

    .prologue
    .line 2088300
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2088301
    if-eqz p1, :cond_7

    .line 2088302
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->P(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088303
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av()V

    .line 2088304
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2088305
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    sget-object v1, LX/EC6;->MULTI_VIEW:LX/EC6;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-nez v0, :cond_8

    .line 2088306
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->az()V

    .line 2088307
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aY()V

    .line 2088308
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2088309
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ae:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/EDx;->b(Ljava/lang/String;)LX/EGE;

    move-result-object v1

    .line 2088310
    if-eqz v1, :cond_1

    .line 2088311
    iput-boolean p1, v1, LX/EGE;->i:Z

    .line 2088312
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088313
    if-eqz v1, :cond_1

    iget-object v2, v1, LX/EGE;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2088314
    :cond_1
    :goto_2
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->p(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088315
    :cond_2
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088316
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aH(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088317
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088318
    return-void

    .line 2088319
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2088320
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    sget-object v1, LX/EC6;->FULL:LX/EC6;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-nez v0, :cond_e

    .line 2088321
    :cond_4
    :goto_3
    goto :goto_0

    .line 2088322
    :cond_5
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    sget-object v1, LX/EC6;->FLOAT:LX/EC6;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2088323
    :cond_6
    :goto_4
    goto/16 :goto_0

    .line 2088324
    :cond_7
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Q(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088325
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EDv;->STOPPED:LX/EDv;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EDv;)V

    .line 2088326
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA()V

    .line 2088327
    sget-object v0, LX/EC6;->NONE:LX/EC6;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    goto/16 :goto_1

    .line 2088328
    :cond_8
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Q(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088329
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    .line 2088330
    iget-object v2, v0, LX/EIU;->Q:LX/EH4;

    if-nez v2, :cond_9

    .line 2088331
    new-instance v2, LX/EH4;

    invoke-virtual {v0}, LX/EIU;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/EH4;-><init>(Landroid/content/Context;)V

    iput-object v2, v0, LX/EIU;->Q:LX/EH4;

    .line 2088332
    :cond_9
    iget-object v2, v0, LX/EIU;->Q:LX/EH4;

    .line 2088333
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2088334
    iget-object v3, v2, LX/EH4;->b:Landroid/view/View;

    if-eqz v3, :cond_a

    .line 2088335
    iget-object v3, v2, LX/EH4;->b:Landroid/view/View;

    invoke-virtual {v2, v3}, LX/EH4;->removeView(Landroid/view/View;)V

    .line 2088336
    :cond_a
    iput-object v1, v2, LX/EH4;->b:Landroid/view/View;

    .line 2088337
    iget-object v3, v2, LX/EH4;->b:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/EH4;->addView(Landroid/view/View;I)V

    .line 2088338
    iget-object v2, v0, LX/EIU;->an:LX/EIT;

    sget-object v3, LX/EIT;->DOMINANT_SPEAKER_VIEW:LX/EIT;

    if-ne v2, v3, :cond_c

    .line 2088339
    invoke-static {v0}, LX/EIU;->af(LX/EIU;)V

    .line 2088340
    :cond_b
    :goto_5
    sget-object v0, LX/EC6;->MULTI_VIEW:LX/EC6;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    goto/16 :goto_0

    .line 2088341
    :cond_c
    iget-object v2, v0, LX/EIU;->an:LX/EIT;

    sget-object v3, LX/EIT;->GRID_VIEW:LX/EIT;

    if-ne v2, v3, :cond_b

    .line 2088342
    invoke-static {v0}, LX/EIU;->ae(LX/EIU;)V

    goto :goto_5

    .line 2088343
    :cond_d
    iget-object v2, v0, LX/EDx;->ae:Ljava/util/Map;

    iget-object v3, v1, LX/EGE;->b:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 2088344
    :cond_e
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Q(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088345
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    .line 2088346
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2088347
    iget-object v2, v0, LX/EIU;->V:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbFrameLayout;->getChildCount()I

    move-result v2

    if-lez v2, :cond_f

    .line 2088348
    :goto_6
    sget-object v0, LX/EC6;->FULL:LX/EC6;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    goto/16 :goto_3

    .line 2088349
    :cond_f
    iget-object v2, v0, LX/EIU;->V:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->addView(Landroid/view/View;)V

    .line 2088350
    iget-object v2, v0, LX/EIU;->V:Lcom/facebook/resources/ui/FbFrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    goto :goto_6

    .line 2088351
    :cond_10
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->Q(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088352
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2088353
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    invoke-virtual {v1, v2, v0, v3}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(IILandroid/view/View;)V

    .line 2088354
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->setVisibility(I)V

    .line 2088355
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    .line 2088356
    invoke-static {v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->e$redex0(Lcom/facebook/rtc/views/RtcSpringDragView;)V

    .line 2088357
    sget-object v0, LX/EC6;->FLOAT:LX/EC6;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bd:LX/EC6;

    goto/16 :goto_4
.end method

.method public static l(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V
    .locals 2

    .prologue
    .line 2088358
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    if-eqz v0, :cond_0

    .line 2088359
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    .line 2088360
    iget-object v1, v0, LX/EIY;->d:LX/EH3;

    invoke-virtual {v1, p1}, LX/EH3;->setButtonsEnabled(Z)V

    .line 2088361
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    if-eqz v0, :cond_1

    .line 2088362
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au:LX/EIh;

    .line 2088363
    iget-object v1, v0, LX/EIh;->b:LX/EH3;

    invoke-virtual {v1, p1}, LX/EH3;->setButtonsEnabled(Z)V

    .line 2088364
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_2

    .line 2088365
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2088366
    iget-object v1, v0, LX/EIU;->z:LX/EH3;

    invoke-virtual {v1, p1}, LX/EH3;->setButtonsEnabled(Z)V

    .line 2088367
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    if-eqz v0, :cond_3

    .line 2088368
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    .line 2088369
    iget-object v1, v0, LX/EIZ;->d:Lcom/facebook/rtc/views/RtcIncomingCallButtons;

    invoke-virtual {v1, p1}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->setButtonsEnabled(Z)V

    .line 2088370
    :cond_3
    return-void
.end method

.method public static n(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 1

    .prologue
    .line 2088371
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-nez v0, :cond_1

    .line 2088372
    :cond_0
    :goto_0
    return-void

    .line 2088373
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->z()V

    goto :goto_0
.end method

.method public static o$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2088374
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_1

    .line 2088375
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/EIU;->b(LX/0gc;)V

    .line 2088376
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    if-eqz v0, :cond_0

    .line 2088377
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2088378
    iput-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    .line 2088379
    :cond_0
    :goto_0
    return-void

    .line 2088380
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_2

    .line 2088381
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->l()V

    .line 2088382
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    if-nez v0, :cond_3

    .line 2088383
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ai:LX/ED7;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    sget-object v2, LX/ED4;->AUDIO:LX/ED4;

    invoke-virtual {v0, v1, v2}, LX/ED7;->a(LX/0gc;LX/ED4;)LX/ED6;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    .line 2088384
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2088385
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 2088386
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    invoke-virtual {v0}, LX/ED6;->d()V

    goto :goto_0
.end method

.method public static p(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    .line 2088387
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088388
    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/EDx;->B(LX/EDx;Z)Z

    move-result v1

    move v0, v1

    .line 2088389
    iget-boolean v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-ne v1, v0, :cond_1

    .line 2088390
    :cond_0
    :goto_0
    return-void

    .line 2088391
    :cond_1
    if-eqz v0, :cond_2

    .line 2088392
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088393
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Z)V

    .line 2088394
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    .line 2088395
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->j()V

    goto :goto_0

    .line 2088396
    :cond_2
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ax(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088397
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088398
    sget-object v0, LX/EC8;->INCALL:LX/EC8;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)V

    .line 2088399
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Z)V

    goto :goto_1
.end method

.method public static q$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    .line 2088400
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->q:LX/00H;

    .line 2088401
    iget-object v1, v0, LX/00H;->j:LX/01T;

    move-object v0, v1

    .line 2088402
    sget-object v1, LX/01T;->MESSENGER:LX/01T;

    if-ne v0, v1, :cond_1

    .line 2088403
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088404
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ap()Ljava/lang/String;

    .line 2088405
    :cond_0
    :goto_0
    return-void

    .line 2088406
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    goto :goto_0
.end method

.method public static r$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 4

    .prologue
    .line 2088407
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2088408
    iget-object v1, v0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_2

    .line 2088409
    iget-object v1, v0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2088410
    iget-object v1, v0, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2088411
    :goto_0
    invoke-static {v0}, LX/EIU;->N(LX/EIU;)V

    .line 2088412
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2088413
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    const/high16 v1, 0x3fe00000    # 1.75f

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(F)V

    .line 2088414
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2088415
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->N$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088416
    :cond_0
    :goto_1
    return-void

    .line 2088417
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->c()V

    goto :goto_1

    .line 2088418
    :cond_2
    iget-object v1, v0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 2088419
    iget-object v1, v0, LX/EIU;->A:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public static t(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 8

    .prologue
    .line 2088420
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2088421
    :cond_0
    :goto_0
    return-void

    .line 2088422
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    .line 2088423
    iget-object v1, v0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    if-nez v1, :cond_2

    .line 2088424
    :goto_1
    goto :goto_0

    .line 2088425
    :cond_2
    iget-object v1, v0, LX/ED6;->j:Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    .line 2088426
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->isShown()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2088427
    :cond_3
    :goto_2
    goto :goto_1

    .line 2088428
    :cond_4
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    .line 2088429
    iget-boolean v3, v2, LX/EEp;->b:Z

    move v2, v3

    .line 2088430
    if-nez v2, :cond_5

    .line 2088431
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->g:LX/0hs;

    invoke-virtual {v2}, LX/0ht;->l()V

    .line 2088432
    invoke-static {v1}, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->k(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;)V

    goto :goto_2

    .line 2088433
    :cond_5
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->h:Ljava/util/concurrent/ScheduledFuture;

    if-nez v2, :cond_3

    .line 2088434
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->c:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-short v4, LX/3Dx;->dG:S

    const/4 v5, 0x0

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2088435
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->c:LX/0ad;

    sget-object v3, LX/0c0;->Cached:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget v5, LX/3Dx;->dF:I

    const/16 v6, 0x1e

    invoke-interface {v2, v3, v4, v5, v6}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    .line 2088436
    iget-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v4, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment$1;

    invoke-direct {v4, v1}, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment$1;-><init>(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;)V

    int-to-long v6, v3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v6, v7, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->h:Ljava/util/concurrent/ScheduledFuture;

    goto :goto_2
.end method

.method public static u(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2088437
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aQ()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088438
    const v0, 0x7f08072d

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088439
    const v0, 0x7f08072d

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;Z)V

    .line 2088440
    :goto_0
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ai()V

    .line 2088441
    return-void

    .line 2088442
    :cond_0
    const v0, 0x7f08072b

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088443
    const v0, 0x7f08072b

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public static v(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    .line 2088444
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aZ()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088445
    iget-boolean v1, v0, LX/EDx;->aj:Z

    move v0, v1

    .line 2088446
    if-eqz v0, :cond_2

    .line 2088447
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aB()Ljava/lang/String;

    move-result-object v0

    .line 2088448
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2088449
    :goto_0
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aJ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2088450
    return-void

    .line 2088451
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2088452
    const v0, 0x7f080719

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2088453
    :cond_1
    const v0, 0x7f080718

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2088454
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ay()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static w$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2088277
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aB()Ljava/lang/String;

    move-result-object v1

    .line 2088278
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088279
    iget-boolean v2, v0, LX/EDx;->aj:Z

    move v2, v2

    .line 2088280
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v3

    .line 2088281
    const-string v0, ""

    .line 2088282
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2088283
    if-eqz v2, :cond_2

    .line 2088284
    if-eqz v3, :cond_1

    .line 2088285
    const v0, 0x7f080714

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2088286
    :cond_0
    :goto_0
    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088287
    return-void

    .line 2088288
    :cond_1
    const v0, 0x7f080713

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2088289
    :cond_2
    const v0, 0x7f080712

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v1, v2, v5

    invoke-virtual {p0, v0, v2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2088290
    :cond_3
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->F:LX/EG4;

    if-eqz v1, :cond_0

    .line 2088291
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->F:LX/EG4;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088292
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->az()Ljava/lang/String;

    invoke-interface {v1}, LX/EG4;->c()Ljava/lang/String;

    move-result-object v0

    .line 2088293
    if-eqz v0, :cond_4

    .line 2088294
    :goto_1
    if-eqz v2, :cond_6

    .line 2088295
    if-eqz v3, :cond_5

    .line 2088296
    const v1, 0x7f080717

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2088297
    :cond_4
    const-string v0, ""

    goto :goto_1

    .line 2088298
    :cond_5
    const v1, 0x7f080716

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2088299
    :cond_6
    const v1, 0x7f080715

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static y$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V
    .locals 2

    .prologue
    .line 2088839
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088840
    iget v1, v0, LX/EDx;->am:I

    move v0, v1

    .line 2088841
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2088842
    sget-object v0, LX/EC8;->INCOMING_CALL:LX/EC8;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)V

    .line 2088843
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088844
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ak()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2088845
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ac(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088846
    :cond_0
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088847
    :goto_0
    return-void

    .line 2088848
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088849
    iget-boolean v1, v0, LX/EDx;->bs:Z

    move v0, v1

    .line 2088850
    if-eqz v0, :cond_2

    .line 2088851
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->au(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0

    .line 2088852
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->N()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2088853
    :cond_3
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aw(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_0

    .line 2088854
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2088855
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->av()V

    .line 2088856
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    goto :goto_0

    .line 2088857
    :cond_5
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA()V

    goto :goto_0
.end method


# virtual methods
.method public final a(IIII)V
    .locals 2

    .prologue
    .line 2088981
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2088982
    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 2088983
    iput p2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 2088984
    iput p3, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 2088985
    iput p4, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 2088986
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v1, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2088987
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 2088979
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    invoke-virtual {v0, p1, p2}, LX/EDO;->a(ILjava/lang/String;)V

    .line 2088980
    return-void
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2088966
    if-lez p1, :cond_1

    .line 2088967
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->p:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v2

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->p:Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMicrophoneMute()Z

    move-result v3

    iget-wide v4, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aZ:J

    move v1, p1

    invoke-virtual/range {v0 .. v5}, LX/2S7;->a(IZZJ)V

    .line 2088968
    iput p1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aU:I

    .line 2088969
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    const-string v1, "rating5"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2088970
    if-eqz p2, :cond_0

    .line 2088971
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    const-string v1, "survey_choice"

    invoke-virtual {v0, v1, p2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2088972
    :cond_0
    if-eqz p3, :cond_1

    .line 2088973
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    const-string v1, "survey_details"

    invoke-virtual {v0, v1, p3}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2088974
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    invoke-virtual {v0}, LX/2S7;->g()V

    .line 2088975
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bb:Z

    if-nez v0, :cond_2

    .line 2088976
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    .line 2088977
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bb:Z

    .line 2088978
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2088962
    invoke-static {p0, p1, p2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->c(Lcom/facebook/rtc/activities/WebrtcIncallActivity;J)V

    .line 2088963
    const-wide/32 v0, 0x1d4c0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 2088964
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    const-string v1, "survey_shown"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2088965
    :cond_0
    return-void
.end method

.method public final a(LX/EIp;)V
    .locals 1

    .prologue
    .line 2088959
    sget-object v0, LX/EIp;->FINISHED:LX/EIp;

    if-ne p1, v0, :cond_0

    .line 2088960
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    .line 2088961
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2088939
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(Landroid/content/Intent;)V

    .line 2088940
    invoke-direct {p0, p1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Landroid/content/Intent;)V

    .line 2088941
    const-string v0, "com.facebook.rtc.fbwebrtc.intent.action.INCOMING_CALL"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2088942
    :cond_0
    :goto_0
    return-void

    .line 2088943
    :cond_1
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->l(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088944
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->am()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2088945
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    invoke-virtual {v0}, LX/2S7;->g()V

    .line 2088946
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aq:LX/2EJ;

    if-eqz v0, :cond_3

    .line 2088947
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aq:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 2088948
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aq:LX/2EJ;

    .line 2088949
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    if-eqz v0, :cond_4

    .line 2088950
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/EIY;->setVoicemailButtonsVisible(Z)V

    .line 2088951
    :cond_4
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088952
    invoke-direct {p0, p1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->c(Landroid/content/Intent;)V

    .line 2088953
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->y$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088954
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    if-eqz v0, :cond_0

    .line 2088955
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->as:LX/EIZ;

    .line 2088956
    iget-object v1, v0, LX/EIZ;->d:Lcom/facebook/rtc/views/RtcIncomingCallButtons;

    .line 2088957
    invoke-static {v1}, Lcom/facebook/rtc/views/RtcIncomingCallButtons;->b(Lcom/facebook/rtc/views/RtcIncomingCallButtons;)V

    .line 2088958
    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;JJLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2088938
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2088937
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aa:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2088933
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088934
    const v0, 0x7f080790

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Ljava/lang/String;)V

    .line 2088935
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->at:LX/EIY;

    invoke-virtual {v0}, LX/EIY;->a()V

    .line 2088936
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 2088931
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    iget-boolean v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->S:Z

    invoke-virtual {v0, p1, v1}, LX/EDO;->a(IZ)V

    .line 2088932
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2088858
    invoke-static {p0, p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2088859
    iput-boolean v6, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bf:Z

    .line 2088860
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aU()J

    move-result-wide v2

    .line 2088861
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    invoke-virtual {v0}, LX/3A0;->e()V

    .line 2088862
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7d6

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    .line 2088863
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x280080

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 2088864
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    .line 2088865
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->v:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 2088866
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 2088867
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xd

    if-lt v4, v5, :cond_4

    .line 2088868
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2088869
    :goto_0
    iget v0, v1, Landroid/graphics/Point;->x:I

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2088870
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    int-to-float v0, v0

    invoke-static {v1, v0}, LX/0tP;->b(Landroid/content/res/Resources;F)I

    move-result v0

    .line 2088871
    const/16 v1, 0x1d6

    if-lt v0, v1, :cond_0

    .line 2088872
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aL(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088873
    iput-boolean v6, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bc:Z

    .line 2088874
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    .line 2088875
    iget-object v1, v0, LX/3A0;->G:LX/2EJ;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/3A0;->G:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2088876
    :try_start_0
    iget-object v1, v0, LX/3A0;->G:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2088877
    :cond_1
    :goto_1
    const/4 v1, 0x0

    iput-object v1, v0, LX/3A0;->G:LX/2EJ;

    .line 2088878
    const v0, 0x7f0315f4

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->setContentView(I)V

    .line 2088879
    const v0, 0x7f0d314d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aR:Landroid/widget/FrameLayout;

    .line 2088880
    const v0, 0x7f0d3154

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aN:Landroid/view/View;

    .line 2088881
    const v0, 0x7f0d3152

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aO:Landroid/view/View;

    .line 2088882
    const v0, 0x7f0d3153

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aP:Landroid/support/v4/view/ViewPager;

    .line 2088883
    const v0, 0x7f0d314e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aA:Landroid/widget/LinearLayout;

    .line 2088884
    const v0, 0x7f0d3155

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aH:Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2088885
    const v0, 0x7f0d3146

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcLevelTileView;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aI:Lcom/facebook/rtc/views/RtcLevelTileView;

    .line 2088886
    const v0, 0x7f0d3148

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aJ:Landroid/widget/TextView;

    .line 2088887
    const v0, 0x7f0d3156

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aK:Landroid/widget/TextView;

    .line 2088888
    const v0, 0x7f0d3150

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/ChildLockBanner;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aL:Lcom/facebook/rtc/views/ChildLockBanner;

    .line 2088889
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aL:Lcom/facebook/rtc/views/ChildLockBanner;

    invoke-virtual {v0, v6}, Lcom/facebook/rtc/views/ChildLockBanner;->setColorScheme(Z)V

    .line 2088890
    const v0, 0x7f0d3151

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/VoipConnectionBanner;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aM:Lcom/facebook/rtc/views/VoipConnectionBanner;

    .line 2088891
    const v0, 0x7f0d3157

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcSnakeView;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->az:Lcom/facebook/rtc/views/RtcSnakeView;

    .line 2088892
    const v0, 0x7f0d3158

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcSpringDragView;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    .line 2088893
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v0, p0}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(LX/ECB;)V

    .line 2088894
    const v0, 0x7f0d314c

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    .line 2088895
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    new-instance v1, LX/EBp;

    invoke-direct {v1, p0}, LX/EBp;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088896
    iput-object v1, v0, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->f:LX/EBo;

    .line 2088897
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aL:Lcom/facebook/rtc/views/ChildLockBanner;

    invoke-virtual {v0, v1}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->a(Lcom/facebook/rtc/views/ChildLockBanner;)V

    .line 2088898
    const v0, 0x7f0d314f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/views/RtcActionBar;

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    .line 2088899
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    new-instance v1, LX/EBw;

    invoke-direct {v1, p0}, LX/EBw;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088900
    iput-object v1, v0, Lcom/facebook/rtc/views/RtcActionBar;->B:LX/EBv;

    .line 2088901
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->E:LX/ECj;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    new-instance v4, LX/EBy;

    invoke-direct {v4, p0}, LX/EBy;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    invoke-virtual {v0, v1, v4}, LX/ECj;->a(Ljava/lang/Boolean;LX/EBx;)LX/ECi;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ad:LX/ECi;

    .line 2088902
    new-instance v0, LX/EBz;

    invoke-direct {v0, p0}, LX/EBz;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aF:LX/EBz;

    .line 2088903
    new-instance v0, LX/EC1;

    invoke-direct {v0, p0}, LX/EC1;-><init>(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ar:LX/EC0;

    .line 2088904
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ar:LX/EC0;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EC0;)V

    .line 2088905
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088906
    iget-object v1, v0, LX/EDx;->bb:LX/EIq;

    move-object v0, v1

    .line 2088907
    invoke-virtual {v0, p0}, LX/EIq;->a(LX/ECD;)V

    .line 2088908
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->c(Landroid/content/Intent;)V

    .line 2088909
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aU()J

    move-result-wide v0

    .line 2088910
    sub-long/2addr v0, v2

    .line 2088911
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    const-string v3, "ui_init"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/2S7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2088912
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088913
    iget-boolean v1, v0, LX/EDx;->aB:Z

    move v0, v1

    .line 2088914
    if-nez v0, :cond_2

    const-string v0, "com.facebook.rtc.fbwebrtc.intent.action.INCOMING_CALL"

    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->q:LX/00H;

    .line 2088915
    iget-object v1, v0, LX/00H;->i:LX/01S;

    move-object v0, v1

    .line 2088916
    sget-object v1, LX/01S;->DEVELOPMENT:LX/01S;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/2S4;->K:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2088917
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->j(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088918
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->C:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bg:LX/0i5;

    .line 2088919
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ac:LX/EDP;

    invoke-virtual {v0, p0}, LX/EDP;->a(LX/EC9;)LX/EDO;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    .line 2088920
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->af()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aX()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2088921
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ar:LX/EC0;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v2

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088922
    iget-boolean v3, v0, LX/EDx;->ba:Z

    if-eqz v3, :cond_5

    .line 2088923
    iget-boolean v3, v0, LX/EDx;->aq:Z

    move v3, v3

    .line 2088924
    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_2
    move v0, v3

    .line 2088925
    invoke-virtual {v1, v2, v0}, LX/EC0;->a(ZZ)V

    .line 2088926
    :cond_3
    return-void

    .line 2088927
    :cond_4
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v4

    iput v4, v1, Landroid/graphics/Point;->x:I

    .line 2088928
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->y:I

    goto/16 :goto_0

    .line 2088929
    :catch_0
    move-exception v1

    .line 2088930
    iget-object v4, v0, LX/3A0;->o:LX/2S7;

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/2S7;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 2088632
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/EDO;->a(IZ)V

    .line 2088633
    return-void
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 2088634
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088635
    const/4 v0, 0x1

    .line 2088636
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final finish()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2088827
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2088828
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    invoke-virtual {v0}, LX/2S7;->g()V

    .line 2088829
    :cond_0
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ag$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088830
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ap:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 2088831
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ap:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2088832
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ap:Ljava/util/concurrent/Future;

    .line 2088833
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_2

    .line 2088834
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->k()V

    .line 2088835
    :cond_2
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2088836
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aV()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2088837
    invoke-virtual {p0, v1, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->overridePendingTransition(II)V

    .line 2088838
    :cond_3
    return-void
.end method

.method public final l()LX/0gc;
    .locals 1

    .prologue
    .line 2088826
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2088816
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2088817
    if-ne p1, v3, :cond_1

    .line 2088818
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 2088819
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    .line 2088820
    if-eqz p3, :cond_0

    .line 2088821
    const-string v1, "EXTRA_ENABLE_SELF_VIEW"

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    invoke-virtual {p3, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2088822
    :cond_0
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    const-string v2, "zero_rating_accepted"

    invoke-virtual {v1, v2, v3}, LX/2S7;->b(Ljava/lang/String;Z)Z

    .line 2088823
    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->j(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088824
    :cond_1
    :goto_0
    return-void

    .line 2088825
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->u:LX/2S7;

    const-string v1, "zero_rating_declined"

    invoke-virtual {v0, v1, v3}, LX/2S7;->b(Ljava/lang/String;Z)Z

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 2088813
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088814
    :goto_0
    return-void

    .line 2088815
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 2088802
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2088803
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    if-eqz v0, :cond_0

    .line 2088804
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2088805
    iget-object v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v1}, Lcom/facebook/rtc/views/RtcSpringDragView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v2}, Lcom/facebook/rtc/views/RtcSpringDragView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2088806
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 2088807
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/facebook/rtc/views/RtcSpringDragView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2088808
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->X()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2088809
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->V()LX/EHZ;

    move-result-object v0

    invoke-virtual {v0}, LX/EHZ;->d()V

    .line 2088810
    :cond_1
    return-void

    .line 2088811
    :cond_2
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 2088812
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/facebook/rtc/views/RtcSpringDragView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x22

    const v1, 0x50ec1868

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2088763
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 2088764
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->bf:Z

    if-nez v0, :cond_0

    .line 2088765
    const/16 v0, 0x23

    const v2, 0x28cfb99

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2088766
    :goto_0
    return-void

    .line 2088767
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088768
    iget v2, v0, LX/EDx;->am:I

    move v0, v2

    .line 2088769
    if-nez v0, :cond_1

    .line 2088770
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v3}, LX/EDx;->n(Z)V

    .line 2088771
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088772
    iget-object v2, v0, LX/EDx;->bb:LX/EIq;

    move-object v0, v2

    .line 2088773
    invoke-virtual {v0}, LX/EIq;->a()V

    .line 2088774
    :cond_1
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aI()V

    .line 2088775
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->a()V

    .line 2088776
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2088777
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->U:LX/EC2;

    invoke-virtual {v0, v2}, LX/EDx;->b(LX/EC2;)V

    .line 2088778
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ar:LX/EC0;

    invoke-virtual {v0, v2}, LX/EDx;->b(LX/EC0;)V

    .line 2088779
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088780
    iget-object v2, v0, LX/EDx;->bb:LX/EIq;

    move-object v0, v2

    .line 2088781
    invoke-virtual {v0, p0}, LX/EIq;->b(LX/ECD;)V

    .line 2088782
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    if-eqz v0, :cond_2

    .line 2088783
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aE:Lcom/facebook/rtc/views/RtcActionBar;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/RtcActionBar;->a()V

    .line 2088784
    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_3

    .line 2088785
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    invoke-virtual {v0}, LX/EIU;->E()V

    .line 2088786
    :cond_3
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    if-eqz v0, :cond_4

    .line 2088787
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aQ:LX/ED6;

    invoke-virtual {v0}, LX/ED6;->f()V

    .line 2088788
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->V:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_5

    .line 2088789
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->V:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0, v3}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2088790
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->V:Ljava/util/concurrent/ScheduledFuture;

    .line 2088791
    :cond_5
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aD:Landroid/view/View$OnLayoutChangeListener;

    if-eqz v0, :cond_6

    .line 2088792
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aD:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v2}, LX/EIE;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 2088793
    :cond_6
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    .line 2088794
    iget-object v3, v0, LX/EDx;->ci:LX/EHZ;

    if-eqz v3, :cond_7

    iget-object v3, v0, LX/EDx;->ci:LX/EHZ;

    .line 2088795
    iget-object v4, v3, LX/EHZ;->d:Landroid/view/TextureView;

    move-object v3, v4

    .line 2088796
    if-ne v3, v2, :cond_7

    .line 2088797
    iget-object v3, v0, LX/EDx;->ci:LX/EHZ;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/EHZ;->a(Landroid/view/TextureView;)V

    .line 2088798
    :cond_7
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    if-eqz v0, :cond_8

    invoke-static {}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC:LX/EIE;

    invoke-virtual {v2}, LX/EIE;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2088799
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2088800
    :cond_8
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->am()Z

    .line 2088801
    const v0, -0x32cdd017

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x15e39cd9

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2088735
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 2088736
    iput-boolean v4, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->O:Z

    .line 2088737
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088738
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    if-eqz v0, :cond_0

    .line 2088739
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v4}, LX/EDx;->l(Z)V

    .line 2088740
    invoke-static {p0, v4}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088741
    iput-boolean v4, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->be:Z

    .line 2088742
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2088743
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B:LX/0ad;

    sget-object v2, LX/0c0;->Cached:LX/0c0;

    sget-short v3, LX/3Dx;->ej:S

    invoke-interface {v0, v2, v3}, LX/0ad;->a(LX/0c0;S)V

    .line 2088744
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aV()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/EDx;->o(Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2088745
    iput-boolean v4, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    .line 2088746
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088747
    iget-boolean v2, v0, LX/EDx;->bZ:Z

    move v0, v2

    .line 2088748
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_3

    .line 2088749
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2088750
    iget-object v2, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EDx;

    .line 2088751
    iget-boolean v3, v2, LX/EDx;->bZ:Z

    move v2, v3

    .line 2088752
    if-eqz v2, :cond_2

    iget-object v2, v0, LX/EIU;->W:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2088753
    iget-object v2, v0, LX/EIU;->W:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;

    invoke-virtual {v2}, Lcom/facebook/rtc/views/RtcGroupVideoCountdownOverlay;->a()V

    .line 2088754
    :cond_2
    iget-object v2, v0, LX/EIU;->W:LX/4ob;

    invoke-virtual {v2}, LX/4ob;->d()V

    .line 2088755
    :cond_3
    :goto_0
    const v0, -0x1c591cb3

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 2088756
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2088757
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v2, LX/EDv;->PAUSED:LX/EDv;

    invoke-virtual {v0, v2}, LX/EDx;->a(LX/EDv;)V

    .line 2088758
    :cond_5
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/webrtc/videoengine/VideoCaptureAndroid;->a(Landroid/graphics/SurfaceTexture;)V

    .line 2088759
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088760
    iget-boolean v2, v0, LX/EDx;->bZ:Z

    move v0, v2

    .line 2088761
    if-eqz v0, :cond_3

    .line 2088762
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->e()V

    goto :goto_0
.end method

.method public final onPostResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2088688
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPostResume()V

    .line 2088689
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0, v2}, LX/EDx;->o(Z)Z

    .line 2088690
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EG5;->Activity:LX/EG5;

    invoke-virtual {v0, v1}, LX/EDx;->b(LX/EG5;)V

    .line 2088691
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->P:Z

    if-eqz v0, :cond_0

    .line 2088692
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab()Z

    .line 2088693
    iput-boolean v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->P:Z

    .line 2088694
    :cond_0
    iput-boolean v3, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->O:Z

    .line 2088695
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ag()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088696
    iget-boolean v1, v0, LX/EDx;->aq:Z

    move v0, v1

    .line 2088697
    if-nez v0, :cond_3

    .line 2088698
    invoke-static {p0, v3}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088699
    :cond_3
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->y$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088700
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2088701
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ar:LX/EC0;

    invoke-virtual {v0}, LX/EC0;->p()V

    .line 2088702
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088703
    iget-boolean v1, v0, LX/EDx;->bZ:Z

    move v0, v1

    .line 2088704
    if-eqz v0, :cond_4

    .line 2088705
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088706
    iget v1, v0, LX/EDx;->ca:I

    move v0, v1

    .line 2088707
    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->e(Lcom/facebook/rtc/activities/WebrtcIncallActivity;I)V

    .line 2088708
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088709
    iget-boolean v1, v0, LX/EDx;->cf:Z

    move v0, v1

    .line 2088710
    if-eqz v0, :cond_5

    .line 2088711
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aG:Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;

    invoke-virtual {v0}, Lcom/facebook/rtc/views/TouchOverrideRelativeLayout;->c()V

    .line 2088712
    :cond_5
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_6

    .line 2088713
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2088714
    iget-object v1, v0, LX/EIU;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->j()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    if-nez v1, :cond_a

    .line 2088715
    :cond_6
    :goto_0
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aX:Z

    if-eqz v0, :cond_9

    .line 2088716
    iput-boolean v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aX:Z

    .line 2088717
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2088718
    iget-object v1, v0, LX/EIU;->ap:Landroid/support/v7/widget/RecyclerView;

    move-object v0, v1

    .line 2088719
    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_7

    .line 2088720
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->r$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088721
    :cond_7
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->X()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2088722
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->V()LX/EHZ;

    move-result-object v0

    .line 2088723
    iget v1, v0, LX/EHZ;->s:I

    iget-object v2, v0, LX/EHZ;->G:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    if-eq v1, v2, :cond_8

    .line 2088724
    invoke-virtual {v0}, LX/EHZ;->d()V

    .line 2088725
    :cond_8
    return-void

    .line 2088726
    :cond_9
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aY:Z

    if-eqz v0, :cond_7

    .line 2088727
    iput-boolean v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aY:Z

    .line 2088728
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    if-eqz v0, :cond_7

    .line 2088729
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aS:LX/EIU;

    .line 2088730
    sget-object v1, LX/ED5;->ROSTER:LX/ED5;

    invoke-static {v0, v1}, LX/EIU;->a$redex0(LX/EIU;LX/ED5;)V

    .line 2088731
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->D(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    goto :goto_1

    .line 2088732
    :cond_a
    invoke-static {v0}, LX/EIU;->P(LX/EIU;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2088733
    iget-object v1, v0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    sget-object v3, LX/EHD;->ROSTER_CONFERENCE:LX/EHD;

    invoke-virtual {v1, v3}, Lcom/facebook/rtc/views/RtcActionBar;->setType(LX/EHD;)V

    goto :goto_0

    .line 2088734
    :cond_b
    iget-object v1, v0, LX/EIU;->J:Lcom/facebook/rtc/views/RtcActionBar;

    sget-object v3, LX/EHD;->VIDEO_CONFERENCE:LX/EHD;

    invoke-virtual {v1, v3}, Lcom/facebook/rtc/views/RtcActionBar;->setType(LX/EHD;)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x6663847a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2088684
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 2088685
    iget-boolean v1, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ba:Z

    if-eqz v1, :cond_0

    .line 2088686
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088687
    :cond_0
    const/16 v1, 0x23

    const v2, -0x71c460f4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/16 v0, 0x22

    const v1, 0x17bbad2d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2088666
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 2088667
    iput-boolean v6, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->T:Z

    .line 2088668
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->t:LX/3A0;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    sget-object v3, LX/0c1;->Off:LX/0c1;

    invoke-virtual {v2, v0, v3}, LX/3A0;->a(ZLX/0c1;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2088669
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ak(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088670
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aT()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2088671
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ao:Ljava/util/concurrent/Future;

    if-nez v0, :cond_1

    sget-object v0, LX/EC8;->REDIAL:LX/EC8;

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->b(Lcom/facebook/rtc/activities/WebrtcIncallActivity;LX/EC8;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->at()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2088672
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    .line 2088673
    :cond_1
    const/16 v0, 0x23

    const v2, 0x588d7a4e

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2088674
    :goto_0
    return-void

    .line 2088675
    :cond_2
    const v0, 0x1020002

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2088676
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aB:Lcom/facebook/rtc/views/RtcSpringDragView;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Lcom/facebook/rtc/views/RtcSpringDragView;->a(II)V

    .line 2088677
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aW:Z

    if-eqz v0, :cond_3

    .line 2088678
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aW:Z

    .line 2088679
    iget-object v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ah:LX/3Eb;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088680
    iget-wide v7, v0, LX/EDx;->ac:J

    move-wide v4, v7

    .line 2088681
    invoke-virtual {v2, v4, v5, v6}, LX/3Eb;->a(JZ)V

    .line 2088682
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->i$redex0(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088683
    :cond_3
    const v0, -0x3c3bdb54

    invoke-static {v0, v1}, LX/02F;->c(II)V

    goto :goto_0
.end method

.method public final onStop()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x2

    const/16 v1, 0x22

    const v3, 0x6f4bd468

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2088641
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStop()V

    .line 2088642
    iput-boolean v2, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->T:Z

    .line 2088643
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->K()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aP()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aX()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2088644
    invoke-static {p0, v2}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->k(Lcom/facebook/rtc/activities/WebrtcIncallActivity;Z)V

    .line 2088645
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    sget-object v1, LX/EDv;->PAUSED:LX/EDv;

    invoke-virtual {v0, v1}, LX/EDx;->a(LX/EDv;)V

    .line 2088646
    :cond_1
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ax(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088647
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ak()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2088648
    invoke-direct {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aI()V

    .line 2088649
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->W:Z

    if-eqz v0, :cond_3

    .line 2088650
    invoke-static {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->aC(Lcom/facebook/rtc/activities/WebrtcIncallActivity;)V

    .line 2088651
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finishActivity(I)V

    .line 2088652
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2088653
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aZ()Z

    move-result v1

    .line 2088654
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aV()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2088655
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->B:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    sget-short v6, LX/3Dx;->ek:S

    invoke-interface {v0, v4, v5, v6, v2}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v0

    .line 2088656
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-nez v0, :cond_4

    move v1, v2

    .line 2088657
    :cond_4
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2088658
    :cond_5
    :goto_0
    if-eqz v2, :cond_6

    .line 2088659
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2088660
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aB()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aB()Ljava/lang/String;

    .line 2088661
    :goto_1
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2088662
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aJ()Z

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->H:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    .line 2088663
    :cond_6
    :goto_2
    const v0, 0x444b558e

    invoke-static {v0, v3}, LX/02F;->c(II)V

    return-void

    .line 2088664
    :cond_7
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ay()Ljava/lang/String;

    goto :goto_1

    .line 2088665
    :cond_8
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ay()Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->H:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->I:LX/3RT;

    invoke-virtual {v0}, LX/3RT;->f()Z

    goto :goto_2

    :cond_9
    move v2, v1

    goto :goto_0

    :cond_a
    move v2, v1

    goto :goto_0
.end method

.method public final onUserLeaveHint()V
    .locals 1

    .prologue
    .line 2088637
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onUserLeaveHint()V

    .line 2088638
    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->ab:LX/EDO;

    invoke-virtual {v0}, LX/EDO;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2088639
    invoke-virtual {p0}, Lcom/facebook/rtc/activities/WebrtcIncallActivity;->finish()V

    .line 2088640
    :cond_0
    return-void
.end method
