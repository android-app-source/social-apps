.class public final Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/EGT;


# direct methods
.method public constructor <init>(LX/EGT;)V
    .locals 0

    .prologue
    .line 2097238
    iput-object p1, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;->a:LX/EGT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2097239
    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->X()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2097240
    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-object v1, v0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    const/4 v2, 0x0

    .line 2097241
    iget v3, v0, LX/EDx;->at:I

    if-lez v3, :cond_0

    iget v3, v0, LX/EDx;->au:I

    if-lez v3, :cond_0

    .line 2097242
    iget v3, v0, LX/EDx;->at:I

    iget v4, v0, LX/EDx;->au:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    iget v4, v0, LX/EDx;->at:I

    iget v5, v0, LX/EDx;->au:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    div-float v2, v3, v4

    .line 2097243
    :cond_0
    move v0, v2

    .line 2097244
    iget-object v2, v1, LX/EI9;->f:Lcom/facebook/rtc/views/RtcFloatingSelfView;

    invoke-virtual {v2, v0}, Lcom/facebook/rtc/views/RtcFloatingSelfView;->setCaptureVideoPortraitRatio(F)V

    .line 2097245
    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$2;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    invoke-static {v0}, LX/EGe;->ah(LX/EGe;)V

    .line 2097246
    :cond_1
    return-void
.end method
