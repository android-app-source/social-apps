.class public final Lcom/facebook/rtc/services/BackgroundVideoCallService$2$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/EGT;


# direct methods
.method public constructor <init>(LX/EGT;)V
    .locals 0

    .prologue
    .line 2097221
    iput-object p1, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$1;->a:LX/EGT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 2097222
    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$1;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->D:LX/EI9;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$1;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-boolean v0, v0, LX/EGe;->P:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$1;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2097223
    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$1;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-object v1, v0, LX/EGe;->D:LX/EI9;

    iget-object v0, p0, Lcom/facebook/rtc/services/BackgroundVideoCallService$2$1;->a:LX/EGT;

    iget-object v0, v0, LX/EGT;->a:LX/EGe;

    iget-object v0, v0, LX/EGe;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2097224
    iget-boolean p0, v0, LX/EDx;->cd:Z

    move v0, p0

    .line 2097225
    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 2097226
    if-eqz v0, :cond_0

    iget-object v2, v1, LX/EI9;->q:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    if-nez v2, :cond_0

    .line 2097227
    iget-object v2, v1, LX/EI9;->q:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2}, Lcom/facebook/fbui/glyph/GlyphView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/TransitionDrawable;

    .line 2097228
    invoke-virtual {v2, v7}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 2097229
    const/16 v5, 0x1f4

    invoke-virtual {v2, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2097230
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    move v5, v3

    move v6, v4

    move v9, v7

    move v10, v8

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    iput-object v2, v1, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    .line 2097231
    iget-object v2, v1, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    const-wide/16 v4, 0xfa

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 2097232
    iget-object v2, v1, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    const-wide/16 v4, 0x8ca

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/ScaleAnimation;->setStartOffset(J)V

    .line 2097233
    iget-object v2, v1, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    new-instance v3, LX/EHq;

    invoke-direct {v3, v1}, LX/EHq;-><init>(LX/EI9;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/ScaleAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2097234
    iget-object v2, v1, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    invoke-virtual {v2, v7}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 2097235
    iget-object v2, v1, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    invoke-virtual {v2, v7}, Landroid/view/animation/ScaleAnimation;->setFillBefore(Z)V

    .line 2097236
    iget-object v2, v1, LX/EI9;->q:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v3, v1, LX/EI9;->v:Landroid/view/animation/ScaleAnimation;

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2097237
    :cond_0
    return-void
.end method
