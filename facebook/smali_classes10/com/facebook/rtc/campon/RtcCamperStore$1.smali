.class public final Lcom/facebook/rtc/campon/RtcCamperStore$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/2S3;


# direct methods
.method public constructor <init>(LX/2S3;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2090226
    iput-object p1, p0, Lcom/facebook/rtc/campon/RtcCamperStore$1;->b:LX/2S3;

    iput-object p2, p0, Lcom/facebook/rtc/campon/RtcCamperStore$1;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 2090227
    :try_start_0
    iget-object v0, p0, Lcom/facebook/rtc/campon/RtcCamperStore$1;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/campon/RtcCamperModel;

    .line 2090228
    iget-object v2, p0, Lcom/facebook/rtc/campon/RtcCamperStore$1;->b:LX/2S3;

    invoke-static {v2, v0}, LX/2S3;->a$redex0(LX/2S3;Lcom/facebook/rtc/campon/RtcCamperModel;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2090229
    iget-object v2, p0, Lcom/facebook/rtc/campon/RtcCamperStore$1;->b:LX/2S3;

    iget-wide v4, v0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerId:J

    invoke-virtual {v2, v4, v5}, LX/2S3;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2090230
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/rtc/campon/RtcCamperStore$1;->b:LX/2S3;

    monitor-enter v1

    .line 2090231
    :try_start_1
    iget-object v2, p0, Lcom/facebook/rtc/campon/RtcCamperStore$1;->b:LX/2S3;

    const/4 v3, 0x1

    .line 2090232
    iput-boolean v3, v2, LX/2S3;->j:Z

    .line 2090233
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    throw v0

    .line 2090234
    :cond_1
    iget-object v1, p0, Lcom/facebook/rtc/campon/RtcCamperStore$1;->b:LX/2S3;

    monitor-enter v1

    .line 2090235
    :try_start_2
    iget-object v0, p0, Lcom/facebook/rtc/campon/RtcCamperStore$1;->b:LX/2S3;

    const/4 v2, 0x1

    .line 2090236
    iput-boolean v2, v0, LX/2S3;->j:Z

    .line 2090237
    monitor-exit v1

    .line 2090238
    return-void

    .line 2090239
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :catchall_2
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0
.end method
