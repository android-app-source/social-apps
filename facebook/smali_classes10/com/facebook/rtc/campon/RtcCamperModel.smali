.class public Lcom/facebook/rtc/campon/RtcCamperModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/rtc/campon/RtcCamperModelDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/rtc/campon/RtcCamperModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mCampTrigger:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "camp_trigger"
    .end annotation
.end field

.field public final mCampType:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "camp_type"
    .end annotation
.end field

.field public final mPeerFirstName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "peer_first_name"
    .end annotation
.end field

.field public final mPeerId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "peer_id"
    .end annotation
.end field

.field public final mPeerName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "peer_name"
    .end annotation
.end field

.field public final mStartTimeMs:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "start_time_ms"
    .end annotation
.end field

.field public final mWaitTimeMs:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "wait_time_ms"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2090198
    const-class v0, Lcom/facebook/rtc/campon/RtcCamperModelDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2090188
    new-instance v0, LX/ECZ;

    invoke-direct {v0}, LX/ECZ;-><init>()V

    sput-object v0, Lcom/facebook/rtc/campon/RtcCamperModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2090189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2090190
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampType:I

    .line 2090191
    iput-wide v2, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerId:J

    .line 2090192
    iput-object v1, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerFirstName:Ljava/lang/String;

    .line 2090193
    iput-object v1, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerName:Ljava/lang/String;

    .line 2090194
    iput-wide v2, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mStartTimeMs:J

    .line 2090195
    iput-wide v2, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mWaitTimeMs:J

    .line 2090196
    iput-object v1, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampTrigger:Ljava/lang/String;

    .line 2090197
    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 0

    .prologue
    .line 2090179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2090180
    iput p1, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampType:I

    .line 2090181
    iput-wide p2, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerId:J

    .line 2090182
    iput-object p4, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerFirstName:Ljava/lang/String;

    .line 2090183
    iput-object p5, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerName:Ljava/lang/String;

    .line 2090184
    iput-wide p6, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mStartTimeMs:J

    .line 2090185
    iput-wide p8, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mWaitTimeMs:J

    .line 2090186
    iput-object p10, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampTrigger:Ljava/lang/String;

    .line 2090187
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2090170
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2090171
    iget v0, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2090172
    iget-wide v0, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2090173
    iget-object v0, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2090174
    iget-object v0, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mPeerName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2090175
    iget-wide v0, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mStartTimeMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2090176
    iget-wide v0, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mWaitTimeMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2090177
    iget-object v0, p0, Lcom/facebook/rtc/campon/RtcCamperModel;->mCampTrigger:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2090178
    return-void
.end method
