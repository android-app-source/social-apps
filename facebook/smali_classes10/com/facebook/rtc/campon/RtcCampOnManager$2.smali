.class public final Lcom/facebook/rtc/campon/RtcCampOnManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/ECY;


# direct methods
.method public constructor <init>(LX/ECY;)V
    .locals 0

    .prologue
    .line 2089937
    iput-object p1, p0, Lcom/facebook/rtc/campon/RtcCampOnManager$2;->a:LX/ECY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 2089938
    iget-object v0, p0, Lcom/facebook/rtc/campon/RtcCampOnManager$2;->a:LX/ECY;

    const/4 v3, 0x1

    .line 2089939
    iget-object v1, v0, LX/ECY;->t:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2089940
    invoke-static {v0}, LX/ECY;->l(LX/ECY;)V

    .line 2089941
    :cond_0
    :goto_0
    return-void

    .line 2089942
    :cond_1
    const/4 v1, 0x0

    .line 2089943
    iget-object v2, v0, LX/ECY;->t:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    .line 2089944
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2089945
    :try_start_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2089946
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ECT;

    .line 2089947
    invoke-virtual {v1}, LX/ECT;->e()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2089948
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 2089949
    iget-object v2, v0, LX/ECY;->o:LX/2S3;

    .line 2089950
    iget-wide v7, v1, LX/ECT;->b:J

    move-wide v5, v7

    .line 2089951
    invoke-virtual {v2, v5, v6}, LX/2S3;->a(J)V

    .line 2089952
    invoke-static {v1}, LX/ECY;->e(LX/ECT;)V

    move v2, v3

    .line 2089953
    goto :goto_1

    .line 2089954
    :catch_0
    move-exception v1

    .line 2089955
    sget-object v2, LX/ECY;->a:Ljava/lang/String;

    const-string v3, "campers map is modified, waiting for next round of check."

    invoke-static {v2, v3, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2089956
    :cond_3
    iget-object v5, v1, LX/ECT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v5, v5

    .line 2089957
    if-nez v5, :cond_2

    .line 2089958
    invoke-virtual {v1}, LX/ECT;->f()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2089959
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 2089960
    iget-object v2, v0, LX/ECY;->o:LX/2S3;

    .line 2089961
    iget-wide v7, v1, LX/ECT;->b:J

    move-wide v5, v7

    .line 2089962
    invoke-virtual {v2, v5, v6}, LX/2S3;->a(J)V

    .line 2089963
    invoke-virtual {v1}, LX/ECT;->i()V

    .line 2089964
    invoke-static {v1}, LX/ECY;->e(LX/ECT;)V

    move v2, v3

    .line 2089965
    goto :goto_1

    .line 2089966
    :cond_4
    goto :goto_1

    .line 2089967
    :cond_5
    iget-object v1, v0, LX/ECY;->t:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2089968
    invoke-static {v0}, LX/ECY;->l(LX/ECY;)V

    goto :goto_0

    .line 2089969
    :cond_6
    if-eqz v2, :cond_0

    .line 2089970
    invoke-static {v0}, LX/ECY;->k(LX/ECY;)V

    goto :goto_0
.end method
