.class public final Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2095209
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2095210
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2095207
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2095208
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2095178
    if-nez p1, :cond_0

    .line 2095179
    :goto_0
    return v0

    .line 2095180
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2095181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2095182
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2095183
    const v2, 0x33c06189

    const/4 v5, 0x0

    .line 2095184
    if-nez v1, :cond_1

    move v4, v5

    .line 2095185
    :goto_1
    move v1, v4

    .line 2095186
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2095187
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2095188
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2095189
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2095190
    const v2, 0x7ba12ea3

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 2095191
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2095192
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2095193
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2095194
    :sswitch_2
    const-class v1, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 2095195
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 2095196
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 2095197
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2095198
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2095199
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 2095200
    if-nez p1, :cond_2

    const/4 v4, 0x0

    .line 2095201
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2095202
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2095203
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v4, v5

    .line 2095204
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2095205
    :cond_2
    new-array v4, p1, [I

    goto :goto_2

    .line 2095206
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v4, v5}, LX/186;->a([IZ)I

    move-result v4

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x33c06189 -> :sswitch_1
        0x7ba12ea3 -> :sswitch_2
        0x7bb6c490 -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2095177
    new-instance v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2095173
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 2095174
    if-eqz v0, :cond_0

    .line 2095175
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2095176
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2095158
    sparse-switch p2, :sswitch_data_0

    .line 2095159
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2095160
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2095161
    const v1, 0x33c06189

    .line 2095162
    if-eqz v0, :cond_0

    .line 2095163
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2095164
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2095165
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2095166
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2095167
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2095168
    :cond_0
    :goto_1
    return-void

    .line 2095169
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 2095170
    const v1, 0x7ba12ea3

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    .line 2095171
    :sswitch_2
    const-class v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 2095172
    invoke-static {v0, p3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x33c06189 -> :sswitch_1
        0x7ba12ea3 -> :sswitch_2
        0x7bb6c490 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2095152
    if-eqz p1, :cond_0

    .line 2095153
    invoke-static {p0, p1, p2}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;

    move-result-object v1

    .line 2095154
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;

    .line 2095155
    if-eq v0, v1, :cond_0

    .line 2095156
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2095157
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2095151
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2095211
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2095212
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2095146
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2095147
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2095148
    :cond_0
    iput-object p1, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->a:LX/15i;

    .line 2095149
    iput p2, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->b:I

    .line 2095150
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2095145
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2095144
    new-instance v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2095141
    iget v0, p0, LX/1vt;->c:I

    .line 2095142
    move v0, v0

    .line 2095143
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2095138
    iget v0, p0, LX/1vt;->c:I

    .line 2095139
    move v0, v0

    .line 2095140
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2095135
    iget v0, p0, LX/1vt;->b:I

    .line 2095136
    move v0, v0

    .line 2095137
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095132
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2095133
    move-object v0, v0

    .line 2095134
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2095123
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2095124
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2095125
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2095126
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2095127
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2095128
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2095129
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2095130
    invoke-static {v3, v9, v2}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2095131
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2095120
    iget v0, p0, LX/1vt;->c:I

    .line 2095121
    move v0, v0

    .line 2095122
    return v0
.end method
