.class public final Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7553b0b5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2095428
    const-class v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2095429
    const-class v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2095432
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2095433
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095430
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->f:Ljava/lang/String;

    .line 2095431
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2095415
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2095416
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2095417
    invoke-direct {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2095418
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2095419
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2095420
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2095421
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2095422
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2095423
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2095424
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2095425
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2095426
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2095427
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095405
    invoke-direct {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2095406
    new-instance v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;

    invoke-direct {v0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;-><init>()V

    .line 2095407
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2095408
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2095409
    const v0, -0x6c5bf5b4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2095410
    const v0, 0x602a9273

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095411
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->e:Ljava/lang/String;

    .line 2095412
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095413
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->g:Ljava/lang/String;

    .line 2095414
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;->g:Ljava/lang/String;

    return-object v0
.end method
