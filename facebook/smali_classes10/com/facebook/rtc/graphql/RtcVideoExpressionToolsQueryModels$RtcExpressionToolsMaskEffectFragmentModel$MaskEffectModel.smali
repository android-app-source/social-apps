.class public final Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xefb282d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2095579
    const-class v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2095582
    const-class v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2095580
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2095581
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2095570
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2095571
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->j()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2095572
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2095573
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2095574
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2095575
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2095576
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2095577
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2095559
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2095560
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->j()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2095561
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->j()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    .line 2095562
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->j()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2095563
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;

    .line 2095564
    iput-object v0, v1, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->e:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    .line 2095565
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2095566
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095578
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2095567
    new-instance v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;

    invoke-direct {v0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;-><init>()V

    .line 2095568
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2095569
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2095558
    const v0, 0x6aed3061

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2095557
    const v0, -0x160d5843

    return v0
.end method

.method public final j()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095555
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->e:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    iput-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->e:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    .line 2095556
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->e:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095553
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->f:Ljava/lang/String;

    .line 2095554
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel;->f:Ljava/lang/String;

    return-object v0
.end method
