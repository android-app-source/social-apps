.class public final Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2095290
    const-class v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;

    new-instance v1, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2095291
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2095213
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2095214
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2095215
    const/4 v2, 0x0

    .line 2095216
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2095217
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2095218
    :goto_0
    move v1, v2

    .line 2095219
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2095220
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2095221
    new-instance v1, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;

    invoke-direct {v1}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;-><init>()V

    .line 2095222
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2095223
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2095224
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2095225
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2095226
    :cond_0
    return-object v1

    .line 2095227
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2095228
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2095229
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2095230
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2095231
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2095232
    const-string v4, "rtc_expression_tools"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2095233
    const/4 v3, 0x0

    .line 2095234
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2095235
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2095236
    :goto_2
    move v1, v3

    .line 2095237
    goto :goto_1

    .line 2095238
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2095239
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2095240
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2095241
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2095242
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 2095243
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2095244
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2095245
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2095246
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2095247
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2095248
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 2095249
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2095250
    const/4 v5, 0x0

    .line 2095251
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 2095252
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2095253
    :goto_5
    move v4, v5

    .line 2095254
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2095255
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2095256
    goto :goto_3

    .line 2095257
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2095258
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2095259
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 2095260
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2095261
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 2095262
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2095263
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2095264
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, p0, :cond_b

    if-eqz v6, :cond_b

    .line 2095265
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2095266
    const/4 v6, 0x0

    .line 2095267
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_12

    .line 2095268
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2095269
    :goto_7
    move v4, v6

    .line 2095270
    goto :goto_6

    .line 2095271
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 2095272
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 2095273
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6

    .line 2095274
    :cond_e
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2095275
    :cond_f
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_11

    .line 2095276
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2095277
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2095278
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_f

    if-eqz v7, :cond_f

    .line 2095279
    const-string p0, "rtc_expression_tool_mask_effects"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 2095280
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2095281
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, p0, :cond_10

    .line 2095282
    :goto_9
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, p0, :cond_10

    .line 2095283
    invoke-static {p1, v0}, LX/EFQ;->b(LX/15w;LX/186;)I

    move-result v7

    .line 2095284
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 2095285
    :cond_10
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 2095286
    goto :goto_8

    .line 2095287
    :cond_11
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2095288
    invoke-virtual {v0, v6, v4}, LX/186;->b(II)V

    .line 2095289
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_7

    :cond_12
    move v4, v6

    goto :goto_8
.end method
