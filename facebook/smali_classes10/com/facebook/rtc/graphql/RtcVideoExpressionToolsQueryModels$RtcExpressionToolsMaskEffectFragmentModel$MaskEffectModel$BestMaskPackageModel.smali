.class public final Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1d1a19f
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2095531
    const-class v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2095530
    const-class v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2095528
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2095529
    return-void
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095526
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->f:Ljava/lang/String;

    .line 2095527
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 2095514
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2095515
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2095516
    invoke-direct {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2095517
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2095518
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->l()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2095519
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2095520
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 2095521
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2095522
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2095523
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2095524
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2095525
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2095501
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2095502
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2095503
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2095504
    if-eqz v1, :cond_2

    .line 2095505
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    .line 2095506
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 2095507
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->l()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2095508
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->l()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    .line 2095509
    invoke-virtual {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->l()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2095510
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    .line 2095511
    iput-object v0, v1, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->h:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    .line 2095512
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2095513
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095532
    invoke-direct {p0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2095498
    new-instance v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;

    invoke-direct {v0}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;-><init>()V

    .line 2095499
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2095500
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2095497
    const v0, -0x7dd6cf7c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2095496
    const v0, 0x2fd4c3c3

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2095494
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$FaceRecognitionModelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->e:Ljava/util/List;

    .line 2095495
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095492
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->g:Ljava/lang/String;

    .line 2095493
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2095490
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->h:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    iput-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->h:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    .line 2095491
    iget-object v0, p0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel;->h:Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$RtcExpressionToolsMaskEffectFragmentModel$MaskEffectModel$BestMaskPackageModel$PackagedFileModel;

    return-object v0
.end method
