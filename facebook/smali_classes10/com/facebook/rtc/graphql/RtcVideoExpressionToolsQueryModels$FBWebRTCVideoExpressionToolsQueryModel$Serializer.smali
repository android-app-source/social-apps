.class public final Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2095292
    const-class v0, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;

    new-instance v1, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2095293
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2095294
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2095295
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2095296
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2095297
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2095298
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2095299
    if-eqz v2, :cond_5

    .line 2095300
    const-string v3, "rtc_expression_tools"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2095301
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2095302
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2095303
    if-eqz v3, :cond_4

    .line 2095304
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2095305
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2095306
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_3

    .line 2095307
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    .line 2095308
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2095309
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2095310
    if-eqz v0, :cond_2

    .line 2095311
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2095312
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2095313
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2095314
    if-eqz v2, :cond_1

    .line 2095315
    const-string p0, "rtc_expression_tool_mask_effects"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2095316
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2095317
    const/4 p0, 0x0

    :goto_1
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_0

    .line 2095318
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/EFQ;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2095319
    add-int/lit8 p0, p0, 0x1

    goto :goto_1

    .line 2095320
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2095321
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2095322
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2095323
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2095324
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2095325
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2095326
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2095327
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2095328
    check-cast p1, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel$Serializer;->a(Lcom/facebook/rtc/graphql/RtcVideoExpressionToolsQueryModels$FBWebRTCVideoExpressionToolsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
