.class public Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;
.super Lcom/facebook/rtc/fragments/WebrtcDialogFragment;
.source ""


# instance fields
.field public A:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EDx;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/3A0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/Random;
    .annotation runtime Lcom/facebook/common/random/InsecureRandom;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/resources/ui/FbTextView;

.field public s:I

.field public t:I

.field private u:LX/2EJ;

.field private v:Z

.field public w:Z

.field private x:Z

.field public y:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EG4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2094945
    invoke-direct {p0}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;-><init>()V

    .line 2094946
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->s:I

    .line 2094947
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2094948
    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->y:LX/0Ot;

    .line 2094949
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2094950
    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->A:LX/0Ot;

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 2094951
    const v0, 0x7f0d313e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tiles/ThreadTileView;

    .line 2094952
    const v1, 0x7f0d313f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 2094953
    const v2, 0x7f0d3141

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbButton;

    .line 2094954
    iget-object v3, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->A:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->j()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2094955
    iget-object v3, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->y:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EG4;

    iget-object v4, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->A:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    .line 2094956
    invoke-interface {v3}, LX/EG4;->a()LX/8Vc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2094957
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->aA()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094958
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2094959
    :goto_0
    const v0, 0x7f0d3140

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2094960
    const v2, 0x7f08080a

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->A:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EDx;

    invoke-virtual {v1}, LX/EDx;->aC()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v8

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094961
    return-void

    .line 2094962
    :cond_0
    iget-object v3, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->y:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EG4;

    iget-object v4, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->A:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EDx;

    .line 2094963
    iget-wide v9, v4, LX/EDx;->ak:J

    move-wide v6, v9

    .line 2094964
    iget-object v4, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->z:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v6, v7, v4, v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(JJ)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v3}, LX/EG4;->a()LX/8Vc;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/tiles/ThreadTileView;->setThreadTileViewData(LX/8Vc;)V

    .line 2094965
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    invoke-virtual {v0}, LX/EDx;->ay()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094966
    invoke-virtual {v2, v8}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2094967
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->A:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EDx;

    .line 2094968
    iget-boolean v1, v0, LX/EDx;->aB:Z

    move v0, v1

    .line 2094969
    if-eqz v0, :cond_1

    const v0, 0x7f080763

    :goto_1
    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2094970
    new-instance v0, LX/EFA;

    invoke-direct {v0, p0}, LX/EFA;-><init>(Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;)V

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2094971
    :cond_1
    const v0, 0x7f080760

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/2EJ;
    .locals 1

    .prologue
    .line 2094972
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->u:LX/2EJ;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2094973
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->x:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0315f0

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2094974
    const v0, 0x7f0d313c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2094975
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v4, v3, :cond_0

    .line 2094976
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    .line 2094977
    new-instance v5, LX/EFB;

    invoke-direct {v5, p0, v4, v0}, LX/EFB;-><init>(Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;ILandroid/view/ViewGroup;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2094978
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2094979
    :cond_0
    const v0, 0x7f0d313d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 2094980
    new-instance v0, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f080772

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/EF9;

    invoke-direct {v4, p0}, LX/EF9;-><init>(Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;)V

    invoke-virtual {v0, v3, v4}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v3, 0x7f080022

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/EF8;

    invoke-direct {v4, p0}, LX/EF8;-><init>(Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;)V

    invoke-virtual {v0, v3, v4}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    .line 2094981
    iget-boolean v3, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->x:Z

    if-eqz v3, :cond_2

    .line 2094982
    invoke-direct {p0, v1}, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->a(Landroid/view/View;)V

    move v3, v2

    move v4, v2

    move v5, v2

    .line 2094983
    invoke-virtual/range {v0 .. v5}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    .line 2094984
    :goto_2
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->u:LX/2EJ;

    .line 2094985
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->u:LX/2EJ;

    return-object v0

    .line 2094986
    :cond_1
    const v0, 0x7f0315ef

    goto :goto_0

    .line 2094987
    :cond_2
    const v2, 0x7f08074e

    invoke-virtual {v0, v2}, LX/0ju;->a(I)LX/0ju;

    .line 2094988
    invoke-virtual {v0, v1}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    goto :goto_2
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2094989
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->v:Z

    .line 2094990
    iget v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->s:I

    invoke-virtual {p0, v0, v1, v1}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2094991
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x21c7f8a7

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2094992
    invoke-super {p0, p1}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2094993
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2094994
    const-string v2, "is_conference"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->w:Z

    .line 2094995
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2094996
    const-string v2, "show_call_again"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->x:Z

    .line 2094997
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v3, p0

    check-cast v3, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;

    invoke-static {v1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v1}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v6

    check-cast v6, Ljava/util/Random;

    const/16 v7, 0x3261

    invoke-static {v1, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x15e8

    invoke-static {v1, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const/16 p1, 0x3257

    invoke-static {v1, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-static {v1}, LX/3A0;->a(LX/0QB;)LX/3A0;

    move-result-object v1

    check-cast v1, LX/3A0;

    iput-object v5, v3, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->m:LX/0ad;

    iput-object v6, v3, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->q:Ljava/util/Random;

    iput-object v7, v3, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->y:LX/0Ot;

    iput-object v8, v3, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->z:LX/0Or;

    iput-object p1, v3, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->A:LX/0Ot;

    iput-object v1, v3, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->B:LX/3A0;

    .line 2094998
    const/16 v1, 0x2b

    const v2, -0x3adc07fb

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2094999
    iget-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->v:Z

    if-eqz v0, :cond_0

    .line 2095000
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->v:Z

    .line 2095001
    :goto_0
    return-void

    .line 2095002
    :cond_0
    const/4 v2, 0x0

    .line 2095003
    iget-object v3, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->m:LX/0ad;

    sget-object v4, LX/0c0;->Cached:LX/0c0;

    sget-object v5, LX/0c1;->Off:LX/0c1;

    iget-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->w:Z

    if-eqz v0, :cond_2

    sget v0, LX/3Dx;->fv:I

    :goto_1
    invoke-interface {v3, v4, v5, v0, v2}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v3

    .line 2095004
    iget-object v4, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->m:LX/0ad;

    sget-object v5, LX/0c0;->Cached:LX/0c0;

    sget-object p1, LX/0c1;->Off:LX/0c1;

    iget-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->w:Z

    if-eqz v0, :cond_3

    sget v0, LX/3Dx;->ft:I

    :goto_2
    invoke-interface {v4, v5, p1, v0, v2}, LX/0ad;->a(LX/0c0;LX/0c1;II)I

    move-result v0

    .line 2095005
    iget v4, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->s:I

    if-lez v4, :cond_4

    iget v4, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->s:I

    if-gt v4, v3, :cond_4

    iget-object v3, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->q:Ljava/util/Random;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    if-ge v3, v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2095006
    if-eqz v0, :cond_1

    .line 2095007
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    iget v1, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->s:I

    invoke-interface {v0, v1}, LX/ECA;->b(I)V

    goto :goto_0

    .line 2095008
    :cond_1
    iget v0, p0, Lcom/facebook/rtc/fragments/WebrtcRatingDialogFragment;->s:I

    invoke-virtual {p0, v0, v1, v1}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2095009
    :cond_2
    sget v0, LX/3Dx;->fu:I

    goto :goto_1

    .line 2095010
    :cond_3
    sget v0, LX/3Dx;->fs:I

    goto :goto_2

    :cond_4
    move v0, v2

    .line 2095011
    goto :goto_3
.end method
