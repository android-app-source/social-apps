.class public abstract Lcom/facebook/rtc/fragments/WebrtcDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field private static final m:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public n:LX/ECA;

.field public o:Landroid/widget/Button;

.field public p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2S7;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2094883
    const-class v0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;

    sput-object v0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->m:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2094881
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2094882
    return-void
.end method


# virtual methods
.method public abstract a()LX/2EJ;
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2094870
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    if-eqz v0, :cond_0

    .line 2094871
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    invoke-interface {v0, p1, p2, p3}, LX/ECA;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2094872
    :goto_0
    return-void

    .line 2094873
    :cond_0
    if-lez p1, :cond_1

    move v1, v8

    .line 2094874
    :goto_1
    if-eqz p2, :cond_2

    move v0, v8

    :goto_2
    add-int/2addr v1, v0

    .line 2094875
    if-eqz p3, :cond_3

    move v0, v8

    :goto_3
    add-int/2addr v0, v1

    .line 2094876
    iget-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2S7;

    const-string v6, "survey_no_listener"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    move-wide v4, v2

    invoke-virtual/range {v1 .. v7}, LX/2S7;->logCallAction(JJLjava/lang/String;Ljava/lang/String;)V

    .line 2094877
    sget-object v1, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->m:Ljava/lang/Class;

    const-string v2, "Attempting to flush survey results but there is no listener set up. code: %d"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v9

    invoke-static {v1, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v1, v9

    .line 2094878
    goto :goto_1

    :cond_2
    move v0, v9

    .line 2094879
    goto :goto_2

    :cond_3
    move v0, v9

    .line 2094880
    goto :goto_3
.end method

.method public abstract b()V
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2094867
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->o:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 2094868
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->o:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2094869
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2094864
    invoke-virtual {p0}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->a()LX/2EJ;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->o:Landroid/widget/Button;

    .line 2094865
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->o:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2094866
    return-void
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2094858
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onAttach(Landroid/content/Context;)V

    .line 2094859
    instance-of v0, p1, LX/ECA;

    if-eqz v0, :cond_1

    .line 2094860
    check-cast p1, LX/ECA;

    iput-object p1, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    .line 2094861
    :cond_0
    :goto_0
    return-void

    .line 2094862
    :cond_1
    instance-of v0, p1, LX/EF7;

    if-eqz v0, :cond_0

    .line 2094863
    check-cast p1, LX/EF7;

    invoke-interface {p1}, LX/EF7;->a()LX/ECA;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x56a9fe1f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2094855
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2094856
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;

    const/16 v1, 0x1111

    invoke-static {p1, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->p:LX/0Ot;

    .line 2094857
    const/16 v1, 0x2b

    const v2, -0x2617e518

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
