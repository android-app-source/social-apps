.class public Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/EFH;

.field private n:Lcom/facebook/user/tiles/UserTileView;

.field private o:Lcom/facebook/user/tiles/UserTileView;

.field public p:J

.field public q:J

.field private r:Landroid/app/Activity;

.field private s:LX/2EJ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2095087
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 2095088
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->m:LX/EFH;

    .line 2095089
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2095090
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->r:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031245

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2095091
    const v0, 0x7f0d2af5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->o:Lcom/facebook/user/tiles/UserTileView;

    .line 2095092
    const v0, 0x7f0d2af6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->n:Lcom/facebook/user/tiles/UserTileView;

    .line 2095093
    iget-wide v2, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->p:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->o:Lcom/facebook/user/tiles/UserTileView;

    if-eqz v0, :cond_0

    .line 2095094
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->o:Lcom/facebook/user/tiles/UserTileView;

    iget-wide v2, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->p:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-static {v2}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2095095
    :cond_0
    iget-wide v2, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->q:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->n:Lcom/facebook/user/tiles/UserTileView;

    if-eqz v0, :cond_1

    .line 2095096
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->n:Lcom/facebook/user/tiles/UserTileView;

    iget-wide v2, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v2

    invoke-static {v2}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 2095097
    :cond_1
    new-instance v0, LX/31Y;

    iget-object v2, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->r:Landroid/app/Activity;

    invoke-direct {v0, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080798

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/EFG;

    invoke-direct {v2, p0}, LX/EFG;-><init>(Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080799

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/EFF;

    invoke-direct {v2, p0}, LX/EFF;-><init>(Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->s:LX/2EJ;

    .line 2095098
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->s:LX/2EJ;

    return-object v0
.end method

.method public final onAttach(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2095099
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onAttach(Landroid/content/Context;)V

    .line 2095100
    instance-of v0, p1, Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2095101
    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/facebook/rtc/fragments/WebrtcVideoChatHeadNuxFragment;->r:Landroid/app/Activity;

    .line 2095102
    :cond_0
    return-void
.end method
