.class public Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;
.super Lcom/facebook/rtc/fragments/WebrtcDialogFragment;
.source ""


# instance fields
.field public m:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:I

.field private r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:J

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2094905
    invoke-direct {p0}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object p0

    check-cast p0, LX/0SG;

    iput-object p0, p1, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->m:LX/0SG;

    return-void
.end method


# virtual methods
.method public final a()LX/2EJ;
    .locals 1

    .prologue
    .line 2094904
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 2094898
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    const-wide/32 v2, 0x2bf20

    invoke-interface {v0, v2, v3}, LX/ECA;->a(J)V

    .line 2094899
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    move-object v0, v0

    .line 2094900
    const v1, 0x7f0315f1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    .line 2094901
    new-instance v1, LX/EF6;

    invoke-direct {v1, p0}, LX/EF6;-><init>(Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2094902
    new-instance v1, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080774

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080772

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/EF5;

    invoke-direct {v3, p0, v0}, LX/EF5;-><init>(Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;Lcom/facebook/resources/ui/FbEditText;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080773

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/EF4;

    invoke-direct {v2, p0}, LX/EF4;-><init>(Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 2094903
    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2094895
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->u:Z

    .line 2094896
    iget v0, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->q:I

    iget-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->s:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2094897
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x12d88982

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2094884
    invoke-super {p0, p1}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2094885
    const-class v1, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2094886
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2094887
    const-string v2, "rating"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->q:I

    .line 2094888
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2094889
    const-string v2, "reason_key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->r:Ljava/lang/String;

    .line 2094890
    const/16 v1, 0x2b

    const v2, 0x43d6b3c3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 2094891
    iget-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->u:Z

    if-eqz v0, :cond_0

    .line 2094892
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->u:Z

    .line 2094893
    :goto_0
    return-void

    .line 2094894
    :cond_0
    iget v0, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->q:I

    iget-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/rtc/fragments/WebrtcCommentDialogFragment;->s:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
