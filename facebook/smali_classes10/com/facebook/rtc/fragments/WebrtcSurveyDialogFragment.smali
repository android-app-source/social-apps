.class public Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;
.super Lcom/facebook/rtc/fragments/WebrtcDialogFragment;
.source ""


# instance fields
.field public m:Ljava/util/Random;
    .annotation runtime Lcom/facebook/common/random/InsecureRandom;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:I

.field public r:Ljava/lang/String;

.field public s:I

.field private t:Z

.field private u:Z

.field private v:LX/2EJ;

.field public w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2095049
    invoke-direct {p0}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object p0

    check-cast p0, Ljava/util/Random;

    iput-object p0, p1, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->m:Ljava/util/Random;

    return-void
.end method


# virtual methods
.method public final a()LX/2EJ;
    .locals 1

    .prologue
    .line 2095048
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->v:LX/2EJ;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 2095050
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    const-wide/32 v2, 0x1d4c0

    invoke-interface {v0, v2, v3}, LX/ECA;->a(J)V

    .line 2095051
    iget-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->t:Z

    if-eqz v0, :cond_0

    .line 2095052
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    .line 2095053
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807b4

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "no_video"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095054
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807b5

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "blurry_video"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095055
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807b6

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "frozen_video"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095056
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807b7

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "choppy_video"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095057
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807b8

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "lip_sync"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095058
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807b9

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "shaky_video"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095059
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807c7

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "battery_life"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095060
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807c8

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "device_got_hot"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095061
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807c9

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "audio_quality"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095062
    :goto_0
    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 2095063
    iget-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->m:Ljava/util/Random;

    invoke-static {v0, v1}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    .line 2095064
    const v1, 0x7f0807b2

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2095065
    iget-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v2, 0x7f0807b2

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "other"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095066
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 2095067
    new-instance v1, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0807b3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080772

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/EFE;

    invoke-direct {v3, p0, v0}, LX/EFE;-><init>(Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;[Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080773

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/EFD;

    invoke-direct {v3, p0}, LX/EFD;-><init>(Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const/4 v2, -0x1

    new-instance v3, LX/EFC;

    invoke-direct {v3, p0}, LX/EFC;-><init>(Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;)V

    invoke-virtual {v1, v0, v2, v3}, LX/0ju;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->v:LX/2EJ;

    .line 2095068
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->v:LX/2EJ;

    return-object v0

    .line 2095069
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    .line 2095070
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807a9

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "silent_call"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095071
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807aa

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "audio_dropout"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095072
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807ab

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "call_gaps"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095073
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807ac

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "voice_distortion"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095074
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807ad

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "background_noise"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095075
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807ae

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "echo"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095076
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807af

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "low_volume"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095077
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->w:Ljava/util/Map;

    const v1, 0x7f0807b0

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "latency"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2095045
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->u:Z

    .line 2095046
    iget v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->q:I

    iget-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->r:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 2095047
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x39465096

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2095038
    invoke-super {p0, p1}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2095039
    const-class v1, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 2095040
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2095041
    const-string v2, "rating"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->q:I

    .line 2095042
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 2095043
    const-string v2, "use_video"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->t:Z

    .line 2095044
    const/16 v1, 0x2b

    const v2, -0x487eeef9

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 2095026
    iget-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->u:Z

    if-eqz v0, :cond_0

    .line 2095027
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->u:Z

    .line 2095028
    :goto_0
    return-void

    .line 2095029
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->r:Ljava/lang/String;

    .line 2095030
    const-string v1, "audio_quality"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 2095031
    if-eqz v0, :cond_1

    .line 2095032
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    iget v1, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->q:I

    invoke-interface {v0, v1}, LX/ECA;->d(I)V

    goto :goto_0

    .line 2095033
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->r:Ljava/lang/String;

    .line 2095034
    const-string v1, "other"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 2095035
    if-eqz v0, :cond_2

    .line 2095036
    iget-object v0, p0, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->n:LX/ECA;

    iget v1, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->q:I

    iget-object v2, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->r:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/ECA;->a(ILjava/lang/String;)V

    goto :goto_0

    .line 2095037
    :cond_2
    iget v0, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->q:I

    iget-object v1, p0, Lcom/facebook/rtc/fragments/WebrtcSurveyDialogFragment;->r:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/facebook/rtc/fragments/WebrtcDialogFragment;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
