.class public Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/EEq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/EDx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/EEp;

.field public f:Landroid/widget/ListView;

.field public g:LX/0hs;

.field public h:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field public i:Landroid/view/View$OnClickListener;

.field public j:I

.field public k:I

.field public l:I

.field public m:Z

.field public n:Z

.field public o:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2094705
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2094706
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2094707
    iput-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->d:LX/0Ot;

    .line 2094708
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->o:Z

    return-void
.end method

.method public static a(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2094709
    iget-boolean v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->n:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 2094710
    :cond_0
    :goto_0
    return-void

    .line 2094711
    :cond_1
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->l:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public static e(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;)V
    .locals 2

    .prologue
    .line 2094712
    iget-boolean v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    if-nez v0, :cond_1

    .line 2094713
    :cond_0
    :goto_0
    return-void

    .line 2094714
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    iget v1, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->j:I

    .line 2094715
    iput v1, v0, LX/EEp;->h:I

    .line 2094716
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    iget v1, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->k:I

    .line 2094717
    iput v1, v0, LX/EEp;->i:I

    .line 2094718
    goto :goto_0
.end method

.method public static k(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;)V
    .locals 2

    .prologue
    .line 2094719
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->h:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    .line 2094720
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->h:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 2094721
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2094722
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2094723
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;

    const-class v2, LX/EEq;

    invoke-interface {p1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/EEq;

    invoke-static {p1}, LX/EDx;->a(LX/0QB;)LX/EDx;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-static {p1}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const/16 v0, 0x1430

    invoke-static {p1, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->a:LX/EEq;

    iput-object v3, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->b:LX/EDx;

    iput-object v4, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->c:LX/0ad;

    iput-object p1, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->d:LX/0Ot;

    .line 2094724
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x5386edc    # 8.672E-36f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2094725
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 2094726
    const v0, 0x7f031239

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2094727
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->a:LX/EEq;

    iget-object v3, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->b:LX/EDx;

    invoke-virtual {v3}, LX/EDx;->bj()LX/0Px;

    move-result-object v3

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/EEq;->a(LX/0Px;LX/0gc;)LX/EEp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    .line 2094728
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    iget-boolean v3, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->o:Z

    invoke-virtual {v0, v3}, LX/EEp;->a(Z)V

    .line 2094729
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    .line 2094730
    iget-object v3, v0, LX/EEp;->s:LX/2CH;

    iget-object v4, v0, LX/EEp;->p:LX/3Mg;

    invoke-virtual {v3, v4}, LX/2CH;->a(LX/3Mg;)V

    .line 2094731
    invoke-static {v0}, LX/EEp;->d(LX/EEp;)V

    .line 2094732
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    iget-object v3, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, LX/EEp;->a(Landroid/view/View$OnClickListener;)V

    .line 2094733
    invoke-static {p0}, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;)V

    .line 2094734
    invoke-static {p0, v2}, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->a(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;Landroid/view/View;)V

    .line 2094735
    const v0, 0x7f0d2ac9

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->f:Landroid/widget/ListView;

    .line 2094736
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->f:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->e:LX/EEp;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2094737
    new-instance v0, LX/0hs;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e02a3

    invoke-direct {v0, v3, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->g:LX/0hs;

    .line 2094738
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->g:LX/0hs;

    const v3, 0x7f080729

    invoke-virtual {v0, v3}, LX/0hs;->b(I)V

    .line 2094739
    iget-object v0, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->g:LX/0hs;

    const/16 v3, 0x4e20

    .line 2094740
    iput v3, v0, LX/0hs;->t:I

    .line 2094741
    const/16 v0, 0x2b

    const v3, -0x18563ff0

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2b288ac1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2094742
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 2094743
    invoke-static {p0}, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->k(Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;)V

    .line 2094744
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/rtc/fragments/RtcGroupCallRosterFragment;->h:Ljava/util/concurrent/ScheduledFuture;

    .line 2094745
    const/16 v1, 0x2b

    const v2, 0x165447e9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
