.class public Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ED8;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Xl;

.field private c:LX/0Yb;

.field private d:Landroid/view/ViewStub;

.field private e:Landroid/widget/TextView;

.field public f:Z

.field private g:Landroid/graphics/Rect;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:LX/EF3;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2094756
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2094757
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->f:Z

    return-void
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2094820
    const-string v1, "CALL_STATUS_IS_INSTANT"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2094821
    const-string v2, "CALL_STATUS_EXTRA_IS_PSTN_UPSELL"

    invoke-virtual {p0, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 2094822
    if-nez v1, :cond_0

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2094823
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2094824
    if-nez v1, :cond_1

    .line 2094825
    :cond_0
    :goto_0
    return v0

    .line 2094826
    :cond_1
    iget-object v1, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->e:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 2094827
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2094828
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2094829
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 2094830
    :cond_0
    :goto_0
    return-void

    .line 2094831
    :cond_1
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->d:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2094832
    const v0, 0x7f0d3136

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->e:Landroid/widget/TextView;

    .line 2094833
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->e:Landroid/widget/TextView;

    new-instance v1, LX/EF2;

    invoke-direct {v1, p0}, LX/EF2;-><init>(Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2094834
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->g:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 2094835
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->g:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->a(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public static d(Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2094775
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ED8;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2094776
    iget-object v3, v0, LX/ED8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    .line 2094777
    iget v6, v3, LX/EDx;->am:I

    move v3, v6

    .line 2094778
    if-eqz v3, :cond_0

    iget-object v3, v0, LX/ED8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    .line 2094779
    iget v6, v3, LX/EDx;->am:I

    move v3, v6

    .line 2094780
    if-ne v3, v5, :cond_5

    :cond_0
    move v3, v4

    .line 2094781
    :goto_0
    move v0, v3

    .line 2094782
    if-eqz v0, :cond_3

    .line 2094783
    invoke-direct {p0}, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2094784
    :goto_1
    invoke-direct {p0}, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->c()V

    .line 2094785
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2094786
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2094787
    iget-object v2, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ED8;

    .line 2094788
    iget-object v3, v0, LX/ED8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->aR()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2094789
    iget-object v3, v0, LX/ED8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->aQ()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2094790
    iget-object v3, v0, LX/ED8;->a:Landroid/content/Context;

    const v4, 0x7f08072d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2094791
    :goto_2
    iget-object v4, v0, LX/ED8;->a:Landroid/content/Context;

    const v5, 0x7f080753

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 2094792
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2094793
    :goto_3
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->h:LX/EF3;

    if-eqz v0, :cond_1

    .line 2094794
    invoke-direct {p0}, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->b()Z

    .line 2094795
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 2094796
    goto :goto_1

    .line 2094797
    :cond_3
    invoke-direct {p0}, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2094798
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 2094799
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_3

    .line 2094800
    :cond_5
    iget-object v3, v0, LX/ED8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->aX()Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v4

    .line 2094801
    goto :goto_0

    .line 2094802
    :cond_6
    iget-object v3, v0, LX/ED8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->q()Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v4

    .line 2094803
    goto/16 :goto_0

    .line 2094804
    :cond_7
    iget-object v3, v0, LX/ED8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    .line 2094805
    iget-boolean v6, v3, LX/EDx;->bQ:Z

    move v3, v6

    .line 2094806
    if-eqz v3, :cond_8

    move v3, v4

    .line 2094807
    goto/16 :goto_0

    :cond_8
    move v3, v5

    .line 2094808
    goto/16 :goto_0

    .line 2094809
    :cond_9
    iget-object v3, v0, LX/ED8;->a:Landroid/content/Context;

    const v4, 0x7f080735

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 2094810
    :cond_a
    iget-object v3, v0, LX/ED8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->at()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2094811
    iget-object v3, v0, LX/ED8;->a:Landroid/content/Context;

    const v4, 0x7f080790

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 2094812
    :cond_b
    iget-object v3, v0, LX/ED8;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EDx;

    invoke-virtual {v3}, LX/EDx;->ac()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 2094813
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->e:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 2094814
    iput-object p1, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->g:Landroid/graphics/Rect;

    .line 2094815
    :goto_0
    return-void

    .line 2094816
    :cond_0
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2094817
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2094818
    iget-object v1, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2094819
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->g:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2094769
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2094770
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->kX_()LX/0QA;

    move-result-object v0

    .line 2094771
    const/16 v1, 0x3253

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->a:LX/0Ot;

    .line 2094772
    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v0

    check-cast v0, LX/0Xl;

    iput-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->b:LX/0Xl;

    .line 2094773
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.rtc.fbwebrtc.CALL_STATUS_AND_DURATION_UPDATE"

    new-instance v2, LX/EF1;

    invoke-direct {v2, p0}, LX/EF1;-><init>(Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->c:LX/0Yb;

    .line 2094774
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3fbc0ae1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2094766
    const v0, 0x7f0315e8

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->d:Landroid/view/ViewStub;

    .line 2094767
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->e:Landroid/widget/TextView;

    .line 2094768
    iget-object v0, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->d:Landroid/view/ViewStub;

    const/16 v2, 0x2b

    const v3, 0x570b4631

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xa8e5395

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2094761
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2094762
    iget-object v1, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->c:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    .line 2094763
    iget-boolean v1, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->f:Z

    if-eqz v1, :cond_0

    .line 2094764
    invoke-static {p0}, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->d(Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;)V

    .line 2094765
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x393525e2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x42a22e4f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2094758
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2094759
    iget-object v1, p0, Lcom/facebook/rtc/fragments/VoipCallStatusBarFragment;->c:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->c()V

    .line 2094760
    const/16 v1, 0x2b

    const v2, -0x4776ac38

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
