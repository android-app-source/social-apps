.class public final Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/rtc/voicemail/VoicemailHandler;

.field public final synthetic c:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic d:LX/2SC;


# direct methods
.method public constructor <init>(LX/2SC;Ljava/lang/String;Lcom/facebook/rtc/voicemail/VoicemailHandler;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 0

    .prologue
    .line 2089498
    iput-object p1, p0, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;->d:LX/2SC;

    iput-object p2, p0, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;->b:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    iput-object p4, p0, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2089499
    iget-object v0, p0, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;->d:LX/2SC;

    iget-object v1, p0, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;->b:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    iget-object v3, p0, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2089500
    if-nez v1, :cond_1

    .line 2089501
    :cond_0
    :goto_0
    return-void

    .line 2089502
    :cond_1
    invoke-virtual {v0, v1}, LX/2SC;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 2089503
    if-nez v4, :cond_2

    .line 2089504
    :try_start_0
    iget-object v5, v0, LX/2SC;->c:LX/1oy;

    invoke-interface {v5, v1}, LX/1oy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2089505
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2089506
    invoke-static {}, LX/15D;->newBuilder()LX/15E;

    move-result-object v5

    new-instance p0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v6}, Ljava/net/URL;->toURI()Ljava/net/URI;

    move-result-object v6

    invoke-direct {p0, v6}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 2089507
    iput-object p0, v5, LX/15E;->b:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 2089508
    move-object v5, v5

    .line 2089509
    const-string v6, "voicemail_download"

    .line 2089510
    iput-object v6, v5, LX/15E;->c:Ljava/lang/String;

    .line 2089511
    move-object v5, v5

    .line 2089512
    iput-object v3, v5, LX/15E;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 2089513
    move-object v5, v5

    .line 2089514
    new-instance v6, LX/ECE;

    invoke-direct {v6, v0, v1}, LX/ECE;-><init>(LX/2SC;Ljava/lang/String;)V

    .line 2089515
    iput-object v6, v5, LX/15E;->g:Lorg/apache/http/client/ResponseHandler;

    .line 2089516
    move-object v5, v5

    .line 2089517
    invoke-virtual {v5}, LX/15E;->a()LX/15D;

    move-result-object v5

    .line 2089518
    iget-object v6, v0, LX/2SC;->e:Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-virtual {v6, v5}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/15D;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/io/File;

    move-object v4, v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2089519
    :cond_2
    iput-object v1, v2, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    .line 2089520
    iput-object v4, v2, Lcom/facebook/rtc/voicemail/VoicemailHandler;->p:Ljava/io/File;

    .line 2089521
    goto :goto_0

    .line 2089522
    :catch_0
    if-eqz v4, :cond_0

    .line 2089523
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method
