.class public final Lcom/facebook/rtc/voicemail/VoicemailHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/rtc/voicemail/VoicemailHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/rtc/voicemail/VoicemailHandler;)V
    .locals 0

    .prologue
    .line 2102300
    iput-object p1, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler$1;->a:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 2102301
    iget-object v0, p0, Lcom/facebook/rtc/voicemail/VoicemailHandler$1;->a:Lcom/facebook/rtc/voicemail/VoicemailHandler;

    .line 2102302
    iget-object v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->j:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 2102303
    iget-object v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->i:LX/2SD;

    .line 2102304
    iget-object v4, v1, LX/2SD;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {}, LX/2SD;->g()LX/0Tn;

    move-result-object v5

    const-wide/16 v6, -0x1

    invoke-interface {v4, v5, v6, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v4

    .line 2102305
    iget-object v6, v1, LX/2SD;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide v6, 0x9a7ec800L

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    const/4 v4, 0x1

    :goto_0
    move v1, v4

    .line 2102306
    if-eqz v1, :cond_0

    .line 2102307
    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->i:LX/2SD;

    .line 2102308
    iget-object v3, v2, LX/2SD;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    invoke-static {}, LX/2SD;->f()LX/0Tn;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2102309
    :cond_0
    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->i:LX/2SD;

    invoke-virtual {v2}, LX/2SD;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    .line 2102310
    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 2102311
    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->i:LX/2SD;

    invoke-virtual {v2}, LX/2SD;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    .line 2102312
    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->i:LX/2SD;

    .line 2102313
    iget-object v4, v2, LX/2SD;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-static {}, LX/2SD;->g()LX/0Tn;

    move-result-object v5

    iget-object v6, v2, LX/2SD;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 2102314
    :cond_1
    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 2102315
    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->i:LX/2SD;

    iget-object v3, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    .line 2102316
    iget-object v4, v2, LX/2SD;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    invoke-static {}, LX/2SD;->f()LX/0Tn;

    move-result-object v5

    invoke-interface {v4, v5, v3}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 2102317
    :cond_2
    if-eqz v1, :cond_3

    .line 2102318
    iget-object v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->f:LX/2SC;

    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    .line 2102319
    if-nez v2, :cond_5

    .line 2102320
    :cond_3
    :goto_1
    iget-object v1, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->f:LX/2SC;

    iget-object v2, v0, Lcom/facebook/rtc/voicemail/VoicemailHandler;->o:Ljava/lang/String;

    sget-object v3, Lcom/facebook/rtc/voicemail/VoicemailHandler;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2102321
    if-nez v2, :cond_6

    .line 2102322
    :goto_2
    return-void

    :cond_4
    const/4 v4, 0x0

    goto :goto_0

    .line 2102323
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fb_voicemail_asset_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2102324
    new-instance v4, Ljava/io/File;

    iget-object v5, v1, LX/2SC;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2102325
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 2102326
    :cond_6
    iget-object v4, v1, LX/2SC;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2MA;

    invoke-interface {v4}, LX/2MA;->c()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, v1, LX/2SC;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0kb;

    invoke-virtual {v4}, LX/0kb;->d()Z

    move-result v4

    if-nez v4, :cond_9

    :cond_7
    const/4 v4, 0x1

    :goto_3
    move v4, v4

    .line 2102327
    if-eqz v4, :cond_8

    .line 2102328
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Unable to download file. Not connected."

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 2102329
    :cond_8
    iget-object v4, v1, LX/2SC;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;

    invoke-direct {v5, v1, v2, v0, v3}, Lcom/facebook/rtc/assetdownloader/AssetDownloader$1;-><init>(LX/2SC;Ljava/lang/String;Lcom/facebook/rtc/voicemail/VoicemailHandler;Lcom/facebook/common/callercontext/CallerContext;)V

    const v6, -0x15ff141a

    invoke-static {v4, v5, v6}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_2

    :cond_9
    const/4 v4, 0x0

    goto :goto_3
.end method
