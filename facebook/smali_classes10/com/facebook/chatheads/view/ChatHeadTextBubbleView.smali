.class public Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static final d:LX/0wT;


# instance fields
.field public a:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:LX/0wd;

.field private final f:LX/0wd;

.field public g:Z

.field private final h:Landroid/widget/FrameLayout;

.field private final i:LX/D9S;

.field private final j:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

.field private final k:Landroid/widget/FrameLayout;

.field private final l:LX/D9S;

.field private final m:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

.field public n:I

.field private final o:Ljava/lang/Runnable;

.field private p:LX/D9U;

.field private q:LX/D9Q;

.field private r:Landroid/view/GestureDetector;

.field public s:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/D9T;

.field private v:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1970229
    const-wide/high16 v0, 0x4044000000000000L    # 40.0

    const-wide/high16 v2, 0x401c000000000000L    # 7.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->d:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1970230
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1970231
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1970232
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1970233
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const-wide v4, 0x3f50624de0000000L    # 0.0010000000474974513

    .line 1970234
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1970235
    const/16 v0, 0x1388

    iput v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->n:I

    .line 1970236
    new-instance v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView$1;

    invoke-direct {v0, p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView$1;-><init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->o:Ljava/lang/Runnable;

    .line 1970237
    sget-object v0, LX/D9Q;->MESSENGER:LX/D9Q;

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->q:LX/D9Q;

    .line 1970238
    const-class v0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-static {v0, p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1970239
    const v0, 0x7f030c91

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1970240
    const v0, 0x7f0d1ee6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->h:Landroid/widget/FrameLayout;

    .line 1970241
    const v0, 0x7f0d1ee7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->j:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    .line 1970242
    const v0, 0x7f0d1ee8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->k:Landroid/widget/FrameLayout;

    .line 1970243
    const v0, 0x7f0d1ee9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->m:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    .line 1970244
    sget-object v0, LX/D9U;->LEFT:LX/D9U;

    invoke-virtual {p0, v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->setOrigin(LX/D9U;)V

    .line 1970245
    invoke-virtual {p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1970246
    new-instance v1, LX/D9S;

    const v2, 0x7f0211f3

    invoke-direct {v1, v0, v2}, LX/D9S;-><init>(Landroid/content/res/Resources;I)V

    iput-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->i:LX/D9S;

    .line 1970247
    new-instance v1, LX/D9S;

    const v2, 0x7f0211f2

    invoke-direct {v1, v0, v2}, LX/D9S;-><init>(Landroid/content/res/Resources;I)V

    iput-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->l:LX/D9S;

    .line 1970248
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->h:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->i:LX/D9S;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1970249
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->k:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->l:LX/D9S;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1970250
    new-instance v0, LX/D9N;

    invoke-direct {v0, p0}, LX/D9N;-><init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1970251
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, LX/D9R;

    invoke-direct {v1, p0}, LX/D9R;-><init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->r:Landroid/view/GestureDetector;

    .line 1970252
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->d:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1970253
    iput-wide v4, v0, LX/0wd;->l:D

    .line 1970254
    move-object v0, v0

    .line 1970255
    iput-wide v4, v0, LX/0wd;->k:D

    .line 1970256
    move-object v0, v0

    .line 1970257
    new-instance v1, LX/D9V;

    invoke-direct {v1, p0}, LX/D9V;-><init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e:LX/0wd;

    .line 1970258
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->d:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1970259
    iput-wide v4, v0, LX/0wd;->l:D

    .line 1970260
    move-object v0, v0

    .line 1970261
    iput-wide v4, v0, LX/0wd;->k:D

    .line 1970262
    move-object v0, v0

    .line 1970263
    new-instance v1, LX/D9P;

    invoke-direct {v1, p0}, LX/D9P;-><init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->f:LX/0wd;

    .line 1970264
    return-void
.end method

.method private a(D)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1970265
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e:LX/0wd;

    .line 1970266
    iget-wide v5, v1, LX/0wd;->i:D

    move-wide v2, v5

    .line 1970267
    cmpl-double v1, v2, p1

    if-nez v1, :cond_1

    .line 1970268
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->s:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_0

    .line 1970269
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->s:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1970270
    :goto_0
    return-object v0

    .line 1970271
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1970272
    :cond_1
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->s:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v1, :cond_2

    .line 1970273
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->s:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, LX/0SQ;->cancel(Z)Z

    .line 1970274
    :cond_2
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->s:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1970275
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e:LX/0wd;

    const-wide/16 v2, 0x0

    cmpl-double v2, p1, v2

    if-eqz v2, :cond_3

    const/4 v0, 0x1

    .line 1970276
    :cond_3
    iput-boolean v0, v1, LX/0wd;->c:Z

    .line 1970277
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e:LX/0wd;

    invoke-virtual {v0, p1, p2}, LX/0wd;->b(D)LX/0wd;

    .line 1970278
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->s:Lcom/google/common/util/concurrent/SettableFuture;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;D)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1970279
    invoke-direct {p0, p1, p2}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b(D)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/view/View;FF)V
    .locals 2

    .prologue
    .line 1970224
    const v0, -0x4247ae14    # -0.09f

    mul-float/2addr v0, p2

    add-float/2addr v0, p1

    .line 1970225
    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleX(F)V

    .line 1970226
    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleY(F)V

    .line 1970227
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1970228
    return-void
.end method

.method private static a(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;Landroid/os/Handler;LX/0wW;LX/03V;)V
    .locals 0

    .prologue
    .line 1970280
    iput-object p1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a:Landroid/os/Handler;

    iput-object p2, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b:LX/0wW;

    iput-object p3, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->c:LX/03V;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;

    invoke-static {v2}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-static {v2}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;Landroid/os/Handler;LX/0wW;LX/03V;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1970281
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b(D)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1970282
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->v:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 1970283
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->v:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1970284
    :cond_0
    return-void
.end method

.method private b(D)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1970285
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->f:LX/0wd;

    .line 1970286
    iget-wide v5, v0, LX/0wd;->i:D

    move-wide v0, v5

    .line 1970287
    cmpl-double v0, p1, v0

    if-nez v0, :cond_1

    .line 1970288
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->t:Lcom/google/common/util/concurrent/SettableFuture;

    if-eqz v0, :cond_0

    .line 1970289
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->t:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1970290
    :goto_0
    return-object v0

    .line 1970291
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1970292
    :cond_1
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->t:Lcom/google/common/util/concurrent/SettableFuture;

    .line 1970293
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->f:LX/0wd;

    const-wide/16 v2, 0x0

    cmpl-double v0, p1, v2

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1970294
    :goto_1
    iput-boolean v0, v1, LX/0wd;->c:Z

    .line 1970295
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->f:LX/0wd;

    invoke-virtual {v0, p1, p2}, LX/0wd;->b(D)LX/0wd;

    .line 1970296
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->t:Lcom/google/common/util/concurrent/SettableFuture;

    goto :goto_0

    .line 1970297
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static synthetic b(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;D)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1970298
    invoke-direct {p0, p1, p2}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(D)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 1970299
    invoke-virtual {p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e()V

    .line 1970300
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->o:Ljava/lang/Runnable;

    iget v2, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->n:I

    int-to-long v2, v2

    const v4, 0x64606866

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1970301
    return-void
.end method

.method public static g(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V
    .locals 2

    .prologue
    .line 1970168
    invoke-virtual {p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/D9O;

    invoke-direct {v1, p0}, LX/D9O;-><init>(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1970169
    return-void
.end method

.method public static getCurrentTextBubbleView(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;
    .locals 1

    .prologue
    .line 1970302
    invoke-static {p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->i(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->j:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->m:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    goto :goto_0
.end method

.method public static h(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)V
    .locals 4

    .prologue
    .line 1970163
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1970164
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->f:LX/0wd;

    invoke-virtual {v1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 1970165
    iget-object v2, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->h:Landroid/widget/FrameLayout;

    invoke-static {v2, v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(Landroid/view/View;FF)V

    .line 1970166
    iget-object v2, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->k:Landroid/widget/FrameLayout;

    invoke-static {v2, v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(Landroid/view/View;FF)V

    .line 1970167
    return-void
.end method

.method public static i(Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;)Z
    .locals 2

    .prologue
    .line 1970170
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->p:LX/D9U;

    sget-object v1, LX/D9U;->LEFT:LX/D9U;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1970171
    invoke-direct {p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->f()V

    .line 1970172
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-direct {p0, v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(D)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1970173
    invoke-virtual {p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e()V

    .line 1970174
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a(D)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1970175
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->o:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1970176
    return-void
.end method

.method public getOrigin()LX/D9U;
    .locals 1

    .prologue
    .line 1970177
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->p:LX/D9U;

    return-object v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x30047fa5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1970178
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomFrameLayout;->onSizeChanged(IIII)V

    .line 1970179
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->h:Landroid/widget/FrameLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setPivotX(F)V

    .line 1970180
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->h:Landroid/widget/FrameLayout;

    div-int/lit8 v2, p2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setPivotY(F)V

    .line 1970181
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->k:Landroid/widget/FrameLayout;

    int-to-float v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setPivotX(F)V

    .line 1970182
    iget-object v1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->k:Landroid/widget/FrameLayout;

    div-int/lit8 v2, p2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setPivotY(F)V

    .line 1970183
    const/16 v1, 0x2d

    const v2, 0x1c6f7b0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x2

    const/4 v1, 0x1

    const v2, 0x6ca13eb3

    invoke-static {v8, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1970184
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-eq v3, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 1970185
    :cond_0
    iget-boolean v3, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->g:Z

    .line 1970186
    iput-boolean v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->g:Z

    .line 1970187
    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->e:LX/0wd;

    .line 1970188
    iget-wide v9, v3, LX/0wd;->i:D

    move-wide v4, v9

    .line 1970189
    const-wide v6, 0x3fe3333333333333L    # 0.6

    cmpg-double v3, v4, v6

    if-gtz v3, :cond_1

    .line 1970190
    invoke-virtual {p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1970191
    const v0, -0x7560a25e

    invoke-static {v8, v8, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1970192
    :goto_0
    return v1

    .line 1970193
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1970194
    invoke-direct {p0}, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->f()V

    .line 1970195
    :cond_2
    iget-object v3, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->r:Landroid/view/GestureDetector;

    invoke-virtual {v3, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move v0, v1

    :cond_4
    const v1, -0x338a27

    invoke-static {v1, v2}, LX/02F;->a(II)V

    move v1, v0

    goto :goto_0
.end method

.method public setDisplayMode(LX/D9Q;)V
    .locals 6

    .prologue
    const v5, -0x65ee52

    const v4, -0x69f755

    const v3, -0x5ff7b01

    const v2, -0x5ff820e

    .line 1970196
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->q:LX/D9Q;

    if-ne v0, p1, :cond_0

    .line 1970197
    :goto_0
    return-void

    .line 1970198
    :cond_0
    iput-object p1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->q:LX/D9Q;

    .line 1970199
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->q:LX/D9Q;

    sget-object v1, LX/D9Q;->SMS:LX/D9Q;

    if-ne v0, v1, :cond_1

    .line 1970200
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->i:LX/D9S;

    invoke-virtual {v0, v5, v4}, LX/D9S;->a(II)V

    .line 1970201
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->l:LX/D9S;

    invoke-virtual {v0, v5, v4}, LX/D9S;->a(II)V

    goto :goto_0

    .line 1970202
    :cond_1
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->i:LX/D9S;

    invoke-virtual {v0, v3, v2}, LX/D9S;->a(II)V

    .line 1970203
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->l:LX/D9S;

    invoke-virtual {v0, v3, v2}, LX/D9S;->a(II)V

    goto :goto_0
.end method

.method public setMaxLines(I)V
    .locals 1

    .prologue
    .line 1970204
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->j:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->setMaxLines(I)V

    .line 1970205
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->m:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->setMaxLines(I)V

    .line 1970206
    return-void
.end method

.method public setMessage(Landroid/text/Spanned;)V
    .locals 1

    .prologue
    .line 1970207
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->j:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1970208
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->m:Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/ui/text/MultilineEllipsizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1970209
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1970210
    iput-object p1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->v:Landroid/view/View$OnClickListener;

    .line 1970211
    return-void
.end method

.method public setOnTextBubbleAutoHideListener(LX/D9T;)V
    .locals 0

    .prologue
    .line 1970212
    iput-object p1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->u:LX/D9T;

    .line 1970213
    return-void
.end method

.method public setOrigin(LX/D9U;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1970214
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->p:LX/D9U;

    if-eq v0, p1, :cond_0

    .line 1970215
    iput-object p1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->p:LX/D9U;

    .line 1970216
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->p:LX/D9U;

    sget-object v1, LX/D9U;->LEFT:LX/D9U;

    if-ne v0, v1, :cond_1

    .line 1970217
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1970218
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->k:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1970219
    :cond_0
    :goto_0
    return-void

    .line 1970220
    :cond_1
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1970221
    iget-object v0, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->k:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTextBubbleDisplayTimeout(I)V
    .locals 0

    .prologue
    .line 1970222
    iput p1, p0, Lcom/facebook/chatheads/view/ChatHeadTextBubbleView;->n:I

    .line 1970223
    return-void
.end method
