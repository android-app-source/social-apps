.class public Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/89S;
.implements LX/89c;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/media/MediaMetadataRetriever;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:Landroid/app/Activity;

.field private f:Landroid/net/Uri;

.field public final g:LX/D2D;

.field private h:LX/D23;

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQj;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1957570
    const-class v0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    const-string v1, "create_profile_video_android"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;LX/0Or;LX/0Or;LX/D2D;LX/D23;LX/0Ot;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Landroid/media/MediaMetadataRetriever;",
            ">;",
            "LX/D2D;",
            "LX/D23;",
            "LX/0Ot",
            "<",
            "LX/BQj;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1957560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1957561
    iput-object p1, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->e:Landroid/app/Activity;

    .line 1957562
    iput-object p2, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->d:Ljava/lang/String;

    .line 1957563
    iput-object p3, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->f:Landroid/net/Uri;

    .line 1957564
    iput-object p4, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->a:LX/0Or;

    .line 1957565
    iput-object p5, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->b:LX/0Or;

    .line 1957566
    iput-object p6, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->g:LX/D2D;

    .line 1957567
    iput-object p7, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->h:LX/D23;

    .line 1957568
    iput-object p8, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->i:LX/0Ot;

    .line 1957569
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/89R;)V
    .locals 0

    .prologue
    .line 1957559
    return-void
.end method

.method public final a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;I)V
    .locals 2

    .prologue
    .line 1957541
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->f:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 1957542
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1957543
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->f:Landroid/net/Uri;

    sget-object v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1957544
    new-instance v0, LX/D1s;

    invoke-direct {v0, p0}, LX/D1s;-><init>(Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;)V

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1957545
    :goto_0
    return-void

    .line 1957546
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1957556
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQj;

    invoke-virtual {v0}, LX/BQj;->k()V

    .line 1957557
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQj;

    invoke-virtual {v0}, LX/BQj;->b()V

    .line 1957558
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 1957555
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957554
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Landroid/net/Uri;LX/89R;)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    .line 1957552
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->h:LX/D23;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->e:Landroid/app/Activity;

    iget-object v3, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;->d:Ljava/lang/String;

    iget-object v0, p2, LX/89R;->a:LX/89W;

    invoke-static {v0}, LX/B61;->a(LX/89W;)I

    move-result v5

    const/4 v6, 0x1

    const-wide/16 v10, 0x0

    move-object v4, p1

    move-object v8, v7

    move-object v9, v7

    invoke-virtual/range {v1 .. v11}, LX/D23;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;IILX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V

    .line 1957553
    return-void
.end method

.method public final c()LX/89c;
    .locals 0

    .prologue
    .line 1957551
    return-object p0
.end method

.method public final d()LX/89f;
    .locals 1

    .prologue
    .line 1957550
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()LX/BFW;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957549
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()LX/B4P;
    .locals 1

    .prologue
    .line 1957548
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()LX/89e;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957547
    const/4 v0, 0x0

    return-object v0
.end method
