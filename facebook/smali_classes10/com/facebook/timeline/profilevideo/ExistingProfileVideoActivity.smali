.class public Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field private p:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/D23;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:LX/D2D;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/5vm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private t:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1957573
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1957598
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1957599
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->PROFILEPIC:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->k()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v1

    .line 1957600
    const-string v2, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1957601
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->p:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x2

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1957602
    return-void
.end method

.method private static a(Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;Lcom/facebook/content/SecureContextHelper;LX/D23;LX/D2D;LX/5vm;)V
    .locals 0

    .prologue
    .line 1957597
    iput-object p1, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->q:LX/D23;

    iput-object p3, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->r:LX/D2D;

    iput-object p4, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->s:LX/5vm;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v3}, LX/D23;->b(LX/0QB;)LX/D23;

    move-result-object v1

    check-cast v1, LX/D23;

    invoke-static {v3}, LX/D2D;->a(LX/0QB;)LX/D2D;

    move-result-object v2

    check-cast v2, LX/D2D;

    invoke-static {v3}, LX/5vm;->b(LX/0QB;)LX/5vm;

    move-result-object v3

    check-cast v3, LX/5vm;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->a(Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;Lcom/facebook/content/SecureContextHelper;LX/D23;LX/D2D;LX/5vm;)V

    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    .line 1957603
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1957604
    :cond_0
    const/4 v0, -0x2

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->setResult(I)V

    .line 1957605
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->finish()V

    .line 1957606
    :goto_0
    return-void

    .line 1957607
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->q:LX/D23;

    iget-object v3, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->t:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x4

    const/4 v6, 0x1

    const-wide/16 v10, 0x0

    move-object v2, p0

    move-object v8, v7

    move-object v9, v7

    invoke-virtual/range {v1 .. v11}, LX/D23;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;IILX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1957588
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1957589
    invoke-static {p0, p0}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1957590
    if-eqz p1, :cond_1

    const-string v0, "session_id_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1957591
    const-string v0, "session_id_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->t:Ljava/lang/String;

    .line 1957592
    :goto_0
    if-nez p1, :cond_0

    .line 1957593
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->a()V

    .line 1957594
    :cond_0
    return-void

    .line 1957595
    :cond_1
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->t:Ljava/lang/String;

    .line 1957596
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->r:LX/D2D;

    sget-object v1, LX/D2C;->OPEN_GALLERY:LX/D2C;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1957576
    packed-switch p1, :pswitch_data_0

    .line 1957577
    :goto_0
    return-void

    .line 1957578
    :pswitch_0
    if-eq p2, v3, :cond_0

    .line 1957579
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->setResult(I)V

    .line 1957580
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->finish()V

    .line 1957581
    :cond_0
    invoke-direct {p0, p3}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->c(Landroid/content/Intent;)V

    goto :goto_0

    .line 1957582
    :pswitch_1
    if-eq p2, v3, :cond_1

    .line 1957583
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->a()V

    goto :goto_0

    .line 1957584
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->r:LX/D2D;

    sget-object v1, LX/D2C;->USER_CREATION_FINISHED:LX/D2C;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    .line 1957585
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->p:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->s:LX/5vm;

    invoke-virtual {v1}, LX/5vm;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1957586
    invoke-virtual {p0, v3}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->setResult(I)V

    .line 1957587
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1957574
    const-string v0, "session_id_key"

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ExistingProfileVideoActivity;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957575
    return-void
.end method
