.class public Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;
.super Landroid/support/v7/widget/RecyclerView;
.source ""


# instance fields
.field private h:LX/1P1;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1959338
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 1959339
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->m()V

    .line 1959340
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1959330
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1959331
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->m()V

    .line 1959332
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1959327
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1959328
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->m()V

    .line 1959329
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1959333
    const/4 v0, 0x1

    .line 1959334
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 1959335
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->h:LX/1P1;

    .line 1959336
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->h:LX/1P1;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1959337
    return-void
.end method


# virtual methods
.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1959326
    const/4 v0, 0x1

    return v0
.end method

.method public final l()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1959323
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1959324
    :goto_0
    return-void

    .line 1959325
    :cond_0
    invoke-virtual {p0, v4}, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v0, v2

    double-to-int v0, v0

    invoke-virtual {p0, v4, v0}, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->scrollBy(II)V

    goto :goto_0
.end method
