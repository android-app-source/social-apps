.class public Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:Landroid/animation/AnimatorSet;

.field public b:Z

.field private c:Z

.field public final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1959195
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1959196
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1959130
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1959131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1959188
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1959189
    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->b:Z

    .line 1959190
    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->c:Z

    .line 1959191
    new-instance v0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator$1;

    invoke-direct {v0, p0}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator$1;-><init>(Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->d:Ljava/lang/Runnable;

    .line 1959192
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031270

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1959193
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->b()V

    .line 1959194
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1959184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->b:Z

    .line 1959185
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1959186
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 1959187
    :cond_0
    return-void
.end method

.method private b()V
    .locals 13

    .prologue
    const-wide/16 v8, 0xfa

    const/4 v6, 0x2

    .line 1959147
    const v0, 0x7f0d2b4d

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1959148
    const v1, 0x7f0d2b4e

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1959149
    const v2, 0x7f0d2b4f

    invoke-virtual {p0, v2}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1959150
    const-string v3, "scaleX"

    new-array v4, v6, [F

    fill-array-data v4, :array_0

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1959151
    invoke-virtual {v3, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1959152
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1959153
    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 1959154
    const-string v5, "scaleY"

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1959155
    const-string v5, "scaleX"

    new-array v6, v6, [F

    fill-array-data v6, :array_1

    invoke-static {v0, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1959156
    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1959157
    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1959158
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 1959159
    const-string v6, "scaleY"

    invoke-virtual {v5, v6}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 1959160
    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 1959161
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 1959162
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 1959163
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 1959164
    invoke-virtual {v6, v1}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1959165
    invoke-virtual {v7, v1}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1959166
    invoke-virtual {v8, v1}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1959167
    invoke-virtual {v9, v1}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1959168
    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1959169
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v10

    .line 1959170
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v11

    .line 1959171
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 1959172
    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1959173
    invoke-virtual {v10, v2}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1959174
    invoke-virtual {v11, v2}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1959175
    invoke-virtual {v12, v2}, Landroid/animation/ValueAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1959176
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a:Landroid/animation/AnimatorSet;

    .line 1959177
    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1959178
    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1959179
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1959180
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v11}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->after(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1959181
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a:Landroid/animation/AnimatorSet;

    new-instance v1, LX/D2i;

    invoke-direct {v1, p0}, LX/D2i;-><init>(Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1959182
    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3faa3d71    # 1.33f
    .end array-data

    .line 1959183
    :array_1
    .array-data 4
        0x3faa3d71    # 1.33f
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x3996ad03

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1959142
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1959143
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->c:Z

    .line 1959144
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1959145
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a()V

    .line 1959146
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x69d28c2a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, 0x39da2f2e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1959138
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 1959139
    iput-boolean v2, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->c:Z

    .line 1959140
    iput-boolean v2, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->b:Z

    .line 1959141
    const/16 v1, 0x2d

    const v2, -0x3b8d644f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1959132
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 1959133
    iget-boolean v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->c:Z

    if-eqz v0, :cond_0

    .line 1959134
    if-nez p2, :cond_1

    .line 1959135
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->a()V

    .line 1959136
    :cond_0
    :goto_0
    return-void

    .line 1959137
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/view/ScrubberProgressIndicator;->b:Z

    goto :goto_0
.end method
