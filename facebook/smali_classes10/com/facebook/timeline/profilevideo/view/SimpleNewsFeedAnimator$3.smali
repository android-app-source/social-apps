.class public final Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedAnimator$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/D2o;


# direct methods
.method public constructor <init>(LX/D2o;)V
    .locals 0

    .prologue
    .line 1959279
    iput-object p1, p0, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedAnimator$3;->a:LX/D2o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 1959280
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedAnimator$3;->a:LX/D2o;

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1959281
    iget-object v1, v0, LX/D2o;->f:Landroid/animation/Animator;

    if-eqz v1, :cond_0

    .line 1959282
    iget-object v1, v0, LX/D2o;->f:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    .line 1959283
    :cond_0
    iget-object v1, v0, LX/D2o;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1959284
    iget-object v1, v0, LX/D2o;->b:Landroid/view/View;

    const v2, 0x3e4ccccd    # 0.2f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 1959285
    iget-object v1, v0, LX/D2o;->e:LX/D2l;

    .line 1959286
    iget-object v2, v1, LX/D2l;->a:Landroid/widget/ImageView;

    move-object v1, v2

    .line 1959287
    if-nez v1, :cond_1

    .line 1959288
    :goto_0
    return-void

    .line 1959289
    :cond_1
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1959290
    iget-object v2, v0, LX/D2o;->g:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1959291
    iget-object v2, v0, LX/D2o;->c:Landroid/view/View;

    iget-object v4, v0, LX/D2o;->h:Landroid/graphics/Rect;

    invoke-virtual {v2, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1959292
    iget-object v2, v0, LX/D2o;->a:Landroid/view/View;

    iget-object v4, v0, LX/D2o;->i:Landroid/graphics/Rect;

    iget-object v5, v0, LX/D2o;->j:Landroid/graphics/Point;

    invoke-virtual {v2, v4, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    .line 1959293
    iget-object v2, v0, LX/D2o;->g:Landroid/graphics/Rect;

    iget-object v4, v0, LX/D2o;->j:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    neg-int v4, v4

    iget-object v5, v0, LX/D2o;->j:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    neg-int v5, v5

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 1959294
    iget-object v2, v0, LX/D2o;->h:Landroid/graphics/Rect;

    iget-object v4, v0, LX/D2o;->j:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    neg-int v4, v4

    iget-object v5, v0, LX/D2o;->j:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    neg-int v5, v5

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 1959295
    iget-object v2, v0, LX/D2o;->g:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v4, v0, LX/D2o;->h:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    iput v2, v0, LX/D2o;->k:F

    .line 1959296
    iget-object v1, v0, LX/D2o;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 1959297
    iget-object v1, v0, LX/D2o;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setPivotY(F)V

    .line 1959298
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1959299
    iget-object v2, v0, LX/D2o;->d:Landroid/widget/ImageView;

    sget-object v3, Landroid/view/View;->X:Landroid/util/Property;

    new-array v4, v9, [F

    iget-object v5, v0, LX/D2o;->g:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    aput v5, v4, v7

    iget-object v5, v0, LX/D2o;->h:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    aput v5, v4, v8

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    iget-object v3, v0, LX/D2o;->d:Landroid/widget/ImageView;

    sget-object v4, Landroid/view/View;->Y:Landroid/util/Property;

    new-array v5, v9, [F

    iget-object v6, v0, LX/D2o;->g:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    aput v6, v5, v7

    iget-object v6, v0, LX/D2o;->h:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    aput v6, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    iget-object v3, v0, LX/D2o;->d:Landroid/widget/ImageView;

    sget-object v4, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v5, v9, [F

    iget v6, v0, LX/D2o;->k:F

    aput v6, v5, v7

    aput v10, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    iget-object v3, v0, LX/D2o;->d:Landroid/widget/ImageView;

    sget-object v4, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v5, v9, [F

    iget v6, v0, LX/D2o;->k:F

    aput v6, v5, v7

    aput v10, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1959300
    const-wide/16 v3, 0x12c

    invoke-virtual {v1, v3, v4}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1959301
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1959302
    iget-object v2, v0, LX/D2o;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1959303
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 1959304
    iput-object v1, v0, LX/D2o;->f:Landroid/animation/Animator;

    .line 1959305
    iput-boolean v7, v0, LX/D2o;->n:Z

    goto/16 :goto_0
.end method
