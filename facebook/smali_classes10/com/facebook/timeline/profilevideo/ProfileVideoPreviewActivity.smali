.class public final Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/D22;


# instance fields
.field private A:LX/0h5;

.field public B:Ljava/lang/String;

.field private C:LX/BSt;

.field private D:I

.field private final E:Landroid/view/View$OnClickListener;

.field private final F:LX/ATX;

.field private p:LX/63Z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D2h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/D2D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:LX/BTG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:LX/63c;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/BQj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private x:LX/63b;

.field public y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

.field public z:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1957965
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1957966
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1957967
    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->q:LX/0Ot;

    .line 1957968
    new-instance v0, LX/D1z;

    invoke-direct {v0, p0}, LX/D1z;-><init>(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->E:Landroid/view/View$OnClickListener;

    .line 1957969
    new-instance v0, LX/D20;

    invoke-direct {v0, p0}, LX/D20;-><init>(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->F:LX/ATX;

    return-void
.end method

.method private static a(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;LX/63Z;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/D2D;LX/0ad;LX/BTG;LX/63c;LX/BQj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;",
            "LX/63Z;",
            "LX/0Ot",
            "<",
            "LX/D2h;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/D2D;",
            "LX/0ad;",
            "LX/BTG;",
            "LX/63c;",
            "LX/BQj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1957970
    iput-object p1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->p:LX/63Z;

    iput-object p2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->q:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->r:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->s:LX/D2D;

    iput-object p5, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->t:LX/0ad;

    iput-object p6, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->u:LX/BTG;

    iput-object p7, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->v:LX/63c;

    iput-object p8, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    invoke-static {v8}, LX/63Z;->a(LX/0QB;)LX/63Z;

    move-result-object v1

    check-cast v1, LX/63Z;

    const/16 v2, 0x36b8

    invoke-static {v8, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v8}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v8}, LX/D2D;->a(LX/0QB;)LX/D2D;

    move-result-object v4

    check-cast v4, LX/D2D;

    invoke-static {v8}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v8}, LX/BTG;->b(LX/0QB;)LX/BTG;

    move-result-object v6

    check-cast v6, LX/BTG;

    const-class v7, LX/63c;

    invoke-interface {v8, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/63c;

    invoke-static {v8}, LX/BQj;->a(LX/0QB;)LX/BQj;

    move-result-object v8

    check-cast v8, LX/BQj;

    invoke-static/range {v0 .. v8}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->a(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;LX/63Z;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/D2D;LX/0ad;LX/BTG;LX/63c;LX/BQj;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1957961
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1957962
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->A:LX/0h5;

    .line 1957963
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->A:LX/0h5;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->E:Landroid/view/View$OnClickListener;

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1957964
    return-void
.end method

.method public static b(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;Z)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    .line 1957929
    iget v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->D:I

    if-ne v0, v3, :cond_1

    .line 1957930
    :cond_0
    :goto_0
    return-void

    .line 1957931
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    .line 1957932
    const-string v1, "android_profile_video_thumbnail"

    invoke-static {v0, v1}, LX/BQj;->a(LX/BQj;Ljava/lang/String;)V

    .line 1957933
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->s:LX/D2D;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v2

    .line 1957934
    sget-object v4, LX/D2C;->SCRUBBER_OPENED:LX/D2C;

    invoke-virtual {v4}, LX/D2C;->getEventName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, LX/D2D;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1957935
    const-string v5, "video_duration"

    .line 1957936
    iget v6, v2, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->b:I

    move v6, v6

    .line 1957937
    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1957938
    const-string v5, "start_ms"

    invoke-virtual {v2}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->j()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1957939
    const-string v5, "end_ms"

    invoke-virtual {v2}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->k()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1957940
    invoke-static {v0, v4}, LX/D2D;->a(LX/D2D;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1957941
    iput v3, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->D:I

    .line 1957942
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1957943
    instance-of v1, v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    if-eqz v1, :cond_2

    .line 1957944
    check-cast v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->z:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    .line 1957945
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->z:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    if-nez v0, :cond_3

    .line 1957946
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    .line 1957947
    new-instance v1, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-direct {v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;-><init>()V

    .line 1957948
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1957949
    const-string v3, "session_id"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957950
    invoke-virtual {v1, v2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1957951
    move-object v0, v1

    .line 1957952
    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->z:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    .line 1957953
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->x:LX/63b;

    if-nez v0, :cond_4

    .line 1957954
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->v:LX/63c;

    const v1, 0x7f0813d1    # 1.808779E38f

    invoke-virtual {v0, v1}, LX/63c;->a(I)LX/63b;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->x:LX/63b;

    .line 1957955
    :cond_4
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->A:LX/0h5;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->x:LX/63b;

    invoke-virtual {v1}, LX/63b;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1957956
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->A:LX/0h5;

    const v1, 0x7f082a33

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 1957957
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->A:LX/0h5;

    new-instance v1, LX/D21;

    invoke-direct {v1, p0}, LX/D21;-><init>(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1957958
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->z:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1957959
    if-eqz p1, :cond_0

    .line 1957960
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->z:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b()V

    goto/16 :goto_0
.end method

.method private l()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 1957889
    iget v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->D:I

    if-ne v0, v3, :cond_0

    .line 1957890
    :goto_0
    return-void

    .line 1957891
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    .line 1957892
    const-string v1, "android_profile_video_preview"

    const-string v2, "addRemoveSound|trim"

    invoke-static {v0, v1, v2}, LX/BQj;->a(LX/BQj;Ljava/lang/String;Ljava/lang/String;)V

    .line 1957893
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->s:LX/D2D;

    sget-object v1, LX/D2C;->PREVIEW_OPENED:LX/D2C;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    .line 1957894
    iput v3, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->D:I

    .line 1957895
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->t:LX/0ad;

    sget v1, LX/0wf;->aN:I

    const/4 v2, 0x7

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 1957896
    new-instance v1, LX/5SM;

    invoke-direct {v1}, LX/5SM;-><init>()V

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    .line 1957897
    iput-object v2, v1, LX/5SM;->b:Ljava/lang/String;

    .line 1957898
    move-object v1, v1

    .line 1957899
    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957900
    iget-object v4, v2, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v2, v4

    .line 1957901
    iput-object v2, v1, LX/5SM;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1957902
    move-object v1, v1

    .line 1957903
    const/4 v2, 0x0

    .line 1957904
    iput-boolean v2, v1, LX/5SM;->e:Z

    .line 1957905
    move-object v1, v1

    .line 1957906
    iput-boolean v3, v1, LX/5SM;->h:Z

    .line 1957907
    move-object v1, v1

    .line 1957908
    iput-boolean v3, v1, LX/5SM;->d:Z

    .line 1957909
    move-object v1, v1

    .line 1957910
    const v2, 0x7f082a30

    .line 1957911
    iput v2, v1, LX/5SM;->m:I

    .line 1957912
    move-object v1, v1

    .line 1957913
    const v2, 0x7f082a32

    .line 1957914
    iput v2, v1, LX/5SM;->n:I

    .line 1957915
    move-object v1, v1

    .line 1957916
    mul-int/lit16 v0, v0, 0x3e8

    .line 1957917
    iput v0, v1, LX/5SM;->l:I

    .line 1957918
    move-object v0, v1

    .line 1957919
    iput-boolean v3, v0, LX/5SM;->i:Z

    .line 1957920
    move-object v0, v0

    .line 1957921
    const-string v1, "standard"

    .line 1957922
    iput-object v1, v0, LX/5SM;->q:Ljava/lang/String;

    .line 1957923
    move-object v0, v0

    .line 1957924
    invoke-virtual {v0}, LX/5SM;->a()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-result-object v1

    .line 1957925
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->u:LX/BTG;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v2}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "profile_video"

    iget-object v4, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    const-string v5, "standard"

    invoke-virtual {v0, v2, v3, v4, v5}, LX/BTG;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1957926
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->C:LX/BSt;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957927
    iget-object v3, v2, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    move-object v2, v3

    .line 1957928
    iget-object v3, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->F:LX/ATX;

    const-string v4, "profile_video"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/BSt;->a(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;Landroid/net/Uri;LX/ATX;Ljava/lang/String;LX/9fh;)V

    goto :goto_0
.end method

.method private m()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1957971
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->t:LX/0ad;

    sget-short v2, LX/0wf;->aM:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v1

    .line 1957972
    iget-object v2, v1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v1, v2

    .line 1957973
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v1

    .line 1957974
    iget-object v2, v1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v1, v2

    .line 1957975
    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getOverlayUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method public final a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;
    .locals 1

    .prologue
    .line 1957888
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    return-object v0
.end method

.method public final a(Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;)V
    .locals 0

    .prologue
    .line 1957886
    iput-object p1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957887
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v6, 0x2

    .line 1957851
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1957852
    invoke-static {p0, p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1957853
    if-eqz p1, :cond_2

    .line 1957854
    const-string v0, "profile_video_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957855
    const-string v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    .line 1957856
    :goto_0
    const v0, 0x7f03108b

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->setContentView(I)V

    .line 1957857
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->b()V

    .line 1957858
    new-instance v0, LX/BSt;

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-direct {v0, v1}, LX/BSt;-><init>(LX/0gc;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->C:LX/BSt;

    .line 1957859
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    .line 1957860
    iget-boolean v1, v0, LX/BQj;->d:Z

    move v0, v1

    .line 1957861
    if-nez v0, :cond_0

    .line 1957862
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    invoke-virtual {v0, v6}, LX/BQj;->a(I)V

    .line 1957863
    :cond_0
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->m()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_5

    const-string v0, "current_fragment_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v6, :cond_5

    .line 1957864
    :cond_1
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->b(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;Z)V

    .line 1957865
    :goto_1
    return-void

    .line 1957866
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "session_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    .line 1957867
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video_model"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957868
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    if-nez v0, :cond_3

    .line 1957869
    const-class v0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;

    const-string v1, "Video Model must be set"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1957870
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->finish()V

    goto :goto_1

    .line 1957871
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video_edit_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1957872
    if-eqz v0, :cond_4

    .line 1957873
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->o()LX/D2G;

    move-result-object v1

    .line 1957874
    iput-object v0, v1, LX/D2G;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1957875
    move-object v0, v1

    .line 1957876
    invoke-virtual {v0}, LX/D2G;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957877
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "video_caption"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1957878
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "video_expiration"

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1957879
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->o()LX/D2G;

    move-result-object v1

    .line 1957880
    iput-object v0, v1, LX/D2G;->f:Ljava/lang/String;

    .line 1957881
    move-object v0, v1

    .line 1957882
    iput-wide v2, v0, LX/D2G;->g:J

    .line 1957883
    move-object v0, v0

    .line 1957884
    invoke-virtual {v0}, LX/D2G;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    goto/16 :goto_0

    .line 1957885
    :cond_5
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->l()V

    goto :goto_1
.end method

.method public final onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1957832
    iget v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->D:I

    if-ne v0, v3, :cond_0

    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->m()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1957833
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->s:LX/D2D;

    sget-object v1, LX/D2C;->SCRUBBER_CLOSED:LX/D2C;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    .line 1957834
    invoke-direct {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->l()V

    .line 1957835
    :goto_0
    return-void

    .line 1957836
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957837
    iget v2, v1, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->d:I

    move v1, v2

    .line 1957838
    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957839
    iget v1, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->d:I

    move v0, v1

    .line 1957840
    if-ne v3, v0, :cond_2

    .line 1957841
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1957842
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->r:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity$3;-><init>(Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;Ljava/lang/String;)V

    const v0, -0xe4816a4

    invoke-static {v1, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1957843
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    .line 1957844
    iget v1, v0, LX/BQj;->e:I

    move v0, v1

    .line 1957845
    if-ne v0, v3, :cond_3

    .line 1957846
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->k()V

    .line 1957847
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->w:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->b()V

    .line 1957848
    :cond_3
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->s:LX/D2D;

    sget-object v1, LX/D2C;->PREVIEW_CLOSED:LX/D2C;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    .line 1957849
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->setResult(I)V

    .line 1957850
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->finish()V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1957828
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1957829
    const-string v0, "current_fragment_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1957830
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->C:LX/BSt;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->F:LX/ATX;

    invoke-virtual {v0, v1}, LX/BSt;->a(LX/ATX;)V

    .line 1957831
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x75462bed

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1957820
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1957821
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->setRequestedOrientation(I)V

    .line 1957822
    const/16 v1, 0x23

    const v2, 0x16408f73

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1957823
    const-string v0, "profile_video_model"

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->y:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1957824
    const-string v0, "current_fragment_id"

    iget v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->D:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1957825
    const-string v0, "session_id"

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoPreviewActivity;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957826
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1957827
    return-void
.end method
