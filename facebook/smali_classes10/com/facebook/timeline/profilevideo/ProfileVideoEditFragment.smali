.class public Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/D2B;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;

.field public d:LX/D2l;

.field public e:LX/D2o;

.field public f:Landroid/view/View;

.field public g:Landroid/widget/ImageView;

.field public h:Landroid/widget/LinearLayout;

.field private i:Landroid/view/View;

.field public j:Z

.field public k:Z

.field public l:I

.field public m:LX/D2j;

.field public n:Landroid/widget/SeekBar;

.field public o:F

.field public p:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private r:Ljava/lang/String;

.field public final s:LX/D1v;

.field public final t:LX/D1w;

.field private final u:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1957755
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1957756
    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->j:Z

    .line 1957757
    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->k:Z

    .line 1957758
    iput v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->l:I

    .line 1957759
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->q:Ljava/util/List;

    .line 1957760
    new-instance v0, LX/D1v;

    invoke-direct {v0, p0}, LX/D1v;-><init>(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->s:LX/D1v;

    .line 1957761
    new-instance v0, LX/D1w;

    invoke-direct {v0, p0}, LX/D1w;-><init>(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->t:LX/D1w;

    .line 1957762
    new-instance v0, LX/D1x;

    invoke-direct {v0, p0}, LX/D1x;-><init>(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->u:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 1957763
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-static {v2}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    new-instance v0, LX/D2B;

    new-instance p0, LX/D2Q;

    invoke-static {v2}, LX/D1u;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-direct {p0, v3, v4}, LX/D2Q;-><init>(LX/0TD;LX/0TD;)V

    move-object v3, p0

    check-cast v3, LX/D2Q;

    const-class v4, LX/BV0;

    invoke-interface {v2, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/BV0;

    invoke-static {v2}, LX/7SE;->b(LX/0QB;)LX/7SE;

    move-result-object p0

    check-cast p0, LX/7SE;

    invoke-direct {v0, v3, v4, p0}, LX/D2B;-><init>(LX/D2Q;LX/BV0;LX/7SE;)V

    move-object v2, v0

    check-cast v2, LX/D2B;

    iput-object v1, p1, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->a:LX/0hB;

    iput-object v2, p1, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b:LX/D2B;

    return-void
.end method

.method public static e(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1957765
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1957766
    instance-of v1, v0, LX/D22;

    if-nez v1, :cond_0

    .line 1957767
    const/4 v0, 0x0

    .line 1957768
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, LX/D22;

    invoke-interface {v0}, LX/D22;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic g(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)I
    .locals 2

    .prologue
    .line 1957764
    iget v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->l:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->l:I

    return v0
.end method

.method public static k(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)V
    .locals 2

    .prologue
    .line 1957743
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1957744
    iget-boolean v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->j:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->k:Z

    if-eqz v0, :cond_1

    .line 1957745
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1957746
    :cond_0
    :goto_0
    return-void

    .line 1957747
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->i:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1957748
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1957749
    const-class v0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    invoke-static {v0, p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1957750
    if-eqz p1, :cond_0

    .line 1957751
    const-string v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->r:Ljava/lang/String;

    .line 1957752
    :goto_0
    return-void

    .line 1957753
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1957754
    const-string v1, "session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->r:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 1957706
    invoke-static {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->e(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v0

    .line 1957707
    if-nez v0, :cond_0

    .line 1957708
    :goto_0
    return-void

    .line 1957709
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b:LX/D2B;

    .line 1957710
    iput-object v0, v1, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    .line 1957711
    invoke-static {v1}, LX/D2B;->b(LX/D2B;)V

    .line 1957712
    invoke-static {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->e(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 1957713
    iget v1, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    move v1, v1

    .line 1957714
    if-nez v1, :cond_4

    .line 1957715
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b:LX/D2B;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->s:LX/D1v;

    invoke-virtual {v1, v2, v3, v5}, LX/D2B;->a(ILX/D1v;F)V

    .line 1957716
    :goto_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1957717
    invoke-virtual {v0}, LX/1FJ;->close()V

    goto :goto_2

    .line 1957718
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1957719
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1957720
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1da1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 1957721
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    div-int/2addr v0, v1

    add-int/lit8 v2, v0, 0x1

    .line 1957722
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v2, :cond_2

    .line 1957723
    new-instance v3, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 1957724
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1957725
    invoke-virtual {v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1957726
    iget-object v4, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1957727
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1957728
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b:LX/D2B;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->t:LX/D1w;

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1957729
    iget-object v4, v0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1957730
    const/4 v4, 0x0

    :goto_4
    if-ge v4, v2, :cond_3

    .line 1957731
    iget-object v5, v0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {v5}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->j()I

    move-result v5

    .line 1957732
    iget-object p0, v0, LX/D2B;->e:Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->k()I

    move-result p0

    .line 1957733
    sub-int/2addr p0, v5

    mul-int/2addr p0, v4

    div-int/2addr p0, v2

    add-int/2addr v5, p0

    .line 1957734
    invoke-virtual {v0, v4, v1, v3, v5}, LX/D2B;->a(ILX/D1w;FI)V

    .line 1957735
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1957736
    :cond_3
    goto/16 :goto_0

    .line 1957737
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->j()I

    move-result v1

    .line 1957738
    invoke-virtual {v0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->k()I

    move-result v2

    .line 1957739
    iget v3, v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    move v3, v3

    .line 1957740
    sub-int v4, v3, v1

    mul-int/lit8 v4, v4, 0x64

    sub-int v1, v2, v1

    div-int v1, v4, v1

    .line 1957741
    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b:LX/D2B;

    iget-object v4, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->s:LX/D1v;

    invoke-virtual {v2, v3, v4, v5}, LX/D2B;->a(ILX/D1v;F)V

    .line 1957742
    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->n:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x59d35997

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1957705
    const v1, 0x7f03108d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x2960d772

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x427c2a81

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1957702
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1957703
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b:LX/D2B;

    invoke-virtual {v1}, LX/D2B;->a()V

    .line 1957704
    const/16 v1, 0x2b

    const v2, -0x46c82f41    # -1.7530004E-4f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x19da2543

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1957699
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1957700
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b()V

    .line 1957701
    const/16 v1, 0x2b

    const v2, 0x6da5e005

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1957696
    const-string v0, "session_id"

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957697
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1957698
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1957675
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->b:LX/D2B;

    .line 1957676
    iput-object p0, v0, LX/D2B;->f:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    .line 1957677
    invoke-static {v0}, LX/D2B;->b(LX/D2B;)V

    .line 1957678
    const v0, 0x7f0d2783

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->c:Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;

    .line 1957679
    new-instance v0, LX/D2l;

    invoke-direct {v0}, LX/D2l;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->d:LX/D2l;

    .line 1957680
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->c:Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->d:LX/D2l;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1957681
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->c:Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;

    new-instance v1, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment$4;

    invoke-direct {v1, p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment$4;-><init>(Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/profilevideo/view/SimpleNewsFeedView;->post(Ljava/lang/Runnable;)Z

    .line 1957682
    const v0, 0x7f0d2781

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->f:Landroid/view/View;

    .line 1957683
    const v0, 0x7f0d2784

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->g:Landroid/widget/ImageView;

    .line 1957684
    const v0, 0x7f0d2786

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->h:Landroid/widget/LinearLayout;

    .line 1957685
    const v0, 0x7f0d2785

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->i:Landroid/view/View;

    .line 1957686
    const v0, 0x7f0d2787

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->n:Landroid/widget/SeekBar;

    .line 1957687
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->n:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->u:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1957688
    new-instance v0, LX/D2j;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/D2j;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->m:LX/D2j;

    .line 1957689
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->n:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1957690
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->m:LX/D2j;

    .line 1957691
    iput v0, v1, LX/D2j;->j:I

    .line 1957692
    iput v0, v1, LX/D2j;->i:I

    .line 1957693
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->n:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->m:LX/D2j;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    .line 1957694
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1da5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;->o:F

    .line 1957695
    return-void
.end method
