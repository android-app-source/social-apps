.class public Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final p:[Ljava/lang/String;


# instance fields
.field public q:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/D23;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field private u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3N2;",
            ">;"
        }
    .end annotation
.end field

.field private w:LX/0i4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:Landroid/net/Uri;

.field public y:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1958160
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->p:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1958107
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1958108
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1958109
    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->s:LX/0Ot;

    .line 1958110
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1958111
    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->t:LX/0Ot;

    .line 1958112
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1958113
    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->u:LX/0Ot;

    .line 1958114
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1958115
    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->v:LX/0Ot;

    .line 1958116
    return-void
.end method

.method private static a(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Lcom/facebook/photos/editgallery/utils/FetchImageUtils;LX/D23;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0i4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;",
            "Lcom/facebook/photos/editgallery/utils/FetchImageUtils;",
            "LX/D23;",
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3N2;",
            ">;",
            "LX/0i4;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1958161
    iput-object p1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->q:Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    iput-object p2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->r:LX/D23;

    iput-object p3, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->s:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->t:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->u:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->v:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->w:LX/0i4;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    invoke-static {v7}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->b(LX/0QB;)Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    invoke-static {v7}, LX/D23;->b(LX/0QB;)LX/D23;

    move-result-object v2

    check-cast v2, LX/D23;

    const/16 v3, 0x15e7

    invoke-static {v7, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xc49

    invoke-static {v7, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x455

    invoke-static {v7, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xf73

    invoke-static {v7, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const-class v8, LX/0i4;

    invoke-interface {v7, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/0i4;

    invoke-static/range {v0 .. v7}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->a(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Lcom/facebook/photos/editgallery/utils/FetchImageUtils;LX/D23;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0i4;)V

    return-void
.end method

.method public static varargs a$redex0(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1958162
    const-class v0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1958163
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->finish()V

    .line 1958164
    return-void
.end method

.method private c(Landroid/content/Intent;)Lcom/facebook/share/model/ComposerAppAttribution;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1958150
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "facebook.intent.action.PROFILE_MEDIA_CREATE"

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    .line 1958151
    if-eqz v4, :cond_0

    const-string v0, "proxied_app_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1958152
    :goto_0
    if-eqz v4, :cond_1

    const-string v0, "proxied_app_package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1958153
    :goto_1
    if-eqz v4, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3N2;

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v5}, LX/3N2;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1958154
    :goto_2
    if-eqz v4, :cond_3

    new-instance v0, Lcom/facebook/share/model/ComposerAppAttribution;

    const-string v1, ""

    const-string v4, ""

    invoke-direct {v0, v3, v1, v2, v4}, Lcom/facebook/share/model/ComposerAppAttribution;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    return-object v0

    :cond_0
    move-object v3, v1

    .line 1958155
    goto :goto_0

    :cond_1
    move-object v2, v1

    .line 1958156
    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 1958157
    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 1958158
    goto :goto_3
.end method

.method private static d(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1958159
    const-string v0, "\u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440_\u043c\u0430\u0441\u043a\u0438"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static e(Landroid/content/Intent;)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1958147
    const-string v0, "profile_media_extras_bundle"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1958148
    if-nez v0, :cond_0

    move-wide v0, v2

    .line 1958149
    :goto_0
    return-wide v0

    :cond_0
    const-string v1, "profile_media_extras_bundle_key_default_expiration_time_in_secs_since_epoch"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1958145
    invoke-static {p0, p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1958146
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1958126
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1958127
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1958128
    if-eqz p1, :cond_0

    const-string v0, "uri_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "has_launched_preview_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1958129
    const-string v0, "uri_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->x:Landroid/net/Uri;

    .line 1958130
    const-string v0, "has_launched_preview_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->y:Z

    .line 1958131
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->x:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 1958132
    const-string v0, "Video Uri is NULL"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->a$redex0(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1958133
    :goto_1
    return-void

    .line 1958134
    :cond_0
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->x:Landroid/net/Uri;

    .line 1958135
    iput-boolean v4, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->y:Z

    goto :goto_0

    .line 1958136
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1958137
    const-string v2, "facebook.intent.action.PROFILE_MEDIA_CREATE"

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "proxied_app_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "proxied_app_package_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "profile_media_extras_bundle"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1958138
    :cond_2
    const-string v0, "Required extras from 3rd party not present"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->a$redex0(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1958139
    :cond_3
    invoke-direct {p0, v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->c(Landroid/content/Intent;)Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v3

    .line 1958140
    const-string v2, "facebook.intent.action.PROFILE_MEDIA_CREATE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez v3, :cond_4

    .line 1958141
    const-string v0, "Application attribution not set"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->a$redex0(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1958142
    :cond_4
    invoke-static {v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->d(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    .line 1958143
    invoke-static {v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->e(Landroid/content/Intent;)J

    move-result-wide v4

    .line 1958144
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->w:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v6

    sget-object v7, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->p:[Ljava/lang/String;

    new-instance v0, LX/D25;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/D25;-><init>(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V

    invoke-virtual {v6, v7, v0}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1958117
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1958118
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->finish()V

    .line 1958119
    :goto_0
    return-void

    .line 1958120
    :cond_0
    sget-object v0, LX/0ax;->bE:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->s:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1958121
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-interface {v0, p0, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1958122
    if-nez v1, :cond_1

    .line 1958123
    const-string v0, "Failed to obtain logged in user"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->a$redex0(Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1958124
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1958125
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->finish()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1958104
    const-string v0, "uri_key"

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->x:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1958105
    const-string v0, "has_launched_preview_key"

    iget-boolean v1, p0, Lcom/facebook/timeline/profilevideo/ProfileVideoShareActivity;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1958106
    return-void
.end method
