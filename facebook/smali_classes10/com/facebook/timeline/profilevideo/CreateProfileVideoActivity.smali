.class public Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field private static final q:[Ljava/lang/String;


# instance fields
.field private A:Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

.field private B:LX/0i5;

.field private C:Landroid/os/Handler;

.field private D:Z

.field public p:LX/D23;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:LX/D1t;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:LX/D2D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private x:LX/1b0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/BQj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private z:LX/2ua;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1957523
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->q:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1957520
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1957521
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->s:Z

    .line 1957522
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->D:Z

    return-void
.end method

.method private a(Landroid/net/Uri;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 1957519
    invoke-direct {p0, p1}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->b(Landroid/net/Uri;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;Landroid/net/Uri;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 1957518
    invoke-direct {p0, p1}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->a(Landroid/net/Uri;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Intent;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V
    .locals 12
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1957514
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 1957515
    if-nez v4, :cond_0

    .line 1957516
    :goto_0
    return-void

    .line 1957517
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->p:LX/D23;

    iget-object v3, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->r:Ljava/lang/String;

    const/4 v5, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v2, p0

    move-object v8, p2

    move-object v9, p3

    move-wide/from16 v10, p4

    invoke-virtual/range {v1 .. v11}, LX/D23;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;IILX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;LX/0ad;LX/D1t;LX/0i4;LX/D2D;LX/1b0;LX/BQj;LX/D23;LX/2ua;)V
    .locals 0

    .prologue
    .line 1957513
    iput-object p1, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->t:LX/0ad;

    iput-object p2, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->u:LX/D1t;

    iput-object p3, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->v:LX/0i4;

    iput-object p4, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->w:LX/D2D;

    iput-object p5, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->x:LX/1b0;

    iput-object p6, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->y:LX/BQj;

    iput-object p7, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->p:LX/D23;

    iput-object p8, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->z:LX/2ua;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    invoke-static {v8}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    const-class v2, LX/D1t;

    invoke-interface {v8, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/D1t;

    const-class v3, LX/0i4;

    invoke-interface {v8, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/0i4;

    invoke-static {v8}, LX/D2D;->a(LX/0QB;)LX/D2D;

    move-result-object v4

    check-cast v4, LX/D2D;

    invoke-static {v8}, LX/1az;->a(LX/0QB;)LX/1az;

    move-result-object v5

    check-cast v5, LX/1b0;

    invoke-static {v8}, LX/BQj;->a(LX/0QB;)LX/BQj;

    move-result-object v6

    check-cast v6, LX/BQj;

    invoke-static {v8}, LX/D23;->b(LX/0QB;)LX/D23;

    move-result-object v7

    check-cast v7, LX/D23;

    invoke-static {v8}, LX/2ua;->b(LX/0QB;)LX/2ua;

    move-result-object v8

    check-cast v8, LX/2ua;

    invoke-static/range {v0 .. v8}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->a(Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;LX/0ad;LX/D1t;LX/0i4;LX/D2D;LX/1b0;LX/BQj;LX/D23;LX/2ua;)V

    return-void
.end method

.method private b(Landroid/net/Uri;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 1957497
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->t:LX/0ad;

    sget v1, LX/0wf;->aN:I

    const/4 v2, 0x7

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 1957498
    new-instance v1, LX/89V;

    invoke-direct {v1}, LX/89V;-><init>()V

    const/4 v2, 0x1

    .line 1957499
    iput-boolean v2, v1, LX/89V;->b:Z

    .line 1957500
    move-object v1, v1

    .line 1957501
    mul-int/lit16 v0, v0, 0x3e8

    .line 1957502
    iput v0, v1, LX/89V;->j:I

    .line 1957503
    move-object v0, v1

    .line 1957504
    sget-object v1, LX/4gI;->VIDEO_ONLY:LX/4gI;

    .line 1957505
    iput-object v1, v0, LX/89V;->f:LX/4gI;

    .line 1957506
    move-object v0, v0

    .line 1957507
    invoke-virtual {v0}, LX/89V;->a()Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    move-result-object v0

    .line 1957508
    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->x:LX/1b0;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->r:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v0, v2, v3}, LX/1b0;->a(Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    move-result-object v0

    .line 1957509
    invoke-direct {p0, p1}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->c(Landroid/net/Uri;)Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    move-result-object v1

    .line 1957510
    iput-object v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    .line 1957511
    move-object v0, v0

    .line 1957512
    return-object v0
.end method

.method public static b(Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 1957524
    iget-boolean v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->D:Z

    if-eqz v0, :cond_0

    .line 1957525
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->s:Z

    if-eqz v0, :cond_1

    .line 1957526
    :cond_0
    :goto_0
    return-void

    .line 1957527
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1957528
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1957529
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->D:Z

    goto :goto_0

    .line 1957530
    :cond_2
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->C:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity$2;-><init>(Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;Landroid/support/v4/app/Fragment;)V

    const v2, -0x792d8965

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method private c(Landroid/net/Uri;)Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;
    .locals 11

    .prologue
    .line 1957489
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->A:Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    if-eqz v0, :cond_0

    .line 1957490
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->A:Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    .line 1957491
    :goto_0
    return-object v0

    .line 1957492
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->u:LX/D1t;

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->r:Ljava/lang/String;

    .line 1957493
    new-instance v2, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    const/16 v3, 0x455

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v3, 0x20

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/D2D;->a(LX/0QB;)LX/D2D;

    move-result-object v8

    check-cast v8, LX/D2D;

    invoke-static {v0}, LX/D23;->b(LX/0QB;)LX/D23;

    move-result-object v9

    check-cast v9, LX/D23;

    const/16 v3, 0x36b3

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    move-object v3, p0

    move-object v4, v1

    move-object v5, p1

    invoke-direct/range {v2 .. v10}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;-><init>(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;LX/0Or;LX/0Or;LX/D2D;LX/D23;LX/0Ot;)V

    .line 1957494
    move-object v0, v2

    .line 1957495
    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->A:Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    .line 1957496
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->A:Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1957476
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1957477
    invoke-static {p0, p0}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1957478
    if-eqz p1, :cond_0

    const-string v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1957479
    const-string v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->r:Ljava/lang/String;

    .line 1957480
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->z:LX/2ua;

    invoke-virtual {v0}, LX/2ua;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1957481
    const-class v0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;

    const-string v1, "Not in the QE to create profile videos"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1957482
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->finish()V

    .line 1957483
    :goto_1
    return-void

    .line 1957484
    :cond_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->r:Ljava/lang/String;

    .line 1957485
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->w:LX/D2D;

    sget-object v1, LX/D2C;->VIDEO_CAMERA_OPEN:LX/D2C;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    goto :goto_0

    .line 1957486
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->C:Landroid/os/Handler;

    .line 1957487
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->v:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->B:LX/0i5;

    .line 1957488
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->B:LX/0i5;

    sget-object v1, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->q:[Ljava/lang/String;

    new-instance v2, LX/D1r;

    invoke-direct {v2, p0}, LX/D1r;-><init>(Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 1957466
    packed-switch p1, :pswitch_data_0

    .line 1957467
    :goto_0
    return-void

    .line 1957468
    :pswitch_0
    if-eq p2, v3, :cond_0

    .line 1957469
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->y:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->e()V

    goto :goto_0

    .line 1957470
    :cond_0
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p3

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V

    goto :goto_0

    .line 1957471
    :pswitch_1
    if-eq p2, v3, :cond_1

    .line 1957472
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->y:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->e()V

    goto :goto_0

    .line 1957473
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->w:LX/D2D;

    sget-object v1, LX/D2C;->USER_CREATION_FINISHED:LX/D2C;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/D2D;->a(LX/D2C;Ljava/lang/String;)V

    .line 1957474
    invoke-virtual {p0, v3}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->setResult(I)V

    .line 1957475
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1957462
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->y:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->k()V

    .line 1957463
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->y:LX/BQj;

    invoke-virtual {v0}, LX/BQj;->b()V

    .line 1957464
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1957465
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x30db2394

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1957459
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1957460
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->s:Z

    .line 1957461
    const/16 v1, 0x23

    const v2, -0x49e0f3ab    # -2.3700024E-6f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x25548a3b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1957456
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1957457
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->setRequestedOrientation(I)V

    .line 1957458
    const/16 v1, 0x23

    const v2, -0x42f19d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1957453
    const-string v0, "session_id"

    iget-object v1, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1957454
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1957455
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x4d919af7    # 3.05356512E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1957447
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1957448
    instance-of v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    if-eqz v2, :cond_0

    .line 1957449
    check-cast v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v2, p0, Lcom/facebook/timeline/profilevideo/CreateProfileVideoActivity;->A:Lcom/facebook/timeline/profilevideo/CreateProfileVideoController;

    .line 1957450
    iput-object v2, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    .line 1957451
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onStart()V

    .line 1957452
    const/16 v0, 0x23

    const v2, -0x75924461

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
