.class public final Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

.field public final f:Ljava/lang/String;

.field public final g:J

.field public final h:Lcom/facebook/share/model/ComposerAppAttribution;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1958475
    new-instance v0, LX/D2F;

    invoke-direct {v0}, LX/D2F;-><init>()V

    sput-object v0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/D2G;)V
    .locals 2

    .prologue
    .line 1958460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958461
    iget-object v0, p1, LX/D2G;->a:Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    .line 1958462
    iget v0, p1, LX/D2G;->b:I

    iput v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->b:I

    .line 1958463
    iget v0, p1, LX/D2G;->d:I

    iput v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->d:I

    .line 1958464
    iget-object v0, p1, LX/D2G;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1958465
    iget-object v0, p1, LX/D2G;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->f:Ljava/lang/String;

    .line 1958466
    iget-wide v0, p1, LX/D2G;->g:J

    iput-wide v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->g:J

    .line 1958467
    iget-object v0, p1, LX/D2G;->h:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->h:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1958468
    iget v0, p1, LX/D2G;->c:I

    .line 1958469
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->j()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 1958470
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->j()I

    move-result v0

    .line 1958471
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->k()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 1958472
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->k()I

    move-result v0

    .line 1958473
    :cond_1
    iput v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    .line 1958474
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1958446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1958447
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    .line 1958448
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->b:I

    .line 1958449
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    .line 1958450
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->d:I

    .line 1958451
    const-class v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1958452
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->f:Ljava/lang/String;

    .line 1958453
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->g:J

    .line 1958454
    const-class v0, Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->h:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1958455
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->j()I

    move-result v0

    iget v3, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    if-gt v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1958456
    iget v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->k()I

    move-result v3

    if-gt v0, v3, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1958457
    return-void

    :cond_0
    move v0, v2

    .line 1958458
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1958459
    goto :goto_1
.end method

.method private static a(IIII)Lcom/facebook/photos/creativeediting/model/VideoTrimParams;
    .locals 2

    .prologue
    .line 1958440
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->newBuilder()LX/5jP;

    move-result-object v0

    .line 1958441
    sub-int v1, p1, p0

    if-le v1, p3, :cond_0

    .line 1958442
    add-int p1, p0, p3

    .line 1958443
    :cond_0
    if-nez p0, :cond_1

    if-eq p1, p2, :cond_2

    .line 1958444
    :cond_1
    invoke-virtual {v0, p0, p1}, LX/5jP;->a(II)LX/5jP;

    .line 1958445
    :cond_2
    invoke-virtual {v0}, LX/5jP;->a()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;IIILandroid/graphics/RectF;LX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;
    .locals 6
    .param p4    # Landroid/graphics/RectF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/5QV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1958421
    if-eqz p5, :cond_1

    invoke-interface {p5}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1958422
    invoke-interface {p5}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1958423
    invoke-interface {p5}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v0

    .line 1958424
    :goto_0
    invoke-static {}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->newBuilder()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v4

    invoke-static {v3, p1, p1, p2}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a(IIII)Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setVideoTrimParams(Lcom/facebook/photos/creativeediting/model/VideoTrimParams;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setOverlayUri(Ljava/lang/String;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setOverlayId(Ljava/lang/String;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setMsqrdMaskId(Ljava/lang/String;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v1

    if-ne v2, p3, :cond_0

    move v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setShouldFlipHorizontally(Z)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v0

    invoke-static {p4}, LX/63w;->a(Landroid/graphics/RectF;)Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setCropRect(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    .line 1958425
    new-instance v1, LX/D2G;

    invoke-direct {v1}, LX/D2G;-><init>()V

    .line 1958426
    iput-object p0, v1, LX/D2G;->a:Landroid/net/Uri;

    .line 1958427
    move-object v1, v1

    .line 1958428
    iput p1, v1, LX/D2G;->b:I

    .line 1958429
    move-object v1, v1

    .line 1958430
    iput v3, v1, LX/D2G;->c:I

    .line 1958431
    move-object v1, v1

    .line 1958432
    iput p3, v1, LX/D2G;->d:I

    .line 1958433
    move-object v1, v1

    .line 1958434
    iput-object v0, v1, LX/D2G;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1958435
    move-object v0, v1

    .line 1958436
    iput-object p7, v0, LX/D2G;->h:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1958437
    move-object v0, v0

    .line 1958438
    invoke-virtual {v0}, LX/D2G;->a()Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v3

    .line 1958439
    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1958420
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1958419
    const/4 v0, 0x0

    return v0
.end method

.method public final h()Landroid/graphics/RectF;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1958416
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1958417
    :cond_0
    const/4 v0, 0x0

    .line 1958418
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getCropRect()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-static {v0}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Landroid/graphics/RectF;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1958394
    invoke-virtual {p0}, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->h()Landroid/graphics/RectF;

    move-result-object v0

    const/high16 p0, 0x3f800000    # 1.0f

    .line 1958395
    if-nez v0, :cond_0

    .line 1958396
    const/4 v1, 0x0

    .line 1958397
    :goto_0
    move-object v0, v1

    .line 1958398
    return-object v0

    :cond_0
    new-instance v1, Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    const/4 v3, 0x0

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    sub-float v4, p0, v4

    add-float/2addr v3, v4

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget v5, v0, Landroid/graphics/RectF;->top:F

    sub-float v5, p0, v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 1958413
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    if-nez v0, :cond_1

    .line 1958414
    :cond_0
    const/4 v0, 0x0

    .line 1958415
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    iget v0, v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 1958410
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    iget-boolean v0, v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->isTrimSpecified:Z

    if-nez v0, :cond_1

    .line 1958411
    :cond_0
    iget v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->b:I

    .line 1958412
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    iget v0, v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    goto :goto_0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 1958409
    iget-wide v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->g:J

    return-wide v0
.end method

.method public final o()LX/D2G;
    .locals 1

    .prologue
    .line 1958408
    new-instance v0, LX/D2G;

    invoke-direct {v0, p0}, LX/D2G;-><init>(Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;)V

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1958399
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1958400
    iget v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1958401
    iget v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1958402
    iget v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1958403
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->e:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1958404
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1958405
    iget-wide v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1958406
    iget-object v0, p0, Lcom/facebook/timeline/profilevideo/model/ProfileVideoModel;->h:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1958407
    return-void
.end method
