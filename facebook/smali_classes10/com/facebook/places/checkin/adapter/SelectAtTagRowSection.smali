.class public Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;
.super LX/Dyu;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/view/LayoutInflater;

.field public final c:Landroid/content/Context;

.field private final d:LX/DzM;

.field public e:LX/9jN;

.field public f:Landroid/location/Location;

.field public g:Ljava/lang/String;

.field private h:Ljava/util/Locale;

.field private i:Z

.field public j:Z

.field public k:LX/1HI;

.field public final l:Ljava/util/concurrent/Executor;

.field public m:LX/9jG;

.field public n:Z

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2066325
    const-class v0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/content/Context;LX/DzM;Ljava/util/Locale;LX/1HI;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2066316
    invoke-direct {p0}, LX/Dyu;-><init>()V

    .line 2066317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->i:Z

    .line 2066318
    iput-object p2, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->c:Landroid/content/Context;

    .line 2066319
    iput-object p1, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->b:Landroid/view/LayoutInflater;

    .line 2066320
    iput-object p3, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->d:LX/DzM;

    .line 2066321
    iput-object p4, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->h:Ljava/util/Locale;

    .line 2066322
    iput-object p6, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->l:Ljava/util/concurrent/Executor;

    .line 2066323
    iput-object p5, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->k:LX/1HI;

    .line 2066324
    return-void
.end method

.method public static a(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2066291
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceType;->RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-ne v0, v1, :cond_1

    .line 2066292
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2066293
    :goto_0
    return-object v0

    .line 2066294
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 2066295
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 2066296
    invoke-static {p0}, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->e(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->n:Z

    if-nez v0, :cond_6

    move v0, v6

    .line 2066297
    :goto_1
    iget-object v1, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->f:Landroid/location/Location;

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v1

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 2066298
    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->d:LX/DzM;

    iget-object v1, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->f:Landroid/location/Location;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v4

    .line 2066299
    new-instance v9, Landroid/location/Location;

    const-string v10, ""

    invoke-direct {v9, v10}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2066300
    invoke-virtual {v9, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 2066301
    invoke-virtual {v9, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    .line 2066302
    iget-object v10, v0, LX/DzM;->a:LX/6aG;

    invoke-virtual {v10, v1, v9}, LX/6aG;->a(Landroid/location/Location;Landroid/location/Location;)Ljava/lang/String;

    move-result-object v9

    move-object v0, v9

    .line 2066303
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2066304
    :cond_2
    invoke-static {p0}, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->e(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->o:Z

    if-eqz v0, :cond_4

    :cond_3
    move v7, v6

    .line 2066305
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_5

    if-eqz v7, :cond_5

    .line 2066306
    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->e:LX/9jN;

    .line 2066307
    iget-object v1, v0, LX/9jN;->g:LX/9jM;

    move-object v0, v1

    .line 2066308
    sget-object v1, LX/9jM;->TRADITIONAL:LX/9jM;

    if-ne v0, v1, :cond_7

    .line 2066309
    invoke-static {p1}, LX/5m4;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Ljava/lang/String;

    move-result-object v0

    .line 2066310
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2066311
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2066312
    :cond_5
    :goto_2
    const-string v0, " \u00b7 "

    invoke-interface {v8}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    move v0, v7

    .line 2066313
    goto :goto_1

    .line 2066314
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2066315
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/fbui/widget/contentview/ContentView;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p2    # Lcom/facebook/fbui/widget/contentview/ContentView;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2066259
    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->h:Ljava/util/Locale;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->g:Ljava/lang/String;

    invoke-static {v0, v1, v2}, LX/DzM;->a(Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2066260
    iget-boolean v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->j:Z

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081770

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2066261
    iget-boolean v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->j:Z

    if-eqz v0, :cond_5

    const v0, 0x7f0e06db

    :goto_1
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 2066262
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->d()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->d()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_0
    const/4 v0, 0x0

    .line 2066263
    :goto_2
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2066264
    iget-object v0, p2, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->f:Landroid/view/View;

    move-object v0, v0

    .line 2066265
    iget-object v1, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021480

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2066266
    return-void

    .line 2066267
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubitleLayout()Landroid/text/Layout;

    move-result-object v0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2066268
    new-instance v1, Landroid/text/SpannableStringBuilder;

    const-string v2, ""

    invoke-direct {v1, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2066269
    iget-object v2, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->m:LX/9jG;

    invoke-static {p1, v2}, LX/DzN;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/9jG;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2066270
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bw_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v2

    .line 2066271
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2066272
    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->bx_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$TaggableActivityModel;

    move-result-object v3

    .line 2066273
    if-eqz v3, :cond_2

    .line 2066274
    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const-string v3, " \u00b7 "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2066275
    :cond_2
    invoke-static {p0, p1}, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->a(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Ljava/lang/String;

    move-result-object v2

    .line 2066276
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2066277
    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    const-string v3, " \u00b7 "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2066278
    :cond_3
    invoke-static {p0, p1}, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->b(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2066279
    if-eqz p3, :cond_4

    .line 2066280
    const-string v2, "  "

    invoke-virtual {v1, v6, v2}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2066281
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, p3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 2066282
    invoke-virtual {v0, v6}, Landroid/text/Layout;->getLineAscent(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v3, v4

    .line 2066283
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v4, v3, v4

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    .line 2066284
    float-to-int v4, v4

    float-to-int v3, v3

    invoke-virtual {v2, v6, v6, v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 2066285
    new-instance v3, Landroid/text/style/ImageSpan;

    invoke-direct {v3, v2, v7}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 2066286
    const/16 v2, 0x21

    invoke-virtual {v1, v3, v6, v7, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2066287
    :cond_4
    move-object v0, v1

    .line 2066288
    goto/16 :goto_0

    .line 2066289
    :cond_5
    const v0, 0x7f0e013e

    goto/16 :goto_1

    .line 2066290
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->d()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_2
.end method

.method public static b(LX/0QB;)Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;
    .locals 7

    .prologue
    .line 2066257
    new-instance v0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    invoke-static {p0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/DzM;->a(LX/0QB;)LX/DzM;

    move-result-object v3

    check-cast v3, LX/DzM;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v4

    check-cast v4, Ljava/util/Locale;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v5

    check-cast v5, LX/1HI;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-direct/range {v0 .. v6}, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;-><init>(Landroid/view/LayoutInflater;Landroid/content/Context;LX/DzM;Ljava/util/Locale;LX/1HI;Ljava/util/concurrent/Executor;)V

    .line 2066258
    return-object v0
.end method

.method public static b(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2066250
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;->RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-ne v0, v2, :cond_0

    .line 2066251
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 2066252
    :goto_0
    return-object v0

    .line 2066253
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->h:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    .line 2066254
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->l()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2066255
    :goto_1
    iget-object v3, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f00a7

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    int-to-long v6, v0

    invoke-virtual {v2, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2066256
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->l()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$PageVisitsModel;->a()I

    move-result v0

    goto :goto_1
.end method

.method private static e(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;)Z
    .locals 1

    .prologue
    .line 2066249
    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/9jL;
    .locals 1

    .prologue
    .line 2066326
    sget-object v0, LX/9jL;->SelectAtTagRow:LX/9jL;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/Object;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2066220
    if-nez p3, :cond_1

    .line 2066221
    if-nez p1, :cond_0

    .line 2066222
    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f0312eb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 2066223
    :cond_0
    const v0, 0x7f0d2c10

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2066224
    iget-object v1, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0816c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2066225
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2066226
    move-object p1, p1

    .line 2066227
    :goto_0
    return-object p1

    .line 2066228
    :cond_1
    check-cast p3, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2066229
    check-cast p1, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2066230
    if-nez p1, :cond_2

    .line 2066231
    new-instance p1, Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->c:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2066232
    sget-object v0, LX/6VF;->SMALL:LX/6VF;

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2066233
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2066234
    iget-object v1, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0e5f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 2066235
    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setPadding(IIII)V

    .line 2066236
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 2066237
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2066238
    const-string v0, "SELECT_AT_TAG_ROW"

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTag(Ljava/lang/Object;)V

    .line 2066239
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->m:LX/9jG;

    invoke-static {p3, v0}, LX/DzN;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/9jG;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2066240
    invoke-virtual {p3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bw_()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;

    move-result-object v0

    .line 2066241
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2066242
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$FlowableTaggableActivityModel$FlowIconModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2066243
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    .line 2066244
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 2066245
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 2066246
    iget-object v3, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->k:LX/1HI;

    sget-object v4, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0, v4}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v3

    .line 2066247
    new-instance v4, LX/Dyz;

    invoke-direct {v4, p0, v2, v1}, LX/Dyz;-><init>(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    iget-object p2, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->l:Ljava/util/concurrent/Executor;

    invoke-interface {v3, v4, p2}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 2066248
    :cond_3
    const/4 v0, 0x0

    invoke-static {p0, p3, p1, v0}, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->a$redex0(Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Lcom/facebook/fbui/widget/contentview/ContentView;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Ljava/util/ArrayList;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "LX/9jL;",
            "Ljava/lang/Object;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2066210
    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->e:LX/9jN;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->e:LX/9jN;

    .line 2066211
    iget-object v1, v0, LX/9jN;->b:Ljava/util/List;

    move-object v0, v1

    .line 2066212
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->e:LX/9jN;

    .line 2066213
    iget-object v1, v0, LX/9jN;->g:LX/9jM;

    move-object v0, v1

    .line 2066214
    sget-object v1, LX/9jM;->RECENT:LX/9jM;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2066215
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->i:Z

    if-nez v0, :cond_0

    .line 2066216
    iput-boolean v3, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->i:Z

    .line 2066217
    new-instance v0, Landroid/util/Pair;

    sget-object v1, LX/9jL;->SelectAtTagRowHeader:LX/9jL;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2066218
    :cond_0
    new-instance v0, Landroid/util/Pair;

    sget-object v1, LX/9jL;->SelectAtTagRow:LX/9jL;

    invoke-direct {v0, v1, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2066219
    return v3

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2066209
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/9jL;
    .locals 1

    .prologue
    .line 2066208
    sget-object v0, LX/9jL;->SelectAtTagRowHeader:LX/9jL;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2066207
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2066205
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->i:Z

    .line 2066206
    return-void
.end method
