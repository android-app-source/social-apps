.class public final Lcom/facebook/places/checkin/PlacePickerFragment$15$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/DyY;


# direct methods
.method public constructor <init>(LX/DyY;)V
    .locals 0

    .prologue
    .line 2064700
    iput-object p1, p0, Lcom/facebook/places/checkin/PlacePickerFragment$15$1;->a:LX/DyY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 2064701
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment$15$1;->a:LX/DyY;

    iget-object v0, v0, LX/DyY;->a:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2064702
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064703
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2064704
    invoke-static {v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->E(Lcom/facebook/places/checkin/PlacePickerFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->getPrimaryButtonSpec()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, LX/9jG;->isSocialSearchType()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2064705
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->A:LX/0wM;

    const v3, 0x7f020952

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2064706
    iput-object v2, v1, LX/108;->b:Landroid/graphics/drawable/Drawable;

    .line 2064707
    move-object v1, v1

    .line 2064708
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0816ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2064709
    iput-object v2, v1, LX/108;->j:Ljava/lang/String;

    .line 2064710
    move-object v1, v1

    .line 2064711
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 2064712
    iget-object v2, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v2, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2064713
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v2, LX/DyW;

    invoke-direct {v2, v0}, LX/DyW;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 2064714
    :cond_0
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->J:Lcom/facebook/places/checkin/ui/PlacesListContainer;

    new-instance v2, LX/Dya;

    invoke-direct {v2, v0}, LX/Dya;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->a(LX/1PH;)V

    .line 2064715
    iget-object v3, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064716
    iget-boolean v4, v3, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->i:Z

    move v3, v4

    .line 2064717
    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->D:LX/0ad;

    sget-short v4, LX/5HH;->a:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2064718
    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064719
    iget-boolean v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->u:Z

    move v1, v2

    .line 2064720
    if-eqz v1, :cond_2

    .line 2064721
    invoke-static {v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->G(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    .line 2064722
    :cond_2
    return-void

    .line 2064723
    :cond_3
    iget-object v3, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2064724
    iget-object v4, v3, LX/Dz6;->h:Landroid/location/Location;

    move-object v3, v4

    .line 2064725
    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2064726
    iget-boolean v4, v3, LX/Dz6;->f:Z

    move v3, v4

    .line 2064727
    if-nez v3, :cond_1

    .line 2064728
    iget-object v3, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2064729
    iget-object v4, v3, LX/Dz6;->h:Landroid/location/Location;

    move-object v4, v4

    .line 2064730
    iget-object v5, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->n:LX/9kE;

    iget-object v3, v0, Lcom/facebook/places/checkin/PlacePickerFragment;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9jd;

    .line 2064731
    const-string v6, "nearby_regions"

    invoke-static {v6}, LX/6Wo;->a(Ljava/lang/String;)LX/6We;

    move-result-object v6

    const/4 v7, 0x3

    new-array v7, v7, [LX/6Wc;

    const/4 v8, 0x0

    sget-object v9, LX/9jd;->a:LX/6Wc;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    sget-object v9, LX/9jd;->b:LX/6Wc;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    sget-object v9, LX/9jd;->c:LX/6Wc;

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, LX/6Wd;->a([LX/6Wc;)LX/6Wk;

    move-result-object v6

    sget-object v7, LX/6Wn;->GEO_REGION:LX/6Wn;

    invoke-virtual {v6, v7}, LX/6Wd;->a(LX/6Wn;)LX/6Wi;

    move-result-object v6

    sget-object v7, LX/9jd;->d:LX/6Wc;

    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, LX/6Wb;->a(D)LX/6Wa;

    move-result-object v7

    sget-object v8, LX/9jd;->e:LX/6Wc;

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, LX/6Wb;->a(D)LX/6Wa;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/6Wa;->a(LX/6WX;)LX/6WZ;

    move-result-object v7

    sget-object v8, LX/9jd;->b:LX/6Wc;

    const-string v9, "city"

    invoke-virtual {v8, v9}, LX/6Wb;->a(Ljava/lang/String;)LX/6Wa;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/6WZ;->a(LX/6WX;)LX/6WZ;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/6Wd;->a(LX/6WX;)LX/6Wl;

    move-result-object v6

    .line 2064732
    iget-object v7, v3, LX/9jd;->g:LX/0TD;

    new-instance v8, Lcom/facebook/places/checkin/protocol/FetchNearbyRegionsRunner$1;

    invoke-direct {v8, v3, v6}, Lcom/facebook/places/checkin/protocol/FetchNearbyRegionsRunner$1;-><init>(LX/9jd;LX/6Wd;)V

    invoke-interface {v7, v8}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v3, v6

    .line 2064733
    new-instance v4, LX/Dyi;

    invoke-direct {v4, v0}, LX/Dyi;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v5, v3, v4}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_0
.end method
