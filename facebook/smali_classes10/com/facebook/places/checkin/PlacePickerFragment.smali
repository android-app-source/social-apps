.class public Lcom/facebook/places/checkin/PlacePickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/6ZZ;
.implements LX/90X;


# static fields
.field public static final F:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:Lcom/facebook/common/perftest/PerfTestConfig;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

.field private H:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

.field public I:Ljava/lang/String;

.field public J:Lcom/facebook/places/checkin/ui/PlacesListContainer;

.field public K:Lcom/facebook/widget/listview/BetterListView;

.field public L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

.field private M:Landroid/view/View;

.field public N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public final O:Landroid/os/Handler;

.field public P:Z

.field public Q:Landroid/support/v4/app/DialogFragment;

.field public R:LX/DzG;

.field public S:LX/9jC;

.field public T:LX/Dys;

.field public U:LX/Dyr;

.field public V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public W:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;",
            ">;"
        }
    .end annotation
.end field

.field public final X:Ljava/lang/Runnable;

.field public final Y:LX/Dyh;

.field public a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/Dyy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Dz6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Dyt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0gd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9jd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/9j5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9jh;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/9kA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/9kE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bgf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/E0T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/9j7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/9ju;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/31f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/DzH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/DzL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/92o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/6Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2065504
    const-class v0, Lcom/facebook/places/checkin/PlacePickerFragment;

    sput-object v0, Lcom/facebook/places/checkin/PlacePickerFragment;->F:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2065505
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2065506
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    .line 2065507
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->O:Landroid/os/Handler;

    .line 2065508
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->P:Z

    .line 2065509
    sget-object v0, LX/Dys;->NONE:LX/Dys;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->T:LX/Dys;

    .line 2065510
    new-instance v0, Lcom/facebook/places/checkin/PlacePickerFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/places/checkin/PlacePickerFragment$1;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->X:Ljava/lang/Runnable;

    .line 2065511
    new-instance v0, LX/Dyh;

    invoke-direct {v0, p0}, LX/Dyh;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->Y:LX/Dyh;

    return-void
.end method

.method private D()V
    .locals 4

    .prologue
    .line 2065512
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->B:LX/0if;

    sget-object v1, LX/0ig;->ay:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2065513
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065514
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v1, v2

    .line 2065515
    iput-object v1, v0, LX/9j5;->d:Ljava/lang/String;

    .line 2065516
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    invoke-static {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->H(Lcom/facebook/places/checkin/PlacePickerFragment;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "place_picker_session_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2065517
    iput-object v1, v0, LX/9j5;->e:Ljava/lang/String;

    .line 2065518
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 2065519
    iput v1, v0, LX/9j5;->j:I

    .line 2065520
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2065521
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_started"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/9j5;->a(LX/9j5;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "device_orientation"

    iget p0, v0, LX/9j5;->j:I

    invoke-virtual {v2, v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065522
    return-void
.end method

.method public static E(Lcom/facebook/places/checkin/PlacePickerFragment;)Z
    .locals 1

    .prologue
    .line 2065523
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->v:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065524
    iget-boolean p0, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->j:Z

    move v0, p0

    .line 2065525
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static G(Lcom/facebook/places/checkin/PlacePickerFragment;)V
    .locals 3

    .prologue
    .line 2065526
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->requestFocus()Z

    .line 2065527
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2065528
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2065529
    return-void
.end method

.method private static H(Lcom/facebook/places/checkin/PlacePickerFragment;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 2065530
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private J()Z
    .locals 1

    .prologue
    .line 2065531
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->t:LX/9ju;

    .line 2065532
    iget-object p0, v0, LX/9ju;->k:Ljava/lang/Runnable;

    if-eqz p0, :cond_2

    const/4 p0, 0x1

    :goto_0
    move p0, p0

    .line 2065533
    if-nez p0, :cond_0

    iget-object p0, v0, LX/9ju;->f:LX/9kB;

    invoke-virtual {p0}, LX/1Ck;->a()Z

    move-result p0

    if-nez p0, :cond_0

    iget-object p0, v0, LX/9ju;->c:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    .line 2065534
    iget-object v0, p0, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->g:LX/9kB;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    move p0, v0

    .line 2065535
    if-eqz p0, :cond_1

    :cond_0
    const/4 p0, 0x1

    :goto_1
    move v0, p0

    .line 2065536
    return v0

    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static K(Lcom/facebook/places/checkin/PlacePickerFragment;)V
    .locals 4

    .prologue
    .line 2065537
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->t:LX/9ju;

    .line 2065538
    iget-object v1, v0, LX/9ju;->c:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    .line 2065539
    iget-object v0, v1, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->d:LX/9jn;

    .line 2065540
    iget-object v1, v0, LX/9jn;->a:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 2065541
    iget-object v1, v0, LX/9jn;->b:LX/2SA;

    invoke-virtual {v1}, LX/306;->clear()V

    .line 2065542
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2065543
    iget-boolean v1, v0, LX/Dz6;->f:Z

    if-eqz v1, :cond_1

    .line 2065544
    iget-object v1, v0, LX/Dz6;->h:Landroid/location/Location;

    invoke-static {v0, v1}, LX/Dz6;->d(LX/Dz6;Landroid/location/Location;)V

    .line 2065545
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    .line 2065546
    return-void

    .line 2065547
    :cond_1
    invoke-virtual {v0}, LX/Dz6;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2065548
    iget-object v1, v0, LX/Dz6;->i:LX/Dz4;

    .line 2065549
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/Dz4;->g:Z

    .line 2065550
    iget-boolean v1, v0, LX/Dz6;->g:Z

    if-nez v1, :cond_2

    .line 2065551
    iget-object v1, v0, LX/Dz6;->d:Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;

    iget-object v2, v0, LX/Dz6;->k:LX/0TF;

    .line 2065552
    sget-object v3, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->c:Lcom/facebook/location/FbLocationOperationParams;

    invoke-static {v1, v3, v2}, Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;->a(Lcom/facebook/places/checkin/protocol/CheckinSearchResultsLoader;Lcom/facebook/location/FbLocationOperationParams;LX/0TF;)V

    .line 2065553
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Dz6;->g:Z

    .line 2065554
    :cond_2
    goto :goto_0
.end method

.method public static a(LX/Dyq;LX/Dyp;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2065555
    sget-object v0, LX/Dyg;->c:[I

    invoke-virtual {p1}, LX/Dyp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2065556
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 2065557
    :pswitch_0
    sget-object v0, LX/Dyg;->b:[I

    invoke-virtual {p0}, LX/Dyq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2065558
    :pswitch_1
    sget-object v0, LX/Dyg;->b:[I

    invoke-virtual {p0}, LX/Dyq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 2065559
    :pswitch_2
    sget-object v0, LX/Dyg;->b:[I

    invoke-virtual {p0}, LX/Dyq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    .line 2065560
    :pswitch_3
    const-string v0, "android_place_picker_long_press"

    goto :goto_1

    .line 2065561
    :pswitch_4
    const-string v0, "android_place_picker_long_press_suggest_edits"

    goto :goto_1

    .line 2065562
    :pswitch_5
    const-string v0, "android_place_picker_edit_menu_suggest_edits"

    goto :goto_1

    .line 2065563
    :pswitch_6
    const-string v0, "android_place_picker_long_press_report_duplicates"

    goto :goto_1

    .line 2065564
    :pswitch_7
    const-string v0, "android_place_picker_edit_menu_report_duplicates"

    goto :goto_1

    .line 2065565
    :pswitch_8
    const-string v0, "android_place_picker_edit_menu"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
        :pswitch_8
    .end packed-switch
.end method

.method private a(LX/E0Q;)V
    .locals 6

    .prologue
    .line 2065566
    invoke-static {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->r(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    .line 2065567
    sget-object v0, LX/E0Q;->PLACE_PICKER:LX/E0Q;

    if-ne p1, v0, :cond_0

    .line 2065568
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->p:LX/E0T;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065569
    iget-object v2, v1, LX/Dyy;->e:LX/9jN;

    move-object v1, v2

    .line 2065570
    iget-object v2, v1, LX/9jN;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2065571
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065572
    iget-object v3, v2, LX/Dyy;->e:LX/9jN;

    move-object v2, v3

    .line 2065573
    iget-object v3, v2, LX/9jN;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2065574
    const-string v3, "home_creation_cell_tapped"

    const-string v4, "home_creation"

    invoke-static {v0, v3, v4}, LX/E0T;->c(LX/E0T;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2065575
    invoke-static {v0, v3, v1, v2}, LX/E0T;->a(LX/E0T;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2065576
    iget-object v4, v0, LX/E0T;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065577
    :cond_0
    new-instance v0, LX/E0R;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, LX/E0R;-><init>(Landroid/content/Context;)V

    .line 2065578
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->p:LX/E0T;

    .line 2065579
    iget-object v2, v1, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    move-object v1, v2

    .line 2065580
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2065581
    iget-object v3, v2, LX/Dz6;->h:Landroid/location/Location;

    move-object v2, v3

    .line 2065582
    sget-object v3, LX/E0Q;->PLACE_PICKER:LX/E0Q;

    if-eq p1, v3, :cond_1

    sget-object v3, LX/E0Q;->PLACE_CREATION:LX/E0Q;

    if-ne p1, v3, :cond_2

    :cond_1
    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2065583
    new-instance v3, Landroid/content/Intent;

    iget-object v4, v0, LX/E0R;->a:Landroid/content/Context;

    const-class v5, Lcom/facebook/places/create/home/HomeCreationActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2065584
    const-string v4, "map_location"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2065585
    const-string v4, "home_creation_logger_data"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2065586
    const-string v4, "home_activity_entry_flow"

    invoke-virtual {p1}, LX/E0Q;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2065587
    move-object v0, v3

    .line 2065588
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->e:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x7

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2065589
    return-void

    .line 2065590
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/places/checkin/PlacePickerFragment;LX/Dyy;LX/Dz6;LX/Dyt;Lcom/facebook/content/SecureContextHelper;LX/0gd;LX/03V;LX/0Ot;LX/9j5;LX/0Ot;LX/0kL;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/9kA;LX/9kE;LX/0Ot;LX/E0T;LX/01T;LX/9j7;LX/17Y;LX/9ju;LX/31f;Ljava/lang/Boolean;LX/DzH;LX/DzL;LX/92o;LX/6Zb;LX/0wM;LX/0if;Lcom/facebook/common/perftest/PerfTestConfig;LX/0ad;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/checkin/PlacePickerFragment;",
            "LX/Dyy;",
            "LX/Dz6;",
            "LX/Dyt;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0gd;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/9jd;",
            ">;",
            "LX/9j5;",
            "LX/0Ot",
            "<",
            "LX/9jh;",
            ">;",
            "LX/0kL;",
            "Lcom/facebook/common/callercontexttagger/AnalyticsTagger;",
            "LX/9kA;",
            "LX/9kE;",
            "LX/0Ot",
            "<",
            "LX/Bgf;",
            ">;",
            "LX/E0T;",
            "LX/01T;",
            "LX/9j7;",
            "LX/17Y;",
            "LX/9ju;",
            "LX/31f;",
            "Ljava/lang/Boolean;",
            "LX/DzH;",
            "LX/DzL;",
            "LX/92o;",
            "LX/6Zb;",
            "LX/0wM;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2065591
    iput-object p1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    iput-object p2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    iput-object p3, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    iput-object p4, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->e:Lcom/facebook/content/SecureContextHelper;

    iput-object p5, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->f:LX/0gd;

    iput-object p6, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->g:LX/03V;

    iput-object p7, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->h:LX/0Ot;

    iput-object p8, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iput-object p9, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->j:LX/0Ot;

    iput-object p10, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->k:LX/0kL;

    iput-object p11, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->l:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    iput-object p12, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->m:LX/9kA;

    iput-object p13, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->n:LX/9kE;

    iput-object p14, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->o:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->p:LX/E0T;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->q:LX/01T;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->r:LX/9j7;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->s:LX/17Y;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->t:LX/9ju;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->u:LX/31f;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->v:Ljava/lang/Boolean;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->w:LX/DzH;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->x:LX/DzL;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->y:LX/92o;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->z:LX/6Zb;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->A:LX/0wM;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->B:LX/0if;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->C:Lcom/facebook/common/perftest/PerfTestConfig;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->D:LX/0ad;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->E:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 34

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v32

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-static/range {v32 .. v32}, LX/Dyy;->a(LX/0QB;)LX/Dyy;

    move-result-object v3

    check-cast v3, LX/Dyy;

    invoke-static/range {v32 .. v32}, LX/Dz6;->a(LX/0QB;)LX/Dz6;

    move-result-object v4

    check-cast v4, LX/Dz6;

    invoke-static/range {v32 .. v32}, LX/Dyt;->a(LX/0QB;)LX/Dyt;

    move-result-object v5

    check-cast v5, LX/Dyt;

    invoke-static/range {v32 .. v32}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v32 .. v32}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v7

    check-cast v7, LX/0gd;

    invoke-static/range {v32 .. v32}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    const/16 v9, 0x2f2f

    move-object/from16 v0, v32

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {v32 .. v32}, LX/9j5;->a(LX/0QB;)LX/9j5;

    move-result-object v10

    check-cast v10, LX/9j5;

    const/16 v11, 0x2f31

    move-object/from16 v0, v32

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {v32 .. v32}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-static/range {v32 .. v32}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v13

    check-cast v13, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static/range {v32 .. v32}, LX/9kA;->a(LX/0QB;)LX/9kA;

    move-result-object v14

    check-cast v14, LX/9kA;

    invoke-static/range {v32 .. v32}, LX/9kE;->a(LX/0QB;)LX/9kE;

    move-result-object v15

    check-cast v15, LX/9kE;

    const/16 v16, 0x1a59

    move-object/from16 v0, v32

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v16

    invoke-static/range {v32 .. v32}, LX/E0T;->a(LX/0QB;)LX/E0T;

    move-result-object v17

    check-cast v17, LX/E0T;

    invoke-static/range {v32 .. v32}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v18

    check-cast v18, LX/01T;

    invoke-static/range {v32 .. v32}, LX/9j7;->a(LX/0QB;)LX/9j7;

    move-result-object v19

    check-cast v19, LX/9j7;

    invoke-static/range {v32 .. v32}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v20

    check-cast v20, LX/17Y;

    invoke-static/range {v32 .. v32}, LX/9ju;->a(LX/0QB;)LX/9ju;

    move-result-object v21

    check-cast v21, LX/9ju;

    invoke-static/range {v32 .. v32}, LX/31f;->a(LX/0QB;)LX/31f;

    move-result-object v22

    check-cast v22, LX/31f;

    invoke-static/range {v32 .. v32}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v23

    check-cast v23, Ljava/lang/Boolean;

    const-class v24, LX/DzH;

    move-object/from16 v0, v32

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/DzH;

    const-class v25, LX/DzL;

    move-object/from16 v0, v32

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v25

    check-cast v25, LX/DzL;

    invoke-static/range {v32 .. v32}, LX/92o;->a(LX/0QB;)LX/92o;

    move-result-object v26

    check-cast v26, LX/92o;

    invoke-static/range {v32 .. v32}, LX/6Zb;->a(LX/0QB;)LX/6Zb;

    move-result-object v27

    check-cast v27, LX/6Zb;

    invoke-static/range {v32 .. v32}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v28

    check-cast v28, LX/0wM;

    invoke-static/range {v32 .. v32}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v29

    check-cast v29, LX/0if;

    invoke-static/range {v32 .. v32}, Lcom/facebook/common/perftest/PerfTestConfig;->a(LX/0QB;)Lcom/facebook/common/perftest/PerfTestConfig;

    move-result-object v30

    check-cast v30, Lcom/facebook/common/perftest/PerfTestConfig;

    invoke-static/range {v32 .. v32}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v31

    check-cast v31, LX/0ad;

    const/16 v33, 0x1399

    invoke-static/range {v32 .. v33}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v32

    invoke-static/range {v2 .. v32}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(Lcom/facebook/places/checkin/PlacePickerFragment;LX/Dyy;LX/Dz6;LX/Dyt;Lcom/facebook/content/SecureContextHelper;LX/0gd;LX/03V;LX/0Ot;LX/9j5;LX/0Ot;LX/0kL;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;LX/9kA;LX/9kE;LX/0Ot;LX/E0T;LX/01T;LX/9j7;LX/17Y;LX/9ju;LX/31f;Ljava/lang/Boolean;LX/DzH;LX/DzL;LX/92o;LX/6Zb;LX/0wM;LX/0if;Lcom/facebook/common/perftest/PerfTestConfig;LX/0ad;LX/0Or;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2065592
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    if-nez v0, :cond_1

    .line 2065593
    :cond_0
    :goto_0
    return-void

    .line 2065594
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getVisibility()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 2065595
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2065596
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_3

    .line 2065597
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v0}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getHeight()I

    move-result v0

    .line 2065598
    if-nez v0, :cond_2

    .line 2065599
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, LX/Dyk;

    invoke-direct {v3, p0}, LX/Dyk;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2065600
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v2, v1, v1, v1, v0}, Lcom/facebook/widget/listview/BetterListView;->setPadding(IIII)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/katana/model/GeoRegion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2065782
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->m:LX/9kA;

    invoke-virtual {v0}, LX/9kA;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2065783
    if-eqz p1, :cond_1

    .line 2065784
    sget-object v0, LX/9jG;->CHECKIN:LX/9jG;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065785
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065786
    if-eq v0, v1, :cond_0

    sget-object v0, LX/9jG;->EVENT:LX/9jG;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065787
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065788
    if-eq v0, v1, :cond_1

    .line 2065789
    :cond_0
    invoke-static {p1}, Lcom/facebook/ipc/katana/model/GeoRegion;->a(LX/0Px;)Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->H:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 2065790
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->H:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 2065791
    iput-object v1, v0, LX/Dyt;->e:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    .line 2065792
    :cond_1
    invoke-direct {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->t()V

    .line 2065793
    invoke-direct {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->o()V

    .line 2065794
    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/Dyr;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 2065755
    iput-object p1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->U:LX/Dyr;

    .line 2065756
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    .line 2065757
    :goto_0
    iget-boolean v4, v2, LX/Dyy;->p:Z

    if-eq v4, v0, :cond_0

    .line 2065758
    iput-boolean v0, v2, LX/Dyy;->p:Z

    .line 2065759
    iget-object v4, v2, LX/Dyy;->f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    .line 2065760
    iput-boolean v0, v4, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->j:Z

    .line 2065761
    const v4, -0x2eb98f25

    invoke-static {v2, v4}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2065762
    :cond_0
    const v0, 0x7f0d2532

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->c(I)LX/0am;

    move-result-object v0

    .line 2065763
    if-eqz p1, :cond_3

    .line 2065764
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2065765
    const v0, 0x7f0d2531

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2065766
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    move-object v2, v0

    .line 2065767
    :goto_1
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2065768
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->U:LX/Dyr;

    .line 2065769
    sget-object v2, LX/Dyg;->a:[I

    invoke-virtual {v1}, LX/Dyr;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 2065770
    const-string v2, ""

    :goto_2
    move-object v1, v2

    .line 2065771
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2065772
    invoke-static {p0, v3}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;I)V

    .line 2065773
    :cond_1
    :goto_3
    return-void

    :cond_2
    move v0, v1

    .line 2065774
    goto :goto_0

    .line 2065775
    :cond_3
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2065776
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2065777
    invoke-direct {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->t()V

    goto :goto_3

    :cond_4
    move-object v2, v0

    goto :goto_1

    .line 2065778
    :pswitch_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0816ee

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 2065779
    :pswitch_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0816f0

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 2065780
    :pswitch_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0816f1

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 2065781
    :pswitch_3
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0816f3

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/9jK;LX/Dyq;)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    .line 2065748
    iput-boolean v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->P:Z

    .line 2065749
    const v0, 0x7f080037

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->Q:Landroid/support/v4/app/DialogFragment;

    .line 2065750
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->Q:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2065751
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->n:LX/9kE;

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jh;

    sget-object v2, LX/Dyp;->FLAG:LX/Dyp;

    invoke-static {p3, v2}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(LX/Dyq;LX/Dyp;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "android_place_picker_report_dialog"

    .line 2065752
    iget-object v10, v0, LX/9jh;->c:LX/0TD;

    new-instance v4, LX/9jg;

    move-object v5, v0

    move-object v6, p1

    move-object v7, p2

    move-object v8, v2

    move-object v9, v3

    invoke-direct/range {v4 .. v9}, LX/9jg;-><init>(LX/9jh;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/9jK;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v10, v4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2065753
    new-instance v2, LX/Dyc;

    invoke-direct {v2, p0}, LX/Dyc;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v1, v0, v2}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2065754
    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/Dyr;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/Dyq;)Z
    .locals 12
    .param p0    # Lcom/facebook/places/checkin/PlacePickerFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2065721
    sget-object v0, LX/Dyr;->SUGGEST_EDITS:LX/Dyr;

    invoke-virtual {v0, p1}, LX/Dyr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2065722
    iget-object v4, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->o:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Bgf;

    invoke-virtual {p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    sget-object v10, LX/CdT;->COMPOSER_EDIT:LX/CdT;

    sget-object v4, LX/Dyp;->SUGGEST_EDITS:LX/Dyp;

    invoke-static {p3, v4}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(LX/Dyq;LX/Dyp;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v5 .. v11}, LX/Bgf;->a(JLjava/lang/String;Ljava/lang/String;LX/CdT;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2065723
    iget-object v5, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->e:Lcom/facebook/content/SecureContextHelper;

    const/4 v6, 0x2

    invoke-interface {v5, v4, v6, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2065724
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2065725
    :cond_0
    sget-object v0, LX/Dyr;->REPORT_DUPLICATES:LX/Dyr;

    invoke-virtual {v0, p1}, LX/Dyr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2065726
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-class v1, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;

    invoke-direct {v2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2065727
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2065728
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    invoke-virtual {v0}, LX/Dyy;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2065729
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    invoke-virtual {v0, v1}, LX/Dyy;->getItemViewType(I)I

    move-result v0

    sget-object v4, LX/9jL;->SelectAtTagRow:LX/9jL;

    invoke-virtual {v4}, LX/9jL;->ordinal()I

    move-result v4

    if-ne v0, v4, :cond_1

    .line 2065730
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    invoke-virtual {v0, v1}, LX/Dyy;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065731
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPlaceType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eq v4, v5, :cond_1

    .line 2065732
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2065733
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2065734
    :cond_2
    const-string v0, "duplicate_place"

    invoke-static {v2, v0, p2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2065735
    const-string v0, "extra_place_list"

    invoke-static {v2, v0, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 2065736
    const-string v0, "entry_point"

    sget-object v1, LX/Dyp;->REPORT_DUPLICATES:LX/Dyp;

    invoke-static {p3, v1}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(LX/Dyq;LX/Dyp;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2065737
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2065738
    goto :goto_0

    .line 2065739
    :cond_3
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2065740
    sget-object v1, LX/Dyr;->INAPPROPRIATE_CONTENT:LX/Dyr;

    invoke-virtual {v1, p1}, LX/Dyr;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2065741
    const v1, 0x7f0816f1

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f0816f2

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f080020

    new-instance v3, LX/Dyd;

    invoke-direct {v3, p0, p2, p3}, LX/Dyd;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/Dyq;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2065742
    :goto_3
    const v1, 0x7f080021

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2065743
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto/16 :goto_0

    .line 2065744
    :cond_4
    sget-object v1, LX/Dyr;->NOT_A_PUBLIC_PLACE:LX/Dyr;

    invoke-virtual {v1, p1}, LX/Dyr;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2065745
    const v1, 0x7f0816f3

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f0816f4

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f080020

    new-instance v3, LX/Dye;

    invoke-direct {v3, p0, p2, p3}, LX/Dye;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/Dyq;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    goto :goto_3

    .line 2065746
    :cond_5
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->g:LX/03V;

    const-string v1, "SelectAtTagActivity"

    const-string v2, "Selected menu item not valid."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2065747
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method private b(LX/9jN;)V
    .locals 7

    .prologue
    .line 2065647
    if-nez p1, :cond_0

    .line 2065648
    new-instance p1, LX/9jN;

    invoke-direct {p1}, LX/9jN;-><init>()V

    .line 2065649
    :cond_0
    iget-object v0, p1, LX/9jN;->b:Ljava/util/List;

    move-object v0, v0

    .line 2065650
    if-nez v0, :cond_1

    .line 2065651
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2065652
    :cond_1
    iget-object v1, p1, LX/9jN;->a:Ljava/util/List;

    iput-object v1, p1, LX/9jN;->b:Ljava/util/List;

    .line 2065653
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065654
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 2065655
    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065656
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 2065657
    iget-object v1, v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065658
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 2065659
    iget-object v1, v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;->c()Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;->NORMAL:Lcom/facebook/graphql/enums/GraphQLCheckinPlaceResultsContext;

    if-eq v1, v2, :cond_6

    .line 2065660
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065661
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 2065662
    iget-object v1, v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    invoke-virtual {v1}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;->a()LX/0Px;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065663
    iget-object v3, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v2, v3

    .line 2065664
    iget-object v2, v2, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->a:Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;

    invoke-virtual {v2}, Lcom/facebook/composer/minutiae/graphql/FetchTaggableObjectGraphQLModels$TaggableObjectEdgeModel$AssociatedPlacesInfoModel;->b()LX/0Px;

    move-result-object v2

    .line 2065665
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2065666
    if-eqz v1, :cond_3

    .line 2065667
    iget-object v3, p1, LX/9jN;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065668
    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2065669
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2065670
    :cond_3
    if-eqz v2, :cond_5

    .line 2065671
    iget-object v3, p1, LX/9jN;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065672
    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2065673
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2065674
    :cond_5
    iput-object v4, p1, LX/9jN;->b:Ljava/util/List;

    .line 2065675
    :cond_6
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065676
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v1, v2

    .line 2065677
    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    invoke-virtual {v1}, LX/Dyy;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2065678
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->f:LX/0gd;

    sget-object v1, LX/0ge;->COMPOSER_ADD_LOCATION_READY:LX/0ge;

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065679
    iget-object v3, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v2, v3

    .line 2065680
    invoke-virtual {v0, v1, v2}, LX/0gd;->a(LX/0ge;Ljava/lang/String;)V

    .line 2065681
    :cond_7
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2065682
    iput-object p1, v0, LX/9j5;->g:LX/9jN;

    .line 2065683
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065684
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065685
    iget-object v2, v0, LX/Dyy;->f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    .line 2065686
    iput-object v1, v2, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->m:LX/9jG;

    .line 2065687
    iget-object v2, v0, LX/Dyy;->j:LX/Dz1;

    .line 2065688
    iput-object v1, v2, LX/Dz1;->h:LX/9jG;

    .line 2065689
    iget-object v2, v0, LX/Dyy;->k:LX/Dz0;

    .line 2065690
    iput-object v1, v2, LX/Dz0;->b:LX/9jG;

    .line 2065691
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065692
    iput-object p1, v0, LX/Dyy;->e:LX/9jN;

    .line 2065693
    iget-object v1, v0, LX/Dyy;->f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    iget-object v2, v0, LX/Dyy;->b:Landroid/location/Location;

    iget-object v3, v0, LX/Dyy;->d:Ljava/lang/String;

    .line 2065694
    iput-object p1, v1, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->e:LX/9jN;

    .line 2065695
    iput-object v2, v1, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->f:Landroid/location/Location;

    .line 2065696
    iput-object v3, v1, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->g:Ljava/lang/String;

    .line 2065697
    iget-object v1, v0, LX/Dyy;->g:LX/Dyv;

    iget-object v2, v0, LX/Dyy;->e:LX/9jN;

    .line 2065698
    iput-object v2, v1, LX/Dyv;->b:LX/9jN;

    .line 2065699
    iget-object v1, v0, LX/Dyy;->j:LX/Dz1;

    iget-object v2, v0, LX/Dyy;->e:LX/9jN;

    .line 2065700
    iput-object v2, v1, LX/Dz1;->f:LX/9jN;

    .line 2065701
    const v1, 0x130bb273

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2065702
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->setSelectionAfterHeaderView()V

    .line 2065703
    invoke-virtual {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    .line 2065704
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    invoke-virtual {v0}, LX/Dyy;->getCount()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Dyy;->getItemViewType(I)I

    move-result v0

    sget-object v1, LX/9jL;->AddHome:LX/9jL;

    invoke-virtual {v1}, LX/9jL;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_8

    .line 2065705
    invoke-static {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->r(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    .line 2065706
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->p:LX/E0T;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065707
    iget-object v2, v1, LX/Dyy;->e:LX/9jN;

    move-object v1, v2

    .line 2065708
    iget-object v2, v1, LX/9jN;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2065709
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065710
    iget-object p0, v2, LX/Dyy;->e:LX/9jN;

    move-object v2, p0

    .line 2065711
    iget-object p0, v2, LX/9jN;->d:Ljava/lang/String;

    move-object v2, p0

    .line 2065712
    iget-object v3, v0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2065713
    iget-boolean p0, v3, Lcom/facebook/places/create/home/HomeActivityLoggerData;->b:Z

    move v3, p0

    .line 2065714
    if-nez v3, :cond_8

    .line 2065715
    iget-object v3, v0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2065716
    const/4 p0, 0x1

    iput-boolean p0, v3, Lcom/facebook/places/create/home/HomeActivityLoggerData;->b:Z

    .line 2065717
    const-string v3, "home_creation_cell_shown"

    const-string p0, "home_creation"

    invoke-static {v0, v3, p0}, LX/E0T;->c(LX/E0T;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2065718
    invoke-static {v0, v3, v1, v2}, LX/E0T;->a(LX/E0T;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2065719
    iget-object p0, v0, LX/E0T;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065720
    :cond_8
    return-void
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2065626
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065627
    iput-object p0, v0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2065628
    iput-object v1, v0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065629
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065630
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v1, v2

    .line 2065631
    iput-object v1, v0, LX/Dyt;->r:Ljava/lang/String;

    .line 2065632
    iget-object v2, v0, LX/Dyt;->f:LX/9j5;

    invoke-static {v0}, LX/Dyt;->e(LX/Dyt;)Z

    move-result v3

    .line 2065633
    iput-boolean v3, v2, LX/9j5;->i:Z

    .line 2065634
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    .line 2065635
    if-eqz p1, :cond_0

    .line 2065636
    const-string v1, "previously_selected_location"

    invoke-static {p1, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v1, v0, LX/Dyt;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065637
    :cond_0
    invoke-static {v0}, LX/Dyt;->h(LX/Dyt;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2065638
    iget-object v1, v0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-virtual {v1}, Lcom/facebook/places/checkin/PlacePickerFragment;->k()V

    .line 2065639
    iget-object v1, v0, LX/Dyt;->f:LX/9j5;

    .line 2065640
    iget-object v0, v1, LX/9j5;->a:LX/0Zb;

    const-string p1, "place_picker_people_to_place_start"

    invoke-static {v1, p1}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065641
    :cond_1
    :goto_0
    return-void

    .line 2065642
    :cond_2
    invoke-static {v0}, LX/Dyt;->g(LX/Dyt;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2065643
    iget-object v1, v0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-virtual {v1}, Lcom/facebook/places/checkin/PlacePickerFragment;->k()V

    .line 2065644
    iget-object v1, v0, LX/Dyt;->f:LX/9j5;

    .line 2065645
    iget-object v0, v1, LX/9j5;->a:LX/0Zb;

    const-string p1, "place_picker_minutiae_to_place_start"

    invoke-static {v1, p1}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v0, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065646
    goto :goto_0
.end method

.method public static b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Z
    .locals 2

    .prologue
    .line 2065625
    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x403827a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/places/checkin/PlacePickerFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2065622
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2065623
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2065624
    return-void
.end method

.method private o()V
    .locals 3

    .prologue
    .line 2065601
    sget-object v0, LX/9jG;->EVENT:LX/9jG;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065602
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065603
    if-eq v0, v1, :cond_0

    sget-object v0, LX/9jG;->PLACE_TIPS_APP:LX/9jG;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065604
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065605
    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->q:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_1

    .line 2065606
    :cond_0
    :goto_0
    return-void

    .line 2065607
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2065608
    iget-boolean v1, v0, LX/Dz6;->f:Z

    move v0, v1

    .line 2065609
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2065610
    iget-object v1, v0, LX/Dz6;->h:Landroid/location/Location;

    .line 2065611
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    const/high16 v0, 0x437a0000    # 250.0f

    cmpg-float v2, v2, v0

    if-gez v2, :cond_4

    const/4 v2, 0x1

    :goto_1
    move v1, v2

    .line 2065612
    move v0, v1

    .line 2065613
    if-nez v0, :cond_3

    .line 2065614
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065615
    iget-object v1, v0, LX/Dyy;->h:LX/Dyx;

    const/4 v2, 0x0

    .line 2065616
    iput-boolean v2, v1, LX/Dyx;->c:Z

    .line 2065617
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    goto :goto_0

    .line 2065618
    :cond_3
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065619
    iget-object v1, v0, LX/Dyy;->h:LX/Dyx;

    const/4 v2, 0x1

    .line 2065620
    iput-boolean v2, v1, LX/Dyx;->c:Z

    .line 2065621
    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static r(Lcom/facebook/places/checkin/PlacePickerFragment;)V
    .locals 6

    .prologue
    .line 2065460
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->p:LX/E0T;

    .line 2065461
    iget-object v1, v0, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    move-object v0, v1

    .line 2065462
    if-eqz v0, :cond_0

    .line 2065463
    :goto_0
    return-void

    .line 2065464
    :cond_0
    new-instance v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;

    invoke-direct {v0}, Lcom/facebook/places/create/home/HomeActivityLoggerData;-><init>()V

    .line 2065465
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    invoke-virtual {v1}, LX/9j5;->z()Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    move-result-object v1

    .line 2065466
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2065467
    iget-object v2, v1, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2065468
    iput-object v2, v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->c:Ljava/lang/String;

    .line 2065469
    iget-object v2, v1, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2065470
    iput-object v2, v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->d:Ljava/lang/String;

    .line 2065471
    iget-wide v4, v1, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->c:J

    move-wide v2, v4

    .line 2065472
    iput-wide v2, v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->e:J

    .line 2065473
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->p:LX/E0T;

    .line 2065474
    iput-object v0, v1, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2065475
    goto :goto_0
.end method

.method private t()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 2065476
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    invoke-virtual {v0}, LX/Dyt;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2065477
    :cond_0
    :goto_0
    return-void

    .line 2065478
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065479
    iget-boolean v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->h:Z

    move v0, v1

    .line 2065480
    if-nez v0, :cond_0

    .line 2065481
    invoke-static {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->u(Lcom/facebook/places/checkin/PlacePickerFragment;)Ljava/lang/String;

    move-result-object v1

    .line 2065482
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 2065483
    invoke-static {p0, v4}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;I)V

    goto :goto_0

    .line 2065484
    :cond_2
    if-eqz v1, :cond_0

    .line 2065485
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->W:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2065486
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const v2, 0x7f0d252d

    invoke-virtual {v0, v2}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2065487
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2065488
    new-instance v1, LX/Dyj;

    invoke-direct {v1, p0}, LX/Dyj;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2065489
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->V:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const v1, 0x7f0d252c

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2065490
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065491
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 2065492
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065493
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 2065494
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065495
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 2065496
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2065497
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065498
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 2065499
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->p()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2065500
    invoke-virtual {v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2065501
    const-class v2, Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2065502
    :goto_1
    invoke-static {p0, v3}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;I)V

    goto/16 :goto_0

    .line 2065503
    :cond_3
    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1
.end method

.method public static u(Lcom/facebook/places/checkin/PlacePickerFragment;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2064877
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064878
    iget-object v3, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v3

    .line 2064879
    if-eqz v0, :cond_1

    .line 2064880
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064881
    iget-object v3, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    move-object v0, v3

    .line 2064882
    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/45Y;->a(Z)V

    .line 2064883
    const v0, 0x7f081706

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064884
    iget-object v4, v3, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v3, v4

    .line 2064885
    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2064886
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 2064887
    goto :goto_0

    .line 2064888
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064889
    iget-object v3, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    move-object v0, v3

    .line 2064890
    if-eqz v0, :cond_2

    .line 2064891
    const v0, 0x7f081706

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064892
    iget-object v4, v3, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->B:Ljava/lang/String;

    move-object v3, v4

    .line 2064893
    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2064894
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064895
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    move-object v0, v1

    .line 2064896
    if-eqz v0, :cond_3

    .line 2064897
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064898
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    move-object v0, v1

    .line 2064899
    goto :goto_1

    .line 2064900
    :cond_3
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->H:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064901
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v0, v1

    .line 2064902
    sget-object v1, LX/9jG;->CHECKIN:LX/9jG;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064903
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v0, v1

    .line 2064904
    sget-object v1, LX/9jG;->EVENT:LX/9jG;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064905
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v0, v1

    .line 2064906
    sget-object v1, LX/9jG;->PLACE_TIPS_EMPLOYEE_SETTINGS:LX/9jG;

    if-eq v0, v1, :cond_4

    .line 2064907
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->H:Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;

    iget-object v0, v0, Lcom/facebook/ipc/katana/model/GeoRegion$ImplicitLocation;->label:Ljava/lang/String;

    goto :goto_1

    .line 2064908
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2064909
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 2064910
    if-nez v0, :cond_0

    .line 2064911
    :goto_0
    return-void

    .line 2064912
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2064913
    iget-boolean v1, v0, LX/Dz6;->g:Z

    move v0, v1

    .line 2064914
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->T:LX/Dys;

    sget-object v1, LX/Dys;->QUERY:LX/Dys;

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    .line 2064915
    :goto_1
    invoke-direct {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->J()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz v0, :cond_3

    .line 2064916
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->J:Lcom/facebook/places/checkin/ui/PlacesListContainer;

    invoke-virtual {v0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->e()V

    goto :goto_0

    .line 2064917
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2064918
    :cond_3
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->J:Lcom/facebook/places/checkin/ui/PlacesListContainer;

    invoke-virtual {v0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->getListViewCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 2064919
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->J:Lcom/facebook/places/checkin/ui/PlacesListContainer;

    invoke-virtual {v0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->a()V

    .line 2064920
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v0}, LX/DzG;->f()V

    .line 2064921
    invoke-static {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->G(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    goto :goto_0

    .line 2064922
    :cond_4
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->J:Lcom/facebook/places/checkin/ui/PlacesListContainer;

    invoke-virtual {v0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b()V

    goto :goto_0
.end method

.method public final a(ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2064923
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2064924
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2064925
    return-void
.end method

.method public final a(LX/6ZY;)V
    .locals 1

    .prologue
    .line 2064926
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    if-eqz v0, :cond_0

    .line 2064927
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v0, p1}, LX/DzG;->a(LX/6ZY;)V

    .line 2064928
    :cond_0
    return-void
.end method

.method public final a(LX/9jN;)V
    .locals 4

    .prologue
    .line 2064929
    invoke-direct {p0, p1}, Lcom/facebook/places/checkin/PlacePickerFragment;->b(LX/9jN;)V

    .line 2064930
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getLastVisiblePosition()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v2}, Lcom/facebook/widget/listview/BetterListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v1, v2

    .line 2064931
    iget-boolean v2, v0, LX/9j5;->p:Z

    if-nez v2, :cond_0

    .line 2064932
    iget-object v2, v0, LX/9j5;->a:LX/0Zb;

    const-string v3, "place_picker_results_loaded"

    invoke-static {v0, v3}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-static {v0, v3}, LX/9j5;->a(LX/9j5;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2064933
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, LX/9j5;->a(II)V

    .line 2064934
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/9j5;->p:Z

    .line 2064935
    iget-object v0, p1, LX/9jN;->b:Ljava/util/List;

    move-object v0, v0

    .line 2064936
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2064937
    invoke-static {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->G(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    .line 2064938
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    .line 2064939
    return-void
.end method

.method public final a(Landroid/location/Location;)V
    .locals 4
    .param p1    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2064940
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2064941
    iput-object p1, v0, LX/Dyy;->b:Landroid/location/Location;

    .line 2064942
    iget-object v1, v0, LX/Dyy;->f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    .line 2064943
    iput-object p1, v1, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->f:Landroid/location/Location;

    .line 2064944
    if-eqz p1, :cond_2

    .line 2064945
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2064946
    iget-boolean v1, v0, LX/9j5;->o:Z

    if-nez v1, :cond_0

    .line 2064947
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/9j5;->o:Z

    .line 2064948
    iput-object p1, v0, LX/9j5;->h:Landroid/location/Location;

    .line 2064949
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_gps_loaded"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v0, v2}, LX/9j5;->a(LX/9j5;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2064950
    :cond_0
    :goto_0
    new-instance v0, LX/9jo;

    invoke-direct {v0}, LX/9jo;-><init>()V

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {v1}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2064951
    iput-object v1, v0, LX/9jo;->a:Ljava/lang/String;

    .line 2064952
    move-object v0, v0

    .line 2064953
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064954
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2064955
    iput-object v1, v0, LX/9jo;->c:LX/9jG;

    .line 2064956
    move-object v0, v0

    .line 2064957
    iput-object p1, v0, LX/9jo;->b:Landroid/location/Location;

    .line 2064958
    move-object v0, v0

    .line 2064959
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2064960
    iget-boolean v2, v1, LX/Dz6;->f:Z

    move v1, v2

    .line 2064961
    iput-boolean v1, v0, LX/9jo;->d:Z

    .line 2064962
    move-object v0, v0

    .line 2064963
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->S:LX/9jC;

    .line 2064964
    iput-object v1, v0, LX/9jo;->g:LX/9jC;

    .line 2064965
    move-object v0, v0

    .line 2064966
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064967
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2064968
    iput-object v1, v0, LX/9jo;->f:Ljava/lang/String;

    .line 2064969
    move-object v0, v0

    .line 2064970
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064971
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    move-object v1, v2

    .line 2064972
    iput-object v1, v0, LX/9jo;->e:Ljava/lang/String;

    .line 2064973
    move-object v1, v0

    .line 2064974
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->t:LX/9ju;

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->T:LX/Dys;

    sget-object v3, LX/Dys;->PTR:LX/Dys;

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->T:LX/Dys;

    sget-object v3, LX/Dys;->NIEM_CLICKED:LX/Dys;

    if-ne v0, v3, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v1, v0}, LX/9ju;->a(LX/9jo;Z)V

    .line 2064975
    invoke-direct {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->o()V

    .line 2064976
    return-void

    .line 2064977
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2064978
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/9j5;->r:Z

    .line 2064979
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->r:LX/9j7;

    invoke-virtual {v0}, LX/9j7;->a()V

    goto :goto_0

    .line 2064980
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2064981
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2064982
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2064983
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->r:LX/9j7;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    .line 2064984
    iget-object v2, v0, LX/9j7;->b:LX/0id;

    invoke-virtual {v2, v1}, LX/0id;->o(Ljava/lang/String;)V

    .line 2064985
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->z:LX/6Zb;

    invoke-virtual {v0, p0, p0}, LX/6Zb;->a(Lcom/facebook/base/fragment/FbFragment;LX/6ZZ;)V

    .line 2064986
    invoke-static {p0}, LX/9jy;->a(Ljava/lang/Object;)V

    .line 2064987
    invoke-static {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->H(Lcom/facebook/places/checkin/PlacePickerFragment;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "place_picker_configuration"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064988
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2064989
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2064990
    iput-object p0, v0, LX/Dz6;->e:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2064991
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064992
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-object v0, v1

    .line 2064993
    if-eqz v0, :cond_0

    .line 2064994
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2064995
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->p:Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-object v1, v2

    .line 2064996
    const-string v2, "preset_search_location"

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerLocation;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 2064997
    iput-object v1, v0, LX/Dz6;->h:Landroid/location/Location;

    .line 2064998
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Dz6;->f:Z

    .line 2064999
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065000
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-object v0, v1

    .line 2065001
    if-eqz v0, :cond_1

    .line 2065002
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065003
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    move-object v1, v2

    .line 2065004
    const-string v2, "composer_location"

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/model/ComposerLocation;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 2065005
    iget-object v2, v0, LX/Dz6;->i:LX/Dz4;

    .line 2065006
    iput-object v1, v2, LX/Dz4;->e:Landroid/location/Location;

    .line 2065007
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065008
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->e:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-object v1, v1

    .line 2065009
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065010
    iget-boolean v2, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    move v0, v2

    .line 2065011
    if-eqz v0, :cond_2

    .line 2065012
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2065013
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->f:LX/0gd;

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065014
    iget-object p1, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->g:Ljava/lang/String;

    move-object v2, p1

    .line 2065015
    invoke-virtual {v0, v2, v1}, LX/0gd;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 2065016
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065017
    iget-boolean v2, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->k:Z

    move v0, v2

    .line 2065018
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065019
    iget-boolean v2, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->y:Z

    move v0, v2

    .line 2065020
    if-eqz v0, :cond_4

    :cond_3
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    .line 2065021
    :goto_0
    if-eqz v1, :cond_8

    .line 2065022
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialAttachments()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2065023
    sget-object v0, LX/9jC;->Photo:LX/9jC;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->S:LX/9jC;

    .line 2065024
    :goto_1
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065025
    iget-boolean v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->a:Z

    move v1, v2

    .line 2065026
    iget-object v2, v0, LX/Dyy;->f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    .line 2065027
    iput-boolean v1, v2, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->n:Z

    .line 2065028
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065029
    iget-boolean v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->b:Z

    move v1, v2

    .line 2065030
    iget-object v2, v0, LX/Dyy;->f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    .line 2065031
    iput-boolean v1, v2, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->o:Z

    .line 2065032
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->t:LX/9ju;

    .line 2065033
    iput-object p0, v0, LX/9ju;->j:LX/90X;

    .line 2065034
    return-void

    .line 2065035
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 2065036
    :cond_6
    if-eqz v0, :cond_7

    .line 2065037
    sget-object v0, LX/9jC;->Checkin:LX/9jC;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->S:LX/9jC;

    goto :goto_1

    .line 2065038
    :cond_7
    sget-object v0, LX/9jC;->Status:LX/9jC;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->S:LX/9jC;

    goto :goto_1

    .line 2065039
    :cond_8
    sget-object v0, LX/9jC;->Other:LX/9jC;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->S:LX/9jC;

    goto :goto_1
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .prologue
    .line 2065040
    if-nez p1, :cond_1

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    .line 2065041
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 2065042
    iput v1, v0, LX/9j5;->j:I

    .line 2065043
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    const/4 p1, 0x1

    .line 2065044
    iput-boolean p1, v0, LX/9j5;->r:Z

    .line 2065045
    iget-boolean v2, v0, LX/9j5;->n:Z

    if-nez v2, :cond_0

    .line 2065046
    iget-object v2, v0, LX/9j5;->a:LX/0Zb;

    const-string v3, "place_picker_first_keystroke"

    invoke-static {v0, v3}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "device_orientation"

    iget v5, v0, LX/9j5;->j:I

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065047
    :cond_0
    iput-object v1, v0, LX/9j5;->c:Ljava/lang/String;

    .line 2065048
    iput-boolean p1, v0, LX/9j5;->n:Z

    .line 2065049
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->r:LX/9j7;

    invoke-virtual {v0}, LX/9j7;->a()V

    .line 2065050
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    if-nez v0, :cond_2

    .line 2065051
    :goto_1
    return-void

    .line 2065052
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2065053
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    .line 2065054
    iget-object v2, v0, LX/Dyy;->c:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/Dyy;->d:Ljava/lang/String;

    .line 2065055
    iget-object v2, v0, LX/Dyy;->f:Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;

    .line 2065056
    iput-object v1, v2, Lcom/facebook/places/checkin/adapter/SelectAtTagRowSection;->g:Ljava/lang/String;

    .line 2065057
    iget-object v2, v0, LX/Dyy;->h:LX/Dyx;

    .line 2065058
    iput-object v1, v2, LX/Dyx;->d:Ljava/lang/String;

    .line 2065059
    iget-object v2, v0, LX/Dyy;->j:LX/Dz1;

    .line 2065060
    iput-object v1, v2, LX/Dz1;->g:Ljava/lang/String;

    .line 2065061
    sget-object v0, LX/Dys;->QUERY:LX/Dys;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->T:LX/Dys;

    .line 2065062
    new-instance v0, LX/9jo;

    invoke-direct {v0}, LX/9jo;-><init>()V

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    .line 2065063
    iput-object v1, v0, LX/9jo;->a:Ljava/lang/String;

    .line 2065064
    move-object v0, v0

    .line 2065065
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065066
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065067
    iput-object v1, v0, LX/9jo;->c:LX/9jG;

    .line 2065068
    move-object v0, v0

    .line 2065069
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2065070
    iget-object v2, v1, LX/Dz6;->h:Landroid/location/Location;

    move-object v1, v2

    .line 2065071
    iput-object v1, v0, LX/9jo;->b:Landroid/location/Location;

    .line 2065072
    move-object v0, v0

    .line 2065073
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2065074
    iget-boolean v2, v1, LX/Dz6;->f:Z

    move v1, v2

    .line 2065075
    iput-boolean v1, v0, LX/9jo;->d:Z

    .line 2065076
    move-object v0, v0

    .line 2065077
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->S:LX/9jC;

    .line 2065078
    iput-object v1, v0, LX/9jo;->g:LX/9jC;

    .line 2065079
    move-object v0, v0

    .line 2065080
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065081
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2065082
    iput-object v1, v0, LX/9jo;->f:Ljava/lang/String;

    .line 2065083
    move-object v0, v0

    .line 2065084
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065085
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->w:Ljava/lang/String;

    move-object v1, v2

    .line 2065086
    iput-object v1, v0, LX/9jo;->e:Ljava/lang/String;

    .line 2065087
    move-object v0, v0

    .line 2065088
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->t:LX/9ju;

    .line 2065089
    iget-object v2, v1, LX/9ju;->k:Ljava/lang/Runnable;

    if-eqz v2, :cond_3

    .line 2065090
    iget-object v2, v1, LX/9ju;->h:Landroid/os/Handler;

    iget-object v3, v1, LX/9ju;->k:Ljava/lang/Runnable;

    invoke-static {v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2065091
    :cond_3
    new-instance v2, Lcom/facebook/places/checkin/protocol/PlacePickerFetcher$3;

    invoke-direct {v2, v1, v0}, Lcom/facebook/places/checkin/protocol/PlacePickerFetcher$3;-><init>(LX/9ju;LX/9jo;)V

    iput-object v2, v1, LX/9ju;->k:Ljava/lang/Runnable;

    .line 2065092
    iget-object v2, v1, LX/9ju;->h:Landroid/os/Handler;

    iget-object v3, v1, LX/9ju;->k:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    const v6, -0x62613d01

    invoke-static {v2, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2065093
    iget-object v2, v1, LX/9ju;->j:LX/90X;

    invoke-interface {v2}, LX/90X;->a()V

    .line 2065094
    sget-object v0, LX/9jG;->EVENT:LX/9jG;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065095
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065096
    if-eq v0, v1, :cond_6

    .line 2065097
    :goto_2
    invoke-direct {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->o()V

    .line 2065098
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2065099
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v0}, LX/DzG;->g()V

    goto/16 :goto_1

    .line 2065100
    :cond_4
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    .line 2065101
    invoke-virtual {v0}, LX/DzG;->m()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, LX/DzG;->j()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, LX/DzG;->k()Z

    move-result v1

    if-nez v1, :cond_9

    .line 2065102
    :cond_5
    :goto_3
    goto/16 :goto_1

    .line 2065103
    :cond_6
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2065104
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065105
    iget-object v1, v0, LX/Dyy;->i:LX/Dz3;

    const/4 v2, 0x0

    .line 2065106
    iput-object v2, v1, LX/Dz3;->b:Ljava/lang/String;

    .line 2065107
    goto :goto_2

    .line 2065108
    :cond_7
    const v0, 0x7f0816ff

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2065109
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065110
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    const/4 v2, 0x1

    :goto_4
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2065111
    iget-object v2, v1, LX/Dyy;->i:LX/Dz3;

    .line 2065112
    iput-object v0, v2, LX/Dz3;->b:Ljava/lang/String;

    .line 2065113
    goto :goto_2

    .line 2065114
    :cond_8
    const/4 v2, 0x0

    goto :goto_4

    .line 2065115
    :cond_9
    invoke-virtual {v0}, LX/DzG;->c()LX/Dz7;

    move-result-object v1

    invoke-virtual {v1}, LX/Dz7;->a()V

    goto :goto_3
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2065116
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    if-eqz v0, :cond_0

    .line 2065117
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v0}, LX/DzG;->f()V

    .line 2065118
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    .line 2065119
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2065120
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2065121
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f081709

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2065122
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 2065123
    move-object v0, v0

    .line 2065124
    const/4 v1, 0x1

    .line 2065125
    iput-boolean v1, v0, LX/108;->q:Z

    .line 2065126
    move-object v0, v0

    .line 2065127
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 2065128
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v1, v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2065129
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/Dyb;

    invoke-direct {v1, p0}, LX/Dyb;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 2065130
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x719074c6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2065131
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2065132
    const/16 v1, 0x2b

    const v2, -0x5c4131d7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    .line 2065133
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2065134
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    invoke-virtual {v0, p1, p2, p3}, LX/Dyt;->a(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2065135
    :cond_0
    :goto_0
    return-void

    .line 2065136
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2065137
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 2065138
    :sswitch_0
    const-string v0, "create_home_from_place_creation"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2065139
    sget-object v0, LX/E0Q;->PLACE_CREATION:LX/E0Q;

    invoke-direct {p0, v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(LX/E0Q;)V

    goto :goto_0

    .line 2065140
    :sswitch_1
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065141
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    invoke-virtual {v1, v0}, LX/Dyt;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    goto :goto_0

    .line 2065142
    :cond_2
    const-string v0, "extra_place"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2065143
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065144
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2065145
    iget-object v2, v1, LX/9j5;->a:LX/0Zb;

    const-string v3, "place_picker_add_place_done"

    invoke-static {v1, v3}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "added_place"

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065146
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    invoke-virtual {v1, v0}, LX/Dyt;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    goto :goto_0

    .line 2065147
    :cond_3
    const-string v0, "selected_existing_place"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2065148
    const-string v0, "selected_existing_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065149
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    invoke-virtual {v1, v0}, LX/Dyt;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    goto :goto_0

    .line 2065150
    :sswitch_2
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f08293f

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x7 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x4a3ba4a7    # 3074345.8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2065151
    const v1, 0x7f030f6f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2065152
    const/16 v2, 0x2b

    const v3, 0x16c397a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3a1a7aa5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2065153
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 2065154
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->B:LX/0if;

    sget-object v2, LX/0ig;->ay:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 2065155
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    .line 2065156
    iget-object v2, v1, LX/DzG;->s:LX/0Yb;

    if-eqz v2, :cond_0

    .line 2065157
    iget-object v2, v1, LX/DzG;->s:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->c()V

    .line 2065158
    :cond_0
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->t:LX/9ju;

    .line 2065159
    invoke-static {v1}, LX/9ju;->i(LX/9ju;)V

    .line 2065160
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->O:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2065161
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->z:LX/6Zb;

    invoke-virtual {v1}, LX/6Zb;->a()V

    .line 2065162
    const/16 v1, 0x2b

    const v2, 0x4d522a79    # 2.20374928E8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2065163
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-lt p3, v0, :cond_0

    .line 2065164
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->g:LX/03V;

    const-string v1, "SelectAtTagActivity"

    const-string v2, "Clicking outside of adapter bounds"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2065165
    :goto_0
    return-void

    .line 2065166
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    invoke-virtual {v0, p3}, LX/Dyy;->getItemViewType(I)I

    move-result v0

    .line 2065167
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->U:LX/Dyr;

    if-eqz v1, :cond_2

    .line 2065168
    sget-object v1, LX/9jL;->SelectAtTagRow:LX/9jL;

    invoke-virtual {v1}, LX/9jL;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 2065169
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->k:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08176f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_0

    .line 2065170
    :cond_1
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->U:LX/Dyr;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    sget-object v2, LX/Dyq;->EDIT_MENU:LX/Dyq;

    invoke-static {p0, v1, v0, v2}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/Dyr;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/Dyq;)Z

    .line 2065171
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/Dyr;)V

    goto :goto_0

    .line 2065172
    :cond_2
    sget-object v1, LX/9jL;->AddHome:LX/9jL;

    invoke-virtual {v1}, LX/9jL;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 2065173
    sget-object v0, LX/E0Q;->PLACE_PICKER:LX/E0Q;

    invoke-direct {p0, v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(LX/E0Q;)V

    goto :goto_0

    .line 2065174
    :cond_3
    sget-object v1, LX/9jL;->AddPlace:LX/9jL;

    invoke-virtual {v1}, LX/9jL;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 2065175
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2065176
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_add_place_started"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065177
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->s:LX/17Y;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    sget-object v2, LX/0ax;->fq:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2065178
    const-string v1, "android.intent.extra.SUBJECT"

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {v2}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2065179
    const-string v1, "place_picker_session_data"

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    invoke-virtual {v2}, LX/9j5;->z()Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2065180
    const-string v1, "extra_location"

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2065181
    iget-object p1, v2, LX/Dz6;->h:Landroid/location/Location;

    move-object v2, p1

    .line 2065182
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2065183
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->e:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2065184
    goto/16 :goto_0

    .line 2065185
    :cond_4
    sget-object v1, LX/9jL;->UseAsText:LX/9jL;

    invoke-virtual {v1}, LX/9jL;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 2065186
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2065187
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    .line 2065188
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2065189
    iget-object p0, v0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065190
    iget-object p1, p0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object p0, p1

    .line 2065191
    if-eqz p0, :cond_5

    .line 2065192
    const-string p0, "minutiae_object"

    iget-object p1, v0, LX/Dyt;->c:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065193
    iget-object p2, p1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object p1, p2

    .line 2065194
    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2065195
    :cond_5
    const-string p0, "extra_location_text"

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2065196
    iget-object p0, v0, LX/Dyt;->b:Lcom/facebook/places/checkin/PlacePickerFragment;

    const/4 p1, -0x1

    invoke-virtual {p0, p1, v2}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(ILandroid/content/Intent;)V

    .line 2065197
    goto/16 :goto_0

    .line 2065198
    :cond_6
    sget-object v1, LX/9jL;->TextOnlyRow:LX/9jL;

    invoke-virtual {v1}, LX/9jL;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_7

    .line 2065199
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    .line 2065200
    iget-object v2, v1, LX/Dyy;->j:LX/Dz1;

    invoke-virtual {v2}, LX/Dz1;->e()Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 2065201
    iget-object v2, v0, LX/9j5;->a:LX/0Zb;

    const-string p1, "place_picker_phrase_row_selected"

    invoke-static {v0, p1}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "qe_group"

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065202
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    invoke-virtual {v0, p3}, LX/Dyy;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/Dyt;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2065203
    :cond_7
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065204
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->y:LX/92o;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/92o;->a(Ljava/lang/String;)V

    .line 2065205
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    check-cast v1, LX/Dyy;

    .line 2065206
    iget-object p1, v1, LX/Dyy;->e:LX/9jN;

    move-object v1, p1

    .line 2065207
    iput-object v1, v2, LX/9j5;->g:LX/9jN;

    .line 2065208
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    invoke-virtual {v1, v0}, LX/Dyt;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2065209
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->B:LX/0if;

    sget-object v1, LX/0ig;->ay:LX/0ih;

    const-string v2, "place_picked"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2065210
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItemViewType(I)I

    move-result v0

    sget-object v2, LX/9jL;->SelectAtTagRow:LX/9jL;

    invoke-virtual {v2}, LX/9jL;->ordinal()I

    move-result v2

    if-eq v0, v2, :cond_0

    move v0, v1

    .line 2065211
    :goto_0
    return v0

    .line 2065212
    :cond_0
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065213
    if-nez v0, :cond_1

    move v0, v1

    .line 2065214
    goto :goto_0

    .line 2065215
    :cond_1
    invoke-static {v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 2065216
    goto :goto_0

    .line 2065217
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, Landroid/widget/AdapterView;->performHapticFeedback(II)Z

    .line 2065218
    invoke-static {v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->U:LX/Dyr;

    if-nez v1, :cond_3

    .line 2065219
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/Dyq;->LONG_PRESS:LX/Dyq;

    .line 2065220
    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    .line 2065221
    iget-object p2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->u:LX/31f;

    sget-object p3, LX/Dyp;->FLAG:LX/Dyp;

    invoke-static {v2, p3}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(LX/Dyq;LX/Dyp;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3, p1}, LX/31f;->a(Ljava/lang/String;LX/0am;)V

    .line 2065222
    iget-object p2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->u:LX/31f;

    sget-object p3, LX/Dyp;->REPORT_DUPLICATES:LX/Dyp;

    invoke-static {v2, p3}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(LX/Dyq;LX/Dyp;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3, p1}, LX/31f;->a(Ljava/lang/String;LX/0am;)V

    .line 2065223
    iget-object p2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->u:LX/31f;

    sget-object p3, LX/Dyp;->SUGGEST_EDITS:LX/Dyp;

    invoke-static {v2, p3}, Lcom/facebook/places/checkin/PlacePickerFragment;->a(LX/Dyq;LX/Dyp;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3, p1}, LX/31f;->a(Ljava/lang/String;LX/0am;)V

    .line 2065224
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->E:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Af;

    .line 2065225
    new-instance v2, LX/34b;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v2, p1}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2065226
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0816ee

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p1

    new-instance p2, LX/Dyl;

    invoke-direct {p2, p0, v0}, LX/Dyl;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    invoke-virtual {p1, p2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2065227
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0816f0

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p1

    new-instance p2, LX/Dym;

    invoke-direct {p2, p0, v0}, LX/Dym;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    invoke-virtual {p1, p2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2065228
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0816f1

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p1

    new-instance p2, LX/Dyn;

    invoke-direct {p2, p0, v0}, LX/Dyn;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    invoke-virtual {p1, p2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2065229
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0816f3

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p1

    new-instance p2, LX/Dyo;

    invoke-direct {p2, p0, v0}, LX/Dyo;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    invoke-virtual {p1, p2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2065230
    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2065231
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2065232
    :cond_3
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2065233
    iget-object v1, v0, LX/9j5;->a:LX/0Zb;

    const-string v2, "place_picker_long_click"

    invoke-static {v0, v2}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065234
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x523d10d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2065235
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2065236
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->r:LX/9j7;

    invoke-virtual {v1}, LX/9j7;->a()V

    .line 2065237
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2065238
    const/16 v1, 0x2b

    const v2, -0x67782c59

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xd9e4577

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2065239
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2065240
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {v1, p0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2065241
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/0Px;)V

    .line 2065242
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v1}, LX/DzG;->m()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v1}, LX/DzG;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2065243
    :cond_0
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v1}, LX/DzG;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2065244
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->r:LX/9j7;

    invoke-virtual {v1}, LX/9j7;->a()V

    .line 2065245
    :cond_1
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v1}, LX/DzG;->g()V

    .line 2065246
    :cond_2
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->r:LX/9j7;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 2065247
    iget-object v4, v1, LX/9j7;->b:LX/0id;

    invoke-virtual {v4, v2}, LX/0id;->p(Ljava/lang/String;)V

    .line 2065248
    invoke-virtual {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    .line 2065249
    const/16 v1, 0x2b

    const v2, -0x4bd3bb5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 2065250
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2065251
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    .line 2065252
    const-string v1, "previously_selected_location"

    iget-object v2, v0, LX/Dyt;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2065253
    const-string v1, "search_text"

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2065254
    const-string v1, "current_context_menu_place"

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2065255
    const-string v1, "home_creation_logger_data"

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->p:LX/E0T;

    .line 2065256
    iget-object v3, v2, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    move-object v2, v3

    .line 2065257
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2065258
    const-string v1, "analytics_bundle"

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2065259
    iget-object v3, v2, LX/9j5;->a:LX/0Zb;

    const-string v4, "place_picker_backgrounded"

    invoke-static {v2, v4}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065260
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2065261
    const-string v4, "has_saved_instance_state"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2065262
    const-string v4, "has_results_loaded"

    iget-boolean v5, v2, LX/9j5;->p:Z

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2065263
    const-string v4, "has_past_places_in_main_list_loaded"

    iget-boolean v5, v2, LX/9j5;->q:Z

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2065264
    const-string v4, "has_location_been_received"

    iget-boolean v5, v2, LX/9j5;->o:Z

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2065265
    const-string v4, "has_typed"

    iget-boolean v5, v2, LX/9j5;->n:Z

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2065266
    const-string v4, "has_scrolled"

    iget-boolean v5, v2, LX/9j5;->m:Z

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2065267
    const-string v4, "has_tti_error"

    iget-boolean v5, v2, LX/9j5;->r:Z

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2065268
    const-string v4, "query"

    iget-object v5, v2, LX/9j5;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2065269
    const-string v4, "composer_session_id"

    iget-object v5, v2, LX/9j5;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2065270
    const-string v4, "place_picker_session_id"

    iget-object v5, v2, LX/9j5;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2065271
    const-string v4, "start_time"

    iget-wide v5, v2, LX/9j5;->s:J

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2065272
    const-string v4, "device_orientation"

    iget v5, v2, LX/9j5;->j:I

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2065273
    move-object v2, v3

    .line 2065274
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2065275
    const-string v1, "crowdsourcing_edit_state"

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->U:LX/Dyr;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2065276
    return-void
.end method

.method public final onStart()V
    .locals 15

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2498aeba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2065277
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 2065278
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    const/4 v5, 0x0

    .line 2065279
    iget-object v4, v1, LX/Dz6;->e:Lcom/facebook/places/checkin/PlacePickerFragment;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2065280
    iget-object v4, v1, LX/Dz6;->h:Landroid/location/Location;

    if-nez v4, :cond_4

    .line 2065281
    invoke-virtual {v1}, LX/Dz6;->h()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2065282
    iget-object v4, v1, LX/Dz6;->i:LX/Dz4;

    .line 2065283
    iget-boolean v6, v4, LX/Dz4;->g:Z

    if-eqz v6, :cond_5

    .line 2065284
    const/4 v6, 0x0

    .line 2065285
    :goto_0
    move-object v4, v6

    .line 2065286
    if-eqz v4, :cond_2

    .line 2065287
    invoke-static {v1, v4}, LX/Dz6;->c(LX/Dz6;Landroid/location/Location;)V

    .line 2065288
    :goto_1
    sget-boolean v4, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->j:Z

    move v4, v4

    .line 2065289
    if-eqz v4, :cond_b

    .line 2065290
    const/4 v4, 0x0

    .line 2065291
    :goto_2
    move v4, v4

    .line 2065292
    if-eqz v4, :cond_1

    .line 2065293
    iget-object v4, v1, LX/Dz6;->e:Lcom/facebook/places/checkin/PlacePickerFragment;

    .line 2065294
    iget-object v5, v4, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    invoke-virtual {v5}, LX/Dz6;->h()Z

    move-result v5

    if-nez v5, :cond_0

    .line 2065295
    iget-object v5, v4, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    .line 2065296
    iget-object v6, v5, LX/9j5;->a:LX/0Zb;

    const-string v7, "place_picker_error_location_disabled"

    invoke-static {v5, v7}, LX/9j5;->h(LX/9j5;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-static {v5, v7}, LX/9j5;->a(LX/9j5;Lcom/facebook/analytics/logger/HoneyClientEvent;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2065297
    :cond_0
    iget-object v5, v4, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    invoke-virtual {v5}, LX/DzG;->g()V

    .line 2065298
    iget-object v5, v4, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {v5}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2065299
    iget-object v5, v4, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    invoke-virtual {v5}, LX/Dz6;->h()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2065300
    invoke-static {v4}, Lcom/facebook/places/checkin/PlacePickerFragment;->G(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    .line 2065301
    :cond_1
    :goto_3
    const/16 v1, 0x2b

    const v2, -0x740c45c0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2065302
    :cond_2
    invoke-static {v1, v5}, LX/Dz6;->d(LX/Dz6;Landroid/location/Location;)V

    goto :goto_1

    .line 2065303
    :cond_3
    iput-object v5, v1, LX/Dz6;->h:Landroid/location/Location;

    .line 2065304
    invoke-static {v1, v5}, LX/Dz6;->d(LX/Dz6;Landroid/location/Location;)V

    goto :goto_1

    .line 2065305
    :cond_4
    iget-object v4, v1, LX/Dz6;->h:Landroid/location/Location;

    invoke-static {v1, v4}, LX/Dz6;->d(LX/Dz6;Landroid/location/Location;)V

    goto :goto_3

    .line 2065306
    :cond_5
    iget-object v6, v4, LX/Dz4;->f:Landroid/location/Location;

    if-nez v6, :cond_7

    .line 2065307
    sget-boolean v7, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->i:Z

    move v7, v7

    .line 2065308
    if-eqz v7, :cond_8

    .line 2065309
    new-instance v11, Landroid/location/Location;

    const-string v12, "fake"

    invoke-direct {v11, v12}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2065310
    const-wide v13, 0x40445d89d6adf71fL    # 40.730769

    invoke-virtual {v11, v13, v14}, Landroid/location/Location;->setLatitude(D)V

    .line 2065311
    const-wide v13, -0x3fad808e2e2b8c76L    # -73.991322

    invoke-virtual {v11, v13, v14}, Landroid/location/Location;->setLongitude(D)V

    .line 2065312
    const/high16 v12, 0x447a0000    # 1000.0f

    invoke-virtual {v11, v12}, Landroid/location/Location;->setAccuracy(F)V

    .line 2065313
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-virtual {v11, v13, v14}, Landroid/location/Location;->setTime(J)V

    .line 2065314
    move-object v7, v11

    .line 2065315
    :cond_6
    :goto_4
    move-object v6, v7

    .line 2065316
    iput-object v6, v4, LX/Dz4;->f:Landroid/location/Location;

    .line 2065317
    :cond_7
    iget-object v6, v4, LX/Dz4;->f:Landroid/location/Location;

    goto/16 :goto_0

    .line 2065318
    :cond_8
    iget-object v7, v4, LX/Dz4;->b:LX/9jT;

    invoke-virtual {v7}, LX/9jT;->a()Landroid/location/Location;

    move-result-object v7

    .line 2065319
    if-nez v7, :cond_6

    .line 2065320
    iget-object v7, v4, LX/Dz4;->e:Landroid/location/Location;

    if-eqz v7, :cond_9

    .line 2065321
    iget-object v7, v4, LX/Dz4;->e:Landroid/location/Location;

    goto :goto_4

    .line 2065322
    :cond_9
    iget-object v7, v4, LX/Dz4;->c:LX/0y2;

    sget-wide v9, LX/Dz4;->a:J

    invoke-virtual {v7, v9, v10}, LX/0y2;->a(J)Lcom/facebook/location/ImmutableLocation;

    move-result-object v7

    .line 2065323
    if-eqz v7, :cond_a

    invoke-virtual {v7}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v7

    goto :goto_4

    :cond_a
    const/4 v7, 0x0

    goto :goto_4

    .line 2065324
    :cond_b
    sget-boolean v4, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->i:Z

    move v4, v4

    .line 2065325
    if-nez v4, :cond_c

    iget-object v4, v1, LX/Dz6;->c:LX/0y3;

    invoke-virtual {v4}, LX/0y3;->b()LX/1rv;

    move-result-object v4

    iget-object v4, v4, LX/1rv;->c:LX/0Rf;

    const-string v5, "network"

    invoke-virtual {v4, v5}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    :goto_5
    move v4, v4

    .line 2065326
    goto/16 :goto_2

    :cond_c
    const/4 v4, 0x0

    goto :goto_5
.end method

.method public final onStop()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x51caf3e4    # 1.089594E11f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2065327
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2065328
    invoke-static {v1}, LX/Dz6;->n(LX/Dz6;)V

    .line 2065329
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->n:LX/9kE;

    invoke-virtual {v1}, LX/9kE;->c()V

    .line 2065330
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->t:LX/9ju;

    .line 2065331
    invoke-static {v1}, LX/9ju;->f(LX/9ju;)V

    .line 2065332
    iget-object v2, v1, LX/9ju;->h:Landroid/os/Handler;

    iget-object v4, v1, LX/9ju;->l:Ljava/lang/Runnable;

    invoke-static {v2, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2065333
    invoke-static {v1}, LX/9ju;->i(LX/9ju;)V

    .line 2065334
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 2065335
    const/16 v1, 0x2b

    const v2, -0x3a2e56a2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2065336
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->M:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2065337
    const v0, 0x7f0d2539

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->M:Landroid/view/View;

    .line 2065338
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->M:Landroid/view/View;

    new-instance v1, LX/Dyf;

    invoke-direct {v1, p0}, LX/Dyf;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2065339
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2065340
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->M:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2065341
    :goto_0
    return-void

    .line 2065342
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->M:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 2065343
    const v0, 0x7f0d2535

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/checkin/ui/PlacesListContainer;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->J:Lcom/facebook/places/checkin/ui/PlacesListContainer;

    .line 2065344
    const v0, 0x7f0d2530

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2065345
    const v0, 0x7f0d252f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2065346
    const v0, 0x7f0d2538

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    .line 2065347
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->J:Lcom/facebook/places/checkin/ui/PlacesListContainer;

    .line 2065348
    iget-object v1, v0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    move-object v0, v1

    .line 2065349
    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    .line 2065350
    new-instance v1, LX/0zw;

    const v0, 0x7f0d2536

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2065351
    new-instance v2, LX/DyU;

    invoke-direct {v2, p0}, LX/DyU;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    move-object v2, v2

    .line 2065352
    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->W:LX/0zw;

    .line 2065353
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->S:LX/9jC;

    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065354
    iget-object p1, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v2, p1

    .line 2065355
    sget-object p1, LX/9jC;->Checkin:LX/9jC;

    if-ne v1, p1, :cond_7

    .line 2065356
    const p1, 0x7f0816bf

    .line 2065357
    :goto_0
    move v1, p1

    .line 2065358
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 2065359
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/DyV;

    invoke-direct {v1, p0}, LX/DyV;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2065360
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065361
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065362
    sget-object v2, LX/9jG;->SOCIAL_SEARCH_CONVERSION:LX/9jG;

    if-ne v1, v2, :cond_b

    .line 2065363
    const v2, 0x7f0816e0

    .line 2065364
    :goto_1
    move v1, v2

    .line 2065365
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->setHint(I)V

    .line 2065366
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->Y:LX/Dyh;

    .line 2065367
    iput-object v1, v0, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->b:LX/Dyh;

    .line 2065368
    invoke-direct {p0, p2}, Lcom/facebook/places/checkin/PlacePickerFragment;->b(Landroid/os/Bundle;)V

    .line 2065369
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/DyY;

    invoke-direct {v1, p0}, LX/DyY;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->a(LX/0fu;)V

    .line 2065370
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/DyZ;

    invoke-direct {v1, p0}, LX/DyZ;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 2065371
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2065372
    invoke-static {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->E(Lcom/facebook/places/checkin/PlacePickerFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2065373
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 2065374
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->b:LX/Dyy;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2065375
    invoke-virtual {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a()V

    .line 2065376
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065377
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v0, v1

    .line 2065378
    if-eqz v0, :cond_1

    .line 2065379
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->d:LX/Dyt;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065380
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065381
    iput-object v1, v0, LX/Dyt;->m:LX/9jG;

    .line 2065382
    :cond_1
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->K:Lcom/facebook/widget/listview/BetterListView;

    const-string v2, "tag_places_view"

    invoke-static {v1, v2, p0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(Landroid/view/View;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 2065383
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065384
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v0, v1

    .line 2065385
    if-eqz v0, :cond_2

    .line 2065386
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065387
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v1, v2

    .line 2065388
    iput-object v1, v0, LX/9j5;->f:LX/9jG;

    .line 2065389
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->c:LX/Dz6;

    .line 2065390
    iget-object v2, v1, LX/Dz6;->c:LX/0y3;

    invoke-virtual {v2}, LX/0y3;->b()LX/1rv;

    move-result-object v2

    iget-object v2, v2, LX/1rv;->b:LX/0Rf;

    invoke-virtual {v2}, LX/0Py;->asList()LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 2065391
    iput-object v1, v0, LX/9j5;->t:Ljava/util/List;

    .line 2065392
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065393
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v0, v1

    .line 2065394
    sget-object v1, LX/9jG;->CHECKIN:LX/9jG;

    if-eq v0, v1, :cond_3

    .line 2065395
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065396
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    move-object v0, v1

    .line 2065397
    if-eqz v0, :cond_3

    .line 2065398
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065399
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    move-object v0, v1

    .line 2065400
    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    .line 2065401
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065402
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->m:Ljava/lang/String;

    move-object v1, v2

    .line 2065403
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2065404
    :cond_3
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065405
    iget-object v3, v2, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->q:LX/9jG;

    move-object v2, v3

    .line 2065406
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2065407
    if-eqz v2, :cond_e

    invoke-virtual {v2}, LX/9jG;->isSocialSearchType()Z

    move-result v3

    if-eqz v3, :cond_e

    move v3, v4

    .line 2065408
    :goto_2
    new-instance v7, LX/9jI;

    invoke-direct {v7}, LX/9jI;-><init>()V

    if-nez v3, :cond_f

    move v6, v4

    .line 2065409
    :goto_3
    iput-boolean v6, v7, LX/9jI;->a:Z

    .line 2065410
    move-object v7, v7

    .line 2065411
    if-nez v3, :cond_10

    move v6, v4

    .line 2065412
    :goto_4
    iput-boolean v6, v7, LX/9jI;->b:Z

    .line 2065413
    move-object v6, v7

    .line 2065414
    if-nez v3, :cond_11

    .line 2065415
    :goto_5
    iput-boolean v4, v6, LX/9jI;->c:Z

    .line 2065416
    move-object v3, v6

    .line 2065417
    new-instance v4, LX/9jJ;

    invoke-direct {v4, v3}, LX/9jJ;-><init>(LX/9jI;)V

    move-object v3, v4

    .line 2065418
    move-object v4, v3

    .line 2065419
    sget-boolean v2, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->i:Z

    move v2, v2

    .line 2065420
    if-eqz v2, :cond_d

    .line 2065421
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->x:LX/DzL;

    const v3, 0x7f0d075d

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v7, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->z:LX/6Zb;

    move-object v6, p0

    invoke-virtual/range {v2 .. v7}, LX/DzL;->a(Landroid/view/View;LX/9jJ;Landroid/app/Activity;Lcom/facebook/places/checkin/PlacePickerFragment;LX/6Zb;)LX/DzK;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    .line 2065422
    :goto_6
    if-nez p2, :cond_5

    .line 2065423
    invoke-direct {p0}, Lcom/facebook/places/checkin/PlacePickerFragment;->D()V

    .line 2065424
    :goto_7
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065425
    iget-object v1, v0, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v0, v1

    .line 2065426
    if-eqz v0, :cond_4

    .line 2065427
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->G:Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    .line 2065428
    iget-object v2, v1, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->o:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-object v1, v2

    .line 2065429
    iget-object v1, v1, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->suggestionMechanism:Ljava/lang/String;

    .line 2065430
    iput-object v1, v0, LX/9j5;->k:Ljava/lang/String;

    .line 2065431
    :cond_4
    new-instance v0, LX/DyS;

    invoke-direct {v0, p0}, LX/DyS;-><init>(Lcom/facebook/places/checkin/PlacePickerFragment;)V

    .line 2065432
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->N:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 2065433
    return-void

    .line 2065434
    :cond_5
    const-string v0, "current_context_menu_place"

    invoke-static {p2, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2065435
    const-string v0, "crowdsourcing_edit_state"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Dyr;

    invoke-static {p0, v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->a$redex0(Lcom/facebook/places/checkin/PlacePickerFragment;LX/Dyr;)V

    .line 2065436
    const-string v0, "home_creation_logger_data"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2065437
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->p:LX/E0T;

    .line 2065438
    iput-object v0, v1, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2065439
    const-string v0, "search_text"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    .line 2065440
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->I:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2065441
    iget-object v0, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->L:Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/SoftKeyboardStateAwareEditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    .line 2065442
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_6

    .line 2065443
    invoke-virtual {p0, v0}, Lcom/facebook/places/checkin/PlacePickerFragment;->afterTextChanged(Landroid/text/Editable;)V

    .line 2065444
    :cond_6
    const-string v0, "analytics_bundle"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2065445
    iget-object v1, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->i:LX/9j5;

    invoke-virtual {v1, v0}, LX/9j5;->a(Landroid/os/Bundle;)V

    .line 2065446
    goto :goto_7

    .line 2065447
    :cond_7
    sget-object p1, LX/9jG;->SOCIAL_SEARCH_CONVERSION:LX/9jG;

    if-ne v2, p1, :cond_8

    .line 2065448
    const p1, 0x7f0816c1

    goto/16 :goto_0

    .line 2065449
    :cond_8
    sget-object p1, LX/9jG;->SOCIAL_SEARCH_COMMENT:LX/9jG;

    if-ne v2, p1, :cond_9

    .line 2065450
    const p1, 0x7f0816c2

    goto/16 :goto_0

    .line 2065451
    :cond_9
    sget-object p1, LX/9jG;->SOCIAL_SEARCH_ADD_PLACE_SEEKER:LX/9jG;

    if-ne v2, p1, :cond_a

    .line 2065452
    const p1, 0x7f0816c3

    goto/16 :goto_0

    .line 2065453
    :cond_a
    const p1, 0x7f0816c0

    goto/16 :goto_0

    .line 2065454
    :cond_b
    sget-object v2, LX/9jG;->SOCIAL_SEARCH_COMMENT:LX/9jG;

    if-ne v1, v2, :cond_c

    .line 2065455
    const v2, 0x7f0816e1

    goto/16 :goto_1

    .line 2065456
    :cond_c
    const v2, 0x7f0816df

    goto/16 :goto_1

    .line 2065457
    :cond_d
    iget-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->w:LX/DzH;

    const v3, 0x7f0d075d

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v7, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->z:LX/6Zb;

    move-object v6, p0

    invoke-virtual/range {v2 .. v7}, LX/DzH;->a(Landroid/view/View;LX/9jJ;Landroid/app/Activity;Lcom/facebook/places/checkin/PlacePickerFragment;LX/6Zb;)LX/DzG;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/places/checkin/PlacePickerFragment;->R:LX/DzG;

    goto/16 :goto_6

    :cond_e
    move v3, v5

    .line 2065458
    goto/16 :goto_2

    :cond_f
    move v6, v5

    .line 2065459
    goto/16 :goto_3

    :cond_10
    move v6, v5

    goto/16 :goto_4

    :cond_11
    move v4, v5

    goto/16 :goto_5
.end method
