.class public Lcom/facebook/places/checkin/ui/PlacesListContainer;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field public b:Lcom/facebook/widget/listview/BetterListView;

.field private c:Landroid/support/v4/widget/SwipeRefreshLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2066807
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2066808
    invoke-direct {p0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->f()V

    .line 2066809
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2066775
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2066776
    invoke-direct {p0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->f()V

    .line 2066777
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2066804
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2066805
    invoke-direct {p0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->f()V

    .line 2066806
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2066798
    const-class v0, Lcom/facebook/places/checkin/ui/PlacesListContainer;

    invoke-static {v0, p0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2066799
    const v0, 0x7f030f7e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2066800
    const v0, 0x7f0d2566

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    .line 2066801
    const v0, 0x7f0d054d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->a:Landroid/view/View;

    .line 2066802
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2066803
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2066810
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2066811
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2066812
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2066813
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2066814
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 2066815
    :cond_0
    return-void
.end method

.method public final a(LX/1PH;)V
    .locals 4

    .prologue
    .line 2066792
    const v0, 0x7f0d2565

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 2066793
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x7f0a00d1

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 2066794
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {p0, v0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->removeView(Landroid/view/View;)V

    .line 2066795
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-object v1, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->addView(Landroid/view/View;)V

    .line 2066796
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 2066797
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2066786
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2066787
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-eqz v0, :cond_0

    .line 2066788
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2066789
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 2066790
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2066791
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 2066779
    invoke-virtual {p0}, Lcom/facebook/places/checkin/ui/PlacesListContainer;->getListViewCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2066780
    :goto_0
    iget-object v4, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    move v2, v1

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2066781
    iget-object v2, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 2066782
    return-void

    :cond_0
    move v0, v1

    .line 2066783
    goto :goto_0

    :cond_1
    move v2, v3

    .line 2066784
    goto :goto_1

    :cond_2
    move v3, v1

    .line 2066785
    goto :goto_2
.end method

.method public getListView()Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 2066778
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    return-object v0
.end method

.method public getListViewCount()I
    .locals 1

    .prologue
    .line 2066774
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/PlacesListContainer;->b:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getCount()I

    move-result v0

    return v0
.end method
