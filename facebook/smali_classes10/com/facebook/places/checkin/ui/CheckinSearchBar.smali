.class public Lcom/facebook/places/checkin/ui/CheckinSearchBar;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2066762
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2066763
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2066764
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2066765
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 2066766
    const v0, 0x7f0d0a6b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/places/checkin/ui/CheckinSearchBar;->a:Landroid/widget/EditText;

    .line 2066767
    const v0, 0x7f0d094d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/checkin/ui/CheckinSearchBar;->b:Landroid/view/View;

    .line 2066768
    return-void
.end method


# virtual methods
.method public getClearSearchTextButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 2066769
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/CheckinSearchBar;->b:Landroid/view/View;

    return-object v0
.end method

.method public getSearchEditBox()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 2066770
    iget-object v0, p0, Lcom/facebook/places/checkin/ui/CheckinSearchBar;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7366215d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2066771
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishInflate()V

    .line 2066772
    invoke-direct {p0}, Lcom/facebook/places/checkin/ui/CheckinSearchBar;->a()V

    .line 2066773
    const/16 v1, 0x2d

    const v2, 0x4ae0168d    # 7342918.5f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
