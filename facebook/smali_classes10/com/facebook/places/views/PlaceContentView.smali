.class public Lcom/facebook/places/views/PlaceContentView;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""


# instance fields
.field private j:LX/1aX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2069541
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2069542
    invoke-direct {p0}, Lcom/facebook/places/views/PlaceContentView;->e()V

    .line 2069543
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2069538
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2069539
    invoke-direct {p0}, Lcom/facebook/places/views/PlaceContentView;->e()V

    .line 2069540
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2069535
    new-instance v0, LX/1Uo;

    invoke-virtual {p0}, Lcom/facebook/places/views/PlaceContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 2069536
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/places/views/PlaceContentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/views/PlaceContentView;->j:LX/1aX;

    .line 2069537
    return-void
.end method


# virtual methods
.method public getMinutiaeHolder()LX/1aX;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2069534
    iget-object v0, p0, Lcom/facebook/places/views/PlaceContentView;->j:LX/1aX;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x555f4d92

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2069544
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->onAttachedToWindow()V

    .line 2069545
    iget-object v1, p0, Lcom/facebook/places/views/PlaceContentView;->j:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->d()V

    .line 2069546
    const/16 v1, 0x2d

    const v2, -0x67fe8027

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x72049940

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2069531
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->onDetachedFromWindow()V

    .line 2069532
    iget-object v1, p0, Lcom/facebook/places/views/PlaceContentView;->j:LX/1aX;

    invoke-virtual {v1}, LX/1aX;->f()V

    .line 2069533
    const/16 v1, 0x2d

    const v2, -0x6a9c88d9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 2069528
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->onFinishTemporaryDetach()V

    .line 2069529
    iget-object v0, p0, Lcom/facebook/places/views/PlaceContentView;->j:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2069530
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2069516
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->onMeasure(II)V

    .line 2069517
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleLayout()Landroid/text/Layout;

    move-result-object v0

    .line 2069518
    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    if-lez v0, :cond_1

    .line 2069519
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2069520
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getMetaText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 2069521
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2069522
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2069523
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " \u00b7 "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2069524
    :goto_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2069525
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->onMeasure(II)V

    .line 2069526
    :cond_1
    return-void

    .line 2069527
    :cond_2
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onStartTemporaryDetach()V
    .locals 1

    .prologue
    .line 2069513
    invoke-super {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->onStartTemporaryDetach()V

    .line 2069514
    iget-object v0, p0, Lcom/facebook/places/views/PlaceContentView;->j:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->f()V

    .line 2069515
    return-void
.end method

.method public setMinutiaeIconController(LX/1aZ;)V
    .locals 1

    .prologue
    .line 2069509
    iget-object v0, p0, Lcom/facebook/places/views/PlaceContentView;->j:LX/1aX;

    invoke-virtual {v0, p1}, LX/1aX;->a(LX/1aZ;)V

    .line 2069510
    invoke-virtual {p0}, Lcom/facebook/places/views/PlaceContentView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2069511
    iget-object v0, p0, Lcom/facebook/places/views/PlaceContentView;->j:LX/1aX;

    invoke-virtual {v0}, LX/1aX;->d()V

    .line 2069512
    :cond_0
    return-void
.end method
