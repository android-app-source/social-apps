.class public Lcom/facebook/places/create/PlaceCreationCityPickerActivity;
.super Lcom/facebook/places/create/BasePlaceCreationActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2067585
    invoke-direct {p0}, Lcom/facebook/places/create/BasePlaceCreationActivity;-><init>()V

    .line 2067586
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2067587
    const v0, 0x7f081737

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/PlaceCreationCityPickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2067588
    invoke-super {p0, p1}, Lcom/facebook/places/create/BasePlaceCreationActivity;->b(Landroid/os/Bundle;)V

    .line 2067589
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2067590
    invoke-virtual {p0}, Lcom/facebook/places/create/PlaceCreationCityPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "current_location"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    const/4 v2, 0x1

    new-instance v3, LX/Dzv;

    invoke-direct {v3}, LX/Dzv;-><init>()V

    sget-object v5, LX/CdF;->PLACE_CREATION_LOGGER:LX/CdF;

    invoke-virtual {p0}, Lcom/facebook/places/create/PlaceCreationCityPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v6, "crowdsourcing_context"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    move v4, v1

    invoke-static/range {v0 .. v6}, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->a(Landroid/location/Location;ZZLX/Ccr;ZLX/CdF;Landroid/os/Parcelable;)Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    move-result-object v0

    .line 2067591
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2067592
    :cond_0
    return-void
.end method
