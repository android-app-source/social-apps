.class public Lcom/facebook/places/create/BellerophonLoggerData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/create/BellerophonLoggerData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2066936
    new-instance v0, LX/DzQ;

    invoke-direct {v0}, LX/DzQ;-><init>()V

    sput-object v0, Lcom/facebook/places/create/BellerophonLoggerData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2066937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2066938
    const-class v0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    iput-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->a:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    .line 2066939
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->b:Ljava/lang/String;

    .line 2066940
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->c:Ljava/lang/String;

    .line 2066941
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->d:Ljava/util/List;

    .line 2066942
    iget-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->d:Ljava/util/List;

    const-class v1, Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 2066943
    return-void
.end method

.method public constructor <init>(Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;)V
    .locals 1

    .prologue
    .line 2066944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2066945
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2066946
    iput-object p1, p0, Lcom/facebook/places/create/BellerophonLoggerData;->a:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    .line 2066947
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->b:Ljava/lang/String;

    .line 2066948
    return-void
.end method


# virtual methods
.method public final d()J
    .locals 4

    .prologue
    .line 2066949
    iget-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->a:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    .line 2066950
    iget-wide v2, v0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;->c:J

    move-wide v0, v2

    .line 2066951
    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2066952
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2066953
    iget-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->d:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->d:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2066954
    iget-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->a:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2066955
    iget-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2066956
    iget-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2066957
    iget-object v0, p0, Lcom/facebook/places/create/BellerophonLoggerData;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2066958
    return-void
.end method
