.class public Lcom/facebook/places/create/PlaceCreationDupActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public p:LX/Dzy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/DzP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/94Z;

.field private s:Lcom/facebook/widget/listview/BetterListView;

.field private t:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field

.field private u:Lcom/facebook/places/create/BellerophonLoggerData;

.field private final v:LX/63W;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2067662
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2067663
    new-instance v0, LX/Dzw;

    invoke-direct {v0, p0}, LX/Dzw;-><init>(Lcom/facebook/places/create/PlaceCreationDupActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->v:LX/63W;

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2067599
    const v0, 0x7f081716

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/places/create/PlaceCreationDupActivity;LX/Dzy;LX/DzP;)V
    .locals 0

    .prologue
    .line 2067661
    iput-object p1, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->p:LX/Dzy;

    iput-object p2, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->q:LX/DzP;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/places/create/PlaceCreationDupActivity;

    new-instance p1, LX/Dzy;

    const-class v0, Landroid/content/Context;

    invoke-interface {v1, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    invoke-static {v1}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v3

    check-cast v3, Ljava/util/Locale;

    invoke-direct {p1, v0, v2, v3}, LX/Dzy;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Ljava/util/Locale;)V

    move-object v0, p1

    check-cast v0, LX/Dzy;

    invoke-static {v1}, LX/DzP;->b(LX/0QB;)LX/DzP;

    move-result-object v1

    check-cast v1, LX/DzP;

    invoke-static {p0, v0, v1}, Lcom/facebook/places/create/PlaceCreationDupActivity;->a(Lcom/facebook/places/create/PlaceCreationDupActivity;LX/Dzy;LX/DzP;)V

    return-void
.end method

.method public static b(Lcom/facebook/places/create/PlaceCreationDupActivity;)V
    .locals 3

    .prologue
    .line 2067654
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->q:LX/DzP;

    .line 2067655
    iget-object v1, v0, LX/DzP;->b:LX/0Zb;

    const-string v2, "bellerophon_skip"

    invoke-static {v0, v2}, LX/DzP;->b(LX/DzP;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2067656
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2067657
    const-string v1, "continue_place_creation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2067658
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->setResult(ILandroid/content/Intent;)V

    .line 2067659
    invoke-virtual {p0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->finish()V

    .line 2067660
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2067615
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2067616
    invoke-virtual {p0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "possible_dup_places"

    invoke-static {v0, v1}, LX/4By;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->t:Ljava/util/ArrayList;

    .line 2067617
    invoke-static {p0, p0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2067618
    const v0, 0x7f030f68

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->setContentView(I)V

    .line 2067619
    invoke-virtual {p0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2067620
    invoke-virtual {p0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "bellerophon_logger_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/BellerophonLoggerData;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->u:Lcom/facebook/places/create/BellerophonLoggerData;

    .line 2067621
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->q:LX/DzP;

    iget-object v1, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->u:Lcom/facebook/places/create/BellerophonLoggerData;

    .line 2067622
    iput-object v1, v0, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    .line 2067623
    const v0, 0x7f0d0a7f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/94V;

    .line 2067624
    new-instance v1, LX/Dzx;

    invoke-direct {v1, p0}, LX/Dzx;-><init>(Lcom/facebook/places/create/PlaceCreationDupActivity;)V

    invoke-virtual {v0, v1}, LX/94V;->setOnBackPressedListener(LX/63J;)V

    .line 2067625
    invoke-direct {p0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->a()Ljava/lang/String;

    move-result-object v1

    .line 2067626
    new-instance v2, LX/94Z;

    new-instance v3, LX/94Y;

    invoke-direct {v3}, LX/94Y;-><init>()V

    .line 2067627
    iput-object v1, v3, LX/94Y;->a:Ljava/lang/String;

    .line 2067628
    move-object v1, v3

    .line 2067629
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v3

    .line 2067630
    iput-object v3, v1, LX/94Y;->d:LX/94c;

    .line 2067631
    move-object v1, v1

    .line 2067632
    invoke-virtual {v1}, LX/94Y;->a()LX/94X;

    move-result-object v1

    invoke-direct {v2, v0, v1}, LX/94Z;-><init>(LX/94V;LX/94X;)V

    iput-object v2, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->r:LX/94Z;

    .line 2067633
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->r:LX/94Z;

    iget-object v1, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->r:LX/94Z;

    .line 2067634
    iget-object v2, v1, LX/94Z;->b:LX/94X;

    move-object v1, v2

    .line 2067635
    invoke-virtual {v1}, LX/94X;->a()LX/94Y;

    move-result-object v1

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v3, 0x7f081709

    invoke-virtual {p0, v3}, Lcom/facebook/places/create/PlaceCreationDupActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2067636
    iput-object v3, v2, LX/108;->g:Ljava/lang/String;

    .line 2067637
    move-object v2, v2

    .line 2067638
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 2067639
    iput-object v2, v1, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2067640
    move-object v1, v1

    .line 2067641
    iget-object v2, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->v:LX/63W;

    .line 2067642
    iput-object v2, v1, LX/94Y;->c:LX/63W;

    .line 2067643
    move-object v1, v1

    .line 2067644
    invoke-virtual {v1}, LX/94Y;->a()LX/94X;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94Z;->a(LX/94X;)V

    .line 2067645
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->s:Lcom/facebook/widget/listview/BetterListView;

    .line 2067646
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->s:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->p:LX/Dzy;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2067647
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->s:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2067648
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2067649
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->p:LX/Dzy;

    iget-object v1, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->t:Ljava/util/ArrayList;

    .line 2067650
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    iput-object v2, v0, LX/Dzy;->d:Ljava/util/List;

    .line 2067651
    const v2, 0x48fcf5ec

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2067652
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->p:LX/Dzy;

    const v1, 0x4f69e27d

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2067653
    return-void
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2067611
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->q:LX/DzP;

    .line 2067612
    iget-object v1, v0, LX/DzP;->b:LX/0Zb;

    const-string v2, "bellerophon_cancel"

    invoke-static {v0, v2}, LX/DzP;->b(LX/DzP;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2067613
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2067614
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2067600
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->s:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067601
    iget-object v1, p0, Lcom/facebook/places/create/PlaceCreationDupActivity;->q:LX/DzP;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    .line 2067602
    const-string v3, "bellerophon_select"

    invoke-static {v1, v3}, LX/DzP;->b(LX/DzP;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2067603
    const-string p1, "selected_place_id"

    invoke-virtual {v3, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2067604
    iget-object p1, v1, LX/DzP;->b:LX/0Zb;

    invoke-interface {p1, v3}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2067605
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2067606
    const-string v2, "continue_place_creation"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2067607
    const-string v2, "select_existing_place"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2067608
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/places/create/PlaceCreationDupActivity;->setResult(ILandroid/content/Intent;)V

    .line 2067609
    invoke-virtual {p0}, Lcom/facebook/places/create/PlaceCreationDupActivity;->finish()V

    .line 2067610
    return-void
.end method
