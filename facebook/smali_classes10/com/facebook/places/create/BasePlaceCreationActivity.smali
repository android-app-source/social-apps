.class public abstract Lcom/facebook/places/create/BasePlaceCreationActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/1ZF;


# instance fields
.field private p:LX/94V;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2066897
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public final a(LX/63W;)V
    .locals 1

    .prologue
    .line 2066895
    iget-object v0, p0, Lcom/facebook/places/create/BasePlaceCreationActivity;->p:LX/94V;

    invoke-virtual {v0, p1}, LX/94V;->setOnToolbarButtonListener(LX/63W;)V

    .line 2066896
    return-void
.end method

.method public final a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 2066893
    iget-object v0, p0, Lcom/facebook/places/create/BasePlaceCreationActivity;->p:LX/94V;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94V;->setButtonSpecs(Ljava/util/List;)V

    .line 2066894
    return-void
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2066891
    iget-object v0, p0, Lcom/facebook/places/create/BasePlaceCreationActivity;->p:LX/94V;

    invoke-virtual {v0, p1}, LX/94V;->setTitle(Ljava/lang/String;)V

    .line 2066892
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2066876
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 2066877
    const v0, 0x7f030f69

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/BasePlaceCreationActivity;->setContentView(I)V

    .line 2066878
    const v0, 0x7f0d0a7f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/94V;

    iput-object v0, p0, Lcom/facebook/places/create/BasePlaceCreationActivity;->p:LX/94V;

    .line 2066879
    iget-object v0, p0, Lcom/facebook/places/create/BasePlaceCreationActivity;->p:LX/94V;

    new-instance v1, LX/DzO;

    invoke-direct {v1, p0}, LX/DzO;-><init>(Lcom/facebook/places/create/BasePlaceCreationActivity;)V

    invoke-virtual {v0, v1}, LX/94V;->setOnBackPressedListener(LX/63J;)V

    .line 2066880
    new-instance v0, LX/94Z;

    iget-object v1, p0, Lcom/facebook/places/create/BasePlaceCreationActivity;->p:LX/94V;

    new-instance v2, LX/94Y;

    invoke-direct {v2}, LX/94Y;-><init>()V

    sget-object v3, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2066881
    iput-object v3, v2, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2066882
    move-object v2, v2

    .line 2066883
    invoke-virtual {p0}, Lcom/facebook/places/create/BasePlaceCreationActivity;->a()Ljava/lang/String;

    move-result-object v3

    .line 2066884
    iput-object v3, v2, LX/94Y;->a:Ljava/lang/String;

    .line 2066885
    move-object v2, v2

    .line 2066886
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v3

    .line 2066887
    iput-object v3, v2, LX/94Y;->d:LX/94c;

    .line 2066888
    move-object v2, v2

    .line 2066889
    invoke-virtual {v2}, LX/94Y;->a()LX/94X;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/94Z;-><init>(LX/94V;LX/94X;)V

    .line 2066890
    return-void
.end method

.method public final b(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 2

    .prologue
    .line 2066874
    iget-object v0, p0, Lcom/facebook/places/create/BasePlaceCreationActivity;->p:LX/94V;

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94V;->setButtonSpecs(Ljava/util/List;)V

    .line 2066875
    return-void
.end method

.method public final f()Landroid/view/View;
    .locals 1

    .prologue
    .line 2066873
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k_(Z)V
    .locals 0

    .prologue
    .line 2066872
    return-void
.end method

.method public final lH_()V
    .locals 2

    .prologue
    .line 2066868
    iget-object v0, p0, Lcom/facebook/places/create/BasePlaceCreationActivity;->p:LX/94V;

    .line 2066869
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2066870
    invoke-virtual {v0, v1}, LX/94V;->setButtonSpecs(Ljava/util/List;)V

    .line 2066871
    return-void
.end method

.method public setCustomTitle(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2066867
    return-void
.end method

.method public final x_(I)V
    .locals 1

    .prologue
    .line 2066865
    iget-object v0, p0, Lcom/facebook/places/create/BasePlaceCreationActivity;->p:LX/94V;

    invoke-virtual {v0, p1}, LX/94V;->setTitle(I)V

    .line 2066866
    return-void
.end method
