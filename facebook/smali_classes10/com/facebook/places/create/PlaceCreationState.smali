.class public Lcom/facebook/places/create/PlaceCreationState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/create/PlaceCreationState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/ipc/model/PageTopic;

.field public final c:Landroid/location/Location;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public final h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/places/create/network/PlacePinAppId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2067914
    new-instance v0, LX/E09;

    invoke-direct {v0}, LX/E09;-><init>()V

    sput-object v0, Lcom/facebook/places/create/PlaceCreationState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/E0A;)V
    .locals 1

    .prologue
    .line 2067903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067904
    iget-object v0, p1, LX/E0A;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->a:Ljava/lang/String;

    .line 2067905
    iget-object v0, p1, LX/E0A;->b:Lcom/facebook/ipc/model/PageTopic;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->b:Lcom/facebook/ipc/model/PageTopic;

    .line 2067906
    iget-object v0, p1, LX/E0A;->c:Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->c:Landroid/location/Location;

    .line 2067907
    iget-object v0, p1, LX/E0A;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->d:Ljava/lang/String;

    .line 2067908
    iget-object v0, p1, LX/E0A;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067909
    iget-object v0, p1, LX/E0A;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->f:Ljava/lang/String;

    .line 2067910
    iget-boolean v0, p1, LX/E0A;->g:Z

    iput-boolean v0, p0, Lcom/facebook/places/create/PlaceCreationState;->g:Z

    .line 2067911
    iget-object v0, p1, LX/E0A;->h:LX/0am;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->h:LX/0am;

    .line 2067912
    iget-object v0, p1, LX/E0A;->i:LX/0am;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->i:LX/0am;

    .line 2067913
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2067891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2067892
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->a:Ljava/lang/String;

    .line 2067893
    const-class v0, Lcom/facebook/ipc/model/PageTopic;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->b:Lcom/facebook/ipc/model/PageTopic;

    .line 2067894
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->c:Landroid/location/Location;

    .line 2067895
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->d:Ljava/lang/String;

    .line 2067896
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067897
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->f:Ljava/lang/String;

    .line 2067898
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/places/create/PlaceCreationState;->g:Z

    .line 2067899
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->h:LX/0am;

    .line 2067900
    const-class v0, Lcom/facebook/places/create/network/PlacePinAppId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->i:LX/0am;

    .line 2067901
    return-void

    .line 2067902
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2067879
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2067880
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2067881
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->b:Lcom/facebook/ipc/model/PageTopic;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2067882
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->c:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2067883
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2067884
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2067885
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2067886
    iget-boolean v0, p0, Lcom/facebook/places/create/PlaceCreationState;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2067887
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->h:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2067888
    iget-object v0, p0, Lcom/facebook/places/create/PlaceCreationState;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2067889
    return-void

    .line 2067890
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
