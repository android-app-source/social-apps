.class public Lcom/facebook/places/create/network/PlaceCreationParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/create/network/PlaceCreationParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/places/create/network/PlacePinAppId;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public final k:Z

.field public final l:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Lcom/facebook/photos/base/media/PhotoItem;

.field private final o:Landroid/location/Location;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2069330
    new-instance v0, LX/E12;

    invoke-direct {v0}, LX/E12;-><init>()V

    sput-object v0, Lcom/facebook/places/create/network/PlaceCreationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2069312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2069313
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->a:Ljava/lang/String;

    .line 2069314
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->o:Landroid/location/Location;

    .line 2069315
    const-class v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->n:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2069316
    invoke-static {p1}, Lcom/facebook/places/create/network/PlaceCreationParams;->a(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->c:LX/0Px;

    .line 2069317
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->d:Ljava/lang/String;

    .line 2069318
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->e:J

    .line 2069319
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->f:Ljava/lang/String;

    .line 2069320
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->g:Ljava/lang/String;

    .line 2069321
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->h:Ljava/lang/String;

    .line 2069322
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->i:Ljava/lang/String;

    .line 2069323
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->j:Ljava/lang/String;

    .line 2069324
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->k:Z

    .line 2069325
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->l:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2069326
    invoke-static {p1}, Lcom/facebook/places/create/network/PlaceCreationParams;->a(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->m:LX/0Px;

    .line 2069327
    const-class v0, Lcom/facebook/places/create/network/PlacePinAppId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->b:LX/0am;

    .line 2069328
    return-void

    .line 2069329
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/location/Location;LX/0am;Lcom/facebook/photos/base/media/PhotoItem;Ljava/util/List;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/location/Location;",
            "LX/0am",
            "<",
            "Lcom/facebook/places/create/network/PlacePinAppId;",
            ">;",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2069291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2069292
    iput-object p1, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->a:Ljava/lang/String;

    .line 2069293
    new-instance v1, Landroid/location/Location;

    invoke-direct {v1, p2}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v1, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->o:Landroid/location/Location;

    .line 2069294
    iput-object p3, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->b:LX/0am;

    .line 2069295
    iput-object p4, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->n:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2069296
    if-eqz p5, :cond_0

    .line 2069297
    invoke-static {p5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->c:LX/0Px;

    .line 2069298
    :goto_0
    iput-object p6, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->d:Ljava/lang/String;

    .line 2069299
    iput-wide p7, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->e:J

    .line 2069300
    iput-object p9, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->f:Ljava/lang/String;

    .line 2069301
    iput-object p10, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->g:Ljava/lang/String;

    .line 2069302
    iput-object p11, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->h:Ljava/lang/String;

    .line 2069303
    iput-object p12, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->i:Ljava/lang/String;

    .line 2069304
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->j:Ljava/lang/String;

    .line 2069305
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->k:Z

    .line 2069306
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->l:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2069307
    if-eqz p16, :cond_1

    .line 2069308
    invoke-static/range {p16 .. p16}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->m:LX/0Px;

    .line 2069309
    :goto_1
    return-void

    .line 2069310
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->c:LX/0Px;

    goto :goto_0

    .line 2069311
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->m:LX/0Px;

    goto :goto_1
.end method

.method private static a(Landroid/os/Parcel;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2069290
    const-class v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/location/Location;LX/0am;Lcom/facebook/photos/base/media/PhotoItem;Ljava/util/List;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/util/List;)Lcom/facebook/places/create/network/PlaceCreationParams;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/location/Location;",
            "LX/0am",
            "<",
            "Lcom/facebook/places/create/network/PlacePinAppId;",
            ">;",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lcom/facebook/places/create/network/PlaceCreationParams;"
        }
    .end annotation

    .prologue
    .line 2069331
    new-instance v1, Lcom/facebook/places/create/network/PlaceCreationParams;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-wide/from16 v8, p6

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move/from16 v15, p13

    move-object/from16 v16, p14

    move-object/from16 v17, p15

    invoke-direct/range {v1 .. v17}, Lcom/facebook/places/create/network/PlaceCreationParams;-><init>(Ljava/lang/String;Landroid/location/Location;LX/0am;Lcom/facebook/photos/base/media/PhotoItem;Ljava/util/List;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/util/List;)V

    return-object v1
.end method

.method public static b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 2

    .prologue
    .line 2069287
    new-instance v0, LX/8QP;

    invoke-direct {v0}, LX/8QP;-><init>()V

    .line 2069288
    const-string v1, "{\"value\":\"EVERYONE\"}"

    invoke-virtual {v0, v1}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    .line 2069289
    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 2

    .prologue
    .line 2069266
    new-instance v0, Landroid/location/Location;

    iget-object v1, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->o:Landroid/location/Location;

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2069286
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2069267
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2069268
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->o:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2069269
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2069270
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->c:LX/0Px;

    .line 2069271
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2069272
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2069273
    iget-wide v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2069274
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2069275
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2069276
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2069277
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2069278
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2069279
    iget-boolean v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2069280
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->l:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2069281
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->m:LX/0Px;

    .line 2069282
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 2069283
    iget-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationParams;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2069284
    return-void

    .line 2069285
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
