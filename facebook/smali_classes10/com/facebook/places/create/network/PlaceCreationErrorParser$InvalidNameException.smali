.class public final Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;
.super Ljava/lang/Exception;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/create/network/PlaceCreationErrorParser_InvalidNameExceptionDeserializer;
.end annotation


# instance fields
.field public final reason:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reason"
    .end annotation
.end field

.field public final suggestion:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggestion"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2069057
    const-class v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser_InvalidNameExceptionDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2069058
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 2069059
    sget-object v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->OTHER:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;->reason:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    .line 2069060
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameException;->suggestion:Ljava/lang/String;

    .line 2069061
    return-void
.end method
