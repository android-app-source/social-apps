.class public final enum Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/create/network/PlaceCreationErrorParser_InvalidNameReasonDeserializer;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

.field public static final enum BLACKLIST:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

.field public static final enum INVALID_CAPS:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

.field public static final enum INVALID_CHARS:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

.field public static final enum OTHER:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;


# instance fields
.field private final reason:Ljava/lang/String;


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2069077
    const-class v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser_InvalidNameReasonDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2069072
    new-instance v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    const-string v1, "INVALID_CHARS"

    const-string v2, "invalid_chars"

    invoke-direct {v0, v1, v3, v2}, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->INVALID_CHARS:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    .line 2069073
    new-instance v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    const-string v1, "BLACKLIST"

    const-string v2, "blacklist"

    invoke-direct {v0, v1, v4, v2}, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->BLACKLIST:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    .line 2069074
    new-instance v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    const-string v1, "INVALID_CAPS"

    const-string v2, "caps"

    invoke-direct {v0, v1, v5, v2}, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->INVALID_CAPS:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    .line 2069075
    new-instance v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    const-string v1, "OTHER"

    const-string v2, "other"

    invoke-direct {v0, v1, v6, v2}, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->OTHER:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    .line 2069076
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    sget-object v1, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->INVALID_CHARS:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->BLACKLIST:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->INVALID_CAPS:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->OTHER:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    aput-object v1, v0, v6

    sput-object v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->$VALUES:[Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2069069
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2069070
    iput-object p3, p0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->reason:Ljava/lang/String;

    .line 2069071
    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;
    .locals 5
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonCreator;
    .end annotation

    .prologue
    .line 2069062
    invoke-static {}, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->values()[Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2069063
    iget-object v4, v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->reason:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2069064
    :goto_1
    return-object v0

    .line 2069065
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2069066
    :cond_1
    sget-object v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->OTHER:Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;
    .locals 1

    .prologue
    .line 2069068
    const-class v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    return-object v0
.end method

.method public static values()[Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;
    .locals 1

    .prologue
    .line 2069067
    sget-object v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;->$VALUES:[Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/places/create/network/PlaceCreationErrorParser$InvalidNameReason;

    return-object v0
.end method
