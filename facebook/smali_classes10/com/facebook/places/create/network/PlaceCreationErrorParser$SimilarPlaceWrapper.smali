.class public final Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceWrapper;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/create/network/PlaceCreationErrorParser_SimilarPlaceWrapperDeserializer;
.end annotation


# instance fields
.field public final similarPlaces:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "similar_places"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2069086
    const-class v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser_SimilarPlaceWrapperDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2069087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2069088
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceWrapper;->similarPlaces:Ljava/util/List;

    .line 2069089
    return-void
.end method
