.class public final Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;
.super Ljava/lang/Exception;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/places/create/network/PlaceCreationErrorParser_SimilarPlaceExceptionDeserializer;
.end annotation


# instance fields
.field public final id:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2069081
    const-class v0, Lcom/facebook/places/create/network/PlaceCreationErrorParser_SimilarPlaceExceptionDeserializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2069082
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 2069083
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;->id:J

    .line 2069084
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/places/create/network/PlaceCreationErrorParser$SimilarPlaceException;->name:Ljava/lang/String;

    .line 2069085
    return-void
.end method
