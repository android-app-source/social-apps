.class public final enum Lcom/facebook/places/create/network/PlacePinAppId;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/facebook/places/create/network/PlacePinAppId;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/facebook/places/create/network/PlacePinAppId;

.field public static final enum CITY_CENTER:Lcom/facebook/places/create/network/PlacePinAppId;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/create/network/PlacePinAppId;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum GEOCODED_ADDRESS:Lcom/facebook/places/create/network/PlacePinAppId;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2069364
    new-instance v0, Lcom/facebook/places/create/network/PlacePinAppId;

    const-string v1, "CITY_CENTER"

    invoke-direct {v0, v1, v2}, Lcom/facebook/places/create/network/PlacePinAppId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/places/create/network/PlacePinAppId;->CITY_CENTER:Lcom/facebook/places/create/network/PlacePinAppId;

    .line 2069365
    new-instance v0, Lcom/facebook/places/create/network/PlacePinAppId;

    const-string v1, "GEOCODED_ADDRESS"

    invoke-direct {v0, v1, v3}, Lcom/facebook/places/create/network/PlacePinAppId;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/facebook/places/create/network/PlacePinAppId;->GEOCODED_ADDRESS:Lcom/facebook/places/create/network/PlacePinAppId;

    .line 2069366
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/facebook/places/create/network/PlacePinAppId;

    sget-object v1, Lcom/facebook/places/create/network/PlacePinAppId;->CITY_CENTER:Lcom/facebook/places/create/network/PlacePinAppId;

    aput-object v1, v0, v2

    sget-object v1, Lcom/facebook/places/create/network/PlacePinAppId;->GEOCODED_ADDRESS:Lcom/facebook/places/create/network/PlacePinAppId;

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/places/create/network/PlacePinAppId;->$VALUES:[Lcom/facebook/places/create/network/PlacePinAppId;

    .line 2069367
    new-instance v0, LX/E15;

    invoke-direct {v0}, LX/E15;-><init>()V

    sput-object v0, Lcom/facebook/places/create/network/PlacePinAppId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2069363
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/facebook/places/create/network/PlacePinAppId;
    .locals 1

    .prologue
    .line 2069358
    const-class v0, Lcom/facebook/places/create/network/PlacePinAppId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/network/PlacePinAppId;

    return-object v0
.end method

.method public static values()[Lcom/facebook/places/create/network/PlacePinAppId;
    .locals 1

    .prologue
    .line 2069362
    sget-object v0, Lcom/facebook/places/create/network/PlacePinAppId;->$VALUES:[Lcom/facebook/places/create/network/PlacePinAppId;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/facebook/places/create/network/PlacePinAppId;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2069361
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2069359
    invoke-virtual {p0}, Lcom/facebook/places/create/network/PlacePinAppId;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2069360
    return-void
.end method
