.class public Lcom/facebook/places/create/NewPlaceCreationFormFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private final A:LX/Dzl;

.field private final B:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

.field public a:LX/Dzt;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/E14;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/E07;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Dzr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/96B;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/Cd4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

.field public l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/4BY;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/Dzp;

.field private o:Lcom/facebook/ipc/model/PageTopic;

.field private p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field private q:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lcom/facebook/places/create/PlaceCreationState;

.field public s:Landroid/location/Location;

.field public t:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<+",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0am",
            "<",
            "Landroid/location/Location;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public v:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field public w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

.field private x:Z

.field private y:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/places/create/network/PlacePinAppId;",
            ">;"
        }
    .end annotation
.end field

.field private z:Landroid/location/Location;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2067391
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 2067392
    new-instance v0, LX/Dzl;

    invoke-direct {v0, p0}, LX/Dzl;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->A:LX/Dzl;

    .line 2067393
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "new_place_creation"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->B:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 2067394
    return-void
.end method

.method public static A(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 1

    .prologue
    .line 2067395
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    if-eqz v0, :cond_0

    .line 2067396
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2067397
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    .line 2067398
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/create/NewPlaceCreationFormFragment;JLjava/lang/String;LX/96A;)V
    .locals 5

    .prologue
    .line 2067399
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    invoke-virtual {v0, v1, p4, p1, p2}, LX/96B;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;J)V

    .line 2067400
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2067401
    const-string v1, "selected_existing_place"

    new-instance v2, LX/5m9;

    invoke-direct {v2}, LX/5m9;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 2067402
    iput-object v3, v2, LX/5m9;->f:Ljava/lang/String;

    .line 2067403
    move-object v2, v2

    .line 2067404
    iput-object p3, v2, LX/5m9;->h:Ljava/lang/String;

    .line 2067405
    move-object v2, v2

    .line 2067406
    invoke-virtual {v2}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2067407
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2067408
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2067409
    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/create/NewPlaceCreationFormFragment;LX/0am;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2067410
    iput-object p1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->q:LX/0am;

    .line 2067411
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067412
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2067413
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2067414
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v0, LX/Dzp;->i:Landroid/widget/ImageView;

    invoke-virtual {p1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 2067415
    :goto_0
    return-void

    .line 2067416
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2067417
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2067418
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V
    .locals 2

    .prologue
    .line 2067419
    iput-object p1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067420
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2067421
    return-void
.end method

.method public static b(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2067478
    if-eqz p0, :cond_0

    invoke-static {p0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 6

    .prologue
    const/16 v2, 0xd

    const/4 v1, 0x1

    .line 2067422
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2067423
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->y:LX/0am;

    .line 2067424
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->s:Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->z:Landroid/location/Location;

    move v0, v1

    move v1, v2

    .line 2067425
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v2, v2, LX/Dzp;->g:Lcom/facebook/maps/FbStaticMapView;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f020e90

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_1
    const/high16 v3, 0x3f000000    # 0.5f

    const v4, 0x3f6e147b    # 0.93f

    invoke-virtual {v2, v0, v3, v4}, LX/3BP;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 2067426
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->g:Lcom/facebook/maps/FbStaticMapView;

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->B:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v2}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a()Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->z:Landroid/location/Location;

    invoke-virtual {v2, v3}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/location/Location;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2067427
    return-void

    .line 2067428
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2067429
    sget-object v0, Lcom/facebook/places/create/network/PlacePinAppId;->GEOCODED_ADDRESS:Lcom/facebook/places/create/network/PlacePinAppId;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->y:LX/0am;

    .line 2067430
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->v:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->z:Landroid/location/Location;

    move v0, v1

    move v1, v2

    goto :goto_0

    .line 2067431
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2067432
    if-eqz v0, :cond_3

    .line 2067433
    invoke-direct {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->s()V

    .line 2067434
    :cond_3
    const/16 v1, 0xa

    .line 2067435
    const/4 v0, 0x0

    .line 2067436
    sget-object v2, Lcom/facebook/places/create/network/PlacePinAppId;->CITY_CENTER:Lcom/facebook/places/create/network/PlacePinAppId;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->y:LX/0am;

    .line 2067437
    new-instance v2, Landroid/location/Location;

    const-string v3, ""

    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->z:Landroid/location/Location;

    .line 2067438
    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2067439
    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->z:Landroid/location/Location;

    iget-object v3, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLatitude(D)V

    .line 2067440
    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->z:Landroid/location/Location;

    iget-object v3, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/location/Location;->setLongitude(D)V

    goto/16 :goto_0

    .line 2067441
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static q(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2067442
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067443
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2067444
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u:LX/0am;

    .line 2067445
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->g:LX/1Ck;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2067446
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->v:LX/0am;

    .line 2067447
    return-void
.end method

.method private s()V
    .locals 10

    .prologue
    .line 2067448
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067449
    :goto_0
    return-void

    .line 2067450
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->e:LX/Dzr;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v4, v4, LX/Dzp;->c:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 2067451
    new-instance v4, LX/4FL;

    invoke-direct {v4}, LX/4FL;-><init>()V

    new-instance v5, LX/4FO;

    invoke-direct {v5}, LX/4FO;-><init>()V

    .line 2067452
    const-string v6, "name"

    invoke-virtual {v5, v6, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067453
    move-object v5, v5

    .line 2067454
    const-string v6, "street"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2067455
    move-object v4, v4

    .line 2067456
    new-instance v5, LX/4FM;

    invoke-direct {v5}, LX/4FM;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    .line 2067457
    const-string v7, "id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2067458
    move-object v5, v5

    .line 2067459
    const-string v6, "city"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2067460
    move-object v4, v4

    .line 2067461
    iget-object v5, v0, LX/Dzr;->a:LX/0tX;

    .line 2067462
    new-instance v6, LX/E0B;

    invoke-direct {v6}, LX/E0B;-><init>()V

    move-object v6, v6

    .line 2067463
    const-string v7, "addresses"

    new-instance v8, LX/4FN;

    invoke-direct {v8}, LX/4FN;-><init>()V

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v8, v4}, LX/4FN;->a(Ljava/util/List;)LX/4FN;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v4

    check-cast v4, LX/E0B;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 2067464
    new-instance v5, LX/Dzq;

    invoke-direct {v5, v0}, LX/Dzq;-><init>(LX/Dzr;)V

    iget-object v6, v0, LX/Dzr;->b:LX/0TD;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2067465
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u:LX/0am;

    .line 2067466
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->v:LX/0am;

    .line 2067467
    new-instance v1, LX/DzT;

    invoke-direct {v1, p0}, LX/DzT;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->f:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0
.end method

.method public static t(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 3

    .prologue
    .line 2067468
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2067469
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f081749

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2067470
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 2067471
    move-object v2, v1

    .line 2067472
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->b(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 2067473
    :goto_0
    iput-boolean v1, v2, LX/108;->d:Z

    .line 2067474
    move-object v1, v2

    .line 2067475
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2067476
    return-void

    .line 2067477
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static u(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)Lcom/facebook/places/create/PlaceCreationState;
    .locals 8

    .prologue
    .line 2067214
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->r:Lcom/facebook/places/create/PlaceCreationState;

    .line 2067215
    new-instance v2, LX/E0A;

    iget-object v3, v0, Lcom/facebook/places/create/PlaceCreationState;->a:Ljava/lang/String;

    iget-object v4, v0, Lcom/facebook/places/create/PlaceCreationState;->b:Lcom/facebook/ipc/model/PageTopic;

    iget-object v5, v0, Lcom/facebook/places/create/PlaceCreationState;->c:Landroid/location/Location;

    iget-object v6, v0, Lcom/facebook/places/create/PlaceCreationState;->i:LX/0am;

    iget-object v7, v0, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-direct/range {v2 .. v7}, LX/E0A;-><init>(Ljava/lang/String;Lcom/facebook/ipc/model/PageTopic;Landroid/location/Location;LX/0am;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    iget-object v3, v0, Lcom/facebook/places/create/PlaceCreationState;->d:Ljava/lang/String;

    .line 2067216
    iput-object v3, v2, LX/E0A;->d:Ljava/lang/String;

    .line 2067217
    move-object v2, v2

    .line 2067218
    iget-object v3, v0, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v2, v3}, LX/E0A;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/E0A;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/places/create/PlaceCreationState;->f:Ljava/lang/String;

    .line 2067219
    iput-object v3, v2, LX/E0A;->f:Ljava/lang/String;

    .line 2067220
    move-object v2, v2

    .line 2067221
    move-object v0, v2

    .line 2067222
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2067223
    iput-object v1, v0, LX/E0A;->a:Ljava/lang/String;

    .line 2067224
    move-object v0, v0

    .line 2067225
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2067226
    iput-object v1, v0, LX/E0A;->d:Ljava/lang/String;

    .line 2067227
    move-object v0, v0

    .line 2067228
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2067229
    iput-object v1, v0, LX/E0A;->f:Ljava/lang/String;

    .line 2067230
    move-object v0, v0

    .line 2067231
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->f:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 2067232
    iput-boolean v1, v0, LX/E0A;->g:Z

    .line 2067233
    move-object v0, v0

    .line 2067234
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->o:Lcom/facebook/ipc/model/PageTopic;

    .line 2067235
    iput-object v1, v0, LX/E0A;->b:Lcom/facebook/ipc/model/PageTopic;

    .line 2067236
    move-object v0, v0

    .line 2067237
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0, v1}, LX/E0A;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/E0A;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->z:Landroid/location/Location;

    .line 2067238
    iput-object v1, v0, LX/E0A;->c:Landroid/location/Location;

    .line 2067239
    move-object v0, v0

    .line 2067240
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->y:LX/0am;

    .line 2067241
    iput-object v1, v0, LX/E0A;->i:LX/0am;

    .line 2067242
    move-object v0, v0

    .line 2067243
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->q:LX/0am;

    .line 2067244
    iput-object v1, v0, LX/E0A;->h:LX/0am;

    .line 2067245
    move-object v0, v0

    .line 2067246
    invoke-virtual {v0}, LX/E0A;->a()Lcom/facebook/places/create/PlaceCreationState;

    move-result-object v0

    return-object v0
.end method

.method private w()Z
    .locals 1

    .prologue
    .line 2067390
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 7

    .prologue
    .line 2067365
    invoke-direct {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067366
    :goto_0
    return-void

    .line 2067367
    :cond_0
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->z(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067368
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)Lcom/facebook/places/create/PlaceCreationState;

    move-result-object v0

    .line 2067369
    new-instance v1, LX/E01;

    invoke-direct {v1}, LX/E01;-><init>()V

    .line 2067370
    iget-object v2, v0, Lcom/facebook/places/create/PlaceCreationState;->a:Ljava/lang/String;

    .line 2067371
    iput-object v2, v1, LX/E01;->a:Ljava/lang/String;

    .line 2067372
    iget-object v2, v0, Lcom/facebook/places/create/PlaceCreationState;->b:Lcom/facebook/ipc/model/PageTopic;

    iget-wide v2, v2, Lcom/facebook/ipc/model/PageTopic;->id:J

    long-to-int v2, v2

    .line 2067373
    iput v2, v1, LX/E01;->c:I

    .line 2067374
    iget-object v2, v0, Lcom/facebook/places/create/PlaceCreationState;->c:Landroid/location/Location;

    .line 2067375
    iput-object v2, v1, LX/E01;->b:Landroid/location/Location;

    .line 2067376
    iget-object v2, v0, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2067377
    iput-object v2, v1, LX/E01;->e:Ljava/lang/String;

    .line 2067378
    iget-object v0, v0, Lcom/facebook/places/create/PlaceCreationState;->d:Ljava/lang/String;

    .line 2067379
    iput-object v0, v1, LX/E01;->d:Ljava/lang/String;

    .line 2067380
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->a:LX/Dzt;

    new-instance v2, LX/DzU;

    invoke-direct {v2, p0}, LX/DzU;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    iget-object v3, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->A:LX/Dzl;

    iget-object v4, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->k:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    .line 2067381
    iget-object v5, v0, LX/Dzt;->d:LX/0Uh;

    const/16 v6, 0x2f1

    const/4 p0, 0x0

    invoke-virtual {v5, v6, p0}, LX/0Uh;->a(IZ)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2067382
    invoke-virtual {v3}, LX/Dzl;->b()V

    .line 2067383
    :goto_1
    goto :goto_0

    .line 2067384
    :cond_1
    iget-object v5, v0, LX/Dzt;->b:LX/DzP;

    new-instance v6, Lcom/facebook/places/create/BellerophonLoggerData;

    invoke-direct {v6, v4}, Lcom/facebook/places/create/BellerophonLoggerData;-><init>(Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;)V

    .line 2067385
    iput-object v6, v5, LX/DzP;->a:Lcom/facebook/places/create/BellerophonLoggerData;

    .line 2067386
    iget-object v5, v0, LX/Dzt;->b:LX/DzP;

    .line 2067387
    iget-object v6, v5, LX/DzP;->b:LX/0Zb;

    const-string p0, "bellerophon_start"

    invoke-static {v5, p0}, LX/DzP;->b(LX/DzP;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v6, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2067388
    iget-object v5, v0, LX/Dzt;->c:LX/E00;

    invoke-virtual {v5}, LX/E00;->a()V

    .line 2067389
    iget-object v5, v0, LX/Dzt;->c:LX/E00;

    new-instance v6, LX/Dzs;

    invoke-direct {v6, v0, v3, v2}, LX/Dzs;-><init>(LX/Dzt;LX/Dzl;LX/DzU;)V

    invoke-virtual {v5, v1, v6}, LX/E00;->a(LX/E01;LX/0TF;)V

    goto :goto_1
.end method

.method public static y(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 18

    .prologue
    .line 2067360
    invoke-static/range {p0 .. p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)Lcom/facebook/places/create/PlaceCreationState;

    move-result-object v11

    .line 2067361
    iget-object v2, v11, Lcom/facebook/places/create/PlaceCreationState;->a:Ljava/lang/String;

    iget-object v3, v11, Lcom/facebook/places/create/PlaceCreationState;->c:Landroid/location/Location;

    iget-object v4, v11, Lcom/facebook/places/create/PlaceCreationState;->i:LX/0am;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->q:LX/0am;

    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v6, LX/74k;

    invoke-direct {v6}, LX/74k;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->q:LX/0am;

    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/74k;->a(Ljava/lang/String;)LX/74k;

    move-result-object v5

    const-string v6, "image/jpeg"

    invoke-virtual {v5, v6}, LX/74k;->c(Ljava/lang/String;)LX/74k;

    move-result-object v5

    invoke-virtual {v5}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v5

    :goto_0
    iget-object v6, v11, Lcom/facebook/places/create/PlaceCreationState;->b:Lcom/facebook/ipc/model/PageTopic;

    iget-wide v6, v6, Lcom/facebook/ipc/model/PageTopic;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    iget-object v7, v11, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v7}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v11, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v8}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    iget-object v10, v11, Lcom/facebook/places/create/PlaceCreationState;->d:Ljava/lang/String;

    iget-object v11, v11, Lcom/facebook/places/create/PlaceCreationState;->f:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {}, Lcom/facebook/places/create/network/PlaceCreationParams;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->l:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-static/range {v2 .. v17}, Lcom/facebook/places/create/network/PlaceCreationParams;->a(Ljava/lang/String;Landroid/location/Location;LX/0am;Lcom/facebook/photos/base/media/PhotoItem;Ljava/util/List;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/util/List;)Lcom/facebook/places/create/network/PlaceCreationParams;

    move-result-object v2

    .line 2067362
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->c:LX/E14;

    new-instance v4, LX/Dzk;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, LX/Dzk;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v3, v2, v4}, LX/E14;->a(Lcom/facebook/places/create/network/PlaceCreationParams;LX/0TF;)V

    .line 2067363
    return-void

    .line 2067364
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static z(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2067351
    invoke-direct {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067352
    :goto_0
    return-void

    .line 2067353
    :cond_0
    new-instance v0, LX/4BY;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    .line 2067354
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    .line 2067355
    iput v2, v0, LX/4BY;->d:I

    .line 2067356
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    const v1, 0x7f08173e

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2067357
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/4BY;->a(Z)V

    .line 2067358
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    invoke-virtual {v0, v2}, LX/4BY;->setCancelable(Z)V

    .line 2067359
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->m:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->show()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 2067332
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 2067333
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v3, p0

    check-cast v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    new-instance v8, LX/Dzt;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/DzP;->b(LX/0QB;)LX/DzP;

    move-result-object v5

    check-cast v5, LX/DzP;

    new-instance v9, LX/E00;

    invoke-static {v0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v6

    check-cast v6, LX/9kE;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-direct {v9, v6, v7}, LX/E00;-><init>(LX/9kE;LX/0tX;)V

    move-object v6, v9

    check-cast v6, LX/E00;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-direct {v8, v4, v5, v6, v7}, LX/Dzt;-><init>(Landroid/content/Context;LX/DzP;LX/E00;LX/0Uh;)V

    move-object v4, v8

    check-cast v4, LX/Dzt;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/E14;->b(LX/0QB;)LX/E14;

    move-result-object v6

    check-cast v6, LX/E14;

    invoke-static {v0}, LX/E07;->a(LX/0QB;)LX/E07;

    move-result-object v7

    check-cast v7, LX/E07;

    new-instance v10, LX/Dzr;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v9

    check-cast v9, LX/0TD;

    invoke-direct {v10, v8, v9}, LX/Dzr;-><init>(LX/0tX;LX/0TD;)V

    move-object v8, v10

    check-cast v8, LX/Dzr;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v11

    check-cast v11, LX/0TD;

    invoke-static {v0}, LX/96B;->a(LX/0QB;)LX/96B;

    move-result-object v12

    check-cast v12, LX/96B;

    invoke-static {v0}, LX/Cd4;->b(LX/0QB;)LX/Cd4;

    move-result-object v0

    check-cast v0, LX/Cd4;

    iput-object v4, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->a:LX/Dzt;

    iput-object v5, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v6, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->c:LX/E14;

    iput-object v7, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->d:LX/E07;

    iput-object v8, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->e:LX/Dzr;

    iput-object v9, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->f:Ljava/util/concurrent/Executor;

    iput-object v10, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->g:LX/1Ck;

    iput-object v11, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->h:LX/0TD;

    iput-object v12, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iput-object v0, v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->j:LX/Cd4;

    .line 2067334
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2067335
    const-string v2, "place_picker_session_data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->k:Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    .line 2067336
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2067337
    const-string v2, "initial_place_state"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/PlaceCreationState;

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->r:Lcom/facebook/places/create/PlaceCreationState;

    .line 2067338
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2067339
    const-string v2, "user_current_location"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->s:Landroid/location/Location;

    .line 2067340
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 2067341
    const-string v2, "crowdsourcing_context"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 2067342
    if-eqz p1, :cond_0

    const-string v0, "duplicate_override_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2067343
    :goto_0
    if-eqz v0, :cond_1

    const-string v0, "duplicate_override_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    invoke-static {v0}, LX/1YA;->a([J)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->l:Ljava/util/List;

    .line 2067344
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u:LX/0am;

    .line 2067345
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->v:LX/0am;

    .line 2067346
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->t:LX/0am;

    .line 2067347
    iput-boolean v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->x:Z

    .line 2067348
    return-void

    :cond_0
    move v0, v1

    .line 2067349
    goto :goto_0

    .line 2067350
    :cond_1
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 2067304
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2067305
    packed-switch p1, :pswitch_data_0

    .line 2067306
    :goto_0
    return-void

    .line 2067307
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->A:LX/Dzl;

    invoke-static {v0, p2, p3}, LX/Dzt;->a(LX/Dzl;ILandroid/content/Intent;)V

    goto :goto_0

    .line 2067308
    :pswitch_1
    if-ne p2, v4, :cond_0

    .line 2067309
    const-string v0, "category"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/PageTopic;

    .line 2067310
    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->o:Lcom/facebook/ipc/model/PageTopic;

    .line 2067311
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, v0, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2067312
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v3, LX/969;->PLACE_CATEGORY:LX/969;

    invoke-virtual {v1, v2, v3}, LX/96B;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/969;)V

    .line 2067313
    iget-wide v0, v0, Lcom/facebook/ipc/model/PageTopic;->id:J

    const-wide v2, 0xb36f1da84d60L

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 2067314
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v2, LX/96A;->HOME_CREATION:LX/96A;

    invoke-virtual {v0, v1, v2}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    .line 2067315
    const-string v0, "create_home_from_place_creation"

    invoke-virtual {p3, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2067316
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v4, p3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2067317
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 2067318
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v2, LX/96A;->FORM:LX/96A;

    invoke-virtual {v0, v1, v2}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    goto :goto_0

    .line 2067319
    :pswitch_2
    if-ne p2, v4, :cond_1

    .line 2067320
    const-string v0, "picked_city"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067321
    const-string v1, "is_currently_there"

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2067322
    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v2, v2, LX/Dzp;->f:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2067323
    invoke-static {p0, v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->a$redex0(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2067324
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->q(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067325
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067326
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v2, LX/969;->PLACE_CITY:LX/969;

    invoke-virtual {v0, v1, v2}, LX/96B;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/969;)V

    .line 2067327
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v2, LX/96A;->FORM:LX/96A;

    invoke-virtual {v0, v1, v2}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    goto/16 :goto_0

    .line 2067328
    :pswitch_3
    if-ne p2, v4, :cond_2

    .line 2067329
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2067330
    new-instance v1, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v2, 0x7f081700

    invoke-virtual {v1, v2}, LX/0ju;->b(I)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    const v2, 0x7f08001a

    new-instance v3, LX/DzW;

    invoke-direct {v3, p0, v0}, LX/DzW;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/DzV;

    invoke-direct {v2, p0}, LX/DzV;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2067331
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->i:LX/96B;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v2, LX/96A;->FORM:LX/96A;

    invoke-virtual {v0, v1, v2}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x68a329d    # 5.19842E-35f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2067303
    const v1, 0x7f030bf2

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6561bded

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5090793

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2067293
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 2067294
    invoke-direct {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w()Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->x:Z

    .line 2067295
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->A(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067296
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->a:LX/Dzt;

    .line 2067297
    iget-object v2, v1, LX/Dzt;->c:LX/E00;

    invoke-virtual {v2}, LX/E00;->a()V

    .line 2067298
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->c:LX/E14;

    .line 2067299
    iget-object v2, v1, LX/E14;->a:LX/9kE;

    invoke-virtual {v2}, LX/9kE;->c()V

    .line 2067300
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->q(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067301
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2067302
    const/16 v1, 0x2b

    const v2, -0x60a203b0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x225313e3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2067284
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 2067285
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 2067286
    const v2, 0x7f081735

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 2067287
    new-instance v2, LX/DzX;

    invoke-direct {v2, p0}, LX/DzX;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-interface {v0, v2}, LX/1ZF;->a(LX/63W;)V

    .line 2067288
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->t(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067289
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067290
    iget-boolean v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->x:Z

    if-eqz v0, :cond_0

    .line 2067291
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->x(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067292
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x1db4f4d4

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2067278
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2067279
    const-string v0, "place"

    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->u(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)Lcom/facebook/places/create/PlaceCreationState;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2067280
    const-string v0, "duplicate_override_ids"

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->l:Ljava/util/List;

    invoke-static {v1}, LX/1YA;->a(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 2067281
    const-string v1, "paused_create_request"

    iget-boolean v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->x:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2067282
    return-void

    .line 2067283
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2067247
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2067248
    if-eqz p2, :cond_1

    const-string v0, "place"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "place"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/PlaceCreationState;

    .line 2067249
    :goto_0
    new-instance v1, LX/Dzp;

    invoke-direct {v1, p0, p1}, LX/Dzp;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Landroid/view/View;)V

    iput-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    .line 2067250
    iget-object v1, v0, Lcom/facebook/places/create/PlaceCreationState;->b:Lcom/facebook/ipc/model/PageTopic;

    iput-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->o:Lcom/facebook/ipc/model/PageTopic;

    .line 2067251
    iget-object v1, v0, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2067252
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->a:Landroid/widget/EditText;

    iget-object v2, v0, Lcom/facebook/places/create/PlaceCreationState;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2067253
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->b:Landroid/widget/EditText;

    iget-object v2, v0, Lcom/facebook/places/create/PlaceCreationState;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2067254
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->c:Landroid/widget/EditText;

    iget-object v2, v0, Lcom/facebook/places/create/PlaceCreationState;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2067255
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->d:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, v0, Lcom/facebook/places/create/PlaceCreationState;->b:Lcom/facebook/ipc/model/PageTopic;

    iget-object v2, v2, Lcom/facebook/ipc/model/PageTopic;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2067256
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, v0, Lcom/facebook/places/create/PlaceCreationState;->e:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2067257
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v1, v1, LX/Dzp;->f:Landroid/widget/CheckBox;

    iget-boolean v2, v0, Lcom/facebook/places/create/PlaceCreationState;->g:Z

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2067258
    iget-object v0, v0, Lcom/facebook/places/create/PlaceCreationState;->h:LX/0am;

    invoke-static {p0, v0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->a$redex0(Lcom/facebook/places/create/NewPlaceCreationFormFragment;LX/0am;)V

    .line 2067259
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->d:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/DzY;

    invoke-direct {v1, p0}, LX/DzY;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2067260
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->e:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/DzZ;

    invoke-direct {v1, p0}, LX/DzZ;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2067261
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->f:Landroid/widget/CheckBox;

    new-instance v1, LX/Dza;

    invoke-direct {v1, p0}, LX/Dza;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2067262
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->a:Landroid/widget/EditText;

    new-instance v1, LX/Dzn;

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v2, v2, LX/Dzp;->a:Landroid/widget/EditText;

    sget-object p1, LX/969;->PAGE_NAME:LX/969;

    invoke-direct {v1, p0, v2, p1}, LX/Dzn;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Landroid/widget/EditText;LX/969;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2067263
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->a:Landroid/widget/EditText;

    new-instance v1, LX/Dzo;

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v2, v2, LX/Dzp;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-direct {v1, p0, v2}, LX/Dzo;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2067264
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->b:Landroid/widget/EditText;

    new-instance v1, LX/Dzj;

    invoke-direct {v1, p0}, LX/Dzj;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2067265
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->b:Landroid/widget/EditText;

    new-instance v1, LX/Dzn;

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v2, v2, LX/Dzp;->b:Landroid/widget/EditText;

    sget-object p1, LX/969;->PLACE_STREET_ADDRESS:LX/969;

    invoke-direct {v1, p0, v2, p1}, LX/Dzn;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Landroid/widget/EditText;LX/969;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2067266
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->c:Landroid/widget/EditText;

    new-instance v1, LX/Dzj;

    invoke-direct {v1, p0}, LX/Dzj;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2067267
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->c:Landroid/widget/EditText;

    new-instance v1, LX/Dzn;

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v2, v2, LX/Dzp;->c:Landroid/widget/EditText;

    sget-object p1, LX/969;->PLACE_ZIP_CODE:LX/969;

    invoke-direct {v1, p0, v2, p1}, LX/Dzn;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Landroid/widget/EditText;LX/969;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2067268
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->h:Landroid/widget/ImageView;

    new-instance v1, LX/Dzb;

    invoke-direct {v1, p0}, LX/Dzb;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2067269
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->n:LX/Dzp;

    iget-object v0, v0, LX/Dzp;->j:Landroid/view/View;

    new-instance v1, LX/Dzd;

    invoke-direct {v1, p0}, LX/Dzd;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2067270
    const v0, 0x7f0d1da5

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    .line 2067271
    const v1, 0x7f0d1da4

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, LX/Dze;

    invoke-direct {v2, p0, v0}, LX/Dze;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2067272
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->g:LX/1Ck;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, LX/Dzf;

    invoke-direct {v2, p0}, LX/Dzf;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    new-instance p1, LX/Dzg;

    invoke-direct {p1, p0}, LX/Dzg;-><init>(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    invoke-virtual {v0, v1, v2, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2067273
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->p(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067274
    if-eqz p2, :cond_0

    const-string v0, "paused_create_request"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2067275
    invoke-static {p0}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->x(Lcom/facebook/places/create/NewPlaceCreationFormFragment;)V

    .line 2067276
    :cond_0
    return-void

    .line 2067277
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationFormFragment;->r:Lcom/facebook/places/create/PlaceCreationState;

    goto/16 :goto_0
.end method
