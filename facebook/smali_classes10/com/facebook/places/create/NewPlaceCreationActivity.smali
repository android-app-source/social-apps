.class public Lcom/facebook/places/create/NewPlaceCreationActivity;
.super Lcom/facebook/places/create/BasePlaceCreationActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/96B;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/ipc/model/PageTopic;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2067039
    invoke-direct {p0}, Lcom/facebook/places/create/BasePlaceCreationActivity;-><init>()V

    .line 2067040
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/places/create/NewPlaceCreationActivity;

    invoke-static {v0}, LX/96B;->a(LX/0QB;)LX/96B;

    move-result-object v0

    check-cast v0, LX/96B;

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->p:LX/96B;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/create/NewPlaceCreationActivity;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;LX/96A;)V
    .locals 6

    .prologue
    .line 2067044
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2067045
    const-string v1, "selected_existing_place"

    invoke-static {v0, v1, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2067046
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->p:LX/96B;

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, p2, v4, v5}, LX/96B;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;J)V

    .line 2067047
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->setResult(ILandroid/content/Intent;)V

    .line 2067048
    invoke-virtual {p0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->finish()V

    .line 2067049
    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/create/NewPlaceCreationActivity;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Z)V
    .locals 3

    .prologue
    .line 2067041
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->p:LX/96B;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v2, LX/96A;->FORM:LX/96A;

    invoke-virtual {v0, v1, v2}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    .line 2067042
    invoke-direct {p0, p1, p2}, Lcom/facebook/places/create/NewPlaceCreationActivity;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Z)V

    .line 2067043
    return-void
.end method

.method private b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Z)V
    .locals 7

    .prologue
    .line 2067012
    invoke-virtual {p0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/location/Location;

    .line 2067013
    if-eqz p2, :cond_0

    .line 2067014
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    move-object v3, v6

    .line 2067015
    :goto_0
    new-instance v0, LX/E0A;

    invoke-virtual {p0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.SUBJECT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->q:LX/0am;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/model/PageTopic;

    invoke-static {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/E0A;-><init>(Ljava/lang/String;Lcom/facebook/ipc/model/PageTopic;Landroid/location/Location;LX/0am;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 2067016
    iput-boolean p2, v0, LX/E0A;->g:Z

    .line 2067017
    move-object v0, v0

    .line 2067018
    invoke-virtual {v0}, LX/E0A;->a()Lcom/facebook/places/create/PlaceCreationState;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    invoke-virtual {p0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "place_picker_session_data"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/checkin/analytics/PlacePickerSessionData;

    .line 2067019
    new-instance v3, Lcom/facebook/places/create/NewPlaceCreationFormFragment;

    invoke-direct {v3}, Lcom/facebook/places/create/NewPlaceCreationFormFragment;-><init>()V

    .line 2067020
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2067021
    const-string v5, "initial_place_state"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2067022
    const-string v5, "user_current_location"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2067023
    const-string v5, "crowdsourcing_context"

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2067024
    const-string v5, "place_picker_session_data"

    invoke-virtual {v4, v5, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2067025
    invoke-virtual {v3, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2067026
    move-object v0, v3

    .line 2067027
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v1

    const v2, 0x7f0400b9

    const v3, 0x7f040033

    const v4, 0x7f0400b9

    const v5, 0x7f040033

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2067028
    return-void

    .line 2067029
    :cond_0
    new-instance v3, Landroid/location/Location;

    const-string v0, ""

    invoke-direct {v3, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2067030
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2067031
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    .line 2067032
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 2067033
    :cond_1
    sget-object v0, Lcom/facebook/places/create/network/PlacePinAppId;->CITY_CENTER:Lcom/facebook/places/create/network/PlacePinAppId;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    goto/16 :goto_0
.end method

.method public static b$redex0(Lcom/facebook/places/create/NewPlaceCreationActivity;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 2067034
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->p:LX/96B;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v3, LX/96A;->CITY_PICKER:LX/96A;

    invoke-virtual {v0, v1, v3}, LX/96B;->b(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;)V

    .line 2067035
    invoke-virtual {p0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->q:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/model/PageTopic;

    iget-wide v6, v1, Lcom/facebook/ipc/model/PageTopic;->id:J

    const-wide v8, 0xcc2417d7e5b1L

    cmp-long v1, v6, v8

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    new-instance v3, LX/DzS;

    invoke-direct {v3}, LX/DzS;-><init>()V

    sget-object v5, LX/CdF;->PLACE_CREATION_LOGGER:LX/CdF;

    iget-object v6, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    move v4, v2

    invoke-static/range {v0 .. v6}, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->a(Landroid/location/Location;ZZLX/Ccr;ZLX/CdF;Landroid/os/Parcelable;)Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    move-result-object v0

    .line 2067036
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v1

    const v2, 0x7f0400b9

    const v3, 0x7f040033

    const v4, 0x7f0400b9

    const v5, 0x7f040033

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2067037
    return-void

    :cond_0
    move v1, v4

    .line 2067038
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2067011
    const v0, 0x7f081735

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2067000
    invoke-super {p0, p1}, Lcom/facebook/places/create/BasePlaceCreationActivity;->b(Landroid/os/Bundle;)V

    .line 2067001
    invoke-static {p0, p0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2067002
    new-instance v0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    const-string v1, "android_place_picker_add_button"

    const-string v2, "android_place_creation_v2_with_form"

    invoke-direct {v0, v1, v2}, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 2067003
    if-eqz p1, :cond_1

    const-string v0, "state_category"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->q:LX/0am;

    .line 2067004
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2067005
    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->p:LX/96B;

    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v2, LX/96A;->CATEGORY_PICKER:LX/96A;

    .line 2067006
    iget-object v3, v0, LX/96B;->a:LX/0Zb;

    const-string v4, "endpoint_impression"

    invoke-static {v0, v1, v4}, LX/96B;->a(LX/96B;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "starting_view_name"

    iget-object p1, v2, LX/96A;->logValue:Ljava/lang/String;

    invoke-virtual {v4, v5, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2067007
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    new-instance v1, LX/DzR;

    invoke-direct {v1}, LX/DzR;-><init>()V

    const/4 v2, 0x1

    sget-object v3, LX/9kb;->PLACE_CREATION_LOGGER:LX/9kb;

    iget-object v4, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a(LX/0am;LX/9kF;ZLX/9kb;Landroid/os/Parcelable;)Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    move-result-object v0

    .line 2067008
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2067009
    :cond_0
    return-void

    .line 2067010
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2066985
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/places/create/BasePlaceCreationActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2066986
    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    .line 2066987
    :cond_0
    :goto_0
    return-void

    .line 2066988
    :cond_1
    const-string v1, "create_home_from_place_creation"

    invoke-virtual {p3, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2066989
    packed-switch p1, :pswitch_data_0

    .line 2066990
    :goto_1
    if-eqz v0, :cond_0

    .line 2066991
    invoke-virtual {p0, p2, p3}, Lcom/facebook/places/create/NewPlaceCreationActivity;->setResult(ILandroid/content/Intent;)V

    .line 2066992
    invoke-virtual {p0}, Lcom/facebook/places/create/NewPlaceCreationActivity;->finish()V

    goto :goto_0

    .line 2066993
    :pswitch_0
    const-string v0, "extra_place"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2066994
    const-string v0, "extra_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2066995
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->p:LX/96B;

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/96B;->c(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;J)V

    .line 2066996
    :cond_2
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 2066997
    :cond_3
    const-string v0, "selected_existing_place"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2066998
    const-string v0, "selected_existing_place"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2066999
    iget-object v1, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->p:LX/96B;

    iget-object v2, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->r:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    sget-object v3, LX/96A;->DEDUPER:LX/96A;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, LX/96B;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;LX/96A;J)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2066982
    invoke-super {p0, p1}, Lcom/facebook/places/create/BasePlaceCreationActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2066983
    const-string v1, "state_category"

    iget-object v0, p0, Lcom/facebook/places/create/NewPlaceCreationActivity;->q:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2066984
    return-void
.end method
