.class public final Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2067960
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2067961
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2068010
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2068011
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 2067976
    if-nez p1, :cond_0

    .line 2067977
    :goto_0
    return v0

    .line 2067978
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2067979
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2067980
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 2067981
    const v2, -0x2c0b8ec2

    const/4 v5, 0x0

    .line 2067982
    if-nez v1, :cond_1

    move v3, v5

    .line 2067983
    :goto_1
    move v1, v3

    .line 2067984
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2067985
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2067986
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2067987
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 2067988
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 2067989
    const v3, 0x232e7699

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 2067990
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2067991
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 2067992
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2067993
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2067994
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2067995
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2067996
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2067997
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2067998
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 2067999
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2068000
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 2068001
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2068002
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 2068003
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 2068004
    :goto_2
    if-ge v5, p1, :cond_3

    .line 2068005
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 2068006
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 2068007
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2068008
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 2068009
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x2c0b8ec2 -> :sswitch_1
        -0x1703498c -> :sswitch_0
        0x232e7699 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2067975
    new-instance v0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    .line 2067962
    sparse-switch p2, :sswitch_data_0

    .line 2067963
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2067964
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2067965
    const v1, -0x2c0b8ec2

    .line 2067966
    if-eqz v0, :cond_0

    .line 2067967
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 2067968
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 2067969
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2067970
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2067971
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2067972
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 2067973
    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2067974
    const v1, 0x232e7699

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2c0b8ec2 -> :sswitch_2
        -0x1703498c -> :sswitch_0
        0x232e7699 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2067925
    if-eqz p1, :cond_0

    .line 2067926
    invoke-static {p0, p1, p2}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;

    move-result-object v1

    .line 2067927
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;

    .line 2067928
    if-eq v0, v1, :cond_0

    .line 2067929
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2067930
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2067959
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2067957
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2067958
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2068012
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2068013
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2068014
    :cond_0
    iput-object p1, p0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->a:LX/15i;

    .line 2068015
    iput p2, p0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->b:I

    .line 2068016
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2067956
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2067955
    new-instance v0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2067952
    iget v0, p0, LX/1vt;->c:I

    .line 2067953
    move v0, v0

    .line 2067954
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2067949
    iget v0, p0, LX/1vt;->c:I

    .line 2067950
    move v0, v0

    .line 2067951
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2067946
    iget v0, p0, LX/1vt;->b:I

    .line 2067947
    move v0, v0

    .line 2067948
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2067943
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2067944
    move-object v0, v0

    .line 2067945
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2067934
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2067935
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2067936
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2067937
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2067938
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2067939
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2067940
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2067941
    invoke-static {v3, v9, v2}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/create/graphql/GeocodeAddressModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2067942
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2067931
    iget v0, p0, LX/1vt;->c:I

    .line 2067932
    move v0, v0

    .line 2067933
    return v0
.end method
