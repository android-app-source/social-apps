.class public final Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2068095
    const-class v0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;

    new-instance v1, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2068096
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2068094
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2068017
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2068018
    const/4 v2, 0x0

    .line 2068019
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 2068020
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2068021
    :goto_0
    move v1, v2

    .line 2068022
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2068023
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2068024
    new-instance v1, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;

    invoke-direct {v1}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;-><init>()V

    .line 2068025
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2068026
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2068027
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2068028
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2068029
    :cond_0
    return-object v1

    .line 2068030
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2068031
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2068032
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2068033
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2068034
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 2068035
    const-string v4, "geocode_address_data"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2068036
    const/4 v3, 0x0

    .line 2068037
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2068038
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2068039
    :goto_2
    move v1, v3

    .line 2068040
    goto :goto_1

    .line 2068041
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2068042
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2068043
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 2068044
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2068045
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 2068046
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2068047
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2068048
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2068049
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2068050
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2068051
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 2068052
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2068053
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2068054
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_f

    .line 2068055
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2068056
    :goto_5
    move v4, v5

    .line 2068057
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2068058
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2068059
    goto :goto_3

    .line 2068060
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 2068061
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 2068062
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 2068063
    :cond_a
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_d

    .line 2068064
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2068065
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2068066
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_a

    if-eqz v9, :cond_a

    .line 2068067
    const-string v10, "geo_score"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 2068068
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v6

    goto :goto_6

    .line 2068069
    :cond_b
    const-string v10, "node"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 2068070
    const/4 v9, 0x0

    .line 2068071
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_14

    .line 2068072
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2068073
    :goto_7
    move v7, v9

    .line 2068074
    goto :goto_6

    .line 2068075
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 2068076
    :cond_d
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 2068077
    if-eqz v4, :cond_e

    .line 2068078
    invoke-virtual {v0, v5, v8, v5}, LX/186;->a(III)V

    .line 2068079
    :cond_e
    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 2068080
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_f
    move v4, v5

    move v7, v5

    move v8, v5

    goto :goto_6

    .line 2068081
    :cond_10
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2068082
    :cond_11
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_13

    .line 2068083
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2068084
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2068085
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_11

    if-eqz v11, :cond_11

    .line 2068086
    const-string p0, "latitude"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_12

    .line 2068087
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_8

    .line 2068088
    :cond_12
    const-string p0, "longitude"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 2068089
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_8

    .line 2068090
    :cond_13
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 2068091
    invoke-virtual {v0, v9, v10}, LX/186;->b(II)V

    .line 2068092
    const/4 v9, 0x1

    invoke-virtual {v0, v9, v7}, LX/186;->b(II)V

    .line 2068093
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto :goto_7

    :cond_14
    move v7, v9

    move v10, v9

    goto :goto_8
.end method
