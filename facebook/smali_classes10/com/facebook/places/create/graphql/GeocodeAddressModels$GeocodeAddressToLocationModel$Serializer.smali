.class public final Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2068097
    const-class v0, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;

    new-instance v1, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2068098
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2068099
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2068100
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2068101
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2068102
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2068103
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2068104
    if-eqz v2, :cond_6

    .line 2068105
    const-string v3, "geocode_address_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2068106
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2068107
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 2068108
    if-eqz v3, :cond_5

    .line 2068109
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2068110
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2068111
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result p0

    if-ge v4, p0, :cond_4

    .line 2068112
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result p0

    const/4 v0, 0x0

    .line 2068113
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2068114
    invoke-virtual {v1, p0, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 2068115
    if-eqz v0, :cond_0

    .line 2068116
    const-string v2, "geo_score"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2068117
    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 2068118
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2068119
    if-eqz v0, :cond_3

    .line 2068120
    const-string v2, "node"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2068121
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2068122
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2068123
    if-eqz v2, :cond_1

    .line 2068124
    const-string p0, "latitude"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2068125
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2068126
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2068127
    if-eqz v2, :cond_2

    .line 2068128
    const-string p0, "longitude"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2068129
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2068130
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2068131
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2068132
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2068133
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2068134
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2068135
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2068136
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2068137
    check-cast p1, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel$Serializer;->a(Lcom/facebook/places/create/graphql/GeocodeAddressModels$GeocodeAddressToLocationModel;LX/0nX;LX/0my;)V

    return-void
.end method
