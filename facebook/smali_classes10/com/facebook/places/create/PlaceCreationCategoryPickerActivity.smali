.class public Lcom/facebook/places/create/PlaceCreationCategoryPickerActivity;
.super Lcom/facebook/places/create/BasePlaceCreationActivity;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2067566
    invoke-direct {p0}, Lcom/facebook/places/create/BasePlaceCreationActivity;-><init>()V

    .line 2067567
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2067568
    const v0, 0x7f081736

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/PlaceCreationCategoryPickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2067569
    invoke-super {p0, p1}, Lcom/facebook/places/create/BasePlaceCreationActivity;->b(Landroid/os/Bundle;)V

    .line 2067570
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d002f

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2067571
    invoke-virtual {p0}, Lcom/facebook/places/create/PlaceCreationCategoryPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "logger_type"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/9kb;

    .line 2067572
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    new-instance v2, LX/Dzu;

    invoke-direct {v2}, LX/Dzu;-><init>()V

    if-eqz v0, :cond_1

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/places/create/PlaceCreationCategoryPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "logger_params"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-static {v1, v2, v5, v0, v3}, Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;->a(LX/0am;LX/9kF;ZLX/9kb;Landroid/os/Parcelable;)Lcom/facebook/places/pagetopics/PlaceCategoryPickerFragment;

    move-result-object v0

    .line 2067573
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    invoke-virtual {v1, v2, v0}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2067574
    :cond_0
    return-void

    .line 2067575
    :cond_1
    sget-object v0, LX/9kb;->NO_LOGGER:LX/9kb;

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 2067563
    invoke-super {p0}, Lcom/facebook/places/create/BasePlaceCreationActivity;->onBackPressed()V

    .line 2067564
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2067565
    return-void
.end method
