.class public Lcom/facebook/places/create/home/HomeUpdateParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/create/home/HomeUpdateParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:J

.field public final h:Lcom/facebook/photos/base/media/PhotoItem;

.field public final i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2069005
    new-instance v0, LX/E0k;

    invoke-direct {v0}, LX/E0k;-><init>()V

    sput-object v0, Lcom/facebook/places/create/home/HomeUpdateParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2068993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068994
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->a:J

    .line 2068995
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->b:Ljava/lang/String;

    .line 2068996
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2068997
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->d:Ljava/lang/String;

    .line 2068998
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->e:Ljava/lang/String;

    .line 2068999
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->f:Ljava/lang/String;

    .line 2069000
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->g:J

    .line 2069001
    const-class v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->h:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2069002
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->i:Z

    .line 2069003
    return-void

    .line 2069004
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/places/create/home/HomeActivityModel;)V
    .locals 2

    .prologue
    .line 2068970
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068971
    iget-wide v0, p1, Lcom/facebook/places/create/home/HomeActivityModel;->l:J

    iput-wide v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->a:J

    .line 2068972
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->b:Ljava/lang/String;

    .line 2068973
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2068974
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->d:Ljava/lang/String;

    .line 2068975
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->e:Ljava/lang/String;

    .line 2068976
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->f:Ljava/lang/String;

    .line 2068977
    iget-wide v0, p1, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    iput-wide v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->g:J

    .line 2068978
    iget-object v0, p1, Lcom/facebook/places/create/home/HomeActivityModel;->h:Lcom/facebook/photos/base/media/PhotoItem;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->h:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2068979
    iget-boolean v0, p1, Lcom/facebook/places/create/home/HomeActivityModel;->j:Z

    iput-boolean v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->i:Z

    .line 2068980
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2068992
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2068981
    iget-wide v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2068982
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068983
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2068984
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068985
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068986
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068987
    iget-wide v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2068988
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->h:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2068989
    iget-boolean v0, p0, Lcom/facebook/places/create/home/HomeUpdateParams;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2068990
    return-void

    .line 2068991
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
