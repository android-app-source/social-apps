.class public Lcom/facebook/places/create/home/HomeActivityModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/create/home/HomeActivityModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:Landroid/location/Location;

.field public g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public h:Lcom/facebook/photos/base/media/PhotoItem;

.field public i:Landroid/net/Uri;

.field public j:Z

.field public k:LX/E0V;

.field public l:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2068557
    new-instance v0, LX/E0W;

    invoke-direct {v0}, LX/E0W;-><init>()V

    sput-object v0, Lcom/facebook/places/create/home/HomeActivityModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/E0V;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 2068558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068559
    sget-object v0, LX/E0V;->INVALID:LX/E0V;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068560
    iput-object v2, p0, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    .line 2068561
    iput-object v2, p0, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    .line 2068562
    iput-object v2, p0, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2068563
    iput-object v2, p0, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    .line 2068564
    iput-wide v4, p0, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    .line 2068565
    iput-object v2, p0, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    .line 2068566
    iput-object v2, p0, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    .line 2068567
    iput-object v2, p0, Lcom/facebook/places/create/home/HomeActivityModel;->h:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2068568
    iput-object v2, p0, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    .line 2068569
    iput-boolean v1, p0, Lcom/facebook/places/create/home/HomeActivityModel;->j:Z

    .line 2068570
    iput-object p1, p0, Lcom/facebook/places/create/home/HomeActivityModel;->k:LX/E0V;

    .line 2068571
    iput-wide v4, p0, Lcom/facebook/places/create/home/HomeActivityModel;->l:J

    .line 2068572
    return-void

    :cond_0
    move v0, v1

    .line 2068573
    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2068574
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068575
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    .line 2068576
    const-class v0, Landroid/location/Location;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    .line 2068577
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2068578
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    .line 2068579
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    .line 2068580
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    .line 2068581
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    .line 2068582
    const-class v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->h:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2068583
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    .line 2068584
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->j:Z

    .line 2068585
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 2068586
    invoke-static {}, LX/E0V;->values()[LX/E0V;

    move-result-object v1

    aget-object v0, v1, v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->k:LX/E0V;

    .line 2068587
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->l:J

    .line 2068588
    return-void

    .line 2068589
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2068590
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2068591
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068592
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 2068593
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2068594
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068595
    iget-wide v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2068596
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068597
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068598
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->h:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2068599
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 2068600
    iget-boolean v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2068601
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->k:LX/E0V;

    invoke-virtual {v0}, LX/E0V;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2068602
    iget-wide v0, p0, Lcom/facebook/places/create/home/HomeActivityModel;->l:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2068603
    return-void

    .line 2068604
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
