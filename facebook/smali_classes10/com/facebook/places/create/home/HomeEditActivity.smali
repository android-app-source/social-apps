.class public Lcom/facebook/places/create/home/HomeEditActivity;
.super Lcom/facebook/places/create/home/HomeActivity;
.source ""


# instance fields
.field public A:LX/E0o;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final C:Landroid/view/View$OnClickListener;

.field private D:LX/E0h;

.field public E:Ljava/lang/String;

.field public F:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

.field private final G:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final H:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$HomeResidenceQuery$;",
            ">;"
        }
    .end annotation
.end field

.field public z:LX/E0i;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2068865
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeActivity;-><init>()V

    .line 2068866
    new-instance v0, LX/E0a;

    invoke-direct {v0, p0}, LX/E0a;-><init>(Lcom/facebook/places/create/home/HomeEditActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->C:Landroid/view/View$OnClickListener;

    .line 2068867
    new-instance v0, LX/E0b;

    invoke-direct {v0, p0}, LX/E0b;-><init>(Lcom/facebook/places/create/home/HomeEditActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->G:LX/0TF;

    .line 2068868
    new-instance v0, LX/E0c;

    invoke-direct {v0, p0}, LX/E0c;-><init>(Lcom/facebook/places/create/home/HomeEditActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->H:LX/0TF;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)LX/31Y;
    .locals 1

    .prologue
    .line 2068869
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2068870
    invoke-virtual {v0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2068871
    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2068872
    return-object v0
.end method

.method private static a(Lcom/facebook/places/create/home/HomeEditActivity;LX/E0i;LX/E0o;LX/0kL;)V
    .locals 0

    .prologue
    .line 2068873
    iput-object p1, p0, Lcom/facebook/places/create/home/HomeEditActivity;->z:LX/E0i;

    iput-object p2, p0, Lcom/facebook/places/create/home/HomeEditActivity;->A:LX/E0o;

    iput-object p3, p0, Lcom/facebook/places/create/home/HomeEditActivity;->B:LX/0kL;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/places/create/home/HomeEditActivity;

    new-instance p1, LX/E0i;

    invoke-static {v2}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v0

    check-cast v0, LX/9kE;

    invoke-static {v2}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-direct {p1, v0, v1}, LX/E0i;-><init>(LX/9kE;LX/0tX;)V

    move-object v0, p1

    check-cast v0, LX/E0i;

    new-instance v4, LX/E0o;

    invoke-static {v2}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v1

    check-cast v1, LX/9kE;

    const/16 v3, 0xb83

    invoke-static {v2, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    new-instance v3, LX/E0j;

    invoke-direct {v3}, LX/E0j;-><init>()V

    move-object v3, v3

    move-object v3, v3

    check-cast v3, LX/E0j;

    invoke-direct {v4, v1, p1, v3}, LX/E0o;-><init>(LX/9kE;LX/0Or;LX/E0j;)V

    move-object v1, v4

    check-cast v1, LX/E0o;

    invoke-static {v2}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/places/create/home/HomeEditActivity;->a(Lcom/facebook/places/create/home/HomeEditActivity;LX/E0i;LX/E0o;LX/0kL;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/create/home/HomeEditActivity;Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2068882
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->r()V

    .line 2068883
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    .line 2068884
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->E:Ljava/lang/String;

    .line 2068885
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->bz_()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2068886
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->bz_()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->F:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    .line 2068887
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->bz_()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    .line 2068888
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->bz_()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    .line 2068889
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->e()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2068890
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->e()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    .line 2068891
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    .line 2068892
    new-instance v0, Landroid/location/Location;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2068893
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->bz_()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v4}, LX/15i;->l(II)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 2068894
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->bz_()Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LX/15i;->l(II)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 2068895
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iput-object v0, v1, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    .line 2068896
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2068897
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->by_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v2, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v2, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    .line 2068898
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel;->c()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2068899
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->n()V

    .line 2068900
    return-void
.end method

.method public static b(Lcom/facebook/places/create/home/HomeEditActivity;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2068874
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->r()V

    .line 2068875
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeActivity;->b(Z)V

    .line 2068876
    :try_start_0
    throw p1
    :try_end_0
    .catch LX/E0n; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/E0m; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    .line 2068877
    :catch_0
    sget-object v0, LX/E0h;->HOME_NAME_CHANGE_DENIED_ERROR:LX/E0h;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->D:LX/E0h;

    .line 2068878
    :goto_0
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->y()V

    .line 2068879
    return-void

    .line 2068880
    :catch_1
    sget-object v0, LX/E0h;->HOME_CITY_CHANGE_DENIED_ERROR:LX/E0h;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->D:LX/E0h;

    goto :goto_0

    .line 2068881
    :catch_2
    sget-object v0, LX/E0h;->HOME_UPDATE_ERROR:LX/E0h;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->D:LX/E0h;

    goto :goto_0
.end method

.method public static t(Lcom/facebook/places/create/home/HomeEditActivity;)V
    .locals 3

    .prologue
    .line 2068910
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->p()V

    .line 2068911
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/places/create/citypicker/CityPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2068912
    const-string v1, "extra_location"

    iget-object v2, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v2, v2, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2068913
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->u:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2068914
    return-void
.end method

.method private u()V
    .locals 2

    .prologue
    .line 2068915
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->h:Lcom/facebook/photos/base/media/PhotoItem;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2068916
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->j:Z

    .line 2068917
    :cond_0
    return-void
.end method

.method private v()V
    .locals 8

    .prologue
    .line 2068901
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->q()V

    .line 2068902
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->z:LX/E0i;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-wide v2, v1, Lcom/facebook/places/create/home/HomeActivityModel;->l:J

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeEditActivity;->H:LX/0TF;

    .line 2068903
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068904
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068905
    new-instance v4, LX/5m2;

    invoke-direct {v4}, LX/5m2;-><init>()V

    move-object v4, v4

    .line 2068906
    const-string v5, "node"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    const-string v6, "size"

    const-string v7, "320"

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2068907
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    .line 2068908
    iget-object v5, v0, LX/E0i;->a:LX/9kE;

    iget-object v6, v0, LX/E0i;->b:LX/0tX;

    invoke-virtual {v6, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    invoke-static {v4}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-virtual {v5, v4, v1}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2068909
    return-void
.end method

.method public static w(Lcom/facebook/places/create/home/HomeEditActivity;)V
    .locals 3

    .prologue
    .line 2068856
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068857
    iget-object v1, v0, LX/E0T;->a:LX/0Zb;

    const-string v2, "home_edit_updated"

    invoke-static {v0, v2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068858
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->setResult(ILandroid/content/Intent;)V

    .line 2068859
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->finish()V

    .line 2068860
    return-void
.end method

.method public static x(Lcom/facebook/places/create/home/HomeEditActivity;)V
    .locals 1

    .prologue
    .line 2068861
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->r()V

    .line 2068862
    sget-object v0, LX/E0h;->HOME_FETCH_ERROR:LX/E0h;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->D:LX/E0h;

    .line 2068863
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->y()V

    .line 2068864
    return-void
.end method

.method private y()V
    .locals 3

    .prologue
    .line 2068786
    sget-object v0, LX/E0g;->a:[I

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeEditActivity;->D:LX/E0h;

    invoke-virtual {v1}, LX/E0h;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2068787
    :goto_0
    return-void

    .line 2068788
    :pswitch_0
    const v0, 0x7f0816cb

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f081750

    invoke-virtual {p0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->a(Ljava/lang/String;Ljava/lang/String;)LX/31Y;

    move-result-object v0

    .line 2068789
    const v1, 0x7f081748

    new-instance v2, LX/E0d;

    invoke-direct {v2, p0}, LX/E0d;-><init>(Lcom/facebook/places/create/home/HomeEditActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2068790
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0

    .line 2068791
    :pswitch_1
    const v0, 0x7f0816cb

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f081751

    invoke-virtual {p0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->a(Ljava/lang/String;Ljava/lang/String;)LX/31Y;

    move-result-object v0

    .line 2068792
    const v1, 0x7f081748

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2068793
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0

    .line 2068794
    :pswitch_2
    const v0, 0x7f081768

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f081767

    invoke-virtual {p0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->a(Ljava/lang/String;Ljava/lang/String;)LX/31Y;

    move-result-object v0

    .line 2068795
    const v1, 0x7f081748

    new-instance v2, LX/E0e;

    invoke-direct {v2, p0}, LX/E0e;-><init>(Lcom/facebook/places/create/home/HomeEditActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2068796
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0

    .line 2068797
    :pswitch_3
    const v0, 0x7f081769

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f08176a

    invoke-virtual {p0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/places/create/home/HomeEditActivity;->a(Ljava/lang/String;Ljava/lang/String;)LX/31Y;

    move-result-object v0

    .line 2068798
    const v1, 0x7f081748

    new-instance v2, LX/E0f;

    invoke-direct {v2, p0}, LX/E0f;-><init>(Lcom/facebook/places/create/home/HomeEditActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2068799
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2068800
    const v0, 0x7f08174f

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/photos/base/media/PhotoItem;)V
    .locals 0

    .prologue
    .line 2068801
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->u()V

    .line 2068802
    invoke-super {p0, p1}, Lcom/facebook/places/create/home/HomeActivity;->a(Lcom/facebook/photos/base/media/PhotoItem;)V

    .line 2068803
    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 2068804
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "home_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068805
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "home_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068806
    new-instance v0, Lcom/facebook/places/create/home/HomeActivityModel;

    sget-object v1, LX/E0V;->EDIT:LX/E0V;

    invoke-direct {v0, v1}, Lcom/facebook/places/create/home/HomeActivityModel;-><init>(LX/E0V;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    .line 2068807
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "home_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    .line 2068808
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "home_id"

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/facebook/places/create/home/HomeActivityModel;->l:J

    .line 2068809
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-wide v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->l:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068810
    return-void

    .line 2068811
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2068812
    invoke-static {p0, p0}, Lcom/facebook/places/create/home/HomeEditActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2068813
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->t:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeEditActivity;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2068814
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeEditActivity;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2068815
    if-nez p1, :cond_1

    .line 2068816
    sget-object v0, LX/E0h;->NO_ERROR:LX/E0h;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->D:LX/E0h;

    .line 2068817
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->v()V

    .line 2068818
    :cond_0
    :goto_0
    return-void

    .line 2068819
    :cond_1
    const-string v0, "state_error_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068820
    const-string v0, "state_can_edit_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068821
    const-string v0, "state_original_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068822
    const-string v0, "state_original_city"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068823
    invoke-static {}, LX/E0h;->values()[LX/E0h;

    move-result-object v0

    const-string v1, "state_error_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->D:LX/E0h;

    .line 2068824
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    const-string v1, "state_can_edit_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 2068825
    const-string v0, "state_original_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->E:Ljava/lang/String;

    .line 2068826
    const-string v0, "state_original_city"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->F:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    .line 2068827
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeEditActivity;->D:LX/E0h;

    sget-object v1, LX/E0h;->NO_ERROR:LX/E0h;

    if-eq v0, v1, :cond_0

    .line 2068828
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->y()V

    goto :goto_0
.end method

.method public final l()Lcom/facebook/places/create/home/HomeActivityLoggerData;
    .locals 1

    .prologue
    .line 2068829
    new-instance v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;

    invoke-direct {v0}, Lcom/facebook/places/create/home/HomeActivityLoggerData;-><init>()V

    return-object v0
.end method

.method public final m()V
    .locals 4

    .prologue
    .line 2068830
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeActivity;->b(Z)V

    .line 2068831
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->q()V

    .line 2068832
    new-instance v0, Lcom/facebook/places/create/home/HomeUpdateParams;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-direct {v0, v1}, Lcom/facebook/places/create/home/HomeUpdateParams;-><init>(Lcom/facebook/places/create/home/HomeActivityModel;)V

    .line 2068833
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeEditActivity;->A:LX/E0o;

    iget-object v2, p0, Lcom/facebook/places/create/home/HomeEditActivity;->G:LX/0TF;

    .line 2068834
    iget-object v3, v1, LX/E0o;->a:LX/9kE;

    new-instance p0, LX/E0l;

    invoke-direct {p0, v1, v0}, LX/E0l;-><init>(LX/E0o;Lcom/facebook/places/create/home/HomeUpdateParams;)V

    invoke-virtual {v3, p0, v2}, LX/9kE;->a(Ljava/util/concurrent/Callable;LX/0TF;)V

    .line 2068835
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 2068836
    invoke-super {p0}, Lcom/facebook/places/create/home/HomeActivity;->o()V

    .line 2068837
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->s:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1060003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2068838
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2068839
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 2068840
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 2068841
    const-string v0, "selected_city"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2068842
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    .line 2068843
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    .line 2068844
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->o()V

    .line 2068845
    :cond_0
    :goto_0
    return-void

    .line 2068846
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/places/create/home/HomeActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2068847
    invoke-super {p0, p1}, Lcom/facebook/places/create/home/HomeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2068848
    const-string v0, "state_can_edit_name"

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2068849
    const-string v0, "state_original_name"

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeEditActivity;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2068850
    const-string v0, "state_original_city"

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeEditActivity;->F:Lcom/facebook/places/graphql/PlacesGraphQLModels$HomeResidenceQueryModel$CityModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2068851
    const-string v0, "state_error_state"

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeEditActivity;->D:LX/E0h;

    invoke-virtual {v1}, LX/E0h;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2068852
    return-void
.end method

.method public final s()V
    .locals 0

    .prologue
    .line 2068853
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeEditActivity;->u()V

    .line 2068854
    invoke-super {p0}, Lcom/facebook/places/create/home/HomeActivity;->s()V

    .line 2068855
    return-void
.end method
