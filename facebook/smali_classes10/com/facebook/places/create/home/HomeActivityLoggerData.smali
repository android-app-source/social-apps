.class public Lcom/facebook/places/create/home/HomeActivityLoggerData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/create/home/HomeActivityLoggerData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2068525
    new-instance v0, LX/E0U;

    invoke-direct {v0}, LX/E0U;-><init>()V

    sput-object v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2068526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068527
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->b:Z

    .line 2068528
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2068529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2068530
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->c:Ljava/lang/String;

    .line 2068531
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->d:Ljava/lang/String;

    .line 2068532
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->a:Ljava/lang/String;

    .line 2068533
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->f:Ljava/lang/String;

    .line 2068534
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->e:J

    .line 2068535
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->b:Z

    .line 2068536
    return-void

    .line 2068537
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2068538
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2068539
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068540
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068541
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068542
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2068543
    iget-wide v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2068544
    iget-boolean v0, p0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2068545
    return-void

    .line 2068546
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
