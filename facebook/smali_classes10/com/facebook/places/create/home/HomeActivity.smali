.class public abstract Lcom/facebook/places/create/home/HomeActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final z:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:LX/E0P;

.field private B:Landroid/widget/EditText;

.field private C:Landroid/widget/EditText;

.field private D:Landroid/widget/FrameLayout;

.field public E:Landroid/widget/RelativeLayout;

.field private F:Landroid/widget/ImageView;

.field public G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private H:Landroid/widget/RelativeLayout;

.field private I:Landroid/widget/ImageView;

.field private J:Landroid/widget/TextView;

.field private final K:LX/63W;

.field private final L:LX/63J;

.field private final M:LX/2dD;

.field public p:Lcom/facebook/places/create/home/HomeActivityModel;

.field public q:LX/94Z;

.field public r:Landroid/widget/EditText;

.field public s:Landroid/widget/TextView;

.field public t:Landroid/widget/ImageView;

.field public u:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/E0T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/8Sa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2068337
    const-class v0, Lcom/facebook/places/create/home/HomeActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/places/create/home/HomeActivity;->z:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2068338
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2068339
    new-instance v0, LX/E0F;

    invoke-direct {v0, p0}, LX/E0F;-><init>(Lcom/facebook/places/create/home/HomeActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->K:LX/63W;

    .line 2068340
    new-instance v0, LX/E0G;

    invoke-direct {v0, p0}, LX/E0G;-><init>(Lcom/facebook/places/create/home/HomeActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->L:LX/63J;

    .line 2068341
    new-instance v0, LX/E0H;

    invoke-direct {v0, p0}, LX/E0H;-><init>(Lcom/facebook/places/create/home/HomeActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->M:LX/2dD;

    .line 2068342
    return-void
.end method

.method private t()V
    .locals 4

    .prologue
    .line 2068343
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_0

    .line 2068344
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v1

    sget-object v2, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-static {v1, v2}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v0

    .line 2068345
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->I:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/facebook/places/create/home/HomeActivity;->y:LX/0wM;

    const v3, -0x958e80

    invoke-virtual {v2, v0, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2068346
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->J:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2068347
    :cond_0
    return-void
.end method

.method public static w(Lcom/facebook/places/create/home/HomeActivity;)V
    .locals 4

    .prologue
    .line 2068348
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->p()V

    .line 2068349
    new-instance v0, LX/6WS;

    invoke-direct {v0, p0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2068350
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 2068351
    const v2, 0x7f08171f

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 2068352
    const v3, 0x7f02144b

    invoke-virtual {v2, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2068353
    new-instance v3, LX/E0K;

    invoke-direct {v3, p0}, LX/E0K;-><init>(Lcom/facebook/places/create/home/HomeActivity;)V

    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2068354
    const v2, 0x7f081720

    invoke-virtual {v1, v2}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v1

    .line 2068355
    const v2, 0x7f02161e

    invoke-virtual {v1, v2}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2068356
    new-instance v2, LX/E0L;

    invoke-direct {v2, p0}, LX/E0L;-><init>(Lcom/facebook/places/create/home/HomeActivity;)V

    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2068357
    sget-object v1, LX/E0P;->PHOTO:LX/E0P;

    iput-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->A:LX/E0P;

    .line 2068358
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->M:LX/2dD;

    .line 2068359
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 2068360
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->F:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2068361
    return-void
.end method

.method private x()V
    .locals 3

    .prologue
    .line 2068362
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 2068363
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    sget-object v2, Lcom/facebook/places/create/home/HomeActivity;->z:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2068364
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2068365
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->E:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0e49

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2068366
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->E:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestLayout()V

    .line 2068367
    :goto_0
    return-void

    .line 2068368
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2068369
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/places/create/home/HomeActivity;->z:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public a(Lcom/facebook/photos/base/media/PhotoItem;)V
    .locals 3

    .prologue
    .line 2068443
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068444
    iget-object v1, v0, LX/E0T;->a:LX/0Zb;

    const-string v2, "home_%s_photo_picker_picked"

    invoke-static {v0, v2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068445
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iput-object p1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->h:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2068446
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v2, v2, Lcom/facebook/places/create/home/HomeActivityModel;->h:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    .line 2068447
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeActivity;->x()V

    .line 2068448
    return-void
.end method

.method public abstract b()V
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2068370
    const v0, 0x7f03088d

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeActivity;->setContentView(I)V

    .line 2068371
    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Lcom/facebook/places/create/home/HomeActivity;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x12cb

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/E0T;->b(LX/0QB;)LX/E0T;

    move-result-object v5

    check-cast v5, LX/E0T;

    invoke-static {v0}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v6

    check-cast v6, LX/8Sa;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v3, v1, Lcom/facebook/places/create/home/HomeActivity;->u:Lcom/facebook/content/SecureContextHelper;

    iput-object v4, v1, Lcom/facebook/places/create/home/HomeActivity;->v:LX/0Or;

    iput-object v5, v1, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    iput-object v6, v1, Lcom/facebook/places/create/home/HomeActivity;->x:LX/8Sa;

    iput-object v0, v1, Lcom/facebook/places/create/home/HomeActivity;->y:LX/0wM;

    .line 2068372
    invoke-static {p0}, LX/9jy;->a(Ljava/lang/Object;)V

    .line 2068373
    if-nez p1, :cond_3

    .line 2068374
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "home_activity_entry_flow"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068375
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->b()V

    .line 2068376
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->l()Lcom/facebook/places/create/home/HomeActivityLoggerData;

    move-result-object v0

    .line 2068377
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2068378
    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;->a:Ljava/lang/String;

    .line 2068379
    sget-object v1, LX/E0P;->CLOSED:LX/E0P;

    iput-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->A:LX/E0P;

    move v1, v2

    .line 2068380
    :goto_0
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068381
    iget-object v3, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068382
    iput-object v0, v3, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068383
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    iget-object v3, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    .line 2068384
    iput-object v3, v0, LX/E0T;->c:Lcom/facebook/places/create/home/HomeActivityModel;

    .line 2068385
    if-nez p1, :cond_0

    .line 2068386
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068387
    const-string v2, "home_%s_init"

    invoke-static {v0, v2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2068388
    const-string v3, "default_value"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068389
    iget-object v3, v0, LX/E0T;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068390
    :cond_0
    const v0, 0x7f0d0a7f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/94V;

    .line 2068391
    iget-object v3, p0, Lcom/facebook/places/create/home/HomeActivity;->L:LX/63J;

    invoke-virtual {v0, v3}, LX/94V;->setOnBackPressedListener(LX/63J;)V

    .line 2068392
    new-instance v3, LX/94Z;

    new-instance v4, LX/94Y;

    invoke-direct {v4}, LX/94Y;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->a()Ljava/lang/String;

    move-result-object v5

    .line 2068393
    iput-object v5, v4, LX/94Y;->a:Ljava/lang/String;

    .line 2068394
    move-object v4, v4

    .line 2068395
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v5

    .line 2068396
    iput-object v5, v4, LX/94Y;->d:LX/94c;

    .line 2068397
    move-object v4, v4

    .line 2068398
    invoke-virtual {v4}, LX/94Y;->a()LX/94X;

    move-result-object v4

    invoke-direct {v3, v0, v4}, LX/94Z;-><init>(LX/94V;LX/94X;)V

    iput-object v3, p0, Lcom/facebook/places/create/home/HomeActivity;->q:LX/94Z;

    .line 2068399
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->q:LX/94Z;

    iget-object v3, p0, Lcom/facebook/places/create/home/HomeActivity;->q:LX/94Z;

    .line 2068400
    iget-object v2, v3, LX/94Z;->b:LX/94X;

    move-object v3, v2

    .line 2068401
    invoke-virtual {v3}, LX/94X;->a()LX/94Y;

    move-result-object v3

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v4

    const v5, 0x7f081747

    invoke-virtual {p0, v5}, Lcom/facebook/places/create/home/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2068402
    iput-object v5, v4, LX/108;->g:Ljava/lang/String;

    .line 2068403
    move-object v4, v4

    .line 2068404
    invoke-virtual {v4}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v4

    .line 2068405
    iput-object v4, v3, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2068406
    move-object v3, v3

    .line 2068407
    iget-object v4, p0, Lcom/facebook/places/create/home/HomeActivity;->K:LX/63W;

    .line 2068408
    iput-object v4, v3, LX/94Y;->c:LX/63W;

    .line 2068409
    move-object v3, v3

    .line 2068410
    invoke-virtual {v3}, LX/94Y;->a()LX/94X;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/94Z;->a(LX/94X;)V

    .line 2068411
    const v0, 0x7f0d0446

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->D:Landroid/widget/FrameLayout;

    .line 2068412
    if-eqz v1, :cond_1

    .line 2068413
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->q()V

    .line 2068414
    :cond_1
    const v0, 0x7f0d162d

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->E:Landroid/widget/RelativeLayout;

    .line 2068415
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->E:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0de7

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->G:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2068416
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->E:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d162a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->F:Landroid/widget/ImageView;

    .line 2068417
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->F:Landroid/widget/ImageView;

    new-instance v1, LX/E0I;

    invoke-direct {v1, p0}, LX/E0I;-><init>(Lcom/facebook/places/create/home/HomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2068418
    const v0, 0x7f0d162b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    .line 2068419
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    new-instance v1, LX/E0O;

    iget-object v3, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    invoke-direct {v1, p0, v3}, LX/E0O;-><init>(Lcom/facebook/places/create/home/HomeActivity;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2068420
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    new-instance v1, LX/E0N;

    iget-object v3, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    invoke-direct {v1, p0, v3}, LX/E0N;-><init>(Lcom/facebook/places/create/home/HomeActivity;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2068421
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 2068422
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 2068423
    const v0, 0x7f0d1630

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->B:Landroid/widget/EditText;

    .line 2068424
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->B:Landroid/widget/EditText;

    new-instance v1, LX/E0O;

    iget-object v3, p0, Lcom/facebook/places/create/home/HomeActivity;->B:Landroid/widget/EditText;

    invoke-direct {v1, p0, v3}, LX/E0O;-><init>(Lcom/facebook/places/create/home/HomeActivity;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2068425
    const v0, 0x7f0d1631

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->C:Landroid/widget/EditText;

    .line 2068426
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->C:Landroid/widget/EditText;

    new-instance v1, LX/E0O;

    iget-object v3, p0, Lcom/facebook/places/create/home/HomeActivity;->C:Landroid/widget/EditText;

    invoke-direct {v1, p0, v3}, LX/E0O;-><init>(Lcom/facebook/places/create/home/HomeActivity;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2068427
    const v0, 0x7f0d162e

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->s:Landroid/widget/TextView;

    .line 2068428
    const v0, 0x7f0d162f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->t:Landroid/widget/ImageView;

    .line 2068429
    const v0, 0x7f0d1632

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->H:Landroid/widget/RelativeLayout;

    .line 2068430
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->H:Landroid/widget/RelativeLayout;

    new-instance v1, LX/E0J;

    invoke-direct {v1, p0}, LX/E0J;-><init>(Lcom/facebook/places/create/home/HomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2068431
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->H:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0c49

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->I:Landroid/widget/ImageView;

    .line 2068432
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->H:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d253b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->J:Landroid/widget/TextView;

    .line 2068433
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->A:LX/E0P;

    sget-object v1, LX/E0P;->CLOSED:LX/E0P;

    if-eq v0, v1, :cond_2

    .line 2068434
    sget-object v0, LX/E0M;->a:[I

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->A:LX/E0P;

    invoke-virtual {v1}, LX/E0P;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2068435
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->n()V

    .line 2068436
    invoke-virtual {p0, p1}, Lcom/facebook/places/create/home/HomeActivity;->d(Landroid/os/Bundle;)V

    .line 2068437
    return-void

    .line 2068438
    :cond_3
    const-string v0, "state_home_creation_model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/home/HomeActivityModel;

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    .line 2068439
    const-string v0, "state_home_creation_logger_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;

    .line 2068440
    const-string v1, "state_menu_popover"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, LX/E0P;

    iput-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->A:LX/E0P;

    .line 2068441
    const-string v1, "state_loading_view"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    goto/16 :goto_0

    .line 2068442
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->E:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/facebook/places/create/home/HomeActivity$6;

    invoke-direct {v1, p0}, Lcom/facebook/places/create/home/HomeActivity$6;-><init>(Lcom/facebook/places/create/home/HomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 2068329
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->q:LX/94Z;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->q:LX/94Z;

    .line 2068330
    iget-object v2, v1, LX/94Z;->b:LX/94X;

    move-object v1, v2

    .line 2068331
    invoke-virtual {v1}, LX/94X;->a()LX/94Y;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/94Y;->a(Z)LX/94Y;

    move-result-object v1

    invoke-virtual {v1}, LX/94Y;->a()LX/94X;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94Z;->a(LX/94X;)V

    .line 2068332
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->q:LX/94Z;

    .line 2068333
    iget-object v1, v0, LX/94Z;->b:LX/94X;

    move-object v0, v1

    .line 2068334
    invoke-virtual {v0}, LX/94X;->a()LX/94Y;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/94Y;->a(Z)LX/94Y;

    .line 2068335
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2068336
    return-void
.end method

.method public abstract l()Lcom/facebook/places/create/home/HomeActivityLoggerData;
.end method

.method public abstract m()V
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 2068319
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2068320
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2068321
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2068322
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->C:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2068323
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2068324
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->B:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2068325
    :cond_2
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeActivity;->x()V

    .line 2068326
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->o()V

    .line 2068327
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeActivity;->t()V

    .line 2068328
    return-void
.end method

.method public o()V
    .locals 3

    .prologue
    .line 2068315
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2068316
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2068317
    :goto_0
    return-void

    .line 2068318
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->s:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0816fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2068300
    const/16 v0, 0xb

    if-ne p1, v0, :cond_3

    .line 2068301
    if-ne p2, v1, :cond_2

    .line 2068302
    const-string v0, "extra_media_items"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2068303
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2068304
    :cond_0
    :goto_0
    return-void

    .line 2068305
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeActivity;->a(Lcom/facebook/photos/base/media/PhotoItem;)V

    goto :goto_0

    .line 2068306
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068307
    iget-object v1, v0, LX/E0T;->a:LX/0Zb;

    const-string p0, "home_%s_photo_picker_cancelled"

    invoke-static {v0, p0}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, p0}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v1, p0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068308
    goto :goto_0

    .line 2068309
    :cond_3
    const/16 v0, 0xd

    if-ne p1, v0, :cond_0

    .line 2068310
    if-ne p2, v1, :cond_0

    .line 2068311
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    const-string v0, "selected_privacy"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, v1, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2068312
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068313
    iget-object v1, v0, LX/E0T;->a:LX/0Zb;

    const-string p1, "home_%s_privacy_updated"

    invoke-static {v0, p1}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v1, p1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068314
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeActivity;->t()V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 2068295
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->p()V

    .line 2068296
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068297
    iget-object v1, v0, LX/E0T;->a:LX/0Zb;

    const-string v2, "home_%s_cancelled"

    invoke-static {v0, v2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068298
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 2068299
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2068284
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2068285
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068286
    iget-object v1, v0, LX/E0T;->a:LX/0Zb;

    const-string v2, "home_%s_backgrounded"

    invoke-static {v0, v2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068287
    const-string v0, "state_home_creation_model"

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2068288
    const-string v0, "state_home_creation_logger_data"

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068289
    iget-object v2, v1, LX/E0T;->e:Lcom/facebook/places/create/home/HomeActivityLoggerData;

    move-object v1, v2

    .line 2068290
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2068291
    const-string v0, "state_menu_popover"

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->A:LX/E0P;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2068292
    const-string v1, "state_loading_view"

    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2068293
    return-void

    .line 2068294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 2068277
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 2068278
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->C:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 2068279
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->B:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 2068280
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2068281
    if-eqz v0, :cond_0

    .line 2068282
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2068283
    :cond_0
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    .line 2068274
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->D:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2068275
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->D:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->bringToFront()V

    .line 2068276
    return-void
.end method

.method public final r()V
    .locals 2

    .prologue
    .line 2068272
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->D:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2068273
    return-void
.end method

.method public s()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2068268
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->h:Lcom/facebook/photos/base/media/PhotoItem;

    .line 2068269
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iput-object v1, v0, Lcom/facebook/places/create/home/HomeActivityModel;->i:Landroid/net/Uri;

    .line 2068270
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeActivity;->x()V

    .line 2068271
    return-void
.end method
