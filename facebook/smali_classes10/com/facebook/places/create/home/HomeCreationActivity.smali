.class public Lcom/facebook/places/create/home/HomeCreationActivity;
.super Lcom/facebook/places/create/home/HomeActivity;
.source ""


# instance fields
.field public A:LX/E0q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private B:Lcom/facebook/places/create/network/PlaceCreationParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final C:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final D:Landroid/view/View$OnClickListener;

.field public z:LX/E14;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2068697
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeActivity;-><init>()V

    .line 2068698
    new-instance v0, LX/E0X;

    invoke-direct {v0, p0}, LX/E0X;-><init>(Lcom/facebook/places/create/home/HomeCreationActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->C:LX/0TF;

    .line 2068699
    new-instance v0, LX/E0Y;

    invoke-direct {v0, p0}, LX/E0Y;-><init>(Lcom/facebook/places/create/home/HomeCreationActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->D:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private static a(Lcom/facebook/places/create/home/HomeCreationActivity;LX/E14;LX/E0q;)V
    .locals 0

    .prologue
    .line 2068672
    iput-object p1, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->z:LX/E14;

    iput-object p2, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->A:LX/E0q;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/places/create/home/HomeCreationActivity;

    invoke-static {v1}, LX/E14;->b(LX/0QB;)LX/E14;

    move-result-object v0

    check-cast v0, LX/E14;

    new-instance p1, LX/E0q;

    invoke-static {v1}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v2

    check-cast v2, LX/9kE;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p1, v2, v3}, LX/E0q;-><init>(LX/9kE;LX/0tX;)V

    move-object v1, p1

    check-cast v1, LX/E0q;

    invoke-static {p0, v0, v1}, Lcom/facebook/places/create/home/HomeCreationActivity;->a(Lcom/facebook/places/create/home/HomeCreationActivity;LX/E14;LX/E0q;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2068700
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2068701
    invoke-virtual {v0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 2068702
    const v1, 0x7f081748

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 2068703
    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 2068704
    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2068705
    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/create/home/HomeCreationActivity;J)V
    .locals 3

    .prologue
    .line 2068706
    new-instance v0, LX/5m9;

    invoke-direct {v0}, LX/5m9;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2068707
    iput-object v1, v0, LX/5m9;->f:Ljava/lang/String;

    .line 2068708
    move-object v0, v0

    .line 2068709
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2068710
    iput-object v1, v0, LX/5m9;->h:Ljava/lang/String;

    .line 2068711
    move-object v0, v0

    .line 2068712
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 2068713
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2068714
    iput-object v2, v1, LX/E0T;->d:Ljava/lang/String;

    .line 2068715
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068716
    const-string v2, "home_creation_created"

    invoke-static {v1, v2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2068717
    const-string p1, "created_place_id"

    iget-object p2, v1, LX/E0T;->d:Ljava/lang/String;

    invoke-virtual {v2, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2068718
    iget-object p1, v1, LX/E0T;->a:LX/0Zb;

    invoke-interface {p1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068719
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2068720
    const-string v2, "extra_place"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2068721
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/places/create/home/HomeCreationActivity;->setResult(ILandroid/content/Intent;)V

    .line 2068722
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->finish()V

    .line 2068723
    return-void
.end method

.method public static b(Lcom/facebook/places/create/home/HomeCreationActivity;Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2068724
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->r()V

    .line 2068725
    invoke-virtual {p0, v3}, Lcom/facebook/places/create/home/HomeActivity;->b(Z)V

    .line 2068726
    :try_start_0
    throw p1
    :try_end_0
    .catch LX/E0w; {:try_start_0 .. :try_end_0} :catch_0
    .catch LX/E0y; {:try_start_0 .. :try_end_0} :catch_1
    .catch LX/E0x; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4

    .line 2068727
    :catch_0
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0816ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2068728
    :goto_0
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->q:LX/94Z;

    iget-object v2, p0, Lcom/facebook/places/create/home/HomeActivity;->q:LX/94Z;

    .line 2068729
    iget-object p1, v2, LX/94Z;->b:LX/94X;

    move-object v2, p1

    .line 2068730
    invoke-virtual {v2}, LX/94X;->a()LX/94Y;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/94Y;->a(Z)LX/94Y;

    move-result-object v2

    invoke-virtual {v2}, LX/94Y;->a()LX/94X;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/94Z;->a(LX/94X;)V

    .line 2068731
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    invoke-virtual {v1}, LX/E0T;->u()V

    .line 2068732
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0816cc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/facebook/places/create/home/HomeCreationActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2068733
    return-void

    .line 2068734
    :catch_1
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081724

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2068735
    :catch_2
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081724

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2068736
    :catch_3
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081725

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2068737
    :catch_4
    move-exception v0

    .line 2068738
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private t()V
    .locals 18

    .prologue
    .line 2068739
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/facebook/places/create/home/HomeActivity;->b(Z)V

    .line 2068740
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/places/create/home/HomeActivity;->q()V

    .line 2068741
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v2, v2, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v3, v3, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v3, v3, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    :goto_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v5, v5, Lcom/facebook/places/create/home/HomeActivityModel;->h:Lcom/facebook/photos/base/media/PhotoItem;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v7, v7, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-wide v8, v8, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v10, v10, Lcom/facebook/places/create/home/HomeActivityModel;->c:Ljava/lang/String;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v12, v12, Lcom/facebook/places/create/home/HomeActivityModel;->d:Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static/range {v2 .. v17}, Lcom/facebook/places/create/network/PlaceCreationParams;->a(Ljava/lang/String;Landroid/location/Location;LX/0am;Lcom/facebook/photos/base/media/PhotoItem;Ljava/util/List;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/util/List;)Lcom/facebook/places/create/network/PlaceCreationParams;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/facebook/places/create/home/HomeCreationActivity;->B:Lcom/facebook/places/create/network/PlaceCreationParams;

    .line 2068742
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/places/create/home/HomeCreationActivity;->z:LX/E14;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/places/create/home/HomeCreationActivity;->B:Lcom/facebook/places/create/network/PlaceCreationParams;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/places/create/home/HomeCreationActivity;->C:LX/0TF;

    invoke-virtual {v2, v3, v4}, LX/E14;->a(Lcom/facebook/places/create/network/PlaceCreationParams;LX/0TF;)V

    .line 2068743
    return-void

    .line 2068744
    :cond_0
    new-instance v3, Landroid/location/Location;

    const-string v4, ""

    invoke-direct {v3, v4}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static u(Lcom/facebook/places/create/home/HomeCreationActivity;)V
    .locals 3

    .prologue
    .line 2068673
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeActivity;->p()V

    .line 2068674
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/places/create/citypicker/CityPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2068675
    const-string v1, "extra_location"

    iget-object v2, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v2, v2, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2068676
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->u:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0xc

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2068677
    return-void
.end method

.method private v()V
    .locals 7

    .prologue
    .line 2068678
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 2068679
    :goto_0
    return-void

    .line 2068680
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->A:LX/E0q;

    invoke-virtual {v0}, LX/E0q;->a()V

    .line 2068681
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->A:LX/E0q;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    new-instance v2, LX/E0Z;

    invoke-direct {v2, p0}, LX/E0Z;-><init>(Lcom/facebook/places/create/home/HomeCreationActivity;)V

    .line 2068682
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068683
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068684
    new-instance v3, LX/4DI;

    invoke-direct {v3}, LX/4DI;-><init>()V

    .line 2068685
    new-instance v4, LX/3Aj;

    invoke-direct {v4}, LX/3Aj;-><init>()V

    .line 2068686
    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    .line 2068687
    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    .line 2068688
    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v5

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_1

    .line 2068689
    invoke-virtual {v1}, Landroid/location/Location;->getAccuracy()F

    move-result v5

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 2068690
    :cond_1
    invoke-virtual {v1}, Landroid/location/Location;->hasSpeed()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2068691
    invoke-virtual {v1}, Landroid/location/Location;->getSpeed()F

    move-result v5

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/3Aj;->d(Ljava/lang/Double;)LX/3Aj;

    .line 2068692
    :cond_2
    invoke-virtual {v3, v4}, LX/4DI;->a(LX/3Aj;)LX/4DI;

    .line 2068693
    new-instance v4, LX/5m0;

    invoke-direct {v4}, LX/5m0;-><init>()V

    move-object v4, v4

    .line 2068694
    const-string v5, "query"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/5m0;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2068695
    iget-object v4, v0, LX/E0q;->a:LX/9kE;

    iget-object v5, v0, LX/E0q;->b:LX/0tX;

    invoke-virtual {v5, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    new-instance v5, LX/E0p;

    invoke-direct {v5, v0, v2}, LX/E0p;-><init>(LX/E0q;LX/0TF;)V

    invoke-virtual {v4, v3, v5}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2068696
    goto/16 :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2068625
    const v0, 0x7f08171a

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2068626
    new-instance v0, Lcom/facebook/places/create/home/HomeActivityModel;

    sget-object v1, LX/E0V;->CREATE:LX/E0V;

    invoke-direct {v0, v1}, Lcom/facebook/places/create/home/HomeActivityModel;-><init>(LX/E0V;)V

    iput-object v0, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    .line 2068627
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08171d

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->v:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2068628
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "map_location"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 2068629
    iget-object v2, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iput-object v1, v2, Lcom/facebook/places/create/home/HomeActivityModel;->a:Ljava/lang/String;

    .line 2068630
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iput-object v0, v1, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    .line 2068631
    new-instance v0, LX/8QP;

    invoke-direct {v0}, LX/8QP;-><init>()V

    const v1, 0x7f081732

    invoke-virtual {p0, v1}, Lcom/facebook/places/create/home/HomeCreationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->d(Ljava/lang/String;)LX/8QP;

    move-result-object v0

    new-instance v1, LX/2dc;

    invoke-direct {v1}, LX/2dc;-><init>()V

    const-string v2, "friends"

    .line 2068632
    iput-object v2, v1, LX/2dc;->f:Ljava/lang/String;

    .line 2068633
    move-object v1, v1

    .line 2068634
    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/8QP;

    move-result-object v0

    const-string v1, "{\"value\":\"ALL_FRIENDS\"}"

    invoke-virtual {v0, v1}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    move-result-object v0

    .line 2068635
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    iput-object v0, v1, Lcom/facebook/places/create/home/HomeActivityModel;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2068636
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2068637
    invoke-static {p0, p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2068638
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->s:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->D:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2068639
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->t:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->D:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2068640
    if-nez p1, :cond_0

    .line 2068641
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->v()V

    .line 2068642
    :cond_0
    return-void
.end method

.method public final l()Lcom/facebook/places/create/home/HomeActivityLoggerData;
    .locals 2

    .prologue
    .line 2068643
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "home_creation_logger_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2068644
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "home_creation_logger_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/home/HomeActivityLoggerData;

    return-object v0
.end method

.method public final m()V
    .locals 4

    .prologue
    .line 2068645
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-wide v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2068646
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081726

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081727

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/places/create/home/HomeCreationActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2068647
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    invoke-virtual {v0}, LX/E0T;->u()V

    .line 2068648
    :goto_0
    return-void

    .line 2068649
    :cond_1
    invoke-direct {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->t()V

    goto :goto_0
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 2068650
    invoke-super {p0}, Lcom/facebook/places/create/home/HomeActivity;->o()V

    .line 2068651
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v0, v0, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2068652
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->s:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1060003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2068653
    :goto_0
    return-void

    .line 2068654
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->s:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1060008

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2068655
    const/16 v0, 0xc

    if-ne p1, v0, :cond_2

    .line 2068656
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 2068657
    const-string v0, "selected_city"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 2068658
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/places/create/home/HomeActivityModel;->b:Ljava/lang/String;

    .line 2068659
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/facebook/places/create/home/HomeActivityModel;->e:J

    .line 2068660
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    if-nez v1, :cond_0

    .line 2068661
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    new-instance v2, Landroid/location/Location;

    const-string v3, ""

    invoke-direct {v2, v3}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v2, v1, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    .line 2068662
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 2068663
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeActivity;->p:Lcom/facebook/places/create/home/HomeActivityModel;

    iget-object v1, v1, Lcom/facebook/places/create/home/HomeActivityModel;->f:Landroid/location/Location;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 2068664
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/places/create/home/HomeCreationActivity;->o()V

    .line 2068665
    iget-object v0, p0, Lcom/facebook/places/create/home/HomeActivity;->w:LX/E0T;

    .line 2068666
    iget-object v1, v0, LX/E0T;->a:LX/0Zb;

    const-string v2, "home_%s_city_updated"

    invoke-static {v0, v2}, LX/E0T;->b(LX/E0T;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, LX/E0T;->c(LX/E0T;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2068667
    :cond_1
    :goto_0
    return-void

    .line 2068668
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/places/create/home/HomeActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x52e83799

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2068669
    invoke-super {p0}, Lcom/facebook/places/create/home/HomeActivity;->onDestroy()V

    .line 2068670
    iget-object v1, p0, Lcom/facebook/places/create/home/HomeCreationActivity;->A:LX/E0q;

    invoke-virtual {v1}, LX/E0q;->a()V

    .line 2068671
    const/16 v1, 0x23

    const v2, 0x3f8d5d8c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
