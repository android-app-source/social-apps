.class public Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final A:LX/2h1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2h1",
            "<",
            "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/E19;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/privacy/PrivacyOperationsClient;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/94Z;

.field private u:Lcom/facebook/widget/listview/BetterListView;

.field private v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field private w:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;",
            ">;"
        }
    .end annotation
.end field

.field private x:Lcom/facebook/privacy/model/PrivacyOptionsResult;

.field private final y:LX/63W;

.field private final z:LX/63J;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2069460
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 2069461
    new-instance v0, LX/E16;

    invoke-direct {v0, p0}, LX/E16;-><init>(Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->y:LX/63W;

    .line 2069462
    new-instance v0, LX/E17;

    invoke-direct {v0, p0}, LX/E17;-><init>(Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->z:LX/63J;

    .line 2069463
    new-instance v0, LX/E18;

    invoke-direct {v0, p0}, LX/E18;-><init>(Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->A:LX/2h1;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2069457
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->r:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2069458
    iget-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->s:LX/0Sh;

    iget-object v2, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->A:LX/2h1;

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2069459
    return-void
.end method

.method private static a(Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;LX/E19;LX/0Or;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Sh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;",
            "LX/E19;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2069464
    iput-object p1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->p:LX/E19;

    iput-object p2, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->q:LX/0Or;

    iput-object p3, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->r:Lcom/facebook/privacy/PrivacyOperationsClient;

    iput-object p4, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->s:LX/0Sh;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;

    new-instance p1, LX/E19;

    invoke-static {v2}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v2}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    invoke-static {v2}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v3

    check-cast v3, LX/8Sa;

    invoke-direct {p1, v0, v1, v3}, LX/E19;-><init>(LX/0wM;Landroid/view/LayoutInflater;LX/8Sa;)V

    move-object v0, p1

    check-cast v0, LX/E19;

    const/16 v1, 0x12cb

    invoke-static {v2, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v2}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v1

    check-cast v1, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v2}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v2

    check-cast v2, LX/0Sh;

    invoke-static {p0, v0, v3, v1, v2}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->a(Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;LX/E19;LX/0Or;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Sh;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;Lcom/facebook/privacy/model/PrivacyOptionsResult;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2069437
    iput-object p1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->x:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 2069438
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->x:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v5, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    move v2, v3

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2069439
    new-instance v7, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;

    invoke-direct {v7, v0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;-><init>(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2069440
    iget-object v0, v7, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v8, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v8}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2069441
    iput-boolean v1, v7, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->b:Z

    .line 2069442
    move v0, v1

    .line 2069443
    :goto_1
    iget-object v2, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->w:Ljava/util/List;

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069444
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_0

    .line 2069445
    :cond_0
    if-nez v2, :cond_1

    .line 2069446
    new-instance v0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;

    iget-object v2, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-direct {v0, v2}, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;-><init>(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2069447
    iput-boolean v1, v0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->b:Z

    .line 2069448
    iget-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->w:Ljava/util/List;

    iget-object v2, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->x:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v2, v2, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2069449
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->x:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v2, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    move v1, v3

    :goto_2
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2069450
    iget-object v3, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v3}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2069451
    new-instance v3, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;

    invoke-direct {v3, v0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;-><init>(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2069452
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->w:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2069453
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2069454
    :cond_3
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->p:LX/E19;

    iget-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->w:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/E19;->a(Ljava/util/List;)V

    .line 2069455
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->p:LX/E19;

    const v1, 0x13f7714b

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2069456
    return-void

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public static b(Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;)V
    .locals 3

    .prologue
    .line 2069432
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2069433
    const-string v1, "selected_privacy"

    iget-object v2, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2069434
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 2069435
    invoke-virtual {p0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->finish()V

    .line 2069436
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2069381
    invoke-static {p0, p0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2069382
    const v0, 0x7f03102a

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->setContentView(I)V

    .line 2069383
    invoke-virtual {p0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2069384
    if-nez p1, :cond_0

    .line 2069385
    invoke-virtual {p0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_initial_privacy"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2069386
    :goto_0
    const v0, 0x7f0d0a7f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/94V;

    .line 2069387
    iget-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->z:LX/63J;

    invoke-virtual {v0, v1}, LX/94V;->setOnBackPressedListener(LX/63J;)V

    .line 2069388
    new-instance v1, LX/94Z;

    new-instance v2, LX/94Y;

    invoke-direct {v2}, LX/94Y;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08172f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2069389
    iput-object v3, v2, LX/94Y;->a:Ljava/lang/String;

    .line 2069390
    move-object v2, v2

    .line 2069391
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v3

    .line 2069392
    iput-object v3, v2, LX/94Y;->d:LX/94c;

    .line 2069393
    move-object v2, v2

    .line 2069394
    invoke-virtual {v2}, LX/94Y;->a()LX/94X;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/94Z;-><init>(LX/94V;LX/94X;)V

    iput-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->t:LX/94Z;

    .line 2069395
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->t:LX/94Z;

    iget-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->t:LX/94Z;

    .line 2069396
    iget-object v2, v1, LX/94Z;->b:LX/94X;

    move-object v1, v2

    .line 2069397
    invoke-virtual {v1}, LX/94X;->a()LX/94Y;

    move-result-object v1

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v3, 0x7f080031

    invoke-virtual {p0, v3}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2069398
    iput-object v3, v2, LX/108;->g:Ljava/lang/String;

    .line 2069399
    move-object v2, v2

    .line 2069400
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 2069401
    iput-object v2, v1, LX/94Y;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2069402
    move-object v1, v1

    .line 2069403
    iget-object v2, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->y:LX/63W;

    .line 2069404
    iput-object v2, v1, LX/94Y;->c:LX/63W;

    .line 2069405
    move-object v1, v1

    .line 2069406
    invoke-virtual {v1}, LX/94Y;->a()LX/94X;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/94Z;->a(LX/94X;)V

    .line 2069407
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->u:Lcom/facebook/widget/listview/BetterListView;

    .line 2069408
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->u:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->p:LX/E19;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2069409
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->u:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 2069410
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2069411
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->w:Ljava/util/List;

    .line 2069412
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->p:LX/E19;

    iget-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->w:Ljava/util/List;

    invoke-virtual {v0, v1}, LX/E19;->a(Ljava/util/List;)V

    .line 2069413
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->p:LX/E19;

    const v1, 0x25a1d8b7

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2069414
    invoke-direct {p0}, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->a()V

    .line 2069415
    return-void

    .line 2069416
    :cond_0
    const-string v0, "state_current_privacy"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    goto/16 :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2069420
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->u:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;

    .line 2069421
    iget-object v1, v0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v2, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v1, v2}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2069422
    :goto_0
    return-void

    .line 2069423
    :cond_0
    iget-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->w:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;

    .line 2069424
    iget-object v3, v1, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v4, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v3, v4}, LX/2cA;->a(LX/1oU;LX/1oU;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2069425
    const/4 v3, 0x0

    .line 2069426
    iput-boolean v3, v1, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->b:Z

    .line 2069427
    goto :goto_1

    .line 2069428
    :cond_2
    iget-object v1, v0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2069429
    const/4 v1, 0x1

    .line 2069430
    iput-boolean v1, v0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->b:Z

    .line 2069431
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->p:LX/E19;

    const v1, 0x4d0f6af1    # 1.503844E8f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2069417
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 2069418
    const-string v0, "state_current_privacy"

    iget-object v1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerActivity;->v:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2069419
    return-void
.end method
