.class public Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2069494
    new-instance v0, LX/E1A;

    invoke-direct {v0}, LX/E1A;-><init>()V

    sput-object v0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2069495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2069496
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2069497
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->b:Z

    .line 2069498
    return-void

    .line 2069499
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 1

    .prologue
    .line 2069500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2069501
    iput-object p1, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2069502
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->b:Z

    .line 2069503
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2069504
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2069505
    iget-object v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->a:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 2069506
    iget-boolean v0, p0, Lcom/facebook/places/create/privacypicker/PrivacyPickerRowData;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2069507
    return-void

    .line 2069508
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
