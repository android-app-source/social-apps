.class public final Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x51e91acb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1984101
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1984100
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1984098
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1984099
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1984096
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;->e:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;->e:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    .line 1984097
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;->e:Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1984102
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;->f:Ljava/lang/String;

    .line 1984103
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1984088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1984089
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;->a()Lcom/facebook/graphql/enums/GraphQLWeatherConditionsCode;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1984090
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1984091
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1984092
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1984093
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1984094
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1984095
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1984085
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1984086
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1984087
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1984082
    new-instance v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;

    invoke-direct {v0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel$NodesModel$WeatherConditionModel;-><init>()V

    .line 1984083
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1984084
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1984081
    const v0, -0x6ceebcae

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1984080
    const v0, -0x503eb459

    return v0
.end method
