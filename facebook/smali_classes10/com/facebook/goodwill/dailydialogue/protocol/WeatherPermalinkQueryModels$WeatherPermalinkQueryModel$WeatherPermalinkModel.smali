.class public final Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3b07c71a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1984240
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1984239
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1984237
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1984238
    return-void
.end method

.method private j()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1984194
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    .line 1984195
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1984235
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->g:Ljava/lang/String;

    .line 1984236
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1984233
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->h:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->h:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    .line 1984234
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->h:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1984221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1984222
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->j()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1984223
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1984224
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1984225
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->l()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1984226
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1984227
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1984228
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1984229
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1984230
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1984231
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1984232
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1984203
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1984204
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->j()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1984205
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->j()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    .line 1984206
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->j()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1984207
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;

    .line 1984208
    iput-object v0, v1, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$CurrentCityModel;

    .line 1984209
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1984210
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    .line 1984211
    invoke-virtual {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1984212
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;

    .line 1984213
    iput-object v0, v1, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->f:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    .line 1984214
    :cond_1
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->l()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1984215
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->l()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    .line 1984216
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->l()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1984217
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;

    .line 1984218
    iput-object v0, v1, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->h:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    .line 1984219
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1984220
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1984201
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->f:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->f:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    .line 1984202
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;->f:Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1984198
    new-instance v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;

    invoke-direct {v0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel;-><init>()V

    .line 1984199
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1984200
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1984197
    const v0, 0x22b74ca0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1984196
    const v0, 0x51f6ce99

    return v0
.end method
