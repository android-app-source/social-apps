.class public final Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3b2eb3bb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1984169
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1984170
    const-class v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1984171
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1984172
    return-void
.end method

.method private a()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1984173
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    iput-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    .line 1984174
    iget-object v0, p0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1984175
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1984176
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;->a()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1984177
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1984178
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1984179
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1984180
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1984181
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1984182
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;->a()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1984183
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;->a()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    .line 1984184
    invoke-direct {p0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;->a()Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1984185
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    .line 1984186
    iput-object v0, v1, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;->e:Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel$WeatherSettingSavedCitiesModel;

    .line 1984187
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1984188
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1984189
    new-instance v0, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;

    invoke-direct {v0}, Lcom/facebook/goodwill/dailydialogue/protocol/WeatherPermalinkQueryModels$WeatherPermalinkQueryModel$WeatherPermalinkModel$WeatherPermalinkSettingModel;-><init>()V

    .line 1984190
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1984191
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1984192
    const v0, -0x733f63cc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1984193
    const v0, 0x15fb757e

    return v0
.end method
