.class public Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol_TrackingSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1983539
    const-class v0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;

    new-instance v1, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol_TrackingSerializer;

    invoke-direct {v1}, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol_TrackingSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1983540
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1983546
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1983547
    if-nez p0, :cond_0

    .line 1983548
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1983549
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1983550
    invoke-static {p0, p1, p2}, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol_TrackingSerializer;->b(Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;LX/0nX;LX/0my;)V

    .line 1983551
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1983552
    return-void
.end method

.method private static b(Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1983542
    const-string v0, "daily_dialogue_lightweight_unit_type"

    iget-object v1, p0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;->type:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1983543
    const-string v0, "daily_dialogue_lightweight_unit_id"

    iget-object v1, p0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;->id:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1983544
    const-string v0, "daily_dialogue_lightweight_extra"

    iget-object v1, p0, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;->extra:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1983545
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1983541
    check-cast p1, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;

    invoke-static {p1, p2, p3}, Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol_TrackingSerializer;->a(Lcom/facebook/goodwill/dailydialogue/data/DailyDialogueViewedMutationProtocol$Tracking;LX/0nX;LX/0my;)V

    return-void
.end method
