.class public Lcom/facebook/bookmark/ui/analytics/BookmarkAdClickEvent;
.super Lcom/facebook/analytics/logger/HoneyClientEvent;
.source ""


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 4

    .prologue
    .line 2158486
    const-string v0, "ad_click"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2158487
    const-string v0, "bookmark_id"

    iget-wide v2, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158488
    const-string v0, "bookmark_type"

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->type:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158489
    const-string v0, "client_token"

    iget-object v1, p1, Lcom/facebook/bookmark/model/Bookmark;->clientToken:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158490
    return-void
.end method
