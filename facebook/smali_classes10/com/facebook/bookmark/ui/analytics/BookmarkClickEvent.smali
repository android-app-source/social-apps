.class public Lcom/facebook/bookmark/ui/analytics/BookmarkClickEvent;
.super Lcom/facebook/analytics/logger/HoneyClientEvent;
.source ""


# direct methods
.method public constructor <init>(LX/2l4;Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 2

    .prologue
    .line 2158491
    const-string v0, "bookmark_click"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2158492
    iget-wide v0, p2, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158493
    const-string v0, "sidebar_menu"

    .line 2158494
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2158495
    const-string v0, "mobile_platform"

    const-string v1, "android"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158496
    const-string v0, "item_category"

    invoke-static {p2}, LX/2l4;->c(Lcom/facebook/bookmark/model/Bookmark;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158497
    const-string v0, "item_name"

    iget-object v1, p2, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158498
    const-string v0, "mobile_location_type"

    const-string v1, "sidebar"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158499
    const-string v0, "nav_section_mobile_sidebar"

    invoke-virtual {p1, p2}, LX/2l4;->b(Lcom/facebook/bookmark/model/Bookmark;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158500
    const-string v0, "mobile_location"

    .line 2158501
    invoke-virtual {p1, p2}, LX/2l4;->a(Lcom/facebook/bookmark/model/Bookmark;)Lcom/facebook/bookmark/model/BookmarksGroup;

    move-result-object v1

    .line 2158502
    if-eqz v1, :cond_0

    invoke-virtual {v1, p2}, Lcom/facebook/bookmark/model/BookmarksGroup;->c(Lcom/facebook/bookmark/model/Bookmark;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 2158503
    :goto_0
    if-eqz v1, :cond_2

    const-string v1, "load"

    :goto_1
    move-object v1, v1

    .line 2158504
    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158505
    return-void

    .line 2158506
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2158507
    :cond_2
    const-string v1, "see_all"

    goto :goto_1
.end method
