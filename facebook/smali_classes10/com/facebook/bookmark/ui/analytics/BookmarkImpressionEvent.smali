.class public Lcom/facebook/bookmark/ui/analytics/BookmarkImpressionEvent;
.super Lcom/facebook/analytics/logger/HoneyClientEvent;
.source ""


# direct methods
.method public constructor <init>(LX/2l4;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2l4;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2158508
    const-string v0, "bookmark_impression"

    invoke-direct {p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2158509
    const-string v0, "sidebar_menu"

    .line 2158510
    iput-object v0, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2158511
    const-string v0, "mobile_platform"

    const-string v1, "android"

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158512
    new-instance v3, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2158513
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    .line 2158514
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 2158515
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 2158516
    new-instance v5, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v5, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2158517
    invoke-virtual {p1, v0}, LX/2l4;->a(Lcom/facebook/bookmark/model/Bookmark;)Lcom/facebook/bookmark/model/BookmarksGroup;

    move-result-object v1

    .line 2158518
    const-string v6, "index"

    add-int/lit8 v7, v2, 0x1

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2158519
    const-string v6, "name"

    iget-object v7, v0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2158520
    const-string v6, "group"

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    :goto_1
    invoke-virtual {v5, v6, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2158521
    const-string v1, "category"

    invoke-static {v0}, LX/2l4;->c(Lcom/facebook/bookmark/model/Bookmark;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2158522
    const-string v1, "nav_section"

    invoke-virtual {p1, v0}, LX/2l4;->b(Lcom/facebook/bookmark/model/Bookmark;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2158523
    invoke-virtual {v3, v5}, LX/162;->a(LX/0lF;)LX/162;

    .line 2158524
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2158525
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 2158526
    :cond_1
    const-string v0, "bookmarks"

    invoke-virtual {p0, v0, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2158527
    return-void
.end method
