.class public final Lcom/facebook/bookmark/ui/BaseViewItemFactory$FamilyBridgesProfileViewItem;
.super LX/2lH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2lH",
        "<",
        "LX/EhV;",
        "Lcom/facebook/bookmark/model/Bookmark;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;


# direct methods
.method public constructor <init>(Lcom/facebook/bookmark/ui/BaseViewItemFactory;LX/2lb;Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 2

    .prologue
    .line 2158459
    iput-object p1, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory$FamilyBridgesProfileViewItem;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    .line 2158460
    const v0, 0x7f0301bc

    iget-object v1, p1, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b:Landroid/view/LayoutInflater;

    invoke-direct {p0, p2, v0, p3, v1}, LX/2lH;-><init>(LX/2lb;ILjava/lang/Object;Landroid/view/LayoutInflater;)V

    .line 2158461
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2158462
    new-instance v0, LX/EhV;

    check-cast p1, Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-direct {v0, p1}, LX/EhV;-><init>(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2158463
    check-cast p1, LX/EhV;

    .line 2158464
    iget-object v0, p1, LX/EhV;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    const v1, 0x7f020b5e

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailPlaceholderResource(I)V

    .line 2158465
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2158466
    :try_start_0
    iget-object v1, p1, LX/EhV;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->pic:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2158467
    :cond_0
    :goto_0
    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->miniAppIcon:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2158468
    iget-object v1, p1, LX/EhV;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->miniAppIcon:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2158469
    iget-object v0, p1, LX/EhV;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 2158470
    :goto_1
    iget-object v1, p1, LX/EhV;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2158471
    iget-object v1, p1, LX/EhV;->c:Landroid/widget/TextView;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v0, v0, Lcom/facebook/bookmark/model/Bookmark;->subName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2158472
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory$FamilyBridgesProfileViewItem;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v2, p1, LX/EhV;->d:Landroid/widget/TextView;

    invoke-static {v1, v0, v2}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;Lcom/facebook/bookmark/model/Bookmark;Landroid/widget/TextView;)V

    .line 2158473
    iget-object v1, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory$FamilyBridgesProfileViewItem;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    iget-object v0, p0, LX/2lH;->d:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-object v2, p1, LX/EhV;->d:Landroid/widget/TextView;

    invoke-static {v1, v0, v2}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;Lcom/facebook/bookmark/model/Bookmark;Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    .line 2158474
    iget-object v1, p1, LX/EhV;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2158475
    iget-object v0, p0, Lcom/facebook/bookmark/ui/BaseViewItemFactory$FamilyBridgesProfileViewItem;->a:Lcom/facebook/bookmark/ui/BaseViewItemFactory;

    invoke-static {v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->a$redex0(Lcom/facebook/bookmark/ui/BaseViewItemFactory;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2158476
    iget-object v0, p1, LX/EhV;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-static {v0}, Lcom/facebook/bookmark/ui/BaseViewItemFactory;->b(Lcom/facebook/fbui/widget/contentview/ContentView;)V

    .line 2158477
    :cond_1
    return-void

    .line 2158478
    :catch_0
    iget-object v0, p1, LX/EhV;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    goto :goto_0

    .line 2158479
    :cond_2
    iget-object v0, p1, LX/EhV;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_1
.end method
