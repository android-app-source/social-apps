.class public Lcom/facebook/bookmark/model/BookmarksGroup;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/model/BookmarksGroupDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Z

.field private b:I

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field private mAll:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "all"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;"
        }
    .end annotation
.end field

.field private mVisibleCount:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "visible_count"
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2158256
    const-class v0, Lcom/facebook/bookmark/model/BookmarksGroupDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2158290
    new-instance v0, LX/EhK;

    invoke-direct {v0}, LX/EhK;-><init>()V

    sput-object v0, Lcom/facebook/bookmark/model/BookmarksGroup;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 2158291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2158292
    iput-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    .line 2158293
    iput-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    .line 2158294
    iput v1, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    .line 2158295
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    .line 2158296
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    .line 2158297
    iput-boolean v1, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->a:Z

    .line 2158298
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2158299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2158300
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    .line 2158301
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    .line 2158302
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    .line 2158303
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    .line 2158304
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->a:Z

    .line 2158305
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    sget-object v1, Lcom/facebook/bookmark/model/Bookmark;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 2158306
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    .line 2158307
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2158308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2158309
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    .line 2158310
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    .line 2158311
    iput p3, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    .line 2158312
    iput p4, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    .line 2158313
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    .line 2158314
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->a:Z

    .line 2158315
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2158316
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/bookmark/model/BookmarksGroup;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;)V

    .line 2158317
    return-void
.end method

.method private static a(Lcom/facebook/bookmark/model/BookmarksGroup;Lcom/facebook/bookmark/model/BookmarksGroup;)Z
    .locals 2

    .prologue
    .line 2158318
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    iget v1, p1, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    iget-object v1, p1, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/facebook/bookmark/model/Bookmark;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/BookmarksGroup;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2158319
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v2, v3

    .line 2158320
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2158321
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-static {v0, v1}, Lcom/facebook/bookmark/model/BookmarksGroup;->a(Lcom/facebook/bookmark/model/BookmarksGroup;Lcom/facebook/bookmark/model/BookmarksGroup;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2158322
    :cond_0
    :goto_1
    return v3

    .line 2158323
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2158324
    :cond_2
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private c(J)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2158325
    move v1, v2

    :goto_0
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2158326
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v4, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v4, p1

    if-nez v0, :cond_1

    .line 2158327
    const/4 v2, 0x1

    .line 2158328
    :cond_0
    return v2

    .line 2158329
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 2158330
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->a:Z

    .line 2158331
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2158332
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    goto :goto_0
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 2158333
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2158334
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    iget-wide v2, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v2, p1

    if-nez v0, :cond_1

    .line 2158335
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2158336
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    .line 2158337
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    if-ge v1, v0, :cond_0

    .line 2158338
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    .line 2158339
    :cond_0
    return-void

    .line 2158340
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/bookmark/model/Bookmark;)V
    .locals 2

    .prologue
    .line 2158282
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2158283
    :goto_0
    return-void

    .line 2158284
    :cond_0
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    iget v1, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2158285
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    .line 2158286
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    .line 2158287
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->a:Z

    goto :goto_0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2158288
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2158289
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2158281
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    return v0
.end method

.method public final b(J)Z
    .locals 5

    .prologue
    .line 2158277
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/model/Bookmark;

    .line 2158278
    iget-wide v2, v0, Lcom/facebook/bookmark/model/Bookmark;->id:J

    cmp-long v0, v2, p1

    if-nez v0, :cond_0

    .line 2158279
    const/4 v0, 0x1

    .line 2158280
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/bookmark/model/Bookmark;)Z
    .locals 2

    .prologue
    .line 2158276
    iget-wide v0, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-virtual {p0, v0, v1}, Lcom/facebook/bookmark/model/BookmarksGroup;->b(J)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2158275
    iget-boolean v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->a:Z

    return v0
.end method

.method public final c(Lcom/facebook/bookmark/model/Bookmark;)Z
    .locals 2

    .prologue
    .line 2158274
    iget-wide v0, p1, Lcom/facebook/bookmark/model/Bookmark;->id:J

    invoke-direct {p0, v0, v1}, Lcom/facebook/bookmark/model/BookmarksGroup;->c(J)Z

    move-result v0

    return v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2158273
    invoke-virtual {p0}, Lcom/facebook/bookmark/model/BookmarksGroup;->f()Lcom/facebook/bookmark/model/BookmarksGroup;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2158272
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2158271
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/model/Bookmark;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2158270
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/facebook/bookmark/model/BookmarksGroup;
    .locals 6

    .prologue
    .line 2158265
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/facebook/bookmark/model/BookmarksGroup;->d()Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2158266
    new-instance v0, Lcom/facebook/bookmark/model/BookmarksGroup;

    iget-object v1, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    iget v3, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    invoke-virtual {p0}, Lcom/facebook/bookmark/model/BookmarksGroup;->a()I

    move-result v4

    invoke-direct/range {v0 .. v5}, Lcom/facebook/bookmark/model/BookmarksGroup;-><init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;)V

    .line 2158267
    iget-boolean v1, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->a:Z

    if-eqz v1, :cond_0

    .line 2158268
    invoke-direct {v0}, Lcom/facebook/bookmark/model/BookmarksGroup;->i()V

    .line 2158269
    :cond_0
    return-object v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 2158264
    invoke-virtual {p0}, Lcom/facebook/bookmark/model/BookmarksGroup;->a()I

    move-result v0

    iget v1, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 2158263
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    iget-object v1, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2158257
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2158258
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2158259
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mVisibleCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2158260
    iget-object v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->mAll:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2158261
    iget v0, p0, Lcom/facebook/bookmark/model/BookmarksGroup;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2158262
    return-void
.end method
