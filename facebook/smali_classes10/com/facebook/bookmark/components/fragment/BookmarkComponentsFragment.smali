.class public Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;
.super Lcom/facebook/components/list/fb/fragment/ListComponentFragment;
.source ""


# instance fields
.field public a:LX/2lX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0jh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Egl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2156068
    invoke-direct {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/BcP;LX/BcQ;)LX/BcO;
    .locals 7

    .prologue
    .line 2156069
    iget-object v0, p0, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->e:LX/Egl;

    .line 2156070
    new-instance v1, LX/Egj;

    invoke-direct {v1, v0}, LX/Egj;-><init>(LX/Egl;)V

    .line 2156071
    sget-object v2, LX/Egl;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Egk;

    .line 2156072
    if-nez v2, :cond_0

    .line 2156073
    new-instance v2, LX/Egk;

    invoke-direct {v2}, LX/Egk;-><init>()V

    .line 2156074
    :cond_0
    iput-object v1, v2, LX/BcN;->a:LX/BcO;

    .line 2156075
    iput-object v1, v2, LX/Egk;->a:LX/Egj;

    .line 2156076
    iget-object v0, v2, LX/Egk;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2156077
    move-object v1, v2

    .line 2156078
    move-object v0, v1

    .line 2156079
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->b:LX/0ad;

    invoke-static {v1, v2}, LX/FBm;->a(Landroid/content/Context;LX/0ad;)Ljava/lang/String;

    move-result-object v1

    .line 2156080
    iget-object v2, v0, LX/Egk;->a:LX/Egj;

    iput-object v1, v2, LX/Egj;->b:Ljava/lang/String;

    .line 2156081
    iget-object v2, v0, LX/Egk;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2156082
    move-object v0, v0

    .line 2156083
    iget-object v1, p0, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->a:LX/2lX;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->b:LX/0ad;

    invoke-static {v3}, LX/FBm;->a(LX/0ad;)Z

    move-result v3

    iget-object v4, p0, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->c:LX/0tK;

    invoke-static {v4}, LX/FBm;->a(LX/0tK;)Z

    move-result v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->d:LX/0jh;

    invoke-static {v5, v6}, LX/FBm;->a(Landroid/content/Context;LX/0jh;)Z

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/2lX;->a(Landroid/content/Context;ZZZ)Ljava/util/List;

    move-result-object v1

    .line 2156084
    iget-object v2, v0, LX/Egk;->a:LX/Egj;

    iput-object v1, v2, LX/Egj;->c:Ljava/util/List;

    .line 2156085
    iget-object v2, v0, LX/Egk;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2156086
    move-object v0, v0

    .line 2156087
    invoke-virtual {v0, p2}, LX/Egk;->b(LX/BcQ;)LX/Egk;

    move-result-object v0

    invoke-virtual {v0}, LX/Egk;->b()LX/BcO;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2156088
    invoke-super {p0, p1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(Landroid/os/Bundle;)V

    .line 2156089
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;

    invoke-static {v0}, LX/2lX;->a(LX/0QB;)LX/2lX;

    move-result-object v3

    check-cast v3, LX/2lX;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0tK;->a(LX/0QB;)LX/0tK;

    move-result-object v5

    check-cast v5, LX/0tK;

    invoke-static {v0}, LX/0jg;->a(LX/0QB;)LX/0jh;

    move-result-object p1

    check-cast p1, LX/0jh;

    invoke-static {v0}, LX/Egl;->a(LX/0QB;)LX/Egl;

    move-result-object v0

    check-cast v0, LX/Egl;

    iput-object v3, v2, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->a:LX/2lX;

    iput-object v4, v2, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->b:LX/0ad;

    iput-object v5, v2, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->c:LX/0tK;

    iput-object p1, v2, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->d:LX/0jh;

    iput-object v0, v2, Lcom/facebook/bookmark/components/fragment/BookmarkComponentsFragment;->e:LX/Egl;

    .line 2156090
    return-void
.end method

.method public final d()LX/BcG;
    .locals 1

    .prologue
    .line 2156091
    new-instance v0, LX/BdH;

    invoke-direct {v0, p0}, LX/BdH;-><init>(Lcom/facebook/components/list/fb/fragment/ListComponentFragment;)V

    return-object v0
.end method
