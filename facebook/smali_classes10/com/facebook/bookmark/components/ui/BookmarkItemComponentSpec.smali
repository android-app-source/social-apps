.class public Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Ljava/util/regex/Pattern;

.field private static k:LX/0Xm;


# instance fields
.field public c:LX/EtW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation runtime Lcom/facebook/base/activity/ReactFragmentActivity;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2lA;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17X;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2157976
    const-class v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BookmarkItem"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2157977
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/0ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[^?]+)(\\?.*)?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->b:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2157978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2157979
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2157980
    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->d:LX/0Ot;

    .line 2157981
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2157982
    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->e:LX/0Ot;

    .line 2157983
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2157984
    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->f:LX/0Ot;

    .line 2157985
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2157986
    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->g:LX/0Ot;

    .line 2157987
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2157988
    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->h:LX/0Ot;

    .line 2157989
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2157990
    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->i:LX/0Ot;

    .line 2157991
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2157992
    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->j:LX/0Ot;

    .line 2157993
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;
    .locals 11

    .prologue
    .line 2157994
    const-class v1, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;

    monitor-enter v1

    .line 2157995
    :try_start_0
    sget-object v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2157996
    sput-object v2, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2157997
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2157998
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2157999
    new-instance v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;

    invoke-direct {v3}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;-><init>()V

    .line 2158000
    invoke-static {v0}, LX/EtW;->a(LX/0QB;)LX/EtW;

    move-result-object v4

    check-cast v4, LX/EtW;

    const/16 v5, 0x1032

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xc

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xac0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x78

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xc49

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 p0, 0x455

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2158001
    iput-object v4, v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->c:LX/EtW;

    iput-object v5, v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->d:LX/0Ot;

    iput-object v6, v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->e:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->f:LX/0Ot;

    iput-object v8, v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->g:LX/0Ot;

    iput-object v9, v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->h:LX/0Ot;

    iput-object v10, v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->i:LX/0Ot;

    iput-object p0, v3, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->j:LX/0Ot;

    .line 2158002
    move-object v0, v3

    .line 2158003
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2158004
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2158005
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2158006
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(JLjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2158007
    sget-object v1, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentSpec;->b:Ljava/util/regex/Pattern;

    if-nez p2, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 2158008
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2158009
    :cond_0
    :goto_1
    return-object p2

    :cond_1
    move-object v0, p2

    .line 2158010
    goto :goto_0

    .line 2158011
    :cond_2
    const-wide v0, 0x211dfd19646adL

    cmp-long v0, p0, v0

    if-nez v0, :cond_3

    .line 2158012
    sget-object v0, LX/0ax;->dX:Ljava/lang/String;

    const-string v1, "bookmark"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_1

    .line 2158013
    :cond_3
    const-wide v0, 0x378ae22b932d7L

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 2158014
    sget-object p2, LX/0ax;->iT:Ljava/lang/String;

    goto :goto_1
.end method
