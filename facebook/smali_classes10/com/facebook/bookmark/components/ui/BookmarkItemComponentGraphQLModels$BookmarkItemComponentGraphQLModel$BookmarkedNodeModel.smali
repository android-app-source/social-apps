.class public final Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x47608616
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Lcom/facebook/graphql/enums/GraphQLFriendListType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2157563
    const-class v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2157588
    const-class v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2157586
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2157587
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2157583
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 2157584
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2157585
    :cond_0
    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2157581
    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->f:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->f:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    .line 2157582
    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->f:Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLFriendListType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2157579
    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendListType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendListType;

    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    .line 2157580
    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->i:Lcom/facebook/graphql/enums/GraphQLFriendListType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 2157564
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2157565
    invoke-direct {p0}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2157566
    invoke-direct {p0}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->k()Lcom/facebook/graphql/enums/GraphQLBookmarkHighlightStyle;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2157567
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2157568
    invoke-direct {p0}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->l()Lcom/facebook/graphql/enums/GraphQLFriendListType;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 2157569
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2157570
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2157571
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2157572
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2157573
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2157574
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2157575
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2157576
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2157577
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2157578
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2157544
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2157545
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2157546
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2157562
    new-instance v0, LX/Eh0;

    invoke-direct {v0, p1}, LX/Eh0;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2157589
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2157559
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2157560
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->h:Z

    .line 2157561
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2157557
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2157558
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2157556
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2157553
    new-instance v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;-><init>()V

    .line 2157554
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2157555
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2157551
    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->g:Ljava/lang/String;

    .line 2157552
    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2157549
    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->j:Ljava/lang/String;

    .line 2157550
    iget-object v0, p0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel$BookmarkedNodeModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2157548
    const v0, -0x7db588b0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2157547
    const v0, 0x252222

    return v0
.end method
