.class public Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/BdA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BdA",
            "<",
            "Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/Egq;

.field public final c:LX/EgS;

.field public final d:LX/EtW;

.field private e:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public constructor <init>(LX/BdA;LX/Egq;LX/EgS;LX/EtW;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2157261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2157262
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fb4a"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2157263
    iput-object p1, p0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->a:LX/BdA;

    .line 2157264
    iput-object p2, p0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->b:LX/Egq;

    .line 2157265
    iput-object p3, p0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->c:LX/EgS;

    .line 2157266
    iput-object p4, p0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->d:LX/EtW;

    .line 2157267
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;
    .locals 7

    .prologue
    .line 2157268
    const-class v1, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;

    monitor-enter v1

    .line 2157269
    :try_start_0
    sget-object v0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2157270
    sput-object v2, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2157271
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2157272
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2157273
    new-instance p0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;

    invoke-static {v0}, LX/BdA;->a(LX/0QB;)LX/BdA;

    move-result-object v3

    check-cast v3, LX/BdA;

    invoke-static {v0}, LX/Egq;->a(LX/0QB;)LX/Egq;

    move-result-object v4

    check-cast v4, LX/Egq;

    invoke-static {v0}, LX/EgS;->a(LX/0QB;)LX/EgS;

    move-result-object v5

    check-cast v5, LX/EgS;

    invoke-static {v0}, LX/EtW;->a(LX/0QB;)LX/EtW;

    move-result-object v6

    check-cast v6, LX/EtW;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;-><init>(LX/BdA;LX/Egq;LX/EgS;LX/EtW;)V

    .line 2157274
    move-object v0, p0

    .line 2157275
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2157276
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2157277
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2157278
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
