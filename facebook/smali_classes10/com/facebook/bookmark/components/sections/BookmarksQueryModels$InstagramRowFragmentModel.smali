.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x75c9f104
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156845
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156846
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2156849
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2156850
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156847
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->e:Ljava/lang/String;

    .line 2156848
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156841
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    .line 2156842
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156843
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->g:Ljava/lang/String;

    .line 2156844
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2156826
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156827
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2156828
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2156829
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2156830
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2156831
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2156832
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2156833
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2156834
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156835
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2156818
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156819
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2156820
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    .line 2156821
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2156822
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    .line 2156823
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel$ProfilePictureModel;

    .line 2156824
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156825
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156817
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2156836
    new-instance v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;-><init>()V

    .line 2156837
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2156838
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2156839
    const v0, 0x7b961611

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2156840
    const v0, 0x19716e7d

    return v0
.end method
