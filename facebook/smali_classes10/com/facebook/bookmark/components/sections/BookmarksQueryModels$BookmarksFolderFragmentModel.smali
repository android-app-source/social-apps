.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x38dff3bd
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156451
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156450
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2156448
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2156449
    return-void
.end method

.method private j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156446
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    .line 2156447
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156444
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->f:Ljava/lang/String;

    .line 2156445
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2156436
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156437
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2156438
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2156439
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2156440
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2156441
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2156442
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156443
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2156428
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156429
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2156430
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    .line 2156431
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2156432
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156433
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    .line 2156434
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156435
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156427
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2156422
    new-instance v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;-><init>()V

    .line 2156423
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2156424
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2156426
    const v0, -0xa781d51

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2156425
    const v0, 0x7891a3e4

    return v0
.end method
