.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x634b1160
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I

.field private h:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156306
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156305
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2156303
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2156304
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156301
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->e:Ljava/lang/String;

    .line 2156302
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156299
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->f:Ljava/lang/String;

    .line 2156300
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156297
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->h:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->h:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    .line 2156298
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->h:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156295
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->i:Ljava/lang/String;

    .line 2156296
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2156282
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156283
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2156284
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2156285
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2156286
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2156287
    const/4 v4, 0x5

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2156288
    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 2156289
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2156290
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->g:I

    invoke-virtual {p1, v0, v1, v5}, LX/186;->a(III)V

    .line 2156291
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2156292
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2156293
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156294
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2156274
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156275
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2156276
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    .line 2156277
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2156278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    .line 2156279
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->h:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel$ProfilePictureModel;

    .line 2156280
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156281
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2156261
    new-instance v0, LX/EgW;

    invoke-direct {v0, p1}, LX/EgW;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156273
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2156270
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2156271
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;->g:I

    .line 2156272
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2156268
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2156269
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2156267
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2156264
    new-instance v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;-><init>()V

    .line 2156265
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2156266
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2156263
    const v0, 0x56d6383b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2156262
    const v0, 0x25d6af

    return v0
.end method
