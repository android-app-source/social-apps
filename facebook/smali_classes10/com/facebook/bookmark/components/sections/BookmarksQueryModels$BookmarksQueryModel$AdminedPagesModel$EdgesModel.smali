.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1040aec
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156509
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156508
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2156506
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2156507
    return-void
.end method

.method private a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156504
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    .line 2156505
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2156498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156499
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2156500
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2156501
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2156502
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156503
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2156490
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156491
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2156492
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    .line 2156493
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2156494
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;

    .line 2156495
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$AdminedPagesFragmentModel;

    .line 2156496
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156497
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2156487
    new-instance v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel$EdgesModel;-><init>()V

    .line 2156488
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2156489
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2156485
    const v0, 0x172018d3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2156486
    const v0, -0x7aaf70cd

    return v0
.end method
