.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x675eb0c2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156712
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156756
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2156754
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2156755
    return-void
.end method

.method private k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156752
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    .line 2156753
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    return-object v0
.end method

.method private l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156750
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->g:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->g:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156751
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->g:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156748
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->h:Ljava/lang/String;

    .line 2156749
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156746
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->i:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->i:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156747
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->i:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    return-object v0
.end method

.method private o()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156744
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->j:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->j:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    .line 2156745
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->j:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    return-object v0
.end method

.method private p()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156742
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->l:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->l:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    .line 2156743
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->l:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 2156722
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156723
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2156724
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2156725
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2156726
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2156727
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->n()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2156728
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->o()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2156729
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2156730
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->p()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2156731
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 2156732
    const/4 v8, 0x0

    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 2156733
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2156734
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2156735
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 2156736
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2156737
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2156738
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2156739
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2156740
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156741
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2156674
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156675
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2156676
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    .line 2156677
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2156678
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 2156679
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    .line 2156680
    :cond_0
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2156681
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    .line 2156682
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->k()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 2156683
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 2156684
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->f:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$AdminedPagesModel;

    .line 2156685
    :cond_1
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2156686
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156687
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->l()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 2156688
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 2156689
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->g:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156690
    :cond_2
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->n()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2156691
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->n()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156692
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->n()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 2156693
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 2156694
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->i:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156695
    :cond_3
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->o()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2156696
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->o()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    .line 2156697
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->o()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 2156698
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 2156699
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->j:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$InstagramRowFragmentModel;

    .line 2156700
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2156701
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156702
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 2156703
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 2156704
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->k:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156705
    :cond_5
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->p()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2156706
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->p()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    .line 2156707
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->p()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 2156708
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 2156709
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->l:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    .line 2156710
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156711
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    :cond_7
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156720
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    .line 2156721
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->e:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2156717
    new-instance v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;-><init>()V

    .line 2156718
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2156719
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2156716
    const v0, -0x54a2715e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2156715
    const v0, -0x6747e1ce

    return v0
.end method

.method public final j()Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156713
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->k:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->k:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    .line 2156714
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;->k:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel;

    return-object v0
.end method
