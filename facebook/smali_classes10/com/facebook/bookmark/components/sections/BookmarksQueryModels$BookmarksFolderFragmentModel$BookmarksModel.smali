.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x162ee2fa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156401
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156400
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2156398
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2156399
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2156373
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156374
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2156375
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2156376
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2156377
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 2156378
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156379
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2156396
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->e:Ljava/util/List;

    .line 2156397
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2156388
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156389
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2156390
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2156391
    if-eqz v1, :cond_0

    .line 2156392
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    .line 2156393
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->e:Ljava/util/List;

    .line 2156394
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156395
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2156385
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2156386
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;->f:I

    .line 2156387
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2156382
    new-instance v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel;-><init>()V

    .line 2156383
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2156384
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2156381
    const v0, -0x37480dea

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2156380
    const v0, 0x54a46d4d

    return v0
.end method
