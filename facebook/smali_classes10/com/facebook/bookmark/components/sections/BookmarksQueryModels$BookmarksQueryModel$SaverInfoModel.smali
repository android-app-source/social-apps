.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x34b54d78
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156609
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156610
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2156611
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2156612
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLSaveNuxState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156613
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;->f:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;->f:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    .line 2156614
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;->f:Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 2156615
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156616
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLSaveNuxState;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 2156617
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2156618
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2156619
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 2156620
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156621
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2156622
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156623
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156624
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 2156625
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2156626
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;->e:J

    .line 2156627
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2156628
    new-instance v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$SaverInfoModel;-><init>()V

    .line 2156629
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2156630
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2156631
    const v0, 0x516868e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2156632
    const v0, -0x7c9beb3d

    return v0
.end method
