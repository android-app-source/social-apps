.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x17e3be57
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156910
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156909
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2156907
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2156908
    return-void
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156905
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->e:Ljava/lang/String;

    .line 2156906
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 2156895
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156896
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2156897
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2156898
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 2156899
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2156900
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 2156901
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2156902
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 2156903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156904
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2156873
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156874
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2156875
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2156876
    invoke-virtual {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2156877
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    .line 2156878
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2156879
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156880
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 2156894
    new-instance v0, LX/EgX;

    invoke-direct {v0, p1}, LX/EgX;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156893
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 2156891
    invoke-virtual {p2}, LX/18L;->a()V

    .line 2156892
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 2156890
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2156887
    new-instance v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;-><init>()V

    .line 2156888
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2156889
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2156886
    const v0, 0x73afb8af

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2156885
    const v0, 0x285feb

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156883
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->f:Ljava/lang/String;

    .line 2156884
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156881
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2156882
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$ProfilePictureRowFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    return-object v0
.end method
