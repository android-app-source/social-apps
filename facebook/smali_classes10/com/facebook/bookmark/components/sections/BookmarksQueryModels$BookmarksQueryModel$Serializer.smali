.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2156672
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    new-instance v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2156673
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2156633
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2156635
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2156636
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2156637
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2156638
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2156639
    if-eqz v2, :cond_0

    .line 2156640
    const-string p0, "actor"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156641
    invoke-static {v1, v2, p1, p2}, LX/Egh;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2156642
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2156643
    if-eqz v2, :cond_1

    .line 2156644
    const-string p0, "admined_pages"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156645
    invoke-static {v1, v2, p1, p2}, LX/Egd;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2156646
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2156647
    if-eqz v2, :cond_2

    .line 2156648
    const-string p0, "appsBookmarkFolder"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156649
    invoke-static {v1, v2, p1, p2}, LX/Egb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2156650
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2156651
    if-eqz v2, :cond_3

    .line 2156652
    const-string p0, "bookmark_see_all_pages_rich_badging"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156653
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2156654
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2156655
    if-eqz v2, :cond_4

    .line 2156656
    const-string p0, "favoritesBookmarkFolder"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156657
    invoke-static {v1, v2, p1, p2}, LX/Egb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2156658
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2156659
    if-eqz v2, :cond_5

    .line 2156660
    const-string p0, "instagram_single_fbconnected_account"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156661
    invoke-static {v1, v2, p1, p2}, LX/Egg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2156662
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2156663
    if-eqz v2, :cond_6

    .line 2156664
    const-string p0, "productsBookmarkFolder"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156665
    invoke-static {v1, v2, p1, p2}, LX/Egb;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2156666
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2156667
    if-eqz v2, :cond_7

    .line 2156668
    const-string p0, "saver_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2156669
    invoke-static {v1, v2, p1}, LX/Ege;->a(LX/15i;ILX/0nX;)V

    .line 2156670
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2156671
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2156634
    check-cast p1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel$Serializer;->a(Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
