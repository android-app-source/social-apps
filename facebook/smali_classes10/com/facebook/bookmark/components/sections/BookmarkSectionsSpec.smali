.class public Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/list/annotations/GroupSectionSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public b:LX/EtW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Egy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2156167
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Sections"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2156168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2156169
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;
    .locals 5

    .prologue
    .line 2156170
    const-class v1, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;

    monitor-enter v1

    .line 2156171
    :try_start_0
    sget-object v0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2156172
    sput-object v2, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2156173
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2156174
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2156175
    new-instance p0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;

    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;-><init>()V

    .line 2156176
    invoke-static {v0}, LX/EtW;->a(LX/0QB;)LX/EtW;

    move-result-object v3

    check-cast v3, LX/EtW;

    invoke-static {v0}, LX/Egy;->a(LX/0QB;)LX/Egy;

    move-result-object v4

    check-cast v4, LX/Egy;

    .line 2156177
    iput-object v3, p0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;->b:LX/EtW;

    iput-object v4, p0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;->c:LX/Egy;

    .line 2156178
    move-object v0, p0

    .line 2156179
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2156180
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/bookmark/components/sections/BookmarkSectionsSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2156181
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2156182
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
