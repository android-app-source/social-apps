.class public final Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x62bfaf81
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156365
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2156364
    const-class v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2156362
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2156363
    return-void
.end method

.method private j()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156360
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->e:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    iput-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->e:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    .line 2156361
    iget-object v0, p0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->e:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2156354
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156355
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->j()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2156356
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2156357
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2156358
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156359
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2156346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2156347
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->j()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2156348
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->j()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    .line 2156349
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->j()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2156350
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;

    .line 2156351
    iput-object v0, v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->e:Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    .line 2156352
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2156353
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2156340
    invoke-direct {p0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;->j()Lcom/facebook/bookmark/components/ui/BookmarkItemComponentGraphQLModels$BookmarkItemComponentGraphQLModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2156341
    new-instance v0, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksFolderFragmentModel$BookmarksModel$EdgesModel;-><init>()V

    .line 2156342
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2156343
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2156344
    const v0, 0x107e8f17

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2156345
    const v0, -0x147d6494

    return v0
.end method
