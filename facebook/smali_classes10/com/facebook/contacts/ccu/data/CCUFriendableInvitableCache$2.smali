.class public final Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/30e;


# direct methods
.method public constructor <init>(LX/30e;)V
    .locals 0

    .prologue
    .line 2161188
    iput-object p1, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$2;->a:LX/30e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 2161189
    iget-object v0, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$2;->a:LX/30e;

    iget-object v0, v0, LX/30e;->h:LX/F7U;

    if-eqz v0, :cond_1

    .line 2161190
    iget-object v0, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$2;->a:LX/30e;

    iget-object v0, v0, LX/30e;->h:LX/F7U;

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 2161191
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    .line 2161192
    iput-boolean v5, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->N:Z

    .line 2161193
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    sget-object v2, LX/F72;->DEFAULT:LX/F72;

    invoke-virtual {v1, v2}, LX/F73;->a(LX/F72;)V

    .line 2161194
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->d:LX/30e;

    const/4 v2, 0x0

    .line 2161195
    iput-object v2, v1, LX/30e;->h:LX/F7U;

    .line 2161196
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w:LX/F7s;

    if-eqz v1, :cond_0

    .line 2161197
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w:LX/F7s;

    invoke-virtual {v1}, LX/F7s;->a()V

    .line 2161198
    :cond_0
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-boolean v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->T:Z

    if-eqz v1, :cond_3

    .line 2161199
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->z:Landroid/widget/TextView;

    iget-object v2, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0168

    iget-object v4, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v4, v4, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v6, v6, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2161200
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    const/16 v2, 0x7d0

    .line 2161201
    if-gtz v2, :cond_4

    .line 2161202
    invoke-virtual {v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->b()V

    .line 2161203
    :goto_0
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->t(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2161204
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v2, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    sget-object v3, LX/9Ti;->FRIENDABLE_CONTACTS_API:LX/9Ti;

    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v4, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v5

    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v7

    invoke-virtual/range {v2 .. v7}, LX/9Tk;->a(LX/9Ti;Ljava/lang/String;JI)V

    .line 2161205
    :cond_1
    iget-object v0, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$2;->a:LX/30e;

    iget-object v0, v0, LX/30e;->i:LX/F8b;

    if-eqz v0, :cond_2

    .line 2161206
    iget-object v0, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$2;->a:LX/30e;

    iget-object v0, v0, LX/30e;->i:LX/F8b;

    .line 2161207
    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->w:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2161208
    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    sget-object v2, LX/F8L;->DEFAULT:LX/F8L;

    invoke-virtual {v1, v2}, LX/F8M;->a(LX/F8L;)V

    .line 2161209
    iget-object v2, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    invoke-static {v2, v1}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Z)V

    .line 2161210
    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->b:LX/30e;

    const/4 v2, 0x0

    .line 2161211
    iput-object v2, v1, LX/30e;->i:LX/F8b;

    .line 2161212
    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v2, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e:LX/9Tk;

    sget-object v3, LX/9Ti;->INVITABLE_CONTACTS_API:LX/9Ti;

    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    iget-object v4, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {v1}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J

    move-result-wide v5

    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v7

    invoke-virtual/range {v2 .. v7}, LX/9Tk;->a(LX/9Ti;Ljava/lang/String;JI)V

    .line 2161213
    :cond_2
    return-void

    .line 2161214
    :cond_3
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2161215
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->A:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 2161216
    :cond_4
    iget-object v8, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a:LX/0Sh;

    new-instance v9, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment$5;

    invoke-direct {v9, v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment$5;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    int-to-long v10, v2

    invoke-virtual {v8, v9, v10, v11}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    .line 2161217
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method
