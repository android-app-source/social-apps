.class public final Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/30e;


# direct methods
.method public constructor <init>(LX/30e;)V
    .locals 0

    .prologue
    .line 2161218
    iput-object p1, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$3;->a:LX/30e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 2161219
    iget-object v0, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$3;->a:LX/30e;

    iget-object v0, v0, LX/30e;->h:LX/F7U;

    if-eqz v0, :cond_1

    .line 2161220
    iget-object v0, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$3;->a:LX/30e;

    iget-object v0, v0, LX/30e;->h:LX/F7U;

    .line 2161221
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->x:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2161222
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->E:LX/F73;

    sget-object v2, LX/F72;->FAILURE:LX/F72;

    invoke-virtual {v1, v2}, LX/F73;->a(LX/F72;)V

    .line 2161223
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v1}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->t(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)V

    .line 2161224
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w:LX/F7s;

    if-eqz v1, :cond_0

    .line 2161225
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->w:LX/F7s;

    invoke-virtual {v1}, LX/F7s;->a()V

    .line 2161226
    :cond_0
    iget-object v1, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->j:LX/9Tk;

    iget-object v2, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->G:LX/89v;

    iget-object v2, v2, LX/89v;->value:Ljava/lang/String;

    iget-object v3, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    invoke-static {v3}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->u$redex0(Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;)J

    move-result-wide v3

    iget-object v5, v0, LX/F7U;->a:Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    iget-object v5, v5, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->r:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    sget-object v6, LX/9Tj;->FRIENDABLE_CONTACTS_FETCH_FAILED:LX/9Tj;

    invoke-virtual/range {v1 .. v6}, LX/9Tk;->a(Ljava/lang/String;JILX/9Tj;)V

    .line 2161227
    :cond_1
    iget-object v0, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$3;->a:LX/30e;

    iget-object v0, v0, LX/30e;->i:LX/F8b;

    if-eqz v0, :cond_3

    .line 2161228
    iget-object v0, p0, Lcom/facebook/contacts/ccu/data/CCUFriendableInvitableCache$3;->a:LX/30e;

    iget-object v0, v0, LX/30e;->i:LX/F8b;

    .line 2161229
    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->w:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2161230
    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    sget-object v2, LX/F8L;->FAILURE:LX/F8L;

    invoke-virtual {v1, v2}, LX/F8M;->a(LX/F8L;)V

    .line 2161231
    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2161232
    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a$redex0(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;Z)V

    .line 2161233
    :cond_2
    iget-object v1, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e:LX/9Tk;

    iget-object v2, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    iget-object v2, v2, LX/89v;->value:Ljava/lang/String;

    iget-object v3, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-static {v3}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->s(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)J

    move-result-wide v3

    iget-object v5, v0, LX/F8b;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v5, v5, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    sget-object v6, LX/9Tj;->INVITES_FETCH_FAILED:LX/9Tj;

    invoke-virtual/range {v1 .. v6}, LX/9Tk;->a(Ljava/lang/String;JILX/9Tj;)V

    .line 2161234
    :cond_3
    return-void
.end method
