.class public Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;
.super LX/3Nf;
.source ""


# instance fields
.field public a:LX/DAi;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1971401
    invoke-direct {p0, p1, p2}, LX/3Nf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1971402
    invoke-direct {p0}, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->i()V

    .line 1971403
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1971414
    const v0, 0x7f0d2086

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->b:Landroid/widget/Button;

    .line 1971415
    const v0, 0x7f0d2087

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->c:Landroid/widget/Button;

    .line 1971416
    iget-object v0, p0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->b:Landroid/widget/Button;

    new-instance v1, LX/DAg;

    invoke-direct {v1, p0}, LX/DAg;-><init>(Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1971417
    iget-object v0, p0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->c:Landroid/widget/Button;

    new-instance v1, LX/DAh;

    invoke-direct {v1, p0}, LX/DAh;-><init>(Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1971418
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 1971409
    const v0, 0x7f030d02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1971410
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1971411
    iget-object v0, p0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->b:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1971412
    iget-object v0, p0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->c:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1971413
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1971406
    iget-object v0, p0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->b:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1971407
    iget-object v0, p0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1971408
    return-void
.end method

.method public setButtonListener(LX/DAi;)V
    .locals 0

    .prologue
    .line 1971404
    iput-object p1, p0, Lcom/facebook/contacts/picker/DivebarFaveditSearchBarView;->a:LX/DAi;

    .line 1971405
    return-void
.end method
