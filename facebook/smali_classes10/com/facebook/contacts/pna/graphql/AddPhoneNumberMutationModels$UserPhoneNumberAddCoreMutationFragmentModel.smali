.class public final Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x58919177
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2162972
    const-class v0, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2162971
    const-class v0, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2162969
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2162970
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2162966
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2162967
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2162968
    return-void
.end method

.method private a()Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2162964
    iget-object v0, p0, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;->e:Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    iput-object v0, p0, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;->e:Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    .line 2162965
    iget-object v0, p0, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;->e:Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2162958
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2162959
    invoke-direct {p0}, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;->a()Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2162960
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2162961
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2162962
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2162963
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2162950
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2162951
    invoke-direct {p0}, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;->a()Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2162952
    invoke-direct {p0}, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;->a()Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    .line 2162953
    invoke-direct {p0}, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;->a()Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2162954
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;

    .line 2162955
    iput-object v0, v1, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;->e:Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel$UserModel;

    .line 2162956
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2162957
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2162945
    new-instance v0, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/contacts/pna/graphql/AddPhoneNumberMutationModels$UserPhoneNumberAddCoreMutationFragmentModel;-><init>()V

    .line 2162946
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2162947
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2162949
    const v0, 0x1e52185b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2162948
    const v0, 0x123c3db8

    return v0
.end method
