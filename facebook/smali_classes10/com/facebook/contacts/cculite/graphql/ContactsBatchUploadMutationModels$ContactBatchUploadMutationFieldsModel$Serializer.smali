.class public final Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2161618
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2161619
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2161577
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2161579
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2161580
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v2, 0x0

    .line 2161581
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2161582
    invoke-virtual {v1, v0, v2, v2}, LX/15i;->a(III)I

    move-result v2

    .line 2161583
    if-eqz v2, :cond_0

    .line 2161584
    const-string v3, "batch_size"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2161585
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 2161586
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2161587
    if-eqz v2, :cond_2

    .line 2161588
    const-string v3, "friendable_contacts"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2161589
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2161590
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_1

    .line 2161591
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/EjV;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2161592
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2161593
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2161594
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2161595
    if-eqz v2, :cond_7

    .line 2161596
    const-string v3, "invitable_contacts"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2161597
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2161598
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_6

    .line 2161599
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    .line 2161600
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2161601
    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2161602
    if-eqz v0, :cond_3

    .line 2161603
    const-string p2, "contact_point"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2161604
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2161605
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2161606
    if-eqz v0, :cond_4

    .line 2161607
    const-string p2, "name"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2161608
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2161609
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {v1, p0, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2161610
    if-eqz v0, :cond_5

    .line 2161611
    const-string p2, "record_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2161612
    invoke-virtual {p1, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2161613
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2161614
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2161615
    :cond_6
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2161616
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2161617
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2161578
    check-cast p1, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Serializer;->a(Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
