.class public final Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2161877
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;

    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2161878
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2161879
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2161880
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2161881
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2161882
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2161883
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 2161884
    if-eqz p0, :cond_0

    .line 2161885
    const-string p2, "in_sync"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2161886
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 2161887
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2161888
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2161889
    check-cast p1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Serializer;->a(Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
