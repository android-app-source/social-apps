.class public final Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7edd9f55
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2162708
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2162709
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2162710
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2162711
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2162712
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2162713
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2162714
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2162715
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2162716
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 2162717
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 2162718
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2162719
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2162720
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2162721
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2162722
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2162723
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    .line 2162724
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 2162725
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;

    .line 2162726
    iput-object v0, v1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->e:Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    .line 2162727
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2162728
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2162729
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->e:Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    iput-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->e:Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    .line 2162730
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;->e:Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2162731
    new-instance v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel;-><init>()V

    .line 2162732
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2162733
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2162734
    const v0, 0x2f765376

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2162735
    const v0, 0x54648206

    return v0
.end method
