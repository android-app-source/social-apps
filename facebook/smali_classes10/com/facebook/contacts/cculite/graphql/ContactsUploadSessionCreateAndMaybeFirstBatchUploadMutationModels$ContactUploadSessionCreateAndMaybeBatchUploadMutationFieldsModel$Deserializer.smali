.class public final Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2161948
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2161949
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2161950
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2161951
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2161952
    const/4 v2, 0x0

    .line 2161953
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 2161954
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2161955
    :goto_0
    move v1, v2

    .line 2161956
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2161957
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2161958
    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-direct {v1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;-><init>()V

    .line 2161959
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2161960
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2161961
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2161962
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2161963
    :cond_0
    return-object v1

    .line 2161964
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2161965
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_7

    .line 2161966
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2161967
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2161968
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v5, :cond_2

    .line 2161969
    const-string p0, "contact_upload_session"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2161970
    invoke-static {p1, v0}, LX/Eji;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2161971
    :cond_3
    const-string p0, "friendable_contacts"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2161972
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2161973
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, p0, :cond_4

    .line 2161974
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, p0, :cond_4

    .line 2161975
    invoke-static {p1, v0}, LX/Ejj;->b(LX/15w;LX/186;)I

    move-result v5

    .line 2161976
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2161977
    :cond_4
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2161978
    goto :goto_1

    .line 2161979
    :cond_5
    const-string p0, "invitable_contacts"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2161980
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2161981
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, p0, :cond_6

    .line 2161982
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, p0, :cond_6

    .line 2161983
    invoke-static {p1, v0}, LX/Ejk;->b(LX/15w;LX/186;)I

    move-result v5

    .line 2161984
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2161985
    :cond_6
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 2161986
    goto/16 :goto_1

    .line 2161987
    :cond_7
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 2161988
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 2161989
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 2161990
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2161991
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    goto/16 :goto_1
.end method
