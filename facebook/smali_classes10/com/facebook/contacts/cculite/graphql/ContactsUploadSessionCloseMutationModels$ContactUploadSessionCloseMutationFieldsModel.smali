.class public final Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x208ec303
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2161911
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2161910
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2161908
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2161909
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2161905
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2161906
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2161907
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 2161900
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2161901
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2161902
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 2161903
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2161904
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2161912
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2161913
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2161914
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2161892
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2161893
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;->e:Z

    .line 2161894
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2161890
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 2161891
    iget-boolean v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2161895
    new-instance v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;-><init>()V

    .line 2161896
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2161897
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2161899
    const v0, 0x69b9ce54

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2161898
    const v0, 0xa8469ca

    return v0
.end method
