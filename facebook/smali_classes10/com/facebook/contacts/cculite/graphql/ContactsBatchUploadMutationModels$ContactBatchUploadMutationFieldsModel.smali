.class public final Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1d128e91
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2161645
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2161646
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2161647
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2161648
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 2161658
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2161659
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2161660
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2161649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2161650
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 2161651
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->j()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 2161652
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2161653
    iget v2, p0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->e:I

    invoke-virtual {p1, v3, v2, v3}, LX/186;->a(III)V

    .line 2161654
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2161655
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2161656
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2161657
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFriendableContacts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2161643
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$FriendableContactsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->f:Ljava/util/List;

    .line 2161644
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2161630
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2161631
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2161632
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 2161633
    if-eqz v1, :cond_0

    .line 2161634
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    .line 2161635
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->f:Ljava/util/List;

    .line 2161636
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->j()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2161637
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->j()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 2161638
    if-eqz v1, :cond_1

    .line 2161639
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    .line 2161640
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->g:LX/3Sb;

    .line 2161641
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2161642
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2161620
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2161621
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->e:I

    .line 2161622
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2161627
    new-instance v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;-><init>()V

    .line 2161628
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2161629
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2161626
    const v0, 0x94b6fc7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2161625
    const v0, 0xb178ad2

    return v0
.end method

.method public final j()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getInvitableContacts"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2161623
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, -0x1b4e6c77

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->g:LX/3Sb;

    .line 2161624
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method
