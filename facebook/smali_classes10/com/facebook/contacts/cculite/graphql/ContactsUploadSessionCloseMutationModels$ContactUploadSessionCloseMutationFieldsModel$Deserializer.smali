.class public final Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2161850
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;

    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2161851
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2161852
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2161853
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2161854
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2161855
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_5

    .line 2161856
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2161857
    :goto_0
    move v1, v2

    .line 2161858
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2161859
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2161860
    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;

    invoke-direct {v1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCloseMutationModels$ContactUploadSessionCloseMutationFieldsModel;-><init>()V

    .line 2161861
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2161862
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2161863
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2161864
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2161865
    :cond_0
    return-object v1

    .line 2161866
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_3

    .line 2161867
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2161868
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2161869
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v5, :cond_1

    .line 2161870
    const-string p0, "in_sync"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2161871
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v4, v1

    move v1, v3

    goto :goto_1

    .line 2161872
    :cond_2
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2161873
    :cond_3
    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2161874
    if-eqz v1, :cond_4

    .line 2161875
    invoke-virtual {v0, v2, v4}, LX/186;->a(IZ)V

    .line 2161876
    :cond_4
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v4, v2

    goto :goto_1
.end method
