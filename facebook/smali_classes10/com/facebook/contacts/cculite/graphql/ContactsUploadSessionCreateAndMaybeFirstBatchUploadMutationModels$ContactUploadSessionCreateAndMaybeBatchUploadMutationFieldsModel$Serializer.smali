.class public final Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2162116
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2162117
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2162118
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2162119
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 2162120
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 2162121
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2162122
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2162123
    if-eqz v2, :cond_0

    .line 2162124
    const-string v3, "contact_upload_session"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162125
    invoke-static {v1, v2, p1, p2}, LX/Eji;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2162126
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2162127
    if-eqz v2, :cond_2

    .line 2162128
    const-string v3, "friendable_contacts"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162129
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2162130
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_1

    .line 2162131
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/Ejj;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2162132
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2162133
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2162134
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 2162135
    if-eqz v2, :cond_4

    .line 2162136
    const-string v3, "invitable_contacts"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2162137
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 2162138
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_3

    .line 2162139
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/Ejk;->a(LX/15i;ILX/0nX;)V

    .line 2162140
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2162141
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 2162142
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2162143
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2162144
    check-cast p1, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel$Serializer;->a(Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$ContactUploadSessionCreateAndMaybeBatchUploadMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
