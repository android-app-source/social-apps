.class public final Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2161437
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 2161438
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2161439
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2161440
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2161441
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2161442
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 2161443
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2161444
    :goto_0
    move v1, v2

    .line 2161445
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2161446
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 2161447
    new-instance v1, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;

    invoke-direct {v1}, Lcom/facebook/contacts/cculite/graphql/ContactsBatchUploadMutationModels$ContactBatchUploadMutationFieldsModel;-><init>()V

    .line 2161448
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 2161449
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 2161450
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 2161451
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 2161452
    :cond_0
    return-object v1

    .line 2161453
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 2161454
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2161455
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2161456
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2161457
    const-string v8, "batch_size"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2161458
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_1

    .line 2161459
    :cond_2
    const-string v8, "friendable_contacts"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2161460
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2161461
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_3

    .line 2161462
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_3

    .line 2161463
    invoke-static {p1, v0}, LX/EjV;->b(LX/15w;LX/186;)I

    move-result v7

    .line 2161464
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2161465
    :cond_3
    invoke-static {v5, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 2161466
    goto :goto_1

    .line 2161467
    :cond_4
    const-string v8, "invitable_contacts"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2161468
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2161469
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_5

    .line 2161470
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_5

    .line 2161471
    const/4 v8, 0x0

    .line 2161472
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_f

    .line 2161473
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2161474
    :goto_4
    move v7, v8

    .line 2161475
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2161476
    :cond_5
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 2161477
    goto/16 :goto_1

    .line 2161478
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2161479
    :cond_7
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2161480
    if-eqz v1, :cond_8

    .line 2161481
    invoke-virtual {v0, v2, v6, v2}, LX/186;->a(III)V

    .line 2161482
    :cond_8
    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 2161483
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2161484
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto/16 :goto_1

    .line 2161485
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 2161486
    :cond_b
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_e

    .line 2161487
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2161488
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 2161489
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v11, :cond_b

    .line 2161490
    const-string p0, "contact_point"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 2161491
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_5

    .line 2161492
    :cond_c
    const-string p0, "name"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_d

    .line 2161493
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_5

    .line 2161494
    :cond_d
    const-string p0, "record_id"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 2161495
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_5

    .line 2161496
    :cond_e
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 2161497
    invoke-virtual {v0, v8, v10}, LX/186;->b(II)V

    .line 2161498
    const/4 v8, 0x1

    invoke-virtual {v0, v8, v9}, LX/186;->b(II)V

    .line 2161499
    const/4 v8, 0x2

    invoke-virtual {v0, v8, v7}, LX/186;->b(II)V

    .line 2161500
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto/16 :goto_4

    :cond_f
    move v7, v8

    move v9, v8

    move v10, v8

    goto :goto_5
.end method
