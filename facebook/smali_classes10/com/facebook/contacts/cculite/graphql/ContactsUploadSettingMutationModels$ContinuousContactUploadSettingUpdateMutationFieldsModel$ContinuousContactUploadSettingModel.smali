.class public final Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x768ac303
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2162665
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2162664
    const-class v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2162662
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 2162663
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2162660
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->e:Ljava/lang/String;

    .line 2162661
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2162652
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2162653
    invoke-direct {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2162654
    invoke-virtual {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->j()Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2162655
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2162656
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 2162657
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 2162658
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2162659
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 2162666
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2162667
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 2162668
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2162651
    invoke-direct {p0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 2162648
    new-instance v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;

    invoke-direct {v0}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;-><init>()V

    .line 2162649
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 2162650
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2162647
    const v0, -0x72cfc10e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2162646
    const v0, -0x2423c562

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2162644
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->f:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    iput-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->f:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    .line 2162645
    iget-object v0, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSettingMutationModels$ContinuousContactUploadSettingUpdateMutationFieldsModel$ContinuousContactUploadSettingModel;->f:Lcom/facebook/graphql/enums/GraphQLContinuousContactUploadSettingStatusEnum;

    return-object v0
.end method
