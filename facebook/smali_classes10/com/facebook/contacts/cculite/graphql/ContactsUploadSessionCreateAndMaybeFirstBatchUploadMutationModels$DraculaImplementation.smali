.class public final Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2162316
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 2162317
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 2162314
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 2162315
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    .line 2162250
    if-nez p1, :cond_0

    .line 2162251
    const/4 v0, 0x0

    .line 2162252
    :goto_0
    return v0

    .line 2162253
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2162254
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2162255
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2162256
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 2162257
    const v2, -0x121a803

    const/4 v4, 0x0

    .line 2162258
    if-nez v1, :cond_1

    move v3, v4

    .line 2162259
    :goto_1
    move v1, v3

    .line 2162260
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 2162261
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2162262
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v3

    .line 2162263
    const v4, 0x4dcd135c    # 4.30074752E8f

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 2162264
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 2162265
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v0}, LX/186;->a(IZ)V

    .line 2162266
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2162267
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 2162268
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 2162269
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2162270
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2162271
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2162272
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2162273
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2162274
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 2162275
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 2162276
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2162277
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 2162278
    :sswitch_2
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2162279
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLContactUploadFieldSettingEnum;

    move-result-object v1

    .line 2162280
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2162281
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(III)I

    move-result v2

    .line 2162282
    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v3, v4}, LX/15i;->a(III)I

    move-result v3

    .line 2162283
    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-virtual {p0, p1, v4, v5}, LX/15i;->a(III)I

    move-result v4

    .line 2162284
    const/4 v5, 0x5

    const/4 v6, 0x0

    invoke-virtual {p0, p1, v5, v6}, LX/15i;->a(III)I

    move-result v5

    .line 2162285
    const/4 v6, 0x6

    const/4 v7, 0x0

    invoke-virtual {p0, p1, v6, v7}, LX/15i;->a(III)I

    move-result v6

    .line 2162286
    const/4 v7, 0x7

    const/4 v8, 0x0

    invoke-virtual {p0, p1, v7, v8}, LX/15i;->a(III)I

    move-result v7

    .line 2162287
    const/16 v8, 0x8

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 2162288
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {p3, v8, v0, v9}, LX/186;->a(III)V

    .line 2162289
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 2162290
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v2, v1}, LX/186;->a(III)V

    .line 2162291
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v3, v1}, LX/186;->a(III)V

    .line 2162292
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v4, v1}, LX/186;->a(III)V

    .line 2162293
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v5, v1}, LX/186;->a(III)V

    .line 2162294
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v6, v1}, LX/186;->a(III)V

    .line 2162295
    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v7, v1}, LX/186;->a(III)V

    .line 2162296
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2162297
    :sswitch_3
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 2162298
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2162299
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v0, v2}, LX/186;->a(III)V

    .line 2162300
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2162301
    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2162302
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 2162303
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 2162304
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 2162305
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 2162306
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v5

    .line 2162307
    if-nez v5, :cond_2

    const/4 v3, 0x0

    .line 2162308
    :goto_2
    if-ge v4, v5, :cond_3

    .line 2162309
    invoke-virtual {p0, v1, v4}, LX/15i;->q(II)I

    move-result v6

    .line 2162310
    invoke-static {p0, v6, v2, p3}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v6

    aput v6, v3, v4

    .line 2162311
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2162312
    :cond_2
    new-array v3, v5, [I

    goto :goto_2

    .line 2162313
    :cond_3
    const/4 v4, 0x1

    invoke-virtual {p3, v3, v4}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x13b9ee09 -> :sswitch_4
        -0x121a803 -> :sswitch_1
        0x4dcd135c -> :sswitch_2
        0x6f1553b2 -> :sswitch_3
        0x728b971d -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 2162249
    new-instance v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 4

    .prologue
    .line 2162236
    sparse-switch p2, :sswitch_data_0

    .line 2162237
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2162238
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2162239
    const v1, -0x121a803

    .line 2162240
    if-eqz v0, :cond_0

    .line 2162241
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result v3

    .line 2162242
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 2162243
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 2162244
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2162245
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2162246
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 2162247
    const v1, 0x4dcd135c    # 4.30074752E8f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 2162248
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x13b9ee09 -> :sswitch_1
        -0x121a803 -> :sswitch_1
        0x4dcd135c -> :sswitch_1
        0x6f1553b2 -> :sswitch_1
        0x728b971d -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 2162230
    if-eqz p1, :cond_0

    .line 2162231
    invoke-static {p0, p1, p2}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;

    move-result-object v1

    .line 2162232
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;

    .line 2162233
    if-eq v0, v1, :cond_0

    .line 2162234
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2162235
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 2162196
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 2162228
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 2162229
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 2162223
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 2162224
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 2162225
    :cond_0
    iput-object p1, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->a:LX/15i;

    .line 2162226
    iput p2, p0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->b:I

    .line 2162227
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 2162222
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 2162221
    new-instance v0, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 2162218
    iget v0, p0, LX/1vt;->c:I

    .line 2162219
    move v0, v0

    .line 2162220
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 2162215
    iget v0, p0, LX/1vt;->c:I

    .line 2162216
    move v0, v0

    .line 2162217
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 2162212
    iget v0, p0, LX/1vt;->b:I

    .line 2162213
    move v0, v0

    .line 2162214
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2162209
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 2162210
    move-object v0, v0

    .line 2162211
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2162200
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 2162201
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2162202
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 2162203
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 2162204
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2162205
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2162206
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2162207
    invoke-static {v3, v9, v2}, Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/contacts/cculite/graphql/ContactsUploadSessionCreateAndMaybeFirstBatchUploadMutationModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 2162208
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 2162197
    iget v0, p0, LX/1vt;->c:I

    .line 2162198
    move v0, v0

    .line 2162199
    return v0
.end method
