.class public final Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2152087
    new-instance v0, LX/EeA;

    invoke-direct {v0}, LX/EeA;-><init>()V

    sput-object v0, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2152078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152079
    iput-object p1, p0, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;->a:Ljava/lang/String;

    .line 2152080
    iput-boolean p2, p0, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;->b:Z

    .line 2152081
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2152086
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2152082
    iget-object v0, p0, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152083
    iget-boolean v0, p0, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2152084
    return-void

    .line 2152085
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
