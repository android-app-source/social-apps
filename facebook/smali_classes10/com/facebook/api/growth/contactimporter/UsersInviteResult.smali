.class public Lcom/facebook/api/growth/contactimporter/UsersInviteResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/growth/contactimporter/UsersInviteResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final email:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "email"
    .end annotation
.end field

.field public final inviteStatus:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "invite_status"
    .end annotation
.end field

.field public final normalizedId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "normalized_id"
    .end annotation
.end field

.field public final userId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2152268
    const-class v0, Lcom/facebook/api/growth/contactimporter/UsersInviteResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2152269
    new-instance v0, LX/EeG;

    invoke-direct {v0}, LX/EeG;-><init>()V

    sput-object v0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2152270
    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 2152271
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2152272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152273
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->email:Ljava/lang/String;

    .line 2152274
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->inviteStatus:Ljava/lang/String;

    .line 2152275
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->normalizedId:Ljava/lang/String;

    .line 2152276
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->userId:J

    .line 2152277
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 2152278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152279
    iput-object p1, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->email:Ljava/lang/String;

    .line 2152280
    iput-object p2, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->inviteStatus:Ljava/lang/String;

    .line 2152281
    iput-object p3, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->normalizedId:Ljava/lang/String;

    .line 2152282
    iput-wide p4, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->userId:J

    .line 2152283
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2152284
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2152285
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->email:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152286
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->inviteStatus:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152287
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->normalizedId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152288
    iget-wide v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteResult;->userId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2152289
    return-void
.end method
