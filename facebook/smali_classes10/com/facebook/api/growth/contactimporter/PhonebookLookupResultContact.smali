.class public Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final email:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "email"
    .end annotation
.end field

.field public final isFriend:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_friend"
    .end annotation
.end field

.field public final mutualFriends:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "mutual_friends"
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "name"
    .end annotation
.end field

.field public final nativeName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "native_name"
    .end annotation
.end field

.field public final ordinal:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "ordinal"
    .end annotation
.end field

.field public final phone:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "cell"
    .end annotation
.end field

.field public final profilePic:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pic_square_with_logo"
    .end annotation
.end field

.field public final recordId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "record_id"
    .end annotation
.end field

.field public final userId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uid"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2152132
    const-class v0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContactDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 2152133
    const-class v0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContactSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2152104
    new-instance v0, LX/EeC;

    invoke-direct {v0}, LX/EeC;-><init>()V

    sput-object v0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 14

    .prologue
    const-wide/16 v2, -0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 2152130
    const-wide/16 v10, 0x0

    move-object v0, p0

    move-object v4, v1

    move-object v5, v1

    move-wide v6, v2

    move-object v9, v1

    move-object v12, v1

    move v13, v8

    invoke-direct/range {v0 .. v13}, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;JZLjava/lang/String;JLjava/lang/String;I)V

    .line 2152131
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2152134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152135
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->name:Ljava/lang/String;

    .line 2152136
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->recordId:J

    .line 2152137
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->email:Ljava/lang/String;

    .line 2152138
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->phone:Ljava/lang/String;

    .line 2152139
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->userId:J

    .line 2152140
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->isFriend:Z

    .line 2152141
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->profilePic:Ljava/lang/String;

    .line 2152142
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->ordinal:J

    .line 2152143
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->nativeName:Ljava/lang/String;

    .line 2152144
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->mutualFriends:I

    .line 2152145
    return-void

    .line 2152146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;JZLjava/lang/String;JLjava/lang/String;I)V
    .locals 0

    .prologue
    .line 2152118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152119
    iput-object p1, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->name:Ljava/lang/String;

    .line 2152120
    iput-wide p2, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->recordId:J

    .line 2152121
    iput-object p4, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->email:Ljava/lang/String;

    .line 2152122
    iput-object p5, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->phone:Ljava/lang/String;

    .line 2152123
    iput-wide p6, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->userId:J

    .line 2152124
    iput-boolean p8, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->isFriend:Z

    .line 2152125
    iput-object p9, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->profilePic:Ljava/lang/String;

    .line 2152126
    iput-wide p10, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->ordinal:J

    .line 2152127
    iput-object p12, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->nativeName:Ljava/lang/String;

    .line 2152128
    iput p13, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->mutualFriends:I

    .line 2152129
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2152117
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2152105
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152106
    iget-wide v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->recordId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2152107
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->email:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152108
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->phone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152109
    iget-wide v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->userId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2152110
    iget-boolean v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->isFriend:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 2152111
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->profilePic:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152112
    iget-wide v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->ordinal:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2152113
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->nativeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152114
    iget v0, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->mutualFriends:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2152115
    return-void

    .line 2152116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
