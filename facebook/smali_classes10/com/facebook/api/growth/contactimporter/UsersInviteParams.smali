.class public Lcom/facebook/api/growth/contactimporter/UsersInviteParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/growth/contactimporter/UsersInviteParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:LX/89v;

.field public final e:Z

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2152235
    new-instance v0, LX/EeF;

    invoke-direct {v0}, LX/EeF;-><init>()V

    sput-object v0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2152246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152247
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->a:Ljava/util/List;

    .line 2152248
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->b:Ljava/lang/String;

    .line 2152249
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->c:Ljava/lang/String;

    .line 2152250
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/89v;

    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->d:LX/89v;

    .line 2152251
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->e:Z

    .line 2152252
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->f:Z

    .line 2152253
    return-void

    :cond_0
    move v0, v2

    .line 2152254
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2152255
    goto :goto_1
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;LX/89v;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "LX/89v;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 2152256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152257
    iput-object p1, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->a:Ljava/util/List;

    .line 2152258
    iput-object p2, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->b:Ljava/lang/String;

    .line 2152259
    sget-object v0, LX/89w;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2152260
    iput-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->c:Ljava/lang/String;

    .line 2152261
    iput-object p3, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->d:LX/89v;

    .line 2152262
    iput-boolean p4, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->e:Z

    .line 2152263
    iput-boolean p5, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->f:Z

    .line 2152264
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2152245
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2152236
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2152237
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152238
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152239
    iget-object v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->d:LX/89v;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 2152240
    iget-boolean v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->e:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2152241
    iget-boolean v0, p0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;->f:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2152242
    return-void

    :cond_0
    move v0, v2

    .line 2152243
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2152244
    goto :goto_1
.end method
