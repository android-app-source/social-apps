.class public Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContactSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2152196
    const-class v0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;

    new-instance v1, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContactSerializer;

    invoke-direct {v1}, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContactSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 2152197
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2152195
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2152189
    if-nez p0, :cond_0

    .line 2152190
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 2152191
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 2152192
    invoke-static {p0, p1, p2}, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContactSerializer;->b(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;LX/0nX;LX/0my;)V

    .line 2152193
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 2152194
    return-void
.end method

.method private static b(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 2152178
    const-string v0, "name"

    iget-object v1, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2152179
    const-string v0, "record_id"

    iget-wide v2, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->recordId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2152180
    const-string v0, "email"

    iget-object v1, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->email:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2152181
    const-string v0, "cell"

    iget-object v1, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->phone:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2152182
    const-string v0, "uid"

    iget-wide v2, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->userId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2152183
    const-string v0, "is_friend"

    iget-boolean v1, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->isFriend:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2152184
    const-string v0, "pic_square_with_logo"

    iget-object v1, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->profilePic:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2152185
    const-string v0, "ordinal"

    iget-wide v2, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->ordinal:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 2152186
    const-string v0, "native_name"

    iget-object v1, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->nativeName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 2152187
    const-string v0, "mutual_friends"

    iget v1, p0, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->mutualFriends:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2152188
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 2152177
    check-cast p1, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;

    invoke-static {p1, p2, p3}, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContactSerializer;->a(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;LX/0nX;LX/0my;)V

    return-void
.end method
