.class public Lcom/facebook/api/growth/profile/SetProfilePhotoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/growth/profile/SetProfilePhotoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2152446
    new-instance v0, LX/EeK;

    invoke-direct {v0}, LX/EeK;-><init>()V

    sput-object v0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2152425
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v6, v5

    invoke-direct/range {v1 .. v6}, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2152426
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2152440
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152441
    iput-wide p1, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->a:J

    .line 2152442
    iput-object p3, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->b:Ljava/lang/String;

    .line 2152443
    iput-object p4, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->c:Ljava/lang/String;

    .line 2152444
    iput-object p5, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->d:Ljava/lang/String;

    .line 2152445
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 2152434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152435
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->a:J

    .line 2152436
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->b:Ljava/lang/String;

    .line 2152437
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->c:Ljava/lang/String;

    .line 2152438
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->d:Ljava/lang/String;

    .line 2152439
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2152433
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2152432
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "method"

    const-string v2, "SetProfilePhotoMethod"

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "profileId"

    iget-wide v2, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "photoFilePath"

    iget-object v2, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "profilePhotoSource"

    iget-object v2, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "profilePhotoMethod"

    iget-object v2, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 2152427
    iget-wide v0, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 2152428
    iget-object v0, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152429
    iget-object v0, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152430
    iget-object v0, p0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152431
    return-void
.end method
