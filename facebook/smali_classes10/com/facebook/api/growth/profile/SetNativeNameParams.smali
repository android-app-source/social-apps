.class public Lcom/facebook/api/growth/profile/SetNativeNameParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/api/growth/profile/SetNativeNameParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2152360
    new-instance v0, LX/EeI;

    invoke-direct {v0}, LX/EeI;-><init>()V

    sput-object v0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2152353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152354
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->a:Ljava/lang/String;

    .line 2152355
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->b:Ljava/lang/String;

    .line 2152356
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->c:Ljava/lang/String;

    .line 2152357
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->d:Ljava/lang/String;

    .line 2152358
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->e:Ljava/lang/String;

    .line 2152359
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2152361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2152362
    iput-object p1, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->a:Ljava/lang/String;

    .line 2152363
    iput-object p2, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->b:Ljava/lang/String;

    .line 2152364
    iput-object p3, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->c:Ljava/lang/String;

    .line 2152365
    iput-object p4, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->d:Ljava/lang/String;

    .line 2152366
    iput-object p5, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->e:Ljava/lang/String;

    .line 2152367
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2152352
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2152346
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "first_name"

    iget-object v2, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "last_name"

    iget-object v2, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "locale"

    iget-object v2, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    .line 2152347
    iget-object v1, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2152348
    const-string v1, "first_name_extra"

    iget-object v2, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 2152349
    :cond_0
    iget-object v1, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2152350
    const-string v1, "last_name_extra"

    iget-object v2, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    .line 2152351
    :cond_1
    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2152340
    iget-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152341
    iget-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152342
    iget-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152343
    iget-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152344
    iget-object v0, p0, Lcom/facebook/api/growth/profile/SetNativeNameParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2152345
    return-void
.end method
