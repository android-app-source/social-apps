.class public Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/SearchResultsBridge;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102595
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2102596
    iput-object p1, p0, Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;->a:Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;

    .line 2102597
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;
    .locals 4

    .prologue
    .line 2102598
    const-class v1, Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;

    monitor-enter v1

    .line 2102599
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2102600
    sput-object v2, Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2102601
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102602
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2102603
    new-instance p0, Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;-><init>(Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;)V

    .line 2102604
    move-object v0, p0

    .line 2102605
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2102606
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2102607
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2102608
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2102609
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2102610
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsBridgePartDefinition;->a:Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;

    .line 2102611
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2102612
    check-cast v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    .line 2102613
    iget-object p0, v0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    move-object v0, p0

    .line 2102614
    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2102615
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2102616
    const/4 v0, 0x1

    return v0
.end method
