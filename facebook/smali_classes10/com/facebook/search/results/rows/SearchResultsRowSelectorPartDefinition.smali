.class public Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
        "Ljava/lang/Void;",
        "LX/Cxk;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<+",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
            ">;-",
            "LX/Cxk;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<+",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
            ">;-",
            "LX/Cxk;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserResultPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageResultPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupResultPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventResultPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102834
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2102835
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->a:Ljava/util/Map;

    .line 2102836
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102837
    move-object v2, p1

    .line 2102838
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102839
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102840
    move-object v2, p2

    .line 2102841
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102842
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102843
    move-object v2, p3

    .line 2102844
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102845
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102846
    move-object v2, p8

    .line 2102847
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102848
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102849
    move-object v2, p4

    .line 2102850
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102851
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->b:Ljava/util/Map;

    .line 2102852
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->b:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102853
    move-object v2, p6

    .line 2102854
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102855
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->b:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102856
    move-object v2, p6

    .line 2102857
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102858
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->b:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS_WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102859
    move-object v2, p7

    .line 2102860
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102861
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->b:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->WEB:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102862
    move-object v2, p7

    .line 2102863
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102864
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->b:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102865
    move-object v2, p5

    .line 2102866
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102867
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->b:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->NEWS_LINK:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102868
    move-object v2, p9

    .line 2102869
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102870
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;
    .locals 13

    .prologue
    .line 2102877
    const-class v1, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;

    monitor-enter v1

    .line 2102878
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2102879
    sput-object v2, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2102880
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102881
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2102882
    new-instance v3, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;

    const/16 v4, 0x3418

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3401

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x33e2

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x33fa

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x33b9

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x34ac

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x11ab

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x33f5

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x347f

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2102883
    move-object v0, v3

    .line 2102884
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2102885
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2102886
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2102887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2102888
    check-cast p2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    const/4 v2, 0x0

    .line 2102889
    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->p()LX/0Px;

    move-result-object v0

    .line 2102890
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102891
    :goto_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2102892
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-static {p2, p2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2102893
    :goto_1
    return-object v2

    .line 2102894
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    .line 2102895
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-static {v1, p2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2102871
    check-cast p1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    const/4 v1, 0x0

    .line 2102872
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->p()LX/0Px;

    move-result-object v0

    .line 2102873
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2102874
    :goto_0
    iget-object v2, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->a:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2102875
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2102876
    goto :goto_1
.end method
