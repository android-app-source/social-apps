.class public Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/Cyv;",
        "Ljava/lang/Void;",
        "LX/Cxk;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<+",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
            ">;-",
            "LX/Cxk;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;",
            "Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/SearchResultsRowSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/SearchResultsGrammarModuleSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/SearchResultsTopicMediaPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedModulePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialModulePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialEmptyModulePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102676
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2102677
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->h:LX/0Ot;

    .line 2102678
    iput-object p1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->a:Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;

    .line 2102679
    invoke-static {p9}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->b:LX/0Ot;

    .line 2102680
    iput-object p2, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->c:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    .line 2102681
    iput-object p3, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->d:LX/0Ot;

    .line 2102682
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->f:LX/0Ot;

    .line 2102683
    iput-object p4, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->e:LX/0Ot;

    .line 2102684
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->g:LX/0Ot;

    .line 2102685
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    .line 2102686
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x1e97fa47

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p37 .. p37}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102687
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x5389cb28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p5}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102688
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0xd5fee8a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p6}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102689
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x735086f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p7}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102690
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x5cf17590

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p8}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102691
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x59a2740a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p10}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102692
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x30189aaf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {p11}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102693
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x541d5224

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p12 .. p12}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102694
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x581d640b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102695
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x6411e5e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102696
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x31093a57

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102697
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x585964cb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102698
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x5cafdbb0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102699
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x34fdde85

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102700
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x1f44e6bd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102701
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0xa123fbe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102702
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x2a731e0d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p36 .. p36}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102703
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x44790698

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p18 .. p18}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102704
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x736a6b81

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p24 .. p24}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102705
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x6436c331

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p25 .. p25}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102706
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x64d27454

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102707
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0xfe7b1ce

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p15 .. p15}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102708
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x30cf2d19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p17 .. p17}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102709
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x793c6590

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p19 .. p19}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102710
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x696d5b57

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p16 .. p16}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102711
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0xd7270e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p14 .. p14}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102712
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x1180c7de

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p20 .. p20}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102713
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x1f9bd759

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p21 .. p21}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102714
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x6eeaf2f1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p22 .. p22}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102715
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x37da4ee4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p22 .. p22}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102716
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x5c6a201d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p22 .. p22}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102717
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x3ba7c68c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p22 .. p22}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102718
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x50950962

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p22 .. p22}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102719
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x3c4e1b9e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p23 .. p23}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102720
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x2ea62261

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p26 .. p26}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102721
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x1c2adf8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p28 .. p28}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102722
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x28d5023a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p34 .. p34}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102723
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x4cffb344    # 1.34060576E8f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102724
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x576f3484

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102725
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x2cae6ca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102726
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x3d41ea40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102727
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x2ee1a4b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102728
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x27fd8c6a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102729
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x12950f0e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102730
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x7ce7a42c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102731
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x13aee2a8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102732
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x26f38d22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102733
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x5c7f5535

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102734
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x47372562

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102735
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x2377a7a8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102736
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x7f391154

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102737
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x2caf33f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102738
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x6ede9477

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102739
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x4917208e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102740
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x1f769f14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102741
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x250f9e17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102742
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x2414f9b4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102743
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x616e3661

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p22 .. p22}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102744
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x296518b0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102745
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x745bd835

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102746
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x2431447b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102747
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x201fe86c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102748
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x561a872b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102749
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x2ffa91ad

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102750
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x532582a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102751
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x70103892

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102752
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x6c62c912

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102753
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x637a4e73

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102754
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x4bc06a37

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102755
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x22c75e22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102756
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x282f6163

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102757
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x5987cfd6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102758
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x143ca067

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102759
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x5f335fec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p13 .. p13}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102760
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x5852ec13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p29 .. p29}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102761
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x70671f61

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p30 .. p30}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102762
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x44e957c6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p31 .. p31}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102763
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x2238d5d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p32 .. p32}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102764
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, 0x72e46842

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p33 .. p33}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102765
    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    const v2, -0x1e18e05a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static/range {p38 .. p38}, LX/3SS;->a(LX/0Ot;)LX/0Ot;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102766
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;
    .locals 3

    .prologue
    .line 2102767
    const-class v1, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;

    monitor-enter v1

    .line 2102768
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2102769
    sput-object v2, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2102770
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102771
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->b(LX/0QB;)Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2102772
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2102773
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2102774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;
    .locals 41

    .prologue
    .line 2102775
    new-instance v2, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    const/16 v5, 0x3379

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x6fa

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x343b

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x349f

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x34a1

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x34aa

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x3378

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3423

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3473

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x3390

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x337b

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x3483

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x348c

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x337e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x34a8

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x3386

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    const/16 v21, 0x33e3

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v21

    const/16 v22, 0x342d

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v22

    const/16 v23, 0x3395

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v23

    const/16 v24, 0x33b8

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v24

    const/16 v25, 0x33d5

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v25

    const/16 v26, 0x3383

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v26

    const/16 v27, 0x337f

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v27

    const/16 v28, 0x1155

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v28

    const/16 v29, 0x3389

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v29

    const/16 v30, 0x3444

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v30

    const/16 v31, 0x346c

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v31

    const/16 v32, 0x3463

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v32

    const/16 v33, 0x345f

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v33

    const/16 v34, 0x3468

    move-object/from16 v0, p0

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v34

    const/16 v35, 0x3464

    move-object/from16 v0, p0

    move/from16 v1, v35

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v35

    const/16 v36, 0x3495

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v36

    const/16 v37, 0x343f

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v37

    const/16 v38, 0x337c

    move-object/from16 v0, p0

    move/from16 v1, v38

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v38

    const/16 v39, 0x349a

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v39

    const/16 v40, 0x3452

    move-object/from16 v0, p0

    move/from16 v1, v40

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v40

    invoke-direct/range {v2 .. v40}, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2102776
    return-object v2
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2102777
    check-cast p2, LX/Cyv;

    const/4 v7, 0x0

    .line 2102778
    instance-of v0, p2, LX/Cyw;

    if-eqz v0, :cond_1

    .line 2102779
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    new-instance v2, LX/EJG;

    move-object v1, p2

    check-cast v1, LX/Cyw;

    .line 2102780
    iget-object v3, v1, LX/Cyw;->a:Ljava/lang/String;

    move-object v1, v3

    .line 2102781
    check-cast p2, LX/Cyw;

    .line 2102782
    iget-object v3, p2, LX/Cyw;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2102783
    invoke-direct {v2, v1, v3}, LX/EJG;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2102784
    :cond_0
    :goto_0
    return-object v7

    .line 2102785
    :cond_1
    instance-of v0, p2, LX/Cz3;

    if-nez v0, :cond_2

    .line 2102786
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2102787
    :cond_2
    check-cast p2, LX/Cz3;

    .line 2102788
    invoke-virtual {p2}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v2

    .line 2102789
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v3

    .line 2102790
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    move-object v1, v0

    .line 2102791
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->fj_()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->fj_()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_2
    move v0, v0

    .line 2102792
    if-eqz v0, :cond_3

    .line 2102793
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->fj_()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v4

    invoke-static {v4, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v4

    invoke-static {p1, v0, v4}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2102794
    if-nez v3, :cond_3

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->l()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2102795
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v4, -0x26dfea9b

    if-eq v0, v4, :cond_4

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->l()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->l()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    :cond_4
    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2102796
    if-eqz v0, :cond_6

    .line 2102797
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->l()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    move-result-object v1

    invoke-static {v1, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    goto/16 :goto_0

    .line 2102798
    :cond_5
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    move-object v1, v0

    goto :goto_1

    .line 2102799
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v4, 0x4c808d5

    if-ne v0, v4, :cond_8

    .line 2102800
    invoke-static {v2}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->s()Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2102801
    invoke-static {v2}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->s()Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;

    move-result-object v1

    .line 2102802
    if-nez v1, :cond_e

    .line 2102803
    const/4 v3, 0x0

    .line 2102804
    :goto_4
    move-object v1, v3

    .line 2102805
    invoke-static {v0, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;)V

    .line 2102806
    :cond_7
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->d:LX/0Ot;

    invoke-static {v2}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->c:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    goto/16 :goto_0

    .line 2102807
    :cond_8
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    if-eqz v2, :cond_9

    .line 2102808
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-static {v0, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v3

    .line 2102809
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->i:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-static {p1, v0, v3}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->a:Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;

    new-instance v5, LX/EPd;

    invoke-virtual {p2}, LX/Cz3;->c()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v6

    invoke-virtual {v3}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    invoke-direct {v5, v6, v3, v1}, LX/EPd;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLObjectType;)V

    invoke-virtual {v0, v4, v5}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->c:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    goto/16 :goto_0

    .line 2102810
    :cond_9
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ax()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 2102811
    :goto_5
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_MIXED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v3, v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    if-eqz v2, :cond_b

    .line 2102812
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-static {v0, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v1

    .line 2102813
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->c:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    goto/16 :goto_0

    .line 2102814
    :cond_a
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_5

    .line 2102815
    :cond_b
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->b:LX/0Ot;

    invoke-static {p1, v0, v2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v3, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->a:Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;

    new-instance v4, LX/EPd;

    invoke-virtual {p2}, LX/Cz3;->c()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v5

    invoke-static {v2}, LX/8eM;->e(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    invoke-direct {v4, v5, v2, v1}, LX/EPd;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLObjectType;)V

    invoke-virtual {v0, v3, v4}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/SearchResultsRootPartDefinition;->c:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    goto/16 :goto_0

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 2102816
    :cond_e
    new-instance v5, LX/4Wh;

    invoke-direct {v5}, LX/4Wh;-><init>()V

    .line 2102817
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_10

    .line 2102818
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2102819
    const/4 v3, 0x0

    move v4, v3

    :goto_6
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_f

    .line 2102820
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;

    invoke-static {v3}, LX/A0T;->a(Lcom/facebook/search/results/protocol/common/SearchResultsStoryHighlightSnippetModels$SearchResultsStoryHighlightSnippetModel$HighlightSnippetsModel;)Lcom/facebook/graphql/model/GraphQLGraphSearchHighlightSnippet;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2102821
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    .line 2102822
    :cond_f
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2102823
    iput-object v3, v5, LX/4Wh;->f:LX/0Px;

    .line 2102824
    :cond_10
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsStoryDecorationModels$SearchResultsStoryDecorationModel;->b()LX/0Px;

    move-result-object v3

    .line 2102825
    iput-object v3, v5, LX/4Wh;->i:LX/0Px;

    .line 2102826
    invoke-virtual {v5}, LX/4Wh;->a()Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v3

    goto/16 :goto_4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2102827
    const/4 v0, 0x1

    return v0
.end method
