.class public Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/Cxk;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private a:Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EKJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2102909
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2102910
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2102911
    iput-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->d:LX/0Ot;

    .line 2102912
    iput-object p1, p0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    .line 2102913
    iput-object p2, p0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->b:LX/0Ot;

    .line 2102914
    iput-object p3, p0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->c:LX/0Ot;

    .line 2102915
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;
    .locals 6

    .prologue
    .line 2102896
    const-class v1, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;

    monitor-enter v1

    .line 2102897
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2102898
    sput-object v2, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2102899
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102900
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2102901
    new-instance v4, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    const/16 v5, 0x1eb5

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x6e3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;LX/0Ot;LX/0Ot;)V

    .line 2102902
    const/16 v3, 0x33c5

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 2102903
    iput-object v3, v4, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->d:LX/0Ot;

    .line 2102904
    move-object v0, v4

    .line 2102905
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2102906
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2102907
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2102908
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/0Px;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/common/SearchResultsSnippetInterfaces$SearchResultsSnippet$;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2102917
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2102918
    invoke-virtual {p0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/common/SearchResultsSnippetModels$SearchResultsSnippetModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2102919
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2102920
    if-eqz v0, :cond_0

    .line 2102921
    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2102922
    :goto_0
    return-object v0

    .line 2102923
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2102924
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2102925
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxk;

    .line 2102926
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2102927
    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2102928
    new-instance v3, LX/EPc;

    .line 2102929
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2102930
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2102931
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 2102932
    if-eqz v2, :cond_1

    .line 2102933
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 2102934
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->o()Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2102935
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 2102936
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->o()Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/entity/SearchResultsEntityDecorationModels$SearchResultsEntityDecorationModel$ResultDecorationModel;->a()LX/0Px;

    move-result-object v2

    :goto_0
    invoke-static {v2}, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->a(LX/0Px;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v1, v2}, LX/EPc;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    move-object v1, v3

    .line 2102937
    iget-object v2, p0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    invoke-virtual {p1, v2, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2102938
    sget-object v1, LX/1Qt;->SEARCH_DENSE_FEED:LX/1Qt;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-interface {v2}, LX/1PT;->a()LX/1Qt;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, LX/CzL;->i()LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->DENSE_STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2102939
    iget-object v2, p0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->b:LX/0Ot;

    .line 2102940
    new-instance v4, LX/C33;

    iget-object v3, p0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EKJ;

    .line 2102941
    new-instance v5, LX/EKI;

    invoke-direct {v5, v3, p2, p3}, LX/EKI;-><init>(LX/EKJ;LX/CzL;LX/Cxh;)V

    move-object v5, v5

    .line 2102942
    sget-object v6, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    .line 2102943
    iget-object v3, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 2102944
    check-cast v3, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 2102945
    iget-object v3, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 2102946
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v5, v6, v7, v3}, LX/C33;-><init>(Landroid/view/View$OnClickListener;LX/C34;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    move-object v3, v4

    .line 2102947
    invoke-static {p1, v1, v2, v3}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/SearchResultsStorySelectorPartDefinition;->c:LX/0Ot;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2102948
    const/4 v0, 0x0

    return-object v0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2102916
    const/4 v0, 0x1

    return v0
.end method
