.class public Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionsModuleInterfaces$SearchResultsElectionsNode;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/EKT;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106814
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x41400000    # 12.0f

    .line 2106815
    iput v1, v0, LX/1UY;->b:F

    .line 2106816
    move-object v0, v0

    .line 2106817
    const/high16 v1, 0x40c00000    # 6.0f

    .line 2106818
    iput v1, v0, LX/1UY;->c:F

    .line 2106819
    move-object v0, v0

    .line 2106820
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EKT;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106821
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2106822
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->e:LX/EKT;

    .line 2106823
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->f:LX/1V0;

    .line 2106824
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/Cwx;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionsModuleInterfaces$SearchResultsElectionsNode;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2106825
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->e:LX/EKT;

    const/4 v1, 0x0

    .line 2106826
    new-instance v2, LX/EKS;

    invoke-direct {v2, v0}, LX/EKS;-><init>(LX/EKT;)V

    .line 2106827
    iget-object v3, v0, LX/EKT;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EKR;

    .line 2106828
    if-nez v3, :cond_0

    .line 2106829
    new-instance v3, LX/EKR;

    invoke-direct {v3, v0}, LX/EKR;-><init>(LX/EKT;)V

    .line 2106830
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EKR;->a$redex0(LX/EKR;LX/1De;IILX/EKS;)V

    .line 2106831
    move-object v2, v3

    .line 2106832
    move-object v1, v2

    .line 2106833
    move-object v0, v1

    .line 2106834
    iget-object v1, v0, LX/EKR;->a:LX/EKS;

    iput-object p2, v1, LX/EKS;->a:LX/CzL;

    .line 2106835
    iget-object v1, v0, LX/EKR;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2106836
    move-object v0, v0

    .line 2106837
    iget-object v1, v0, LX/EKR;->a:LX/EKS;

    iput-object p3, v1, LX/EKS;->b:LX/Cwx;

    .line 2106838
    iget-object v1, v0, LX/EKR;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2106839
    move-object v0, v0

    .line 2106840
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2106841
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->d:LX/1Ua;

    sget-object v5, LX/1X9;->DIVIDER_TOP:LX/1X9;

    invoke-direct {v2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;
    .locals 6

    .prologue
    .line 2106842
    const-class v1, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;

    monitor-enter v1

    .line 2106843
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106844
    sput-object v2, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106845
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106846
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106847
    new-instance p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EKT;->a(LX/0QB;)LX/EKT;

    move-result-object v4

    check-cast v4, LX/EKT;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;-><init>(Landroid/content/Context;LX/EKT;LX/1V0;)V

    .line 2106848
    move-object v0, p0

    .line 2106849
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106850
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106851
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106852
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2106853
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cwx;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->a(LX/1De;LX/CzL;LX/Cwx;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2106854
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cwx;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->a(LX/1De;LX/CzL;LX/Cwx;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2106855
    check-cast p1, LX/CzL;

    .line 2106856
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2106857
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2106858
    const/4 v0, 0x0

    return-object v0
.end method
