.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitlePartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitlePartDefinition;Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103974
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2103975
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitlePartDefinition;

    .line 2103976
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;

    .line 2103977
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;

    .line 2103978
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;
    .locals 6

    .prologue
    .line 2103979
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;

    monitor-enter v1

    .line 2103980
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103981
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103982
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103983
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103984
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitlePartDefinition;Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;)V

    .line 2103985
    move-object v0, p0

    .line 2103986
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103987
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103988
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103989
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2103990
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/Cxo;

    const/4 v3, 0x0

    .line 2103991
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2103992
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    .line 2103993
    invoke-static {v0}, LX/EJT;->a(Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;)LX/1KL;

    move-result-object v1

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EJT;

    .line 2103994
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->n()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x96

    if-gt v0, v2, :cond_1

    .line 2103995
    :cond_0
    iput-boolean v3, v1, LX/EJT;->b:Z

    .line 2103996
    iput-boolean v3, v1, LX/EJT;->a:Z

    .line 2103997
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitlePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2103998
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2103999
    invoke-virtual {v1}, LX/EJT;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2104000
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2104001
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104002
    const/4 v0, 0x1

    move v0, v0

    .line 2104003
    return v0
.end method
