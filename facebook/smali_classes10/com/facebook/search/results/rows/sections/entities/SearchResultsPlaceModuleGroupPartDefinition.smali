.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/CxA;",
        ":",
        "LX/Cxh;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsPlaceModuleInterfaces$SearchResultsPlaceModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsPlaceModuleInterfaces$SearchResultsPlaceModule;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109739
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2109740
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    .line 2109741
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;

    .line 2109742
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;

    .line 2109743
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2109744
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;
    .locals 7

    .prologue
    .line 2109745
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;

    monitor-enter v1

    .line 2109746
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109747
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109748
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109749
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109750
    new-instance p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V

    .line 2109751
    move-object v0, p0

    .line 2109752
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109753
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109754
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109755
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2109756
    check-cast p2, LX/CzL;

    .line 2109757
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2109758
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleMapPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2109759
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/EJ4;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/CzL;)V

    .line 2109760
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2109761
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2109762
    const/4 v0, 0x1

    return v0
.end method
