.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextAttributionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/ENS;",
        "Ljava/lang/CharSequence;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/11S;


# direct methods
.method public constructor <init>(LX/11S;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112658
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2112659
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextAttributionPartDefinition;->a:LX/11S;

    .line 2112660
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2112661
    check-cast p2, LX/ENS;

    check-cast p3, LX/1Pn;

    .line 2112662
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2112663
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2112664
    iget-wide v3, p2, LX/ENS;->b:J

    .line 2112665
    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextAttributionPartDefinition;->a:LX/11S;

    sget-object v7, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v3

    invoke-interface {v6, v7, v8, v9}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v6

    :goto_0
    move-object v2, v6

    .line 2112666
    if-eqz v2, :cond_0

    .line 2112667
    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112668
    :cond_0
    iget-object v3, p2, LX/ENS;->a:Ljava/lang/String;

    .line 2112669
    if-eqz v3, :cond_2

    .line 2112670
    if-eqz v2, :cond_1

    .line 2112671
    const v2, 0x7f08114e

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2112672
    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112673
    :cond_1
    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112674
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00d9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v2, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int v3, v4, v3

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    const/16 v5, 0x11

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2112675
    :cond_2
    move-object v0, v1

    .line 2112676
    return-object v0

    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5886ed90

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2112677
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 2112678
    invoke-virtual {p4, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2112679
    const/16 v1, 0x1f

    const v2, -0x4281f754

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
