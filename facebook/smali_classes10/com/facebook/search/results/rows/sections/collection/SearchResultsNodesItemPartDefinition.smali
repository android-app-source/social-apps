.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/EJZ;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104568
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104569
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    .line 2104570
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;

    .line 2104571
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    .line 2104572
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    .line 2104573
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;

    .line 2104574
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;

    .line 2104575
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;
    .locals 10

    .prologue
    .line 2104576
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;

    monitor-enter v1

    .line 2104577
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104578
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104579
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104580
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104581
    new-instance v3, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;)V

    .line 2104582
    move-object v0, v3

    .line 2104583
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104584
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104585
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104586
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2104587
    check-cast p2, LX/EJZ;

    .line 2104588
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowFigPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageRowPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2104589
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104590
    const/4 v0, 0x1

    move v0, v0

    .line 2104591
    return v0
.end method
