.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsTimelineHeaderModuleInterfaces$SearchResultsTimelineHeaderModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111683
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2111684
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;

    .line 2111685
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;
    .locals 4

    .prologue
    .line 2111686
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;

    monitor-enter v1

    .line 2111687
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111688
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111689
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111690
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111691
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;)V

    .line 2111692
    move-object v0, p0

    .line 2111693
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111694
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111695
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111696
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2111697
    check-cast p2, LX/CzL;

    .line 2111698
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;

    .line 2111699
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2111700
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111701
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2111702
    check-cast p1, LX/CzL;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2111703
    new-array v3, v1, [Ljava/lang/CharSequence;

    .line 2111704
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2111705
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method
