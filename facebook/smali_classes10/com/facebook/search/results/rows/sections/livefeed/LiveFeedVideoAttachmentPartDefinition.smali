.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112336
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2112337
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    .line 2112338
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;
    .locals 4

    .prologue
    .line 2112321
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    monitor-enter v1

    .line 2112322
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112323
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112324
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112325
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112326
    new-instance p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;)V

    .line 2112327
    move-object v0, p0

    .line 2112328
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112329
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112330
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112331
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2112333
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2112334
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedVideoAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerPartDefinition;

    new-instance v1, LX/3EE;

    invoke-static {p2}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    const/4 v3, -0x1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    new-instance v5, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v5}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, LX/3EE;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;ILX/0am;Ljava/util/concurrent/atomic/AtomicReference;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2112335
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xcc97e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2112332
    const/16 v1, 0x1f

    const v2, 0x21e805ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
