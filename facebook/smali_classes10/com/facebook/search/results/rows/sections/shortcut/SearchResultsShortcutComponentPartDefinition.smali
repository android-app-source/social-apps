.class public Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxh;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/shortcut/SearchResultsShortcutInterfaces$SearchResultsShortcut;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;

.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/EPS;

.field public final f:LX/17W;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2116884
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 2116885
    iput v1, v0, LX/1UY;->b:F

    .line 2116886
    move-object v0, v0

    .line 2116887
    const/high16 v1, -0x3ec00000    # -12.0f

    .line 2116888
    iput v1, v0, LX/1UY;->c:F

    .line 2116889
    move-object v0, v0

    .line 2116890
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EPS;LX/17W;LX/0Ot;LX/1V0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/EPS;",
            "LX/17W;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;",
            "LX/1V0;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116828
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2116829
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->e:LX/EPS;

    .line 2116830
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->f:LX/17W;

    .line 2116831
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->g:LX/0Ot;

    .line 2116832
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->h:LX/1V0;

    .line 2116833
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/Cxh;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/shortcut/SearchResultsShortcutInterfaces$SearchResultsShortcut;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2116851
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->e:LX/EPS;

    const/4 v1, 0x0

    .line 2116852
    new-instance v2, LX/EPR;

    invoke-direct {v2, v0}, LX/EPR;-><init>(LX/EPS;)V

    .line 2116853
    sget-object v3, LX/EPS;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EPQ;

    .line 2116854
    if-nez v3, :cond_0

    .line 2116855
    new-instance v3, LX/EPQ;

    invoke-direct {v3}, LX/EPQ;-><init>()V

    .line 2116856
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EPQ;->a$redex0(LX/EPQ;LX/1De;IILX/EPR;)V

    .line 2116857
    move-object v2, v3

    .line 2116858
    move-object v1, v2

    .line 2116859
    move-object v1, v1

    .line 2116860
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116861
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->ak()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2116862
    iget-object v2, v1, LX/EPQ;->a:LX/EPR;

    iput-object v0, v2, LX/EPR;->a:Landroid/net/Uri;

    .line 2116863
    iget-object v2, v1, LX/EPQ;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2116864
    move-object v1, v1

    .line 2116865
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116866
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2116867
    iget-object v2, v1, LX/EPQ;->a:LX/EPR;

    iput-object v0, v2, LX/EPR;->b:Ljava/lang/CharSequence;

    .line 2116868
    iget-object v2, v1, LX/EPQ;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2116869
    move-object v0, v1

    .line 2116870
    new-instance v1, LX/EPT;

    invoke-direct {v1, p0, p2, p3}, LX/EPT;-><init>(Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;LX/CzL;LX/Cxh;)V

    move-object v1, v1

    .line 2116871
    iget-object v2, v0, LX/EPQ;->a:LX/EPR;

    iput-object v1, v2, LX/EPR;->c:LX/EPT;

    .line 2116872
    iget-object v2, v0, LX/EPQ;->d:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2116873
    move-object v0, v0

    .line 2116874
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2116875
    new-instance v2, LX/1X6;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->d:LX/1Ua;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    .line 2116876
    invoke-interface {v0}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object p2

    if-nez p2, :cond_1

    invoke-interface {v0}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object p2

    if-nez p2, :cond_1

    .line 2116877
    sget-object p2, LX/1X9;->BOX:LX/1X9;

    .line 2116878
    :goto_0
    move-object v0, p2

    .line 2116879
    invoke-direct {v2, v3, v4, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 2116880
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->h:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v0, p1, p3, v2, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 2116881
    :cond_1
    invoke-interface {v0}, LX/1Ps;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object p2

    if-eqz p2, :cond_3

    .line 2116882
    invoke-interface {v0}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object p2

    if-eqz p2, :cond_2

    sget-object p2, LX/1X9;->MIDDLE:LX/1X9;

    goto :goto_0

    :cond_2
    sget-object p2, LX/1X9;->BOTTOM:LX/1X9;

    goto :goto_0

    .line 2116883
    :cond_3
    sget-object p2, LX/1X9;->TOP:LX/1X9;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;
    .locals 9

    .prologue
    .line 2116840
    const-class v1, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    monitor-enter v1

    .line 2116841
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116842
    sput-object v2, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116843
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116844
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116845
    new-instance v3, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/EPS;->a(LX/0QB;)LX/EPS;

    move-result-object v5

    check-cast v5, LX/EPS;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v6

    check-cast v6, LX/17W;

    const/16 v7, 0x113f

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;-><init>(Landroid/content/Context;LX/EPS;LX/17W;LX/0Ot;LX/1V0;)V

    .line 2116846
    move-object v0, v3

    .line 2116847
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116848
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116849
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116850
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2116839
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxh;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->a(LX/1De;LX/CzL;LX/Cxh;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2116838
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxh;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->a(LX/1De;LX/CzL;LX/Cxh;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116835
    check-cast p1, LX/CzL;

    .line 2116836
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116837
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2116834
    const/4 v0, 0x0

    return-object v0
.end method
