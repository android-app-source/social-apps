.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<TT;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106171
    sget-object v0, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->b:LX/1Cz;

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;->a:LX/1Cz;

    .line 2106172
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 2106173
    iput v1, v0, LX/1UY;->b:F

    .line 2106174
    move-object v0, v0

    .line 2106175
    const/high16 v1, -0x40000000    # -2.0f

    .line 2106176
    iput v1, v0, LX/1UY;->c:F

    .line 2106177
    move-object v0, v0

    .line 2106178
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106151
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2106152
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 2106153
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;
    .locals 4

    .prologue
    .line 2106160
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;

    monitor-enter v1

    .line 2106161
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106162
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106163
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106164
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106165
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V

    .line 2106166
    move-object v0, p0

    .line 2106167
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106168
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106169
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106170
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2106159
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2106156
    const/4 v5, 0x0

    .line 2106157
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v1, LX/3aw;

    sget-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    const/4 v4, 0x0

    invoke-direct {v1, v5, v2, v3, v4}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2106158
    return-object v5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 2106155
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 2106154
    sget-object v0, LX/1X8;->GAP_PART_DEFINITION:LX/1X8;

    return-object v0
.end method
