.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104662
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104663
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->a:LX/0Ot;

    .line 2104664
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->b:LX/0Ot;

    .line 2104665
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

    .line 2104666
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    .line 2104667
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;
    .locals 7

    .prologue
    .line 2104671
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;

    monitor-enter v1

    .line 2104672
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104673
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104674
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104675
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104676
    new-instance v5, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;

    const/16 v3, 0x3433

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x6e3

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    invoke-direct {v5, v6, p0, v3, v4}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;)V

    .line 2104677
    move-object v0, v5

    .line 2104678
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104679
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104680
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104681
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2104682
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    .line 2104683
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104684
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->LIVE_CONVERSATION_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 2104685
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2104686
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104687
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_1
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2104688
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2104689
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->a:LX/0Ot;

    invoke-static {p1, v1, v5, v0}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->b:LX/0Ot;

    invoke-virtual {v5, v6, v0}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2104690
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v1, v2

    .line 2104691
    goto :goto_0

    .line 2104692
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2104693
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104668
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104669
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104670
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
