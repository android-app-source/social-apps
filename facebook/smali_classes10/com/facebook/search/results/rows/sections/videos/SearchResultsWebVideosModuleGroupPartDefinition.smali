.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/video/SearchResultsWebVideosModuleInterfaces$SearchResultsWebVideosModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117921
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2117922
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    .line 2117923
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    .line 2117924
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2117925
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;
    .locals 6

    .prologue
    .line 2117928
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;

    monitor-enter v1

    .line 2117929
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117930
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117931
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117932
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117933
    new-instance p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V

    .line 2117934
    move-object v0, p0

    .line 2117935
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117936
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117937
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117938
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2117939
    check-cast p2, LX/CzL;

    const/4 v1, 0x0

    .line 2117940
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    .line 2117941
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 2117942
    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117943
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117944
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->cc()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    .line 2117945
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2117946
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->u()LX/A52;

    move-result-object v0

    invoke-virtual {p2, v0, v2}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v0

    invoke-virtual {p1, v5, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117947
    add-int/lit8 v2, v2, 0x1

    .line 2117948
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2117949
    :cond_0
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117950
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2117951
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideosModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117952
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2117926
    check-cast p1, LX/CzL;

    .line 2117927
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
