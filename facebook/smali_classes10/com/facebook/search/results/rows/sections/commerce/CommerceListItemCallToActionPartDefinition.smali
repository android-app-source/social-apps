.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104781
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2104782
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    .line 2104783
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;

    .line 2104784
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;
    .locals 5

    .prologue
    .line 2104785
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;

    monitor-enter v1

    .line 2104786
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104787
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104788
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104789
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104790
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;)V

    .line 2104791
    move-object v0, p0

    .line 2104792
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104793
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104794
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104795
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2104796
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2104797
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104798
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104799
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2104800
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method
