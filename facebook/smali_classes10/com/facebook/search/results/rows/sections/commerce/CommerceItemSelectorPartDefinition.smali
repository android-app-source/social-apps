.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;>;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104704
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104705
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;

    .line 2104706
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    .line 2104707
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;

    .line 2104708
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;
    .locals 6

    .prologue
    .line 2104709
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;

    monitor-enter v1

    .line 2104710
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104711
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104712
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104713
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104714
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;)V

    .line 2104715
    move-object v0, p0

    .line 2104716
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104717
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104718
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104719
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2104720
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/Cxo;

    .line 2104721
    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    .line 2104722
    invoke-static {v0}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2104723
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;

    .line 2104724
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2104725
    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;

    .line 2104726
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2104727
    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2104728
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2104729
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;

    .line 2104730
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2104731
    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2104732
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104733
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104734
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    .line 2104735
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104736
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    .line 2104737
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104738
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
