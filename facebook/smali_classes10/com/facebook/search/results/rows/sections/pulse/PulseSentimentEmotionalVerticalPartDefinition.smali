.class public Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115784
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2115785
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;

    .line 2115786
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;
    .locals 4

    .prologue
    .line 2115787
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;

    monitor-enter v1

    .line 2115788
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115789
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115790
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115791
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115792
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;)V

    .line 2115793
    move-object v0, p0

    .line 2115794
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115795
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115796
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115797
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2115798
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    .line 2115799
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;

    new-instance v1, LX/EP3;

    .line 2115800
    iget v2, p2, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->b:I

    move v2, v2

    .line 2115801
    invoke-direct {v1, v2}, LX/EP3;-><init>(I)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115802
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-object v0, v0

    .line 2115803
    if-eqz v0, :cond_0

    .line 2115804
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-object v0, v0

    .line 2115805
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;->a()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2115806
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-object v0, v0

    .line 2115807
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;->a()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2115808
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    move-object v0, v0

    .line 2115809
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;->a()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;->a()LX/0Px;

    move-result-object v2

    .line 2115810
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsEdge;

    .line 2115811
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItem;

    move-result-object v0

    .line 2115812
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalItemPartDefinition;

    new-instance v5, LX/EP3;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItem;->a()I

    move-result v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItem;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-direct {v5, v6, v7, v0}, LX/EP3;-><init>(Ljava/lang/String;ILcom/facebook/graphql/model/GraphQLImage;)V

    invoke-virtual {p1, v4, v5}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115813
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2115814
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115815
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    .line 2115816
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->o()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
