.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A4u;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pn;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final b:LX/0ad;

.field public final c:LX/154;

.field public final d:LX/11S;

.field public final e:LX/0W9;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;LX/0ad;LX/154;LX/11S;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117666
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2117667
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2117668
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->b:LX/0ad;

    .line 2117669
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->c:LX/154;

    .line 2117670
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->d:LX/11S;

    .line 2117671
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->e:LX/0W9;

    .line 2117672
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;
    .locals 9

    .prologue
    .line 2117655
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    monitor-enter v1

    .line 2117656
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117657
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117658
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117659
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117660
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v6

    check-cast v6, LX/154;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v7

    check-cast v7, LX/11S;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v8

    check-cast v8, LX/0W9;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;LX/0ad;LX/154;LX/11S;LX/0W9;)V

    .line 2117661
    move-object v0, v3

    .line 2117662
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117663
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117664
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117665
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/A4u;LX/1Pn;)Ljava/lang/CharSequence;
    .locals 11

    .prologue
    .line 2117607
    invoke-interface {p1}, LX/A4u;->ap()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117608
    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2117609
    invoke-interface {p1}, LX/A4u;->au()I

    move-result v1

    .line 2117610
    if-gtz v1, :cond_1

    .line 2117611
    const-string v0, ""

    .line 2117612
    :goto_0
    move-object v0, v0

    .line 2117613
    :goto_1
    return-object v0

    :cond_0
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2117614
    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2117615
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->d:LX/11S;

    sget-object v3, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    invoke-interface {p1}, LX/A4u;->V()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-interface {v1, v3, v5, v6}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 2117616
    invoke-interface {p1}, LX/A4u;->aE()I

    move-result v3

    .line 2117617
    if-gtz v3, :cond_2

    .line 2117618
    :goto_2
    move-object v0, v1

    .line 2117619
    goto :goto_1

    .line 2117620
    :cond_1
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->c:LX/154;

    invoke-virtual {v2, v1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->e:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2117621
    const v2, 0x7f080d4b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2117622
    :cond_2
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->c:LX/154;

    invoke-virtual {v4, v3, v9}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->e:LX/0W9;

    invoke-virtual {v5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 2117623
    const v5, 0x7f0f0051

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {v2, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2117624
    const v4, 0x7f080d4a

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v9

    aput-object v3, v5, v10

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLMedia;LX/1Pn;)Ljava/lang/CharSequence;
    .locals 11

    .prologue
    .line 2117637
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117638
    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2117639
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->az()I

    move-result v1

    .line 2117640
    if-gtz v1, :cond_1

    .line 2117641
    const-string v0, ""

    .line 2117642
    :goto_0
    move-object v0, v0

    .line 2117643
    :goto_1
    return-object v0

    :cond_0
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2117644
    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 2117645
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->d:LX/11S;

    sget-object v3, LX/1lB;->SHORT_DATE_STYLE:LX/1lB;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->F()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-interface {v1, v3, v5, v6}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 2117646
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aO()I

    move-result v3

    .line 2117647
    if-gtz v3, :cond_2

    .line 2117648
    :goto_2
    move-object v0, v1

    .line 2117649
    goto :goto_1

    .line 2117650
    :cond_1
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->c:LX/154;

    invoke-virtual {v2, v1}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->e:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 2117651
    const v2, 0x7f080d4b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2117652
    :cond_2
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->c:LX/154;

    invoke-virtual {v4, v3, v9}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->e:LX/0W9;

    invoke-virtual {v5}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    .line 2117653
    const v5, 0x7f0f0051

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v4, v6, v9

    invoke-virtual {v2, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2117654
    const v4, 0x7f080d4a

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v9

    aput-object v3, v5, v10

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2117625
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 2117626
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117627
    check-cast v0, LX/A4u;

    invoke-interface {v0}, LX/A4u;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2117628
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2117629
    invoke-static {v0}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2117630
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2117631
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->b:LX/0ad;

    invoke-static {v1}, LX/7Ax;->c(LX/0ad;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/1Pn;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2117632
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2117633
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->a:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2117634
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 2117635
    :cond_1
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117636
    check-cast v0, LX/A4u;

    invoke-direct {p0, v0, p3}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->a(LX/A4u;LX/1Pn;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method
