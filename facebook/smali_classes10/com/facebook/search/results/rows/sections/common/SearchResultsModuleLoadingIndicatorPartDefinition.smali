.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsModuleLoadingIndicatorPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<-",
        "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
        ">;TE;>;"
    }
.end annotation


# instance fields
.field private final d:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106141
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2106142
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsModuleLoadingIndicatorPartDefinition;->d:LX/1V0;

    .line 2106143
    return-void
.end method

.method private a(LX/1De;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2106144
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0822b9

    invoke-virtual {v0, v1}, LX/1ne;->h(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2106145
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsModuleLoadingIndicatorPartDefinition;->d:LX/1V0;

    check-cast p2, LX/1Ps;

    new-instance v2, LX/1X6;

    const/4 v3, 0x0

    sget-object v4, LX/1Ua;->a:LX/1Ua;

    sget-object v5, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-virtual {v1, p1, p2, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2106146
    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p3}, Lcom/facebook/search/results/rows/sections/common/SearchResultsModuleLoadingIndicatorPartDefinition;->a(LX/1De;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2106147
    invoke-direct {p0, p1, p3}, Lcom/facebook/search/results/rows/sections/common/SearchResultsModuleLoadingIndicatorPartDefinition;->a(LX/1De;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2106148
    check-cast p1, LX/CzL;

    .line 2106149
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2106150
    const/4 v0, 0x0

    return-object v0
.end method
