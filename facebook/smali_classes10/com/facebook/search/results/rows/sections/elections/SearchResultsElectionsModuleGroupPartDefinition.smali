.class public Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/elections/SearchResultsElectionsModuleInterfaces$SearchResultsElectionsModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107278
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2107279
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    .line 2107280
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;

    .line 2107281
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;
    .locals 5

    .prologue
    .line 2107282
    const-class v1, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;

    monitor-enter v1

    .line 2107283
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107284
    sput-object v2, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107285
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107286
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107287
    new-instance p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;)V

    .line 2107288
    move-object v0, p0

    .line 2107289
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107290
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107291
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107292
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2107293
    check-cast p2, LX/CzL;

    .line 2107294
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2107295
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionsModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/elections/SearchResultsElectionComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/EJ4;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/CzL;)V

    .line 2107296
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2107297
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 2107298
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2107299
    :goto_0
    return v0

    .line 2107300
    :cond_0
    invoke-static {p1, v1}, LX/CzM;->b(LX/CzL;I)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2107301
    if-nez v0, :cond_1

    move v0, v1

    .line 2107302
    goto :goto_0

    .line 2107303
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->aR()Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;

    move-result-object v0

    .line 2107304
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    .line 2107305
    goto :goto_0

    .line 2107306
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionModel;->b()LX/0Px;

    move-result-object v0

    .line 2107307
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/elections/SearchResultsElectionModels$SearchResultsElectionDateModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method
