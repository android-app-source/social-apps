.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ":",
        "LX/Cxc;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110557
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2110558
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    .line 2110559
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    .line 2110560
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;
    .locals 5

    .prologue
    .line 2110561
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;

    monitor-enter v1

    .line 2110562
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110563
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110564
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110565
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110566
    new-instance p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;)V

    .line 2110567
    move-object v0, p0

    .line 2110568
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110569
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110570
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110571
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2110572
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    .line 2110573
    new-instance v1, LX/EJZ;

    .line 2110574
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2110575
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    .line 2110576
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 2110577
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2110578
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;

    .line 2110579
    iget-object p2, v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->d:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-object v0, p2

    .line 2110580
    invoke-direct {v1, v2, v0, v3}, LX/EJZ;-><init>(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)V

    .line 2110581
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowFigPartDefinition;

    invoke-static {p1, v0, v1}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsSingleEntityNodePartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;

    invoke-virtual {v0, v2, v1}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2110582
    return-object v3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2110583
    const/4 v0, 0x1

    return v0
.end method
