.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<+",
            "LX/CzL",
            "<-",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
            ">;-",
            "LX/Cxk;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModulePageGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104110
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->a:Ljava/util/Map;

    .line 2104112
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2104113
    move-object v2, p3

    .line 2104114
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2104115
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->a:Ljava/util/Map;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2104116
    move-object v2, p4

    .line 2104117
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2104118
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->b:LX/0Ot;

    .line 2104119
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->c:LX/0Ot;

    .line 2104120
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;
    .locals 7

    .prologue
    .line 2104099
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;

    monitor-enter v1

    .line 2104100
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104101
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104102
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104103
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104104
    new-instance v3, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;

    const/16 v4, 0x6f3

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x11a4

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3394

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x3396

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2104105
    move-object v0, v3

    .line 2104106
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104107
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104108
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104109
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2104091
    check-cast p2, LX/CzL;

    const/4 v5, 0x0

    .line 2104092
    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    .line 2104093
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2104094
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->b:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2104095
    :goto_0
    return-object v5

    .line 2104096
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->c:LX/0Ot;

    new-instance v2, LX/EPd;

    invoke-virtual {p2}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    invoke-virtual {p2}, LX/CzL;->g()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, LX/EPd;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLObjectType;)V

    invoke-static {p1, v1, v2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleSelectorPartDefinition;->b:LX/0Ot;

    .line 2104097
    move-object v1, v1

    .line 2104098
    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104090
    const/4 v0, 0x1

    return v0
.end method
