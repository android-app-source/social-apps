.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/Cxh;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/CxV;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EMI;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2109719
    const-class v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/EMI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2109721
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->b:LX/0Ot;

    .line 2109722
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->c:LX/0Or;

    .line 2109723
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->d:LX/0Ot;

    .line 2109724
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->e:LX/0Ot;

    .line 2109725
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->f:LX/0Ot;

    .line 2109726
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;
    .locals 9

    .prologue
    .line 2109727
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;

    monitor-enter v1

    .line 2109728
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109729
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109730
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109731
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109732
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;

    const/16 v4, 0x3412

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x509

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x3512

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xb19

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2eb

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;-><init>(LX/0Ot;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2109733
    move-object v0, v3

    .line 2109734
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109735
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109736
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109737
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/CxA;LX/CzL;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)LX/1Dg;
    .locals 6
    .param p2    # LX/CxA;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/CzL;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "LX/CzL",
            "<+",
            "LX/8dB;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Z",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2109738
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p8}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v3

    if-nez p7, :cond_1

    const v1, 0x7f021491

    :goto_0
    invoke-virtual {v3, v1}, LX/1up;->h(I)LX/1up;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1, p7}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v1

    sget-object v4, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v3, 0x7f0b1715

    invoke-interface {v1, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    const v3, 0x7f0b1715

    invoke-interface {v1, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v1, v3}, LX/1Di;->b(I)LX/1Di;

    move-result-object v1

    const/4 v3, 0x5

    const v4, 0x7f0b1713

    invoke-interface {v1, v3, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-static {p1}, LX/ELm;->d(LX/1De;)LX/1dQ;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v1, v3}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/ELm;->d(LX/1De;)LX/1dQ;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    if-eqz p6, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8i7;

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, p5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, LX/8i7;->b(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object p5

    :cond_0
    invoke-virtual {v4, p5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    const v4, 0x7f0b0052

    invoke-virtual {v1, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v4, 0x7f0a00ab

    invoke-virtual {v1, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00a4

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static/range {p11 .. p11}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static/range {p12 .. p12}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPlaceComponentSpec;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EMI;

    invoke-virtual {v1, p1}, LX/EMI;->c(LX/1De;)LX/EMG;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/EMG;->a(LX/CzL;)LX/EMG;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/EMG;->a(LX/CxA;)LX/EMG;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/EMG;->b(Ljava/lang/String;)LX/EMG;

    move-result-object v1

    move/from16 v0, p10

    invoke-virtual {v1, v0}, LX/EMG;->a(Z)LX/EMG;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_2
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    const v4, 0x7f0b0050

    invoke-virtual {v1, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v4, 0x7f0a00a4

    invoke-virtual {v1, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    goto :goto_1

    :cond_3
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v1

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    const v4, 0x7f0b0050

    invoke-virtual {v1, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    const v4, 0x7f0a00a4

    invoke-virtual {v1, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    goto/16 :goto_2
.end method
