.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxy;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/EL8;

.field private static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGroupJoinState;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityTitlePartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2108117
    new-instance v0, LX/EL8;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, LX/EL8;-><init>(IILandroid/view/View$OnClickListener;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a:LX/EL8;

    .line 2108118
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const v1, 0x7f020973

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_JOIN:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const v3, 0x7f020973

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const v5, 0x7f0207db

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const v7, 0x7f0207db

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityTitlePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108113
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2108114
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityTitlePartDefinition;

    .line 2108115
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    .line 2108116
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/0jW;
    .locals 1

    .prologue
    .line 2108112
    new-instance v0, LX/ELA;

    invoke-direct {v0, p0}, LX/ELA;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Lcom/facebook/graphql/enums/GraphQLGroupCategory;Landroid/view/View$OnClickListener;)LX/EL8;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2108063
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne p0, v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne p1, v0, :cond_1

    move v0, v1

    .line 2108064
    :goto_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne p0, v3, :cond_2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne p1, v3, :cond_2

    move v3, v1

    .line 2108065
    :goto_1
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->NONE:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    invoke-virtual {p2, v5}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2108066
    :goto_2
    if-nez v3, :cond_4

    if-nez v0, :cond_4

    if-nez v1, :cond_4

    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->b:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2108067
    :goto_3
    new-instance v1, LX/EL8;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq p0, v0, :cond_5

    const v0, 0x7f08226d

    :goto_4
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq p0, v3, :cond_6

    :goto_5
    invoke-direct {v1, v2, v0, p3}, LX/EL8;-><init>(IILandroid/view/View$OnClickListener;)V

    return-object v1

    :cond_1
    move v0, v2

    .line 2108068
    goto :goto_0

    :cond_2
    move v3, v2

    .line 2108069
    goto :goto_1

    :cond_3
    move v1, v2

    .line 2108070
    goto :goto_2

    :cond_4
    move-object v0, v4

    .line 2108071
    goto :goto_3

    .line 2108072
    :cond_5
    const v0, 0x7f08226e

    goto :goto_4

    :cond_6
    move-object p3, v4

    goto :goto_5
.end method

.method public static a(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;LX/CzL;LX/Cxy;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
            ">;",
            "LX/Cxy;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2108111
    new-instance v0, LX/EL9;

    invoke-direct {v0, p0, p2, p1}, LX/EL9;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;LX/Cxy;LX/CzL;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2108073
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 2108074
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityTitlePartDefinition;

    .line 2108075
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2108076
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108077
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    .line 2108078
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2108079
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-static {v1}, LX/ELM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)I

    move-result v1

    .line 2108080
    sparse-switch v1, :sswitch_data_0

    .line 2108081
    sget-object v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a:LX/EL8;

    :goto_0
    move-object v1, v1

    .line 2108082
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108083
    const/4 v0, 0x0

    return-object v0

    .line 2108084
    :sswitch_0
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2108085
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2108086
    new-instance v3, LX/EJK;

    invoke-direct {v3, v1}, LX/EJK;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)V

    .line 2108087
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->w()Z

    move-result v4

    move-object v2, p3

    .line 2108088
    check-cast v2, LX/1Pr;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/0jW;

    move-result-object v1

    invoke-interface {v2, v3, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2108089
    check-cast p3, LX/Cxy;

    invoke-static {p0, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;LX/CzL;LX/Cxy;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 2108090
    if-eqz v4, :cond_1

    if-eqz v1, :cond_1

    .line 2108091
    const/4 v3, 0x0

    .line 2108092
    :goto_1
    new-instance p2, LX/EL8;

    if-eqz v4, :cond_3

    const p0, 0x7f08226c

    :goto_2
    if-eqz v4, :cond_0

    const/4 v2, 0x0

    :cond_0
    invoke-direct {p2, v3, p0, v2}, LX/EL8;-><init>(IILandroid/view/View$OnClickListener;)V

    move-object v1, p2

    .line 2108093
    move-object v1, v1

    .line 2108094
    goto :goto_0

    .line 2108095
    :sswitch_1
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2108096
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2108097
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v2

    .line 2108098
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    .line 2108099
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    .line 2108100
    check-cast p3, LX/Cxy;

    invoke-static {p0, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;LX/CzL;LX/Cxy;)Landroid/view/View$OnClickListener;

    move-result-object v4

    .line 2108101
    invoke-static {v2, v3, v1, v4}, LX/ELK;->a(ZLcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Landroid/view/View$OnClickListener;)LX/EL8;

    move-result-object v1

    move-object v1, v1

    .line 2108102
    goto :goto_0

    .line 2108103
    :sswitch_2
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2108104
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2108105
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    move-object v2, p3

    .line 2108106
    check-cast v2, LX/1Pr;

    new-instance v4, LX/EJJ;

    invoke-direct {v4, v1}, LX/EJJ;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)V

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/0jW;

    move-result-object v5

    invoke-interface {v2, v4, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2108107
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    check-cast p3, LX/Cxy;

    invoke-static {p0, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;LX/CzL;LX/Cxy;)Landroid/view/View$OnClickListener;

    move-result-object v4

    invoke-static {v3, v2, v1, v4}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Lcom/facebook/graphql/enums/GraphQLGroupCategory;Landroid/view/View$OnClickListener;)LX/EL8;

    move-result-object v1

    move-object v1, v1

    .line 2108108
    goto/16 :goto_0

    .line 2108109
    :cond_1
    if-eqz v4, :cond_2

    const v3, 0x7f0207d6

    goto :goto_1

    :cond_2
    const v3, 0x7f0208fa

    goto :goto_1

    .line 2108110
    :cond_3
    const p0, 0x7f08226b

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_0
        0x403827a -> :sswitch_1
        0x41e065f -> :sswitch_2
    .end sparse-switch
.end method
