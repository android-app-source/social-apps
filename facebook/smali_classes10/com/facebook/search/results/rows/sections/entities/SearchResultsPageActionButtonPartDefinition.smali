.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d0;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p7    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109411
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2109412
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    .line 2109413
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->b:LX/0Ot;

    .line 2109414
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->c:LX/0Ot;

    .line 2109415
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->d:LX/0Ot;

    .line 2109416
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->e:LX/0Ot;

    .line 2109417
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->f:LX/0Ot;

    .line 2109418
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->g:LX/0Ot;

    .line 2109419
    return-void
.end method

.method public static a(LX/8d0;)LX/0jW;
    .locals 1

    .prologue
    .line 2109410
    new-instance v0, LX/ELe;

    invoke-direct {v0, p0}, LX/ELe;-><init>(LX/8d0;)V

    return-object v0
.end method

.method public static a(LX/CzL;LX/1Pr;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)LX/EL8;
    .locals 6
    .param p7    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pr;",
            ":",
            "LX/1Pq;",
            ":",
            "LX/CxA;",
            ":",
            "LX/CxV;",
            ":",
            "LX/CxP;",
            ">(",
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;TE;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)",
            "LX/EL8;"
        }
    .end annotation

    .prologue
    .line 2109393
    new-instance v1, LX/EL8;

    .line 2109394
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109395
    check-cast v0, LX/8d0;

    .line 2109396
    new-instance v2, LX/ELU;

    invoke-interface {v0}, LX/8d0;->dW_()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/8d0;->w()Z

    move-result v4

    invoke-direct {v2, v3, v4}, LX/ELU;-><init>(Ljava/lang/String;Z)V

    .line 2109397
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->a(LX/8d0;)LX/0jW;

    move-result-object v3

    invoke-interface {p1, v2, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 2109398
    invoke-interface {v0}, LX/8d0;->w()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v0}, LX/8d0;->u()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2109399
    const v2, 0x7f0208fa

    .line 2109400
    :goto_0
    move v2, v2

    .line 2109401
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109402
    check-cast v0, LX/8d0;

    .line 2109403
    new-instance v3, LX/ELU;

    invoke-interface {v0}, LX/8d0;->dW_()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, LX/8d0;->w()Z

    move-result v5

    invoke-direct {v3, v4, v5}, LX/ELU;-><init>(Ljava/lang/String;Z)V

    .line 2109404
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->a(LX/8d0;)LX/0jW;

    move-result-object v4

    invoke-interface {p1, v3, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 2109405
    if-eqz v3, :cond_2

    const v3, 0x7f08226c

    :goto_1
    move v0, v3

    .line 2109406
    invoke-static/range {p0 .. p7}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->b(LX/CzL;LX/1Pr;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, LX/EL8;-><init>(IILandroid/view/View$OnClickListener;)V

    return-object v1

    .line 2109407
    :cond_0
    invoke-interface {v0}, LX/8d0;->w()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 2109408
    const v2, 0x7f0207d6

    goto :goto_0

    .line 2109409
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const v3, 0x7f08226b

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;
    .locals 11

    .prologue
    .line 2109376
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;

    monitor-enter v1

    .line 2109377
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109378
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109379
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109380
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109381
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    const/16 v5, 0x12b1

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x474

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x32d4

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x113f

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x32bb

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x140d

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2109382
    move-object v0, v3

    .line 2109383
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109384
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109385
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109386
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/CzL;LX/1Pr;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)Landroid/view/View$OnClickListener;
    .locals 9
    .param p7    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pr;",
            ":",
            "LX/1Pq;",
            ":",
            "LX/CxA;",
            ":",
            "LX/CxV;",
            ":",
            "LX/CxP;",
            ">(",
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;TE;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2109390
    invoke-virtual {p0}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8d0;

    invoke-interface {v0}, LX/8d0;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109391
    const/4 v0, 0x0

    .line 2109392
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/ELd;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p5

    move-object/from16 v5, p7

    move-object v6, p6

    move-object v7, p4

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, LX/ELd;-><init>(LX/CzL;LX/1Pr;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2109387
    check-cast p2, LX/CzL;

    check-cast p3, LX/CxA;

    .line 2109388
    iget-object v8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    move-object v1, p3

    check-cast v1, LX/1Pr;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->e:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->b:LX/0Ot;

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->d:LX/0Ot;

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->c:LX/0Ot;

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->f:LX/0Ot;

    iget-object v7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->g:LX/0Ot;

    move-object v0, p2

    invoke-static/range {v0 .. v7}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->a(LX/CzL;LX/1Pr;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)LX/EL8;

    move-result-object v0

    invoke-interface {p1, v8, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109389
    const/4 v0, 0x0

    return-object v0
.end method
