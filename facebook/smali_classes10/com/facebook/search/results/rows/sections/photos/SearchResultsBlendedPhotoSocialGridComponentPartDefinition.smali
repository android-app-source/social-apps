.class public Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxh;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsBlendedPhotoSocialModuleInterfaces$SearchResultsBlendedPhotoSocialModule;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1X6;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/EOX;

.field private final f:LX/1V0;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2114751
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EOX;LX/1V0;LX/0Or;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/EOX;",
            "LX/1V0;",
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2114752
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2114753
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->e:LX/EOX;

    .line 2114754
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->f:LX/1V0;

    .line 2114755
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->g:LX/0Or;

    .line 2114756
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsBlendedPhotoSocialModuleInterfaces$SearchResultsBlendedPhotoSocialModule;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2114757
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->e:LX/EOX;

    const/4 v1, 0x0

    .line 2114758
    new-instance v2, LX/EOW;

    invoke-direct {v2, v0}, LX/EOW;-><init>(LX/EOX;)V

    .line 2114759
    sget-object v3, LX/EOX;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EOV;

    .line 2114760
    if-nez v3, :cond_0

    .line 2114761
    new-instance v3, LX/EOV;

    invoke-direct {v3}, LX/EOV;-><init>()V

    .line 2114762
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EOV;->a$redex0(LX/EOV;LX/1De;IILX/EOW;)V

    .line 2114763
    move-object v2, v3

    .line 2114764
    move-object v1, v2

    .line 2114765
    move-object v0, v1

    .line 2114766
    const/4 v1, 0x3

    .line 2114767
    iget-object v2, v0, LX/EOV;->a:LX/EOW;

    iput v1, v2, LX/EOW;->c:I

    .line 2114768
    iget-object v2, v0, LX/EOV;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2114769
    move-object v0, v0

    .line 2114770
    iget-object v1, v0, LX/EOV;->a:LX/EOW;

    iput-object p2, v1, LX/EOW;->a:LX/CzL;

    .line 2114771
    iget-object v1, v0, LX/EOV;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2114772
    move-object v0, v0

    .line 2114773
    new-instance v1, LX/EOZ;

    invoke-direct {v1, p0, p2, p3}, LX/EOZ;-><init>(Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;LX/CzL;LX/1Pn;)V

    move-object v1, v1

    .line 2114774
    iget-object v2, v0, LX/EOV;->a:LX/EOW;

    iput-object v1, v2, LX/EOW;->b:LX/EOS;

    .line 2114775
    move-object v0, v0

    .line 2114776
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2114777
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    sget-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->d:LX/1X6;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;
    .locals 7

    .prologue
    .line 2114778
    const-class v1, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;

    monitor-enter v1

    .line 2114779
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114780
    sput-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114781
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114782
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114783
    new-instance v6, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EOX;->a(LX/0QB;)LX/EOX;

    move-result-object v4

    check-cast v4, LX/EOX;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    const/16 p0, 0xf2f

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;-><init>(Landroid/content/Context;LX/EOX;LX/1V0;LX/0Or;)V

    .line 2114784
    move-object v0, v6

    .line 2114785
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114786
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114787
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114788
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2114789
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2114790
    check-cast p2, LX/CzL;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoSocialGridComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2114791
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2114792
    const/4 v0, 0x0

    return-object v0
.end method
