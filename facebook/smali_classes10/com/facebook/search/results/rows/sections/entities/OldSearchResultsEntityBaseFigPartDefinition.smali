.class public Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/0ad;

.field private final c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/D0N;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2107707
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->EMAIL:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0ad;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;LX/D0N;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107700
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2107701
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->b:LX/0ad;

    .line 2107702
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    .line 2107703
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;

    .line 2107704
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    .line 2107705
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->f:LX/D0N;

    .line 2107706
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)LX/0jW;
    .locals 1

    .prologue
    .line 2107699
    new-instance v0, LX/EL5;

    invoke-direct {v0, p0}, LX/EL5;-><init>(Lcom/facebook/graphql/model/GraphQLNode;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;)LX/EL8;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F::",
            "LX/1Pn;",
            ":",
            "LX/1Pr;",
            ":",
            "LX/Cxe;",
            ":",
            "LX/Cxd;",
            ">(",
            "Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "TF;)",
            "LX/EL8;"
        }
    .end annotation

    .prologue
    .line 2107708
    sget-object v0, LX/EL6;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2107709
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unimplemented page call to action of type %s"

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2107710
    :pswitch_0
    const v0, 0x7f020956

    .line 2107711
    :goto_0
    new-instance v1, LX/EL8;

    const/4 v2, 0x0

    check-cast p2, LX/Cxd;

    invoke-static {p1, p2}, LX/ELj;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, LX/EL8;-><init>(IILandroid/view/View$OnClickListener;)V

    return-object v1

    .line 2107712
    :pswitch_1
    invoke-static {}, LX/10A;->a()I

    move-result v0

    goto :goto_0

    .line 2107713
    :pswitch_2
    const v0, 0x7f020850

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;LX/0ad;LX/D0N;)LX/EL8;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<F::",
            "LX/1Pn;",
            ":",
            "LX/1Pr;",
            ":",
            "LX/Cxe;",
            ":",
            "LX/Cxd;",
            ">(",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "TF;",
            "LX/0ad;",
            "LX/D0N;",
            ")",
            "LX/EL8;"
        }
    .end annotation

    .prologue
    .line 2107650
    invoke-static {p0}, LX/ELM;->d(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v0

    .line 2107651
    sparse-switch v0, :sswitch_data_0

    .line 2107652
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a:LX/EL8;

    :goto_0
    return-object v0

    .line 2107653
    :sswitch_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2107654
    invoke-virtual {p3, p0}, LX/D0N;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 2107655
    check-cast p1, LX/Cxd;

    invoke-static {p0, p1}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 2107656
    new-instance p2, LX/EL8;

    sget-object p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->a:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    sget-object p1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->b:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-direct {p2, p0, p1, v0}, LX/EL8;-><init>(IILandroid/view/View$OnClickListener;)V

    move-object v0, p2

    .line 2107657
    :goto_1
    move-object v0, v0

    .line 2107658
    goto :goto_0

    .line 2107659
    :sswitch_1
    const/4 v1, 0x0

    .line 2107660
    new-instance v2, LX/EJK;

    invoke-direct {v2, p0}, LX/EJK;-><init>(Lcom/facebook/graphql/model/GraphQLNode;)V

    .line 2107661
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gb()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a:LX/0Rf;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gb()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-short v0, LX/100;->bq:S

    invoke-interface {p2, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2107662
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->gb()Lcom/facebook/graphql/model/GraphQLPageCallToAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageCallToAction;->n()Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    invoke-static {v0, p0, p1}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;)LX/EL8;

    move-result-object v0

    .line 2107663
    :goto_2
    move-object v0, v0

    .line 2107664
    goto :goto_0

    .line 2107665
    :sswitch_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->bx()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v0

    .line 2107666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kK()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v1

    .line 2107667
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kW()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    .line 2107668
    check-cast p1, LX/Cxd;

    invoke-static {p0, p1}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)Landroid/view/View$OnClickListener;

    move-result-object v3

    .line 2107669
    invoke-static {v0, v1, v2, v3}, LX/ELK;->a(ZLcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Landroid/view/View$OnClickListener;)LX/EL8;

    move-result-object v0

    move-object v0, v0

    .line 2107670
    goto :goto_0

    .line 2107671
    :sswitch_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->kO()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    .line 2107672
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->mq()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    move-object v0, p1

    .line 2107673
    check-cast v0, LX/1Pr;

    new-instance v3, LX/EJJ;

    invoke-direct {v3, p0}, LX/EJJ;-><init>(Lcom/facebook/graphql/model/GraphQLNode;)V

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/0jW;

    move-result-object p2

    invoke-interface {v0, v3, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2107674
    check-cast p1, LX/Cxd;

    invoke-static {p0, p1}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Lcom/facebook/graphql/enums/GraphQLGroupCategory;Landroid/view/View$OnClickListener;)LX/EL8;

    move-result-object v0

    move-object v0, v0

    .line 2107675
    goto/16 :goto_0

    :cond_0
    check-cast p1, LX/Cxd;

    invoke-static {p0, p1}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)Landroid/view/View$OnClickListener;

    move-result-object p2

    invoke-static {v0, p2}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Landroid/view/View$OnClickListener;)LX/EL8;

    move-result-object v0

    goto/16 :goto_1

    .line 2107676
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cc()Z

    move-result p3

    move-object v0, p1

    .line 2107677
    check-cast v0, LX/1Pr;

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/0jW;

    move-result-object v3

    invoke-interface {v0, v2, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2107678
    if-eqz p3, :cond_2

    if-eqz v0, :cond_2

    move v0, v1

    .line 2107679
    :goto_3
    new-instance v2, LX/EL8;

    if-eqz p3, :cond_4

    const v1, 0x7f08226c

    move v3, v1

    :goto_4
    if-eqz p3, :cond_5

    const/4 v1, 0x0

    :goto_5
    invoke-direct {v2, v0, v3, v1}, LX/EL8;-><init>(IILandroid/view/View$OnClickListener;)V

    move-object v0, v2

    goto :goto_2

    .line 2107680
    :cond_2
    if-eqz p3, :cond_3

    const v0, 0x7f0207d6

    goto :goto_3

    :cond_3
    const v0, 0x7f0208fa

    goto :goto_3

    .line 2107681
    :cond_4
    const v1, 0x7f08226b

    move v3, v1

    goto :goto_4

    :cond_5
    check-cast p1, LX/Cxd;

    invoke-static {p0, p1}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)Landroid/view/View$OnClickListener;

    move-result-object v1

    goto :goto_5

    nop

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_2
        0x41e065f -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;LX/Cxd;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2107682
    new-instance v0, LX/EL4;

    invoke-direct {v0, p1, p0}, LX/EL4;-><init>(LX/Cxd;Lcom/facebook/graphql/model/GraphQLNode;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;
    .locals 9

    .prologue
    .line 2107683
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;

    monitor-enter v1

    .line 2107684
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107685
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107686
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107687
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107688
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    invoke-static {v0}, LX/D0N;->b(LX/0QB;)LX/D0N;

    move-result-object v8

    check-cast v8, LX/D0N;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;-><init>(LX/0ad;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;LX/D0N;)V

    .line 2107689
    move-object v0, v3

    .line 2107690
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107691
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107692
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107693
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2107694
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Pn;

    .line 2107695
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107696
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->b:LX/0ad;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->f:LX/D0N;

    invoke-static {p2, p3, v1, v2}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;LX/0ad;LX/D0N;)LX/EL8;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107697
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->e:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107698
    const/4 v0, 0x0

    return-object v0
.end method
