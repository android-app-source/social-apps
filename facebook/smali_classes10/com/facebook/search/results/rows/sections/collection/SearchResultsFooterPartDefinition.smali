.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<TT;>;>;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition",
            "<TT;",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104365
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104366
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    .line 2104367
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;->b:Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;

    .line 2104368
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;
    .locals 5

    .prologue
    .line 2104369
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    monitor-enter v1

    .line 2104370
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104371
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104372
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104373
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104374
    new-instance p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;)V

    .line 2104375
    move-object v0, p0

    .line 2104376
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104377
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104378
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104379
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2104380
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104381
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;->b:Lcom/facebook/search/results/rows/sections/collection/SearchResultsSeeMorePartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    .line 2104382
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2104383
    invoke-virtual {v0, v1, v2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2104384
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2104385
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104386
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104387
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->PROMOTED_ENTITY_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
