.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A2T;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/7j6;

.field public final d:LX/0hy;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:LX/CvY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2105155
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0xa7c5482

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/CvY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105133
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2105134
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2105135
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->c:LX/7j6;

    .line 2105136
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->d:LX/0hy;

    .line 2105137
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2105138
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->f:LX/CvY;

    .line 2105139
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;
    .locals 9

    .prologue
    .line 2105144
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    monitor-enter v1

    .line 2105145
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105146
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105147
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105148
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105149
    new-instance v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v5

    check-cast v5, LX/7j6;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v6

    check-cast v6, LX/0hy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v8

    check-cast v8, LX/CvY;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/CvY;)V

    .line 2105150
    move-object v0, v3

    .line 2105151
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105152
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105153
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105154
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static synthetic d(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;)LX/CvY;
    .locals 1

    .prologue
    .line 2105143
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->f:LX/CvY;

    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2105140
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 2105141
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EJl;

    invoke-direct {v1, p0, p2, p3}, LX/EJl;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemClickPartDefinition;LX/CzL;LX/1Pn;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2105142
    const/4 v0, 0x0

    return-object v0
.end method
