.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxy;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108528
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2108529
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2108530
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;

    .line 2108531
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2108532
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 2108533
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 2108534
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 2108535
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2108536
    sget-object v0, LX/3ap;->c:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2108537
    check-cast p2, LX/CzL;

    .line 2108538
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108539
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2108540
    iget-object v1, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 2108541
    invoke-static {v1}, LX/ELM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108542
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewContentDescriptionPartDefinition;

    .line 2108543
    iget-object v1, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 2108544
    invoke-static {v1}, LX/ELM;->b(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108545
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->f:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 2108546
    iget-object v1, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 2108547
    const/4 p2, 0x0

    .line 2108548
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object p3

    .line 2108549
    if-nez p3, :cond_1

    .line 2108550
    :cond_0
    :goto_0
    move-object v1, p2

    .line 2108551
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108552
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityLargeRowPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, LX/3ap;->b:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108553
    const/4 v0, 0x0

    return-object v0

    .line 2108554
    :cond_1
    invoke-virtual {p3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object p3

    .line 2108555
    if-eqz p3, :cond_0

    invoke-static {p3}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object p2

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2108556
    check-cast p1, LX/CzL;

    .line 2108557
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2108558
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
