.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/ELI;

.field private final d:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2108348
    const-class v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;

    const-string v1, "search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/ELI;LX/0ad;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/ELI;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2108349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2108350
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->b:LX/0Or;

    .line 2108351
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->c:LX/ELI;

    .line 2108352
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->d:LX/0ad;

    .line 2108353
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;
    .locals 6

    .prologue
    .line 2108354
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;

    monitor-enter v1

    .line 2108355
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108356
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2108357
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108358
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2108359
    new-instance v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;

    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/ELI;->a(LX/0QB;)LX/ELI;

    move-result-object v3

    check-cast v3, LX/ELI;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, p0, v3, v4}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;-><init>(LX/0Or;LX/ELI;LX/0ad;)V

    .line 2108360
    move-object v0, v5

    .line 2108361
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2108362
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2108363
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2108364
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dc;ZLjava/lang/CharSequence;Landroid/view/View$OnClickListener;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)LX/1Dg;
    .locals 8
    .param p2    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p6    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p7    # LX/1dc;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DRAWABLE:LX/32B;
        .end annotation
    .end param
    .param p8    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p10    # Landroid/view/View$OnClickListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p12    # Landroid/view/View$OnClickListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;Z",
            "Ljava/lang/CharSequence;",
            "Landroid/view/View$OnClickListener;",
            "Ljava/lang/CharSequence;",
            "Landroid/view/View$OnClickListener;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2108365
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->d:LX/0ad;

    sget-short v2, LX/100;->d:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    .line 2108366
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x2

    invoke-interface {v1, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x1

    invoke-interface {v1, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x6

    const v5, 0x7f0b0083

    invoke-interface {v1, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v4, 0x7

    const v5, 0x7f0b0082

    invoke-interface {v1, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const v4, 0x7f0a0097

    invoke-interface {v1, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/ELF;->d(LX/1De;)LX/1dQ;

    move-result-object v4

    invoke-interface {v1, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    if-nez p2, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v5, 0x0

    invoke-interface {v1, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v5, 0x2

    invoke-interface {v1, v5}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v1

    const/4 v5, 0x1

    invoke-interface {v1, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v1, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v5

    if-nez p4, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v5, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    if-nez p5, :cond_2

    const/4 v1, 0x0

    :goto_2
    invoke-interface {v5, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    if-nez p6, :cond_3

    const/4 v1, 0x0

    :goto_3
    invoke-interface {v5, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    if-eqz p8, :cond_4

    if-nez v2, :cond_4

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->c:LX/ELI;

    invoke-virtual {v1, p1}, LX/ELI;->c(LX/1De;)LX/ELG;

    move-result-object v1

    move-object/from16 v0, p9

    invoke-virtual {v1, v0}, LX/ELG;->a(Ljava/lang/CharSequence;)LX/ELG;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, LX/ELG;->a(Landroid/view/View$OnClickListener;)LX/ELG;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/ELG;->b(Ljava/lang/CharSequence;)LX/ELG;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/ELG;->b(Landroid/view/View$OnClickListener;)LX/ELG;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    :goto_4
    invoke-interface {v5, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v1

    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    if-nez p7, :cond_5

    const/4 v1, 0x0

    :goto_5
    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    if-eqz p8, :cond_6

    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->c:LX/ELI;

    invoke-virtual {v1, p1}, LX/ELI;->c(LX/1De;)LX/ELG;

    move-result-object v1

    move-object/from16 v0, p9

    invoke-virtual {v1, v0}, LX/ELG;->a(Ljava/lang/CharSequence;)LX/ELG;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, LX/ELG;->a(Landroid/view/View$OnClickListener;)LX/ELG;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/ELG;->b(Ljava/lang/CharSequence;)LX/ELG;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/ELG;->b(Landroid/view/View$OnClickListener;)LX/ELG;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    const v4, 0x7f0b0083

    invoke-interface {v1, v2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    :goto_6
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    :cond_0
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v5

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p2}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v6

    invoke-static {p3}, LX/1ny;->a(Ljava/lang/String;)LX/1ny;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1bX;->a(LX/1ny;)LX/1bX;

    move-result-object v6

    invoke-virtual {v6}, LX/1bX;->n()LX/1bf;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    sget-object v6, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v5, v1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v5, 0x7f010204

    invoke-interface {v1, v5}, LX/1Di;->h(I)LX/1Di;

    move-result-object v1

    const v5, 0x7f010204

    invoke-interface {v1, v5}, LX/1Di;->p(I)LX/1Di;

    move-result-object v1

    const/4 v5, 0x5

    const v6, 0x7f0b0083

    invoke-interface {v1, v5, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    goto/16 :goto_0

    :cond_1
    const/4 v1, 0x0

    const v6, 0x7f0e086d

    invoke-static {p1, v1, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    goto/16 :goto_1

    :cond_2
    const/4 v1, 0x0

    const v6, 0x7f0e0867

    invoke-static {p1, v1, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    goto/16 :goto_2

    :cond_3
    const/4 v1, 0x0

    const v6, 0x7f0e0867

    invoke-static {p1, v1, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v6, 0x1

    invoke-virtual {v1, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    goto/16 :goto_3

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_4

    :cond_5
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1, p7}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b171b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-interface {v1, v5}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b171c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    invoke-interface {v1, v5}, LX/1Di;->o(I)LX/1Di;

    move-result-object v1

    const/16 v5, 0x8

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b1719

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-interface {v1, v5, v6}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    invoke-static {p1}, LX/ELF;->e(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v1, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    goto/16 :goto_5

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_6
.end method
