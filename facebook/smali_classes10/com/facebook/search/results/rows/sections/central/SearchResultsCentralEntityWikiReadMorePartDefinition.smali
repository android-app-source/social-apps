.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104025
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2104026
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;->b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    .line 2104027
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2104028
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;
    .locals 5

    .prologue
    .line 2104014
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;

    monitor-enter v1

    .line 2104015
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104016
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104017
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104018
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104019
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2104020
    move-object v0, p0

    .line 2104021
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104022
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104023
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104024
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2104004
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2104008
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104009
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104010
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    .line 2104011
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->b:LX/1X6;

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104012
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMorePartDefinition;->b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104013
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104005
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104006
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104007
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
