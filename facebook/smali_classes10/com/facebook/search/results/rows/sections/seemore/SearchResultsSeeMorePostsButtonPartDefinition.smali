.class public Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/text/ImageWithTextView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/text/ImageWithTextView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/1Ua;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

.field private final e:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x40800000    # 4.0f

    .line 2116489
    const v0, 0x7f0312e6

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->a:LX/1Cz;

    .line 2116490
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    .line 2116491
    iput v2, v0, LX/1UY;->b:F

    .line 2116492
    move-object v0, v0

    .line 2116493
    const/high16 v1, 0x41000000    # 8.0f

    .line 2116494
    iput v1, v0, LX/1UY;->d:F

    .line 2116495
    move-object v0, v0

    .line 2116496
    iput v2, v0, LX/1UY;->c:F

    .line 2116497
    move-object v0, v0

    .line 2116498
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2116499
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2116500
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2116501
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->d:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    .line 2116502
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->e:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 2116503
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;
    .locals 6

    .prologue
    .line 2116504
    const-class v1, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    monitor-enter v1

    .line 2116505
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116506
    sput-object v2, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116507
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116508
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116509
    new-instance p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;-><init>(Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V

    .line 2116510
    move-object v0, p0

    .line 2116511
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116512
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116513
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116514
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/text/ImageWithTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2116515
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2116516
    check-cast p2, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    .line 2116517
    const v0, 0x7f0d032f

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2116518
    const v0, 0x7f0d032f

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->d:Lcom/facebook/multirow/parts/ImageWithTextViewDrawablePartDefinition;

    new-instance v2, LX/2eV;

    const v3, 0x7f0217dd

    const v4, -0xc4a668

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/2eV;-><init>(ILjava/lang/Integer;)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2116519
    const v0, 0x7f0d032f

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->e:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v2, LX/3aw;

    sget-object v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->b:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    const/4 v5, 0x0

    invoke-direct {v2, v6, v3, v4, v5}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2116520
    return-object v6
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116521
    check-cast p1, Ljava/lang/CharSequence;

    .line 2116522
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
