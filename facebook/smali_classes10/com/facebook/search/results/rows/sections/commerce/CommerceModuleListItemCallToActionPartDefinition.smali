.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A2T;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemMessengerCallToActionPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemMessengerCallToActionPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104962
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2104963
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemMessengerCallToActionPartDefinition;

    .line 2104964
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;

    .line 2104965
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;
    .locals 5

    .prologue
    .line 2104966
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;

    monitor-enter v1

    .line 2104967
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104968
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104969
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104970
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104971
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemMessengerCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemMessengerCallToActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemMessengerCallToActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemMessengerCallToActionPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;)V

    .line 2104972
    move-object v0, p0

    .line 2104973
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104974
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104975
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104976
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2104977
    check-cast p2, LX/CzL;

    .line 2104978
    invoke-static {p2}, LX/EJf;->a(LX/CzL;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104979
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemBuyNowCallToActionPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104980
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2104981
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemMessengerCallToActionPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method
