.class public Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/EKr;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106685
    const-class v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/EKr;LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EKr;",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106679
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2106680
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->b:LX/EKr;

    .line 2106681
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->c:LX/0Or;

    .line 2106682
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->d:LX/0Ot;

    .line 2106683
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->e:LX/0Ot;

    .line 2106684
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;
    .locals 7

    .prologue
    .line 2106668
    const-class v1, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;

    monitor-enter v1

    .line 2106669
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106670
    sput-object v2, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106671
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106672
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106673
    new-instance v4, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;

    invoke-static {v0}, LX/EKr;->a(LX/0QB;)LX/EKr;

    move-result-object v3

    check-cast v3, LX/EKr;

    const/16 v5, 0x509

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0xb19

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x2eb

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, v6, p0}, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;-><init>(LX/EKr;LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 2106674
    move-object v0, v4

    .line 2106675
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106676
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/elections/SearchResultsCandidateInfoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106677
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106678
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
