.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/String;",
        "LX/1Pn;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/154;


# direct methods
.method public constructor <init>(LX/154;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104322
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2104323
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;->a:LX/154;

    .line 2104324
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;
    .locals 4

    .prologue
    .line 2104325
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;

    monitor-enter v1

    .line 2104326
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104327
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104328
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104329
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104330
    new-instance p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v3

    check-cast v3, LX/154;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;-><init>(LX/154;)V

    .line 2104331
    move-object v0, p0

    .line 2104332
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104333
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104334
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104335
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2104336
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Pn;

    .line 2104337
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->H()Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAllShareStoriesConnection;->a()I

    move-result v0

    .line 2104338
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewMetaPartDefinition;->a:LX/154;

    invoke-virtual {v1, v0}, LX/154;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 2104339
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0109

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1faa69d9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2104340
    check-cast p2, Ljava/lang/String;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2104341
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2104342
    const/16 v1, 0x1f

    const v2, 0x36cbf622

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
