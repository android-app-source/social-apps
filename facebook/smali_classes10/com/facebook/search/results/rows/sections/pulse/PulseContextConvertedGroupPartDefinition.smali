.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        "E::",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/CxA;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/pulse/PulseContextModuleInterfaces$PulseContextModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition",
            "<TV;TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115263
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2115264
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;

    .line 2115265
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;

    .line 2115266
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    .line 2115267
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;

    .line 2115268
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    .line 2115269
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;
    .locals 9

    .prologue
    .line 2115270
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;

    monitor-enter v1

    .line 2115271
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115272
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115273
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115274
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115275
    new-instance v3, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;)V

    .line 2115276
    move-object v0, v3

    .line 2115277
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115278
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115279
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115280
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2115281
    check-cast p2, LX/CzL;

    .line 2115282
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2115283
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bI()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->n()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2115284
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoConvertedPartDefinition;

    invoke-virtual {p2, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115285
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextConvertedPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115286
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryConvertedPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115287
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115288
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextConvertedGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;

    invoke-virtual {p2, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115289
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115290
    const/4 v0, 0x1

    return v0
.end method
