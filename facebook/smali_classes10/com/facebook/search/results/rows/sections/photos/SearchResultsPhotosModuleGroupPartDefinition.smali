.class public Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/grammar/SearchResultsGrammarPhotosModuleInterfaces$SearchResultsGrammarPhotosModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector",
            "<",
            "Lcom/facebook/search/results/protocol/grammar/SearchResultsGrammarPhotosModuleInterfaces$SearchResultsGrammarPhotosModule;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115144
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2115145
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    .line 2115146
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    .line 2115147
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2115148
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;
    .locals 6

    .prologue
    .line 2115149
    const-class v1, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;

    monitor-enter v1

    .line 2115150
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115151
    sput-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115152
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115153
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115154
    new-instance p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V

    .line 2115155
    move-object v0, p0

    .line 2115156
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115157
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115158
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2115160
    check-cast p2, LX/CzL;

    .line 2115161
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115162
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/collection/SearchResultsMediaGridPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115163
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotosModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115164
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115165
    check-cast p1, LX/CzL;

    .line 2115166
    invoke-virtual {p1}, LX/CzL;->k()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
