.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/CxA;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxU;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/entities/SearchResultsPageModuleInterfaces$SearchResultsPageModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector",
            "<",
            "Lcom/facebook/search/results/protocol/entities/SearchResultsPageModuleInterfaces$SearchResultsPageModule;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/0ad;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;LX/0ad;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109530
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2109531
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    .line 2109532
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    .line 2109533
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    .line 2109534
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;

    .line 2109535
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2109536
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->f:LX/0ad;

    .line 2109537
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->g:LX/0Ot;

    .line 2109538
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;
    .locals 11

    .prologue
    .line 2109500
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;

    monitor-enter v1

    .line 2109501
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109502
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109503
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109504
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109505
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    const/16 v10, 0x2be

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;LX/0ad;LX/0Ot;)V

    .line 2109506
    move-object v0, v3

    .line 2109507
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109508
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109509
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109510
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2109512
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxi;

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2109513
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2109514
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->f:LX/0ad;

    sget-short v1, LX/100;->e:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v4

    move-object v0, p3

    .line 2109515
    check-cast v0, LX/CxP;

    invoke-interface {v0, p2}, LX/CxP;->b(LX/CzL;)I

    move-result v1

    move-object v0, p3

    .line 2109516
    check-cast v0, LX/CxU;

    invoke-interface {v0}, LX/CxU;->s()I

    move-result v0

    .line 2109517
    if-ltz v1, :cond_0

    add-int/lit8 v5, v1, 0x1

    if-ge v5, v0, :cond_0

    move-object v0, p3

    check-cast v0, LX/CxU;

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, LX/CxU;->a(I)LX/Cyv;

    move-result-object v0

    .line 2109518
    :goto_0
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/Cz3;

    if-eqz v1, :cond_1

    check-cast v0, LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4c808d5

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 2109519
    :goto_1
    or-int/2addr v4, v1

    .line 2109520
    check-cast p3, LX/CxV;

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {p2, p3, v0}, LX/EJ4;->a(LX/CzL;LX/CxV;LX/0W9;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->f:LX/0ad;

    sget-short v5, LX/100;->f:S

    invoke-interface {v0, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2109521
    if-eqz v4, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    :goto_2
    invoke-static {p1, v0, p2}, LX/EJ4;->b(LX/1RF;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/CzL;)V

    .line 2109522
    :goto_3
    if-eqz v1, :cond_5

    .line 2109523
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    invoke-virtual {p1, v0, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2109524
    :goto_4
    return-object v3

    :cond_0
    move-object v0, v3

    .line 2109525
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2109526
    goto :goto_1

    .line 2109527
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;

    goto :goto_2

    .line 2109528
    :cond_3
    if-eqz v4, :cond_4

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    :goto_5
    invoke-static {p1, v0, p2}, LX/EJ4;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/CzL;)V

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;

    goto :goto_5

    .line 2109529
    :cond_5
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2109511
    const/4 v0, 0x1

    return v0
.end method
