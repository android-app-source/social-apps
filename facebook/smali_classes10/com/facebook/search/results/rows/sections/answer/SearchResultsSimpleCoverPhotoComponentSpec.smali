.class public Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static c:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2103351
    const-class v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2103364
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;->b:LX/0Or;

    .line 2103365
    return-void
.end method

.method public static a(LX/8cw;)Landroid/net/Uri;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2103366
    invoke-interface {p0}, LX/8cw;->b()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v1

    .line 2103367
    if-nez v1, :cond_1

    .line 2103368
    :cond_0
    :goto_0
    return-object v0

    .line 2103369
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;->a()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel$PhotoModel;

    move-result-object v1

    .line 2103370
    if-eqz v1, :cond_0

    .line 2103371
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel$PhotoModel;->a()LX/1Fb;

    move-result-object v1

    .line 2103372
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;
    .locals 4

    .prologue
    .line 2103352
    const-class v1, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;

    monitor-enter v1

    .line 2103353
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103354
    sput-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103355
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103356
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103357
    new-instance v3, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;-><init>(LX/0Or;)V

    .line 2103358
    move-object v0, v3

    .line 2103359
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103360
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103361
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103362
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
