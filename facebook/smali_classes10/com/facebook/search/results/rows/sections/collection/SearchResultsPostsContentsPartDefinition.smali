.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "LX/EJb;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:LX/1nD;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2104650
    sget-object v0, LX/3ab;->c:LX/1Cz;

    sput-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104641
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2104642
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2104643
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2104644
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->d:LX/1nD;

    .line 2104645
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2104646
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->f:LX/0wM;

    .line 2104647
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;
    .locals 9

    .prologue
    .line 2104651
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

    monitor-enter v1

    .line 2104652
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104653
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104654
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104655
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104656
    new-instance v3, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v6

    check-cast v6, LX/1nD;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v8

    check-cast v8, LX/0wM;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/0wM;)V

    .line 2104657
    move-object v0, v3

    .line 2104658
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104659
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104660
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104661
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Z
    .locals 1

    .prologue
    .line 2104648
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hw()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hw()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->hu()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2104649
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2104606
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/Cxe;

    const v5, -0xc4a668

    .line 2104607
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    const/4 v2, 0x0

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v1, v2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104608
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EJa;

    invoke-direct {v1, p0, p2, p3}, LX/EJa;-><init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/Cxe;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104609
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2104610
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b16df

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2104611
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->MY_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->hv()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2104612
    const v1, 0x7f082279    # 1.80954E38f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2104613
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->f:LX/0wM;

    const v3, 0x7f02143c

    invoke-virtual {v0, v3, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2104614
    :goto_0
    new-instance v3, LX/EJb;

    invoke-direct {v3, v0, v1, v2}, LX/EJb;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;I)V

    return-object v3

    .line 2104615
    :cond_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->hv()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2104616
    const v1, 0x7f08227a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2104617
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->f:LX/0wM;

    const v3, 0x7f02142b

    invoke-virtual {v0, v3, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2104618
    :cond_1
    const v1, 0x7f08227b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2104619
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->f:LX/0wM;

    const v3, 0x7f020c03

    invoke-virtual {v0, v3, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x153fef8c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2104620
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p2, LX/EJb;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2104621
    const v1, 0x7f0e01ec

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2104622
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hw()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2104623
    const v1, 0x7f0e0200

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 2104624
    iget-object v1, p2, LX/EJb;->b:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2104625
    iget-object v1, p2, LX/EJb;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2104626
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2104627
    iget v1, p2, LX/EJb;->c:I

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 2104628
    const/16 v1, 0x10

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 2104629
    const/16 v1, 0x1f

    const v2, 0x59b2e131

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104630
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2104631
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    const/4 v1, 0x0

    .line 2104632
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2104633
    const-string v0, ""

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2104634
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleTextAppearance(I)V

    .line 2104635
    const-string v0, ""

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2104636
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2104637
    const/4 v0, 0x1

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMaxLinesFromThumbnailSize(Z)V

    .line 2104638
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 2104639
    const/16 v0, 0x30

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 2104640
    return-void
.end method
