.class public Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<*>;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115203
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2115204
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;

    .line 2115205
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;
    .locals 4

    .prologue
    .line 2115206
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;

    monitor-enter v1

    .line 2115207
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115208
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115209
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115210
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115211
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;)V

    .line 2115212
    move-object v0, p0

    .line 2115213
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115214
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115215
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115216
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2115217
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2115218
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2115219
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 2115220
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhraseItemPartDefinition;

    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115221
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2115222
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115223
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v0

    return v0
.end method
