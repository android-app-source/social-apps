.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2105913
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2105914
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->a()V

    .line 2105915
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2105916
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2105917
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->a()V

    .line 2105918
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2105919
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2105920
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->a()V

    .line 2105921
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2105922
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->a:Landroid/graphics/Paint;

    .line 2105923
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2105924
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b16c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2105925
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2105926
    invoke-super {p0, p1}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2105927
    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 2105928
    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2105929
    return-void
.end method
