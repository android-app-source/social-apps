.class public Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition",
            "<TE;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextIconPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2118009
    const v0, 0x7f030929

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;Lcom/facebook/multirow/parts/TextIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2118002
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2118003
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    .line 2118004
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    .line 2118005
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->d:Lcom/facebook/multirow/parts/TextIconPartDefinition;

    .line 2118006
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2118007
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->f:Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    .line 2118008
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;
    .locals 9

    .prologue
    .line 2117968
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;

    monitor-enter v1

    .line 2117969
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117970
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117971
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117972
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117973
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextIconPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextIconPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/TextIconPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;Lcom/facebook/multirow/parts/TextIconPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;)V

    .line 2117974
    move-object v0, v3

    .line 2117975
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117976
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117977
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117978
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2118001
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2117980
    check-cast p2, LX/CzL;

    const/4 v6, 0x0

    .line 2117981
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117982
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2117983
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 2117984
    const v2, 0x7f0d02c4

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->f:Lcom/facebook/multirow/parts/TextWithImageEntitiesOrHiddenPartDefinition;

    new-instance v4, LX/8Cv;

    .line 2117985
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v5

    .line 2117986
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-direct {v4, v1}, LX/8Cv;-><init>(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2117987
    const v1, 0x7f0d0a81

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    .line 2117988
    iget v3, p2, LX/CzL;->b:I

    move v4, v3

    .line 2117989
    iget-object v3, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v3, v3

    .line 2117990
    invoke-static {v3}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v3

    .line 2117991
    invoke-virtual {p2, v3, v4}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v3

    move-object v3, v3

    .line 2117992
    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2117993
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2117994
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 2117995
    if-nez v0, :cond_0

    .line 2117996
    :goto_0
    return-object v6

    .line 2117997
    :cond_0
    const v1, 0x7f0d02c3

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2117998
    const v1, 0x7f0d02c3

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->d:Lcom/facebook/multirow/parts/TextIconPartDefinition;

    new-instance v3, LX/8Ct;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f021a25

    :goto_1
    const v4, 0x7f0b16b6

    sget-object v5, LX/8Cs;->END:LX/8Cs;

    invoke-direct {v3, v0, v4, v5}, LX/8Ct;-><init>(IILX/8Cs;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2117999
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsInlineVideoInfoPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0

    .line 2118000
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2117979
    const/4 v0, 0x1

    return v0
.end method
