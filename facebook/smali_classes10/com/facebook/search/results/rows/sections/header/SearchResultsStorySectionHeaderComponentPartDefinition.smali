.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/CharSequence;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/EMp;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/EMp;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2111453
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2111454
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;->d:LX/EMp;

    .line 2111455
    return-void
.end method

.method private a(LX/1De;Ljava/lang/CharSequence;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/CharSequence;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2111439
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;->d:LX/EMp;

    const/4 v1, 0x0

    .line 2111440
    new-instance v2, LX/EMo;

    invoke-direct {v2, v0}, LX/EMo;-><init>(LX/EMp;)V

    .line 2111441
    sget-object p0, LX/EMp;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EMn;

    .line 2111442
    if-nez p0, :cond_0

    .line 2111443
    new-instance p0, LX/EMn;

    invoke-direct {p0}, LX/EMn;-><init>()V

    .line 2111444
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/EMn;->a$redex0(LX/EMn;LX/1De;IILX/EMo;)V

    .line 2111445
    move-object v2, p0

    .line 2111446
    move-object v1, v2

    .line 2111447
    move-object v0, v1

    .line 2111448
    iget-object v1, v0, LX/EMn;->a:LX/EMo;

    iput-object p2, v1, LX/EMo;->a:Ljava/lang/CharSequence;

    .line 2111449
    iget-object v1, v0, LX/EMn;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2111450
    move-object v0, v0

    .line 2111451
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2111452
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;
    .locals 5

    .prologue
    .line 2111428
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2111429
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111430
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111431
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111432
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111433
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EMp;->a(LX/0QB;)LX/EMp;

    move-result-object v4

    check-cast v4, LX/EMp;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/EMp;)V

    .line 2111434
    move-object v0, p0

    .line 2111435
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111436
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111437
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111438
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2111427
    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;->a(LX/1De;Ljava/lang/CharSequence;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2111426
    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;->a(LX/1De;Ljava/lang/CharSequence;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111424
    check-cast p1, Ljava/lang/CharSequence;

    .line 2111425
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2111423
    const/4 v0, 0x0

    return-object v0
.end method
