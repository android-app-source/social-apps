.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxj;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/video/SearchResultsVideosModuleInterfaces$SearchResultsVideosModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117874
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2117875
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;->a:LX/0Ot;

    .line 2117876
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;
    .locals 4

    .prologue
    .line 2117877
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    monitor-enter v1

    .line 2117878
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117879
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117880
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117881
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117882
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    const/16 p0, 0x34ac

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;-><init>(LX/0Ot;)V

    .line 2117883
    move-object v0, v3

    .line 2117884
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117885
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117886
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2117888
    check-cast p2, LX/CzL;

    const/4 v1, 0x0

    .line 2117889
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117890
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bM()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    .line 2117891
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2117892
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->t()LX/A4u;

    move-result-object v5

    .line 2117893
    if-eqz v5, :cond_1

    .line 2117894
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p2, v5, v1}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v5

    invoke-virtual {p1, v0, v5}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117895
    add-int/lit8 v0, v1, 0x1

    .line 2117896
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2117897
    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2117898
    check-cast p1, LX/CzL;

    .line 2117899
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
