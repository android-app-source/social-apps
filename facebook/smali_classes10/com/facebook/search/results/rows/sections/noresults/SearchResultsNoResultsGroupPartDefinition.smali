.class public Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113415
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2113416
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    .line 2113417
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;

    .line 2113418
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    .line 2113419
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;
    .locals 6

    .prologue
    .line 2113420
    const-class v1, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;

    monitor-enter v1

    .line 2113421
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113422
    sput-object v2, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113423
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113424
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113425
    new-instance p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;)V

    .line 2113426
    move-object v0, p0

    .line 2113427
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113428
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113429
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113430
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2113431
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2113432
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    .line 2113433
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2113434
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113435
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsContentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113436
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsNoResultsGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    .line 2113437
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2113438
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113439
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2113414
    const/4 v0, 0x1

    return v0
.end method
