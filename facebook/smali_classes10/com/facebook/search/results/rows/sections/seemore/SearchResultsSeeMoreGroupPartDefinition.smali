.class public Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsDividerPartDefinition",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsDividerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116332
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2116333
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;->a:LX/0Ot;

    .line 2116334
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;->b:LX/0Ot;

    .line 2116335
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;->c:LX/0Ot;

    .line 2116336
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;
    .locals 6

    .prologue
    .line 2116337
    const-class v1, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;

    monitor-enter v1

    .line 2116338
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116339
    sput-object v2, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116340
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116341
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116342
    new-instance v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;

    const/16 v4, 0x11a0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x119f

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x3487

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2116343
    move-object v0, v3

    .line 2116344
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116345
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116346
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116347
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2116348
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/Cxo;

    .line 2116349
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2116350
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2116351
    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2116352
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2116353
    :cond_0
    :goto_0
    move v0, v1

    .line 2116354
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;->c:LX/0Ot;

    .line 2116355
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2116356
    invoke-static {p1, v0, v1, v2}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;->b:LX/0Ot;

    .line 2116357
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2116358
    invoke-virtual {v1, v2, v3}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2116359
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreGroupPartDefinition;->a:LX/0Ot;

    .line 2116360
    iget-object v2, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 2116361
    invoke-virtual {p1, v0, v1, v2}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 2116362
    const/4 v0, 0x0

    return-object v0

    .line 2116363
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2116364
    :cond_2
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    sget-object v4, LX/1Qt;->SEARCH_DENSE_FEED:LX/1Qt;

    if-ne v3, v4, :cond_4

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v3, v4, :cond_4

    move v3, v2

    .line 2116365
    :goto_2
    if-nez v3, :cond_3

    .line 2116366
    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    move-object v3, v3

    .line 2116367
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    move v3, v1

    .line 2116368
    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116369
    const/4 v0, 0x1

    return v0
.end method
