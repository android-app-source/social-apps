.class public Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxk;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsRelatedSharesModuleInterfaces$SearchResultsRelatedSharesModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

.field private final b:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;LX/0Ot;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;",
            "Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116154
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2116155
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    .line 2116156
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;

    .line 2116157
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->c:LX/0Ot;

    .line 2116158
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2116159
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;
    .locals 7

    .prologue
    .line 2116160
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;

    monitor-enter v1

    .line 2116161
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116162
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116163
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116164
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116165
    new-instance v6, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;

    const/16 v5, 0x3481

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-direct {v6, v3, v4, p0, v5}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;LX/0Ot;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V

    .line 2116166
    move-object v0, v6

    .line 2116167
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116168
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116169
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116170
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2116171
    check-cast p2, LX/CzL;

    .line 2116172
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2116173
    const/4 v0, 0x0

    move v1, v0

    .line 2116174
    :goto_0
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116175
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bU()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2116176
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116177
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bU()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2116178
    invoke-virtual {p2, v0, v1}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v0

    .line 2116179
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->c:LX/0Ot;

    .line 2116180
    move-object v2, v2

    .line 2116181
    invoke-static {p1, v2, v0}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesPartDefinition;

    invoke-virtual {v2, v3, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2116182
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2116183
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2116184
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116185
    check-cast p1, LX/CzL;

    .line 2116186
    invoke-virtual {p1}, LX/CzL;->k()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
