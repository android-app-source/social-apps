.class public Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxe;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/1nG;

.field public final b:LX/17W;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2SY;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EQ6;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final f:LX/0ad;


# direct methods
.method public constructor <init>(LX/1nG;LX/17W;LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nG;",
            "LX/17W;",
            "LX/0Ot",
            "<",
            "LX/2SY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EQ6;",
            ">;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104238
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2104239
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->a:LX/1nG;

    .line 2104240
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->b:LX/17W;

    .line 2104241
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->c:LX/0Ot;

    .line 2104242
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->d:LX/0Ot;

    .line 2104243
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2104244
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->f:LX/0ad;

    .line 2104245
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;
    .locals 10

    .prologue
    .line 2104246
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    monitor-enter v1

    .line 2104247
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104248
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104249
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104250
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104251
    new-instance v3, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v4

    check-cast v4, LX/1nG;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    const/16 v6, 0x11b8

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x34d5

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;-><init>(LX/1nG;LX/17W;LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0ad;)V

    .line 2104252
    move-object v0, v3

    .line 2104253
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104254
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104255
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104256
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2104257
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/Cxe;

    .line 2104258
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EJV;

    invoke-direct {v1, p0, p2, p3}, LX/EJV;-><init>(Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/Cxe;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104259
    const/4 v0, 0x0

    return-object v0
.end method
