.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/EN9;",
        "LX/CxV;",
        "LX/ENE;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static k:LX/0Xm;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/17W;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/ENB;

.field private final f:LX/8i7;

.field public final g:LX/1nM;

.field private final h:LX/11R;

.field public final i:LX/1xc;

.field public final j:LX/1nG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2112095
    const-class v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    const-string v1, "graph_search_results_live_conversation_fragment"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/ENB;LX/8i7;LX/1nM;LX/11R;LX/1xc;LX/1nG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112136
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2112137
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    .line 2112138
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->c:LX/17W;

    .line 2112139
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2112140
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->e:LX/ENB;

    .line 2112141
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->f:LX/8i7;

    .line 2112142
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->g:LX/1nM;

    .line 2112143
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->h:LX/11R;

    .line 2112144
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->i:LX/1xc;

    .line 2112145
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->j:LX/1nG;

    .line 2112146
    return-void
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/CxV;)LX/EN9;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/CxV;",
            ")",
            "LX/EN9;"
        }
    .end annotation

    .prologue
    .line 2112096
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2112097
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v4, 0x7f082289

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2112098
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v4, 0x7f08228a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2112099
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v4, 0x7f08228b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2112100
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->e:LX/ENB;

    invoke-interface/range {p2 .. p2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, LX/ENB;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/search/results/model/SearchResultsMutableContext;)LX/ENA;

    move-result-object v9

    .line 2112101
    invoke-static {v2}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v10

    .line 2112102
    new-instance v11, LX/EN5;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v2, v10}, LX/EN5;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLActor;)V

    .line 2112103
    if-nez v10, :cond_3

    const/4 v3, 0x0

    .line 2112104
    :goto_0
    invoke-interface/range {p2 .. p2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LATEST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v4, v5, :cond_4

    const/4 v4, 0x1

    .line 2112105
    :goto_1
    invoke-static {v2}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    const/4 v5, 0x1

    .line 2112106
    :goto_2
    if-nez v4, :cond_6

    invoke-static {v2}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    .line 2112107
    :goto_3
    new-instance v12, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    invoke-static {v0, v10, v2, v4}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;Z)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-direct {v12, v13}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2112108
    if-eqz v10, :cond_0

    invoke-virtual {v12}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v13

    if-lez v13, :cond_0

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 2112109
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->f:LX/8i7;

    new-instance v14, Landroid/text/SpannableStringBuilder;

    invoke-direct {v14, v12}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v13, v14}, LX/8i7;->b(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    .line 2112110
    :cond_0
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Landroid/view/View$OnClickListener;)Landroid/text/style/ClickableSpan;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v12}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v15

    const/16 v16, 0x21

    invoke-virtual/range {v12 .. v16}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2112111
    if-nez v5, :cond_7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    if-eqz v5, :cond_7

    .line 2112112
    new-instance v4, Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v13, 0x7f08227c

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v10}, LX/1xl;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v14, v15

    invoke-virtual {v5, v13, v14}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2112113
    move-object/from16 v0, p0

    invoke-static {v0, v9}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Landroid/view/View$OnClickListener;)Landroid/text/style/ClickableSpan;

    move-result-object v5

    const/4 v10, 0x0

    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v13

    const/16 v14, 0x12

    invoke-interface {v4, v5, v10, v13, v14}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2112114
    :goto_4
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2112115
    invoke-virtual {v5, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112116
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 2112117
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v10, v14, v16

    if-nez v10, :cond_8

    const-string v2, ""

    .line 2112118
    :goto_5
    invoke-virtual {v5, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112119
    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v10

    if-lez v10, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 2112120
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112121
    invoke-virtual {v5, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112122
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112123
    :cond_1
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    .line 2112124
    sub-int v6, v2, v7

    if-lez v6, :cond_2

    .line 2112125
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v10, 0x7f0a00a3

    invoke-static {v8, v10}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v8

    invoke-direct {v6, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2112126
    const/16 v8, 0x21

    invoke-virtual {v5, v6, v7, v2, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2112127
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/16 v8, 0x21

    invoke-virtual {v5, v6, v7, v2, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2112128
    :cond_2
    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112129
    new-instance v2, LX/EN9;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v6, 0x0

    aput-object v12, v4, v6

    const/4 v6, 0x1

    aput-object v5, v4, v6

    invoke-static {v4}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-direct {v2, v3, v9, v11, v4}, LX/EN9;-><init>(Landroid/net/Uri;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Ljava/lang/CharSequence;)V

    return-object v2

    .line 2112130
    :cond_3
    invoke-static {v10}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    goto/16 :goto_0

    .line 2112131
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 2112132
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2112133
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 2112134
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v9}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;ZLandroid/view/View$OnClickListener;)Landroid/text/Spannable;

    move-result-object v4

    goto/16 :goto_4

    .line 2112135
    :cond_8
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->h:LX/11R;

    sget-object v13, LX/1lB;->FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    mul-long v14, v14, v16

    invoke-virtual {v10, v13, v14, v15}, LX/11R;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;ZLandroid/view/View$OnClickListener;)Landroid/text/Spannable;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z",
            "Landroid/view/View$OnClickListener;",
            ")",
            "Landroid/text/Spannable;"
        }
    .end annotation

    .prologue
    .line 2112014
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2112015
    invoke-static {p0, p3}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Landroid/view/View$OnClickListener;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x12

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2112016
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->g:LX/1nM;

    invoke-virtual {v0, p1}, LX/1nM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;

    move-result-object v0

    .line 2112017
    if-eqz v0, :cond_a

    :goto_0
    move-object v5, v0

    .line 2112018
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v0

    const/16 v1, 0xa0

    if-le v0, v1, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 2112019
    :goto_1
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2112020
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    const/16 v8, 0xa0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2112021
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v2

    if-le v2, v8, :cond_b

    move v3, v6

    .line 2112022
    :goto_2
    if-eqz v3, :cond_c

    invoke-static {v5, v7, v8}, LX/33Q;->a(Ljava/lang/CharSequence;II)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2112023
    :goto_3
    if-eqz p2, :cond_0

    .line 2112024
    iget-object v8, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->i:LX/1xc;

    invoke-virtual {v8, v0, v2}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2112025
    :cond_0
    if-eqz v3, :cond_1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    aput-object v2, v3, v7

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v7, 0x7f080023

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v6

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    :cond_1
    move-object v0, v2

    .line 2112026
    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112027
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 2112028
    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v2

    const/16 v3, 0xa0

    if-le v2, v3, :cond_9

    .line 2112029
    add-int/lit8 v0, v0, -0x1

    move v2, v0

    .line 2112030
    :goto_4
    const/4 v0, 0x0

    const-class v3, Landroid/text/style/CharacterStyle;

    invoke-interface {v5, v0, v2, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/CharacterStyle;

    array-length v6, v0

    const/4 v3, 0x0

    :goto_5
    if-ge v3, v6, :cond_3

    aget-object v7, v0, v3

    .line 2112031
    invoke-interface {v5, v7}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v8

    invoke-interface {v5, v7}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v9

    invoke-static {v9, v2}, Ljava/lang/Math;->min(II)I

    move-result v9

    const/16 v10, 0x21

    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2112032
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 2112033
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    .line 2112034
    :cond_3
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2112035
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2112036
    if-nez v1, :cond_4

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x3c

    if-le v1, v2, :cond_8

    :cond_4
    const/4 v1, 0x1

    .line 2112037
    :goto_6
    const/16 v7, 0x3c

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2112038
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 2112039
    if-nez v2, :cond_11

    .line 2112040
    const-string v3, ""

    .line 2112041
    :goto_7
    move-object v2, v3

    .line 2112042
    if-nez v2, :cond_d

    .line 2112043
    const-string v2, ""

    .line 2112044
    :cond_5
    :goto_8
    move-object v2, v2

    .line 2112045
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v5, 0x7f082289

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2112046
    if-eqz v2, :cond_6

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_6

    .line 2112047
    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112048
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    .line 2112049
    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112050
    new-instance v6, LX/EN8;

    invoke-direct {v6, p0, v0}, LX/EN8;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v0, v6

    .line 2112051
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/2addr v2, v5

    const/16 v6, 0x22

    invoke-virtual {v4, v0, v5, v2, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2112052
    :cond_6
    if-eqz v1, :cond_7

    .line 2112053
    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112054
    new-instance v0, Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v2, 0x7f08228d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2112055
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v3, 0x7f0a00a3

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2112056
    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v5, 0x21

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2112057
    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2112058
    :cond_7
    return-object v4

    .line 2112059
    :cond_8
    const/4 v1, 0x0

    goto :goto_6

    :cond_9
    move v2, v0

    goto/16 :goto_4

    :cond_a
    new-instance v0, Landroid/text/SpannableString;

    const-string v1, ""

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_b
    move v3, v7

    .line 2112060
    goto/16 :goto_2

    :cond_c
    move-object v2, v5

    .line 2112061
    goto/16 :goto_3

    .line 2112062
    :cond_d
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v7, :cond_10

    move v3, v5

    .line 2112063
    :goto_9
    if-eqz v3, :cond_e

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 2112064
    :cond_e
    if-eqz p2, :cond_f

    .line 2112065
    iget-object v7, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->i:LX/1xc;

    invoke-virtual {v7, v0, v2}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2112066
    :cond_f
    if-eqz v3, :cond_5

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    aput-object v2, v3, v6

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->b:Landroid/content/Context;

    const v6, 0x7f080023

    invoke-virtual {v2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v5

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_8

    :cond_10
    move v3, v6

    .line 2112067
    goto :goto_9

    :cond_11
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v8, "^https?://www\\.|^https?://|^www\\."

    const-string v9, ""

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_7
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Landroid/view/View$OnClickListener;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 2112094
    new-instance v0, LX/EN6;

    invoke-direct {v0, p0, p1}, LX/EN6;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;
    .locals 13

    .prologue
    .line 2112083
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    monitor-enter v1

    .line 2112084
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112085
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112086
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112087
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112088
    new-instance v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    const-class v7, LX/ENB;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/ENB;

    invoke-static {v0}, LX/8i7;->a(LX/0QB;)LX/8i7;

    move-result-object v8

    check-cast v8, LX/8i7;

    invoke-static {v0}, LX/1nM;->a(LX/0QB;)LX/1nM;

    move-result-object v9

    check-cast v9, LX/1nM;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v10

    check-cast v10, LX/11R;

    invoke-static {v0}, LX/1xc;->a(LX/0QB;)LX/1xc;

    move-result-object v11

    check-cast v11, LX/1xc;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v12

    check-cast v12, LX/1nG;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;-><init>(Landroid/content/Context;LX/17W;Lcom/facebook/content/SecureContextHelper;LX/ENB;LX/8i7;LX/1nM;LX/11R;LX/1xc;LX/1nG;)V

    .line 2112089
    move-object v0, v3

    .line 2112090
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112091
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112092
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112093
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;Z)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2112147
    if-nez p1, :cond_1

    .line 2112148
    const-string v0, ""

    .line 2112149
    :cond_0
    :goto_0
    return-object v0

    .line 2112150
    :cond_1
    invoke-static {p1}, LX/1xl;->a(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    .line 2112151
    if-eqz p3, :cond_0

    .line 2112152
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->i:LX/1xc;

    invoke-virtual {v1, p2, v0}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2112082
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->m()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static b(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Landroid/view/View$OnClickListener;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 2112081
    new-instance v0, LX/EN7;

    invoke-direct {v0, p0, p1}, LX/EN7;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2112080
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/CxV;

    invoke-direct {p0, p2, p3}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/CxV;)LX/EN9;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2dba4880

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2112073
    check-cast p2, LX/EN9;

    check-cast p4, LX/ENE;

    .line 2112074
    iget-object v1, p2, LX/EN9;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2112075
    iget-object p0, p4, LX/ENE;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2112076
    iget-object v1, p2, LX/EN9;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/ENE;->setBodyClickListener(Landroid/view/View$OnClickListener;)V

    .line 2112077
    iget-object v1, p2, LX/EN9;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, LX/ENE;->setActorClickListener(Landroid/view/View$OnClickListener;)V

    .line 2112078
    iget-object v1, p2, LX/EN9;->d:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, LX/ENE;->setPostBodyText(Ljava/lang/CharSequence;)V

    .line 2112079
    const/16 v1, 0x1f

    const v2, -0x7630f56e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2112068
    check-cast p4, LX/ENE;

    const/4 v0, 0x0

    .line 2112069
    invoke-virtual {p4, v0}, LX/ENE;->setBodyClickListener(Landroid/view/View$OnClickListener;)V

    .line 2112070
    invoke-virtual {p4, v0}, LX/ENE;->setActorClickListener(Landroid/view/View$OnClickListener;)V

    .line 2112071
    invoke-virtual {p4, v0}, LX/ENE;->setPostBodyText(Ljava/lang/CharSequence;)V

    .line 2112072
    return-void
.end method
