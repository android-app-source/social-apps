.class public Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0sX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2115127
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sput-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2115128
    const-class v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0sX;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0sX;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2115129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2115130
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->c:LX/0Or;

    .line 2115131
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->d:LX/0sX;

    .line 2115132
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;
    .locals 5

    .prologue
    .line 2115133
    const-class v1, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;

    monitor-enter v1

    .line 2115134
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115135
    sput-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115136
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115137
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115138
    new-instance v4, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;

    const/16 v3, 0x509

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v3

    check-cast v3, LX/0sX;

    invoke-direct {v4, p0, v3}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;-><init>(LX/0Or;LX/0sX;)V

    .line 2115139
    move-object v0, v4

    .line 2115140
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115141
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsPhotoWithCaptionComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115142
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115143
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
