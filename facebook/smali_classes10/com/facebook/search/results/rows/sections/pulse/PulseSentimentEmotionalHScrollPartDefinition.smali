.class public Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;

.field private final b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final d:LX/2dq;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115719
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2115720
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;

    .line 2115721
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2115722
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2115723
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->d:LX/2dq;

    .line 2115724
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;
    .locals 7

    .prologue
    .line 2115725
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

    monitor-enter v1

    .line 2115726
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115727
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115728
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115729
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115730
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v6

    check-cast v6, LX/2dq;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalItemPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;)V

    .line 2115731
    move-object v0, p0

    .line 2115732
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115733
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115734
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115735
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2115736
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2115737
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->o()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2115738
    sget-object v0, LX/2eA;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2115739
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2115740
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2115741
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;

    .line 2115742
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->b:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v2, p2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2115743
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2115744
    new-instance v5, LX/2eG;

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->d:LX/2dq;

    const/high16 v7, 0x431e0000    # 158.0f

    sget-object v8, LX/2eF;->a:LX/1Ua;

    const/4 v9, 0x1

    invoke-virtual {v6, v7, v8, v9}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v6

    const/4 v7, 0x0

    .line 2115745
    new-instance v8, LX/EP2;

    invoke-direct {v8, p0, v0}, LX/EP2;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;)V

    move-object v8, v8

    .line 2115746
    invoke-virtual {v0}, Lcom/facebook/graphql/model/BaseFeedUnit;->g()Ljava/lang/String;

    move-result-object v9

    move-object v10, v0

    invoke-direct/range {v5 .. v10}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    move-object v0, v5

    .line 2115747
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2115748
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115749
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
