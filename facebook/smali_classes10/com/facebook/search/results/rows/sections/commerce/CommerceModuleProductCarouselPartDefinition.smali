.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxV;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/commerce/SearchResultsCommerceModuleInterfaces$SearchResultsCommerceModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/0Uh;

.field public final b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/2dq;

.field public final f:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;

.field public final g:LX/CvY;


# direct methods
.method public constructor <init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;LX/CvY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105087
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2105088
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->a:LX/0Uh;

    .line 2105089
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;

    .line 2105090
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->c:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 2105091
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2105092
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->e:LX/2dq;

    .line 2105093
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->f:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;

    .line 2105094
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->g:LX/CvY;

    .line 2105095
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;
    .locals 11

    .prologue
    .line 2105096
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    monitor-enter v1

    .line 2105097
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105098
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105099
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105100
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105101
    new-instance v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v8

    check-cast v8, LX/2dq;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v10

    check-cast v10, LX/CvY;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;-><init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemPartDefinition;Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselSeeMorePartDefinition;LX/CvY;)V

    .line 2105102
    move-object v0, v3

    .line 2105103
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105104
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105105
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105106
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2105107
    sget-object v0, LX/2eA;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2105108
    check-cast p2, LX/CzL;

    check-cast p3, LX/CxV;

    .line 2105109
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->c:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v1, LX/EJI;

    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    invoke-direct {v1, p2, v2}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2105110
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->d:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    const/4 v5, 0x0

    .line 2105111
    iget-object v3, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 2105112
    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bJ()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->o()LX/A2T;

    move-result-object v3

    invoke-interface {v3}, LX/A2T;->dW_()Ljava/lang/String;

    move-result-object v7

    .line 2105113
    new-instance v3, LX/2eG;

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->e:LX/2dq;

    const/high16 v6, 0x43200000    # 160.0f

    sget-object v8, LX/2eF;->a:LX/1Ua;

    const/4 v9, 0x1

    invoke-virtual {v4, v6, v8, v9}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v4

    .line 2105114
    new-instance v6, LX/EJk;

    invoke-direct {v6, p0, p2, p3}, LX/EJk;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;LX/CzL;LX/CxV;)V

    move-object v6, v6

    .line 2105115
    new-instance v8, LX/EJj;

    invoke-direct {v8, p0, v7}, LX/EJj;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;Ljava/lang/String;)V

    invoke-direct/range {v3 .. v8}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    move-object v1, v3

    .line 2105116
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2105117
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2105118
    check-cast p1, LX/CzL;

    const/4 v0, 0x0

    .line 2105119
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->a:LX/0Uh;

    sget v2, LX/2SU;->X:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, LX/CzL;->k()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
