.class public Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxe;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:LX/1nD;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/1nD;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116205
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2116206
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2116207
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;->b:LX/1nD;

    .line 2116208
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2116209
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;
    .locals 6

    .prologue
    .line 2116210
    const-class v1, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

    monitor-enter v1

    .line 2116211
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116212
    sput-object v2, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116213
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116214
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116215
    new-instance p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v4

    check-cast v4, LX/1nD;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;-><init>(Lcom/facebook/content/SecureContextHelper;LX/1nD;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V

    .line 2116216
    move-object v0, p0

    .line 2116217
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116218
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116219
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116220
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2116221
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/Cxe;

    .line 2116222
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EPE;

    invoke-direct {v1, p0, p2, p3}, LX/EPE;-><init>(Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsClickListenerPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/Cxe;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2116223
    const/4 v0, 0x0

    return-object v0
.end method
