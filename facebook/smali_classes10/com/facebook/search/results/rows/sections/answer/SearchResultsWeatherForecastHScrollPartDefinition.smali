.class public Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A2F;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Ua;

.field private static h:LX/0Xm;


# instance fields
.field public final b:LX/0SG;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "TE;>;>;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dq;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2103544
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x40000000    # -2.0f

    .line 2103545
    iput v1, v0, LX/1UY;->b:F

    .line 2103546
    move-object v0, v0

    .line 2103547
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dq;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103536
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2103537
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->b:LX/0SG;

    .line 2103538
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->c:LX/0Ot;

    .line 2103539
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->d:LX/0Ot;

    .line 2103540
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->e:LX/0Ot;

    .line 2103541
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->f:LX/0Ot;

    .line 2103542
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->g:LX/0Ot;

    .line 2103543
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;
    .locals 10

    .prologue
    .line 2103525
    const-class v1, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    monitor-enter v1

    .line 2103526
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103527
    sput-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103528
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103529
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103530
    new-instance v3, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    const/16 v5, 0x114d

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x338b

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x741

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x73f

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x32d4

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;-><init>(LX/0SG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2103531
    move-object v0, v3

    .line 2103532
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103533
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103534
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103535
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/CzL;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/A2F;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x6

    const/4 v2, 0x0

    .line 2103499
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2103500
    check-cast v0, LX/A2F;

    .line 2103501
    invoke-interface {v0}, LX/A2F;->bm()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/A2F;->bm()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v1, v6, :cond_1

    :cond_0
    move v0, v2

    .line 2103502
    :goto_0
    return v0

    .line 2103503
    :cond_1
    invoke-interface {v0}, LX/A2F;->bm()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;

    .line 2103504
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastItemPartDefinition;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherHourlyForecastModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2103505
    add-int/lit8 v0, v1, 0x1

    if-lt v0, v6, :cond_3

    .line 2103506
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2103507
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_4
    move v0, v2

    .line 2103508
    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2103524
    sget-object v0, LX/2eA;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2103510
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    const/4 v4, 0x0

    .line 2103511
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    .line 2103512
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 2103513
    iget-object v5, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v5, v5

    .line 2103514
    check-cast v5, LX/A2F;

    invoke-interface {v5}, LX/A2F;->d()Ljava/lang/String;

    move-result-object v9

    .line 2103515
    const v5, 0x7f0b171f

    invoke-virtual {v6, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-static {v6, v5}, LX/0tP;->b(Landroid/content/res/Resources;F)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x41000000    # 8.0f

    add-float v7, v5, v6

    .line 2103516
    new-instance v5, LX/2eG;

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/2dq;

    sget-object v8, LX/2eF;->a:LX/1Ua;

    const/4 v10, 0x1

    invoke-virtual {v6, v7, v8, v10}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v6

    const/4 v7, 0x0

    .line 2103517
    iget-object v8, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v8, v8

    .line 2103518
    check-cast v8, LX/A2F;

    .line 2103519
    new-instance v10, LX/EJB;

    invoke-direct {v10, p0, v8, p2, p3}, LX/EJB;-><init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;LX/A2F;LX/CzL;LX/1Pn;)V

    move-object v8, v10

    .line 2103520
    new-instance v10, LX/EJA;

    invoke-direct {v10, p0, v9}, LX/EJA;-><init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;Ljava/lang/String;)V

    invoke-direct/range {v5 .. v10}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    move-object v1, v5

    .line 2103521
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2103522
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->a:LX/1Ua;

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, v4, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2103523
    return-object v4
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2103509
    check-cast p1, LX/CzL;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->a(LX/CzL;)Z

    move-result v0

    return v0
.end method
