.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xP;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2113106
    const-class v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;

    const-string v1, "keyword_search"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1xP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2113108
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->b:LX/0Or;

    .line 2113109
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->c:LX/0Ot;

    .line 2113110
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->d:LX/0Ot;

    .line 2113111
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;
    .locals 6

    .prologue
    .line 2113112
    const-class v1, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;

    monitor-enter v1

    .line 2113113
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113114
    sput-object v2, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113115
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113116
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113117
    new-instance v3, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xc7b

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x32d4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;-><init>(LX/0Or;LX/0Ot;LX/0Ot;)V

    .line 2113118
    move-object v0, v3

    .line 2113119
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113120
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113121
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CzL;LX/1Ps;)V
    .locals 8
    .param p1    # LX/CzL;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataInterfaces$SearchResultsFlexibleContextMetadata;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2113123
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2113124
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2113125
    :goto_0
    return-void

    .line 2113126
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    move-object v1, p2

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->OPEN_LINK_BY_IMAGE:LX/8ch;

    move-object v3, p2

    check-cast v3, LX/CxP;

    invoke-interface {v3, p1}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v4, p2

    check-cast v4, LX/CxV;

    invoke-interface {v4}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v5

    move-object v4, p2

    check-cast v4, LX/CxP;

    invoke-interface {v4, p1}, LX/CxP;->b(LX/CzL;)I

    move-result v4

    sget-object v6, LX/8ch;->OPEN_LINK_BY_IMAGE:LX/8ch;

    invoke-static {v5, v4, v6, v7}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/8ch;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2113127
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoComponentSpec;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1xP;

    check-cast p2, LX/1Pn;

    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2113128
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2113129
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1, v7, v7}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    goto :goto_0
.end method
