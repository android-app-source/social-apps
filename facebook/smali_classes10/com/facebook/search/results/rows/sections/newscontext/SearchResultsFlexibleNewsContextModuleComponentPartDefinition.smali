.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "LX/8dK;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/ENZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/ENZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112929
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2112930
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;->d:LX/ENZ;

    .line 2112931
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<",
            "LX/8dK;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2112950
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;->d:LX/ENZ;

    const/4 v1, 0x0

    .line 2112951
    new-instance v2, LX/ENY;

    invoke-direct {v2, v0}, LX/ENY;-><init>(LX/ENZ;)V

    .line 2112952
    iget-object p0, v0, LX/ENZ;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/ENX;

    .line 2112953
    if-nez p0, :cond_0

    .line 2112954
    new-instance p0, LX/ENX;

    invoke-direct {p0, v0}, LX/ENX;-><init>(LX/ENZ;)V

    .line 2112955
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/ENX;->a$redex0(LX/ENX;LX/1De;IILX/ENY;)V

    .line 2112956
    move-object v2, p0

    .line 2112957
    move-object v1, v2

    .line 2112958
    move-object v0, v1

    .line 2112959
    iget-object v1, v0, LX/ENX;->a:LX/ENY;

    iput-object p2, v1, LX/ENY;->a:LX/CzL;

    .line 2112960
    iget-object v1, v0, LX/ENX;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2112961
    move-object v0, v0

    .line 2112962
    check-cast p3, LX/1Ps;

    .line 2112963
    iget-object v1, v0, LX/ENX;->a:LX/ENY;

    iput-object p3, v1, LX/ENY;->b:LX/1Ps;

    .line 2112964
    iget-object v1, v0, LX/ENX;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2112965
    move-object v0, v0

    .line 2112966
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;
    .locals 5

    .prologue
    .line 2112939
    const-class v1, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;

    monitor-enter v1

    .line 2112940
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112941
    sput-object v2, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112942
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112943
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112944
    new-instance p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/ENZ;->a(LX/0QB;)LX/ENZ;

    move-result-object v4

    check-cast v4, LX/ENZ;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;-><init>(Landroid/content/Context;LX/ENZ;)V

    .line 2112945
    move-object v0, p0

    .line 2112946
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112947
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112948
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112949
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2112938
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2112937
    check-cast p2, LX/CzL;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModuleComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2112933
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 2112934
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2112935
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v2

    .line 2112936
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8dD;

    invoke-interface {v0}, LX/8dD;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8dD;

    invoke-interface {v0}, LX/8dD;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8dD;

    invoke-interface {v0}, LX/8dD;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2112932
    const/4 v0, 0x0

    return-object v0
.end method
