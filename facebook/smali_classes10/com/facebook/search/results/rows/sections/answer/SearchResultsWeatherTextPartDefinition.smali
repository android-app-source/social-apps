.class public Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A2F;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2103629
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->FAHRENHEIT:Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLWeatherTemperature;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103622
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2103623
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 2103624
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2103625
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2103626
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->e:LX/0Ot;

    .line 2103627
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->f:LX/0Ot;

    .line 2103628
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;
    .locals 9

    .prologue
    .line 2103611
    const-class v1, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    monitor-enter v1

    .line 2103612
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103613
    sput-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103614
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103615
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103616
    new-instance v3, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    const/16 v7, 0x32d4

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x455

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Ot;LX/0Ot;)V

    .line 2103617
    move-object v0, v3

    .line 2103618
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103619
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103620
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103621
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/A2F;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2103607
    invoke-interface {p0}, LX/A2F;->bl()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;->c()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object v1

    .line 2103608
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "\u2109"

    .line 2103609
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;->b()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2103610
    :cond_0
    const-string v0, "\u2103"

    goto :goto_0
.end method

.method public static a(LX/CzL;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/A2F;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2103592
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2103593
    check-cast v0, LX/A2F;

    .line 2103594
    invoke-interface {v0}, LX/A2F;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/A2F;->bl()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/A2F;->bl()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;->c()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$TemperatureFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2103606
    sget-object v0, LX/3bQ;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2103596
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 2103597
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2103598
    check-cast v0, LX/A2F;

    .line 2103599
    const v1, 0x7f0d0a0a

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082290

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->a(LX/A2F;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-interface {v0}, LX/A2F;->d()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103600
    const v1, 0x7f0d0ada

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->c:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {v0}, LX/A2F;->bl()Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsWeatherModels$SearchResultsWeatherModel$WeatherConditionModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103601
    new-instance v0, LX/EJC;

    invoke-direct {v0, p0, p2, p3}, LX/EJC;-><init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;LX/CzL;LX/1Pn;)V

    move-object v0, v0

    .line 2103602
    const v1, 0x7f0d0a0a

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103603
    const v1, 0x7f0d0ada

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103604
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    check-cast p3, LX/1Ps;

    invoke-static {p3}, LX/3bQ;->a(LX/1Ps;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2103605
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2103595
    check-cast p1, LX/CzL;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->a(LX/CzL;)Z

    move-result v0

    return v0
.end method
