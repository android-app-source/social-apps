.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/String;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1Ua;


# instance fields
.field private final e:LX/0ad;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2113311
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f800000    # -4.0f

    .line 2113312
    iput v1, v0, LX/1UY;->b:F

    .line 2113313
    move-object v0, v0

    .line 2113314
    const/4 v1, 0x0

    .line 2113315
    iput v1, v0, LX/1UY;->c:F

    .line 2113316
    move-object v0, v0

    .line 2113317
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;->d:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113318
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2113319
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;->e:LX/0ad;

    .line 2113320
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;->f:LX/1V0;

    .line 2113321
    return-void
.end method

.method private a(LX/1De;Ljava/lang/String;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/String;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2113322
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a010d

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v0

    const v1, 0x3f8ccccd    # 1.1f

    invoke-virtual {v0, v1}, LX/1ne;->j(F)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    move-object v0, p3

    .line 2113323
    check-cast v0, LX/1Ps;

    invoke-interface {v0}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;

    if-eqz v0, :cond_0

    sget-object v0, LX/1X9;->MIDDLE:LX/1X9;

    .line 2113324
    :goto_0
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v3, LX/1X6;

    const/4 v4, 0x0

    sget-object v5, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;->d:LX/1Ua;

    invoke-direct {v3, v4, v5, v0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-virtual {v2, p1, p3, v3, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 2113325
    :cond_0
    sget-object v0, LX/1X9;->BOTTOM:LX/1X9;

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2113305
    check-cast p2, Ljava/lang/String;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;->a(LX/1De;Ljava/lang/String;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2113310
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;->a(LX/1De;Ljava/lang/String;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 2113309
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 2113308
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;->e:LX/0ad;

    sget-short v1, LX/100;->S:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2113307
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2113306
    const/4 v0, 0x0

    return-object v0
.end method
