.class public Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxa;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsSeeMorePivotModuleInterfaces$SearchResultsSeeMorePivotModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

.field private final d:LX/EPI;

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nD;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;LX/EPI;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2116410
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2116411
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116412
    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->e:LX/0Ot;

    .line 2116413
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116414
    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->f:LX/0Ot;

    .line 2116415
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116416
    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->g:LX/0Ot;

    .line 2116417
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116418
    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->h:LX/0Ot;

    .line 2116419
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2116420
    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->i:LX/0Ot;

    .line 2116421
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    .line 2116422
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2116423
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 2116424
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->d:LX/EPI;

    .line 2116425
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;
    .locals 8

    .prologue
    .line 2116426
    const-class v1, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;

    monitor-enter v1

    .line 2116427
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116428
    sput-object v2, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116429
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116430
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116431
    new-instance v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-static {v0}, LX/EPI;->b(LX/0QB;)LX/EPI;

    move-result-object v7

    check-cast v7, LX/EPI;

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;LX/EPI;)V

    .line 2116432
    const/16 v4, 0x1140

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x455

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32d4

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1032

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 p0, 0x3488

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2116433
    iput-object v4, v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->e:LX/0Ot;

    iput-object v5, v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->f:LX/0Ot;

    iput-object v6, v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->g:LX/0Ot;

    iput-object v7, v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->h:LX/0Ot;

    iput-object p0, v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->i:LX/0Ot;

    .line 2116434
    move-object v0, v3

    .line 2116435
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116436
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116437
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116438
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2116439
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2116440
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxi;

    const/4 v5, 0x0

    .line 2116441
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePostsButtonPartDefinition;

    .line 2116442
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116443
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2116444
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v1, LX/3aw;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    const/4 v4, 0x0

    invoke-direct {v1, v5, v2, v3, v4}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2116445
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2116446
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116447
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v0

    invoke-interface {v0}, LX/8dH;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2116448
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116449
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v0

    .line 2116450
    invoke-interface {v0}, LX/8dH;->fo_()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/8dH;->fo_()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, LX/8dH;->fo_()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;->a()LX/0Px;

    .line 2116451
    :goto_0
    goto :goto_2

    .line 2116452
    :goto_1
    return-object v5

    .line 2116453
    :goto_2
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;->d:LX/EPI;

    invoke-virtual {v1, p2, p3}, LX/EPI;->a(LX/CzL;LX/Cxi;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_1

    .line 2116454
    :cond_0
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116455
    check-cast p1, LX/CzL;

    .line 2116456
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116457
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
