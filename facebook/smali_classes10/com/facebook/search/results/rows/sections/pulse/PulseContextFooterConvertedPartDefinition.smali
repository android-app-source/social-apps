.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        "E::",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/CxA;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "LX/A3T;",
        ">;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2115484
    sget-object v0, LX/3b7;->a:LX/1Cz;

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115501
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2115502
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    .line 2115503
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;

    .line 2115504
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;
    .locals 5

    .prologue
    .line 2115490
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;

    monitor-enter v1

    .line 2115491
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115492
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115493
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115494
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115495
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;)V

    .line 2115496
    move-object v0, p0

    .line 2115497
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115498
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115499
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115500
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 2115505
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2115486
    check-cast p2, LX/CzL;

    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 2115487
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsConvertedPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2115488
    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterConvertedPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    new-instance v0, LX/21P;

    sget-object v4, LX/1Wi;->TOP:LX/1Wi;

    move v2, v1

    move v3, v1

    invoke-direct/range {v0 .. v5}, LX/21P;-><init>(ZZZLX/1Wi;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2115489
    return-object v5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115485
    const/4 v0, 0x1

    return v0
.end method
