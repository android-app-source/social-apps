.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxG;",
        ":",
        "LX/Cxh;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        "LX/EJd;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/7j6;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/7j6;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/7j6;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104776
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2104777
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2104778
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;->b:LX/7j6;

    .line 2104779
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;->c:LX/0Ot;

    .line 2104780
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;
    .locals 6

    .prologue
    .line 2104753
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;

    monitor-enter v1

    .line 2104754
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104755
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104756
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104757
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104758
    new-instance v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v4

    check-cast v4, LX/7j6;

    const/16 p0, 0x32d4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/7j6;LX/0Ot;)V

    .line 2104759
    move-object v0, v5

    .line 2104760
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104761
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104762
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104763
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2104773
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    check-cast p3, LX/1Pn;

    .line 2104774
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EJc;

    invoke-direct {v1, p0, p2, p3}, LX/EJc;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemBuyNowCallToActionPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Pn;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104775
    new-instance v0, LX/EJd;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0822bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0202ab

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/EJd;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x434d788b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2104769
    check-cast p2, LX/EJd;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 2104770
    iget-object v1, p2, LX/EJd;->a:Ljava/lang/String;

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2104771
    iget-object v1, p2, LX/EJd;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2104772
    const/16 v1, 0x1f

    const v2, -0x5a962562

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2104764
    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2104765
    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2104766
    invoke-static {p4, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2104767
    invoke-virtual {p4, v0, v0, v0, v0}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2104768
    return-void
.end method
