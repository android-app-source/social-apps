.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8dA;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition",
            "<TE;",
            "LX/8dA;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

.field private final g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field private final i:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109089
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2109090
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 2109091
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2109092
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2109093
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->d:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2109094
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    .line 2109095
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    .line 2109096
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    .line 2109097
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 2109098
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->i:LX/0Uh;

    .line 2109099
    return-void
.end method

.method public static a(LX/CzL;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8dA;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2109100
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109101
    check-cast v0, LX/8dA;

    .line 2109102
    invoke-interface {v0}, LX/8dA;->q()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;

    move-result-object v0

    .line 2109103
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2109104
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel$NodesModel;

    .line 2109105
    new-instance v5, LX/2dc;

    invoke-direct {v5}, LX/2dc;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel$NodesModel;->a()LX/1Fb;

    move-result-object v6

    invoke-interface {v6}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v6

    .line 2109106
    iput-object v6, v5, LX/2dc;->h:Ljava/lang/String;

    .line 2109107
    move-object v5, v5

    .line 2109108
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel$NodesModel;->a()LX/1Fb;

    move-result-object v6

    invoke-interface {v6}, LX/1Fb;->c()I

    move-result v6

    .line 2109109
    iput v6, v5, LX/2dc;->i:I

    .line 2109110
    move-object v5, v5

    .line 2109111
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$GroupMembersModel$NodesModel;->a()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->a()I

    move-result v0

    .line 2109112
    iput v0, v5, LX/2dc;->c:I

    .line 2109113
    move-object v0, v5

    .line 2109114
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2109115
    new-instance v5, LX/33O;

    invoke-direct {v5}, LX/33O;-><init>()V

    .line 2109116
    iput-object v0, v5, LX/33O;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2109117
    move-object v0, v5

    .line 2109118
    invoke-virtual {v0}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 2109119
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2109120
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2109121
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;
    .locals 13

    .prologue
    .line 2109122
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;

    monitor-enter v1

    .line 2109123
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109124
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109125
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109126
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109127
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;LX/0Uh;)V

    .line 2109128
    move-object v0, v3

    .line 2109129
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109130
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109131
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109132
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/CzL;LX/0Uh;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8dA;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2109133
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2109134
    invoke-static {v0}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 2109135
    invoke-static {v0}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2109136
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v0

    .line 2109137
    :goto_0
    return-object v0

    .line 2109138
    :cond_1
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109139
    check-cast v0, LX/8dA;

    .line 2109140
    invoke-interface {v0}, LX/8dA;->t()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-interface {v0}, LX/8dA;->t()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$VisibilitySentenceModel;->a()Ljava/lang/String;

    move-result-object p0

    :goto_1
    move-object v0, p0

    .line 2109141
    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public static b(LX/CzL;LX/0Uh;)Ljava/lang/CharSequence;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8dA;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 2109142
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2109143
    invoke-static {v0}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 2109144
    invoke-static {v0}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2109145
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v0

    .line 2109146
    :goto_0
    return-object v0

    .line 2109147
    :cond_1
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109148
    check-cast v0, LX/8dA;

    .line 2109149
    invoke-interface {v0}, LX/8dA;->r()Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;

    move-result-object p0

    .line 2109150
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsGroupModels$SearchResultsGroupModel$SocialContextModel;->a()Ljava/lang/String;

    move-result-object p0

    :goto_1
    move-object v0, p0

    .line 2109151
    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2109152
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2109153
    check-cast p2, LX/CzL;

    .line 2109154
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v1, LX/EJI;

    sget-object v2, LX/3ap;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109155
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2109156
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109157
    check-cast v0, LX/8dA;

    invoke-interface {v0}, LX/8dA;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109158
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->i:LX/0Uh;

    invoke-static {p2, v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->a(LX/CzL;LX/0Uh;)Ljava/lang/String;

    move-result-object v1

    .line 2109159
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109160
    const v0, 0x7f0d0d8e

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    new-instance v3, LX/ELL;

    const/16 v4, 0x9

    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->a(LX/CzL;)LX/0Px;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/ELL;-><init>(ILX/0Px;)V

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2109161
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->d:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2109162
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109163
    check-cast v0, LX/8dA;

    invoke-interface {v0}, LX/8dA;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LX/ELM;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109164
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109165
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109166
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->i:LX/0Uh;

    invoke-static {p2, v1}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->b(LX/CzL;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109167
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2109168
    const/4 v0, 0x1

    return v0
.end method
