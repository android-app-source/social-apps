.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxj;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117448
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2117449
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    .line 2117450
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;->b:LX/0Ot;

    .line 2117451
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;
    .locals 5

    .prologue
    .line 2117452
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    monitor-enter v1

    .line 2117453
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117454
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117455
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117456
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117457
    new-instance v4, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    const/16 p0, 0x32d4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;-><init>(Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;LX/0Ot;)V

    .line 2117458
    move-object v0, v4

    .line 2117459
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117460
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117461
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117462
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2117463
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxj;

    const/4 v4, 0x0

    .line 2117464
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117465
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    const/4 v1, -0x1

    invoke-interface {p3, v0, v1}, LX/Cxj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/CyM;

    move-result-object v5

    move-object v0, p3

    .line 2117466
    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v0

    invoke-interface {v0}, LX/8ef;->c()Ljava/lang/String;

    move-result-object v7

    .line 2117467
    :goto_0
    iget-object v9, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;->a:Lcom/facebook/video/channelfeed/ChannelFeedFromVideoLauncherPartDefinition;

    new-instance v0, LX/3FV;

    .line 2117468
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2117469
    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2117470
    new-instance v2, Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v3, LX/EPj;

    invoke-direct {v3, p0, v5}, LX/EPj;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;LX/2oM;)V

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    move-object v2, v2

    .line 2117471
    new-instance v3, LX/7gJ;

    invoke-direct {v3, v5}, LX/7gJ;-><init>(LX/2oM;)V

    .line 2117472
    new-instance v5, LX/EPk;

    invoke-direct {v5, p0, p2, p3}, LX/EPk;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;LX/CzL;LX/Cxj;)V

    move-object v5, v5

    .line 2117473
    sget-object v6, LX/0JG;->SEARCH_RESULTS:LX/0JG;

    move-object v8, v4

    invoke-direct/range {v0 .. v8}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;LX/3FQ;LX/D4s;LX/D6L;LX/0JG;Ljava/lang/String;LX/04D;)V

    invoke-interface {p1, v9, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2117474
    return-object v4

    :cond_0
    move-object v7, v4

    .line 2117475
    goto :goto_0
.end method
