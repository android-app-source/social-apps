.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107882
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2107883
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;

    .line 2107884
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;
    .locals 4

    .prologue
    .line 2107885
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;

    monitor-enter v1

    .line 2107886
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107887
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107888
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107889
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107890
    new-instance p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;)V

    .line 2107891
    move-object v0, p0

    .line 2107892
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107893
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107894
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107895
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2107896
    check-cast p2, LX/CzL;

    .line 2107897
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsArticlesNavigationalModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsNavigationalExternalUrlPartDefinition;

    invoke-static {p1, v0, p2}, LX/EJ4;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/CzL;)V

    .line 2107898
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2107899
    check-cast p1, LX/CzL;

    .line 2107900
    invoke-virtual {p1}, LX/CzL;->k()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
