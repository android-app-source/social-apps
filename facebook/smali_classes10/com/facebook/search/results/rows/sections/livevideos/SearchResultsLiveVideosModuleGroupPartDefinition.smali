.class public Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ":",
        "LX/7Lk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/video/SearchResultsLiveVideosModuleInterfaces$SearchResultsLiveVideosModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosHScrollPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;",
            "Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112377
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2112378
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    .line 2112379
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->b:LX/0Ot;

    .line 2112380
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->c:LX/0Ot;

    .line 2112381
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2112382
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    .line 2112383
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;
    .locals 9

    .prologue
    .line 2112384
    const-class v1, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;

    monitor-enter v1

    .line 2112385
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112386
    sput-object v2, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112387
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112388
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112389
    new-instance v3, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    const/16 v5, 0x11aa

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x34a9

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;)V

    .line 2112390
    move-object v0, v3

    .line 2112391
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112392
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112393
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112394
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2112395
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxi;

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 2112396
    check-cast p3, LX/CxV;

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    .line 2112397
    iget-object v1, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 2112398
    invoke-static {v1}, LX/8eM;->j(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v1

    .line 2112399
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v2, :cond_1

    if-gt v1, v4, :cond_1

    .line 2112400
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->e:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    invoke-virtual {p1, v0, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2112401
    :cond_0
    :goto_0
    return-object v3

    .line 2112402
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    .line 2112403
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 2112404
    invoke-virtual {p1, v0, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2112405
    if-le v1, v4, :cond_2

    .line 2112406
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-static {p2}, LX/EPr;->a(LX/CzL;)LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2112407
    :goto_1
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2112408
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2112409
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2112410
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livevideos/SearchResultsLiveVideosModuleGroupPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2112411
    check-cast p1, LX/CzL;

    .line 2112412
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
