.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d9;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CyD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CyD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108819
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2108820
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    .line 2108821
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->b:LX/0Ot;

    .line 2108822
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->c:LX/0Ot;

    .line 2108823
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->d:LX/0Ot;

    .line 2108824
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->e:LX/0Ot;

    .line 2108825
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;
    .locals 9

    .prologue
    .line 2108826
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    monitor-enter v1

    .line 2108827
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108828
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2108829
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108830
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2108831
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    const/16 v5, 0x12b1

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32d4

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x113f

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x3301

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2108832
    move-object v0, v3

    .line 2108833
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2108834
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2108835
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2108836
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2108837
    check-cast p2, LX/CzL;

    check-cast p3, LX/CxA;

    .line 2108838
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    .line 2108839
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2108840
    check-cast v1, LX/8d9;

    .line 2108841
    invoke-interface {v1}, LX/8d9;->dV_()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/events/model/Event;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;)Z

    move-result v2

    .line 2108842
    invoke-interface {v1}, LX/8d9;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v3

    invoke-interface {v1}, LX/8d9;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v1

    .line 2108843
    iget-object v4, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2108844
    check-cast v4, LX/8d9;

    invoke-interface {v4}, LX/8d9;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v4, v5, :cond_0

    .line 2108845
    iget-object v4, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2108846
    check-cast v4, LX/8d9;

    invoke-interface {v4}, LX/8d9;->l()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eq v4, v5, :cond_0

    .line 2108847
    iget-object v4, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2108848
    check-cast v4, LX/8d9;

    invoke-interface {v4}, LX/8d9;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v4, v5, :cond_0

    .line 2108849
    iget-object v4, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2108850
    check-cast v4, LX/8d9;

    invoke-interface {v4}, LX/8d9;->m()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v4, v5, :cond_1

    .line 2108851
    :cond_0
    const/4 v4, 0x0

    .line 2108852
    :goto_0
    move-object v4, v4

    .line 2108853
    invoke-static {v2, v3, v1, v4}, LX/ELK;->a(ZLcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Landroid/view/View$OnClickListener;)LX/EL8;

    move-result-object v1

    move-object v1, v1

    .line 2108854
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108855
    const/4 v0, 0x0

    return-object v0

    :cond_1
    new-instance v4, LX/ELP;

    invoke-direct {v4, p0, p2, p3}, LX/ELP;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;LX/CzL;LX/CxA;)V

    goto :goto_0
.end method
