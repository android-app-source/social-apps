.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/0Uh;

.field private final c:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2105573
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0xa7c5482

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-void
.end method

.method public constructor <init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105574
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2105575
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->b:LX/0Uh;

    .line 2105576
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;

    .line 2105577
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    .line 2105578
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;
    .locals 6

    .prologue
    .line 2105579
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;

    monitor-enter v1

    .line 2105580
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105581
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105582
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105583
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105584
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;-><init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;)V

    .line 2105585
    move-object v0, p0

    .line 2105586
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105587
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105588
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105589
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2105590
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    check-cast p3, LX/1Pr;

    const/4 v1, 0x0

    .line 2105591
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 2105592
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v0, v2, :cond_2

    new-instance v0, LX/EK2;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v2}, LX/EK2;-><init>(Ljava/lang/Boolean;)V

    invoke-interface {p3, v0}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2105593
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2105594
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105595
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2105596
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2105597
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2105598
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move v0, v1

    .line 2105599
    :goto_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    if-gt v0, v1, :cond_4

    .line 2105600
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridRowPartDefinition;

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {p1, v1, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105601
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 2105602
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->b:LX/0Uh;

    sget v2, LX/2SU;->X:I

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2105603
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 2105604
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    move v2, v0

    .line 2105605
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    :goto_3
    if-ge v3, v5, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105606
    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemPartDefinition;

    invoke-virtual {p1, v6, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v0

    .line 2105607
    if-eqz v0, :cond_5

    if-eqz v2, :cond_5

    .line 2105608
    add-int/lit8 v0, v1, 0x1

    .line 2105609
    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    .line 2105610
    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_3

    :cond_3
    move v2, v1

    .line 2105611
    goto :goto_2

    .line 2105612
    :cond_4
    const/4 v0, 0x0

    return-object v0

    :cond_5
    move v0, v1

    goto :goto_4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2105613
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2105614
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->u()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->u()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGroupPartDefinition;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
