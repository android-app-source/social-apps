.class public Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/answer/SearchResultsTimeInterfaces$SearchResultsTime;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0SG;

.field private final b:LX/0W9;

.field public final c:Ljava/util/TimeZone;

.field private final d:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public constructor <init>(LX/0SG;LX/0W9;LX/0Or;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0W9;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103446
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2103447
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->a:LX/0SG;

    .line 2103448
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->b:LX/0W9;

    .line 2103449
    invoke-interface {p3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->c:Ljava/util/TimeZone;

    .line 2103450
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->d:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 2103451
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2103452
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;
    .locals 9

    .prologue
    .line 2103435
    const-class v1, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;

    monitor-enter v1

    .line 2103436
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103437
    sput-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103438
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103439
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103440
    new-instance v3, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v5

    check-cast v5, LX/0W9;

    const/16 v6, 0x161a

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;-><init>(LX/0SG;LX/0W9;LX/0Or;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2103441
    move-object v0, v3

    .line 2103442
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103443
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103444
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103445
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Ljava/util/TimeZone;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2103431
    new-instance v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->b:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, p2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2103432
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 2103433
    new-instance v1, Ljava/util/Date;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 2103434
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/CzL;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/answer/SearchResultsTimeInterfaces$SearchResultsTime;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2103428
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2103429
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2103430
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cA()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cA()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2103427
    sget-object v0, LX/3bQ;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2103396
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 2103397
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 2103398
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2103399
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2103400
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cA()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    .line 2103401
    const v3, 0x7f08228f

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "h:mm a"

    invoke-direct {p0, v2, v6}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->a(Ljava/util/TimeZone;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2103402
    const-string v0, "EEEE, MMMM dd, yyyy"

    invoke-direct {p0, v2, v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->a(Ljava/util/TimeZone;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2103403
    iget-object v7, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->c:Ljava/util/TimeZone;

    iget-object v8, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->a:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v7

    .line 2103404
    iget-object v8, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->a:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v9

    invoke-virtual {v2, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v8

    .line 2103405
    sub-int v7, v8, v7

    .line 2103406
    const v8, 0x36ee80

    div-int v8, v7, v8

    .line 2103407
    const v9, 0xea60

    div-int/2addr v7, v9

    rem-int/lit8 v7, v7, 0x3c

    .line 2103408
    if-nez v8, :cond_1

    if-nez v7, :cond_1

    .line 2103409
    const/4 v7, 0x0

    .line 2103410
    :goto_0
    move-object v1, v7

    .line 2103411
    if-eqz v1, :cond_0

    .line 2103412
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2103413
    :cond_0
    const v1, 0x7f0d0a0a

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103414
    const v1, 0x7f0d0ada

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103415
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->d:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    check-cast p3, LX/1Ps;

    invoke-static {p3}, LX/3bQ;->a(LX/1Ps;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2103416
    const/4 v0, 0x0

    return-object v0

    .line 2103417
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 2103418
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2103419
    if-eqz v7, :cond_2

    .line 2103420
    const/16 v10, 0x3a

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2103421
    :cond_2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v9, v9

    .line 2103422
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_5

    if-eqz v7, :cond_5

    const/4 v10, 0x2

    :goto_1
    move v10, v10

    .line 2103423
    if-gtz v8, :cond_3

    if-lez v7, :cond_4

    :cond_3
    const v7, 0x7f0f010c

    .line 2103424
    :goto_2
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v9, v8, v11

    invoke-virtual {v1, v7, v10, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 2103425
    :cond_4
    const v7, 0x7f0f010d

    goto :goto_2

    :cond_5
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v10

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2103426
    check-cast p1, LX/CzL;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsTimeTextPartDefinition;->a(LX/CzL;)Z

    move-result v0

    return v0
.end method
