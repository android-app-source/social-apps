.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxy;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8dA;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CyD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CyD;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108990
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2108991
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    .line 2108992
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->b:LX/0Ot;

    .line 2108993
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->c:LX/0Ot;

    .line 2108994
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->d:LX/0Ot;

    .line 2108995
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->e:LX/0Ot;

    .line 2108996
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;
    .locals 9

    .prologue
    .line 2108997
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    monitor-enter v1

    .line 2108998
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108999
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109000
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109001
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109002
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    const/16 v5, 0x12b1

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32d4

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x3301

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x113f

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2109003
    move-object v0, v3

    .line 2109004
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109005
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109006
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109007
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2109008
    check-cast p2, LX/CzL;

    check-cast p3, LX/CxA;

    .line 2109009
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    .line 2109010
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2109011
    check-cast v1, LX/8dA;

    .line 2109012
    invoke-interface {v1}, LX/8dA;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v3

    move-object v2, p3

    .line 2109013
    check-cast v2, LX/1Pr;

    new-instance v4, LX/EJJ;

    .line 2109014
    iget-object v5, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v5, v5

    .line 2109015
    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v5

    invoke-direct {v4, v5}, LX/EJJ;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)V

    .line 2109016
    new-instance v5, LX/ELT;

    invoke-direct {v5, v1}, LX/ELT;-><init>(LX/8dA;)V

    move-object v5, v5

    .line 2109017
    invoke-interface {v2, v4, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2109018
    invoke-interface {v1}, LX/8dA;->p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v1

    .line 2109019
    iget-object v4, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2109020
    check-cast v4, LX/8dA;

    invoke-interface {v4}, LX/8dA;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-eq v4, v5, :cond_0

    .line 2109021
    iget-object v4, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2109022
    check-cast v4, LX/8dA;

    invoke-interface {v4}, LX/8dA;->s()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->REQUESTED:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v4, v5, :cond_1

    .line 2109023
    :cond_0
    const/4 v4, 0x0

    .line 2109024
    :goto_0
    move-object v4, v4

    .line 2109025
    invoke-static {v3, v2, v1, v4}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityBasePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;Lcom/facebook/graphql/enums/GraphQLGroupCategory;Landroid/view/View$OnClickListener;)LX/EL8;

    move-result-object v1

    move-object v1, v1

    .line 2109026
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109027
    const/4 v0, 0x0

    return-object v0

    :cond_1
    new-instance v4, LX/ELS;

    invoke-direct {v4, p0, p2, p3}, LX/ELS;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;LX/CzL;LX/CxA;)V

    goto :goto_0
.end method
