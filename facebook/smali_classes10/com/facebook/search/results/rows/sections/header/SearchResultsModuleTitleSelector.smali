.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/facebook/search/results/protocol/SearchResultsModuleTitleInterfaces$SearchResultsModuleTitle;",
        ":",
        "Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModuleInterfaces$SearchResultsSeeMoreQueryModule;",
        "E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+TT;>;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleSeeMoreComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2111305
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2111306
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2111307
    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a:LX/0Ot;

    .line 2111308
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2111309
    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->b:LX/0Ot;

    .line 2111310
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;
    .locals 5

    .prologue
    .line 2111311
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    monitor-enter v1

    .line 2111312
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111313
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111314
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111315
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111316
    new-instance v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-direct {v3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;-><init>()V

    .line 2111317
    const/16 v4, 0x1175

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x1177

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2111318
    iput-object v4, v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a:LX/0Ot;

    iput-object p0, v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->b:LX/0Ot;

    .line 2111319
    move-object v0, v3

    .line 2111320
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111321
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111322
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111323
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2111324
    check-cast p2, LX/CzL;

    .line 2111325
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2111326
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111327
    const/4 v0, 0x1

    return v0
.end method
