.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/commerce/SearchResultsCommerceModuleInterfaces$SearchResultsCommerceModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104894
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104895
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;

    .line 2104896
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    .line 2104897
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;

    .line 2104898
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;
    .locals 6

    .prologue
    .line 2104908
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;

    monitor-enter v1

    .line 2104909
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104910
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104911
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104912
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104913
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;)V

    .line 2104914
    move-object v0, p0

    .line 2104915
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104916
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104917
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104918
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2104902
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pr;

    .line 2104903
    check-cast p3, LX/CxV;

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v0

    .line 2104904
    invoke-static {v0}, LX/7BG;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2104905
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2104906
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2104907
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductCarouselPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2104899
    check-cast p1, LX/CzL;

    .line 2104900
    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 2104901
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
