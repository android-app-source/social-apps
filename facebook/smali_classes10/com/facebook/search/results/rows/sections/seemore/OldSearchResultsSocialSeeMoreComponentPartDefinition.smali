.class public Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxa;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/CxG;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1X6;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/1V0;

.field private final f:LX/EPO;

.field private final g:LX/EPI;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2116315
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v2

    const/high16 v3, 0x40c00000    # 6.0f

    .line 2116316
    iput v3, v2, LX/1UY;->b:F

    .line 2116317
    move-object v2, v2

    .line 2116318
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EPO;LX/1V0;LX/EPI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116310
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2116311
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->f:LX/EPO;

    .line 2116312
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->e:LX/1V0;

    .line 2116313
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->g:LX/EPI;

    .line 2116314
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;LX/1Ps;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2116293
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->f:LX/EPO;

    invoke-virtual {v0, p1}, LX/EPO;->c(LX/1De;)LX/EPM;

    move-result-object v0

    .line 2116294
    iget-object v1, p2, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2116295
    invoke-virtual {v0, v1}, LX/EPM;->a(Ljava/lang/CharSequence;)LX/EPM;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->g:LX/EPI;

    .line 2116296
    new-instance v2, LX/EPG;

    invoke-direct {v2, v1, p3, p2}, LX/EPG;-><init>(LX/EPI;LX/1Ps;Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;)V

    move-object v1, v2

    .line 2116297
    invoke-virtual {v0, v1}, LX/EPM;->a(Landroid/view/View$OnClickListener;)LX/EPM;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2116298
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->e:LX/1V0;

    sget-object v2, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->d:LX/1X6;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;
    .locals 7

    .prologue
    .line 2116299
    const-class v1, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;

    monitor-enter v1

    .line 2116300
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116301
    sput-object v2, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116302
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116303
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116304
    new-instance p0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EPO;->a(LX/0QB;)LX/EPO;

    move-result-object v4

    check-cast v4, LX/EPO;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-static {v0}, LX/EPI;->b(LX/0QB;)LX/EPI;

    move-result-object v6

    check-cast v6, LX/EPI;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;-><init>(Landroid/content/Context;LX/EPO;LX/1V0;LX/EPI;)V

    .line 2116305
    move-object v0, p0

    .line 2116306
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116307
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116308
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116309
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2116292
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2116291
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/seemore/OldSearchResultsSocialSeeMoreComponentPartDefinition;->a(LX/1De;Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116289
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2116290
    const/4 v0, 0x0

    return-object v0
.end method
