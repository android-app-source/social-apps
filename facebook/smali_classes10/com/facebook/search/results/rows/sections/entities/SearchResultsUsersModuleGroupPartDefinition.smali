.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/CxA;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/entities/SearchResultsUserModuleInterfaces$SearchResultsUserModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector",
            "<",
            "Lcom/facebook/search/results/protocol/entities/SearchResultsUserModuleInterfaces$SearchResultsUserModule;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;

.field private final f:LX/0W9;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;LX/0ad;LX/0W9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;",
            "LX/0ad;",
            "LX/0W9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110964
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2110965
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    .line 2110966
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->b:LX/0Ot;

    .line 2110967
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->c:LX/0Ot;

    .line 2110968
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2110969
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->e:LX/0ad;

    .line 2110970
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->f:LX/0W9;

    .line 2110971
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;
    .locals 10

    .prologue
    .line 2110953
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;

    monitor-enter v1

    .line 2110954
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110955
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110956
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110957
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110958
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    const/16 v5, 0x3416

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1173

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v9

    check-cast v9, LX/0W9;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;LX/0ad;LX/0W9;)V

    .line 2110959
    move-object v0, v3

    .line 2110960
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110961
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110962
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110963
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2110941
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cwx;

    const/4 v3, 0x0

    .line 2110942
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2110943
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->e:LX/0ad;

    sget-short v1, LX/100;->h:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2110944
    check-cast p3, LX/CxV;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->f:LX/0W9;

    invoke-static {p2, p3, v1}, LX/EJ4;->a(LX/CzL;LX/CxV;LX/0W9;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->e:LX/0ad;

    sget-short v2, LX/100;->g:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2110945
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    :goto_0
    invoke-static {p1, v0, p2}, LX/EJ4;->b(LX/1RF;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/CzL;)V

    .line 2110946
    :goto_1
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2110947
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2110948
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2110949
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 2110950
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    goto :goto_0

    .line 2110951
    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    :goto_2
    invoke-static {p1, v0, p2}, LX/EJ4;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/CzL;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUsersModuleGroupPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2110952
    const/4 v0, 0x1

    return v0
.end method
