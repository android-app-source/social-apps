.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/ENF;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static d:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2112179
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;->a:LX/1Cz;

    sput-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112175
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2112176
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    .line 2112177
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2112178
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;
    .locals 5

    .prologue
    .line 2112164
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;

    monitor-enter v1

    .line 2112165
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112166
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112167
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112168
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112169
    new-instance p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2112170
    move-object v0, p0

    .line 2112171
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112172
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112173
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112174
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2112159
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2112160
    if-nez v0, :cond_0

    move v0, v1

    .line 2112161
    :goto_0
    return v0

    .line 2112162
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2112163
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/ENF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2112158
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2112153
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2112154
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedHeaderPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2112155
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    sget-object v3, LX/1X9;->DIVIDER_TOP:LX/1X9;

    invoke-direct {v1, p2, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2112156
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2112157
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
