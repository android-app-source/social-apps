.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d9;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

.field private final f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition",
            "<TE;",
            "LX/8d9;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field public final i:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108926
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2108927
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 2108928
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2108929
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2108930
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 2108931
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->e:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2108932
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    .line 2108933
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    .line 2108934
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 2108935
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->i:LX/0Uh;

    .line 2108936
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;
    .locals 13

    .prologue
    .line 2108915
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;

    monitor-enter v1

    .line 2108916
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108917
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2108918
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108919
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2108920
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;LX/0Uh;)V

    .line 2108921
    move-object v0, v3

    .line 2108922
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2108923
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2108924
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2108925
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2108937
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2108882
    check-cast p2, LX/CzL;

    .line 2108883
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2108884
    check-cast v0, LX/8d9;

    .line 2108885
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v2, LX/EJI;

    sget-object v3, LX/3ap;->a:LX/1Ua;

    invoke-direct {v2, p2, v3}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108886
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-interface {v0}, LX/8d9;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108887
    iget-object v1, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 2108888
    invoke-static {v1}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 2108889
    invoke-static {v1}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->i:LX/0Uh;

    invoke-static {v1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2108890
    :cond_0
    const/4 v1, 0x0

    invoke-static {p2, v1}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v1

    .line 2108891
    :goto_0
    move-object v1, v1

    .line 2108892
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108893
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 2108894
    invoke-interface {v0}, LX/8d9;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    .line 2108895
    if-eqz v3, :cond_3

    invoke-static {v3}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v3

    :goto_1
    move-object v3, v3

    .line 2108896
    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108897
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->e:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-interface {v0}, LX/8d9;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LX/ELM;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108898
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108899
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108900
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 2108901
    iget-object v1, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 2108902
    invoke-static {v1}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 2108903
    invoke-static {v1}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;->i:LX/0Uh;

    invoke-static {v1}, LX/ELM;->a(LX/0Uh;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2108904
    :cond_1
    const/4 v1, 0x1

    invoke-static {p2, v1}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v1

    .line 2108905
    :goto_2
    move-object v1, v1

    .line 2108906
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2108907
    const/4 v0, 0x0

    return-object v0

    .line 2108908
    :cond_2
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2108909
    check-cast v1, LX/8d9;

    invoke-interface {v1}, LX/8d9;->k()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 2108910
    :cond_4
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2108911
    check-cast v1, LX/8d9;

    .line 2108912
    invoke-interface {v1}, LX/8d9;->j()Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;

    move-result-object p0

    .line 2108913
    if-eqz p0, :cond_5

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/entity/SearchResultsEventModels$SearchResultsEventModel$EventSocialContextModel;->a()Ljava/lang/String;

    move-result-object p0

    :goto_3
    move-object v1, p0

    .line 2108914
    goto :goto_2

    :cond_5
    const/4 p0, 0x0

    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1a4db72c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2108879
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2108880
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2108881
    const/16 v1, 0x1f

    const v2, -0x457258c4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2108878
    const/4 v0, 0x1

    return v0
.end method
