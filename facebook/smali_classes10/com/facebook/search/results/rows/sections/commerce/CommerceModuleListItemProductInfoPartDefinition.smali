.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A2T;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/11S;

.field public final b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field public final c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field public final d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;

.field public final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/11S;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105019
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2105020
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->a:LX/11S;

    .line 2105021
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2105022
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2105023
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;

    .line 2105024
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->e:LX/0ad;

    .line 2105025
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;
    .locals 9

    .prologue
    .line 2105026
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;

    monitor-enter v1

    .line 2105027
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105028
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105029
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105030
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105031
    new-instance v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v4

    check-cast v4, LX/11S;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;-><init>(LX/11S;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;LX/0ad;)V

    .line 2105032
    move-object v0, v3

    .line 2105033
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105034
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105035
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105036
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2105037
    check-cast p2, LX/CzL;

    .line 2105038
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2105039
    check-cast v0, LX/A2T;

    .line 2105040
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-interface {v0}, LX/A2T;->aJ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/A2T;->aJ()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SalePriceModel;->b()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2105041
    invoke-static {p2}, LX/EJf;->a(LX/CzL;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v1, v2, :cond_1

    invoke-interface {v0}, LX/A2T;->aL()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$SellerModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2105042
    :goto_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2105043
    const v0, 0x7f0d2ba0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemCallToActionPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2105044
    const/4 v0, 0x0

    return-object v0

    .line 2105045
    :cond_0
    invoke-interface {v0}, LX/A2T;->as()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ItemPriceModel;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2105046
    :cond_1
    iget-object v3, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v3, v3

    .line 2105047
    check-cast v3, LX/A2T;

    .line 2105048
    invoke-interface {v3}, LX/A2T;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->e()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$ToModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$ToModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2105049
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemProductInfoPartDefinition;->a:LX/11S;

    sget-object v6, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    invoke-interface {v3}, LX/A2T;->W()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    mul-long/2addr v7, v9

    invoke-interface {v5, v6, v7, v8}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v3

    .line 2105050
    new-instance v5, LX/3DI;

    const-string v6, " \u2022 "

    invoke-direct {v5, v6}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 2105051
    invoke-virtual {v5, v4}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2105052
    invoke-virtual {v5, v3}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2105053
    move-object v0, v5

    .line 2105054
    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x55e19aea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2105055
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2105056
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2105057
    const/16 v1, 0x1f

    const v2, -0x16114e41

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2105058
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2105059
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2105060
    return-void
.end method
