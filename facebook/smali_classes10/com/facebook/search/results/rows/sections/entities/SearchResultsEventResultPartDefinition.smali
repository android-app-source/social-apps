.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventResultPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/entity/SearchResultsEventResultInterfaces$SearchResultsEventResult;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108938
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2108939
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventResultPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;

    .line 2108940
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2108941
    sget-object v0, LX/3ap;->c:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2108942
    check-cast p2, LX/CzL;

    const/4 v2, 0x0

    .line 2108943
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2108944
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->b()LX/8d9;

    move-result-object v0

    .line 2108945
    if-nez v0, :cond_0

    .line 2108946
    :goto_0
    return-object v2

    .line 2108947
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventResultPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEventPartDefinition;

    invoke-virtual {p2, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2108948
    const/4 v0, 0x1

    return v0
.end method
