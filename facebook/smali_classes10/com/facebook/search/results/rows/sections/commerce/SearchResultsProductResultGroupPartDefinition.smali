.class public Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2105844
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2105845
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    .line 2105846
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    .line 2105847
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;
    .locals 5

    .prologue
    .line 2105824
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;

    monitor-enter v1

    .line 2105825
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105826
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105827
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105828
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105829
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;)V

    .line 2105830
    move-object v0, p0

    .line 2105831
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105832
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105833
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105834
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2105835
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    const/4 v4, 0x0

    .line 2105836
    move-object v0, p3

    check-cast v0, LX/CxP;

    invoke-interface {v0, p2}, LX/CxP;->b(LX/CzL;)I

    move-result v0

    if-nez v0, :cond_0

    .line 2105837
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    new-instance v1, LX/EMg;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0822c9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v1, v2, v4, v3}, LX/EMg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105838
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsProductResultGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    .line 2105839
    iget-object v1, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2105840
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 2105841
    invoke-static {v1, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105842
    return-object v4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2105843
    const/4 v0, 0x1

    return v0
.end method
