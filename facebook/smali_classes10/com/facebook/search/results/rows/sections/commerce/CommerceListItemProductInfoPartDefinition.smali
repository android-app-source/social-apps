.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/11S;

.field public final b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field public final c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field public final d:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;

.field public final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/11S;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104839
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2104840
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->a:LX/11S;

    .line 2104841
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2104842
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2104843
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;

    .line 2104844
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->e:LX/0ad;

    .line 2104845
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;
    .locals 9

    .prologue
    .line 2104846
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;

    monitor-enter v1

    .line 2104847
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104848
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104849
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104850
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104851
    new-instance v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v4

    check-cast v4, LX/11S;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;-><init>(LX/11S;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;LX/0ad;)V

    .line 2104852
    move-object v0, v3

    .line 2104853
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104854
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104855
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104856
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2104857
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2104858
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2104859
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 2104860
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2104861
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 2104862
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->hR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104863
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_1

    .line 2104864
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 2104865
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ie()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    .line 2104866
    :goto_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104867
    const v0, 0x7f0d2ba0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->d:Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemCallToActionPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2104868
    const/4 v0, 0x0

    return-object v0

    .line 2104869
    :cond_0
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v0, v0

    .line 2104870
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->eR()Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLCurrencyQuantity;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2104871
    :cond_1
    iget-object v2, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v2, v2

    .line 2104872
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->gh()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v2

    .line 2104873
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemProductInfoPartDefinition;->a:LX/11S;

    sget-object v4, LX/1lB;->STREAM_RELATIVE_STYLE:LX/1lB;

    .line 2104874
    iget-object v5, p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v5, v5

    .line 2104875
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-interface {v3, v4, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v3

    .line 2104876
    new-instance v4, LX/3DI;

    const-string v5, " \u2022 "

    invoke-direct {v4, v5}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 2104877
    invoke-virtual {v4, v2}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2104878
    invoke-virtual {v4, v3}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2104879
    move-object v0, v4

    .line 2104880
    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x734c463c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2104881
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2104882
    const/4 v1, 0x1

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2104883
    const/16 v1, 0x1f

    const v2, -0x40e60edd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2104884
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2104885
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2104886
    return-void
.end method
