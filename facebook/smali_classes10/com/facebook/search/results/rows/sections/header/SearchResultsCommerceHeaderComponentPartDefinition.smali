.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/EMg;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final g:LX/1X6;

.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/EMf;

.field private final e:LX/1vg;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2111167
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v2

    const/4 v3, 0x0

    .line 2111168
    iput v3, v2, LX/1UY;->b:F

    .line 2111169
    move-object v2, v2

    .line 2111170
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->g:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EMf;LX/1vg;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111127
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2111128
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->d:LX/EMf;

    .line 2111129
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->e:LX/1vg;

    .line 2111130
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->f:LX/1V0;

    .line 2111131
    return-void
.end method

.method private a(LX/1De;LX/EMg;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/EMg;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2111145
    iget-object v0, p2, LX/EMg;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->e:LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    const v1, 0x7f0202ac

    invoke-virtual {v0, v1}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    const v1, -0xb95800

    invoke-virtual {v0, v1}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 2111146
    :goto_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->d:LX/EMf;

    const/4 v2, 0x0

    .line 2111147
    new-instance v3, LX/EMe;

    invoke-direct {v3, v1}, LX/EMe;-><init>(LX/EMf;)V

    .line 2111148
    sget-object v4, LX/EMf;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EMd;

    .line 2111149
    if-nez v4, :cond_0

    .line 2111150
    new-instance v4, LX/EMd;

    invoke-direct {v4}, LX/EMd;-><init>()V

    .line 2111151
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/EMd;->a$redex0(LX/EMd;LX/1De;IILX/EMe;)V

    .line 2111152
    move-object v3, v4

    .line 2111153
    move-object v2, v3

    .line 2111154
    move-object v1, v2

    .line 2111155
    iget-object v2, p2, LX/EMg;->a:Ljava/lang/String;

    .line 2111156
    iget-object v3, v1, LX/EMd;->a:LX/EMe;

    iput-object v2, v3, LX/EMe;->a:Ljava/lang/String;

    .line 2111157
    iget-object v3, v1, LX/EMd;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2111158
    move-object v1, v1

    .line 2111159
    iget-object v2, p2, LX/EMg;->b:Ljava/lang/String;

    .line 2111160
    iget-object v3, v1, LX/EMd;->a:LX/EMe;

    iput-object v2, v3, LX/EMe;->b:Ljava/lang/String;

    .line 2111161
    move-object v1, v1

    .line 2111162
    iget-object v2, v1, LX/EMd;->a:LX/EMe;

    iput-object v0, v2, LX/EMe;->c:LX/1dc;

    .line 2111163
    move-object v0, v1

    .line 2111164
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2111165
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    sget-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->g:LX/1X6;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 2111166
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;
    .locals 7

    .prologue
    .line 2111134
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2111135
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111136
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111137
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111138
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111139
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EMf;->a(LX/0QB;)LX/EMf;

    move-result-object v4

    check-cast v4, LX/EMf;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v5

    check-cast v5, LX/1vg;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/EMf;LX/1vg;LX/1V0;)V

    .line 2111140
    move-object v0, p0

    .line 2111141
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111142
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111143
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111144
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2111171
    check-cast p2, LX/EMg;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->a(LX/1De;LX/EMg;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2111133
    check-cast p2, LX/EMg;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->a(LX/1De;LX/EMg;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111132
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2111126
    const/4 v0, 0x0

    return-object v0
.end method
