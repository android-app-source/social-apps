.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EL8;",
        "LX/EL7;",
        "LX/1Pn;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0wM;


# direct methods
.method public constructor <init>(LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107930
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2107931
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;->a:LX/0wM;

    .line 2107932
    return-void
.end method

.method public static a(LX/EL8;LX/1Pn;LX/0wM;)LX/EL7;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2107933
    invoke-interface {p1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2107934
    iget v0, p0, LX/EL8;->a:I

    if-eqz v0, :cond_1

    iget v0, p0, LX/EL8;->a:I

    iget v3, p0, LX/EL8;->b:I

    invoke-virtual {p2, v0, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2107935
    :goto_0
    iget v3, p0, LX/EL8;->d:I

    if-eqz v3, :cond_0

    iget v1, p0, LX/EL8;->d:I

    invoke-virtual {v2, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2107936
    :cond_0
    new-instance v2, LX/EL7;

    invoke-direct {v2, v0, v1}, LX/EL7;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V

    return-object v2

    :cond_1
    move-object v0, v1

    .line 2107937
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;
    .locals 4

    .prologue
    .line 2107938
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;

    monitor-enter v1

    .line 2107939
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107940
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107941
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107942
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107943
    new-instance p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;-><init>(LX/0wM;)V

    .line 2107944
    move-object v0, p0

    .line 2107945
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107946
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107947
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107948
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2107949
    check-cast p2, LX/EL8;

    check-cast p3, LX/1Pn;

    .line 2107950
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;->a:LX/0wM;

    invoke-static {p2, p3, v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;->a(LX/EL8;LX/1Pn;LX/0wM;)LX/EL7;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x68c28b52

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2107951
    check-cast p1, LX/EL8;

    check-cast p2, LX/EL7;

    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 2107952
    iget-object v1, p2, LX/EL7;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 2107953
    :goto_0
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2107954
    iget-object v1, p2, LX/EL7;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2107955
    iget-object v1, p1, LX/EL8;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2107956
    iget-object v1, p2, LX/EL7;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionContentDescription(Ljava/lang/CharSequence;)V

    .line 2107957
    const/16 v1, 0x1f

    const v2, 0x19d3bcf7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2107958
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2107959
    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 2107960
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2107961
    return-void
.end method
