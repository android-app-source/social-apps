.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;>;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field private static i:LX/0Xm;


# instance fields
.field private final b:LX/0Uh;

.field public final c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition",
            "<",
            "Ljava/lang/Object;",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/2dq;

.field public final g:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;

.field public final h:LX/CvY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2105330
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0xa7c5482

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-void
.end method

.method public constructor <init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;LX/CvY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105331
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2105332
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->b:LX/0Uh;

    .line 2105333
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;

    .line 2105334
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2105335
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    .line 2105336
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->f:LX/2dq;

    .line 2105337
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->g:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;

    .line 2105338
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->h:LX/CvY;

    .line 2105339
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;
    .locals 11

    .prologue
    .line 2105340
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    monitor-enter v1

    .line 2105341
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105342
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105343
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105344
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105345
    new-instance v3, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    invoke-static {v0}, LX/2dq;->b(LX/0QB;)LX/2dq;

    move-result-object v8

    check-cast v8, LX/2dq;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v10

    check-cast v10, LX/CvY;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;-><init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;LX/2dq;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselSeeMorePartDefinition;LX/CvY;)V

    .line 2105346
    move-object v0, v3

    .line 2105347
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105348
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105349
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105350
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2105351
    sget-object v0, LX/2eA;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2105352
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/Cxo;

    .line 2105353
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2105354
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2105355
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    sget-object v4, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v2, p2, v3, v4}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2105356
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->e:Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PersistentRecyclerPartDefinition;

    const/4 v7, 0x0

    .line 2105357
    new-instance v5, LX/2eG;

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->f:LX/2dq;

    const/high16 v8, 0x43200000    # 160.0f

    sget-object v9, LX/2eF;->a:LX/1Ua;

    const/4 v10, 0x1

    invoke-virtual {v6, v8, v9, v10}, LX/2dq;->a(FLX/1Ua;Z)LX/2eF;

    move-result-object v6

    .line 2105358
    new-instance v8, LX/EJp;

    invoke-direct {v8, p0, v0, p3}, LX/EJp;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/Cxo;)V

    move-object v8, v8

    .line 2105359
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v9

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105360
    iget-object v10, v9, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    move-object v9, v10

    .line 2105361
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v9

    move-object v10, v0

    invoke-direct/range {v5 .. v10}, LX/2eG;-><init>(LX/2eF;ILX/2eJ;Ljava/lang/String;LX/0jW;)V

    move-object v0, v5

    .line 2105362
    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2105363
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2105364
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2105365
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2105366
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2105367
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->b:LX/0Uh;

    sget v3, LX/2SU;->X:I

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2105368
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v2, v2

    .line 2105369
    invoke-virtual {v2}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->u()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->u()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductCarouselPartDefinition;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
