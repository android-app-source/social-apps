.class public Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "LX/EL3;",
        "TE;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final c:LX/17W;

.field public final d:LX/0wM;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107581
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2107582
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    .line 2107583
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2107584
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->c:LX/17W;

    .line 2107585
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->d:LX/0wM;

    .line 2107586
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;
    .locals 7

    .prologue
    .line 2107551
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;

    monitor-enter v1

    .line 2107552
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107553
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107554
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107555
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107556
    new-instance p0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v6

    check-cast v6, LX/0wM;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/17W;LX/0wM;)V

    .line 2107557
    move-object v0, p0

    .line 2107558
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107559
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107560
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107561
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2107574
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Pn;

    .line 2107575
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107576
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->d:LX/0wM;

    const v1, 0x7f0208f8

    const v2, -0x6e685d

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v0, v0

    .line 2107577
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08226a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2107578
    new-instance v2, LX/EL1;

    invoke-direct {v2, p0, p2, p3}, LX/EL1;-><init>(Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;)V

    move-object v2, v2

    .line 2107579
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/NavigationalLinkFigPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v3, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107580
    new-instance v3, LX/EL3;

    invoke-direct {v3, v0, v1, v2}, LX/EL3;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5de17816

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2107567
    check-cast p2, LX/EL3;

    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 2107568
    const/4 v1, 0x1

    .line 2107569
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2107570
    iget-object v1, p2, LX/EL3;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2107571
    iget-object v1, p2, LX/EL3;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2107572
    iget-object v1, p2, LX/EL3;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionContentDescription(Ljava/lang/CharSequence;)V

    .line 2107573
    const/16 v1, 0x1f

    const v2, 0x3ce936d8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2107562
    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    const/4 v0, 0x0

    .line 2107563
    invoke-virtual {p4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2107564
    invoke-virtual {p4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2107565
    invoke-virtual {p4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setActionContentDescription(Ljava/lang/CharSequence;)V

    .line 2107566
    return-void
.end method
