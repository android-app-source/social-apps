.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/CharSequence;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/EMt;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/EMt;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2111528
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2111529
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;->d:LX/EMt;

    .line 2111530
    return-void
.end method

.method private a(LX/1De;Ljava/lang/CharSequence;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/CharSequence;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2111531
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;->d:LX/EMt;

    const/4 v1, 0x0

    .line 2111532
    new-instance v2, LX/EMs;

    invoke-direct {v2, v0}, LX/EMs;-><init>(LX/EMt;)V

    .line 2111533
    sget-object p0, LX/EMt;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EMr;

    .line 2111534
    if-nez p0, :cond_0

    .line 2111535
    new-instance p0, LX/EMr;

    invoke-direct {p0}, LX/EMr;-><init>()V

    .line 2111536
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/EMr;->a$redex0(LX/EMr;LX/1De;IILX/EMs;)V

    .line 2111537
    move-object v2, p0

    .line 2111538
    move-object v1, v2

    .line 2111539
    move-object v0, v1

    .line 2111540
    iget-object v1, v0, LX/EMr;->a:LX/EMs;

    iput-object p2, v1, LX/EMs;->a:Ljava/lang/CharSequence;

    .line 2111541
    iget-object v1, v0, LX/EMr;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2111542
    move-object v0, v0

    .line 2111543
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2111544
    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;
    .locals 5

    .prologue
    .line 2111545
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    monitor-enter v1

    .line 2111546
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111547
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111548
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111549
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111550
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EMt;->a(LX/0QB;)LX/EMt;

    move-result-object v4

    check-cast v4, LX/EMt;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/EMt;)V

    .line 2111551
    move-object v0, p0

    .line 2111552
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111553
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111554
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111555
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2111556
    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;->a(LX/1De;Ljava/lang/CharSequence;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2111557
    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2}, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;->a(LX/1De;Ljava/lang/CharSequence;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111558
    check-cast p1, Ljava/lang/CharSequence;

    .line 2111559
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2111560
    const/4 v0, 0x0

    return-object v0
.end method
