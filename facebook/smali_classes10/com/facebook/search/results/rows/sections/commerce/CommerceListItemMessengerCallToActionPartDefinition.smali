.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxG;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        "Landroid/graphics/drawable/Drawable;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104812
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2104813
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2104814
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->b:LX/0Ot;

    .line 2104815
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->c:LX/0Ot;

    .line 2104816
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;
    .locals 6

    .prologue
    .line 2104825
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    monitor-enter v1

    .line 2104826
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104827
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104828
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104829
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104830
    new-instance v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    const/16 v5, 0x2eb

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x32d4

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, v5, p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0Ot;LX/0Ot;)V

    .line 2104831
    move-object v0, v4

    .line 2104832
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104833
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104834
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2104836
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    check-cast p3, LX/1Pn;

    .line 2104837
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EJe;

    invoke-direct {v1, p0, p2, p3}, LX/EJe;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceListItemMessengerCallToActionPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Pn;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2104838
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02092d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x38898250

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2104821
    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    .line 2104822
    invoke-virtual {p4, p2, v1, v1, v1}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2104823
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0822be

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2104824
    const/16 v1, 0x1f

    const v2, -0xba8e04

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2104817
    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    const/4 v0, 0x0

    .line 2104818
    invoke-virtual {p4, v0, v0, v0, v0}, Lcom/facebook/widget/text/BetterTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2104819
    invoke-virtual {p4, v0}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2104820
    return-void
.end method
