.class public Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EJI;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103784
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2103785
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 2103786
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .locals 4

    .prologue
    .line 2103787
    const-class v1, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    monitor-enter v1

    .line 2103788
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103789
    sput-object v2, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103790
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103791
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103792
    new-instance p0, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;)V

    .line 2103793
    move-object v0, p0

    .line 2103794
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103795
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103796
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103797
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2103798
    check-cast p2, LX/EJI;

    check-cast p3, LX/1Pn;

    const/4 v4, 0x0

    .line 2103799
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p2, LX/EJI;->b:LX/1Ua;

    iget-object v3, p2, LX/EJI;->a:LX/CzL;

    check-cast p3, LX/1Ps;

    .line 2103800
    invoke-interface {p3}, LX/1Ps;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-virtual {v3}, LX/CzL;->j()Z

    move-result p0

    if-nez p0, :cond_1

    :cond_0
    sget-object p0, LX/1X9;->MIDDLE:LX/1X9;

    :goto_0
    move-object v3, p0

    .line 2103801
    invoke-direct {v1, v4, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2103802
    return-object v4

    :cond_1
    sget-object p0, LX/1X9;->BOTTOM:LX/1X9;

    goto :goto_0
.end method
