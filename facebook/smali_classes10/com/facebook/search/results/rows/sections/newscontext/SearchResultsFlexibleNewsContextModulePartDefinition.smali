.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModulePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "LX/8dK;",
        ">;",
        "Ljava/lang/Void;",
        "LX/Cxk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113001
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2113002
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModulePartDefinition;->b:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;

    .line 2113003
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;

    .line 2113004
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModulePartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;

    .line 2113005
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModulePartDefinition;->d:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;

    .line 2113006
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2113011
    check-cast p2, LX/CzL;

    const/4 v2, 0x0

    .line 2113012
    iget-object v0, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2113013
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    .line 2113014
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    move-object v1, v0

    .line 2113015
    :goto_0
    iget-object v0, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2113016
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2113017
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModulePartDefinition;->d:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextVideoPartDefinition;

    invoke-static {p1, v2, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModulePartDefinition;->b:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextPhotoPartDefinition;

    .line 2113018
    iget-object v4, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v4, v4

    .line 2113019
    invoke-static {v1, v4}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2113020
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextTitlePartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113021
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsFlexibleNewsContextModulePartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;

    .line 2113022
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 2113023
    invoke-static {v0, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113024
    const/4 v0, 0x0

    return-object v0

    .line 2113025
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2113007
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 2113008
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2113009
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v2

    .line 2113010
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8dD;

    invoke-interface {v0}, LX/8dD;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8dD;

    invoke-interface {v0}, LX/8dD;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->a()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8dD;

    invoke-interface {v0}, LX/8dD;->e()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextModuleModels$SearchResultsFlexibleContextModuleModel$ModuleResultsModel$EdgesModel$MetadataModel;->b()Lcom/facebook/search/results/protocol/SearchResultsFlexibleContextMetadataModels$SearchResultsFlexibleContextMetadataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
