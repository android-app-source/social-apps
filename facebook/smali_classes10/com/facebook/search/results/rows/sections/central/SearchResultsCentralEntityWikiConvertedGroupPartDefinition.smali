.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleInterfaces$SearchResultsWikiModule;",
        ">;",
        "Ljava/lang/Void;",
        "LX/Cxk;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0jW;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2103939
    new-instance v0, LX/EJQ;

    invoke-direct {v0}, LX/EJQ;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->a:LX/0jW;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103969
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2103970
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;

    .line 2103971
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;

    .line 2103972
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;

    .line 2103973
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;
    .locals 6

    .prologue
    .line 2103958
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;

    monitor-enter v1

    .line 2103959
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103960
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103961
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103962
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103963
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;)V

    .line 2103964
    move-object v0, p0

    .line 2103965
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103966
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103967
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103968
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2103942
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxk;

    const/4 v4, 0x0

    .line 2103943
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2103944
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->cd()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2103945
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->v()LX/A5I;

    move-result-object v1

    invoke-static {v1}, LX/EJT;->a(LX/A5I;)LX/1KL;

    move-result-object v1

    sget-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->a:LX/0jW;

    invoke-interface {p3, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EJT;

    .line 2103946
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->v()LX/A5I;

    move-result-object v2

    invoke-interface {v2}, LX/A5I;->S()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->v()LX/A5I;

    move-result-object v2

    invoke-interface {v2}, LX/A5I;->S()Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/wiki/SearchResultsWikiModuleModels$SearchResultsWikiModulePageModel$BestDescriptionModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x96

    if-gt v2, v3, :cond_1

    .line 2103947
    :cond_0
    iput-boolean v4, v1, LX/EJT;->b:Z

    .line 2103948
    iput-boolean v4, v1, LX/EJT;->a:Z

    .line 2103949
    :cond_1
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiTitleConvertedPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->v()LX/A5I;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2103950
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2103951
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->cd()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->v()LX/A5I;

    move-result-object v0

    .line 2103952
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 2103953
    invoke-static {v0, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v0

    .line 2103954
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;

    invoke-virtual {p1, v2, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2103955
    invoke-virtual {v1}, LX/EJT;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2103956
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiConvertedGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiReadMoreConvertedPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2103957
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2103940
    const/4 v0, 0x1

    move v0, v0

    .line 2103941
    return v0
.end method
