.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Uh;

.field private final b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;


# direct methods
.method public constructor <init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105502
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2105503
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;->a:LX/0Uh;

    .line 2105504
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    .line 2105505
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;
    .locals 5

    .prologue
    .line 2105506
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;

    monitor-enter v1

    .line 2105507
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105508
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105509
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105510
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105511
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;-><init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;)V

    .line 2105512
    move-object v0, p0

    .line 2105513
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105514
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105515
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105516
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2105517
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    const/4 v8, 0x6

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2105518
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2105519
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    move v1, v3

    .line 2105520
    :goto_0
    if-ge v4, v6, :cond_0

    if-ge v1, v8, :cond_0

    .line 2105521
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Landroid/net/Uri;

    move-result-object v0

    sget-object v7, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    if-eq v0, v7, :cond_3

    .line 2105522
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2105523
    add-int/lit8 v0, v1, 0x1

    .line 2105524
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    .line 2105525
    :cond_0
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2105526
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v8, :cond_1

    move v0, v2

    :goto_2
    const-string v4, "Not enough grid view items"

    invoke-static {v0, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2105527
    const-string v0, "Number of grid view items must be exactly divisible by number of items in row"

    invoke-static {v2, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2105528
    :goto_3
    const/4 v0, 0x2

    if-ge v3, v0, :cond_2

    .line 2105529
    mul-int/lit8 v0, v3, 0x3

    .line 2105530
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsTopTabGridRowPartDefinition;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v4, v5, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105531
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_1
    move v0, v3

    .line 2105532
    goto :goto_2

    .line 2105533
    :cond_2
    const/4 v0, 0x0

    return-object v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2105534
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    const/4 v0, 0x0

    .line 2105535
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemsGridGroupPartDefinition;->a:LX/0Uh;

    sget v2, LX/2SU;->X:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v3, 0x0

    .line 2105536
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v5

    move v2, v3

    move v4, v3

    .line 2105537
    :goto_0
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 2105538
    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    .line 2105539
    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/commerce/CommerceThumbnailPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;)Landroid/net/Uri;

    move-result-object v1

    sget-object p0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    if-eq v1, p0, :cond_0

    .line 2105540
    add-int/lit8 v4, v4, 0x1

    .line 2105541
    :cond_0
    const/4 v1, 0x6

    if-lt v4, v1, :cond_3

    .line 2105542
    const/4 v3, 0x1

    .line 2105543
    :cond_1
    move v1, v3

    .line 2105544
    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0

    .line 2105545
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method
