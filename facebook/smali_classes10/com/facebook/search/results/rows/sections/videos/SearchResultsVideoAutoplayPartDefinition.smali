.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EPh;",
        "Ljava/lang/Void;",
        "LX/Cxj;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/EPe;

.field public final b:LX/7zd;


# direct methods
.method public constructor <init>(LX/EPe;LX/7zd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117424
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2117425
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;->a:LX/EPe;

    .line 2117426
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;->b:LX/7zd;

    .line 2117427
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;
    .locals 5

    .prologue
    .line 2117413
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;

    monitor-enter v1

    .line 2117414
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117415
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117416
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117417
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117418
    new-instance p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;

    invoke-static {v0}, LX/EPe;->a(LX/0QB;)LX/EPe;

    move-result-object v3

    check-cast v3, LX/EPe;

    const-class v4, LX/7zd;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/7zd;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;-><init>(LX/EPe;LX/7zd;)V

    .line 2117419
    move-object v0, p0

    .line 2117420
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117421
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117422
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117423
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 11

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0xa2016c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2117404
    check-cast p1, LX/EPh;

    check-cast p3, LX/Cxj;

    check-cast p4, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    .line 2117405
    iget-object v4, p1, LX/EPh;->a:LX/CzL;

    invoke-interface {p3, v4}, LX/Cxj;->d(LX/CzL;)LX/CyM;

    move-result-object v6

    .line 2117406
    iget-object v4, v6, LX/CyM;->c:LX/2oV;

    move-object v4, v4

    .line 2117407
    if-nez v4, :cond_0

    .line 2117408
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;->b:LX/7zd;

    iget-object v5, p1, LX/EPh;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v7, p1, LX/EPh;->c:LX/093;

    iget-object v8, p1, LX/EPh;->d:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v9, p1, LX/EPh;->e:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    iget-object v10, p1, LX/EPh;->f:LX/04D;

    invoke-virtual/range {v4 .. v10}, LX/7zd;->a(Ljava/lang/String;LX/2oM;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;)LX/7zc;

    move-result-object v4

    .line 2117409
    iput-object v4, v6, LX/CyM;->c:LX/2oV;

    .line 2117410
    :cond_0
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoAutoplayPartDefinition;->a:LX/EPe;

    .line 2117411
    iget-object v6, v5, LX/EPe;->a:LX/1Aa;

    invoke-virtual {v6, p4, v4}, LX/1Aa;->a(Landroid/view/View;LX/2oV;)V

    .line 2117412
    const/16 v1, 0x1f

    const v2, 0x2773d633

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
