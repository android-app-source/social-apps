.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115561
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2115562
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryPartDefinition;

    .line 2115563
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;

    .line 2115564
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    .line 2115565
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterPartDefinition;

    .line 2115566
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    .line 2115567
    return-void
.end method

.method private static a(Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;)LX/A3T;
    .locals 10
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2115537
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2115538
    :cond_0
    const/4 v0, 0x0

    .line 2115539
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/A4K;

    invoke-direct {v0}, LX/A4K;-><init>()V

    .line 2115540
    iget-object v4, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v4

    move-wide v2, v4

    .line 2115541
    iput-wide v2, v0, LX/A4K;->b:J

    .line 2115542
    move-object v0, v0

    .line 2115543
    new-instance v1, LX/A4R;

    invoke-direct {v1}, LX/A4R;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 2115544
    iput-object v2, v1, LX/A4R;->a:Ljava/lang/String;

    .line 2115545
    move-object v1, v1

    .line 2115546
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2115547
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2115548
    iget-object v5, v1, LX/A4R;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2115549
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 2115550
    invoke-virtual {v4, v7, v5}, LX/186;->b(II)V

    .line 2115551
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2115552
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2115553
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2115554
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2115555
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2115556
    new-instance v5, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;

    invoke-direct {v5, v4}, Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;-><init>(LX/15i;)V

    .line 2115557
    move-object v1, v5

    .line 2115558
    iput-object v1, v0, LX/A4K;->i:Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel$SourceModel;

    .line 2115559
    move-object v0, v0

    .line 2115560
    invoke-virtual {v0}, LX/A4K;->a()Lcom/facebook/search/results/protocol/pulse/SearchResultsArticleExternalUrlModels$SearchResultsArticleExternalUrlModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;
    .locals 9

    .prologue
    .line 2115516
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;

    monitor-enter v1

    .line 2115517
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115518
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115519
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115520
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115521
    new-instance v3, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;)V

    .line 2115522
    move-object v0, v3

    .line 2115523
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115524
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115525
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115526
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2115528
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2115529
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2115530
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    .line 2115531
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderPhotoPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115532
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/PulseContextHeaderTextPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115533
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSummaryPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115534
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;)LX/A3T;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115535
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115536
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115527
    const/4 v0, 0x1

    return v0
.end method
