.class public Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "LX/8d6;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:LX/1Kf;

.field public final c:LX/CvY;

.field private final d:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;


# direct methods
.method public constructor <init>(LX/1Kf;Landroid/app/Activity;LX/CvY;Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113364
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2113365
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->b:LX/1Kf;

    .line 2113366
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->a:Landroid/app/Activity;

    .line 2113367
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->c:LX/CvY;

    .line 2113368
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    .line 2113369
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->e:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    .line 2113370
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2113379
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2113375
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pg;

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 2113376
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->e:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    new-instance v2, LX/ENl;

    invoke-direct {v2, p0, p3, p2}, LX/ENl;-><init>(Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;LX/1Pg;LX/CzL;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2113377
    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;->d:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    new-instance v0, LX/21P;

    const/4 v3, 0x1

    sget-object v4, LX/1Wi;->PAGE:LX/1Wi;

    move v2, v1

    invoke-direct/range {v0 .. v5}, LX/21P;-><init>(ZZZLX/1Wi;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2113378
    return-object v5
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5789ba6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2113372
    check-cast p4, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;

    .line 2113373
    sget-object v1, LX/20X;->SHARE:LX/20X;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->setButtons(Ljava/util/Set;)V

    .line 2113374
    const/16 v1, 0x1f

    const v2, -0x337b24fb    # -6.9654568E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2113371
    const/4 v0, 0x1

    return v0
.end method
