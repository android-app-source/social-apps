.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxj;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/video/SearchResultsFeedVideosModuleInterfaces$SearchResultsFeedVideosModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117296
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2117297
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    .line 2117298
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    .line 2117299
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2117300
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;
    .locals 6

    .prologue
    .line 2117312
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;

    monitor-enter v1

    .line 2117313
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117314
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117315
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117316
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117317
    new-instance p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V

    .line 2117318
    move-object v0, p0

    .line 2117319
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117320
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117321
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117322
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2117303
    check-cast p2, LX/CzL;

    .line 2117304
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    .line 2117305
    iget-object v1, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 2117306
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117307
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117308
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117309
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2117310
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsFeedVideosModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117311
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2117301
    check-cast p1, LX/CzL;

    .line 2117302
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
