.class public Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8dL;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EK7;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPA;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EJ5;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/EK7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EPA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EJ5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115976
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2115977
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->d:LX/0Ot;

    .line 2115978
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->e:LX/0Ot;

    .line 2115979
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->f:LX/0Ot;

    .line 2115980
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->g:LX/0Ot;

    .line 2115981
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->h:LX/0ad;

    .line 2115982
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<+",
            "LX/8dL;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2115956
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2115957
    check-cast v0, LX/8dL;

    .line 2115958
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EK7;

    invoke-virtual {v1, p1}, LX/EK7;->c(LX/1De;)LX/EK5;

    move-result-object v1

    .line 2115959
    invoke-interface {v0}, LX/8dL;->bS()LX/A4E;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/8dL;->bS()LX/A4E;

    move-result-object v2

    invoke-interface {v2}, LX/A4E;->b()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v2

    if-nez v2, :cond_5

    .line 2115960
    :cond_0
    const/4 v2, 0x0

    .line 2115961
    :goto_0
    move-object v2, v2

    .line 2115962
    invoke-virtual {v1, v2}, LX/EK5;->c(Ljava/lang/String;)LX/EK5;

    move-result-object v1

    invoke-interface {v0}, LX/8dL;->bQ()LX/8dJ;

    move-result-object v2

    invoke-interface {v2}, LX/8dJ;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/EK5;->a(Ljava/lang/CharSequence;)LX/EK5;

    move-result-object v2

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->a(LX/8dL;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, LX/8dL;->an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;->c()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;->b()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v2, v1}, LX/EK5;->b(Ljava/lang/CharSequence;)LX/EK5;

    move-result-object v1

    .line 2115963
    invoke-interface {v0}, LX/8dL;->ad()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, LX/8dL;->ad()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;->b()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, LX/8dL;->ad()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;->b()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel$ProfilePictureModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->h:LX/0ad;

    sget-short v3, LX/100;->W:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2115964
    invoke-interface {v0}, LX/8dL;->ad()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;->b()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel$ProfilePictureModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2115965
    :goto_2
    move-object v2, v2

    .line 2115966
    invoke-virtual {v1, v2}, LX/EK5;->b(Ljava/lang/String;)LX/EK5;

    move-result-object v1

    .line 2115967
    invoke-interface {v0}, LX/8dL;->ad()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, LX/8dL;->ad()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;->b()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 2115968
    invoke-interface {v0}, LX/8dL;->ad()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel;->b()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$ExternalUrlOwningProfileModel$ProfilePictureModel;->a()I

    move-result v2

    .line 2115969
    :goto_3
    move v2, v2

    .line 2115970
    invoke-virtual {v1, v2}, LX/EK5;->h(I)LX/EK5;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EJ5;

    invoke-interface {v0}, LX/8dL;->W()J

    move-result-wide v4

    invoke-interface {v0}, LX/8dL;->bT()LX/8d4;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, LX/8dL;->bT()LX/8d4;

    move-result-object v2

    invoke-interface {v2}, LX/8d4;->a()I

    move-result v2

    :goto_4
    invoke-virtual {v1, v4, v5, v2, p1}, LX/EJ5;->a(JILandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/EK5;->c(Ljava/lang/CharSequence;)LX/EK5;

    move-result-object v1

    new-instance v2, LX/EP7;

    invoke-direct {v2, p0, p3, p2, v0}, LX/EP7;-><init>(Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;LX/1Pn;LX/CzL;LX/8dL;)V

    invoke-virtual {v1, v2}, LX/EK5;->a(Landroid/view/View$OnClickListener;)LX/EK5;

    move-result-object v1

    const v2, 0x7f0208ca

    invoke-virtual {v1, v2}, LX/EK5;->n(I)LX/EK5;

    move-result-object v2

    .line 2115971
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EPA;

    check-cast p3, LX/1Ps;

    invoke-virtual {v1, p3, v2}, LX/EPA;->a(LX/1Ps;LX/EK5;)V

    .line 2115972
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->a(LX/8dL;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2115973
    const v0, 0x7f02077d

    invoke-virtual {v2, v0}, LX/EK5;->i(I)LX/EK5;

    .line 2115974
    :cond_1
    invoke-virtual {v2}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 2115975
    :cond_2
    invoke-interface {v0}, LX/8dL;->bR()LX/8dI;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, LX/8dL;->bR()LX/8dI;

    move-result-object v1

    invoke-interface {v1}, LX/8dI;->a()Ljava/lang/String;

    move-result-object v1

    :goto_5
    invoke-static {v1}, LX/EJ5;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_5

    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    :cond_5
    invoke-interface {v0}, LX/8dL;->bS()LX/A4E;

    move-result-object v2

    invoke-interface {v2}, LX/A4E;->b()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;
    .locals 10

    .prologue
    .line 2115945
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;

    monitor-enter v1

    .line 2115946
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115947
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115948
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115949
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115950
    new-instance v3, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0x33bc

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3480

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x337d

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x32d4

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;)V

    .line 2115951
    move-object v0, v3

    .line 2115952
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115953
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115954
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115955
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/8dL;)Z
    .locals 1

    .prologue
    .line 2115942
    invoke-interface {p0}, LX/8dL;->an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/8dL;->an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;->c()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/8dL;->an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;->c()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;->b()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/8dL;->an()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel;->c()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel;->b()Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsRelatedLinkModels$SearchResultsRelatedLinkModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2115943
    const/4 v0, 0x1

    .line 2115944
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2115933
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2115941
    check-cast p2, LX/CzL;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedLinkPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115936
    check-cast p1, LX/CzL;

    .line 2115937
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2115938
    if-eqz v0, :cond_0

    .line 2115939
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2115940
    check-cast v0, LX/8dL;

    invoke-interface {v0}, LX/8dL;->bQ()LX/8dJ;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2115935
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 2115934
    sget-object v0, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method
