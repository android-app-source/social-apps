.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxG;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/EJZ;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static p:LX/0Xm;


# instance fields
.field public final d:LX/0ad;

.field private final e:LX/ELF;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/1nG;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2SY;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EQ6;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0N;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/lang/String;

.field public final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/0ad;LX/ELF;LX/0Ot;LX/1nG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Ot;)V
    .locals 0
    .param p12    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/ELF;",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;",
            "LX/1nG;",
            "LX/0Ot",
            "<",
            "LX/2SY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EQ6;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D0N;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2109317
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2109318
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->d:LX/0ad;

    .line 2109319
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->e:LX/ELF;

    .line 2109320
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->f:LX/0Ot;

    .line 2109321
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->g:LX/1nG;

    .line 2109322
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->h:LX/0Ot;

    .line 2109323
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->i:LX/0Ot;

    .line 2109324
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->j:LX/0Ot;

    .line 2109325
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->k:LX/0Ot;

    .line 2109326
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->l:LX/0Ot;

    .line 2109327
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->m:LX/0Ot;

    .line 2109328
    iput-object p12, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->n:Ljava/lang/String;

    .line 2109329
    iput-object p13, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->o:LX/0Ot;

    .line 2109330
    return-void
.end method

.method private a(LX/1De;LX/EJZ;LX/1Pn;)LX/1X1;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/EJZ;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2109296
    new-instance v0, LX/ELX;

    invoke-direct {v0, p0, p2, p3}, LX/ELX;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;LX/EJZ;LX/1Pn;)V

    .line 2109297
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->e:LX/ELF;

    invoke-virtual {v1, p1}, LX/ELF;->c(LX/1De;)LX/ELD;

    move-result-object v1

    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v2}, LX/ELM;->c(Lcom/facebook/graphql/model/GraphQLNode;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/ELD;->a(Landroid/net/Uri;)LX/ELD;

    move-result-object v1

    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v2

    iget-object v3, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->f:LX/0Ot;

    invoke-static {v2, v3, v4}, LX/ELM;->a(ZLjava/lang/String;LX/0Ot;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/ELD;->a(Ljava/lang/CharSequence;)LX/ELD;

    move-result-object v1

    invoke-static {p2, v7}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/ELD;->b(Ljava/lang/CharSequence;)LX/ELD;

    move-result-object v1

    invoke-static {p2, v8}, LX/ELM;->a(LX/EJZ;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/ELD;->c(Ljava/lang/CharSequence;)LX/ELD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/ELD;->c(Landroid/view/View$OnClickListener;)LX/ELD;

    move-result-object v1

    .line 2109298
    iget-object v2, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->d:LX/0ad;

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0N;

    invoke-static {v2, p3, v3, v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;LX/0ad;LX/D0N;)LX/EL8;

    move-result-object v2

    .line 2109299
    iget v0, v2, LX/EL8;->a:I

    if-eqz v0, :cond_0

    .line 2109300
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    iget v3, v2, LX/EL8;->a:I

    invoke-virtual {v0, v3}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    iget v3, v2, LX/EL8;->b:I

    invoke-virtual {v0, v3}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ELD;->a(LX/1n6;)LX/ELD;

    .line 2109301
    iget-object v0, v2, LX/EL8;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, LX/ELD;->d(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2109302
    :cond_0
    new-instance v2, LX/ELY;

    invoke-direct {v2, p0, p2, p3}, LX/ELY;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;LX/EJZ;LX/1Pn;)V

    .line 2109303
    new-instance v3, LX/ELZ;

    invoke-direct {v3, p0, p2, p3}, LX/ELZ;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;LX/EJZ;LX/1Pn;)V

    .line 2109304
    iget-object v0, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v4, 0x285feb

    if-ne v0, v4, :cond_2

    iget-object v0, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    move-object v0, p3

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {v4, v5, v6, v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0W9;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->d:LX/0ad;

    sget-short v4, LX/100;->c:S

    invoke-interface {v0, v4, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2109305
    invoke-virtual {v1, v8}, LX/ELD;->a(Z)LX/ELD;

    .line 2109306
    const v0, 0x7f0822c5

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ELD;->d(Ljava/lang/CharSequence;)LX/ELD;

    .line 2109307
    invoke-virtual {v1, v2}, LX/ELD;->a(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2109308
    const v0, 0x7f0822c6

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ELD;->e(Ljava/lang/CharSequence;)LX/ELD;

    .line 2109309
    invoke-virtual {v1, v3}, LX/ELD;->b(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2109310
    :cond_1
    :goto_0
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 2109311
    :cond_2
    iget-object v0, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v2, 0x25d6af

    if-ne v0, v2, :cond_1

    iget-object v0, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p2, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v3

    move-object v0, p3

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {v2, v3, v4, v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(Ljava/lang/String;ZLcom/facebook/search/results/model/SearchResultsMutableContext;LX/0W9;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->d:LX/0ad;

    sget-short v2, LX/100;->b:S

    invoke-interface {v0, v2, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2109312
    invoke-virtual {v1, v8}, LX/ELD;->a(Z)LX/ELD;

    .line 2109313
    const v0, 0x7f0822c5

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ELD;->d(Ljava/lang/CharSequence;)LX/ELD;

    .line 2109314
    const-string v0, "photos"

    sget-object v2, LX/7CM;->INLINE_PHOTOS_LINK:LX/7CM;

    invoke-direct {p0, p2, v0, p3, v2}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(LX/EJZ;Ljava/lang/String;LX/1Pn;LX/7CM;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ELD;->a(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2109315
    const v0, 0x7f0822c7

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ELD;->e(Ljava/lang/CharSequence;)LX/ELD;

    .line 2109316
    const-string v0, "events"

    sget-object v2, LX/7CM;->INLINE_EVENTS_LINK:LX/7CM;

    invoke-direct {p0, p2, v0, p3, v2}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(LX/EJZ;Ljava/lang/String;LX/1Pn;LX/7CM;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/ELD;->b(Landroid/view/View$OnClickListener;)LX/ELD;

    goto :goto_0
.end method

.method private a(LX/EJZ;Ljava/lang/String;LX/1Pn;LX/7CM;)Landroid/view/View$OnClickListener;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EJZ;",
            "Ljava/lang/String;",
            "TE;",
            "LX/7CM;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2109295
    new-instance v0, LX/ELa;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/ELa;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;LX/EJZ;Ljava/lang/String;LX/1Pn;LX/7CM;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;
    .locals 3

    .prologue
    .line 2109287
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    monitor-enter v1

    .line 2109288
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109289
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109290
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109291
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->b(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109292
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109293
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109294
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0W9;)Z
    .locals 3

    .prologue
    .line 2109278
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ZLcom/facebook/search/results/model/SearchResultsMutableContext;LX/0W9;)Z
    .locals 3

    .prologue
    .line 2109286
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;
    .locals 14

    .prologue
    .line 2109284
    new-instance v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0}, LX/ELF;->a(LX/0QB;)LX/ELF;

    move-result-object v3

    check-cast v3, LX/ELF;

    const/16 v4, 0x3512

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v5

    check-cast v5, LX/1nG;

    const/16 v6, 0x11b8

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x34d5

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x2eb

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x34ae

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x3b2

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2be

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {p0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    const/16 v13, 0x32d4

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;-><init>(Landroid/content/Context;LX/0ad;LX/ELF;LX/0Ot;LX/1nG;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Ot;)V

    .line 2109285
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2109283
    check-cast p2, LX/EJZ;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(LX/1De;LX/EJZ;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2109282
    check-cast p2, LX/EJZ;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(LX/1De;LX/EJZ;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2109280
    check-cast p1, LX/EJZ;

    const/4 v0, 0x0

    .line 2109281
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->d:LX/0ad;

    sget-short v2, LX/100;->h:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v1}, LX/ELM;->d(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v1

    const v2, 0x285feb

    if-eq v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->d:LX/0ad;

    sget-short v2, LX/100;->e:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, LX/EJZ;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v1}, LX/ELM;->d(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v1

    const v2, 0x25d6af

    if-ne v1, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2109279
    const/4 v0, 0x0

    return-object v0
.end method
