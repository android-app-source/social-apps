.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/Cxh;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/7j6;

.field public final c:LX/0hy;

.field public final d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105425
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2105426
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2105427
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->b:LX/7j6;

    .line 2105428
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->c:LX/0hy;

    .line 2105429
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2105430
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;
    .locals 7

    .prologue
    .line 2105431
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    monitor-enter v1

    .line 2105432
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105433
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105434
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105435
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105436
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/7j6;->a(LX/0QB;)LX/7j6;

    move-result-object v4

    check-cast v4, LX/7j6;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v5

    check-cast v5, LX/0hy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v6

    check-cast v6, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/7j6;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V

    .line 2105437
    move-object v0, p0

    .line 2105438
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105439
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105440
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105441
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2105442
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    check-cast p3, LX/1Pn;

    .line 2105443
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EJq;

    invoke-direct {v1, p0, p2, p3}, LX/EJq;-><init>(Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemClickPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;LX/1Pn;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2105444
    const/4 v0, 0x0

    return-object v0
.end method
