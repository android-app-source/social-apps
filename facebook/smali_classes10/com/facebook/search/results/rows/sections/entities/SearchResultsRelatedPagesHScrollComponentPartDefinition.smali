.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/entities/SearchResultsPageModuleInterfaces$SearchResultsPageModule;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/EM7;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/EM7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110230
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2110231
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;->d:LX/EM7;

    .line 2110232
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/CxA;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/entities/SearchResultsPageModuleInterfaces$SearchResultsPageModule;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2110233
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;->d:LX/EM7;

    const/4 v1, 0x0

    .line 2110234
    new-instance v2, LX/EM6;

    invoke-direct {v2, v0}, LX/EM6;-><init>(LX/EM7;)V

    .line 2110235
    iget-object p0, v0, LX/EM7;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EM5;

    .line 2110236
    if-nez p0, :cond_0

    .line 2110237
    new-instance p0, LX/EM5;

    invoke-direct {p0, v0}, LX/EM5;-><init>(LX/EM7;)V

    .line 2110238
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/EM5;->a$redex0(LX/EM5;LX/1De;IILX/EM6;)V

    .line 2110239
    move-object v2, p0

    .line 2110240
    move-object v1, v2

    .line 2110241
    move-object v0, v1

    .line 2110242
    iget-object v1, v0, LX/EM5;->a:LX/EM6;

    iput-object p3, v1, LX/EM6;->a:LX/CxA;

    .line 2110243
    iget-object v1, v0, LX/EM5;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2110244
    move-object v0, v0

    .line 2110245
    iget-object v1, v0, LX/EM5;->a:LX/EM6;

    iput-object p2, v1, LX/EM6;->b:LX/CzL;

    .line 2110246
    iget-object v1, v0, LX/EM5;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2110247
    move-object v0, v0

    .line 2110248
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;
    .locals 5

    .prologue
    .line 2110249
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;

    monitor-enter v1

    .line 2110250
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110251
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110252
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110253
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110254
    new-instance p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EM7;->a(LX/0QB;)LX/EM7;

    move-result-object v4

    check-cast v4, LX/EM7;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;-><init>(Landroid/content/Context;LX/EM7;)V

    .line 2110255
    move-object v0, p0

    .line 2110256
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110257
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110258
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110259
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2110260
    check-cast p2, LX/CzL;

    check-cast p3, LX/CxA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;->a(LX/1De;LX/CzL;LX/CxA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2110261
    check-cast p2, LX/CzL;

    check-cast p3, LX/CxA;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPagesHScrollComponentPartDefinition;->a(LX/1De;LX/CzL;LX/CxA;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2110262
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2110263
    const/4 v0, 0x0

    return-object v0
.end method
