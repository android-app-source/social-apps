.class public Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103548
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2103549
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;

    .line 2103550
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    .line 2103551
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    .line 2103552
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;
    .locals 6

    .prologue
    .line 2103553
    const-class v1, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;

    monitor-enter v1

    .line 2103554
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103555
    sput-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103556
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103557
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103558
    new-instance p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;)V

    .line 2103559
    move-object v0, p0

    .line 2103560
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103561
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103562
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103563
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2103564
    check-cast p2, LX/CzL;

    .line 2103565
    const/4 v0, 0x0

    invoke-static {p2, v0}, LX/CzM;->a(LX/CzL;I)LX/CzL;

    move-result-object v0

    .line 2103566
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;->a(LX/CzL;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;->a(LX/CzL;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2103567
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/answer/SearchResultsSimpleCoverPhotoPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2103568
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherTextPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2103569
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/answer/SearchResultsWeatherForecastHScrollPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2103570
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2103571
    check-cast p1, LX/CzL;

    .line 2103572
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
