.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d8;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

.field private final f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition",
            "<TE;",
            "LX/8d8;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107850
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2107851
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 2107852
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2107853
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2107854
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 2107855
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->e:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2107856
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    .line 2107857
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->g:LX/0Uh;

    .line 2107858
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;
    .locals 11

    .prologue
    .line 2107839
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;

    monitor-enter v1

    .line 2107840
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107841
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107842
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107843
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107844
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;LX/0Uh;)V

    .line 2107845
    move-object v0, v3

    .line 2107846
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107847
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107848
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107849
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/CzL;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d8;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2107828
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->g:LX/0Uh;

    invoke-static {v0}, LX/ELM;->a(LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107829
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v0

    .line 2107830
    :goto_0
    return-object v0

    .line 2107831
    :cond_0
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2107832
    check-cast v0, LX/8d8;

    .line 2107833
    new-instance v3, LX/3DI;

    const-string v1, " \u2022 "

    invoke-direct {v3, v1}, LX/3DI;-><init>(Ljava/lang/CharSequence;)V

    .line 2107834
    invoke-interface {v0}, LX/8d8;->c()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p1

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, p1, :cond_1

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2107835
    invoke-virtual {v3, v1}, LX/3DI;->a(Ljava/lang/CharSequence;)LX/3DI;

    .line 2107836
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2107837
    :cond_1
    move-object v0, v3

    .line 2107838
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2107859
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2107810
    check-cast p2, LX/CzL;

    .line 2107811
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v1, LX/EJI;

    sget-object v2, LX/3ap;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107812
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2107813
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2107814
    check-cast v0, LX/8d8;

    invoke-interface {v0}, LX/8d8;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107815
    invoke-direct {p0, p2}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->a(LX/CzL;)Ljava/lang/String;

    move-result-object v1

    .line 2107816
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107817
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 2107818
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2107819
    check-cast v0, LX/8d8;

    .line 2107820
    invoke-interface {v0}, LX/8d8;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object p3

    .line 2107821
    if-eqz p3, :cond_0

    invoke-static {p3}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object p3

    :goto_0
    move-object v0, p3

    .line 2107822
    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107823
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->e:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2107824
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2107825
    check-cast v0, LX/8d8;

    invoke-interface {v0}, LX/8d8;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LX/ELM;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107826
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107827
    const/4 v0, 0x0

    return-object v0

    :cond_0
    const/4 p3, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x34a509e0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2107807
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2107808
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2107809
    const/16 v1, 0x1f

    const v2, -0x3c7c7cd2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2107806
    const/4 v0, 0x1

    return v0
.end method
