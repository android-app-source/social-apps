.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<*>;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field public final a:LX/0Uh;

.field public final b:LX/0ad;

.field public final c:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;

.field public final d:Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;

.field public final e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;

.field public final f:Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;

.field private final h:LX/2Sc;

.field public final i:Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

.field public final j:Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0W9;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0ad;Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;LX/2Sc;Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;LX/0W9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;",
            "LX/2Sc;",
            "Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacesMapPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/local/SearchResultsPlacePartDefinition;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;",
            "LX/0W9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104483
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104484
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->a:LX/0Uh;

    .line 2104485
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->b:LX/0ad;

    .line 2104486
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;

    .line 2104487
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;

    .line 2104488
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;

    .line 2104489
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->f:Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;

    .line 2104490
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->h:LX/2Sc;

    .line 2104491
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->i:Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    .line 2104492
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->j:Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

    .line 2104493
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->k:LX/0Ot;

    .line 2104494
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->l:LX/0Ot;

    .line 2104495
    iput-object p12, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->g:Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;

    .line 2104496
    iput-object p13, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->m:LX/0W9;

    .line 2104497
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;
    .locals 3

    .prologue
    .line 2104500
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;

    monitor-enter v1

    .line 2104501
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104502
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104503
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104504
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104505
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104506
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104507
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;
    .locals 14

    .prologue
    .line 2104498
    new-instance v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;

    invoke-static {p0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v7

    check-cast v7, LX/2Sc;

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

    const/16 v10, 0x1182

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1181

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v13

    check-cast v13, LX/0W9;

    invoke-direct/range {v0 .. v13}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;-><init>(LX/0Uh;LX/0ad;Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;LX/2Sc;Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;LX/0W9;)V

    .line 2104499
    return-object v0
.end method

.method public static b(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2104482
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_MODULE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2104481
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SPORT_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2104508
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_SHARES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PIVOT:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2104480
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->RELATED_TOPICS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2104479
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->POSTS_CONTENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2104388
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    check-cast p3, LX/Cxo;

    .line 2104389
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->d(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v4

    .line 2104390
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->b(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v5

    .line 2104391
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->c(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v6

    .line 2104392
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->e(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v7

    .line 2104393
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->f(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v8

    .line 2104394
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_c

    .line 2104395
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 2104396
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2104397
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 2104398
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    .line 2104399
    if-nez v1, :cond_0

    .line 2104400
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->GRAMMAR:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v2, :cond_d

    .line 2104401
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 2104402
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2104403
    iget-object v0, p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 2104404
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v2, :cond_d

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2104405
    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    move v3, v0

    .line 2104406
    :goto_2
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->a:LX/0Uh;

    const/16 v1, 0x23d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2104407
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2104408
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    :goto_3
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 2104409
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2104410
    if-eqz v4, :cond_4

    .line 2104411
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->g:Lcom/facebook/search/results/rows/sections/pulse/OldSearchResultsRelatedSharesArticlePartDefinition;

    invoke-static {p1, v1, v0}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v9, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->f:Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;

    invoke-virtual {v1, v9, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2104412
    :cond_2
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2104413
    :cond_3
    const/4 v0, 0x0

    move v3, v0

    goto :goto_2

    .line 2104414
    :cond_4
    if-eqz v5, :cond_5

    .line 2104415
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_4

    .line 2104416
    :cond_5
    if-eqz v6, :cond_6

    .line 2104417
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_4

    .line 2104418
    :cond_6
    if-eqz v7, :cond_7

    .line 2104419
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->i:Lcom/facebook/search/results/rows/sections/pulsetopic/PulseTopicRelatedTopicsPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_4

    .line 2104420
    :cond_7
    if-eqz v8, :cond_8

    .line 2104421
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->j:Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_4

    .line 2104422
    :cond_8
    if-eqz v3, :cond_9

    .line 2104423
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->l:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_4

    .line 2104424
    :cond_9
    iget-object v9, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/collection/SearchResultsNodesItemPartDefinition;

    new-instance v10, LX/EJZ;

    .line 2104425
    iget-object v1, p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->o:LX/0P1;

    move-object v1, v1

    .line 2104426
    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    invoke-direct {v10, v0, v1, p2}, LX/EJZ;-><init>(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)V

    invoke-virtual {p1, v9, v10}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2104427
    invoke-static {v0}, LX/ELM;->d(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v1

    const v9, 0x285feb

    if-ne v1, v9, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v9

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v10

    iget-object v11, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->m:LX/0W9;

    invoke-static {v1, v9, v10, v11}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0W9;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->b:LX/0ad;

    sget-short v9, LX/100;->g:S

    const/4 v10, 0x0

    invoke-interface {v1, v9, v10}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_b

    .line 2104428
    :cond_a
    invoke-static {v0}, LX/ELM;->d(Lcom/facebook/graphql/model/GraphQLNode;)I

    move-result v1

    const v9, 0x25d6af

    if-ne v1, v9, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v0

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    iget-object v10, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->m:LX/0W9;

    invoke-static {v1, v0, v9, v10}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(Ljava/lang/String;ZLcom/facebook/search/results/model/SearchResultsMutableContext;LX/0W9;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->b:LX/0ad;

    sget-short v1, LX/100;->f:S

    const/4 v9, 0x0

    invoke-interface {v0, v1, v9}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2104429
    :cond_b
    const/4 v0, 0x0

    move-object v0, v0

    .line 2104430
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 2104432
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->d(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v5

    .line 2104433
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->b(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v6

    .line 2104434
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->c(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v7

    .line 2104435
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->e(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v8

    .line 2104436
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->f(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v9

    .line 2104437
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 2104438
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2104439
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 2104440
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_0
    move v10, v0

    .line 2104441
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 2104442
    :goto_1
    return v0

    .line 2104443
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v4, v2

    .line 2104444
    :goto_2
    if-ge v4, v12, :cond_8

    invoke-virtual {v11, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2104445
    if-eqz v5, :cond_1

    .line 2104446
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/OldPulseRelatedLinksItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v3

    .line 2104447
    goto :goto_1

    .line 2104448
    :cond_1
    if-eqz v6, :cond_2

    .line 2104449
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v3

    .line 2104450
    goto :goto_1

    .line 2104451
    :cond_2
    if-eqz v7, :cond_3

    .line 2104452
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsArticlesItemPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v3

    .line 2104453
    goto :goto_1

    .line 2104454
    :cond_3
    if-eqz v8, :cond_4

    .line 2104455
    const/4 v0, 0x1

    move v0, v0

    .line 2104456
    if-eqz v0, :cond_7

    move v0, v3

    .line 2104457
    goto :goto_1

    .line 2104458
    :cond_4
    if-eqz v9, :cond_5

    .line 2104459
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsPostsContentsPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v3

    .line 2104460
    goto :goto_1

    .line 2104461
    :cond_5
    if-eqz v10, :cond_6

    .line 2104462
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2104463
    const/4 v0, 0x1

    move v0, v0

    .line 2104464
    if-eqz v0, :cond_7

    move v0, v3

    .line 2104465
    goto :goto_1

    .line 2104466
    :cond_6
    new-instance v13, LX/EJZ;

    .line 2104467
    iget-object v1, p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->o:LX/0P1;

    move-object v1, v1

    .line 2104468
    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    invoke-direct {v13, v0, v1, p1}, LX/EJZ;-><init>(Lcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)V

    .line 2104469
    const/4 v0, 0x1

    move v0, v0

    .line 2104470
    if-eqz v0, :cond_7

    move v0, v3

    .line 2104471
    goto :goto_1

    .line 2104472
    :cond_7
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 2104473
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2104474
    iget-object v0, p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, v0

    .line 2104475
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2104476
    invoke-virtual {p1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2104477
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->h:LX/2Sc;

    sget-object v4, LX/3Ql;->MISSING_RESULT_DATA:LX/3Ql;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Display Style: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " Role: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    :cond_9
    move v0, v2

    .line 2104478
    goto/16 :goto_1

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104431
    check-cast p1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {p0, p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v0

    return v0
.end method
