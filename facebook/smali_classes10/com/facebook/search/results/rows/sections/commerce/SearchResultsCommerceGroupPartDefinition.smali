.class public Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
        ">;>;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105686
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2105687
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    .line 2105688
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;

    .line 2105689
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;

    .line 2105690
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    .line 2105691
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;
    .locals 7

    .prologue
    .line 2105692
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;

    monitor-enter v1

    .line 2105693
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105694
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105695
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105696
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105697
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;)V

    .line 2105698
    move-object v0, p0

    .line 2105699
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105700
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105701
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105702
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2105703
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/Cxo;

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2105704
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2105705
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2105706
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v1, v1

    .line 2105707
    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2105708
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v1, v3, :cond_2

    move v3, v4

    .line 2105709
    :goto_0
    if-nez v3, :cond_3

    invoke-interface {p3, v0}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v6

    if-nez v6, :cond_3

    .line 2105710
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceTitleTogglePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105711
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceItemSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105712
    if-eqz v3, :cond_1

    .line 2105713
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105714
    :cond_1
    return-object v2

    :cond_2
    move v3, v5

    .line 2105715
    goto :goto_0

    .line 2105716
    :cond_3
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v1, v6, :cond_4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v1, v6, :cond_0

    invoke-interface {p3, v0}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_0

    .line 2105717
    :cond_4
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->k:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->k:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_2
    move v1, v1

    .line 2105718
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 2105719
    new-instance v7, LX/EMg;

    .line 2105720
    iget-object v8, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    move-object v0, v8

    .line 2105721
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-lez v1, :cond_5

    if-eqz v3, :cond_5

    const v8, 0x7f0f010f

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v4, v5

    invoke-virtual {v6, v8, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v7, v0, v1, v4}, LX/EMg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2105722
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move-object v1, v2

    .line 2105723
    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2105724
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2105725
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2105726
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    .line 2105727
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2105728
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    .line 2105729
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2105730
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
