.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<TT;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106213
    sget-object v0, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->b:LX/1Cz;

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->a:LX/1Cz;

    .line 2106214
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 2106215
    iput v1, v0, LX/1UY;->b:F

    .line 2106216
    move-object v0, v0

    .line 2106217
    const/high16 v1, -0x3f800000    # -4.0f

    .line 2106218
    iput v1, v0, LX/1UY;->c:F

    .line 2106219
    move-object v0, v0

    .line 2106220
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106221
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2106222
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2106223
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;
    .locals 4

    .prologue
    .line 2106224
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    monitor-enter v1

    .line 2106225
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106226
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106227
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106228
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106229
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2106230
    move-object v0, p0

    .line 2106231
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106232
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106233
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106234
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2106235
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2106236
    const/4 v4, 0x0

    .line 2106237
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->BOTTOM:LX/1X9;

    invoke-direct {v1, v4, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2106238
    return-object v4
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 2106239
    const/4 v0, 0x1

    return v0
.end method
