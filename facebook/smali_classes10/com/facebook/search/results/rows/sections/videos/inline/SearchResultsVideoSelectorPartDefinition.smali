.class public Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A4u;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2118010
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2118011
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;->a:LX/0Ot;

    .line 2118012
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;->b:LX/0Ot;

    .line 2118013
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;
    .locals 5

    .prologue
    .line 2118014
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;

    monitor-enter v1

    .line 2118015
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2118016
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2118017
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2118018
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2118019
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;

    const/16 v4, 0x34a7

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x11a8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2118020
    move-object v0, v3

    .line 2118021
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2118022
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2118023
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2118024
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2118025
    check-cast p2, LX/CzL;

    .line 2118026
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    invoke-virtual {v1, v0, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2118027
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2118028
    check-cast p1, LX/CzL;

    .line 2118029
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2118030
    check-cast v0, LX/A4u;

    .line 2118031
    invoke-interface {v0}, LX/A4u;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2118032
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2118033
    :goto_0
    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2118034
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2118035
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
