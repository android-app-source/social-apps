.class public Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteContentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteContentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115239
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2115240
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteContentPartDefinition;

    .line 2115241
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;
    .locals 4

    .prologue
    .line 2115228
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;

    monitor-enter v1

    .line 2115229
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115230
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115231
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115232
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115233
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteContentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteContentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteContentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteContentPartDefinition;)V

    .line 2115234
    move-object v0, p0

    .line 2115235
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115236
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115237
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115238
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2115224
    check-cast p2, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    .line 2115225
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteRowPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuoteContentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115226
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115227
    const/4 v0, 0x1

    return v0
.end method
