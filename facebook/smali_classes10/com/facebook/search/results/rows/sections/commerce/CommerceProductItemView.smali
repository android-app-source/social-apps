.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private b:Lcom/facebook/fbui/widget/contentview/ContentView;

.field private c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2105497
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2105498
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->a()V

    .line 2105499
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2105494
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2105495
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->a()V

    .line 2105496
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2105491
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2105492
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->a()V

    .line 2105493
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2105485
    const v0, 0x7f031287

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2105486
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2105487
    const v0, 0x7f0d0cbc

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2105488
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const v1, 0x7f020c36

    invoke-virtual {v0, v1}, LX/1af;->b(I)V

    .line 2105489
    const v0, 0x7f0d2b70

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2105490
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2105500
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2105501
    return-void
.end method

.method public setNearbyIndicatorEnabled(Z)V
    .locals 2

    .prologue
    .line 2105475
    if-eqz p1, :cond_0

    .line 2105476
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2105477
    :goto_0
    return-void

    .line 2105478
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setProductName(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2105479
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2105480
    return-void
.end method

.method public setProductPrice(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2105481
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2105482
    return-void
.end method

.method public setProductSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2105483
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemView;->b:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2105484
    return-void
.end method
