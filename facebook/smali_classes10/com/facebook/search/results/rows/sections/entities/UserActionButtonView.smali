.class public Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2111025
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2111026
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->b()V

    .line 2111027
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2111022
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2111023
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->b()V

    .line 2111024
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2111019
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2111020
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->b()V

    .line 2111021
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2111014
    const v0, 0x7f0312c0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2111015
    const v0, 0x7f0d2baa

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2111016
    const v0, 0x7f0d2bab

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2111017
    const v0, 0x7f0d03cc

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2111018
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 2111009
    invoke-virtual {p0, v1}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyphSize(I)V

    .line 2111010
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setText(Ljava/lang/CharSequence;)V

    .line 2111011
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 2111012
    new-instance v0, LX/6VC;

    invoke-direct {v0, v1, v1}, LX/6VC;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2111013
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 2111028
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2111029
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2111030
    return-void
.end method

.method public setColor(I)V
    .locals 1

    .prologue
    .line 2111005
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2111006
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2111007
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 2111008
    return-void
.end method

.method public setGlyph(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 2111003
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2111004
    return-void
.end method

.method public setGlyphSize(I)V
    .locals 2

    .prologue
    .line 2111001
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, p1, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2111002
    return-void
.end method

.method public setPhotoCount(I)V
    .locals 2

    .prologue
    .line 2110996
    if-nez p1, :cond_0

    .line 2110997
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->b:Lcom/facebook/widget/text/BetterTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2110998
    :goto_0
    return-void

    .line 2110999
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2111000
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2110993
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2110994
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2110995
    return-void
.end method
