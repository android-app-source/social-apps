.class public Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1X6;",
        "Ljava/lang/Void;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103755
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2103756
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2103757
    return-void
.end method

.method public static a(LX/1Pn;LX/1X6;)LX/1X6;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, -0x1

    .line 2103758
    iget v0, p1, LX/1X6;->c:I

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    const-string v4, "innerBackgroundResourceId is already defined"

    invoke-static {v0, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2103759
    iget v0, p1, LX/1X6;->d:I

    if-ne v0, v3, :cond_0

    move v2, v1

    :cond_0
    const-string v0, "touchSelectorResourceId is already defined"

    invoke-static {v2, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2103760
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 2103761
    invoke-interface {p0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v4, 0x7f0106ce

    invoke-virtual {v2, v4, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 2103762
    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    if-lez v1, :cond_1

    .line 2103763
    iget v3, v0, Landroid/util/TypedValue;->resourceId:I

    .line 2103764
    :cond_1
    new-instance v0, LX/1X6;

    iget-object v1, p1, LX/1X6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p1, LX/1X6;->b:LX/1Ua;

    iget-object v5, p1, LX/1X6;->e:LX/1X9;

    move v4, v3

    invoke-direct/range {v0 .. v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;IILX/1X9;)V

    return-object v0

    :cond_2
    move v0, v2

    .line 2103765
    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;
    .locals 4

    .prologue
    .line 2103766
    const-class v1, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    monitor-enter v1

    .line 2103767
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103768
    sput-object v2, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103769
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103770
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103771
    new-instance p0, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2103772
    move-object v0, p0

    .line 2103773
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103774
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103775
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103776
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2103777
    check-cast p2, LX/1X6;

    check-cast p3, LX/1Ps;

    .line 2103778
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    check-cast p3, LX/1Pn;

    invoke-static {p3, p2}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/1Pn;LX/1X6;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2103779
    const/4 v0, 0x0

    return-object v0
.end method
