.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<TT;>;>;",
        "Ljava/lang/Void;",
        "LX/1Ps;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;

.field private b:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104318
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104319
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;

    .line 2104320
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    .line 2104321
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;
    .locals 5

    .prologue
    .line 2104298
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;

    monitor-enter v1

    .line 2104299
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104300
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104301
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104302
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104303
    new-instance p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;)V

    .line 2104304
    move-object v0, p0

    .line 2104305
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104306
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104307
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104308
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2104309
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104310
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104311
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2104312
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitleActionPartDefinition;

    invoke-static {p1, v1, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionTitlePartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    new-instance v4, LX/EN0;

    .line 2104313
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    move-object v1, v1

    .line 2104314
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v5

    .line 2104315
    iget-object p0, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v0, p0

    .line 2104316
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v4, v1, v5, v0}, LX/EN0;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    invoke-virtual {v2, v3, v4}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2104317
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    .line 2104288
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2104289
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104290
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2104291
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NEWS_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v3, v4, :cond_1

    .line 2104292
    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    move-object v3, v3

    .line 2104293
    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    .line 2104294
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 2104295
    goto :goto_0

    .line 2104296
    :cond_1
    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    move-object v0, v3

    .line 2104297
    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method
