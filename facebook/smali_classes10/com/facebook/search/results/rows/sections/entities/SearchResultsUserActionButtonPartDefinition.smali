.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d1;",
        ">;",
        "LX/EL8;",
        "TE;",
        "Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static p:LX/0Xm;


# instance fields
.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hZ;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final k:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

.field public final l:LX/23P;

.field public m:Z

.field public n:Z

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2110619
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v1, 0x7f020886

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v3, 0x7f0208a0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v5, 0x7f020889

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->a:LX/0P1;

    .line 2110620
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v1, 0x7f080f7b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v3, 0x7f080f7e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v5, 0x7f080f83

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->b:LX/0P1;

    .line 2110621
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v1, 0x7f080f7c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v3, 0x7f080f7e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v5, 0x7f080f83

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->c:LX/0P1;

    .line 2110622
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v1, 0x7f082263

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v3, 0x7f082264

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const v5, 0x7f082265

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->d:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;LX/0ad;LX/23P;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2hZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;",
            "LX/0ad;",
            "LX/23P;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2110700
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2110701
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->e:LX/0Ot;

    .line 2110702
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->f:LX/0Ot;

    .line 2110703
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->g:LX/0Ot;

    .line 2110704
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->h:LX/0Ot;

    .line 2110705
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->i:LX/0Ot;

    .line 2110706
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->j:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2110707
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->k:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2110708
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->l:LX/23P;

    .line 2110709
    sget-short v0, LX/100;->aq:S

    invoke-interface {p8, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->m:Z

    .line 2110710
    sget-short v0, LX/100;->ap:S

    invoke-interface {p8, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->n:Z

    .line 2110711
    sget-short v0, LX/100;->ar:S

    invoke-interface {p8, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->o:Z

    .line 2110712
    return-void
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Landroid/view/View$OnClickListener;)LX/EL8;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2110695
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->a:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Integer;

    .line 2110696
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->b:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 2110697
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->c:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 2110698
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->d:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/Integer;

    .line 2110699
    new-instance v0, LX/EL8;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_1
    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/EL8;-><init>(IILandroid/view/View$OnClickListener;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;
    .locals 13

    .prologue
    .line 2110684
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    monitor-enter v1

    .line 2110685
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110686
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110687
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110688
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110689
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;

    const/16 v4, 0x32d4

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x12b1

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa7a

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xa71

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x113f

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v12

    check-cast v12, LX/23P;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;LX/0ad;LX/23P;)V

    .line 2110690
    move-object v0, v3

    .line 2110691
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110692
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110693
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110694
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;LX/CzL;LX/CzL;Ljava/lang/Throwable;LX/CxA;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;",
            "Ljava/lang/Throwable;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2110713
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hZ;

    .line 2110714
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2110715
    check-cast v1, LX/8d1;

    invoke-interface {v1}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v1

    .line 2110716
    new-instance v2, LX/EMQ;

    move-object v3, p0

    move-object v4, p4

    move-object v5, p1

    move-object v6, p2

    move-object v7, v1

    invoke-direct/range {v2 .. v7}, LX/EMQ;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;LX/CxA;LX/CzL;LX/CzL;Ljava/lang/String;)V

    move-object v1, v2

    .line 2110717
    invoke-virtual {v0, p3, v1}, LX/2hZ;->a(Ljava/lang/Throwable;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2110718
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    sget-object v1, LX/3Ql;->FAILED_MUTATION:LX/3Ql;

    invoke-virtual {v0, v1, p3}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2110719
    invoke-interface {p4, p2}, LX/CxA;->a(LX/CzL;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110720
    invoke-interface {p4, p2, p1}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2110721
    check-cast p4, LX/1Pq;

    invoke-interface {p4}, LX/1Pq;->iN_()V

    .line 2110722
    :cond_0
    return-void
.end method

.method public static b(LX/CzL;)LX/CzL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;)",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2110661
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2110662
    check-cast v0, LX/8d1;

    .line 2110663
    invoke-interface {v0}, LX/8d1;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2110664
    sget-object v2, LX/CzN;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2110665
    if-nez v1, :cond_0

    .line 2110666
    :goto_0
    return-object p0

    .line 2110667
    :cond_0
    instance-of v2, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    if-eqz v2, :cond_1

    .line 2110668
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2110669
    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2110670
    invoke-static {v0}, LX/8dX;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)LX/8dX;

    move-result-object v0

    .line 2110671
    iput-object v1, v0, LX/8dX;->F:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2110672
    move-object v0, v0

    .line 2110673
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2110674
    invoke-virtual {p0, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object p0

    goto :goto_0

    .line 2110675
    :cond_1
    instance-of v2, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    if-eqz v2, :cond_2

    .line 2110676
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2110677
    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 2110678
    invoke-static {v0}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v0

    .line 2110679
    iput-object v1, v0, LX/8dQ;->C:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2110680
    move-object v0, v0

    .line 2110681
    invoke-virtual {v0}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 2110682
    invoke-virtual {p0, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object p0

    goto :goto_0

    .line 2110683
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2110649
    check-cast p2, LX/CzL;

    check-cast p3, LX/CxA;

    const/4 v1, 0x0

    .line 2110650
    sget-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->d:LX/0P1;

    .line 2110651
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2110652
    check-cast v0, LX/8d1;

    invoke-interface {v0}, LX/8d1;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2110653
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2110654
    :goto_0
    return-object v0

    .line 2110655
    :cond_0
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->j:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2110656
    new-instance v3, LX/EMN;

    invoke-direct {v3, p0, p2, p3}, LX/EMN;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;LX/CzL;LX/CxA;)V

    move-object v3, v3

    .line 2110657
    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2110658
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->k:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2110659
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2110660
    check-cast v0, LX/8d1;

    invoke-interface {v0}, LX/8d1;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Landroid/view/View$OnClickListener;)LX/EL8;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4b98bc6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2110627
    check-cast p1, LX/CzL;

    check-cast p2, LX/EL8;

    check-cast p3, LX/CxA;

    check-cast p4, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    const/4 v7, 0x0

    const/4 v6, -0x2

    .line 2110628
    iget-boolean v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->m:Z

    if-eqz v1, :cond_4

    if-eqz p2, :cond_4

    iget-object v1, p2, LX/EL8;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 2110629
    iget-object v1, p2, LX/EL8;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->o:Z

    if-eqz v1, :cond_1

    iget-object v1, p2, LX/EL8;->f:Ljava/lang/Integer;

    move-object v2, v1

    :goto_0
    move-object v1, p3

    .line 2110630
    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0a029b

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 2110631
    iget-boolean v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->n:Z

    if-eqz v1, :cond_2

    .line 2110632
    iget v1, p2, LX/EL8;->a:I

    invoke-virtual {p4, v1, v4}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a(II)V

    move-object v1, p3

    .line 2110633
    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v5, 0x7f0b1727

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyphSize(I)V

    .line 2110634
    :goto_1
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->l:LX/23P;

    move-object v1, p3

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1, v7}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, p3

    .line 2110635
    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020b0b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p4, v1}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2110636
    invoke-virtual {p4, v4}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setColor(I)V

    .line 2110637
    new-instance v2, LX/6VC;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-boolean v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->o:Z

    if-eqz v1, :cond_3

    const v1, 0x7f0b174a

    :goto_2
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {v2, v1, v6}, LX/6VC;-><init>(II)V

    invoke-virtual {p4, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2110638
    const/16 v1, 0x11

    invoke-virtual {p4, v1}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGravity(I)V

    .line 2110639
    :cond_0
    :goto_3
    const/16 v1, 0x1f

    const v2, 0x5c5934ac

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2110640
    :cond_1
    iget-object v1, p2, LX/EL8;->e:Ljava/lang/Integer;

    move-object v2, v1

    goto/16 :goto_0

    .line 2110641
    :cond_2
    invoke-virtual {p4, v7}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 2110642
    :cond_3
    const v1, 0x7f0b1749

    goto :goto_2

    .line 2110643
    :cond_4
    sget-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->a:LX/0P1;

    .line 2110644
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2110645
    check-cast v1, LX/8d1;

    invoke-interface {v1}, LX/8d1;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2110646
    if-eqz v1, :cond_0

    .line 2110647
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v2, -0x6f6b64

    invoke-virtual {p4, v1, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a(II)V

    .line 2110648
    invoke-virtual {p4, v6}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyphSize(I)V

    goto :goto_3
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2110623
    check-cast p4, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    .line 2110624
    const/4 v0, 0x0

    invoke-static {p4, v0}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2110625
    invoke-virtual {p4}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a()V

    .line 2110626
    return-void
.end method
