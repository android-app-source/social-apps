.class public Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxh;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsBlendedPhotoPublicModuleInterfaces$SearchResultsBlendedPhotoPublicModule;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1X6;

.field private static h:LX/0Xm;


# instance fields
.field private final e:LX/EOQ;

.field private final f:LX/1V0;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2114550
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EOQ;LX/1V0;LX/0Or;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/EOQ;",
            "LX/1V0;",
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2114551
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2114552
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->e:LX/EOQ;

    .line 2114553
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->f:LX/1V0;

    .line 2114554
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->g:LX/0Or;

    .line 2114555
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsBlendedPhotoPublicModuleInterfaces$SearchResultsBlendedPhotoPublicModule;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2114556
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->e:LX/EOQ;

    const/4 v1, 0x0

    .line 2114557
    new-instance v2, LX/EOP;

    invoke-direct {v2, v0}, LX/EOP;-><init>(LX/EOQ;)V

    .line 2114558
    sget-object v3, LX/EOQ;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EOO;

    .line 2114559
    if-nez v3, :cond_0

    .line 2114560
    new-instance v3, LX/EOO;

    invoke-direct {v3}, LX/EOO;-><init>()V

    .line 2114561
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EOO;->a$redex0(LX/EOO;LX/1De;IILX/EOP;)V

    .line 2114562
    move-object v2, v3

    .line 2114563
    move-object v1, v2

    .line 2114564
    move-object v0, v1

    .line 2114565
    const/4 v1, 0x3

    .line 2114566
    iget-object v2, v0, LX/EOO;->a:LX/EOP;

    iput v1, v2, LX/EOP;->c:I

    .line 2114567
    iget-object v2, v0, LX/EOO;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2114568
    move-object v0, v0

    .line 2114569
    iget-object v1, v0, LX/EOO;->a:LX/EOP;

    iput-object p2, v1, LX/EOP;->a:LX/CzL;

    .line 2114570
    iget-object v1, v0, LX/EOO;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2114571
    move-object v0, v0

    .line 2114572
    new-instance v1, LX/EOT;

    invoke-direct {v1, p0, p2, p3}, LX/EOT;-><init>(Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;LX/CzL;LX/1Pn;)V

    move-object v1, v1

    .line 2114573
    iget-object v2, v0, LX/EOO;->a:LX/EOP;

    iput-object v1, v2, LX/EOP;->b:LX/EOS;

    .line 2114574
    move-object v0, v0

    .line 2114575
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2114576
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    sget-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->d:LX/1X6;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;
    .locals 7

    .prologue
    .line 2114577
    const-class v1, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;

    monitor-enter v1

    .line 2114578
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114579
    sput-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114580
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114581
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114582
    new-instance v6, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EOQ;->a(LX/0QB;)LX/EOQ;

    move-result-object v4

    check-cast v4, LX/EOQ;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    const/16 p0, 0xf2f

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;-><init>(Landroid/content/Context;LX/EOQ;LX/1V0;LX/0Or;)V

    .line 2114583
    move-object v0, v6

    .line 2114584
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114585
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114586
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114587
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2114588
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2114589
    check-cast p2, LX/CzL;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2114590
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2114591
    const/4 v0, 0x0

    return-object v0
.end method
