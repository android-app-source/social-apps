.class public Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        ">;",
        "LX/ENL;",
        "TE;",
        "Lcom/facebook/maps/FbMapViewDelegate;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112499
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2112500
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->a:LX/0Ot;

    .line 2112501
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->b:LX/0Ot;

    .line 2112502
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->c:LX/0Ot;

    .line 2112503
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->d:LX/0Ot;

    .line 2112504
    return-void
.end method

.method public static a()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2112496
    const-string v0, "Places"

    .line 2112497
    const-string v1, "places"

    .line 2112498
    new-instance v2, LX/CyH;

    const-string v3, "places_set_search"

    invoke-direct {v2, v3, v0, v1}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;LX/1Pn;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Ljava/util/List;LX/697;)LX/6Zz;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;",
            "LX/697;",
            ")",
            "LX/6Zz;"
        }
    .end annotation

    .prologue
    .line 2112460
    new-instance v0, LX/ENK;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/ENK;-><init>(Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;LX/1Pn;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Ljava/util/List;LX/697;)V

    return-object v0
.end method

.method private a(LX/1aD;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/1Pn;)LX/ENL;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;TE;)",
            "LX/ENL;"
        }
    .end annotation

    .prologue
    .line 2112471
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v2

    .line 2112472
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 2112473
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2112474
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    .line 2112475
    if-eqz v0, :cond_0

    .line 2112476
    new-instance v6, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v10

    invoke-direct {v6, v8, v9, v10, v11}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2112477
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2112478
    invoke-virtual {v2, v6}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2112479
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2112480
    :cond_1
    invoke-virtual {v2}, LX/696;->a()LX/697;

    move-result-object v0

    .line 2112481
    invoke-static {p0, p3, p2, v3, v0}, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->a(Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;LX/1Pn;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Ljava/util/List;LX/697;)LX/6Zz;

    move-result-object v1

    .line 2112482
    invoke-static {}, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->a()LX/0Px;

    move-result-object v2

    .line 2112483
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/EKB;

    sget-object v4, LX/CyI;->PLACES:LX/CyI;

    invoke-direct {v3, v4, p2, v2}, LX/EKB;-><init>(LX/CyI;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/0Px;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2112484
    new-instance v0, LX/ENL;

    invoke-direct {v0, v1}, LX/ENL;-><init>(LX/6Zz;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;
    .locals 7

    .prologue
    .line 2112485
    const-class v1, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    monitor-enter v1

    .line 2112486
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112487
    sput-object v2, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112488
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112489
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112490
    new-instance v3, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;

    const/16 v4, 0x1b

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xc83

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x32d4

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x33c3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2112491
    move-object v0, v3

    .line 2112492
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112493
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112494
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112495
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;LX/1Pn;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2112467
    invoke-static {}, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->a()LX/0Px;

    move-result-object v0

    .line 2112468
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    new-instance v1, LX/EKB;

    sget-object v2, LX/CyI;->PLACES:LX/CyI;

    invoke-direct {v1, v2, p2, v0}, LX/EKB;-><init>(LX/CyI;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/0Px;)V

    invoke-static {p1, v1}, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;->a(LX/1Pn;LX/EKB;)V

    .line 2112469
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    check-cast p1, LX/CxV;

    invoke-interface {p1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CvY;->c(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    .line 2112470
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2112466
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/local/SearchResultsFbMapViewDelegatePartDefinition;->a(LX/1aD;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/1Pn;)LX/ENL;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x514ea84

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2112461
    check-cast p2, LX/ENL;

    check-cast p4, Lcom/facebook/maps/FbMapViewDelegate;

    .line 2112462
    iget-boolean v1, p2, LX/ENL;->b:Z

    if-nez v1, :cond_0

    .line 2112463
    iget-object v1, p2, LX/ENL;->c:LX/6Zz;

    invoke-virtual {p4, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2112464
    const/4 v1, 0x1

    iput-boolean v1, p2, LX/ENL;->b:Z

    .line 2112465
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x68f68dfc

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
