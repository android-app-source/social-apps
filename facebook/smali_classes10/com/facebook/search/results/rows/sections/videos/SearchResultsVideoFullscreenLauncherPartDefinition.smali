.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxj;",
        ":",
        "LX/1Po;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/1qa;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/EPe;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/1qa;LX/0Ot;LX/EPe;LX/0Ot;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1qa;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/video/VideoZeroDialogPartDefinition;",
            ">;",
            "LX/EPe;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117527
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2117528
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a:LX/1qa;

    .line 2117529
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->b:LX/0Ot;

    .line 2117530
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->c:LX/EPe;

    .line 2117531
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->d:LX/0Ot;

    .line 2117532
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->e:LX/0ad;

    .line 2117533
    return-void
.end method

.method private a(LX/Cxj;LX/CzL;LX/CyM;LX/395;)Landroid/view/View$OnClickListener;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/CzL",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;",
            "LX/CyM;",
            "LX/395;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2117534
    new-instance v0, LX/EPm;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/EPm;-><init>(Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;LX/CyM;LX/395;LX/CzL;LX/Cxj;)V

    return-object v0
.end method

.method private static a(LX/CzL;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;)",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2117535
    iget-object v0, p0, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2117536
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2117537
    iget v1, p0, LX/CzL;->b:I

    move v1, v1

    .line 2117538
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2117539
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    .line 2117540
    iget v1, p0, LX/CzL;->b:I

    move v1, v1

    .line 2117541
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2117542
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->j()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 2117543
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;
    .locals 9

    .prologue
    .line 2117544
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    monitor-enter v1

    .line 2117545
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117546
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117547
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117548
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117549
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    const/16 v5, 0x848

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/EPe;->a(LX/0QB;)LX/EPe;

    move-result-object v6

    check-cast v6, LX/EPe;

    const/16 v7, 0x32d4

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;-><init>(LX/1qa;LX/0Ot;LX/EPe;LX/0Ot;LX/0ad;)V

    .line 2117550
    move-object v0, v3

    .line 2117551
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117552
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117553
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117554
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2117555
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2117556
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2117557
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2117558
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/CvY;LX/CzL;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/CxP;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/CxP;",
            ":",
            "LX/CxV;",
            ">(",
            "LX/CvY;",
            "LX/CzL",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2117559
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v10

    .line 2117560
    invoke-virtual {p1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    .line 2117561
    if-eqz v1, :cond_0

    .line 2117562
    const-string v2, "thumbnail_decoration"

    invoke-virtual {v10, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2117563
    :cond_0
    invoke-virtual {p1}, LX/CzL;->j()Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v1, p3

    .line 2117564
    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v2

    sget-object v3, LX/8ch;->CLICK:LX/8ch;

    move-object/from16 v0, p3

    invoke-interface {v0, p1}, LX/CxP;->b(LX/CzL;)I

    move-result v4

    move-object/from16 v1, p3

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-interface {v0, p1}, LX/CxP;->b(LX/CzL;)I

    move-result v6

    invoke-virtual {p1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10}, LX/0P2;->b()LX/0P1;

    move-result-object v7

    invoke-static {v5, v6, v1, v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v1, p0

    move-object v5, p1

    invoke-virtual/range {v1 .. v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2117565
    :goto_0
    return-void

    .line 2117566
    :cond_1
    if-eqz p2, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 2117567
    :goto_1
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(LX/CzL;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 2117568
    if-eqz v1, :cond_2

    .line 2117569
    const-string v2, "edge_result_role"

    invoke-virtual {v10, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    :cond_2
    move-object/from16 v1, p3

    .line 2117570
    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v11

    sget-object v12, LX/8ch;->CLICK:LX/8ch;

    invoke-virtual {p1}, LX/CzL;->d()I

    move-result v13

    move-object/from16 v1, p3

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {p1}, LX/CzL;->l()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-interface {v0, p1}, LX/CxP;->b(LX/CzL;)I

    move-result v4

    invoke-virtual {p1}, LX/CzL;->d()I

    move-result v5

    invoke-virtual {p1}, LX/CzL;->e()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v6

    invoke-static {v6}, LX/8eM;->j(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v6

    invoke-virtual {p1}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v7

    invoke-virtual {p1}, LX/CzL;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v9}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10}, LX/0P2;->b()LX/0P1;

    move-result-object v10

    invoke-static/range {v1 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;IIILcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v1, p0

    move-object v2, v11

    move-object v3, v12

    move v4, v13

    move-object v5, p1

    invoke-virtual/range {v1 .. v6}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0

    .line 2117571
    :cond_3
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2117572
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2117573
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2117574
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 2117575
    if-nez v0, :cond_0

    .line 2117576
    const/4 v1, 0x0

    .line 2117577
    :goto_0
    move-object v0, v1

    .line 2117578
    return-object v0

    .line 2117579
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2117580
    sget-object v1, LX/Cvg;->LIVE:LX/Cvg;

    invoke-virtual {v1}, LX/Cvg;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2117581
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const p0, 0x1eaef984

    if-ne v1, p0, :cond_2

    .line 2117582
    sget-object v1, LX/Cvg;->WEB:LX/Cvg;

    invoke-virtual {v1}, LX/Cvg;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2117583
    :cond_2
    sget-object v1, LX/Cvg;->NATIVE:LX/Cvg;

    invoke-virtual {v1}, LX/Cvg;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2117584
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxj;

    .line 2117585
    invoke-interface {p3, p2}, LX/Cxj;->d(LX/CzL;)LX/CyM;

    move-result-object v8

    .line 2117586
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v5, v0

    .line 2117587
    check-cast v5, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2117588
    iget-object v0, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2117589
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2117590
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 2117591
    invoke-static {v5}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 2117592
    invoke-static {v5}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2117593
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2117594
    invoke-static {v2}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    .line 2117595
    new-instance v3, LX/0AW;

    invoke-direct {v3, v1}, LX/0AW;-><init>(LX/162;)V

    .line 2117596
    iput-boolean v2, v3, LX/0AW;->d:Z

    .line 2117597
    move-object v1, v3

    .line 2117598
    invoke-virtual {v1}, LX/0AW;->a()Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-result-object v2

    .line 2117599
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a:LX/1qa;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    sget-object v3, LX/26P;->Video:LX/26P;

    invoke-virtual {v1, v0, v3}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v3

    .line 2117600
    new-instance v0, LX/395;

    new-instance v1, LX/0AV;

    .line 2117601
    iget-object v6, v8, LX/CyM;->a:Ljava/lang/String;

    move-object v6, v6

    .line 2117602
    invoke-direct {v1, v6}, LX/0AV;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/0AV;->a()Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;

    move-result-object v1

    sget-object v6, LX/0JG;->SEARCH_RESULTS:LX/0JG;

    move-object v7, p3

    check-cast v7, LX/CxV;

    invoke-interface {v7}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v7

    invoke-interface {v7}, LX/8ef;->c()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, LX/395;-><init>(Lcom/facebook/video/analytics/VideoAnalyticsRequiredInfo;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/1bf;Lcom/facebook/graphql/model/GraphQLVideo;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0JG;Ljava/lang/String;)V

    move-object v1, p3

    .line 2117603
    check-cast v1, LX/1Po;

    invoke-interface {v1}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    invoke-static {v1}, LX/3Ik;->a(LX/1PT;)LX/04D;

    move-result-object v1

    .line 2117604
    invoke-virtual {v0, v1}, LX/395;->a(LX/04D;)LX/395;

    .line 2117605
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Nt;

    new-instance v2, LX/3FZ;

    invoke-direct {p0, p3, p2, v8, v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(LX/Cxj;LX/CzL;LX/CyM;LX/395;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-direct {v2, v0}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2117606
    const/4 v0, 0x0

    return-object v0
.end method
