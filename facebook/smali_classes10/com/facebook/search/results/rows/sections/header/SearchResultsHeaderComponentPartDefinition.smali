.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/EMl;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1X6;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/1V0;

.field private final f:LX/EMk;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2111287
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v2

    const/high16 v3, 0x40000000    # 2.0f

    .line 2111288
    iput v3, v2, LX/1UY;->b:F

    .line 2111289
    move-object v2, v2

    .line 2111290
    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    sget-object v3, LX/1X9;->TOP:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/EMk;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111283
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2111284
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->e:LX/1V0;

    .line 2111285
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->f:LX/EMk;

    .line 2111286
    return-void
.end method

.method private a(LX/1De;LX/EMl;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/EMl;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2111251
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->f:LX/EMk;

    const/4 v1, 0x0

    .line 2111252
    new-instance v2, LX/EMj;

    invoke-direct {v2, v0}, LX/EMj;-><init>(LX/EMk;)V

    .line 2111253
    sget-object v3, LX/EMk;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EMi;

    .line 2111254
    if-nez v3, :cond_0

    .line 2111255
    new-instance v3, LX/EMi;

    invoke-direct {v3}, LX/EMi;-><init>()V

    .line 2111256
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EMi;->a$redex0(LX/EMi;LX/1De;IILX/EMj;)V

    .line 2111257
    move-object v2, v3

    .line 2111258
    move-object v1, v2

    .line 2111259
    move-object v0, v1

    .line 2111260
    iget-object v1, p2, LX/EMl;->a:Ljava/lang/String;

    .line 2111261
    iget-object v2, v0, LX/EMi;->a:LX/EMj;

    iput-object v1, v2, LX/EMj;->a:Ljava/lang/String;

    .line 2111262
    iget-object v2, v0, LX/EMi;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2111263
    move-object v0, v0

    .line 2111264
    iget-object v1, p2, LX/EMl;->b:Ljava/lang/String;

    .line 2111265
    iget-object v2, v0, LX/EMi;->a:LX/EMj;

    iput-object v1, v2, LX/EMj;->b:Ljava/lang/String;

    .line 2111266
    move-object v0, v0

    .line 2111267
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 2111268
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->e:LX/1V0;

    move-object v0, p3

    check-cast v0, LX/1Ps;

    sget-object v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->d:LX/1X6;

    invoke-static {p3, v3}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/1Pn;LX/1X6;)LX/1X6;

    move-result-object v3

    invoke-virtual {v2, p1, v0, v3, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;
    .locals 6

    .prologue
    .line 2111272
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    monitor-enter v1

    .line 2111273
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111274
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111275
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111276
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111277
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/EMk;->a(LX/0QB;)LX/EMk;

    move-result-object v5

    check-cast v5, LX/EMk;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/EMk;)V

    .line 2111278
    move-object v0, p0

    .line 2111279
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111280
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111281
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111282
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2111291
    check-cast p2, LX/EMl;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->a(LX/1De;LX/EMl;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2111271
    check-cast p2, LX/EMl;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->a(LX/1De;LX/EMl;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111270
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2111269
    const/4 v0, 0x0

    return-object v0
.end method
