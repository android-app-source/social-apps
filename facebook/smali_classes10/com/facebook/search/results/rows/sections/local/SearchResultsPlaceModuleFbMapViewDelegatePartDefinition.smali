.class public Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/CxA;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsPlaceModuleInterfaces$SearchResultsPlaceModule;",
        ">;",
        "LX/ENP;",
        "TE;",
        "Lcom/facebook/maps/FbMapViewDelegate;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/maps/FbMapViewDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2112589
    const v0, 0x7f030f7d

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Landroid/content/res/Resources;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112584
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2112585
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->b:LX/0Ot;

    .line 2112586
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->c:LX/0Ot;

    .line 2112587
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->d:LX/0Ot;

    .line 2112588
    return-void
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;LX/Cwx;LX/0Px;Ljava/util/List;LX/697;)LX/6Zz;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsPlaceModuleInterfaces$SearchResultsPlaceModule$ModuleResults$Edges;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/android/maps/model/LatLng;",
            ">;",
            "LX/697;",
            ")",
            "LX/6Zz;"
        }
    .end annotation

    .prologue
    .line 2112583
    new-instance v0, LX/ENO;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/ENO;-><init>(Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;LX/Cwx;LX/0Px;Ljava/util/List;LX/697;)V

    return-object v0
.end method

.method private a(LX/1aD;LX/CzL;LX/Cwx;)LX/ENP;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsPlaceModuleInterfaces$SearchResultsPlaceModule;",
            ">;TE;)",
            "LX/ENP;"
        }
    .end annotation

    .prologue
    .line 2112566
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2112567
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bL()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v2

    .line 2112568
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v3

    .line 2112569
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 2112570
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2112571
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->q()LX/8dB;

    move-result-object v0

    .line 2112572
    invoke-interface {v0}, LX/8dB;->E()LX/8d5;

    move-result-object v0

    .line 2112573
    if-eqz v0, :cond_0

    .line 2112574
    new-instance v6, Lcom/facebook/android/maps/model/LatLng;

    invoke-interface {v0}, LX/8d5;->a()D

    move-result-wide v8

    invoke-interface {v0}, LX/8d5;->b()D

    move-result-wide v10

    invoke-direct {v6, v8, v9, v10, v11}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 2112575
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2112576
    invoke-virtual {v3, v6}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    .line 2112577
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2112578
    :cond_1
    invoke-virtual {v3}, LX/696;->a()LX/697;

    move-result-object v0

    .line 2112579
    invoke-static {p0, p3, v2, v4, v0}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->a(Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;LX/Cwx;LX/0Px;Ljava/util/List;LX/697;)LX/6Zz;

    move-result-object v1

    .line 2112580
    invoke-static {}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->c()LX/0Px;

    move-result-object v2

    .line 2112581
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nt;

    new-instance v3, LX/EKB;

    sget-object v4, LX/CyI;->PLACES:LX/CyI;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5, v2}, LX/EKB;-><init>(LX/CyI;Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;LX/0Px;)V

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2112582
    new-instance v0, LX/ENP;

    invoke-direct {v0, v1}, LX/ENP;-><init>(LX/6Zz;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;
    .locals 6

    .prologue
    .line 2112555
    const-class v1, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;

    monitor-enter v1

    .line 2112556
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112557
    sput-object v2, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112558
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112559
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112560
    new-instance v3, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;

    const/16 v4, 0x1b

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x32d4

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x33c3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2112561
    move-object v0, v3

    .line 2112562
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112563
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112564
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112565
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2112544
    const-string v0, "Places"

    .line 2112545
    const-string v1, "places"

    .line 2112546
    new-instance v2, LX/CyH;

    const-string v3, "places_set_search"

    invoke-direct {v2, v3, v0, v1}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/maps/FbMapViewDelegate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2112554
    sget-object v0, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2112553
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cwx;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/local/SearchResultsPlaceModuleFbMapViewDelegatePartDefinition;->a(LX/1aD;LX/CzL;LX/Cwx;)LX/ENP;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2f6b6b55

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2112548
    check-cast p2, LX/ENP;

    check-cast p4, Lcom/facebook/maps/FbMapViewDelegate;

    .line 2112549
    iget-boolean v1, p2, LX/ENP;->b:Z

    if-nez v1, :cond_0

    .line 2112550
    iget-object v1, p2, LX/ENP;->c:LX/6Zz;

    invoke-virtual {p4, v1}, LX/6Zn;->a(LX/6Zz;)V

    .line 2112551
    const/4 v1, 0x1

    iput-boolean v1, p2, LX/ENP;->b:Z

    .line 2112552
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x317556a1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2112547
    const/4 v0, 0x1

    return v0
.end method
