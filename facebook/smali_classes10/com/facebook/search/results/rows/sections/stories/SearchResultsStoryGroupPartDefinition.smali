.class public Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/EKJ;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;LX/0Ot;LX/EKJ;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;",
            "Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/condensedstory/CondensedStoryPartDefinition;",
            ">;",
            "LX/EKJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2117153
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2117154
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    .line 2117155
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;

    .line 2117156
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->c:LX/0Ot;

    .line 2117157
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->d:LX/EKJ;

    .line 2117158
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;
    .locals 7

    .prologue
    .line 2117159
    const-class v1, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;

    monitor-enter v1

    .line 2117160
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117161
    sput-object v2, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117162
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117163
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117164
    new-instance v6, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;

    const/16 v5, 0x1eb5

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/EKJ;->b(LX/0QB;)LX/EKJ;

    move-result-object v5

    check-cast v5, LX/EKJ;

    invoke-direct {v6, v3, v4, p0, v5}, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;LX/0Ot;LX/EKJ;)V

    .line 2117165
    move-object v0, v6

    .line 2117166
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117167
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117168
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117169
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2117170
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/Cxo;

    .line 2117171
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/stories/SearchResultsStorySnippetPartDefinition;

    const/4 v3, 0x0

    .line 2117172
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2117173
    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    .line 2117174
    new-instance v4, LX/EPc;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 2117175
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->d:LX/0am;

    move-object v2, v2

    .line 2117176
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2117177
    iget-object v2, v1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->d:LX/0am;

    move-object v2, v2

    .line 2117178
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2117179
    :goto_0
    iget-object v6, v1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->e:LX/0am;

    move-object v6, v6

    .line 2117180
    invoke-virtual {v6}, LX/0am;->isPresent()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2117181
    iget-object v3, v1, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->e:LX/0am;

    move-object v1, v3

    .line 2117182
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :goto_1
    invoke-direct {v4, v5, v2, v1}, LX/EPc;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v4

    .line 2117183
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117184
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2117185
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2117186
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    .line 2117187
    sget-object v2, LX/1Qt;->SEARCH_DENSE_FEED:LX/1Qt;

    if-ne v0, v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 2117188
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->c:LX/0Ot;

    new-instance v4, LX/C33;

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->d:LX/EKJ;

    .line 2117189
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2117190
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    .line 2117191
    new-instance v6, LX/EKH;

    invoke-direct {v6, v5, v0, p3}, LX/EKH;-><init>(LX/EKJ;Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;LX/Cxh;)V

    move-object v0, v6

    .line 2117192
    sget-object v5, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v0, v5, v6, v1}, LX/C33;-><init>(Landroid/view/View$OnClickListener;LX/C34;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    invoke-static {p1, v2, v3, v4}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/stories/SearchResultsStoryGroupPartDefinition;->b:Lcom/facebook/feed/rows/sections/GraphQLStorySelectorPartDefinition;

    .line 2117193
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2117194
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2117195
    const/4 v0, 0x0

    return-object v0

    :cond_0
    move-object v2, v3

    goto :goto_0

    :cond_1
    move-object v1, v3

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2117196
    const/4 v0, 0x1

    return v0
.end method
