.class public Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Landroid/text/SpannableStringBuilder;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field public static final b:Ljava/lang/CharSequence;

.field private static f:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field public final d:LX/11S;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2116244
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, -0x3f400000    # -6.0f

    .line 2116245
    iput v1, v0, LX/1UY;->b:F

    .line 2116246
    move-object v0, v0

    .line 2116247
    const/high16 v1, 0x40c00000    # 6.0f

    .line 2116248
    iput v1, v0, LX/1UY;->c:F

    .line 2116249
    move-object v0, v0

    .line 2116250
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->a:LX/1Ua;

    .line 2116251
    const-string v0, "[trending]"

    sput-object v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->b:Ljava/lang/CharSequence;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/11R;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116284
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2116285
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2116286
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->d:LX/11S;

    .line 2116287
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->e:Landroid/content/res/Resources;

    .line 2116288
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;
    .locals 6

    .prologue
    .line 2116273
    const-class v1, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;

    monitor-enter v1

    .line 2116274
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116275
    sput-object v2, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116276
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116277
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116278
    new-instance p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v4

    check-cast v4, LX/11R;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/11R;Landroid/content/res/Resources;)V

    .line 2116279
    move-object v0, p0

    .line 2116280
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116281
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116282
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116283
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2116272
    sget-object v0, LX/3ab;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2116259
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2116260
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->a:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2116261
    const/4 v11, 0x0

    .line 2116262
    new-instance v5, Landroid/text/SpannableStringBuilder;

    sget-object v3, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->b:Ljava/lang/CharSequence;

    invoke-direct {v5, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2116263
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jP()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    .line 2116264
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v3

    .line 2116265
    :goto_0
    const-string v6, "   "

    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->d:LX/11S;

    sget-object v8, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    const-wide/16 v9, 0x3e8

    mul-long/2addr v3, v9

    invoke-interface {v7, v8, v3, v4}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2116266
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->e:Landroid/content/res/Resources;

    const v4, 0x7f0219a8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 2116267
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v3, v11, v11, v4, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2116268
    new-instance v4, Landroid/text/style/ImageSpan;

    invoke-direct {v4, v3}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    sget-object v3, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->b:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/16 v6, 0x11

    invoke-virtual {v5, v4, v11, v3, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2116269
    move-object v0, v5

    .line 2116270
    return-object v0

    .line 2116271
    :cond_0
    const-wide/16 v3, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7af1e30f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2116255
    check-cast p2, Landroid/text/SpannableStringBuilder;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 2116256
    invoke-virtual {p4}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0867

    invoke-virtual {p4, v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2116257
    invoke-virtual {p4, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2116258
    const/16 v1, 0x1f

    const v2, -0x536f8e65

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2116252
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2116253
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->jP()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2116254
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
