.class public Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModulePartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModulePartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117238
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2117239
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    .line 2117240
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModulePartDefinition;

    .line 2117241
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    .line 2117242
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    .line 2117243
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;
    .locals 7

    .prologue
    .line 2117227
    const-class v1, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;

    monitor-enter v1

    .line 2117228
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117229
    sput-object v2, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117230
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117231
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117232
    new-instance p0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModulePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModulePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModulePartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;)V

    .line 2117233
    move-object v0, p0

    .line 2117234
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117235
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117236
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117237
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2117245
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2117246
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2117247
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;

    .line 2117248
    sget-boolean v1, LX/007;->i:Z

    move v1, v1

    .line 2117249
    if-eqz v1, :cond_0

    .line 2117250
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117251
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModulePartDefinition;

    invoke-virtual {p1, v1, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117252
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyFooterPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117253
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2117254
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/unsupported/UnsupportedModuleGroupPartDefinition;->d:Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2117244
    const/4 v0, 0x1

    return v0
.end method
