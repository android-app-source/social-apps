.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/EN0;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1Ps;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final c:LX/0wM;

.field private final d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;LX/0wM;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111724
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2111725
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->a:Landroid/content/Context;

    .line 2111726
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2111727
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->c:LX/0wM;

    .line 2111728
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2111729
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;
    .locals 7

    .prologue
    .line 2111730
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    monitor-enter v1

    .line 2111731
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111732
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111733
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111734
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111735
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;LX/0wM;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2111736
    move-object v0, p0

    .line 2111737
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111738
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111739
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111740
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2111741
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2111742
    check-cast p2, LX/EN0;

    .line 2111743
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    iget-object v1, p2, LX/EN0;->a:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2111744
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->d:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    sget-object v1, LX/EMz;->a:LX/1X6;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2111745
    iget-object v0, p2, LX/EN0;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2111746
    iget-object v1, p2, LX/EN0;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v2, p2, LX/EN0;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v1, v2}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/Integer;

    move-result-object v1

    .line 2111747
    if-eqz v1, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    .line 2111748
    :goto_0
    if-eqz v0, :cond_1

    sget-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2111749
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->c:LX/0wM;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->b:LX/0P1;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2111750
    :goto_1
    return-object v0

    .line 2111751
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2111752
    :cond_1
    if-eqz v0, :cond_2

    .line 2111753
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 2111754
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x44be2263

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2111755
    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2111756
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2111757
    const/16 v1, 0x1f

    const v2, 0x5dd21b43

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111758
    const/4 v0, 0x1

    return v0
.end method
