.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsSectionHeaderModuleInterfaces$SearchResultsSectionHeaderModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsSectionHeaderModuleInterfaces$SearchResultsSectionHeaderModule;",
            "TE;>;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;LX/0Ot;Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111328
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2111329
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    .line 2111330
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;

    .line 2111331
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->c:LX/0Ot;

    .line 2111332
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->d:Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    .line 2111333
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;
    .locals 7

    .prologue
    .line 2111353
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;

    monitor-enter v1

    .line 2111354
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111355
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111356
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111357
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111358
    new-instance v6, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;

    const/16 v5, 0x3422

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    invoke-direct {v6, v3, v4, p0, v5}, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;LX/0Ot;Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;)V

    .line 2111359
    move-object v0, v6

    .line 2111360
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111361
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111362
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111363
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2111338
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxi;

    const/4 v3, 0x0

    .line 2111339
    invoke-virtual {p2}, LX/CzL;->i()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SUBHEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111340
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->d:Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    .line 2111341
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2111342
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111343
    :goto_0
    return-object v3

    .line 2111344
    :cond_0
    invoke-virtual {p2}, LX/CzL;->i()LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->SEARCH_DENSE_FEED:LX/1Qt;

    invoke-virtual {v0, v1}, LX/1Qt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2111345
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2111346
    :cond_2
    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2111347
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionHeaderComponentPartDefinition;

    .line 2111348
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2111349
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0

    .line 2111350
    :cond_3
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsSectionHeaderModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    new-instance v2, LX/EMl;

    .line 2111351
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2111352
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v3}, LX/EMl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2111334
    check-cast p1, LX/CzL;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2111335
    new-array v3, v1, [Ljava/lang/CharSequence;

    .line 2111336
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2111337
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method
