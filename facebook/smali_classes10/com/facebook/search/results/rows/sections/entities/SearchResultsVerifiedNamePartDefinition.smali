.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8cz;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110972
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2110973
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;->a:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2110974
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;->b:LX/0Ot;

    .line 2110975
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;
    .locals 5

    .prologue
    .line 2110976
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    monitor-enter v1

    .line 2110977
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110978
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110979
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110980
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110981
    new-instance v4, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    const/16 p0, 0x3512

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;-><init>(Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;LX/0Ot;)V

    .line 2110982
    move-object v0, v4

    .line 2110983
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110984
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110985
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110986
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2110987
    check-cast p2, LX/CzL;

    .line 2110988
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2110989
    check-cast v0, LX/8cz;

    .line 2110990
    invoke-interface {v0}, LX/8cz;->o()Z

    move-result v1

    invoke-interface {v0}, LX/8cz;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;->b:LX/0Ot;

    invoke-static {v1, v0, v2}, LX/ELM;->a(ZLjava/lang/String;LX/0Ot;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2110991
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;->a:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2110992
    const/4 v0, 0x0

    return-object v0
.end method
