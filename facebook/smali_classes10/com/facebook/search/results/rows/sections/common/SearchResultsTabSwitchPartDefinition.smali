.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/Cxi;",
        ":",
        "LX/CxG;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EKB;",
        "Landroid/view/View$OnClickListener;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/CvY;


# direct methods
.method public constructor <init>(LX/CvY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106355
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2106356
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;->a:LX/CvY;

    .line 2106357
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;
    .locals 4

    .prologue
    .line 2106344
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    monitor-enter v1

    .line 2106345
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106346
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106347
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106348
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106349
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v3

    check-cast v3, LX/CvY;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;-><init>(LX/CvY;)V

    .line 2106350
    move-object v0, p0

    .line 2106351
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106352
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106353
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106354
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1Pn;LX/EKB;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/EKB;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2106342
    check-cast p0, LX/Cxi;

    iget-object v0, p1, LX/EKB;->a:LX/CyI;

    iget-object v1, p1, LX/EKB;->c:LX/0Px;

    invoke-interface {p0, v0, v1}, LX/Cxi;->a(LX/CyI;LX/0Px;)V

    .line 2106343
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2106340
    check-cast p2, LX/EKB;

    check-cast p3, LX/1Pn;

    .line 2106341
    new-instance v0, LX/EKA;

    invoke-direct {v0, p0, p3, p2}, LX/EKA;-><init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;LX/1Pn;LX/EKB;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x15571a0b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2106337
    check-cast p2, Landroid/view/View$OnClickListener;

    .line 2106338
    invoke-virtual {p4, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2106339
    const/16 v1, 0x1f

    const v2, 0x59318167

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1Pn;LX/EKB;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/EKB;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2106332
    move-object v0, p1

    check-cast v0, LX/Cxi;

    iget-object v1, p2, LX/EKB;->a:LX/CyI;

    iget-object v2, p2, LX/EKB;->c:LX/0Px;

    invoke-interface {v0, v1, v2}, LX/Cxi;->a(LX/CyI;LX/0Px;)V

    .line 2106333
    iget-object v8, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsTabSwitchPartDefinition;->a:LX/CvY;

    move-object v0, p1

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v9

    move-object v0, p1

    check-cast v0, LX/CxG;

    iget-object v1, p2, LX/EKB;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-interface {v0, v1}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v10

    iget-object v11, p2, LX/EKB;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-object v0, p1

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    iget-object v1, p2, LX/EKB;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    iget-object v2, p2, LX/EKB;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    iget-object v3, p2, LX/EKB;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m()LX/0am;

    move-result-object v3

    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p2, LX/EKB;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2106334
    iget-object v5, v4, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v4, v5

    .line 2106335
    invoke-virtual {v4}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    check-cast p1, LX/CxG;

    iget-object v5, p2, LX/EKB;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-interface {p1, v5}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v5

    iget-object v6, p2, LX/EKB;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v6}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v6

    iget-object v7, p2, LX/EKB;->b:Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v7}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->u()LX/0am;

    move-result-object v7

    invoke-virtual {v7}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static/range {v0 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;IILcom/facebook/graphql/enums/GraphQLObjectType;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v8, v9, v10, v11, v0}, LX/CvY;->b(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2106336
    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2106330
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2106331
    return-void
.end method
