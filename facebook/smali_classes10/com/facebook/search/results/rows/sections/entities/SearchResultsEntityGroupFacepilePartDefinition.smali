.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/ELL;",
        "LX/0Px",
        "<",
        "LX/6UY;",
        ">;",
        "LX/1Pn;",
        "Lcom/facebook/fbui/facepile/FacepileGridView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2108527
    const v0, 0x7f0a00e8

    sput v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108525
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2108526
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;
    .locals 3

    .prologue
    .line 2108514
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    monitor-enter v1

    .line 2108515
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108516
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2108517
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108518
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2108519
    new-instance v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    invoke-direct {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;-><init>()V

    .line 2108520
    move-object v0, v0

    .line 2108521
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2108522
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2108523
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2108524
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2108493
    check-cast p2, LX/ELL;

    check-cast p3, LX/1Pn;

    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 2108494
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2108495
    iget-object v4, p2, LX/ELL;->b:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    .line 2108496
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 2108497
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 2108498
    :goto_1
    if-eqz v0, :cond_1

    .line 2108499
    new-instance v6, LX/6UY;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v6, v0, v7, v7}, LX/6UY;-><init>(Landroid/net/Uri;II)V

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2108500
    add-int/lit8 v0, v1, 0x1

    .line 2108501
    iget v1, p2, LX/ELL;->a:I

    if-ne v0, v1, :cond_2

    .line 2108502
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2108503
    :goto_2
    return-object v0

    .line 2108504
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 2108505
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2108506
    :cond_3
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;->a:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2108507
    :goto_3
    iget v2, p2, LX/ELL;->a:I

    if-ge v1, v2, :cond_4

    .line 2108508
    new-instance v2, LX/6UY;

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-direct {v2, v4, v7, v7}, LX/6UY;-><init>(Landroid/graphics/drawable/Drawable;II)V

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2108509
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2108510
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x479b3911

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2108511
    check-cast p2, LX/0Px;

    check-cast p4, Lcom/facebook/fbui/facepile/FacepileGridView;

    .line 2108512
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/facepile/FacepileGridView;->setFaces(Ljava/util/List;)V

    .line 2108513
    const/16 v1, 0x1f

    const v2, -0x4782257b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
