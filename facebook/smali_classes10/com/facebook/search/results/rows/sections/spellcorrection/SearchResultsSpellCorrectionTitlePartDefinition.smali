.class public Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;",
        ">;",
        "Ljava/lang/Integer;",
        "LX/1Ps;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116993
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2116994
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2116995
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;
    .locals 4

    .prologue
    .line 2116982
    const-class v1, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;

    monitor-enter v1

    .line 2116983
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116984
    sput-object v2, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116985
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116986
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116987
    new-instance p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2116988
    move-object v0, p0

    .line 2116989
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116990
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116991
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116992
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2116981
    sget-object v0, LX/3ab;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2116956
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2116957
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2116958
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;

    .line 2116959
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->INCLUDING:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    .line 2116960
    :goto_0
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v2

    .line 2116961
    iput v1, v2, LX/1UY;->c:F

    .line 2116962
    move-object v1, v2

    .line 2116963
    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    .line 2116964
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->INCLUDING:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-ne v1, v3, :cond_1

    sget-object v1, LX/1X9;->MIDDLE:LX/1X9;

    .line 2116965
    :goto_1
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v4, LX/1X6;

    invoke-direct {v4, p2, v2, v1}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2116966
    iget-object v0, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->SHOWING:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-ne v0, v1, :cond_2

    const v0, 0x7f0822ab

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 2116967
    :cond_0
    const/high16 v1, 0x40800000    # 4.0f

    goto :goto_0

    .line 2116968
    :cond_1
    sget-object v1, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    goto :goto_1

    .line 2116969
    :cond_2
    const v0, 0x7f0822ac

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x58b2062

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2116974
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p2, Ljava/lang/Integer;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 2116975
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2116976
    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;

    .line 2116977
    invoke-virtual {p4}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p3, 0x0

    iget-object v1, v1, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->d:Ljava/lang/String;

    aput-object v1, p0, p3

    invoke-virtual {v2, v4, p0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2116978
    invoke-virtual {p4}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f0e086a

    invoke-virtual {p4, v2, v4}, Lcom/facebook/widget/text/BetterTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 2116979
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2116980
    const/16 v1, 0x1f

    const v2, -0x6470a759

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2116970
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2116971
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2116972
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;

    .line 2116973
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->SHOWING:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-eq v1, v2, :cond_0

    iget-object v0, v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;->INCLUDING:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
