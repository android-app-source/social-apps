.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/A3T;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1X6;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/1V0;

.field private final f:LX/EP0;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, -0x3f400000    # -6.0f

    .line 2115683
    new-instance v0, LX/1X6;

    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v1

    .line 2115684
    iput v2, v1, LX/1UY;->b:F

    .line 2115685
    move-object v1, v1

    .line 2115686
    iput v2, v1, LX/1UY;->c:F

    .line 2115687
    move-object v1, v1

    .line 2115688
    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1X6;-><init>(LX/1Ua;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1V0;LX/EP0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115649
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2115650
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->e:LX/1V0;

    .line 2115651
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->f:LX/EP0;

    .line 2115652
    return-void
.end method

.method private a(LX/1De;LX/A3T;LX/1Ps;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/A3T;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2115669
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->f:LX/EP0;

    const/4 v1, 0x0

    .line 2115670
    new-instance v2, LX/EOz;

    invoke-direct {v2, v0}, LX/EOz;-><init>(LX/EP0;)V

    .line 2115671
    sget-object v3, LX/EP0;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EOy;

    .line 2115672
    if-nez v3, :cond_0

    .line 2115673
    new-instance v3, LX/EOy;

    invoke-direct {v3}, LX/EOy;-><init>()V

    .line 2115674
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EOy;->a$redex0(LX/EOy;LX/1De;IILX/EOz;)V

    .line 2115675
    move-object v2, v3

    .line 2115676
    move-object v1, v2

    .line 2115677
    move-object v0, v1

    .line 2115678
    iget-object v1, v0, LX/EOy;->a:LX/EOz;

    iput-object p2, v1, LX/EOz;->a:LX/A3T;

    .line 2115679
    iget-object v1, v0, LX/EOy;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2115680
    move-object v0, v0

    .line 2115681
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2115682
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->e:LX/1V0;

    sget-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->d:LX/1X6;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;
    .locals 6

    .prologue
    .line 2115658
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    monitor-enter v1

    .line 2115659
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115660
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115661
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115662
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115663
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v4

    check-cast v4, LX/1V0;

    invoke-static {v0}, LX/EP0;->a(LX/0QB;)LX/EP0;

    move-result-object v5

    check-cast v5, LX/EP0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;-><init>(Landroid/content/Context;LX/1V0;LX/EP0;)V

    .line 2115664
    move-object v0, p0

    .line 2115665
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115666
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115667
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115668
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2115657
    check-cast p2, LX/A3T;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->a(LX/1De;LX/A3T;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2115656
    check-cast p2, LX/A3T;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextSourceComponentPartDefinition;->a(LX/1De;LX/A3T;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115654
    check-cast p1, LX/A3T;

    .line 2115655
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/A3T;->cs()LX/A4G;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/A3T;->cs()LX/A4G;

    move-result-object v0

    invoke-interface {v0}, LX/A4G;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2115653
    const/4 v0, 0x0

    return-object v0
.end method
