.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1X9;",
        "Ljava/lang/Integer;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112253
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2112254
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    .line 2112255
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->b:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 2112256
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;
    .locals 5

    .prologue
    .line 2112257
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    monitor-enter v1

    .line 2112258
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112259
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112260
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112261
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112262
    new-instance p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;-><init>(Landroid/content/res/Resources;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V

    .line 2112263
    move-object v0, p0

    .line 2112264
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112265
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112266
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112267
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2112268
    check-cast p2, LX/1X9;

    .line 2112269
    sget-object v0, LX/END;->a:[I

    invoke-virtual {p2}, LX/1X9;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2112270
    new-instance v0, Landroid/util/AndroidRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid parameter forLiveFeedResharedAttachmentBackgroundPartDefinition:  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, LX/1X9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2112271
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b16d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2112272
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b16d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2112273
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b16d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2112274
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b16d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2112275
    const v0, 0x7f020a6e

    .line 2112276
    :goto_0
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->b:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v6, LX/1ds;

    invoke-direct {v6, v4, v3, v2, v1}, LX/1ds;-><init>(IIII)V

    invoke-interface {p1, v5, v6}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2112277
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 2112278
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b16d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2112279
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b16d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2112280
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v2, 0x7f0b16d3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2112281
    const v0, 0x7f020a6d

    move v4, v3

    move v3, v1

    .line 2112282
    goto :goto_0

    .line 2112283
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b16d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 2112284
    const/4 v3, 0x0

    .line 2112285
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b16d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2112286
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentBackgroundPartDefinition;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b16d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2112287
    const v0, 0x7f020a6a

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x334cad9c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2112288
    check-cast p2, Ljava/lang/Integer;

    .line 2112289
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2112290
    const/16 v1, 0x1f

    const v2, -0xc264d9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
