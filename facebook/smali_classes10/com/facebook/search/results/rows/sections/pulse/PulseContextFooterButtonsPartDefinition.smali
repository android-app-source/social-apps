.class public Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxV;",
        ":",
        "LX/CxG;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/Cx5;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
        "LX/EOt;",
        "TE;",
        "LX/3b7;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

.field public final b:LX/1Kf;

.field public final c:Landroid/app/Activity;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:LX/5up;

.field public final f:LX/0kL;

.field public final g:LX/CvY;

.field public final h:LX/0wM;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;LX/1Kf;Landroid/app/Activity;Lcom/facebook/content/SecureContextHelper;LX/5up;LX/0kL;LX/CvY;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115470
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2115471
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    .line 2115472
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->b:LX/1Kf;

    .line 2115473
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->c:Landroid/app/Activity;

    .line 2115474
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2115475
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->e:LX/5up;

    .line 2115476
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->f:LX/0kL;

    .line 2115477
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->g:LX/CvY;

    .line 2115478
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->h:LX/0wM;

    .line 2115479
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;
    .locals 12

    .prologue
    .line 2115421
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;

    monitor-enter v1

    .line 2115422
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115423
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115424
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115425
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115426
    new-instance v3, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v8

    check-cast v8, LX/5up;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v10

    check-cast v10, LX/CvY;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v11

    check-cast v11, LX/0wM;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;LX/1Kf;Landroid/app/Activity;Lcom/facebook/content/SecureContextHelper;LX/5up;LX/0kL;LX/CvY;LX/0wM;)V

    .line 2115427
    move-object v0, v3

    .line 2115428
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115429
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115430
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2115480
    if-eqz p1, :cond_0

    move-object v0, p2

    .line 2115481
    check-cast v0, LX/Cx5;

    invoke-interface {v0, p0, p1}, LX/Cx5;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2115482
    check-cast p2, LX/1Pq;

    invoke-interface {p2}, LX/1Pq;->iN_()V

    .line 2115483
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;Ljava/lang/String;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2115458
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->m()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 2115459
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115460
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->g:LX/CvY;

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->UNSAVE:LX/8ch;

    move-object v3, p3

    check-cast v3, LX/CxG;

    invoke-interface {v3, p2}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v3

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    move-object v5, p3

    check-cast v5, LX/CxG;

    invoke-interface {v5, p2}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v5

    sget-object v7, LX/8ch;->UNSAVE:LX/8ch;

    invoke-static {v6, v5, v7, v4}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/8ch;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2115461
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {p2, v0}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    move-result-object v3

    .line 2115462
    invoke-static {p2, v3, p3}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;)V

    .line 2115463
    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->e:LX/5up;

    const-string v7, "native_web_view"

    const-string v8, "saved_add"

    new-instance v0, LX/EOs;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LX/EOs;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;Z)V

    invoke-virtual {v6, p1, v7, v8, v0}, LX/5up;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2115464
    :goto_0
    return-void

    .line 2115465
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->g:LX/CvY;

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    sget-object v2, LX/8ch;->SAVE:LX/8ch;

    move-object v3, p3

    check-cast v3, LX/CxG;

    invoke-interface {v3, p2}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v3

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    move-object v5, p3

    check-cast v5, LX/CxG;

    invoke-interface {v5, p2}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v5

    sget-object v7, LX/8ch;->SAVE:LX/8ch;

    invoke-static {v6, v5, v7, v4}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/8ch;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2115466
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {p2, v0}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a(Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    move-result-object v3

    .line 2115467
    invoke-static {p2, v3, p3}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;)V

    .line 2115468
    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->e:LX/5up;

    const-string v7, "native_web_view"

    const-string v8, "saved_add"

    new-instance v0, LX/EOs;

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LX/EOs;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;Z)V

    invoke-virtual {v6, p1, v7, v8, v0}, LX/5up;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2115469
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->t()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->t()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->t()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public static a$redex0(Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;LX/20X;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20X;",
            "Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2115446
    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->o()Ljava/lang/String;

    move-result-object v1

    move-object v0, p3

    .line 2115447
    check-cast v0, LX/CxG;

    invoke-interface {v0, p2}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v0

    .line 2115448
    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v2

    .line 2115449
    sget-object v3, LX/EOr;->a:[I

    invoke-virtual {p1}, LX/20X;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2115450
    :goto_0
    return-void

    .line 2115451
    :pswitch_0
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->g:LX/CvY;

    sget-object v7, LX/8ch;->SHARE:LX/8ch;

    sget-object v8, LX/8ch;->SHARE:LX/8ch;

    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->m()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v2, v0, v8, v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/8ch;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    move-object v6, v2

    move v8, v0

    move-object v9, p2

    invoke-virtual/range {v5 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2115452
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->b:LX/1Kf;

    const/4 v6, 0x0

    sget-object v7, LX/21D;->NEWSFEED:LX/21D;

    const-string v8, "pulseContextFooterButtons"

    invoke-static {v7, v8}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    invoke-static {v1}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v8

    invoke-virtual {v8}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v7

    const/16 v8, 0x6dc

    iget-object v9, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->c:Landroid/app/Activity;

    invoke-interface {v5, v6, v7, v8, v9}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2115453
    goto :goto_0

    .line 2115454
    :pswitch_1
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->g:LX/CvY;

    sget-object v7, LX/8ch;->OPEN_LINK:LX/8ch;

    sget-object v8, LX/8ch;->OPEN_LINK:LX/8ch;

    invoke-virtual {p2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->m()LX/0am;

    move-result-object v6

    invoke-virtual {v6}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v2, v0, v8, v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILX/8ch;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    move-object v6, v2

    move v8, v0

    move-object v9, p2

    invoke-virtual/range {v5 .. v10}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2115455
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->c:Landroid/app/Activity;

    invoke-interface {v5, v6, v7}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2115456
    goto :goto_0

    .line 2115457
    :pswitch_2
    invoke-static {p0, v1, p2, p3}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->a(Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;Ljava/lang/String;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2115437
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    check-cast p3, LX/CxV;

    .line 2115438
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    new-instance v1, LX/EOq;

    invoke-direct {v1, p0, p2, p3}, LX/EOq;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;LX/CxV;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2115439
    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2115440
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2115441
    new-instance v1, LX/EOt;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->h:LX/0wM;

    const p1, 0x7f020781

    const p3, -0xa76f01

    invoke-virtual {v2, p1, p3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const p1, 0x7f082285

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, LX/EOt;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 2115442
    :goto_0
    move-object v0, v1

    .line 2115443
    return-object v0

    .line 2115444
    :cond_0
    const v1, 0x7f0102a3

    const v2, -0x6e685d

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v2

    .line 2115445
    new-instance v1, LX/EOt;

    iget-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseContextFooterButtonsPartDefinition;->h:LX/0wM;

    const p3, 0x7f020781

    invoke-virtual {p1, p3, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const p1, 0x7f082284

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, LX/EOt;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x50985c9f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2115432
    check-cast p2, LX/EOt;

    check-cast p4, LX/3b7;

    .line 2115433
    sget-object v1, LX/20X;->SAVE:LX/20X;

    invoke-virtual {p4, v1}, LX/3b7;->a(LX/20X;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 2115434
    iget-object v2, p2, LX/EOt;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2115435
    iget-object v2, p2, LX/EOt;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(Ljava/lang/CharSequence;)V

    .line 2115436
    const/16 v1, 0x1f

    const v2, -0x87decf7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
