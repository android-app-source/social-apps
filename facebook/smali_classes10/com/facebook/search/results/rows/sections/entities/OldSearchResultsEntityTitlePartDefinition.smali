.class public Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/CharSequence;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107761
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2107762
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;->a:LX/0Ot;

    .line 2107763
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;
    .locals 4

    .prologue
    .line 2107773
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    monitor-enter v1

    .line 2107774
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107775
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107776
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107777
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107778
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    const/16 p0, 0x3512

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;-><init>(LX/0Ot;)V

    .line 2107779
    move-object v0, v3

    .line 2107780
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107781
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107782
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107783
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2107767
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2107768
    invoke-static {p2}, LX/ELM;->g(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2107769
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2107770
    :goto_0
    return-object v0

    .line 2107771
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2107772
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;->a:LX/0Ot;

    invoke-static {v0, v1, v2}, LX/ELM;->a(ZLjava/lang/String;LX/0Ot;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x51549b3a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2107764
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2107765
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2107766
    const/16 v1, 0x1f

    const v2, -0x6da6f5c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
