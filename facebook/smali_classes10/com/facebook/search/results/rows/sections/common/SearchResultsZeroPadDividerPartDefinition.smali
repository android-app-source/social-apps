.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements LX/1Vh;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Void:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<TVoid;TVoid;",
        "LX/1PW;",
        "Ljava/lang/Object;",
        ">;",
        "LX/1Vh;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static final b:LX/1Ua;

.field private static d:LX/0Xm;


# instance fields
.field private final c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, -0x3f400000    # -6.0f

    .line 2106358
    const v0, 0x7f0312ac

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->a:LX/1Cz;

    .line 2106359
    invoke-static {}, LX/1UY;->c()LX/1UY;

    move-result-object v0

    .line 2106360
    iput v1, v0, LX/1UY;->b:F

    .line 2106361
    move-object v0, v0

    .line 2106362
    iput v1, v0, LX/1UY;->c:F

    .line 2106363
    move-object v0, v0

    .line 2106364
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->b:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106365
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2106366
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 2106367
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;
    .locals 4

    .prologue
    .line 2106368
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    monitor-enter v1

    .line 2106369
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106370
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106371
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106372
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106373
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/PaddingPartDefinition;)V

    .line 2106374
    move-object v0, p0

    .line 2106375
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106376
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106377
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106378
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2106379
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<",
            "LX/1PW;",
            ">;TVoid;",
            "LX/1PW;",
            ")TVoid;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2106380
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v1, LX/3aw;

    sget-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->b:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    const/4 v4, 0x0

    invoke-direct {v1, v5, v2, v3, v4}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2106381
    return-object v5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVoid;)Z"
        }
    .end annotation

    .prologue
    .line 2106382
    const/4 v0, 0x1

    return v0
.end method

.method public final b()LX/1X8;
    .locals 1

    .prologue
    .line 2106383
    sget-object v0, LX/1X8;->GAP_PART_DEFINITION:LX/1X8;

    return-object v0
.end method
