.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Ljava/lang/CharSequence;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1X6;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/EMx;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2111634
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    sget-object v3, LX/1X9;->BOX:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EMx;LX/1V0;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2111664
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2111665
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->e:LX/EMx;

    .line 2111666
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->f:LX/1V0;

    .line 2111667
    return-void
.end method

.method private a(LX/1De;Ljava/lang/CharSequence;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Ljava/lang/CharSequence;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2111650
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->e:LX/EMx;

    const/4 v1, 0x0

    .line 2111651
    new-instance v2, LX/EMw;

    invoke-direct {v2, v0}, LX/EMw;-><init>(LX/EMx;)V

    .line 2111652
    sget-object v3, LX/EMx;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EMv;

    .line 2111653
    if-nez v3, :cond_0

    .line 2111654
    new-instance v3, LX/EMv;

    invoke-direct {v3}, LX/EMv;-><init>()V

    .line 2111655
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EMv;->a$redex0(LX/EMv;LX/1De;IILX/EMw;)V

    .line 2111656
    move-object v2, v3

    .line 2111657
    move-object v1, v2

    .line 2111658
    move-object v0, v1

    .line 2111659
    iget-object v1, v0, LX/EMv;->a:LX/EMw;

    iput-object p2, v1, LX/EMw;->a:Ljava/lang/CharSequence;

    .line 2111660
    iget-object v1, v0, LX/EMv;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2111661
    move-object v0, v0

    .line 2111662
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2111663
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    sget-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->d:LX/1X6;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;
    .locals 6

    .prologue
    .line 2111639
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;

    monitor-enter v1

    .line 2111640
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111641
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111642
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111643
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111644
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EMx;->a(LX/0QB;)LX/EMx;

    move-result-object v4

    check-cast v4, LX/EMx;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;-><init>(Landroid/content/Context;LX/EMx;LX/1V0;)V

    .line 2111645
    move-object v0, p0

    .line 2111646
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111647
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111648
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111649
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2111668
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->a(LX/1De;Ljava/lang/CharSequence;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2111638
    check-cast p2, Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTimelineHeaderCardComponentPartDefinition;->a(LX/1De;Ljava/lang/CharSequence;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111636
    check-cast p1, Ljava/lang/CharSequence;

    .line 2111637
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2111635
    const/4 v0, 0x0

    return-object v0
.end method
