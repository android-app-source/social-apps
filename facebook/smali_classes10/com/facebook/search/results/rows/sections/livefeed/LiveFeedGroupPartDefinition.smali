.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsLiveFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;

.field private final c:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGapPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;

.field private final f:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGapPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111949
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2111950
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;

    .line 2111951
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;

    .line 2111952
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;

    .line 2111953
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    .line 2111954
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGapPartDefinition;

    .line 2111955
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->f:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;

    .line 2111956
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;
    .locals 10

    .prologue
    .line 2111957
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;

    monitor-enter v1

    .line 2111958
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111959
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111960
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111961
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111962
    new-instance v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGapPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGapPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGapPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGapPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;)V

    .line 2111963
    move-object v0, v3

    .line 2111964
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111965
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111966
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111967
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 2111968
    invoke-static {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2111969
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2111970
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2111971
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111972
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainVideoAttachmentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111973
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainPhotoAttachmentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111974
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->f:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111975
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111976
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGapPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111977
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111978
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedGroupPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
