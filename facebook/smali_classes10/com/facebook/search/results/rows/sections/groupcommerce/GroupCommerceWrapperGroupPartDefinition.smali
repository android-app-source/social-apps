.class public Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111036
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2111037
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    .line 2111038
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;
    .locals 4

    .prologue
    .line 2111039
    const-class v1, Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;

    monitor-enter v1

    .line 2111040
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111041
    sput-object v2, Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111044
    new-instance p0, Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;-><init>(Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;)V

    .line 2111045
    move-object v0, p0

    .line 2111046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2111050
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/Cxo;

    .line 2111051
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;->a:Lcom/facebook/feedplugins/groupcommerce/GroupCommerceProductFeedGroupPartDefinition;

    new-instance v2, LX/DDe;

    .line 2111052
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2111053
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    new-instance v3, LX/EMc;

    invoke-direct {v3, p0, p3, p2}, LX/EMc;-><init>(Lcom/facebook/search/results/rows/sections/groupcommerce/GroupCommerceWrapperGroupPartDefinition;LX/Cxo;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-direct {v2, v0, v3}, LX/DDe;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111054
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111055
    const/4 v0, 0x1

    return v0
.end method
