.class public Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsMediaCombinedModuleInterfaces$SearchResultsMediaCombinedModule;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1X6;

.field private static i:LX/0Xm;


# instance fields
.field private final e:LX/EOd;

.field private final f:LX/1V0;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/CvY;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2114928
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, LX/1Ua;->e:LX/1Ua;

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EOd;LX/1V0;LX/0Or;LX/CvY;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/EOd;",
            "LX/1V0;",
            "LX/0Or",
            "<",
            "LX/23R;",
            ">;",
            "LX/CvY;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2114929
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2114930
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->e:LX/EOd;

    .line 2114931
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->f:LX/1V0;

    .line 2114932
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->g:LX/0Or;

    .line 2114933
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->h:LX/CvY;

    .line 2114934
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsMediaCombinedModuleInterfaces$SearchResultsMediaCombinedModule;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2114935
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->e:LX/EOd;

    const/4 v1, 0x0

    .line 2114936
    new-instance v2, LX/EOc;

    invoke-direct {v2, v0}, LX/EOc;-><init>(LX/EOd;)V

    .line 2114937
    sget-object v3, LX/EOd;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EOb;

    .line 2114938
    if-nez v3, :cond_0

    .line 2114939
    new-instance v3, LX/EOb;

    invoke-direct {v3}, LX/EOb;-><init>()V

    .line 2114940
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EOb;->a$redex0(LX/EOb;LX/1De;IILX/EOc;)V

    .line 2114941
    move-object v2, v3

    .line 2114942
    move-object v1, v2

    .line 2114943
    move-object v0, v1

    .line 2114944
    const/4 v1, 0x3

    .line 2114945
    iget-object v2, v0, LX/EOb;->a:LX/EOc;

    iput v1, v2, LX/EOc;->c:I

    .line 2114946
    iget-object v2, v0, LX/EOb;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2114947
    move-object v0, v0

    .line 2114948
    iget-object v1, v0, LX/EOb;->a:LX/EOc;

    iput-object p2, v1, LX/EOc;->a:LX/CzL;

    .line 2114949
    iget-object v1, v0, LX/EOb;->d:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2114950
    move-object v0, v0

    .line 2114951
    new-instance v1, LX/EOf;

    invoke-direct {v1, p0, p2, p3}, LX/EOf;-><init>(Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;LX/CzL;LX/1Pn;)V

    move-object v1, v1

    .line 2114952
    iget-object v2, v0, LX/EOb;->a:LX/EOc;

    iput-object v1, v2, LX/EOc;->b:LX/EOS;

    .line 2114953
    move-object v0, v0

    .line 2114954
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2114955
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    sget-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->d:LX/1X6;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;
    .locals 9

    .prologue
    .line 2114956
    const-class v1, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;

    monitor-enter v1

    .line 2114957
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114958
    sput-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114959
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114960
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114961
    new-instance v3, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/EOd;->a(LX/0QB;)LX/EOd;

    move-result-object v5

    check-cast v5, LX/EOd;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    const/16 v7, 0xf2f

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v8

    check-cast v8, LX/CvY;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;-><init>(Landroid/content/Context;LX/EOd;LX/1V0;LX/0Or;LX/CvY;)V

    .line 2114962
    move-object v0, v3

    .line 2114963
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114964
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114965
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114966
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static synthetic b(Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;)LX/CvY;
    .locals 1

    .prologue
    .line 2114967
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->h:LX/CvY;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2114968
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2114969
    check-cast p2, LX/CzL;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsMediaCombinedGridComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2114970
    check-cast p1, LX/CzL;

    .line 2114971
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2114972
    const/4 v0, 0x0

    return-object v0
.end method
