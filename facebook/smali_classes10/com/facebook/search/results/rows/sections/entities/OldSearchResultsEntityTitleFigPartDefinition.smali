.class public Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/CharSequence;",
        "LX/1PW;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107737
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2107738
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;->a:LX/0Ot;

    .line 2107739
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;->b:LX/0Ot;

    .line 2107740
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;
    .locals 5

    .prologue
    .line 2107741
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    monitor-enter v1

    .line 2107742
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107743
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107744
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107745
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107746
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;

    const/16 v4, 0x3512

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0xb2c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2107747
    move-object v0, v3

    .line 2107748
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107749
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107750
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107751
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2107752
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2107753
    invoke-static {p2}, LX/ELM;->g(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2107754
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2107755
    :goto_0
    return-object v0

    .line 2107756
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2107757
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;->a:LX/0Ot;

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitleFigPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3my;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->mq()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    invoke-static {v1, v2, v3, v0}, LX/ELM;->a(ZLjava/lang/String;LX/0Ot;Z)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x5242dbbf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2107758
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 2107759
    invoke-virtual {p4, p2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2107760
    const/16 v1, 0x1f

    const v2, 0x73552a46

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
