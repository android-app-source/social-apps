.class public Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116224
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2116225
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;->a:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;

    .line 2116226
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;->b:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;

    .line 2116227
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;
    .locals 5

    .prologue
    .line 2116228
    const-class v1, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;

    monitor-enter v1

    .line 2116229
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116230
    sput-object v2, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116231
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116232
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116233
    new-instance p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;)V

    .line 2116234
    move-object v0, p0

    .line 2116235
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116236
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116237
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116238
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2116239
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2116240
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;->b:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsContentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2116241
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsItemPartDefinition;->a:Lcom/facebook/search/results/rows/sections/relatednews/SearchResultsRelatedNewsTrendingPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2116242
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116243
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;)Z

    move-result v0

    return v0
.end method
