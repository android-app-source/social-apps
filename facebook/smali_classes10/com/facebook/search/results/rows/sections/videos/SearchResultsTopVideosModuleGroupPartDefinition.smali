.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxj;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/video/SearchResultsTopVideosModuleInterfaces$SearchResultsTopVideosModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117373
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2117374
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    .line 2117375
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    .line 2117376
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;
    .locals 5

    .prologue
    .line 2117377
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;

    monitor-enter v1

    .line 2117378
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117379
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117380
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117381
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117382
    new-instance p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;)V

    .line 2117383
    move-object v0, p0

    .line 2117384
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117385
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117386
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117387
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2117388
    check-cast p2, LX/CzL;

    .line 2117389
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    .line 2117390
    iget-object v1, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v1, v1

    .line 2117391
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117392
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsTopVideosModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosModuleGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117393
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2117394
    check-cast p1, LX/CzL;

    .line 2117395
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
