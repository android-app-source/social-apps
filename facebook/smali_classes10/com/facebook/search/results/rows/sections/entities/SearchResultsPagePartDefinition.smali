.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d0;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

.field private final g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition",
            "<TE;",
            "LX/8d0;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;

.field public final i:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109539
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2109540
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 2109541
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    .line 2109542
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2109543
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 2109544
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 2109545
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->f:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2109546
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    .line 2109547
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->h:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;

    .line 2109548
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->i:LX/0Uh;

    .line 2109549
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;
    .locals 13

    .prologue
    .line 2109550
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;

    monitor-enter v1

    .line 2109551
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109552
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109553
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109554
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109555
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;LX/0Uh;)V

    .line 2109556
    move-object v0, v3

    .line 2109557
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109558
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109559
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109560
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/CzL;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2109561
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2109562
    invoke-static {v0}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 2109563
    invoke-static {v0}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->i:LX/0Uh;

    invoke-static {v0}, LX/ELM;->a(LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2109564
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v0

    .line 2109565
    :goto_0
    return-object v0

    .line 2109566
    :cond_1
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109567
    check-cast v0, LX/8d0;

    .line 2109568
    invoke-interface {v0}, LX/8d0;->v()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_2

    invoke-interface {v0}, LX/8d0;->v()LX/0Px;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    :goto_1
    move-object v0, p0

    .line 2109569
    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2109570
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2109571
    check-cast p2, LX/CzL;

    .line 2109572
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v1, LX/EJI;

    sget-object v2, LX/3ap;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109573
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/SearchResultsVerifiedNamePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109574
    invoke-direct {p0, p2}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->a(LX/CzL;)Ljava/lang/String;

    move-result-object v1

    .line 2109575
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109576
    iget-object v0, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2109577
    invoke-static {v0}, LX/8eM;->c(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    .line 2109578
    invoke-static {v0}, LX/ELM;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->i:LX/0Uh;

    invoke-static {v0}, LX/ELM;->a(LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2109579
    :cond_0
    const/4 v0, 0x1

    invoke-static {p2, v0}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v0

    .line 2109580
    :goto_0
    move-object v0, v0

    .line 2109581
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109582
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 2109583
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109584
    check-cast v0, LX/8d0;

    .line 2109585
    invoke-interface {v0}, LX/8d0;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object p3

    .line 2109586
    if-eqz p3, :cond_2

    invoke-static {p3}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object p3

    :goto_1
    move-object v0, p3

    .line 2109587
    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109588
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->f:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2109589
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109590
    check-cast v0, LX/8d0;

    invoke-interface {v0}, LX/8d0;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LX/ELM;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109591
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109592
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPagePartDefinition;->h:Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109593
    const/4 v0, 0x0

    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    const/4 p3, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2109594
    const/4 v0, 0x1

    return v0
.end method
