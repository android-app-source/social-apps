.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A4u;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation
.end field

.field private static l:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/TextPartDefinition;

.field public final f:LX/8i7;

.field private final g:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

.field private final h:LX/23s;

.field private final i:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition",
            "<TE;",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2117742
    const v0, 0x7f031344

    invoke-static {v0}, LX/1Cz;->a(I)LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/8i7;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;LX/23s;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117743
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2117744
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 2117745
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    .line 2117746
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->d:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    .line 2117747
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2117748
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->f:LX/8i7;

    .line 2117749
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->g:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    .line 2117750
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->h:LX/23s;

    .line 2117751
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->i:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    .line 2117752
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->j:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    .line 2117753
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->k:LX/0ad;

    .line 2117754
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;
    .locals 14

    .prologue
    .line 2117755
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;

    monitor-enter v1

    .line 2117756
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117757
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117758
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117759
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117760
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, LX/8i7;->a(LX/0QB;)LX/8i7;

    move-result-object v8

    check-cast v8, LX/8i7;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    invoke-static {v0}, LX/23s;->a(LX/0QB;)LX/23s;

    move-result-object v10

    check-cast v10, LX/23s;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;LX/8i7;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;LX/23s;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;LX/0ad;)V

    .line 2117761
    move-object v0, v3

    .line 2117762
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117763
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117764
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117765
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2117766
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2117767
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 2117768
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->k:LX/0ad;

    invoke-static {v0}, LX/7Ax;->c(LX/0ad;)Z

    move-result v2

    .line 2117769
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117770
    check-cast v0, LX/A4u;

    .line 2117771
    invoke-interface {v0}, LX/A4u;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2117772
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 2117773
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2117774
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2117775
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 2117776
    const v4, 0x7f0d02c4

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    if-eqz v2, :cond_0

    .line 2117777
    invoke-static {v1}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    .line 2117778
    if-eqz v6, :cond_3

    invoke-interface {v6}, LX/175;->a()Ljava/lang/String;

    move-result-object v6

    :goto_0
    move-object v1, v6

    .line 2117779
    :goto_1
    invoke-interface {p1, v4, v5, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2117780
    const v1, 0x7f0d02c3

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->e:Lcom/facebook/multirow/parts/TextPartDefinition;

    if-eqz v2, :cond_1

    .line 2117781
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 2117782
    if-nez v0, :cond_4

    .line 2117783
    const/4 v0, 0x0

    .line 2117784
    :goto_2
    move-object v0, v0

    .line 2117785
    :goto_3
    invoke-interface {p1, v1, v4, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2117786
    const v0, 0x7f0d2bac

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->g:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoMetaTextPartDefinition;

    invoke-interface {p1, v0, v1, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2117787
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117788
    check-cast v0, LX/A4u;

    invoke-interface {v0}, LX/A4u;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2117789
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2117790
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2117791
    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object v0

    move-object v1, v0

    .line 2117792
    const v0, 0x7f0d2ca1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->c:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerPartDefinition;

    invoke-interface {p1, v0, v2, v1}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2117793
    const v0, 0x7f0d177e

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->d:Lcom/facebook/multirow/parts/ExpandingFixedAspectRatioFrameLayoutPartDefinition;

    const-wide v4, 0x3ff871c720000000L    # 1.5277777910232544

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2117794
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->h:LX/23s;

    .line 2117795
    iget-object v0, v1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117796
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Po;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    invoke-interface {v3}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/23s;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;)LX/04H;

    move-result-object v0

    .line 2117797
    sget-object v2, LX/04H;->ELIGIBLE:LX/04H;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->i:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLauncherPartDefinition;

    :goto_4
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2117798
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->b:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v1, LX/EJI;

    sget-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPartDefinition;->b:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2117799
    const/4 v0, 0x0

    return-object v0

    .line 2117800
    :cond_0
    invoke-interface {v0}, LX/A4u;->av()LX/175;

    move-result-object v1

    .line 2117801
    if-eqz v1, :cond_6

    invoke-interface {v1}, LX/175;->a()Ljava/lang/String;

    move-result-object v1

    :goto_5
    move-object v1, v1

    .line 2117802
    goto/16 :goto_1

    .line 2117803
    :cond_1
    invoke-interface {v0}, LX/A4u;->aB()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v2

    .line 2117804
    if-nez v2, :cond_7

    .line 2117805
    const/4 v2, 0x0

    .line 2117806
    :goto_6
    move-object v0, v2

    .line 2117807
    goto/16 :goto_3

    .line 2117808
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->j:Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoFullscreenLauncherPartDefinition;

    goto :goto_4

    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->f:LX/8i7;

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v5}, LX/8i7;->b(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto/16 :goto_2

    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    :cond_7
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;->b()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->f:LX/8i7;

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v5}, LX/8i7;->b(Landroid/text/SpannableStringBuilder;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto :goto_6

    :cond_8
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;->c()Ljava/lang/String;

    move-result-object v2

    goto :goto_6
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2117809
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 2117810
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117811
    check-cast v0, LX/A4u;

    .line 2117812
    invoke-interface {v0}, LX/A4u;->N()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2117813
    if-eqz v2, :cond_0

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2117814
    :goto_0
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoWithAutoplayPartDefinition;->k:LX/0ad;

    sget-short v2, LX/100;->bk:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2117815
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2117816
    goto :goto_1
.end method
