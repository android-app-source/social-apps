.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceGridItemImageView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2104702
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2104703
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2104694
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2104695
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2104696
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2104697
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 2104698
    invoke-super {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onMeasure(II)V

    .line 2104699
    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceGridItemImageView;->getMeasuredWidth()I

    move-result v0

    .line 2104700
    invoke-virtual {p0, v0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceGridItemImageView;->setMeasuredDimension(II)V

    .line 2104701
    return-void
.end method
