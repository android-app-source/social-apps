.class public Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111882
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2111883
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    .line 2111884
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    .line 2111885
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;
    .locals 5

    .prologue
    .line 2111886
    const-class v1, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    monitor-enter v1

    .line 2111887
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111888
    sput-object v2, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111889
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111890
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111891
    new-instance p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;)V

    .line 2111892
    move-object v0, p0

    .line 2111893
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111894
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111895
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111896
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2111897
    check-cast p2, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    check-cast p3, LX/1Pn;

    const/4 v3, 0x0

    .line 2111898
    invoke-static {p2}, LX/8eM;->g(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Ljava/lang/String;

    move-result-object v0

    .line 2111899
    check-cast p3, LX/CxV;

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v1, v2, :cond_0

    .line 2111900
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitlePartDefinition;

    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v1

    invoke-static {v1, p2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111901
    :goto_0
    return-object v3

    .line 2111902
    :cond_0
    new-instance v1, LX/EMl;

    invoke-direct {v1, v0, v3}, LX/EMl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2111903
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/header/SearchResultsVideosHeaderPartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111904
    check-cast p1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    .line 2111905
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
