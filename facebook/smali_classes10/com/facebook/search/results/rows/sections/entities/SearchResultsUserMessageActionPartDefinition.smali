.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d1;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/Integer;

.field public static final b:Ljava/lang/Integer;

.field private static g:LX/0Xm;


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final f:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2110912
    invoke-static {}, LX/10A;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->a:Ljava/lang/Integer;

    .line 2110913
    const v0, 0x7f082267

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->b:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2110888
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2110889
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->c:LX/0Ot;

    .line 2110890
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->d:LX/0Ot;

    .line 2110891
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2110892
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->f:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2110893
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;
    .locals 7

    .prologue
    .line 2110901
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;

    monitor-enter v1

    .line 2110902
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110903
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110904
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110905
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2110906
    new-instance v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;

    const/16 v3, 0x32d4

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v3, 0x2eb

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-direct {v5, v6, p0, v3, v4}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;-><init>(LX/0Ot;LX/0Ot;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;)V

    .line 2110907
    move-object v0, v5

    .line 2110908
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110909
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110910
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110911
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;LX/CzL;LX/1Pn;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    .line 2110914
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2110915
    check-cast v0, LX/8d1;

    .line 2110916
    sget-object v1, LX/0ax;->ai:Ljava/lang/String;

    invoke-interface {v0}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2110917
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2110918
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CvY;

    move-object v1, p2

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-static {p1}, LX/CvY;->a(LX/CzL;)LX/CvJ;

    move-result-object v2

    move-object v3, p2

    check-cast v3, LX/CxP;

    invoke-interface {v3, p1}, LX/CxP;->b(LX/CzL;)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/CvY;

    check-cast p2, LX/CxV;

    invoke-interface {p2}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v5

    invoke-static {v5, p1}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzL;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CvJ;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2110919
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2110897
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    .line 2110898
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->e:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/EMb;

    invoke-direct {v1, p0, p2, p3}, LX/EMb;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;LX/CzL;LX/1Pn;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2110899
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->f:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2110900
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x5690b7a6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2110894
    check-cast p4, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    .line 2110895
    sget-object v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserMessageActionPartDefinition;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v2, -0x6f6b64

    invoke-virtual {p4, v1, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a(II)V

    .line 2110896
    const/16 v1, 0x1f

    const v2, 0x4e87c236

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2110885
    check-cast p4, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    .line 2110886
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 2110887
    return-void
.end method
