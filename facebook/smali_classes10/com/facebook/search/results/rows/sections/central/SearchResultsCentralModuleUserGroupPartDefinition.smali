.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cwx;",
        ":",
        "LX/CxA;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104136
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104137
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;

    .line 2104138
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;
    .locals 4

    .prologue
    .line 2104121
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;

    monitor-enter v1

    .line 2104122
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104123
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104124
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104125
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104126
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;)V

    .line 2104127
    move-object v0, p0

    .line 2104128
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104129
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104130
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104131
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2104132
    check-cast p2, LX/CzL;

    .line 2104133
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralModuleUserGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserItemPartDefinition;

    invoke-static {p1, v0, p2}, LX/EJ4;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/CzL;)V

    .line 2104134
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104135
    const/4 v0, 0x1

    return v0
.end method
