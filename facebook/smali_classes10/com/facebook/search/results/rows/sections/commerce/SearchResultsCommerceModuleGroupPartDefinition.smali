.class public Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxG;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/commerce/SearchResultsCommerceModuleInterfaces$SearchResultsCommerceModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105731
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2105732
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    .line 2105733
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;

    .line 2105734
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;

    .line 2105735
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2105736
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;
    .locals 7

    .prologue
    .line 2105737
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;

    monitor-enter v1

    .line 2105738
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105739
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105740
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105741
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105742
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V

    .line 2105743
    move-object v0, p0

    .line 2105744
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105745
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105746
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105747
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2105748
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2105749
    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v5

    .line 2105750
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v5, v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->COMMERCE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v5, v0, :cond_3

    :cond_0
    move v2, v4

    .line 2105751
    :goto_0
    if-nez v2, :cond_4

    move-object v0, p3

    check-cast v0, LX/CxG;

    .line 2105752
    iget-object v6, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v6, v6

    .line 2105753
    invoke-interface {v0, v6}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_4

    .line 2105754
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleTitleTogglePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105755
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleItemSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105756
    if-eqz v2, :cond_2

    .line 2105757
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105758
    :cond_2
    return-object v1

    :cond_3
    move v2, v3

    .line 2105759
    goto :goto_0

    .line 2105760
    :cond_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v5, v0, :cond_5

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->COMMERCE_PRODUCTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v5, v0, :cond_5

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SALE_POST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v5, v0, :cond_1

    move-object v0, p3

    check-cast v0, LX/CxG;

    invoke-interface {v0, p2}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v0

    if-nez v0, :cond_1

    .line 2105761
    :cond_5
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2105762
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2105763
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aa()I

    move-result v5

    .line 2105764
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 2105765
    new-instance v7, LX/EMg;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v8

    if-lez v5, :cond_6

    if-eqz v2, :cond_6

    const v0, 0x7f0f010f

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v4, v3

    invoke-virtual {v6, v0, v5, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v7, v8, v0, v3}, LX/EMg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2105766
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/header/SearchResultsCommerceHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, v7}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    move-object v0, v1

    .line 2105767
    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2105768
    check-cast p1, LX/CzL;

    .line 2105769
    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_C2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_COMBINED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
