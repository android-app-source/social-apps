.class public Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A24;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/CustomLinearLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:LX/0SG;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field private final e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/multirow/parts/TextPartDefinition;

.field private final g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final i:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;


# direct methods
.method public constructor <init>(LX/0SG;LX/0Or;LX/0Or;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/TimeZone;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;",
            "Lcom/facebook/multirow/parts/TextPartDefinition;",
            "Lcom/facebook/multirow/parts/TextAppearancePartDefinition;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103210
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2103211
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a:LX/0SG;

    .line 2103212
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->b:LX/0Or;

    .line 2103213
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->c:LX/0Or;

    .line 2103214
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2103215
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    .line 2103216
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2103217
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    .line 2103218
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2103219
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->i:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    .line 2103220
    return-void
.end method

.method private static a(LX/A24;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2103221
    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->b(LX/A24;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103222
    invoke-interface {p0}, LX/A24;->Y()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;->a()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    .line 2103223
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/A24;->T()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;
    .locals 13

    .prologue
    .line 2103224
    const-class v1, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;

    monitor-enter v1

    .line 2103225
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103226
    sput-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103227
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103228
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103229
    new-instance v3, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    const/16 v5, 0xc

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x161a

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;-><init>(LX/0SG;LX/0Or;LX/0Or;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;Lcom/facebook/multirow/parts/TextAppearancePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;)V

    .line 2103230
    move-object v0, v3

    .line 2103231
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103232
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103233
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103234
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/A24;Ljava/util/Calendar;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 2103235
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a(LX/A24;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->b(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2103236
    :cond_0
    :goto_0
    return-object v0

    .line 2103237
    :cond_1
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a(Ljava/util/Calendar;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2103238
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->b(LX/A24;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2103239
    const v0, 0x7f082294

    new-array v1, v1, [Ljava/lang/Object;

    invoke-interface {p1}, LX/A24;->aS()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2103240
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2103241
    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 2103242
    invoke-static {v2, v3}, LX/1lQ;->h(J)J

    move-result-wide v2

    long-to-int v0, v2

    .line 2103243
    if-nez v0, :cond_3

    move v0, v1

    .line 2103244
    :cond_3
    const v2, 0x7f0f010e

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v6

    invoke-virtual {p3, v2, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;Ljava/util/Calendar;Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2103245
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a(Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->b(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103246
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082293

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2103247
    :goto_0
    return-object v0

    .line 2103248
    :cond_0
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 2103249
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->c()I

    move-result v1

    if-eqz v1, :cond_1

    .line 2103250
    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->c()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2103251
    :cond_1
    const/16 v1, 0x8

    .line 2103252
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->c()I

    move-result v2

    if-eqz v2, :cond_2

    .line 2103253
    const/16 v1, 0xc

    .line 2103254
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->a()I

    move-result v2

    if-nez v2, :cond_3

    .line 2103255
    or-int/lit8 v1, v1, 0x20

    .line 2103256
    :cond_3
    move v1, v1

    .line 2103257
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {p2, v2, v3, v1}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/Calendar;)Z
    .locals 2

    .prologue
    .line 2103258
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    return v0
.end method

.method private static b(LX/A24;)Z
    .locals 1

    .prologue
    .line 2103259
    invoke-interface {p0}, LX/A24;->Y()Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$SearchResultsBirthdayModel$CelebrityBasicInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;)Z
    .locals 1

    .prologue
    .line 2103260
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->b()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->a()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/CustomLinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2103261
    sget-object v0, LX/3bQ;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2103262
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    const/4 v3, 0x1

    const/4 v10, 0x0

    .line 2103263
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2103264
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2103265
    check-cast v0, LX/A24;

    .line 2103266
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a(LX/A24;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v5

    .line 2103267
    new-instance v6, Ljava/util/Date;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/TimeZone;

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->a()I

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->b()I

    move-result v7

    invoke-static {v6, v1, v2, v7}, LX/6RS;->a(Ljava/util/Date;Ljava/util/TimeZone;II)Ljava/util/Calendar;

    move-result-object v1

    .line 2103268
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f082292

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-interface {v0}, LX/A24;->d()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v5, v1, v4}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a(Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;Ljava/util/Calendar;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v3

    invoke-virtual {v2, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2103269
    const v3, 0x7f0d0a0a

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-interface {p1, v3, v5, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103270
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a(LX/A24;Ljava/util/Calendar;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    .line 2103271
    if-eqz v2, :cond_0

    .line 2103272
    const v3, 0x7f0d0ada

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->f:Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-interface {p1, v3, v4, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103273
    :cond_0
    invoke-static {v1}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a(Ljava/util/Calendar;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->b(LX/A24;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2103274
    const v0, 0x7f0d0ada

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->h:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2103275
    new-instance v3, LX/EJ6;

    invoke-direct {v3, p0, v2}, LX/EJ6;-><init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;Landroid/content/Context;)V

    move-object v2, v3

    .line 2103276
    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103277
    const v0, 0x7f0d0ada

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->g:Lcom/facebook/multirow/parts/TextAppearancePartDefinition;

    const v2, 0x7f0e095f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103278
    const v0, 0x7f0d0ada

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->i:Lcom/facebook/multirow/parts/ViewPaddingPartDefinition;

    new-instance v2, LX/1ds;

    const/16 v3, 0x1a

    const/16 v4, 0xc

    invoke-direct {v2, v10, v3, v10, v4}, LX/1ds;-><init>(IIII)V

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2103279
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->e:Lcom/facebook/search/results/rows/sections/background/SearchBackgroundPartDefinition;

    check-cast p3, LX/1Ps;

    invoke-static {p3}, LX/3bQ;->a(LX/1Ps;)LX/1X6;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2103280
    const/4 v0, 0x0

    return-object v0

    .line 2103281
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->a()I

    move-result v2

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2103282
    check-cast p1, LX/CzL;

    .line 2103283
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2103284
    check-cast v0, LX/A24;

    .line 2103285
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a(LX/A24;)Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;

    move-result-object v1

    .line 2103286
    invoke-interface {v0}, LX/A24;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/answer/SearchResultsBirthdayModels$DateFieldsModel;->b()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
