.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuotesGroupPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuotesGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104509
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104510
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuotesGroupPartDefinition;

    .line 2104511
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;

    .line 2104512
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    .line 2104513
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;

    .line 2104514
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;
    .locals 7

    .prologue
    .line 2104515
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;

    monitor-enter v1

    .line 2104516
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104517
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104518
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104519
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104520
    new-instance p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuotesGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuotesGroupPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuotesGroupPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuotesGroupPartDefinition;)V

    .line 2104521
    move-object v0, p0

    .line 2104522
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104523
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104524
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104525
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2104526
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104527
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104528
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2104529
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;

    invoke-static {p1, v1, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->c:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->d:Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuotesGroupPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2104530
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2104531
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104532
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2104533
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2104534
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonPhrasesGroupPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v0

    .line 2104535
    :goto_0
    return v0

    .line 2104536
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v1

    const-class v2, Lcom/facebook/graphql/model/GraphQLQuotesAnalysisItem;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2104537
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseCommonQuotesGroupPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v0

    goto :goto_0

    .line 2104538
    :cond_1
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsMediaGridPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsGroupPartDefinition;->a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104539
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
