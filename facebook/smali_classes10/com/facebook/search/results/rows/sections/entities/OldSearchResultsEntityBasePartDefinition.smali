.class public Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Cxe;",
        ":",
        "LX/Cxd;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/0ad;

.field private final b:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/D0N;


# direct methods
.method public constructor <init>(LX/0ad;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;LX/D0N;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107714
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2107715
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->a:LX/0ad;

    .line 2107716
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    .line 2107717
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    .line 2107718
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->d:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    .line 2107719
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->e:LX/D0N;

    .line 2107720
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;
    .locals 9

    .prologue
    .line 2107721
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    monitor-enter v1

    .line 2107722
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107723
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107724
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107725
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107726
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    invoke-static {v0}, LX/D0N;->b(LX/0QB;)LX/D0N;

    move-result-object v8

    check-cast v8, LX/D0N;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;-><init>(LX/0ad;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;LX/D0N;)V

    .line 2107727
    move-object v0, v3

    .line 2107728
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107729
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107730
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107731
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2107732
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Pn;

    .line 2107733
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityTitlePartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107734
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->c:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->a:LX/0ad;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->e:LX/D0N;

    invoke-static {p2, p3, v1, v2}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBaseFigPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLNode;LX/1Pn;LX/0ad;LX/D0N;)LX/EL8;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107735
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityBasePartDefinition;->d:Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsNodeClickListenerPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2107736
    const/4 v0, 0x0

    return-object v0
.end method
