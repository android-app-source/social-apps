.class public Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2115181
    const/4 v0, 0x0

    const v1, 0x7f030475

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2115182
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2115183
    const v0, 0x7f030475

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2115184
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2115173
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2115174
    invoke-virtual {p0, p3}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2115175
    const v0, 0x7f0d0d62

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2115176
    const v0, 0x7f0d0d63

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2115177
    const v0, 0x7f0d0d64

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2115178
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2115179
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2115180
    return-void
.end method

.method public setEmotionCount(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2115171
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2115172
    return-void
.end method

.method public setEmotionName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2115169
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2115170
    return-void
.end method

.method public setEmotionalDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 2115167
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/EmotionalIconItemView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2115168
    return-void
.end method
