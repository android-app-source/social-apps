.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103859
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2103860
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;->b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    .line 2103861
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2103862
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;
    .locals 5

    .prologue
    .line 2103848
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;

    monitor-enter v1

    .line 2103849
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103850
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103851
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103852
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103853
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;)V

    .line 2103854
    move-object v0, p0

    .line 2103855
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103856
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103857
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103858
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2103827
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2103837
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    .line 2103838
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2103839
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    .line 2103840
    invoke-static {v0}, LX/EJT;->a(Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;)LX/1KL;

    move-result-object v1

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EJT;

    .line 2103841
    iget-boolean v2, v1, LX/EJT;->b:Z

    move v2, v2

    .line 2103842
    if-eqz v2, :cond_0

    .line 2103843
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;->b:Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2103844
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 2103845
    new-instance p2, LX/1X6;

    const/4 p3, 0x0

    invoke-virtual {v1}, LX/EJT;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->b:LX/1Ua;

    :goto_0
    invoke-virtual {v1}, LX/EJT;->a()Z

    move-result p0

    if-eqz p0, :cond_2

    sget-object p0, LX/1X9;->MIDDLE:LX/1X9;

    :goto_1
    invoke-direct {p2, p3, v2, p0}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    move-object v1, p2

    .line 2103846
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2103847
    const/4 v0, 0x0

    return-object v0

    :cond_1
    sget-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->c:LX/1Ua;

    goto :goto_0

    :cond_2
    sget-object p0, LX/1X9;->DIVIDER_BOTTOM_NON_TOP:LX/1X9;

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x25c9d38e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2103831
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pr;

    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 2103832
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2103833
    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    .line 2103834
    invoke-static {v1}, LX/EJT;->a(Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;)LX/1KL;

    move-result-object v2

    invoke-interface {p3, v2, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/EJT;

    .line 2103835
    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {p4, v2, v1}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiAboutTextConvertedPartDefinition;->a(Lcom/facebook/widget/text/BetterTextView;LX/EJT;Ljava/lang/String;)V

    .line 2103836
    const/16 v1, 0x1f

    const v2, 0x7361476d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2103828
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2103829
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2103830
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->n()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
