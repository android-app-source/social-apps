.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/CxG;",
        ":",
        "LX/CxV;",
        ":",
        "LX/Cxa;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EK9;",
        "Landroid/view/View$OnClickListener;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/CvY;

.field private final b:LX/1nD;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/CvY;LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106306
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2106307
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->a:LX/CvY;

    .line 2106308
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->b:LX/1nD;

    .line 2106309
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2106310
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->d:LX/0ad;

    .line 2106311
    return-void
.end method

.method private static a(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2106312
    instance-of v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    if-eqz v0, :cond_1

    .line 2106313
    check-cast p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    .line 2106314
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2106315
    :goto_0
    return-object v0

    .line 2106316
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2106317
    goto :goto_0

    .line 2106318
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2106319
    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/1Pn;LX/EK9;)Landroid/content/Intent;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            "TE;",
            "LX/EK9;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 2106297
    invoke-static/range {p7 .. p7}, LX/EK9;->a(LX/EK9;)Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;

    move-result-object v2

    .line 2106298
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    .line 2106299
    :goto_0
    invoke-static/range {p7 .. p7}, LX/EK9;->d(LX/EK9;)LX/D0L;

    move-result-object v11

    .line 2106300
    if-eqz v0, :cond_2

    if-eqz v11, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->b:LX/1nD;

    move-object/from16 v1, p6

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->s()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v1, p6

    check-cast v1, LX/CxV;

    invoke-static {v1}, LX/EPK;->a(LX/CxV;)LX/8ci;

    move-result-object v4

    instance-of v1, v2, LX/Cz6;

    if-eqz v1, :cond_1

    move-object v1, v2

    check-cast v1, LX/Cz6;

    invoke-interface {v1}, LX/Cz6;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v5

    :goto_1
    check-cast p6, LX/CxV;

    invoke-interface/range {p6 .. p6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v6

    invoke-virtual {v11}, LX/D0L;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v11}, LX/D0L;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v11}, LX/D0L;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v11}, LX/D0L;->d()Z

    move-result v10

    invoke-virtual {v11}, LX/D0L;->e()Z

    move-result v11

    move-object v1, p2

    move-object v2, p3

    invoke-virtual/range {v0 .. v11}, LX/1nD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/search/logging/api/SearchTypeaheadSession;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v0

    .line 2106301
    :goto_2
    return-object v0

    .line 2106302
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2106303
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->b:LX/1nD;

    move-object/from16 v1, p6

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->s()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v1, p6

    check-cast v1, LX/CxV;

    invoke-static {v1}, LX/EPK;->a(LX/CxV;)LX/8ci;

    move-result-object v5

    check-cast p6, LX/CxV;

    invoke-interface/range {p6 .. p6}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-result-object v8

    invoke-interface {v2}, Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;->o()LX/0Px;

    move-result-object v9

    invoke-static {v2}, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->a(Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;)LX/0Px;

    move-result-object v10

    invoke-static/range {p7 .. p7}, LX/EK9;->e(LX/EK9;)Z

    move-result v11

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-virtual/range {v0 .. v11}, LX/1nD;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8ci;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/0Px;LX/0Px;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_2
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;
    .locals 7

    .prologue
    .line 2106286
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    monitor-enter v1

    .line 2106287
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106288
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106289
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106290
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106291
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v3

    check-cast v3, LX/CvY;

    invoke-static {v0}, LX/1nD;->a(LX/0QB;)LX/1nD;

    move-result-object v4

    check-cast v4, LX/1nD;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;-><init>(LX/CvY;LX/1nD;Lcom/facebook/content/SecureContextHelper;LX/0ad;)V

    .line 2106292
    move-object v0, p0

    .line 2106293
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106294
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106295
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106296
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2106304
    check-cast p2, LX/EK9;

    check-cast p3, LX/1Pn;

    .line 2106305
    new-instance v0, LX/EK8;

    invoke-direct {v0, p0, p3, p2}, LX/EK8;-><init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;LX/1Pn;LX/EK9;)V

    return-object v0
.end method

.method public final a(LX/1Pn;LX/EK9;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "LX/EK9;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2106264
    iget-object v8, p2, LX/EK9;->a:Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;

    .line 2106265
    instance-of v0, v8, LX/Cz6;

    if-eqz v0, :cond_2

    move-object v0, v8

    check-cast v0, LX/Cz6;

    invoke-interface {v0}, LX/Cz6;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v0, v1, :cond_2

    .line 2106266
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2106267
    :goto_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->d:LX/0ad;

    sget-short v3, LX/100;->a:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 2106268
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v1, v0, :cond_3

    const/4 v0, 0x1

    move v3, v0

    .line 2106269
    :goto_1
    if-nez v4, :cond_0

    if-eqz v3, :cond_4

    :cond_0
    move-object v0, p1

    .line 2106270
    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v2

    .line 2106271
    :goto_2
    if-eqz v3, :cond_6

    invoke-static {v2}, LX/7BG;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2106272
    :goto_3
    invoke-interface {v8}, Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;->r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    move-object v0, p1

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    .line 2106273
    iget-object v5, v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v5, v5

    .line 2106274
    move-object v0, p0

    move-object v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLObjectType;LX/1Pn;LX/EK9;)Landroid/content/Intent;

    move-result-object v1

    .line 2106275
    invoke-static {v1}, LX/1nD;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 2106276
    check-cast v0, LX/Cxa;

    invoke-interface {v0}, LX/Cxa;->u()V

    .line 2106277
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2106278
    iget-object v0, p2, LX/EK9;->c:LX/3ag;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsSeeMoreClickListenerPartDefinition;->a:LX/CvY;

    check-cast p1, LX/CxV;

    invoke-interface {v0, v1, p1, v8}, LX/3ag;->a(LX/CvY;LX/CxV;Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;)V

    .line 2106279
    return-void

    .line 2106280
    :cond_2
    invoke-interface {v8}, Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;->q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    goto :goto_0

    :cond_3
    move v3, v2

    .line 2106281
    goto :goto_1

    .line 2106282
    :cond_4
    invoke-interface {v8}, Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;->k()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;->k()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2106283
    invoke-interface {v8}, Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;->k()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    goto :goto_2

    .line 2106284
    :cond_5
    invoke-interface {p1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v2, p2, LX/EK9;->d:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 2106285
    :cond_6
    invoke-interface {v8}, Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;->n()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v3, v0

    goto :goto_3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x226e523d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2106261
    check-cast p2, Landroid/view/View$OnClickListener;

    .line 2106262
    invoke-virtual {p4, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2106263
    const/16 v1, 0x1f

    const v2, 0x7df064f5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2106259
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2106260
    return-void
.end method
