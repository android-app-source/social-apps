.class public Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxh;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsRelatedSharesExternalUrlInterfaces$SearchResultsRelatedSharesExternalUrl;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static g:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field public final f:LX/EJ5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2116153
    new-instance v0, LX/EPB;

    invoke-direct {v0}, LX/EPB;-><init>()V

    sput-object v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;LX/EJ5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116146
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2116147
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2116148
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 2116149
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    .line 2116150
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2116151
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->f:LX/EJ5;

    .line 2116152
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;
    .locals 9

    .prologue
    .line 2116111
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;

    monitor-enter v1

    .line 2116112
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116113
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116114
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116115
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116116
    new-instance v3, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, LX/EJ5;->a(LX/0QB;)LX/EJ5;

    move-result-object v8

    check-cast v8, LX/EJ5;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;-><init>(Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;LX/EJ5;)V

    .line 2116117
    move-object v0, v3

    .line 2116118
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116119
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116120
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116121
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2116145
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2116123
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxh;

    const/4 v2, 0x0

    .line 2116124
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116125
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2116126
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2116127
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cu()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cu()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2116128
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->W()J

    move-result-wide v4

    .line 2116129
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->e:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->f:LX/EJ5;

    move-object v0, p3

    check-cast v0, LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v6, v1, v4, v5, v0}, LX/EJ5;->a(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v3, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2116130
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116131
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    const/4 v1, 0x0

    .line 2116132
    if-nez v0, :cond_3

    .line 2116133
    :cond_0
    :goto_1
    move-object v0, v1

    .line 2116134
    if-eqz v0, :cond_1

    .line 2116135
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->d:Lcom/facebook/multirow/parts/ContentViewThumbnailUriPartDefinition;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2116136
    :cond_1
    new-instance v0, LX/EPC;

    invoke-direct {v0, p0, p2, p3}, LX/EPC;-><init>(Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;LX/CzL;LX/Cxh;)V

    move-object v0, v0

    .line 2116137
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesExternalUrlPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2116138
    return-object v2

    :cond_2
    move-object v1, v2

    .line 2116139
    goto :goto_0

    .line 2116140
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bS()LX/A4E;

    move-result-object v3

    .line 2116141
    if-eqz v3, :cond_0

    .line 2116142
    invoke-interface {v3}, LX/A4E;->b()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v3

    .line 2116143
    if-eqz v3, :cond_0

    .line 2116144
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116122
    const/4 v0, 0x1

    return v0
.end method
