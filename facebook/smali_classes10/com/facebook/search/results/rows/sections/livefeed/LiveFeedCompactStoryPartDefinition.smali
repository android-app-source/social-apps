.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2111930
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2111931
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;->a:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;

    .line 2111932
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;
    .locals 4

    .prologue
    .line 2111933
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;

    monitor-enter v1

    .line 2111934
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111935
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111936
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111937
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111938
    new-instance p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;)V

    .line 2111939
    move-object v0, p0

    .line 2111940
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111941
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111942
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111943
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2111944
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2111945
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedCompactStoryPartDefinition;->a:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2111946
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2111947
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2111948
    invoke-static {p1}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedMainHeaderWithBackgroundPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
