.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105907
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2105908
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;

    .line 2105909
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    .line 2105910
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->c:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    .line 2105911
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->d:Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    .line 2105912
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;
    .locals 7

    .prologue
    .line 2105896
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;

    monitor-enter v1

    .line 2105897
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105898
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105899
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105900
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105901
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;)V

    .line 2105902
    move-object v0, p0

    .line 2105903
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105904
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105905
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105906
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2105874
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/Cxo;

    .line 2105875
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2105876
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;

    .line 2105877
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationWithPostCountPartDefinition;

    invoke-static {p1, v1, v0}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    .line 2105878
    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->c:LX/0Px;

    move-object v2, v2

    .line 2105879
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2105880
    if-nez v2, :cond_2

    .line 2105881
    :cond_0
    :goto_0
    move v2, v3

    .line 2105882
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->c:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    new-instance v4, LX/EN0;

    .line 2105883
    iget-object v5, v0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2105884
    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-direct {v4, v5, v6}, LX/EN0;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1RG;->a(ZLcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v2

    .line 2105885
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->c:LX/0Px;

    move-object v1, v1

    .line 2105886
    if-eqz v1, :cond_1

    .line 2105887
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->c:LX/0Px;

    move-object v1, v1

    .line 2105888
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SUBHEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v1, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->d:Lcom/facebook/search/results/rows/sections/header/SearchResultsStorySectionSubheaderComponentPartDefinition;

    .line 2105889
    iget-object v4, v0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2105890
    invoke-virtual {v2, v1, v3, v4}, LX/1RG;->a(ZLcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationSelectorPartDefinition;->b:Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    invoke-virtual {v1, v2, v0}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2105891
    const/4 v0, 0x0

    return-object v0

    .line 2105892
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 2105893
    :cond_2
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    invoke-interface {v5}, LX/1PT;->a()LX/1Qt;

    move-result-object v5

    sget-object v6, LX/1Qt;->SEARCH_DENSE_FEED:LX/1Qt;

    if-ne v5, v6, :cond_4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v2, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->SUBHEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v2, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    move v5, v4

    .line 2105894
    :goto_2
    if-nez v5, :cond_3

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v2, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_3
    move v3, v4

    goto :goto_0

    :cond_4
    move v5, v3

    .line 2105895
    goto :goto_2
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2105873
    const/4 v0, 0x1

    return v0
.end method
