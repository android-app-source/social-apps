.class public Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<***>;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/commerce/SearchResultsCommerceGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/collection/SearchResultsStoryCollectionGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102986
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2102987
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->a:Ljava/util/Map;

    .line 2102988
    const-class v0, Lcom/facebook/search/results/model/unit/SearchResultsProductItemUnit;

    invoke-direct {p0, v0, p1}, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102989
    const-class v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, v0, p2}, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102990
    const-class v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    invoke-direct {p0, v0, p4}, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->a(Ljava/lang/Class;LX/0Ot;)V

    .line 2102991
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->b:LX/0Ot;

    .line 2102992
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;
    .locals 7

    .prologue
    .line 2102975
    const-class v1, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;

    monitor-enter v1

    .line 2102976
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2102977
    sput-object v2, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2102978
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102979
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2102980
    new-instance v3, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;

    const/16 v4, 0x33b7

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x33a2

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3397

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x33f0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2102981
    move-object v0, v3

    .line 2102982
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2102983
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2102984
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2102985
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/Class;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/MultiRowGroupPartDefinition",
            "<***>;>;)V"
        }
    .end annotation

    .prologue
    .line 2102964
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102965
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2102967
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    .line 2102968
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2102969
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2102970
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2102971
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2102972
    :goto_0
    return-object v3

    .line 2102973
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/SearchResultsCollectionGroupPartDefinition;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->t()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    .line 2102974
    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2102966
    const/4 v0, 0x1

    return v0
.end method
