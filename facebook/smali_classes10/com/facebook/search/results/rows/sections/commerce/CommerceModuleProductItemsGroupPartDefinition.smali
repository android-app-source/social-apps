.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/commerce/SearchResultsCommerceModuleInterfaces$SearchResultsCommerceModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/0Uh;

.field private final c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2105235
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0xa7c5482

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-void
.end method

.method public constructor <init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105231
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2105232
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;->b:LX/0Uh;

    .line 2105233
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    .line 2105234
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;
    .locals 5

    .prologue
    .line 2105236
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;

    monitor-enter v1

    .line 2105237
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105238
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105239
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105240
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105241
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;-><init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;)V

    .line 2105242
    move-object v0, p0

    .line 2105243
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105244
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105245
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105246
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2105208
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pr;

    const/4 v1, 0x0

    .line 2105209
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2105210
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bJ()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    .line 2105211
    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v0, v2, :cond_2

    new-instance v0, LX/EJy;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v2}, LX/EJy;-><init>(Ljava/lang/Boolean;)V

    invoke-interface {p3, v0}, LX/1Pr;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2105212
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    move v0, v1

    .line 2105213
    :goto_0
    invoke-virtual {p2}, LX/CzL;->k()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 2105214
    invoke-static {p2, v0}, LX/CzM;->a(LX/CzL;I)LX/CzL;

    move-result-object v3

    .line 2105215
    invoke-static {v3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;->a(LX/CzL;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2105216
    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2105217
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2105218
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2105219
    :goto_1
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    if-gt v1, v2, :cond_5

    .line 2105220
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 2105221
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;->b:LX/0Uh;

    sget v2, LX/2SU;->X:I

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->ENTITY_HSCROLL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_2
    move v2, v1

    .line 2105222
    :goto_3
    invoke-virtual {p2}, LX/CzL;->k()I

    move-result v3

    if-ge v1, v3, :cond_5

    .line 2105223
    invoke-static {p2, v1}, LX/CzM;->a(LX/CzL;I)LX/CzL;

    move-result-object v3

    .line 2105224
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleListItemPartDefinition;

    invoke-virtual {p1, v4, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    move-result v3

    .line 2105225
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 2105226
    add-int/lit8 v2, v2, 0x1

    .line 2105227
    const/4 v3, 0x3

    if-eq v2, v3, :cond_5

    .line 2105228
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    move v0, v1

    .line 2105229
    goto :goto_2

    .line 2105230
    :cond_5
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2105207
    const/4 v0, 0x1

    return v0
.end method
