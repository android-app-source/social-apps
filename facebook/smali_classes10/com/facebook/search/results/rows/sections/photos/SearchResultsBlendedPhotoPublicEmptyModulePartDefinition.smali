.class public Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsBlendedPhotoPublicEmptyModuleInterfaces$SearchResultsBlendedPhotoPublicEmptyModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoEmptyComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoEmptyComponentPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2114429
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2114430
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoEmptyComponentPartDefinition;

    .line 2114431
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;
    .locals 4

    .prologue
    .line 2114432
    const-class v1, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;

    monitor-enter v1

    .line 2114433
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114434
    sput-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114435
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114436
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114437
    new-instance p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoEmptyComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoEmptyComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoEmptyComponentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoEmptyComponentPartDefinition;)V

    .line 2114438
    move-object v0, p0

    .line 2114439
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114440
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114441
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2114443
    check-cast p2, LX/CzL;

    .line 2114444
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicEmptyModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoEmptyComponentPartDefinition;

    .line 2114445
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2114446
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2114447
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 2114448
    check-cast p1, LX/CzL;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2114449
    new-array v3, v1, [Ljava/lang/CharSequence;

    .line 2114450
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2114451
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v2

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method
