.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/CxA;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d0;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static p:LX/0Xm;


# instance fields
.field private final d:LX/ELF;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ELB;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/ELF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p13    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/ELF;",
            "LX/0Ot",
            "<",
            "LX/ELB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7Bf;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2109445
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2109446
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->d:LX/ELF;

    .line 2109447
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->e:LX/0Ot;

    .line 2109448
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->f:LX/0Ot;

    .line 2109449
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->g:LX/0Ot;

    .line 2109450
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->h:LX/0Ot;

    .line 2109451
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->i:LX/0Ot;

    .line 2109452
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->j:LX/0Ot;

    .line 2109453
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->k:LX/0Ot;

    .line 2109454
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->l:LX/0Ot;

    .line 2109455
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->m:LX/0Ot;

    .line 2109456
    iput-object p12, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->n:LX/0Ot;

    .line 2109457
    iput-object p13, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->o:LX/0Ot;

    .line 2109458
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2109460
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109461
    move-object v8, v0

    check-cast v8, LX/8d0;

    .line 2109462
    invoke-interface {v8}, LX/8d0;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v0

    .line 2109463
    new-instance v1, LX/ELf;

    invoke-direct {v1, p0, p2, p3}, LX/ELf;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;LX/CzL;LX/1Pn;)V

    .line 2109464
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->d:LX/ELF;

    invoke-virtual {v2, p1}, LX/ELF;->c(LX/1De;)LX/ELD;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/ELD;->a(Landroid/net/Uri;)LX/ELD;

    move-result-object v0

    invoke-interface {v8}, LX/8d0;->o()Z

    move-result v2

    invoke-interface {v8}, LX/8d0;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->f:LX/0Ot;

    invoke-static {v2, v3, v4}, LX/ELM;->a(ZLjava/lang/String;LX/0Ot;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/ELD;->a(Ljava/lang/CharSequence;)LX/ELD;

    move-result-object v0

    invoke-static {p2, v10}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/ELD;->b(Ljava/lang/CharSequence;)LX/ELD;

    move-result-object v0

    invoke-static {p2, v11}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/ELD;->c(Ljava/lang/CharSequence;)LX/ELD;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/ELD;->c(Landroid/view/View$OnClickListener;)LX/ELD;

    move-result-object v9

    move-object v1, p3

    .line 2109465
    check-cast v1, LX/1Pr;

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->g:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->h:LX/0Ot;

    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->i:LX/0Ot;

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->j:LX/0Ot;

    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->n:LX/0Ot;

    iget-object v7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->o:LX/0Ot;

    move-object v0, p2

    invoke-static/range {v0 .. v7}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageActionButtonPartDefinition;->a(LX/CzL;LX/1Pr;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)LX/EL8;

    move-result-object v1

    .line 2109466
    iget v0, v1, LX/EL8;->a:I

    if-eqz v0, :cond_0

    .line 2109467
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    iget v2, v1, LX/EL8;->a:I

    invoke-virtual {v0, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    iget v2, v1, LX/EL8;->b:I

    invoke-virtual {v0, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/ELD;->a(LX/1n6;)LX/ELD;

    .line 2109468
    iget-object v0, v1, LX/EL8;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v0}, LX/ELD;->d(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2109469
    :cond_0
    invoke-interface {v8}, LX/8d0;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v8}, LX/8d0;->o()Z

    move-result v2

    move-object v0, p3

    check-cast v0, LX/CxV;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {v1, v2, v3, v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(Ljava/lang/String;ZLcom/facebook/search/results/model/SearchResultsMutableContext;LX/0W9;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->c:LX/0ad;

    sget-short v1, LX/100;->b:S

    invoke-interface {v0, v1, v10}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2109470
    invoke-virtual {v9, v11}, LX/ELD;->a(Z)LX/ELD;

    .line 2109471
    const v0, 0x7f0822c5

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/ELD;->d(Ljava/lang/CharSequence;)LX/ELD;

    .line 2109472
    const-string v0, "photos"

    sget-object v1, LX/7CM;->INLINE_PHOTOS_LINK:LX/7CM;

    invoke-direct {p0, p2, v0, p3, v1}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->a(LX/CzL;Ljava/lang/String;LX/1Pn;LX/7CM;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/ELD;->a(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2109473
    const v0, 0x7f0822c7

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/ELD;->e(Ljava/lang/CharSequence;)LX/ELD;

    .line 2109474
    const-string v0, "events"

    sget-object v1, LX/7CM;->INLINE_EVENTS_LINK:LX/7CM;

    invoke-direct {p0, p2, v0, p3, v1}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->a(LX/CzL;Ljava/lang/String;LX/1Pn;LX/7CM;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/ELD;->b(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2109475
    :cond_1
    invoke-virtual {v9}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 2109476
    :cond_2
    invoke-interface {v8}, LX/8d0;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private a(LX/CzL;Ljava/lang/String;LX/1Pn;LX/7CM;)Landroid/view/View$OnClickListener;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d0;",
            ">;",
            "Ljava/lang/String;",
            "TE;",
            "LX/7CM;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 2109459
    new-instance v0, LX/ELg;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/ELg;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;LX/CzL;Ljava/lang/String;LX/1Pn;LX/7CM;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;
    .locals 3

    .prologue
    .line 2109435
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    monitor-enter v1

    .line 2109436
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109437
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109438
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109439
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->b(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109440
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109441
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;
    .locals 14

    .prologue
    .line 2109443
    new-instance v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/ELF;->a(LX/0QB;)LX/ELF;

    move-result-object v2

    check-cast v2, LX/ELF;

    const/16 v3, 0x33e8

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x3512

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x113f

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x12b1

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x32d4

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x474

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x3b2

    invoke-static {p0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2be

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2eb

    invoke-static {p0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x32bb

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x140d

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;-><init>(Landroid/content/Context;LX/ELF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2109444
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2109434
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2109433
    check-cast p2, LX/CzL;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2109432
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2109431
    const/4 v0, 0x0

    return-object v0
.end method
