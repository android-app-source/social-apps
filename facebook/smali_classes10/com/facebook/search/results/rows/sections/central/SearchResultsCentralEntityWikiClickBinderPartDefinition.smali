.class public Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxG;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/CvY;

.field public b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/CvY;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103913
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2103914
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;->b:Landroid/view/View$OnClickListener;

    .line 2103915
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;->a:LX/CvY;

    .line 2103916
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;
    .locals 4

    .prologue
    .line 2103917
    const-class v1, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    monitor-enter v1

    .line 2103918
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103919
    sput-object v2, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103920
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103921
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103922
    new-instance p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;

    invoke-static {v0}, LX/CvY;->a(LX/0QB;)LX/CvY;

    move-result-object v3

    check-cast v3, LX/CvY;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;-><init>(LX/CvY;)V

    .line 2103923
    move-object v0, p0

    .line 2103924
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103925
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103926
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2103928
    check-cast p2, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;

    check-cast p3, LX/1Pr;

    .line 2103929
    new-instance v0, LX/EJP;

    invoke-direct {v0, p0, p3, p2}, LX/EJP;-><init>(Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;LX/1Pr;Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;)V

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;->b:Landroid/view/View$OnClickListener;

    .line 2103930
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x29709f22

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2103931
    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 2103932
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/central/SearchResultsCentralEntityWikiClickBinderPartDefinition;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2103933
    const/16 v1, 0x1f

    const v2, -0x4c1e6caf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2103934
    check-cast p4, Lcom/facebook/widget/text/BetterTextView;

    .line 2103935
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2103936
    return-void
.end method
