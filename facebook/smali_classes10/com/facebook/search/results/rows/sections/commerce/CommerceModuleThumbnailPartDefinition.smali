.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/A2T;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105273
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2105274
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->a:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    .line 2105275
    return-void
.end method

.method public static a(LX/CzL;)Landroid/net/Uri;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/A2T;",
            ">;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2105276
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2105277
    check-cast v0, LX/A2T;

    .line 2105278
    iget-object v1, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2105279
    if-nez v1, :cond_0

    .line 2105280
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 2105281
    :goto_0
    return-object v0

    .line 2105282
    :cond_0
    invoke-interface {v0}, LX/A2T;->bP()LX/A2S;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p0}, LX/EJf;->a(LX/CzL;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->COMMERCE_B2C:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v1, v3, :cond_1

    .line 2105283
    invoke-interface {v0}, LX/A2T;->bP()LX/A2S;

    move-result-object v0

    invoke-interface {v0}, LX/A2S;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 2105284
    :cond_1
    invoke-interface {v0}, LX/A2T;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, LX/A2T;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, LX/A2T;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2105285
    :cond_2
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    goto :goto_0

    .line 2105286
    :cond_3
    invoke-interface {v0}, LX/A2T;->aC()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    .line 2105287
    :goto_1
    if-ge v1, v4, :cond_6

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel;

    .line 2105288
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel;->a()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$MediaModel;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel;->a()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$MediaModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$MediaModel;->b()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 2105289
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel;->a()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$MediaModel;->b()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$MediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 2105290
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    .line 2105291
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$SubattachmentsModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$SubattachmentsModel;->a()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$SubattachmentsModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$SubattachmentsModel$MediaModel;->b()Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$SubattachmentsModel$MediaModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/commerce/SearchResultsProductItemModels$SearchResultsProductItemModel$ParentStoryModel$AttachmentsModel$SubattachmentsModel$MediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 2105292
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2105293
    :cond_6
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;
    .locals 4

    .prologue
    .line 2105294
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;

    monitor-enter v1

    .line 2105295
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105296
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105297
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105298
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105299
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;-><init>(Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;)V

    .line 2105300
    move-object v0, p0

    .line 2105301
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105302
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105303
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105304
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2105305
    check-cast p2, LX/CzL;

    .line 2105306
    new-instance v0, LX/8Cq;

    invoke-direct {v0}, LX/8Cq;-><init>()V

    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->a(LX/CzL;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8Cq;->a(Landroid/net/Uri;)LX/8Cq;

    move-result-object v0

    const v1, 0x7f020c36

    .line 2105307
    iput v1, v0, LX/8Cq;->c:I

    .line 2105308
    move-object v0, v0

    .line 2105309
    invoke-virtual {v0}, LX/8Cq;->a()LX/8Cr;

    move-result-object v0

    .line 2105310
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->a:Lcom/facebook/multirow/parts/ImageBlockLayoutThumbnailPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2105311
    const/4 v0, 0x0

    return-object v0
.end method
