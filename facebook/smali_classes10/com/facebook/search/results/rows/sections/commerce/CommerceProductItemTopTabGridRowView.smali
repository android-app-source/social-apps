.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

.field public final b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

.field public final c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2105461
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2105462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2105463
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2105464
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2105465
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2105466
    const v0, 0x7f03128d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2105467
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->setOrientation(I)V

    .line 2105468
    const v0, 0x7f0d2b72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    .line 2105469
    const v0, 0x7f0d2b73

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    .line 2105470
    const v0, 0x7f0d2b74

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    .line 2105471
    return-void
.end method


# virtual methods
.method public getFirstProductView()Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;
    .locals 1

    .prologue
    .line 2105472
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    return-object v0
.end method

.method public getSecondProductView()Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;
    .locals 1

    .prologue
    .line 2105473
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    return-object v0
.end method

.method public getThirdProductView()Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;
    .locals 1

    .prologue
    .line 2105474
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductItemTopTabGridRowView;->c:Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;

    return-object v0
.end method
