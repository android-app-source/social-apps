.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EL8;",
        "LX/EL7;",
        "LX/1Pn;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/23P;

.field public final b:LX/0wM;

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/23P;LX/0wM;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107994
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2107995
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->a:LX/23P;

    .line 2107996
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->b:LX/0wM;

    .line 2107997
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->c:LX/0ad;

    .line 2107998
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;
    .locals 6

    .prologue
    .line 2108046
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    monitor-enter v1

    .line 2108047
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108048
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2108049
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108050
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2108051
    new-instance p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;-><init>(LX/23P;LX/0wM;LX/0ad;)V

    .line 2108052
    move-object v0, p0

    .line 2108053
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2108054
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2108055
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2108056
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2108057
    check-cast p2, LX/EL8;

    check-cast p3, LX/1Pn;

    .line 2108058
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->b:LX/0wM;

    invoke-static {p2, p3, v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonFigPartDefinition;->a(LX/EL8;LX/1Pn;LX/0wM;)LX/EL7;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x1a62ccc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2108006
    check-cast p1, LX/EL8;

    check-cast p2, LX/EL7;

    check-cast p3, LX/1Pn;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2108007
    iget-object v1, p1, LX/EL8;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2108008
    iget-object v1, p2, LX/EL7;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    .line 2108009
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 2108010
    instance-of v1, v1, Lcom/facebook/resources/ui/FbButton;

    if-eqz v1, :cond_2

    .line 2108011
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 2108012
    const/4 v5, 0x0

    invoke-static {v1, v5}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2108013
    iget-object v1, p2, LX/EL7;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2108014
    iget-object v1, p2, LX/EL7;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2108015
    :cond_0
    :goto_1
    const/16 v1, 0x1f

    const v2, 0x1cef786d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move v1, v4

    .line 2108016
    goto :goto_0

    .line 2108017
    :cond_2
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 2108018
    instance-of v1, v1, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    if-eqz v1, :cond_0

    .line 2108019
    iget-object v1, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 2108020
    check-cast v1, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    .line 2108021
    iget-object v5, p1, LX/EL8;->e:Ljava/lang/Integer;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->c:LX/0ad;

    sget-short v6, LX/100;->aq:S

    invoke-interface {v5, v6, v4}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2108022
    invoke-virtual {p4, v2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2108023
    const/4 p4, 0x0

    const/4 v6, 0x0

    .line 2108024
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f020b0b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v1, v2}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2108025
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGravity(I)V

    .line 2108026
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a029b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 2108027
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->c:LX/0ad;

    sget-short v5, LX/100;->ap:S

    invoke-interface {v2, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2108028
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->b:LX/0wM;

    iget v5, p1, LX/EL8;->a:I

    invoke-virtual {v2, v5, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 2108029
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b1727

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyphSize(I)V

    .line 2108030
    :goto_2
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->c:LX/0ad;

    sget-short v5, LX/100;->ar:S

    invoke-interface {v2, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v5

    .line 2108031
    iget-object v2, p1, LX/EL8;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    if-eqz v5, :cond_6

    iget-object v2, p1, LX/EL8;->f:Ljava/lang/Integer;

    :goto_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2108032
    iget-object v6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityActionButtonPartDefinition;->a:LX/23P;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2, p4}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setText(Ljava/lang/CharSequence;)V

    .line 2108033
    invoke-virtual {v1, v4}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setColor(I)V

    .line 2108034
    new-instance v4, LX/6VC;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    if-eqz v5, :cond_7

    const v2, 0x7f0b174a

    :goto_4
    invoke-virtual {v6, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const/4 v5, -0x2

    invoke-direct {v4, v2, v5}, LX/6VC;-><init>(II)V

    invoke-virtual {v1, v4}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2108035
    goto/16 :goto_1

    .line 2108036
    :cond_3
    iget-object v5, p2, LX/EL7;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_4

    :goto_5
    invoke-virtual {p4, v2}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2108037
    const/4 v4, -0x2

    .line 2108038
    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2108039
    new-instance v2, LX/6VC;

    invoke-direct {v2, v4, v4}, LX/6VC;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2108040
    iget-object v2, p2, LX/EL7;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 2108041
    goto/16 :goto_1

    :cond_4
    move v2, v4

    .line 2108042
    goto :goto_5

    .line 2108043
    :cond_5
    invoke-virtual {v1, p4}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 2108044
    :cond_6
    iget-object v2, p1, LX/EL8;->e:Ljava/lang/Integer;

    goto :goto_3

    .line 2108045
    :cond_7
    const v2, 0x7f0b1749

    goto :goto_4
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2107999
    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2108000
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2108001
    iget-object v0, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 2108002
    instance-of v0, v0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    if-eqz v0, :cond_0

    .line 2108003
    iget-object v0, p4, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v0, v0

    .line 2108004
    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;

    invoke-virtual {v0}, Lcom/facebook/search/results/rows/sections/entities/UserActionButtonView;->a()V

    .line 2108005
    :cond_0
    return-void
.end method
