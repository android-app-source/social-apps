.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsEntitiesEnvironment;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsEntitiesEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsEntitiesEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsEntitiesEnvironment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsEntitiesEnvironment;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;",
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsPageLargeRowPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoChannelLargeRowPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107901
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2107902
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;

    .line 2107903
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;

    .line 2107904
    move-object v0, p3

    .line 2107905
    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->c:LX/0Ot;

    .line 2107906
    move-object v0, p4

    .line 2107907
    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->d:LX/0Ot;

    .line 2107908
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;
    .locals 7

    .prologue
    .line 2107915
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;

    monitor-enter v1

    .line 2107916
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107917
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107918
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107919
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107920
    new-instance v5, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;

    const/16 v6, 0x1169

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x11a7

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;LX/0Ot;LX/0Ot;)V

    .line 2107921
    move-object v0, v5

    .line 2107922
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107923
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107924
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107925
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2107910
    check-cast p2, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    check-cast p3, LX/Cxf;

    .line 2107911
    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 2107912
    :goto_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->d:LX/0Ot;

    invoke-static {p1, v0, v1, p2}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileLargeRowPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->c:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntitiesRootGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/entities/OldSearchResultsEntityLargeRowPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2107913
    const/4 v0, 0x0

    return-object v0

    .line 2107914
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2107909
    const/4 v0, 0x1

    return v0
.end method
