.class public Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/shortcut/SearchResultsShortcutModuleInterfaces$SearchResultsShortcutModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private a:Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116915
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2116916
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    .line 2116917
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;
    .locals 4

    .prologue
    .line 2116918
    const-class v1, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;

    monitor-enter v1

    .line 2116919
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116920
    sput-object v2, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116921
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116922
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116923
    new-instance p0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;)V

    .line 2116924
    move-object v0, p0

    .line 2116925
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116926
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116927
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116928
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2116908
    check-cast p2, LX/CzL;

    .line 2116909
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116910
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bV()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v1

    .line 2116911
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2116912
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/shortcut/SearchResultsShortcutComponentPartDefinition;

    invoke-static {p2, v0}, LX/CzM;->a(LX/CzL;I)LX/CzL;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2116913
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2116914
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116906
    check-cast p1, LX/CzL;

    .line 2116907
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
