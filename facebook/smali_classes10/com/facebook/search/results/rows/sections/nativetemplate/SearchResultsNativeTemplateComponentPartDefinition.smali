.class public Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsInterfaces$NativeTemplateViewForSearchFragment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/3j4;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/3j4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112601
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2112602
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;->d:LX/3j4;

    .line 2112603
    return-void
.end method

.method private a(LX/CzL;LX/1Ps;)LX/1X1;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<",
            "Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsInterfaces$NativeTemplateViewForSearchFragment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2112620
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;->d:LX/3j4;

    .line 2112621
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2112622
    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    .line 2112623
    if-nez v0, :cond_0

    .line 2112624
    const/4 v3, 0x0

    .line 2112625
    :goto_0
    move-object v0, v3

    .line 2112626
    check-cast p2, LX/1Pq;

    invoke-virtual {v1, v0, p2, v2, v2}, LX/3j4;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/1Pq;LX/0jW;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v0

    return-object v0

    .line 2112627
    :cond_0
    new-instance v5, LX/4XP;

    invoke-direct {v5}, LX/4XP;-><init>()V

    .line 2112628
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2112629
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2112630
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_1

    .line 2112631
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel;

    .line 2112632
    if-nez v3, :cond_3

    .line 2112633
    const/4 v7, 0x0

    .line 2112634
    :goto_2
    move-object v3, v7

    .line 2112635
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2112636
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 2112637
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2112638
    iput-object v3, v5, LX/4XP;->b:LX/0Px;

    .line 2112639
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2112640
    iput-object v3, v5, LX/4XP;->c:Ljava/lang/String;

    .line 2112641
    invoke-virtual {v5}, LX/4XP;->a()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v3

    goto :goto_0

    .line 2112642
    :cond_3
    new-instance p0, LX/4XO;

    invoke-direct {p0}, LX/4XO;-><init>()V

    .line 2112643
    invoke-virtual {v3}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel;->a()LX/0Px;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 2112644
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object p1

    .line 2112645
    const/4 v7, 0x0

    move v8, v7

    :goto_3
    invoke-virtual {v3}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v7

    if-ge v8, v7, :cond_4

    .line 2112646
    invoke-virtual {v3}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;

    invoke-static {v7}, LX/5eL;->a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel$NtBundleAttributesModel;)Lcom/facebook/graphql/model/GraphQLNTBundleAttribute;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2112647
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_3

    .line 2112648
    :cond_4
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    .line 2112649
    iput-object v7, p0, LX/4XO;->b:LX/0Px;

    .line 2112650
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel$NativeTemplateBundlesModel;->b()Ljava/lang/String;

    move-result-object v7

    .line 2112651
    iput-object v7, p0, LX/4XO;->c:Ljava/lang/String;

    .line 2112652
    invoke-virtual {p0}, LX/4XO;->a()Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;

    move-result-object v7

    goto :goto_2
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;
    .locals 5

    .prologue
    .line 2112609
    const-class v1, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;

    monitor-enter v1

    .line 2112610
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112611
    sput-object v2, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112612
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112613
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112614
    new-instance p0, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/3j4;->a(LX/0QB;)LX/3j4;

    move-result-object v4

    check-cast v4, LX/3j4;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;-><init>(Landroid/content/Context;LX/3j4;)V

    .line 2112615
    move-object v0, p0

    .line 2112616
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112617
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112618
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112619
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2112653
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p2, p3}, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;->a(LX/CzL;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2112608
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p2, p3}, Lcom/facebook/search/results/rows/sections/nativetemplate/SearchResultsNativeTemplateComponentPartDefinition;->a(LX/CzL;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2112605
    check-cast p1, LX/CzL;

    .line 2112606
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2112607
    check-cast v0, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewForSearchFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2112604
    const/4 v0, 0x0

    return-object v0
.end method
