.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final f:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106100
    const v0, 0x7f0e0957

    sput v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a:I

    .line 2106101
    const v0, 0x7f0e0958

    sput v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->b:I

    .line 2106102
    const v0, 0x7f0e0959

    sput v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->c:I

    .line 2106103
    const-class v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;

    const-string v1, "graph_search_results_page"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2106062
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->d:LX/0Or;

    .line 2106063
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->e:LX/0Ot;

    .line 2106064
    return-void
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;Ljava/lang/String;ILjava/lang/CharSequence;III)LX/1Dh;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/high16 v1, -0x80000000

    const/high16 v0, 0x42900000    # 72.0f

    .line 2106110
    if-nez p2, :cond_0

    if-nez p7, :cond_0

    .line 2106111
    const/4 v0, 0x0

    .line 2106112
    :goto_0
    return-object v0

    .line 2106113
    :cond_0
    if-ne p5, v1, :cond_2

    .line 2106114
    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v4

    .line 2106115
    :goto_1
    if-ne p6, v1, :cond_1

    .line 2106116
    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v3

    .line 2106117
    :goto_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p7

    .line 2106118
    if-nez v2, :cond_3

    if-eqz v5, :cond_3

    .line 2106119
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p2

    const/4 p5, 0x1

    invoke-interface {p2, p5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object p2

    const/4 p5, 0x2

    invoke-interface {p2, p5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object p2

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p5

    const p6, 0x7f0a009a

    invoke-virtual {p5, p6}, Landroid/content/res/Resources;->getColor(I)I

    move-result p5

    invoke-interface {p2, p5}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object p5

    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p6

    iget-object p2, v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->e:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/1vg;

    invoke-virtual {p2, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p2

    const p7, -0x808081

    invoke-virtual {p2, p7}, LX/2xv;->i(I)LX/2xv;

    move-result-object p2

    invoke-virtual {p2, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object p2

    invoke-virtual {p2}, LX/1n6;->b()LX/1dc;

    move-result-object p2

    invoke-virtual {p6, p2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object p2

    invoke-virtual {p2}, LX/1X5;->c()LX/1Di;

    move-result-object p2

    invoke-interface {p5, p2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p2

    invoke-interface {p2, v3}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object p2

    invoke-interface {p2, v4}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object p2

    .line 2106120
    :goto_3
    move-object v0, p2

    .line 2106121
    const/4 v1, -0x1

    if-ne p3, v1, :cond_4

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2106122
    const/4 v1, 0x0

    .line 2106123
    :goto_4
    move-object v1, v1

    .line 2106124
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v6, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v3, p6

    goto :goto_2

    :cond_2
    move v4, p5

    goto :goto_1

    :cond_3
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p5

    invoke-static {v1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p6

    iget-object p2, v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->d:LX/0Or;

    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/1Ad;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p7

    invoke-virtual {p2, p7}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object p2

    sget-object p7, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p2, p7}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p2

    invoke-virtual {p2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object p2

    invoke-virtual {p6, p2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object p2

    invoke-virtual {p2}, LX/1X5;->c()LX/1Di;

    move-result-object p2

    invoke-interface {p2, v3}, LX/1Di;->g(I)LX/1Di;

    move-result-object p2

    invoke-interface {p2, v4}, LX/1Di;->o(I)LX/1Di;

    move-result-object p2

    invoke-interface {p5, p2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p2

    goto :goto_3

    .line 2106125
    :cond_4
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2106126
    const/4 p4, 0x4

    const/4 v3, 0x2

    .line 2106127
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    invoke-static {p0, p1, p3}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->b(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;I)LX/1dc;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v3, p4}, LX/1Di;->m(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v1, v2, p4}, LX/1Di;->m(II)LX/1Di;

    move-result-object v1

    const/high16 v2, 0x41800000    # 16.0f

    invoke-static {p1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-interface {v1, v2}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    const/16 v2, 0x8

    invoke-interface {v1, v2, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    const v2, 0x7f021935

    invoke-interface {v1, v2}, LX/1Di;->x(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 2106128
    goto/16 :goto_4

    .line 2106129
    :cond_5
    const/4 p5, 0x4

    const/4 v5, 0x1

    const/4 p2, 0x2

    .line 2106130
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    .line 2106131
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2106132
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p2, p5}, LX/1Dh;->y(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v3, v4, p5}, LX/1Dh;->y(II)LX/1Dh;

    move-result-object v3

    const/16 v4, 0x8

    invoke-interface {v3, v4, p2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    const v4, 0x7f021a5f

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v3

    .line 2106133
    invoke-virtual {v1, v2}, LX/1ne;->p(I)LX/1ne;

    .line 2106134
    const v4, 0x7f0a00d5

    invoke-virtual {v1, v4}, LX/1ne;->n(I)LX/1ne;

    .line 2106135
    invoke-virtual {v1, p4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    .line 2106136
    const/4 v4, -0x1

    if-ne p3, v4, :cond_6

    .line 2106137
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2106138
    :goto_5
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 2106139
    goto/16 :goto_4

    .line 2106140
    :cond_6
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-static {p0, p1, p3}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->b(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;I)LX/1dc;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b0064

    invoke-interface {v4, p2, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v2}, LX/1Di;->g(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v2}, LX/1Di;->o(I)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    goto :goto_5
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;Ljava/lang/String;ILjava/lang/CharSequence;II)LX/1Di;
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 2106104
    const/4 v0, 0x0

    invoke-static {p1, v0, p6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    .line 2106105
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/16 v2, 0x8

    invoke-interface {v0, v2, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    .line 2106106
    invoke-virtual {v1, p4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    .line 2106107
    invoke-virtual {v1, p5}, LX/1ne;->j(I)LX/1ne;

    .line 2106108
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    sget-object v5, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    const/4 v4, 0x4

    invoke-interface {v0, v6, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2106109
    return-object v2
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/lang/CharSequence;II)LX/1Di;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 2106078
    invoke-static {p1, v7, p7}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    .line 2106079
    invoke-virtual {v1, p4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    .line 2106080
    invoke-virtual {v1, p6}, LX/1ne;->j(I)LX/1ne;

    .line 2106081
    invoke-static {p1, v7, p7}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    .line 2106082
    invoke-virtual {v2, p5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    .line 2106083
    invoke-virtual {v2, p6}, LX/1ne;->j(I)LX/1ne;

    .line 2106084
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/16 v3, 0x8

    invoke-interface {v0, v3, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    .line 2106085
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    sget-object v5, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->g(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Di;->o(I)LX/1Di;

    move-result-object v0

    const/4 v4, 0x6

    invoke-interface {v0, v6, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2106086
    return-object v3
.end method

.method private static a(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/CharSequence;III)LX/1Di;
    .locals 11

    .prologue
    .line 2106087
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v10

    .line 2106088
    const/high16 v2, -0x80000000

    move/from16 v0, p7

    if-ne v0, v2, :cond_4

    .line 2106089
    const/high16 v2, 0x41800000    # 16.0f

    invoke-static {p1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v5

    .line 2106090
    :goto_0
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 2106091
    :cond_0
    const/4 v2, 0x4

    const v3, 0x7f0b0060

    invoke-interface {v10, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    .line 2106092
    :cond_1
    const/4 v2, 0x2

    move/from16 v0, p9

    invoke-static {p1, p4, v2, v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1ne;

    move-result-object v2

    invoke-interface {v10, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 2106093
    if-eqz p6, :cond_3

    .line 2106094
    int-to-float v2, v5

    invoke-static {p1, v2}, LX/0tP;->c(Landroid/content/Context;F)I

    move-result v2

    const/16 v3, 0x14

    if-le v2, v3, :cond_2

    .line 2106095
    const/4 v8, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p6

    move-object/from16 v6, p5

    move-object/from16 v7, p8

    move/from16 v9, p10

    invoke-static/range {v2 .. v9}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/lang/CharSequence;II)LX/1Di;

    move-result-object v2

    invoke-interface {v10, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 2106096
    :goto_1
    return-object v2

    .line 2106097
    :cond_2
    const/4 v7, 0x1

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p6

    move-object/from16 v6, p5

    move/from16 v8, p10

    invoke-static/range {v2 .. v8}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;Ljava/lang/String;ILjava/lang/CharSequence;II)LX/1Di;

    move-result-object v2

    invoke-interface {v10, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 2106098
    :goto_2
    const/4 v2, 0x2

    move-object/from16 v0, p8

    move/from16 v1, p11

    invoke-static {p1, v0, v2, v1}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1ne;

    move-result-object v2

    invoke-interface {v10, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    goto :goto_1

    .line 2106099
    :cond_3
    const/4 v2, 0x1

    move-object/from16 v0, p5

    move/from16 v1, p10

    invoke-static {p1, v0, v2, v1}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1ne;

    move-result-object v2

    invoke-interface {v10, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    goto :goto_2

    :cond_4
    move/from16 v5, p7

    goto :goto_0
.end method

.method private static a(LX/1De;Ljava/lang/CharSequence;II)LX/1ne;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2106077
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0, p3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;
    .locals 5

    .prologue
    .line 2106066
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;

    monitor-enter v1

    .line 2106067
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106068
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106069
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106070
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106071
    new-instance v3, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x3b2

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;-><init>(LX/0Or;LX/0Ot;)V

    .line 2106072
    move-object v0, v3

    .line 2106073
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106074
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106075
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106076
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;I)LX/1dc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2106065
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, LX/2xv;->j(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/lang/CharSequence;IIILjava/lang/String;IIII)LX/1Dg;
    .locals 13
    .param p2    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p6    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p7    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p15    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 2106060
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x8

    const v2, 0x7f0b0060

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v8

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p11

    move/from16 v3, p8

    move-object/from16 v4, p7

    move/from16 v5, p10

    move/from16 v6, p9

    move/from16 v7, p15

    invoke-static/range {v0 .. v7}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;Ljava/lang/String;ILjava/lang/CharSequence;III)LX/1Dh;

    move-result-object v0

    invoke-interface {v8, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v12

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p11

    move/from16 v3, p15

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p12

    move/from16 v10, p13

    move/from16 v11, p14

    invoke-static/range {v0 .. v11}, Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;->a(Lcom/facebook/search/results/rows/sections/common/SearchResultsImageBlockLayoutRowComponentSpec;LX/1De;Ljava/lang/String;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;ILjava/lang/CharSequence;III)LX/1Di;

    move-result-object v0

    invoke-interface {v12, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/EK7;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
