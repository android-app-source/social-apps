.class public Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/spellcorrection/SearchResultsSpellerModuleInterfaces$SearchResultsSpellerModule;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public d:LX/EPX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2117151
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2117152
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/spellcorrection/SearchResultsSpellerModuleInterfaces$SearchResultsSpellerModule;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2117111
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;->d:LX/EPX;

    const/4 v1, 0x0

    .line 2117112
    new-instance v2, LX/EPW;

    invoke-direct {v2, v0}, LX/EPW;-><init>(LX/EPX;)V

    .line 2117113
    iget-object p0, v0, LX/EPX;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/EPV;

    .line 2117114
    if-nez p0, :cond_0

    .line 2117115
    new-instance p0, LX/EPV;

    invoke-direct {p0, v0}, LX/EPV;-><init>(LX/EPX;)V

    .line 2117116
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/EPV;->a$redex0(LX/EPV;LX/1De;IILX/EPW;)V

    .line 2117117
    move-object v2, p0

    .line 2117118
    move-object v1, v2

    .line 2117119
    move-object v0, v1

    .line 2117120
    iget-object v1, v0, LX/EPV;->a:LX/EPW;

    iput-object p2, v1, LX/EPW;->a:LX/CzL;

    .line 2117121
    iget-object v1, v0, LX/EPV;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2117122
    move-object v0, v0

    .line 2117123
    check-cast p3, LX/CxV;

    .line 2117124
    iget-object v1, v0, LX/EPV;->a:LX/EPW;

    iput-object p3, v1, LX/EPW;->b:LX/CxV;

    .line 2117125
    iget-object v1, v0, LX/EPV;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2117126
    move-object v0, v0

    .line 2117127
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;
    .locals 5

    .prologue
    .line 2117138
    const-class v1, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;

    monitor-enter v1

    .line 2117139
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117140
    sput-object v2, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117141
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117142
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117143
    new-instance p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;-><init>(Landroid/content/Context;)V

    .line 2117144
    invoke-static {v0}, LX/EPX;->a(LX/0QB;)LX/EPX;

    move-result-object v3

    check-cast v3, LX/EPX;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    .line 2117145
    iput-object v3, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;->d:LX/EPX;

    iput-object v4, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;->e:LX/0ad;

    .line 2117146
    move-object v0, p0

    .line 2117147
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117148
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117149
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117150
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2117137
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2117136
    check-cast p2, LX/CzL;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2117129
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 2117130
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117131
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bW()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2117132
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117133
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bW()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;->k()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel$QueryTitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2117134
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117135
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bW()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$SeeMoreQueryModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellerModulePartDefinition;->e:LX/0ad;

    sget-short v2, LX/100;->aJ:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2117128
    const/4 v0, 0x0

    return-object v0
.end method
