.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lk;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/EPn;",
        "LX/2pa;",
        "TE;",
        "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/D49;


# direct methods
.method public constructor <init>(LX/D49;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117678
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2117679
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;->a:LX/D49;

    .line 2117680
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;
    .locals 4

    .prologue
    .line 2117681
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;

    monitor-enter v1

    .line 2117682
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117683
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117684
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117685
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117686
    new-instance p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;

    invoke-static {v0}, LX/D49;->b(LX/0QB;)LX/D49;

    move-result-object v3

    check-cast v3, LX/D49;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;-><init>(LX/D49;)V

    .line 2117687
    move-object v0, p0

    .line 2117688
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117689
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117690
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117691
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2117692
    check-cast p2, LX/EPn;

    check-cast p3, LX/7Lk;

    .line 2117693
    iget-object v0, p2, LX/EPn;->a:LX/CzL;

    .line 2117694
    iget-object v1, v0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v1

    .line 2117695
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2117696
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2117697
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2117698
    invoke-static {v1}, LX/1VO;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2117699
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v2

    .line 2117700
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2117701
    check-cast p3, LX/Cxj;

    iget-object v2, p2, LX/EPn;->a:LX/CzL;

    .line 2117702
    iget v3, v2, LX/CzL;->b:I

    move v2, v3

    .line 2117703
    invoke-interface {p3, v0, v2}, LX/Cxj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/CyM;

    move-result-object v2

    invoke-virtual {v2}, LX/CyM;->b()LX/2oO;

    move-result-object v2

    .line 2117704
    invoke-static {v0}, LX/D49;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0P2;

    move-result-object v3

    const-string v4, "AutoplayStateManager"

    invoke-virtual {v3, v4, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    .line 2117705
    iget-object v3, p2, LX/EPn;->a:LX/CzL;

    invoke-virtual {v3}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->VIDEOS_LIVE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    if-ne v3, v4, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->ar()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2117706
    const-string v1, "HideDownloadToFacebook"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2117707
    :cond_0
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 2117708
    new-instance v2, LX/2pZ;

    invoke-direct {v2}, LX/2pZ;-><init>()V

    iget-object v3, p2, LX/EPn;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2117709
    iput-object v3, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2117710
    move-object v2, v2

    .line 2117711
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideoPlayerConfiguratorPartDefinition;->a:LX/D49;

    invoke-virtual {v3, v0}, LX/D49;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)D

    move-result-wide v4

    .line 2117712
    iput-wide v4, v2, LX/2pZ;->e:D

    .line 2117713
    move-object v0, v2

    .line 2117714
    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x610c6c30

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2117715
    check-cast p1, LX/EPn;

    check-cast p2, LX/2pa;

    check-cast p3, LX/7Lk;

    check-cast p4, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;

    const/4 p0, 0x1

    .line 2117716
    invoke-virtual {p4}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v2

    move-object v1, p3

    .line 2117717
    check-cast v1, LX/Cxj;

    iget-object v4, p1, LX/EPn;->a:LX/CzL;

    invoke-interface {v1, v4}, LX/Cxj;->d(LX/CzL;)LX/CyM;

    move-result-object v1

    .line 2117718
    iget-boolean v4, v1, LX/CyM;->e:Z

    move v1, v4

    .line 2117719
    if-eqz v1, :cond_0

    .line 2117720
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x20c38145

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2117721
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoId()Ljava/lang/String;

    move-result-object v1

    .line 2117722
    iget-object v4, p1, LX/EPn;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2117723
    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 2117724
    :cond_1
    invoke-virtual {p4}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->getPluginSelector()LX/3Ge;

    move-result-object v1

    invoke-virtual {v1, v2, p2, p3}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 2117725
    iget-object v1, p1, LX/EPn;->b:LX/3It;

    .line 2117726
    iput-object v1, v2, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 2117727
    invoke-virtual {v2, p2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 2117728
    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v2, p0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2117729
    sget-object v1, LX/04D;->SEARCH_RESULTS:LX/04D;

    invoke-virtual {v2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 2117730
    invoke-virtual {v2, p0}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    goto :goto_0
.end method
