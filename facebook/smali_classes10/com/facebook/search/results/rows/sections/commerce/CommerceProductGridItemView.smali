.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/commerce/CommerceGridItemImageView;

.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2105400
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2105401
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2105398
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2105399
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    .line 2105381
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2105382
    iput-boolean v7, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->d:Z

    .line 2105383
    const v0, 0x7f03128a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2105384
    const v0, 0x7f0d0340

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceGridItemImageView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceGridItemImageView;

    .line 2105385
    const v0, 0x7f0d2b71

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2105386
    const v0, 0x7f0d0ad6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 2105387
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceGridItemImageView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const v1, 0x7f020c36

    invoke-virtual {v0, v1}, LX/1af;->b(I)V

    .line 2105388
    if-eqz p2, :cond_0

    .line 2105389
    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->CommerceProductGridItemView:[I

    invoke-virtual {v0, p2, v1, p3, v7}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2105390
    const/16 v1, 0x1

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b16ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    .line 2105391
    const/16 v2, 0x2

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1700

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    .line 2105392
    const/16 v3, 0x3

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b16ff

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v3

    float-to-int v3, v3

    .line 2105393
    const/16 v4, 0x4

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b1700

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    float-to-int v4, v4

    .line 2105394
    const/16 v5, 0x0

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    iput-boolean v5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->d:Z

    .line 2105395
    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v5, v1, v2, v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setPadding(IIII)V

    .line 2105396
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2105397
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2105379
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->a:Lcom/facebook/search/results/rows/sections/commerce/CommerceGridItemImageView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2105380
    return-void
.end method

.method public final b(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2105370
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2105371
    return-void
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 2105374
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 2105375
    iget-boolean v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->d:Z

    if-eqz v0, :cond_0

    .line 2105376
    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->getMeasuredWidth()I

    move-result v0

    .line 2105377
    invoke-virtual {p0, v0, v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->setMeasuredDimension(II)V

    .line 2105378
    :cond_0
    return-void
.end method

.method public setProductPrice(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2105372
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceProductGridItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2105373
    return-void
.end method
