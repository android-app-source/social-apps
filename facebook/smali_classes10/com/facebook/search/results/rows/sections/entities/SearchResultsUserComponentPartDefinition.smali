.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxA;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8d1;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static q:LX/0Xm;


# instance fields
.field private final d:LX/ELF;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ELB;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0N;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2hZ;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation
.end field

.field public final o:Ljava/lang/String;

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/ELF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Ot;)V
    .locals 0
    .param p13    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/ELF;",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/ELB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D0N;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/CvY;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1vg;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2hZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2dj;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Ot",
            "<",
            "LX/0W9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2110867
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2110868
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->d:LX/ELF;

    .line 2110869
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->e:LX/0Ot;

    .line 2110870
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->f:LX/0Ot;

    .line 2110871
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->g:LX/0Ot;

    .line 2110872
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->h:LX/0Ot;

    .line 2110873
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->i:LX/0Ot;

    .line 2110874
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->j:LX/0Ot;

    .line 2110875
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->k:LX/0Ot;

    .line 2110876
    iput-object p10, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->l:LX/0Ot;

    .line 2110877
    iput-object p11, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->m:LX/0Ot;

    .line 2110878
    iput-object p12, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->n:LX/0Ot;

    .line 2110879
    iput-object p13, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->o:Ljava/lang/String;

    .line 2110880
    iput-object p14, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->p:LX/0Ot;

    .line 2110881
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2110836
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2110837
    check-cast v0, LX/8d1;

    .line 2110838
    invoke-interface {v0}, LX/8d1;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    if-nez v1, :cond_2

    move-object v1, v2

    :goto_0
    invoke-static {v1}, LX/1eC;->a(LX/1Fc;)Landroid/net/Uri;

    move-result-object v1

    .line 2110839
    invoke-interface {v0}, LX/8d1;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    if-nez v3, :cond_3

    .line 2110840
    :goto_1
    new-instance v3, LX/EMR;

    invoke-direct {v3, p0, p2, p3}, LX/EMR;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/CzL;LX/1Pn;)V

    .line 2110841
    iget-object v4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->d:LX/ELF;

    invoke-virtual {v4, p1}, LX/ELF;->c(LX/1De;)LX/ELD;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/ELD;->a(Landroid/net/Uri;)LX/ELD;

    move-result-object v1

    .line 2110842
    iget-object v4, v1, LX/ELD;->a:LX/ELE;

    iput-object v2, v4, LX/ELE;->b:Ljava/lang/String;

    .line 2110843
    move-object v1, v1

    .line 2110844
    invoke-interface {v0}, LX/8d1;->o()Z

    move-result v2

    invoke-interface {v0}, LX/8d1;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->e:LX/0Ot;

    invoke-static {v2, v4, v5}, LX/ELM;->a(ZLjava/lang/String;LX/0Ot;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/ELD;->a(Ljava/lang/CharSequence;)LX/ELD;

    move-result-object v1

    invoke-static {p2, v7}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/ELD;->b(Ljava/lang/CharSequence;)LX/ELD;

    move-result-object v1

    invoke-static {p2, v8}, LX/ELM;->a(LX/CzL;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/ELD;->c(Ljava/lang/CharSequence;)LX/ELD;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/ELD;->c(Landroid/view/View$OnClickListener;)LX/ELD;

    move-result-object v4

    .line 2110845
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/D0N;

    invoke-virtual {v1, v0}, LX/D0N;->a(LX/8d1;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2110846
    invoke-static {}, LX/10A;->a()I

    move-result v2

    .line 2110847
    new-instance v1, LX/EMS;

    invoke-direct {v1, p0, v0, p3, p2}, LX/EMS;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/8d1;LX/1Pn;LX/CzL;)V

    move v3, v2

    move-object v2, v1

    .line 2110848
    :goto_2
    if-eqz v3, :cond_0

    .line 2110849
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1vg;

    invoke-virtual {v1, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    const v3, -0x6f6b64

    invoke-virtual {v1, v3}, LX/2xv;->i(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v4, v1}, LX/ELD;->a(LX/1n6;)LX/ELD;

    .line 2110850
    invoke-virtual {v4, v2}, LX/ELD;->d(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2110851
    :cond_0
    new-instance v1, LX/EMT;

    invoke-direct {v1, p0, v0, p3, p2}, LX/EMT;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/8d1;LX/1Pn;LX/CzL;)V

    .line 2110852
    new-instance v2, LX/EMU;

    invoke-direct {v2, p0, v0, p3, p2}, LX/EMU;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/8d1;LX/1Pn;LX/CzL;)V

    .line 2110853
    invoke-interface {v0}, LX/8d1;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/8d1;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v5

    check-cast p3, LX/CxV;

    invoke-interface {p3}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v6

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W9;

    invoke-static {v3, v5, v6, v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsOldEndpointEntityComponentPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/0W9;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/components/feed/ComponentPartDefinition;->c:LX/0ad;

    sget-short v3, LX/100;->c:S

    invoke-interface {v0, v3, v7}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2110854
    invoke-virtual {v4, v8}, LX/ELD;->a(Z)LX/ELD;

    .line 2110855
    const v0, 0x7f0822c5

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/ELD;->d(Ljava/lang/CharSequence;)LX/ELD;

    .line 2110856
    invoke-virtual {v4, v1}, LX/ELD;->a(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2110857
    const v0, 0x7f0822c6

    invoke-virtual {p1, v0}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/ELD;->e(Ljava/lang/CharSequence;)LX/ELD;

    .line 2110858
    invoke-virtual {v4, v2}, LX/ELD;->b(Landroid/view/View$OnClickListener;)LX/ELD;

    .line 2110859
    :cond_1
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    .line 2110860
    :cond_2
    invoke-interface {v0}, LX/8d1;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v1

    goto/16 :goto_0

    .line 2110861
    :cond_3
    invoke-interface {v0}, LX/8d1;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->d()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 2110862
    :cond_4
    invoke-interface {v0}, LX/8d1;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2110863
    new-instance v2, LX/EMX;

    invoke-direct {v2, p0, p2, p3}, LX/EMX;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/CzL;LX/1Pn;)V

    move-object v2, v2

    .line 2110864
    invoke-static {v1, v2}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserActionButtonPartDefinition;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Landroid/view/View$OnClickListener;)LX/EL8;

    move-result-object v1

    .line 2110865
    iget v2, v1, LX/EL8;->a:I

    .line 2110866
    iget-object v1, v1, LX/EL8;->c:Landroid/view/View$OnClickListener;

    move v3, v2

    move-object v2, v1

    goto/16 :goto_2
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;
    .locals 3

    .prologue
    .line 2110828
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    monitor-enter v1

    .line 2110829
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->q:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2110830
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->q:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2110831
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110832
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->b(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2110833
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2110834
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2110835
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/CzL;LX/CzL;Ljava/lang/Throwable;LX/1Pn;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;",
            "Ljava/lang/Throwable;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 2110818
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2hZ;

    .line 2110819
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2110820
    check-cast v1, LX/8d1;

    invoke-interface {v1}, LX/8d1;->dW_()Ljava/lang/String;

    move-result-object v1

    .line 2110821
    new-instance v2, LX/EMa;

    move-object v3, p0

    move-object v4, p4

    move-object v5, p1

    move-object v6, p2

    move-object v7, v1

    invoke-direct/range {v2 .. v7}, LX/EMa;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;LX/1Pn;LX/CzL;LX/CzL;Ljava/lang/String;)V

    move-object v1, v2

    .line 2110822
    invoke-virtual {v0, p3, v1}, LX/2hZ;->a(Ljava/lang/Throwable;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2110823
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    sget-object v1, LX/3Ql;->FAILED_MUTATION:LX/3Ql;

    invoke-virtual {v0, v1, p3}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    move-object v0, p4

    .line 2110824
    check-cast v0, LX/CxA;

    invoke-interface {v0, p2}, LX/CxA;->a(LX/CzL;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p4

    .line 2110825
    check-cast v0, LX/CxA;

    invoke-interface {v0, p2, p1}, LX/CxA;->a(LX/CzL;LX/CzL;)V

    .line 2110826
    check-cast p4, LX/1Pq;

    invoke-interface {p4}, LX/1Pq;->iN_()V

    .line 2110827
    :cond_0
    return-void
.end method

.method public static b(LX/CzL;)LX/CzL;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;)",
            "LX/CzL",
            "<+",
            "LX/8d1;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2110789
    iget-object v0, p0, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2110790
    check-cast v0, LX/8d1;

    .line 2110791
    invoke-interface {v0}, LX/8d1;->M()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2110792
    sget-object v2, LX/CzN;->a:LX/0P1;

    invoke-virtual {v2, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2110793
    if-nez v1, :cond_0

    .line 2110794
    :goto_0
    return-object p0

    .line 2110795
    :cond_0
    instance-of v2, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    if-eqz v2, :cond_1

    .line 2110796
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2110797
    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2110798
    invoke-static {v0}, LX/8dX;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;)LX/8dX;

    move-result-object v0

    .line 2110799
    iput-object v1, v0, LX/8dX;->F:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2110800
    move-object v0, v0

    .line 2110801
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2110802
    invoke-virtual {p0, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object p0

    goto :goto_0

    .line 2110803
    :cond_1
    instance-of v2, v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    if-eqz v2, :cond_2

    .line 2110804
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    .line 2110805
    invoke-static {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 2110806
    invoke-static {v0}, LX/8dQ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;)LX/8dQ;

    move-result-object v0

    .line 2110807
    iput-object v1, v0, LX/8dQ;->C:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2110808
    move-object v0, v0

    .line 2110809
    invoke-virtual {v0}, LX/8dQ;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    .line 2110810
    invoke-virtual {p0, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object p0

    goto :goto_0

    .line 2110811
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static b(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;
    .locals 15

    .prologue
    .line 2110816
    new-instance v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/ELF;->a(LX/0QB;)LX/ELF;

    move-result-object v2

    check-cast v2, LX/ELF;

    const/16 v3, 0x3512

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x33e8

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x34ae

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2eb

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x32d4

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x3b2

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x12b1

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xa7a

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xa71

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x113f

    invoke-static {p0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static {p0}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    const/16 v14, 0x2be

    invoke-static {p0, v14}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v14

    invoke-direct/range {v0 .. v14}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;-><init>(Landroid/content/Context;LX/ELF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Ljava/lang/String;LX/0Ot;)V

    .line 2110817
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2110815
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2110814
    check-cast p2, LX/CzL;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsUserComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2110813
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2110812
    const/4 v0, 0x0

    return-object v0
.end method
