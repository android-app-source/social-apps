.class public Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/commerce/SearchResultsCommerceModuleInterfaces$SearchResultsCommerceModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Uh;

.field private final b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;


# direct methods
.method public constructor <init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105160
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2105161
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;->a:LX/0Uh;

    .line 2105162
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    .line 2105163
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;
    .locals 5

    .prologue
    .line 2105164
    const-class v1, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;

    monitor-enter v1

    .line 2105165
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105166
    sput-object v2, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105167
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105168
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2105169
    new-instance p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;-><init>(LX/0Uh;Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;)V

    .line 2105170
    move-object v0, p0

    .line 2105171
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105172
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105173
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105174
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2105175
    check-cast p2, LX/CzL;

    const/4 v9, 0x6

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2105176
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2105177
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2105178
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bJ()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    move v0, v2

    .line 2105179
    :goto_0
    if-ge v3, v5, :cond_1

    if-ge v0, v9, :cond_1

    .line 2105180
    invoke-static {p2, v3}, LX/CzM;->a(LX/CzL;I)LX/CzL;

    move-result-object v6

    .line 2105181
    invoke-static {v6}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->a(LX/CzL;)Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    if-eq v7, v8, :cond_0

    .line 2105182
    invoke-virtual {v4, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2105183
    add-int/lit8 v0, v0, 0x1

    .line 2105184
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2105185
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2105186
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v9, :cond_2

    move v0, v1

    :goto_1
    const-string v4, "Not enough grid view items"

    invoke-static {v0, v4}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2105187
    const-string v0, "Number of grid view items must be exactly divisible by number of items in row"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2105188
    :goto_2
    const/4 v0, 0x2

    if-ge v2, v0, :cond_3

    .line 2105189
    mul-int/lit8 v0, v2, 0x3

    .line 2105190
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsTopTabGridRowPartDefinition;

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v4, v5, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2105191
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 2105192
    goto :goto_1

    .line 2105193
    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    .line 2105194
    check-cast p1, LX/CzL;

    const/4 v0, 0x0

    .line 2105195
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleProductItemsGridGroupPartDefinition;->a:LX/0Uh;

    sget v2, LX/2SU;->X:I

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v2, 0x0

    .line 2105196
    iget-object v1, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 2105197
    check-cast v1, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bJ()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v4

    move v1, v2

    move v3, v2

    .line 2105198
    :goto_0
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 2105199
    invoke-static {p1, v1}, LX/CzM;->a(LX/CzL;I)LX/CzL;

    move-result-object v5

    .line 2105200
    invoke-static {v5}, Lcom/facebook/search/results/rows/sections/commerce/CommerceModuleThumbnailPartDefinition;->a(LX/CzL;)Landroid/net/Uri;

    move-result-object v5

    sget-object p0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    if-eq v5, p0, :cond_0

    .line 2105201
    add-int/lit8 v3, v3, 0x1

    .line 2105202
    :cond_0
    const/4 v5, 0x6

    if-lt v3, v5, :cond_3

    .line 2105203
    const/4 v2, 0x1

    .line 2105204
    :cond_1
    move v1, v2

    .line 2105205
    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    return v0

    .line 2105206
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
