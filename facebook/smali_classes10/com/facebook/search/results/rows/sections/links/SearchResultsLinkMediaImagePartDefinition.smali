.class public Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/A3S;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2111927
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2111928
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;->a:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    .line 2111929
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;
    .locals 4

    .prologue
    .line 2111906
    const-class v1, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;

    monitor-enter v1

    .line 2111907
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2111908
    sput-object v2, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2111909
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2111910
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2111911
    new-instance p0, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;-><init>(Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;)V

    .line 2111912
    move-object v0, p0

    .line 2111913
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2111914
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2111915
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2111916
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2111917
    check-cast p2, LX/A3S;

    .line 2111918
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/links/SearchResultsLinkMediaImagePartDefinition;->a:Lcom/facebook/multirow/parts/ContentViewThumbnailUriStringPartDefinition;

    const/4 v1, 0x0

    .line 2111919
    invoke-interface {p2}, LX/A3S;->bS()LX/A4E;

    move-result-object p0

    .line 2111920
    if-nez p0, :cond_1

    .line 2111921
    :cond_0
    :goto_0
    move-object v1, v1

    .line 2111922
    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2111923
    const/4 v0, 0x0

    return-object v0

    .line 2111924
    :cond_1
    invoke-interface {p0}, LX/A4E;->b()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object p0

    .line 2111925
    if-eqz p0, :cond_0

    .line 2111926
    invoke-virtual {p0}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
