.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "LX/8dA;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

.field private final d:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition",
            "<TE;",
            "LX/8dA;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

.field private final g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

.field private final h:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

.field private final i:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2109028
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2109029
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    .line 2109030
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2109031
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    .line 2109032
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->d:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2109033
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    .line 2109034
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    .line 2109035
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    .line 2109036
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    .line 2109037
    iput-object p9, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->i:LX/0Uh;

    .line 2109038
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;
    .locals 13

    .prologue
    .line 2109039
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;

    monitor-enter v1

    .line 2109040
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2109041
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2109042
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2109043
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2109044
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v12

    check-cast v12, LX/0Uh;

    invoke-direct/range {v3 .. v12}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;LX/0Uh;)V

    .line 2109045
    move-object v0, v3

    .line 2109046
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2109047
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2109048
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2109049
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2109050
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityWithFacepileRowPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2109051
    check-cast p2, LX/CzL;

    .line 2109052
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->a:Lcom/facebook/search/results/rows/sections/background/SearchResultsBackgroundPartDefinition;

    new-instance v1, LX/EJI;

    sget-object v2, LX/3ap;->a:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/EJI;-><init>(LX/CzL;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109053
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->b:Lcom/facebook/multirow/parts/ContentViewTitlePartDefinition;

    .line 2109054
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109055
    check-cast v0, LX/8dA;

    invoke-interface {v0}, LX/8dA;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109056
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->i:LX/0Uh;

    invoke-static {p2, v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->a(LX/CzL;LX/0Uh;)Ljava/lang/String;

    move-result-object v1

    .line 2109057
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->c:Lcom/facebook/multirow/parts/ContentViewSubtitlePartDefinition;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109058
    const v0, 0x7f0d0d8e

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->f:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityGroupFacepilePartDefinition;

    new-instance v3, LX/ELL;

    const/4 v4, 0x4

    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->a(LX/CzL;)LX/0Px;

    move-result-object v5

    invoke-direct {v3, v4, v5}, LX/ELL;-><init>(ILX/0Px;)V

    invoke-interface {p1, v0, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 2109059
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->d:Lcom/facebook/multirow/parts/ContentDescriptionPartDefinition;

    .line 2109060
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2109061
    check-cast v0, LX/8dA;

    invoke-interface {v0}, LX/8dA;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, LX/ELM;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109062
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->e:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityClickListenerPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109063
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->g:Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupActionButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109064
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->h:Lcom/facebook/multirow/parts/ContentViewMetaTextPartDefinition;

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupItemPartDefinition;->i:LX/0Uh;

    invoke-static {p2, v1}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsGroupPartDefinition;->b(LX/CzL;LX/0Uh;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2109065
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2109066
    const/4 v0, 0x1

    return v0
.end method
