.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/CxA;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/entity/SearchResultsAppResultInterfaces$SearchResultsAppResult;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2107860
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2107861
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;

    .line 2107862
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;
    .locals 4

    .prologue
    .line 2107863
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;

    monitor-enter v1

    .line 2107864
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2107865
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2107866
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2107867
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2107868
    new-instance p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;)V

    .line 2107869
    move-object v0, p0

    .line 2107870
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2107871
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2107872
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2107873
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2107874
    sget-object v0, LX/3ap;->c:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2107875
    check-cast p2, LX/CzL;

    const/4 v2, 0x0

    .line 2107876
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2107877
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->a()LX/8d8;

    move-result-object v0

    .line 2107878
    if-nez v0, :cond_0

    .line 2107879
    :goto_0
    return-object v2

    .line 2107880
    :cond_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppResultPartDefinition;->a:Lcom/facebook/search/results/rows/sections/entities/SearchResultsAppPartDefinition;

    invoke-virtual {p2, v0}, LX/CzL;->a(Ljava/lang/Object;)LX/CzL;

    move-result-object v0

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2107881
    const/4 v0, 0x1

    return v0
.end method
