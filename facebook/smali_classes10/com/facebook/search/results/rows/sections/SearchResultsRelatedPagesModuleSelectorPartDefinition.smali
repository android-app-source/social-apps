.class public Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsEdgeInterfaces$SearchResultsEdge$Node;",
        ">;",
        "Ljava/lang/Void;",
        "LX/Cxk;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPageHScrollModuleGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/unsupported/SearchResultsUnsupportedResultPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsRelatedPageHScrollModuleGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/entities/SearchResultsPageModuleGroupPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103086
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2103087
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;->a:LX/0Ot;

    .line 2103088
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;->b:LX/0Ot;

    .line 2103089
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;->c:LX/0Ot;

    .line 2103090
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;
    .locals 6

    .prologue
    .line 2103091
    const-class v1, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;

    monitor-enter v1

    .line 2103092
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103093
    sput-object v2, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103094
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103095
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103096
    new-instance v3, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;

    const/16 v4, 0x11a4

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x3405

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x33ff

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2103097
    move-object v0, v3

    .line 2103098
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103099
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103100
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103101
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2103102
    check-cast p2, LX/CzL;

    .line 2103103
    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;->a:LX/0Ot;

    invoke-static {p1, v0, v1, p2}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;->b:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/SearchResultsRelatedPagesModuleSelectorPartDefinition;->c:LX/0Ot;

    new-instance v2, LX/EPd;

    invoke-virtual {p2}, LX/CzL;->f()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v3

    invoke-virtual {p2}, LX/CzL;->h()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v4

    invoke-virtual {p2}, LX/CzL;->g()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, LX/EPd;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLObjectType;)V

    invoke-virtual {v0, v1, v2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2103104
    const/4 v0, 0x0

    return-object v0

    .line 2103105
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2103106
    check-cast p1, LX/CzL;

    .line 2103107
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
