.class public Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition",
            "<",
            "Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116932
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2116933
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;

    .line 2116934
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;

    .line 2116935
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    .line 2116936
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;
    .locals 6

    .prologue
    .line 2116937
    const-class v1, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;

    monitor-enter v1

    .line 2116938
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116939
    sput-object v2, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116940
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116941
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116942
    new-instance p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;)V

    .line 2116943
    move-object v0, p0

    .line 2116944
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116945
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116946
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116947
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2116948
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2116949
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2116950
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;

    .line 2116951
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2116952
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionTitlePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2116953
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/spellcorrection/SearchResultsSpellCorrectionEscapePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2116954
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116955
    const/4 v0, 0x1

    return v0
.end method
