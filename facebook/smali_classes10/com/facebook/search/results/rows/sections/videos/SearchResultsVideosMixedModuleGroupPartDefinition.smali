.class public Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxj;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/video/SearchResultsVideosMixedModuleInterfaces$SearchResultsVideosMixedModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector",
            "<",
            "Lcom/facebook/search/results/protocol/video/SearchResultsVideosMixedModuleInterfaces$SearchResultsVideosMixedModule;",
            "TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/inline/SearchResultsVideoSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/videos/SearchResultsWebVideoPartDefinition;",
            ">;",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;",
            "LX/0Ot",
            "<",
            "LX/2Sc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2117826
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2117827
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    .line 2117828
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->b:LX/0Ot;

    .line 2117829
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->c:LX/0Ot;

    .line 2117830
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2117831
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->e:LX/0Ot;

    .line 2117832
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;
    .locals 9

    .prologue
    .line 2117833
    const-class v1, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;

    monitor-enter v1

    .line 2117834
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2117835
    sput-object v2, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2117836
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2117837
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2117838
    new-instance v3, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    const/16 v5, 0x34ac

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x11ab

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    const/16 v8, 0x113f

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;LX/0Ot;LX/0Ot;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;LX/0Ot;)V

    .line 2117839
    move-object v0, v3

    .line 2117840
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2117841
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2117842
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2117843
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2117844
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxi;

    const/4 v1, 0x0

    .line 2117845
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117846
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117847
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bX()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    .line 2117848
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v1

    move v2, v1

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2117849
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->s()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2117850
    if-nez v0, :cond_0

    .line 2117851
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sc;

    sget-object v6, LX/3Ql;->INVALID_SEARCH_RESULT:LX/3Ql;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "Missing node for videos mixed module. Query: "

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object v1, p3

    check-cast v1, LX/CxV;

    invoke-interface {v1}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "Item Index: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    move v0, v2

    .line 2117852
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 2117853
    :cond_0
    const/4 v6, 0x0

    const/4 v7, -0x1

    .line 2117854
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->n()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    .line 2117855
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    .line 2117856
    :goto_2
    if-ne v1, v7, :cond_5

    .line 2117857
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Sc;

    sget-object v7, LX/3Ql;->INVALID_SEARCH_RESULT:LX/3Ql;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Missing object type for node: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    move-object v1, v6

    .line 2117858
    :goto_3
    move-object v1, v1

    .line 2117859
    if-eqz v1, :cond_3

    .line 2117860
    invoke-virtual {p2, v0, v2}, LX/CzL;->a(Ljava/lang/Object;I)LX/CzL;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117861
    add-int/lit8 v0, v2, 0x1

    goto :goto_1

    .line 2117862
    :cond_1
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2117863
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->R()LX/8dH;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2117864
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2117865
    :cond_2
    const/4 v0, 0x0

    return-object v0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v7

    .line 2117866
    goto :goto_2

    .line 2117867
    :cond_5
    sparse-switch v1, :sswitch_data_0

    .line 2117868
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Sc;

    sget-object v7, LX/3Ql;->INVALID_SEARCH_RESULT:LX/3Ql;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Unknown result type for videos mixed module: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    move-object v1, v6

    .line 2117869
    goto :goto_3

    .line 2117870
    :sswitch_0
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_3

    .line 2117871
    :sswitch_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/videos/SearchResultsVideosMixedModuleGroupPartDefinition;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x4ed245b -> :sswitch_0
        0x1eaef984 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2117872
    check-cast p1, LX/CzL;

    .line 2117873
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
