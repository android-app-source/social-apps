.class public Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2103186
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2103187
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;

    .line 2103188
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;
    .locals 4

    .prologue
    .line 2103189
    const-class v1, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;

    monitor-enter v1

    .line 2103190
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103191
    sput-object v2, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103192
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103193
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103194
    new-instance p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;)V

    .line 2103195
    move-object v0, p0

    .line 2103196
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103197
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103198
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103199
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2103200
    check-cast p2, LX/CzL;

    .line 2103201
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/answer/SearchResultsBirthdayTextPartDefinition;

    invoke-static {p1, v0, p2}, LX/EJ4;->a(LX/1RF;Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;LX/CzL;)V

    .line 2103202
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2103203
    check-cast p1, LX/CzL;

    .line 2103204
    invoke-static {p1}, LX/CzM;->b(LX/CzL;)Z

    move-result v0

    return v0
.end method
