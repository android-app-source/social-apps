.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/8d8;",
        ":",
        "LX/8d9;",
        ":",
        "LX/8dA;",
        ":",
        "LX/8d0;",
        ":",
        "LX/8dB;",
        ":",
        "LX/8d1;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<TT;",
        "Ljava/lang/CharSequence;",
        "LX/1PW;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/8i7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108606
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2108607
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityTitlePartDefinition;->a:LX/0Ot;

    .line 2108608
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityTitlePartDefinition;->b:LX/0Ot;

    .line 2108609
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2108604
    check-cast p2, LX/8d8;

    .line 2108605
    move-object v0, p2

    check-cast v0, LX/8d1;

    invoke-interface {v0}, LX/8d1;->o()Z

    move-result v1

    move-object v0, p2

    check-cast v0, LX/8d1;

    invoke-interface {v0}, LX/8d1;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityTitlePartDefinition;->a:LX/0Ot;

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityTitlePartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3my;

    check-cast p2, LX/8dA;

    invoke-interface {p2}, LX/8dA;->p()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v0

    invoke-static {v1, v2, v3, v0}, LX/ELM;->a(ZLjava/lang/String;LX/0Ot;Z)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x24d65c90

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2108601
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2108602
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2108603
    const/16 v1, 0x1f

    const v2, -0x58d1413c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
