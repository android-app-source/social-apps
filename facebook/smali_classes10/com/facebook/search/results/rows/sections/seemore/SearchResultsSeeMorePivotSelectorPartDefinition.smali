.class public Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/Cxa;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsSeeMorePivotModuleInterfaces$SearchResultsSeeMorePivotModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSocialSeeMoreComponentPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotModulePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSocialSeeMoreComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116474
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2116475
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;->a:LX/0Ot;

    .line 2116476
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;->b:LX/0Ot;

    .line 2116477
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;
    .locals 5

    .prologue
    .line 2116478
    const-class v1, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;

    monitor-enter v1

    .line 2116479
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116480
    sput-object v2, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116481
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116482
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116483
    new-instance v3, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;

    const/16 v4, 0x348b

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x3490

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 2116484
    move-object v0, v3

    .line 2116485
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116486
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116487
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116488
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2116459
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    .line 2116460
    check-cast p3, LX/1Po;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2116461
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116462
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->fg_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 2116463
    :goto_0
    move v0, v0

    .line 2116464
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;->b:LX/0Ot;

    .line 2116465
    move-object v1, v1

    .line 2116466
    invoke-static {p1, v0, v1, p2}, LX/1RG;->a(LX/1RF;ZLX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMorePivotSelectorPartDefinition;->a:LX/0Ot;

    .line 2116467
    move-object v1, v1

    .line 2116468
    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 2116469
    const/4 v0, 0x0

    return-object v0

    .line 2116470
    :cond_0
    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v3, LX/1Qt;->SEARCH_DENSE_FEED:LX/1Qt;

    if-ne v0, v3, :cond_2

    invoke-virtual {p2}, LX/CzL;->i()LX/0Px;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 2116471
    :goto_1
    if-nez v0, :cond_1

    invoke-virtual {p2}, LX/CzL;->i()LX/0Px;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->FULL_CARD_FOOTER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2116472
    goto :goto_1

    :cond_3
    move v0, v1

    .line 2116473
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2116458
    const/4 v0, 0x1

    return v0
.end method
