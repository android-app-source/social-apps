.class public Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104139
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2104140
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;

    .line 2104141
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    .line 2104142
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

    .line 2104143
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    .line 2104144
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;
    .locals 7

    .prologue
    .line 2104145
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;

    monitor-enter v1

    .line 2104146
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104147
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104148
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104149
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104150
    new-instance p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;)V

    .line 2104151
    move-object v0, p0

    .line 2104152
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104153
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104154
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104155
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2104156
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 2104157
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/collection/SearchResultsCollectionHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2104158
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2104159
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2104160
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2104161
    iget-boolean p3, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->p:Z

    move v0, p3

    .line 2104162
    if-eqz v0, :cond_0

    .line 2104163
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/common/SearchResultsZeroPadDividerPartDefinition;

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2104164
    :goto_0
    return-object v1

    .line 2104165
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/collection/SearchResultsFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2104166
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2104167
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/collection/OldSearchResultsGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsItemsSelectorPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
