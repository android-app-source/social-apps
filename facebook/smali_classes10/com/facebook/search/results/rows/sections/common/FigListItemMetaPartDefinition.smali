.class public Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/CharSequence;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/fig/listitem/FigListItem;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2105857
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2105858
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;
    .locals 3

    .prologue
    .line 2105859
    const-class v1, Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;

    monitor-enter v1

    .line 2105860
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2105861
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2105862
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2105863
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2105864
    new-instance v0, Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;

    invoke-direct {v0}, Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;-><init>()V

    .line 2105865
    move-object v0, v0

    .line 2105866
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2105867
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/FigListItemMetaPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2105868
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2105869
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x75dbd98

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2105870
    check-cast p1, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/fig/listitem/FigListItem;

    .line 2105871
    invoke-virtual {p4, p1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2105872
    const/16 v1, 0x1f

    const v2, -0x2f09273b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
