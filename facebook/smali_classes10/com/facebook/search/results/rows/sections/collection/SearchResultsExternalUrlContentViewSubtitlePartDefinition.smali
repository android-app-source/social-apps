.class public Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        "Ljava/lang/CharSequence;",
        "TE;",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/EJ5;


# direct methods
.method public constructor <init>(LX/EJ5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2104362
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 2104363
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;->a:LX/EJ5;

    .line 2104364
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;
    .locals 4

    .prologue
    .line 2104351
    const-class v1, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    monitor-enter v1

    .line 2104352
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2104353
    sput-object v2, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2104354
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2104355
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2104356
    new-instance p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;

    invoke-static {v0}, LX/EJ5;->a(LX/0QB;)LX/EJ5;

    move-result-object v3

    check-cast v3, LX/EJ5;

    invoke-direct {p0, v3}, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;-><init>(LX/EJ5;)V

    .line 2104357
    move-object v0, p0

    .line 2104358
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2104359
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2104360
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2104361
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2104346
    check-cast p2, Lcom/facebook/graphql/model/GraphQLNode;

    check-cast p3, LX/1Pn;

    .line 2104347
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->iI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->iI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 2104348
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->bH()J

    move-result-wide v2

    .line 2104349
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/collection/SearchResultsExternalUrlContentViewSubtitlePartDefinition;->a:LX/EJ5;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v1, v0, v2, v3, v4}, LX/EJ5;->a(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 2104350
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x47ea43c0    # 119943.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2104343
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2104344
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2104345
    const/16 v1, 0x1f

    const v2, 0x6ccf38a8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
