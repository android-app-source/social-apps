.class public Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2115817
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2115818
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

    .line 2115819
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;

    .line 2115820
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;
    .locals 5

    .prologue
    .line 2115821
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;

    monitor-enter v1

    .line 2115822
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2115823
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2115824
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115825
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2115826
    new-instance p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;)V

    .line 2115827
    move-object v0, p0

    .line 2115828
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2115829
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2115830
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2115831
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2115832
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2115833
    invoke-static {p2}, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2115834
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalHScrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2115835
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 2115836
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/pulse/PulseSentimentEmotionalVerticalPartDefinition;

    .line 2115837
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2115838
    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2115839
    const/4 v0, 0x1

    return v0
.end method
