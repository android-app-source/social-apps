.class public Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/CxV;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleInterfaces$SearchResultsOpinionModule;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/ENp;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/ENp;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2113535
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2113536
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;->d:LX/ENp;

    .line 2113537
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/1Ps;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsOpinionModuleInterfaces$SearchResultsOpinionModule;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2113538
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;->d:LX/ENp;

    const/4 v1, 0x0

    .line 2113539
    new-instance v2, LX/ENo;

    invoke-direct {v2, v0}, LX/ENo;-><init>(LX/ENp;)V

    .line 2113540
    iget-object p0, v0, LX/ENp;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/ENn;

    .line 2113541
    if-nez p0, :cond_0

    .line 2113542
    new-instance p0, LX/ENn;

    invoke-direct {p0, v0}, LX/ENn;-><init>(LX/ENp;)V

    .line 2113543
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/ENn;->a$redex0(LX/ENn;LX/1De;IILX/ENo;)V

    .line 2113544
    move-object v2, p0

    .line 2113545
    move-object v1, v2

    .line 2113546
    move-object v0, v1

    .line 2113547
    iget-object v1, v0, LX/ENn;->a:LX/ENo;

    iput-object p2, v1, LX/ENo;->a:LX/CzL;

    .line 2113548
    iget-object v1, v0, LX/ENn;->e:Ljava/util/BitSet;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2113549
    move-object v0, v0

    .line 2113550
    iget-object v1, v0, LX/ENn;->a:LX/ENo;

    iput-object p3, v1, LX/ENo;->b:LX/1Ps;

    .line 2113551
    iget-object v1, v0, LX/ENn;->e:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 2113552
    move-object v0, v0

    .line 2113553
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;
    .locals 5

    .prologue
    .line 2113554
    const-class v1, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;

    monitor-enter v1

    .line 2113555
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113556
    sput-object v2, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113557
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113558
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113559
    new-instance p0, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/ENp;->a(LX/0QB;)LX/ENp;

    move-result-object v4

    check-cast v4, LX/ENp;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;-><init>(Landroid/content/Context;LX/ENp;)V

    .line 2113560
    move-object v0, p0

    .line 2113561
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113562
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113563
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113564
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2113565
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2113566
    check-cast p2, LX/CzL;

    check-cast p3, LX/1Ps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/opinion/SearchResultsOpinionSearchModuleComponentPartDefinition;->a(LX/1De;LX/CzL;LX/1Ps;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2113567
    check-cast p1, LX/CzL;

    .line 2113568
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2113569
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->bO()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2113570
    const/4 v0, 0x0

    return-object v0
.end method
