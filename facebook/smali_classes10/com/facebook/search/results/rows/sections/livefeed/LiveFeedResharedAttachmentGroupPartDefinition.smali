.class public Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsLiveFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;

.field private final c:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2112291
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2112292
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->a:Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    .line 2112293
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;

    .line 2112294
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    .line 2112295
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;

    .line 2112296
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;

    .line 2112297
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;
    .locals 9

    .prologue
    .line 2112298
    const-class v1, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;

    monitor-enter v1

    .line 2112299
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2112300
    sput-object v2, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2112301
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2112302
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2112303
    new-instance v3, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;)V

    .line 2112304
    move-object v0, v3

    .line 2112305
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2112306
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2112307
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2112308
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2112309
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2112310
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2112311
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2112312
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2112313
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2112314
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentHeaderPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2112315
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentPhotoPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2112316
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentVideoPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2112317
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/EmptyFooterPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2112318
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2112319
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2112320
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/LiveFeedResharedAttachmentGroupPartDefinition;->a:Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
