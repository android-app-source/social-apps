.class public Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxh;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/CxV;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/CxP;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/CzL",
        "<+",
        "Lcom/facebook/search/results/protocol/SearchResultsRelatedSharesModuleInterfaces$SearchResultsRelatedSharesModule$ModuleResults$Edges;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EK7;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EPA;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Uh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/EK7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EPA;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2116071
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2116072
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->d:LX/0Ot;

    .line 2116073
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    .line 2116074
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->f:LX/0Uh;

    .line 2116075
    return-void
.end method

.method private a(LX/1De;LX/CzL;LX/Cxh;)LX/1X1;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CzL",
            "<+",
            "Lcom/facebook/search/results/protocol/SearchResultsRelatedSharesModuleInterfaces$SearchResultsRelatedSharesModule$ModuleResults$Edges;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2116032
    iget-object v0, p2, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116033
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->r()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v2

    .line 2116034
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EK7;

    invoke-virtual {v0, p1}, LX/EK7;->c(LX/1De;)LX/EK5;

    move-result-object v0

    .line 2116035
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bS()LX/A4E;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bS()LX/A4E;

    move-result-object v1

    invoke-interface {v1}, LX/A4E;->b()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2116036
    :cond_0
    const/4 v1, 0x0

    .line 2116037
    :goto_0
    move-object v1, v1

    .line 2116038
    invoke-virtual {v0, v1}, LX/EK5;->c(Ljava/lang/String;)LX/EK5;

    move-result-object v0

    .line 2116039
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2116040
    invoke-virtual {v0, v1}, LX/EK5;->a(Ljava/lang/CharSequence;)LX/EK5;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    .line 2116041
    const/4 v4, 0x0

    .line 2116042
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2116043
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel$LatestVersionModel$DocumentOwnerModel;->b()Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    .line 2116044
    :goto_1
    iget-object v4, v0, LX/EPA;->c:LX/0ad;

    sget-short v6, LX/100;->Y:S

    const/4 v7, 0x0

    invoke-interface {v4, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2116045
    iget-object v4, v0, LX/EPA;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EJ5;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->W()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7, p1}, LX/EJ5;->b(Ljava/lang/String;JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 2116046
    :cond_1
    move-object v0, v5

    .line 2116047
    invoke-virtual {v1, v0}, LX/EK5;->b(Ljava/lang/CharSequence;)LX/EK5;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    .line 2116048
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cv()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cv()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cv()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel$ProfilePictureModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, v0, LX/EPA;->c:LX/0ad;

    sget-short v4, LX/100;->W:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2116049
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cv()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel$ProfilePictureModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2116050
    :goto_2
    move-object v0, v3

    .line 2116051
    invoke-virtual {v1, v0}, LX/EK5;->b(Ljava/lang/String;)LX/EK5;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2116052
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cv()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cv()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 2116053
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cv()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel;->j()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel$ProfilePictureModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ExternalUrlOwningProfileModel$ProfilePictureModel;->a()I

    move-result v1

    .line 2116054
    :goto_3
    move v1, v1

    .line 2116055
    invoke-virtual {v0, v1}, LX/EK5;->h(I)LX/EK5;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    move-object v1, p3

    check-cast v1, LX/1Ps;

    const/4 v5, 0x0

    .line 2116056
    iget-object v4, v0, LX/EPA;->c:LX/0ad;

    sget-short v6, LX/100;->Y:S

    invoke-interface {v4, v6, v5}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-static {v1}, LX/EPA;->a(LX/1Ps;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cx()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 2116057
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cx()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SummaryModel;->a()Ljava/lang/String;

    move-result-object v4

    .line 2116058
    :goto_4
    move-object v0, v4

    .line 2116059
    invoke-virtual {v3, v0}, LX/EK5;->c(Ljava/lang/CharSequence;)LX/EK5;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    move-object v1, p3

    check-cast v1, LX/CxP;

    .line 2116060
    new-instance v4, LX/EP8;

    invoke-direct {v4, v0, p2, v1}, LX/EP8;-><init>(LX/EPA;LX/CzL;LX/CxP;)V

    move-object v0, v4

    .line 2116061
    invoke-virtual {v3, v0}, LX/EK5;->a(Landroid/view/View$OnClickListener;)LX/EK5;

    move-result-object v0

    const v1, 0x7f0208ca

    invoke-virtual {v0, v1}, LX/EK5;->n(I)LX/EK5;

    move-result-object v1

    .line 2116062
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/EPA;

    check-cast p3, LX/1Ps;

    invoke-virtual {v0, p3, v1}, LX/EPA;->a(LX/1Ps;LX/EK5;)V

    .line 2116063
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cy()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$InstantArticleModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2116064
    const v0, 0x7f02077d

    invoke-virtual {v1, v0}, LX/EK5;->i(I)LX/EK5;

    .line 2116065
    :cond_2
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_3
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bS()LX/A4E;

    move-result-object v1

    invoke-interface {v1}, LX/A4E;->b()Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/pulse/SearchResultsLinkMediaImageModels$SearchResultsLinkMediaImageModel$LinkMediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2116066
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cu()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 2116067
    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cu()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$SourceModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/EJ5;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    goto/16 :goto_1

    :cond_5
    move-object v5, v4

    goto/16 :goto_1

    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 2116068
    :cond_8
    iget-object v4, v0, LX/EPA;->c:LX/0ad;

    sget-short v6, LX/100;->Y:S

    invoke-interface {v4, v6, v5}, LX/0ad;->a(SZ)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-static {v1}, LX/EPA;->a(LX/1Ps;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 2116069
    const/4 v4, 0x0

    goto :goto_4

    .line 2116070
    :cond_9
    iget-object v4, v0, LX/EPA;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/EJ5;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->W()J

    move-result-wide v6

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v8

    if-eqz v8, :cond_a

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cz()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$AllShareStoriesModel;->a()I

    move-result v5

    :cond_a
    invoke-virtual {v4, v6, v7, v5, p1}, LX/EJ5;->a(JILandroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_4
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;
    .locals 7

    .prologue
    .line 2116086
    const-class v1, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;

    monitor-enter v1

    .line 2116087
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2116088
    sput-object v2, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2116089
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2116090
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2116091
    new-instance v5, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x33bc

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v4, 0x3480

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, v3, v6, p0, v4}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Uh;)V

    .line 2116092
    move-object v0, v5

    .line 2116093
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2116094
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2116095
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2116096
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2116085
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxh;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->a(LX/1De;LX/CzL;LX/Cxh;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2116097
    check-cast p2, LX/CzL;

    check-cast p3, LX/Cxh;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->a(LX/1De;LX/CzL;LX/Cxh;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2116078
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 2116079
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116080
    if-eqz v0, :cond_0

    .line 2116081
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116082
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->r()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2116083
    iget-object v0, p1, LX/CzL;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 2116084
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->r()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->cw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/pulse/SearchResultsRelatedSharesArticlePartDefinition;->f:LX/0Uh;

    sget v2, LX/2SU;->ab:I

    invoke-virtual {v0, v2, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2116077
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 2116076
    sget-object v0, Lcom/facebook/components/feed/ComponentPartDefinition;->a:LX/1Cz;

    return-object v0
.end method
