.class public Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPostHeaderUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/BetterTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/1Ua;

.field private static e:LX/0Xm;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

.field private final d:Lcom/facebook/multirow/parts/TextPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2106195
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v0

    const/high16 v1, 0x40c00000    # 6.0f

    .line 2106196
    iput v1, v0, LX/1UY;->b:F

    .line 2106197
    move-object v0, v0

    .line 2106198
    const/high16 v1, -0x3f800000    # -4.0f

    .line 2106199
    iput v1, v0, LX/1UY;->c:F

    .line 2106200
    move-object v0, v0

    .line 2106201
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->a:LX/1Ua;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2106190
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 2106191
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->b:Landroid/content/Context;

    .line 2106192
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    .line 2106193
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    .line 2106194
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;
    .locals 6

    .prologue
    .line 2106202
    const-class v1, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;

    monitor-enter v1

    .line 2106203
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2106204
    sput-object v2, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2106205
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2106206
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2106207
    new-instance p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/TextPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/TextPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/TextPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/styling/PaddingPartDefinition;Lcom/facebook/multirow/parts/TextPartDefinition;)V

    .line 2106208
    move-object v0, p0

    .line 2106209
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2106210
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2106211
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2106212
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 2106189
    sget-object v0, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2106180
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2106181
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2106182
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsPostHeaderUnit;

    .line 2106183
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsPostHeaderUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v1, v1

    .line 2106184
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->FEED_POSTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0822a4

    .line 2106185
    :goto_0
    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->d:Lcom/facebook/multirow/parts/TextPartDefinition;

    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2106186
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->c:Lcom/facebook/feed/rows/styling/PaddingPartDefinition;

    new-instance v2, LX/3aw;

    sget-object v3, Lcom/facebook/search/results/rows/sections/common/SearchResultsPostsHeaderPartDefinition;->a:LX/1Ua;

    sget-object v4, LX/1X9;->TOP:LX/1X9;

    const/4 v5, 0x0

    invoke-direct {v2, v0, v3, v4, v5}, LX/3aw;-><init>(Lcom/facebook/graphql/model/FeedUnit;LX/1Ua;LX/1X9;I)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 2106187
    const/4 v0, 0x0

    return-object v0

    .line 2106188
    :cond_0
    const v1, 0x7f0822a3

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2106179
    const/4 v0, 0x1

    return v0
.end method
