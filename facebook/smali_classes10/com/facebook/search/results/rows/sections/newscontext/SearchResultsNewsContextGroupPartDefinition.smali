.class public Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Ljava/lang/Void;",
        "LX/Cxk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;

.field private final d:Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded",
            "<",
            "Ljava/lang/String;",
            "-",
            "LX/Cxk;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;

.field private final f:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;


# direct methods
.method public constructor <init>(LX/0ad;Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryPartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextSummaryComponentPartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113263
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2113264
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;

    .line 2113265
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;

    .line 2113266
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;

    .line 2113267
    sget-short v0, LX/100;->T:S

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iput-object p6, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->d:Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    .line 2113268
    iput-object p7, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;

    .line 2113269
    iput-object p8, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->f:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;

    .line 2113270
    return-void

    :cond_0
    move-object p6, p5

    .line 2113271
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2113272
    check-cast p2, LX/CzL;

    .line 2113273
    iget-object v0, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2113274
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2113275
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderVideoPartDefinition;

    invoke-static {p1, v1, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextHeaderPhotoPartDefinition;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v3

    .line 2113276
    iget-object v4, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v4, v4

    .line 2113277
    invoke-static {v3, v4}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 2113278
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextTitlePartDefinition;

    .line 2113279
    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;->a(LX/8d6;)Z

    move-result v2

    const-string v3, "Insufficient data to bind provided node: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object v0, v4, p3

    invoke-static {v2, v3, v4}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 2113280
    new-instance v3, LX/CzK;

    invoke-interface {v0}, LX/8d6;->bh()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, LX/8d6;->bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->fs_()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, LX/CzK;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    .line 2113281
    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113282
    invoke-interface {v0}, LX/8d6;->bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v2

    .line 2113283
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 2113284
    :goto_0
    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v1, v1

    .line 2113285
    invoke-interface {v0}, LX/8d6;->bg()Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;

    move-result-object v3

    .line 2113286
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    .line 2113287
    :goto_2
    if-eqz v2, :cond_5

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsTrendingTopicDataModels$SearchResultsTrendingTopicDataModel;->a()Ljava/lang/String;

    move-result-object v2

    :goto_3
    move-object v2, v2

    .line 2113288
    if-eqz v1, :cond_0

    .line 2113289
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->d:Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v3, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113290
    :cond_0
    if-eqz v2, :cond_1

    .line 2113291
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextAttributionPartDefinition;

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113292
    :cond_1
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsContextGroupPartDefinition;->f:Lcom/facebook/search/results/rows/sections/newscontext/ShareTopicFooterPartDefinition;

    .line 2113293
    iget-object v2, p2, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v2, v2

    .line 2113294
    invoke-static {v0, v2}, LX/CzL;->a(Ljava/lang/Object;Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CzL;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113295
    const/4 v0, 0x0

    return-object v0

    .line 2113296
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 2113297
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 2113298
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 2113299
    :cond_5
    const/4 v2, 0x0

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2113300
    check-cast p1, LX/CzL;

    const/4 v1, 0x0

    .line 2113301
    iget-object v0, p1, LX/CzL;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-object v0, v0

    .line 2113302
    invoke-static {v0}, LX/8eM;->h(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/0Px;

    move-result-object v2

    .line 2113303
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2113304
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/newscontext/SearchResultsNewsTitlePartDefinition;->a(LX/8d6;)Z

    move-result v0

    goto :goto_0
.end method
