.class public Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;
.super Landroid/view/View;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2112339
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2112340
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->a()V

    .line 2112341
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2112342
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2112343
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->a()V

    .line 2112344
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2112345
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2112346
    invoke-direct {p0}, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->a()V

    .line 2112347
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 2112348
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->a:Landroid/graphics/Paint;

    .line 2112349
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0168

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2112350
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b16d9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2112351
    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b16d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->b:I

    .line 2112352
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 2112353
    invoke-super {p0, p1}, Landroid/view/View;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2112354
    iget v0, p0, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->b:I

    int-to-float v1, v0

    invoke-virtual {p0}, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->getWidth()I

    move-result v0

    iget v3, p0, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->b:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget-object v5, p0, Lcom/facebook/search/results/rows/sections/livefeed/ui/LiveFeedDividerView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2112355
    return-void
.end method
