.class public Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Cxi;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/CxP;",
        ":",
        "LX/CxV;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/CzL",
        "<",
        "Lcom/facebook/search/results/protocol/SearchResultsBlendedPhotoPublicModuleInterfaces$SearchResultsBlendedPhotoPublicModule;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector",
            "<",
            "Lcom/facebook/search/results/protocol/SearchResultsBlendedPhotoPublicModuleInterfaces$SearchResultsBlendedPhotoPublicModule;",
            "TE;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2114607
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2114608
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    .line 2114609
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;->b:Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;

    .line 2114610
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;->c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    .line 2114611
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;
    .locals 6

    .prologue
    .line 2114612
    const-class v1, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;

    monitor-enter v1

    .line 2114613
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2114614
    sput-object v2, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2114615
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2114616
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2114617
    new-instance p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;)V

    .line 2114618
    move-object v0, p0

    .line 2114619
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2114620
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2114621
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2114622
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2114623
    check-cast p2, LX/CzL;

    .line 2114624
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsModuleTitleSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2114625
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;->b:Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicGridComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2114626
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/photos/SearchResultsBlendedPhotoPublicModulePartDefinition;->c:Lcom/facebook/search/results/rows/sections/seemore/SearchResultsSeeMoreQueryModulePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2114627
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2114628
    const/4 v0, 0x1

    return v0
.end method
