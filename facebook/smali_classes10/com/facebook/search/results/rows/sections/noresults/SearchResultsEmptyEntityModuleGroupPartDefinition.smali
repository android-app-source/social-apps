.class public Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;",
        ">;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

.field public b:Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition",
            "<",
            "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2113380
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2113381
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    .line 2113382
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition;

    .line 2113383
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;
    .locals 5

    .prologue
    .line 2113384
    const-class v1, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;

    monitor-enter v1

    .line 2113385
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2113386
    sput-object v2, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2113387
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2113388
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2113389
    new-instance p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition;)V

    .line 2113390
    move-object v0, p0

    .line 2113391
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2113392
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2113393
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2113394
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2113395
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2113396
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2113397
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;

    .line 2113398
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/header/SearchResultsTitlePartDefinition;

    new-instance v2, LX/EN0;

    .line 2113399
    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2113400
    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    .line 2113401
    iget-object p3, v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-object v0, p3

    .line 2113402
    invoke-direct {v2, v3, v4, v0}, LX/EN0;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)V

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113403
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/noresults/SearchResultsEmptyEntityModuleBodyPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2113404
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2113405
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2113406
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2113407
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;

    .line 2113408
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2113409
    if-eqz v0, :cond_0

    .line 2113410
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2113411
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;

    .line 2113412
    iget-object v1, v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-object v0, v1

    .line 2113413
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
