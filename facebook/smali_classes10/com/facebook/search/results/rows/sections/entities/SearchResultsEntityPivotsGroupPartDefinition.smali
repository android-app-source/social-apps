.class public Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;",
        ">;>;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;

.field private final b:Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

.field private final c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

.field private final d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;

.field private final e:Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2108559
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2108560
    iput-object p1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;

    .line 2108561
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    .line 2108562
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    .line 2108563
    iput-object p4, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;

    .line 2108564
    iput-object p5, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;

    .line 2108565
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;
    .locals 9

    .prologue
    .line 2108566
    const-class v1, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;

    monitor-enter v1

    .line 2108567
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2108568
    sput-object v2, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2108569
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2108570
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2108571
    new-instance v3, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;-><init>(Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;)V

    .line 2108572
    move-object v0, v3

    .line 2108573
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2108574
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2108575
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2108576
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2108577
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/Cxo;

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 2108578
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2108579
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v7

    .line 2108580
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2108581
    invoke-interface {p3, v0}, LX/CxG;->a(Ljava/lang/Object;)I

    move-result v0

    .line 2108582
    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 2108583
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->a:Lcom/facebook/search/results/rows/sections/common/SearchResultsDividerFigPartDefinition;

    invoke-virtual {p1, v0, v5}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2108584
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f08227d

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/Object;

    .line 2108585
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2108586
    check-cast v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v1}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->k()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v2

    .line 2108587
    iget-object v3, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2108588
    check-cast v3, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    .line 2108589
    iget-object v4, v3, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object v3, v4

    .line 2108590
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v3, v4}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->SECTION_HEADER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;-><init>(Ljava/lang/String;ILX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)V

    .line 2108591
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->b:Lcom/facebook/search/results/rows/sections/common/SearchResultsAnnotationPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2108592
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->c:Lcom/facebook/search/results/rows/sections/common/SearchResultsResultsEmptyHeaderPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2108593
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v2

    move v1, v6

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {v7, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;

    .line 2108594
    iget-object v3, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->d:Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotRowPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2108595
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2108596
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/entities/SearchResultsEntityPivotsGroupPartDefinition;->e:Lcom/facebook/search/results/rows/sections/common/SearchResultsPivotFooterPartDefinition;

    invoke-virtual {p1, v0, v5}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2108597
    return-object v5
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2108598
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2108599
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2108600
    check-cast v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
