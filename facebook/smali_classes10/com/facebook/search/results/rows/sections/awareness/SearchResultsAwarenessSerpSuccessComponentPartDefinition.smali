.class public Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/EJG;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static final d:LX/1X6;

.field private static g:LX/0Xm;


# instance fields
.field private final e:LX/EJF;

.field private final f:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2103702
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    sget-object v2, LX/1Ua;->a:LX/1Ua;

    sget-object v3, LX/1X9;->BOX:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    sput-object v0, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->d:LX/1X6;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/EJF;LX/1V0;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2103703
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 2103704
    iput-object p2, p0, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->e:LX/EJF;

    .line 2103705
    iput-object p3, p0, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->f:LX/1V0;

    .line 2103706
    return-void
.end method

.method private a(LX/1De;LX/EJG;LX/1Pn;)LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/EJG;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2103707
    iget-object v0, p0, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->e:LX/EJF;

    const/4 v1, 0x0

    .line 2103708
    new-instance v2, LX/EJE;

    invoke-direct {v2, v0}, LX/EJE;-><init>(LX/EJF;)V

    .line 2103709
    sget-object v3, LX/EJF;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EJD;

    .line 2103710
    if-nez v3, :cond_0

    .line 2103711
    new-instance v3, LX/EJD;

    invoke-direct {v3}, LX/EJD;-><init>()V

    .line 2103712
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/EJD;->a$redex0(LX/EJD;LX/1De;IILX/EJE;)V

    .line 2103713
    move-object v2, v3

    .line 2103714
    move-object v1, v2

    .line 2103715
    move-object v0, v1

    .line 2103716
    iget-object v1, p2, LX/EJG;->a:Ljava/lang/CharSequence;

    .line 2103717
    iget-object v2, v0, LX/EJD;->a:LX/EJE;

    iput-object v1, v2, LX/EJE;->a:Ljava/lang/CharSequence;

    .line 2103718
    iget-object v2, v0, LX/EJD;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2103719
    move-object v0, v0

    .line 2103720
    iget-object v1, p2, LX/EJG;->b:Ljava/lang/CharSequence;

    .line 2103721
    iget-object v2, v0, LX/EJD;->a:LX/EJE;

    iput-object v1, v2, LX/EJE;->b:Ljava/lang/CharSequence;

    .line 2103722
    iget-object v2, v0, LX/EJD;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2103723
    move-object v0, v0

    .line 2103724
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2103725
    iget-object v1, p0, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->f:LX/1V0;

    check-cast p3, LX/1Ps;

    sget-object v2, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->d:LX/1X6;

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;
    .locals 6

    .prologue
    .line 2103726
    const-class v1, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;

    monitor-enter v1

    .line 2103727
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2103728
    sput-object v2, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2103729
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2103730
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2103731
    new-instance p0, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/EJF;->a(LX/0QB;)LX/EJF;

    move-result-object v4

    check-cast v4, LX/EJF;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;-><init>(Landroid/content/Context;LX/EJF;LX/1V0;)V

    .line 2103732
    move-object v0, p0

    .line 2103733
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2103734
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2103735
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2103736
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 2103737
    check-cast p2, LX/EJG;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->a(LX/1De;LX/EJG;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 2103738
    check-cast p2, LX/EJG;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/search/results/rows/sections/awareness/SearchResultsAwarenessSerpSuccessComponentPartDefinition;->a(LX/1De;LX/EJG;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2103739
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 2103740
    const/4 v0, 0x0

    return-object v0
.end method
