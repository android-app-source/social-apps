.class public Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        "Ljava/lang/Void;",
        "Lcom/facebook/search/results/environment/SearchResultsFeedEnvironment;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/EIz;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/EIz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;",
            "LX/EIz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2102617
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 2102618
    iput-object p1, p0, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;->b:LX/0Ot;

    .line 2102619
    iput-object p2, p0, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;->a:LX/EIz;

    .line 2102620
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;
    .locals 5

    .prologue
    .line 2102621
    const-class v1, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;

    monitor-enter v1

    .line 2102622
    :try_start_0
    sget-object v0, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2102623
    sput-object v2, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2102624
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2102625
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2102626
    new-instance v4, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;

    const/16 v3, 0x6fa

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/EIz;->a(LX/0QB;)LX/EIz;

    move-result-object v3

    check-cast v3, LX/EIz;

    invoke-direct {v4, p0, v3}, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;-><init>(LX/0Ot;LX/EIz;)V

    .line 2102627
    move-object v0, v4

    .line 2102628
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2102629
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2102630
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2102631
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2102632
    check-cast p2, Lcom/facebook/graphql/model/FeedUnit;

    const/4 v2, 0x0

    .line 2102633
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;->a:LX/EIz;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 2102634
    iget-object p3, v0, LX/EIz;->a:Ljava/util/Map;

    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, LX/0Ot;

    move-object v0, p3

    .line 2102635
    if-eqz v0, :cond_0

    .line 2102636
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 2102637
    :goto_0
    return-object v2

    .line 2102638
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/rows/SearchResultsFeedRootPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2102639
    const/4 v0, 0x1

    return v0
.end method
