.class public Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz6;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field private final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955140
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955141
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;->a:Ljava/lang/String;

    .line 1955142
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955143
    invoke-static {p3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;->c:LX/0am;

    .line 1955144
    return-void
.end method


# virtual methods
.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955145
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->EMPTY_ENTITY:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955146
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;->c:LX/0am;

    return-object v0
.end method
