.class public Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz5;
.implements LX/Cz6;


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLNode;

.field private final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public d:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;LX/0am;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)V
    .locals 1
    .param p3    # Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchResultDecoration;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1955171
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955172
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1955173
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->b:LX/0am;

    .line 1955174
    invoke-static {p1}, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955175
    iput-object p3, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->d:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    .line 1955176
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955177
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955178
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1955179
    invoke-static {p2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->b:LX/0am;

    .line 1955180
    invoke-static {p1}, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955181
    iput-object p3, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->d:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    .line 1955182
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 2

    .prologue
    .line 1955183
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x1eaef984

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->NAVIGATIONAL_LINKS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->CENTRAL:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    goto :goto_0
.end method


# virtual methods
.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955184
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955185
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->b:LX/0am;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955186
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 1955187
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1955188
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1955189
    goto :goto_0
.end method
