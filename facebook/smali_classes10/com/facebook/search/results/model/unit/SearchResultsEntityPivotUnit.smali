.class public Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz6;
.implements Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:I

.field private final e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public final f:Ljava/lang/String;

.field public final g:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field private final h:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1955169
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_USER:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->USERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PAGES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->TOPIC_MEDIA:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->EVENTS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->GROUPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->ENTITY_APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;)V
    .locals 2

    .prologue
    .line 1955158
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955159
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hw()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hw()Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryTitle;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->b:Ljava/lang/String;

    .line 1955160
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->cu()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->d:I

    .line 1955161
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hv()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955162
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->f:Ljava/lang/String;

    .line 1955163
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->g:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1955164
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->hu()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->c:Ljava/lang/String;

    .line 1955165
    sget-object v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->a:LX/0P1;

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->a:LX/0P1;

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_1
    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->h:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955166
    return-void

    .line 1955167
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1955168
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_1
.end method


# virtual methods
.method public final k()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955152
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955157
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955156
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final n()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955170
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955154
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1955155
    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 1

    .prologue
    .line 1955153
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->h:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955151
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 1955150
    iget v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsEntityPivotUnit;->d:I

    return v0
.end method
