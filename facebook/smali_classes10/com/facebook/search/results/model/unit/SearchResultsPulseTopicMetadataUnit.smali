.class public Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;
.super Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;
.source ""

# interfaces
.implements LX/Cz5;
.implements LX/Cz6;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLNode;

.field private final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955277
    invoke-direct {p0}, Lcom/facebook/search/results/model/unit/SearchResultsEntityWithCoverPhotoFeedUnit;-><init>()V

    .line 1955278
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1955279
    invoke-static {p2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;->b:LX/0am;

    .line 1955280
    return-void
.end method


# virtual methods
.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1955281
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955282
    sget-object v0, LX/D0F;->R:LX/CzP;

    .line 1955283
    iget-object p0, v0, LX/CzP;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v0, p0

    .line 1955284
    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955285
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;->b:LX/0am;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955286
    const/4 v0, 0x0

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/model/GraphQLNode;
    .locals 1

    .prologue
    .line 1955287
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955288
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 1955289
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1955290
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1955291
    goto :goto_0
.end method

.method public final q()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955292
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jS()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
