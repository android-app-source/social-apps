.class public Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz6;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field private final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;ILX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)V
    .locals 1
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1955029
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955030
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->a:Ljava/lang/String;

    .line 1955031
    iput p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->b:I

    .line 1955032
    iput-object p3, p0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->c:LX/0Px;

    .line 1955033
    invoke-static {p5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->e:LX/0am;

    .line 1955034
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955035
    return-void
.end method


# virtual methods
.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955036
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->d:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955037
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsAnnotationUnit;->e:LX/0am;

    return-object v0
.end method
