.class public Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz5;
.implements LX/Cz6;
.implements Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
        "LX/Cz5;",
        "LX/Cz6;",
        "Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;"
    }
.end annotation


# instance fields
.field public final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<TT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/D0L;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final o:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<TT;",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchResultDecoration;",
            ">;"
        }
    .end annotation
.end field

.field public p:Z


# direct methods
.method private constructor <init>(LX/0am;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0am;LX/0P1;LX/0Px;LX/0am;LX/0am;LX/0am;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;LX/0am;LX/0am;LX/D0L;LX/0Px;LX/0P1;)V
    .locals 2
    .param p13    # LX/D0L;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<TT;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/D0L;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;",
            "LX/0P1",
            "<TT;",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchResultDecoration;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1955111
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955112
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->p:Z

    .line 1955113
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0am;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    .line 1955114
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955115
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0am;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    .line 1955116
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0P1;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    .line 1955117
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->e:LX/0Px;

    .line 1955118
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0am;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->f:LX/0am;

    .line 1955119
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0am;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->g:LX/0am;

    .line 1955120
    invoke-static {p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0am;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->h:LX/0am;

    .line 1955121
    iput-object p9, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955122
    iput-object p10, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->j:LX/0Px;

    .line 1955123
    iput-object p11, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->k:LX/0am;

    .line 1955124
    iput-object p12, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l:LX/0am;

    .line 1955125
    iput-object p13, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->n:LX/D0L;

    .line 1955126
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m:LX/0Px;

    .line 1955127
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->o:LX/0P1;

    .line 1955128
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;LX/0P1;LX/0Px;Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;Ljava/lang/Integer;Ljava/lang/String;LX/D0L;LX/0Px;LX/0P1;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/graphql/enums/GraphQLObjectType;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # LX/D0L;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Ljava/lang/String;",
            "LX/0P1",
            "<TT;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "LX/D0L;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;",
            "LX/0P1",
            "<TT;",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchResultDecoration;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1955093
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955094
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->p:Z

    .line 1955095
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    .line 1955096
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955097
    invoke-static {p3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    .line 1955098
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0P1;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    .line 1955099
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->e:LX/0Px;

    .line 1955100
    invoke-static {p6}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->f:LX/0am;

    .line 1955101
    invoke-static {p7}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->g:LX/0am;

    .line 1955102
    invoke-static {p8}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->h:LX/0am;

    .line 1955103
    iput-object p9, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955104
    iput-object p10, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->j:LX/0Px;

    .line 1955105
    invoke-static {p11}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->k:LX/0am;

    .line 1955106
    invoke-static {p12}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l:LX/0am;

    .line 1955107
    iput-object p13, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->n:LX/D0L;

    .line 1955108
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m:LX/0Px;

    .line 1955109
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->o:LX/0P1;

    .line 1955110
    return-void
.end method

.method private D()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 2

    .prologue
    .line 1955090
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x25d6af

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v1, :cond_0

    .line 1955091
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, 0x499e8e7

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1955092
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0P1;LX/0Px;LX/0am;)Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<TT;>;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/0P1",
            "<TT;",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1955089
    new-instance v1, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->f:LX/0am;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->g:LX/0am;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->h:LX/0am;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->j:LX/0Px;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->k:LX/0am;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->n:LX/D0L;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->s()LX/0Px;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->o:LX/0P1;

    move-object/from16 v16, v0

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v13, p4

    invoke-direct/range {v1 .. v16}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;-><init>(LX/0am;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0am;LX/0P1;LX/0Px;LX/0am;LX/0am;LX/0am;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;LX/0am;LX/0am;LX/D0L;LX/0Px;LX/0P1;)V

    return-object v1
.end method

.method public static a(Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;Ljava/lang/Object;Ljava/lang/Object;)Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<TT;>;TT;TT;)",
            "Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1955075
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->y()LX/0P1;

    move-result-object v2

    .line 1955076
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object/from16 v2, p0

    .line 1955077
    :goto_0
    return-object v2

    .line 1955078
    :cond_1
    new-instance v6, LX/0P2;

    invoke-direct {v6}, LX/0P2;-><init>()V

    .line 1955079
    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1955080
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1955081
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v6, v0, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    .line 1955082
    :cond_2
    invoke-virtual {v6, v2}, LX/0P2;->a(Ljava/util/Map$Entry;)LX/0P2;

    goto :goto_1

    .line 1955083
    :cond_3
    new-instance v17, LX/0P2;

    invoke-direct/range {v17 .. v17}, LX/0P2;-><init>()V

    .line 1955084
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->B()LX/0P1;

    move-result-object v2

    invoke-virtual {v2}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1955085
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1955086
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_2

    .line 1955087
    :cond_4
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, LX/0P2;->a(Ljava/util/Map$Entry;)LX/0P2;

    goto :goto_2

    .line 1955088
    :cond_5
    new-instance v2, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->e:LX/0Px;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->f:LX/0am;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->g:LX/0am;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->h:LX/0am;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->j:LX/0Px;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->k:LX/0am;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l:LX/0am;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->n:LX/D0L;

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->s()LX/0Px;

    move-result-object v16

    invoke-virtual/range {v17 .. v17}, LX/0P2;->b()LX/0P1;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;-><init>(LX/0am;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0am;LX/0P1;LX/0Px;LX/0am;LX/0am;LX/0am;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;LX/0am;LX/0am;LX/D0L;LX/0Px;LX/0P1;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final B()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<TT;",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchResultDecoration;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955074
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->o:LX/0P1;

    return-object v0
.end method

.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 1955073
    invoke-direct {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->D()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955070
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1955071
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1955072
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955069
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955068
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->h:LX/0am;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955067
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955129
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->l:LX/0am;

    return-object v0
.end method

.method public final n()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955054
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->g:LX/0am;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955055
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->j:LX/0Px;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955056
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->e:LX/0Px;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 2

    .prologue
    .line 1955057
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955058
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final s()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955059
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->m:LX/0Px;

    return-object v0
.end method

.method public final t()LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<+",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955060
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->x()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1955061
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchResultsCollectionUnit{mDisplayStyle="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRole="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->c:LX/0am;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCollection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEntityIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->e:LX/0Px;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCollectionGraphQLObjectType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->f:LX/0am;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLObjectType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955062
    invoke-direct {p0}, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->D()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 1955063
    if-nez v0, :cond_0

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final v()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955064
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->a:LX/0am;

    return-object v0
.end method

.method public final x()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1955065
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Py;->asList()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final y()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<TT;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955066
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCollectionUnit;->d:LX/0P1;

    return-object v0
.end method
