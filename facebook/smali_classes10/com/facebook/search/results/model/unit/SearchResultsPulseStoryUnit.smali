.class public Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz5;
.implements LX/Cz6;
.implements Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLStory;

.field private final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1955272
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955273
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1955274
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;->b:LX/0am;

    .line 1955275
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955267
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955268
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1955269
    invoke-static {p2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;->b:LX/0am;

    .line 1955270
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;
    .locals 2

    .prologue
    .line 1955271
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;->b:LX/0am;

    invoke-direct {v0, p1, v1}, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;-><init>(Lcom/facebook/graphql/model/GraphQLStory;LX/0am;)V

    return-object v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 1955276
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955259
    sget-object v0, LX/D0F;->P:LX/CzP;

    .line 1955260
    iget-object p0, v0, LX/CzP;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v0, p0

    .line 1955261
    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955262
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;->b:LX/0am;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955263
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 1955264
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1955265
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1955266
    goto :goto_0
.end method
