.class public Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz6;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/composer/minutiae/model/MinutiaeObject;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955130
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955131
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->a:Ljava/lang/String;

    .line 1955132
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->b:Ljava/lang/String;

    .line 1955133
    iput-object p3, p0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->c:Ljava/lang/String;

    .line 1955134
    invoke-static {p4}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->d:LX/0am;

    .line 1955135
    return-void
.end method


# virtual methods
.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955137
    sget-object v0, LX/D0F;->F:LX/CzP;

    .line 1955138
    iget-object p0, v0, LX/CzP;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v0, p0

    .line 1955139
    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955136
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;->d:LX/0am;

    return-object v0
.end method
