.class public Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz5;
.implements LX/Cz6;


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLNode;

.field private final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1955245
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955246
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1955247
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->b:LX/0am;

    .line 1955248
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955241
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955242
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1955243
    invoke-static {p2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->b:LX/0am;

    .line 1955244
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLSavedState;)Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955229
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fR()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1955230
    const/4 v0, 0x0

    .line 1955231
    :goto_0
    return-object v0

    .line 1955232
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fR()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1955233
    invoke-static {v0}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v0

    .line 1955234
    iput-object p1, v0, LX/4XR;->pI:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1955235
    move-object v0, v0

    .line 1955236
    invoke-virtual {v0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 1955237
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;

    iget-object v2, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-static {v2}, LX/4XR;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/4XR;

    move-result-object v2

    .line 1955238
    iput-object v1, v2, LX/4XR;->iQ:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1955239
    move-object v1, v2

    .line 1955240
    invoke-virtual {v1}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->b:LX/0am;

    invoke-direct {v0, v1, v2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;-><init>(Lcom/facebook/graphql/model/GraphQLNode;LX/0am;)V

    goto :goto_0
.end method

.method public final k()Lcom/facebook/graphql/model/GraphQLImage;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955228
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fc()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bc()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955225
    sget-object v0, LX/D0F;->K:LX/CzP;

    .line 1955226
    iget-object p0, v0, LX/CzP;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v0, p0

    .line 1955227
    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955224
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->b:LX/0am;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1955223
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->cV()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955215
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 1955216
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1955217
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1955218
    goto :goto_0
.end method

.method public final q()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 1955222
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->iI()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 1955221
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jo()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 1955220
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955219
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fR()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseContextUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fR()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
