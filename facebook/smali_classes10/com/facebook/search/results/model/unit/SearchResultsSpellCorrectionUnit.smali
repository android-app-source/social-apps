.class public Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:LX/CwB;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CwB;)V
    .locals 0

    .prologue
    .line 1955348
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955349
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    .line 1955350
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->b:Ljava/lang/String;

    .line 1955351
    iput-object p3, p0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->c:Ljava/lang/String;

    .line 1955352
    iput-object p4, p0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->d:Ljava/lang/String;

    .line 1955353
    iput-object p5, p0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->e:Ljava/lang/String;

    .line 1955354
    iput-object p6, p0, Lcom/facebook/search/results/model/unit/SearchResultsSpellCorrectionUnit;->f:LX/CwB;

    .line 1955355
    return-void
.end method
