.class public Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz5;
.implements LX/Cz6;
.implements Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;
.implements Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public final b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/8dH;

.field private final h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field private final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/model/GraphQLStory;LX/8dH;LX/0am;LX/0am;LX/0am;)V
    .locals 2
    .param p2    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/8dH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "LX/8dH;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1955386
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955387
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955388
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955389
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1955390
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->f:LX/0am;

    .line 1955391
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->d:LX/0am;

    .line 1955392
    invoke-static {p8}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->e:LX/0am;

    .line 1955393
    iput-object p5, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->g:LX/8dH;

    .line 1955394
    invoke-static {p5}, LX/D0M;->b(LX/8dH;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->h:LX/0am;

    .line 1955395
    invoke-static {p5}, LX/D0M;->a(LX/8dH;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->i:LX/0am;

    .line 1955396
    iput-object p3, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955397
    if-eqz p5, :cond_0

    invoke-interface {p5}, LX/8dH;->fn_()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_0
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->m:LX/0am;

    .line 1955398
    invoke-static {p5}, LX/D0M;->e(LX/8dH;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->k:LX/0Px;

    .line 1955399
    invoke-static {p5}, LX/D0M;->d(LX/8dH;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->l:LX/0Px;

    .line 1955400
    return-void

    .line 1955401
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/model/GraphQLStory;LX/8dH;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/8dH;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955370
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955371
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955372
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955373
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1955374
    invoke-static {p6}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->f:LX/0am;

    .line 1955375
    invoke-static {p7}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->d:LX/0am;

    .line 1955376
    invoke-static {p8}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->e:LX/0am;

    .line 1955377
    iput-object p5, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->g:LX/8dH;

    .line 1955378
    invoke-static {p5}, LX/D0M;->b(LX/8dH;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->h:LX/0am;

    .line 1955379
    invoke-static {p5}, LX/D0M;->a(LX/8dH;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->i:LX/0am;

    .line 1955380
    iput-object p3, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955381
    if-eqz p5, :cond_0

    invoke-interface {p5}, LX/8dH;->fn_()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_0
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->m:LX/0am;

    .line 1955382
    invoke-static {p5}, LX/D0M;->e(LX/8dH;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->k:LX/0Px;

    .line 1955383
    invoke-static {p5}, LX/D0M;->d(LX/8dH;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->l:LX/0Px;

    .line 1955384
    return-void

    .line 1955385
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final D_()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 1955369
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D_()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;
    .locals 9

    .prologue
    .line 1955368
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v2, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iget-object v3, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    iget-object v5, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->g:LX/8dH;

    iget-object v6, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->f:LX/0am;

    iget-object v7, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->d:LX/0am;

    iget-object v8, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->e:LX/0am;

    move-object v4, p1

    invoke-direct/range {v0 .. v8}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/model/GraphQLStory;LX/8dH;LX/0am;LX/0am;LX/0am;)V

    return-object v0
.end method

.method public final j()Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 1955367
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->c:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method public final k()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955366
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->h:LX/0am;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955402
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955365
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->f:LX/0am;

    return-object v0
.end method

.method public final n()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955364
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->i:LX/0am;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955363
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->l:LX/0Px;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955359
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 1955360
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1955361
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1955362
    goto :goto_0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 1

    .prologue
    .line 1955358
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->m:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955357
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955356
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method
