.class public Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz6;
.implements Lcom/facebook/search/results/model/contract/SearchResultsSeeMoreFeedUnit;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field private final b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public final c:Ljava/lang/String;

.field private final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field private final j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1955336
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955337
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955338
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->c:Ljava/lang/String;

    .line 1955339
    invoke-static {p3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->g:LX/0am;

    .line 1955340
    invoke-static {p4}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->d:LX/0am;

    .line 1955341
    invoke-static {p5}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->e:LX/0am;

    .line 1955342
    iput-object p6, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->f:LX/0Px;

    .line 1955343
    iput-object p7, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->i:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955344
    iput-object p8, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1955345
    iput-object p9, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1955346
    iput-object p10, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->h:LX/0Px;

    .line 1955347
    return-void
.end method


# virtual methods
.method public final k()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955335
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->d:LX/0am;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955334
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955333
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->g:LX/0am;

    return-object v0
.end method

.method public final n()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955332
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->e:LX/0am;

    return-object v0
.end method

.method public final o()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsInterfaces$SearchResultPageMainFilterFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955331
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->f:LX/0Px;

    return-object v0
.end method

.method public final q()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 1

    .prologue
    .line 1955330
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->j:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    return-object v0
.end method

.method public final r()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955328
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->b:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final s()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleInterfaces$SearchResultsSerpTabsModule$Edges;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955329
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;->h:LX/0Px;

    return-object v0
.end method
