.class public Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz6;


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:I

.field private final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955249
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955250
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    .line 1955251
    invoke-static {p2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->c:LX/0am;

    .line 1955252
    iput p3, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->b:I

    .line 1955253
    return-void
.end method


# virtual methods
.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955254
    sget-object v0, LX/D0F;->O:LX/CzP;

    .line 1955255
    iget-object p0, v0, LX/CzP;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-object v0, p0

    .line 1955256
    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955257
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->c:LX/0am;

    return-object v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1955258
    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;->a()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/search/results/model/unit/SearchResultsPulseSentimentUnit;->a:Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysis;->a()Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEmotionalAnalysisItemsConnection;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
