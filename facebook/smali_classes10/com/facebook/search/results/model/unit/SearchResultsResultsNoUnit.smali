.class public Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 1955295
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955296
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;->a:Ljava/lang/String;

    .line 1955297
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;->b:Ljava/lang/String;

    .line 1955298
    iput-boolean p3, p0, Lcom/facebook/search/results/model/unit/SearchResultsResultsNoUnit;->c:Z

    .line 1955299
    return-void
.end method
