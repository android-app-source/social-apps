.class public Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz5;
.implements LX/Cz6;


# instance fields
.field public final a:Lcom/facebook/graphql/model/GraphQLNode;

.field private final b:Ljava/lang/String;

.field private final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1955042
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1955043
    iput-object p1, p0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1955044
    iput-object p2, p0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->b:Ljava/lang/String;

    .line 1955045
    invoke-static {p3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->c:LX/0am;

    .line 1955046
    return-void
.end method


# virtual methods
.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1955047
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->WIKIPEDIA_CARD:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955048
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->c:LX/0am;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1955049
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public final p()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1955050
    iget-object v0, p0, Lcom/facebook/search/results/model/unit/SearchResultsCentralWikiUnit;->a:Lcom/facebook/graphql/model/GraphQLNode;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 1955051
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1955052
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1955053
    goto :goto_0
.end method
