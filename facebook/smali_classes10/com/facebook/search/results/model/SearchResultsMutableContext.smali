.class public Lcom/facebook/search/results/model/SearchResultsMutableContext;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/CwB;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsMutableContext;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/7BH;

.field public b:LX/8ci;

.field public c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

.field public d:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

.field public f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/Boolean;

.field private l:LX/CwF;

.field private m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcom/facebook/search/model/ReactionSearchData;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:LX/103;

.field public s:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/lang/String;

.field public x:LX/8ef;

.field public y:Z

.field private z:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1954567
    new-instance v0, LX/CzH;

    invoke-direct {v0}, LX/CzH;-><init>()V

    sput-object v0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1954512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954513
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a:LX/7BH;

    .line 1954514
    sget-object v0, LX/8ci;->K:LX/8ci;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    .line 1954515
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1954516
    sget-object v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 1954517
    invoke-static {}, LX/7CN;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    .line 1954518
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->k:Ljava/lang/Boolean;

    .line 1954519
    sget-object v0, LX/CwF;->keyword:LX/CwF;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->l:LX/CwF;

    .line 1954520
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1954521
    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    .line 1954522
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1954523
    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->n:LX/0P1;

    .line 1954524
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1954568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1954569
    sget-object v0, LX/7BH;->LIGHT:LX/7BH;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a:LX/7BH;

    .line 1954570
    sget-object v0, LX/8ci;->K:LX/8ci;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    .line 1954571
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1954572
    sget-object v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->a:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 1954573
    invoke-static {}, LX/7CN;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    .line 1954574
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->k:Ljava/lang/Boolean;

    .line 1954575
    sget-object v0, LX/CwF;->keyword:LX/CwF;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->l:LX/CwF;

    .line 1954576
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1954577
    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    .line 1954578
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1954579
    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->n:LX/0P1;

    .line 1954580
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/7BH;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a:LX/7BH;

    .line 1954581
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/8ci;->a(Ljava/lang/String;)LX/8ci;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    .line 1954582
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 1954583
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1954584
    const-class v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 1954585
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    .line 1954586
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->g:Ljava/lang/String;

    .line 1954587
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h:Ljava/lang/String;

    .line 1954588
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->i:Ljava/lang/String;

    .line 1954589
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->j:Ljava/lang/String;

    .line 1954590
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->k:Ljava/lang/Boolean;

    .line 1954591
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CwF;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->l:LX/CwF;

    .line 1954592
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1954593
    const-class v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1954594
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1954595
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1954596
    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1954597
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    .line 1954598
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1954599
    const-class v1, Landroid/os/Parcelable;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1954600
    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->n:LX/0P1;

    .line 1954601
    const-class v0, Lcom/facebook/search/model/ReactionSearchData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/ReactionSearchData;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o:Lcom/facebook/search/model/ReactionSearchData;

    .line 1954602
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->p:Ljava/lang/String;

    .line 1954603
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->q:Ljava/lang/String;

    .line 1954604
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1954605
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/103;->valueOf(Ljava/lang/String;)LX/103;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r:LX/103;

    .line 1954606
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    .line 1954607
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    .line 1954608
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->w:Ljava/lang/String;

    .line 1954609
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1954610
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1954611
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1954612
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    .line 1954613
    if-nez v2, :cond_1

    if-nez v3, :cond_1

    if-nez v4, :cond_1

    if-eqz v5, :cond_2

    .line 1954614
    :cond_1
    new-instance v0, LX/CzI;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/CzI;-><init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;)V

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    .line 1954615
    :cond_2
    return-void

    .line 1954616
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final A()LX/8ef;
    .locals 1

    .prologue
    .line 1954617
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    if-eqz v0, :cond_0

    .line 1954618
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    .line 1954619
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/CzJ;

    invoke-direct {v0, p0}, LX/CzJ;-><init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954620
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/0Uh;LX/0ad;Landroid/os/Bundle;)V
    .locals 2
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1954621
    sget v1, LX/2SU;->S:I

    invoke-virtual {p1, v1, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    sget-short v1, LX/100;->aL:S

    invoke-interface {p2, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->y:Z

    .line 1954622
    iget-boolean v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->y:Z

    if-nez v0, :cond_2

    .line 1954623
    :goto_0
    return-void

    .line 1954624
    :cond_2
    if-eqz p3, :cond_3

    const-string v0, "browse_session_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1954625
    const-string v0, "browse_session_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    .line 1954626
    :cond_3
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V
    .locals 2

    .prologue
    .line 1954627
    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->g:Ljava/lang/String;

    .line 1954628
    invoke-interface {p1}, LX/CwB;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->j:Ljava/lang/String;

    .line 1954629
    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b(Ljava/lang/String;)V

    .line 1954630
    invoke-interface {p1}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->i:Ljava/lang/String;

    .line 1954631
    invoke-interface {p1}, LX/CwB;->e()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->k:Ljava/lang/Boolean;

    .line 1954632
    invoke-interface {p1}, LX/CwB;->jB_()LX/CwF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->l:LX/CwF;

    .line 1954633
    invoke-interface {p1}, LX/CwB;->jC_()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    .line 1954634
    iput-object p2, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    .line 1954635
    iput-object p3, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    .line 1954636
    invoke-interface {p1}, LX/CwB;->n()Lcom/facebook/search/model/ReactionSearchData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o:Lcom/facebook/search/model/ReactionSearchData;

    .line 1954637
    invoke-interface {p1}, LX/CwB;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->p:Ljava/lang/String;

    .line 1954638
    invoke-interface {p1}, LX/CwB;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->q:Ljava/lang/String;

    .line 1954639
    invoke-interface {p1}, LX/CwB;->k()LX/103;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r:LX/103;

    .line 1954640
    invoke-interface {p1}, LX/CwB;->h()LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->n:LX/0P1;

    .line 1954641
    invoke-interface {p1}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    .line 1954642
    invoke-interface {p1}, LX/CwB;->h()LX/0P1;

    move-result-object v0

    sget-object v1, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {v1}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, LX/CwB;->h()LX/0P1;

    move-result-object v0

    sget-object v1, LX/7B4;->SCOPED_TAB:LX/7B4;

    invoke-virtual {v1}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;

    .line 1954643
    iget-boolean v1, v0, Lcom/facebook/search/api/GraphSearchQueryTabModifier;->a:Z

    move v0, v1

    .line 1954644
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->z:Z

    .line 1954645
    instance-of v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v0, :cond_0

    .line 1954646
    check-cast p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 1954647
    iget-object v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    move-object v0, v0

    .line 1954648
    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->s:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 1954649
    :cond_0
    return-void

    .line 1954650
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954651
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r:LX/103;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->j:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->z:Z

    if-nez v0, :cond_0

    .line 1954652
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r:LX/103;

    iget-object v1, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->n:LX/0P1;

    invoke-static {v0, v1, v2, v3}, LX/7BG;->a(LX/103;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Ljava/lang/String;

    move-result-object v0

    .line 1954653
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1954654
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1954655
    invoke-static {}, LX/7CN;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    .line 1954656
    :cond_0
    iput-object p1, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h:Ljava/lang/String;

    .line 1954657
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1954675
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1954658
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "You already set the metadata. Can\'t use deprecated API in combination with new one."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1954659
    iput-object p1, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    .line 1954660
    return-void

    .line 1954661
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1954662
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "You already set the metadata. Can\'t use deprecated API in combination with new one."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1954663
    iget-boolean v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->y:Z

    if-eqz v0, :cond_1

    .line 1954664
    :goto_1
    return-void

    .line 1954665
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1954666
    :cond_1
    iput-object p1, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    goto :goto_1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1954667
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1954668
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->k:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1954669
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "You already set the metadata. Can\'t use deprecated API in combination with new one."

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1954670
    iput-object p1, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->w:Ljava/lang/String;

    .line 1954671
    return-void

    .line 1954672
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954673
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->n:LX/0P1;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1954566
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1954674
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final jA_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1954504
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final jB_()LX/CwF;
    .locals 1

    .prologue
    .line 1954505
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->l:LX/CwF;

    return-object v0
.end method

.method public final jC_()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954506
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    return-object v0
.end method

.method public final k()LX/103;
    .locals 1

    .prologue
    .line 1954507
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r:LX/103;

    return-object v0
.end method

.method public final m()LX/7BH;
    .locals 1

    .prologue
    .line 1954508
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a:LX/7BH;

    return-object v0
.end method

.method public final n()Lcom/facebook/search/model/ReactionSearchData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954509
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o:Lcom/facebook/search/model/ReactionSearchData;

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 2

    .prologue
    .line 1954510
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()LX/8ci;
    .locals 1

    .prologue
    .line 1954511
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    return-object v0
.end method

.method public final r()Lcom/facebook/search/logging/api/SearchTypeaheadSession;
    .locals 1

    .prologue
    .line 1954503
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954525
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1954526
    const-string v0, "%s: \nSource: %s\nFilter Type: %s\nQuery Title: %s\n Query Function: %s\n"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1954527
    iget-boolean v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final w()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954528
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1954529
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a:LX/7BH;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1954530
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    invoke-virtual {v0}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954531
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1954532
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1954533
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1954534
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954535
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954536
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954537
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954538
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954539
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->k:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1954540
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->l:LX/CwF;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1954541
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1954542
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 1954543
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1954544
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move-object v0, v1

    .line 1954545
    goto :goto_0

    .line 1954546
    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1954547
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->n:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1954548
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->o:Lcom/facebook/search/model/ReactionSearchData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1954549
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954550
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954551
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r:LX/103;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->r:LX/103;

    invoke-virtual {v0}, LX/103;->name()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954552
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954553
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954554
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954555
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    invoke-interface {v0}, LX/8ef;->k()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954556
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    invoke-interface {v0}, LX/8ef;->c()Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954557
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    invoke-interface {v0}, LX/8ef;->m()Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1954558
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    invoke-interface {v0}, LX/8ef;->l()Lcom/facebook/graphql/enums/GraphQLGraphSearchSpellerConfidence;

    move-result-object v1

    :cond_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1954559
    return-void

    :cond_3
    move-object v0, v1

    .line 1954560
    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 1954561
    goto :goto_3

    :cond_5
    move-object v0, v1

    .line 1954562
    goto :goto_4

    :cond_6
    move-object v0, v1

    .line 1954563
    goto :goto_5
.end method

.method public final y()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1

    .prologue
    .line 1954564
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final z()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954565
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsMutableContext;->m:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
