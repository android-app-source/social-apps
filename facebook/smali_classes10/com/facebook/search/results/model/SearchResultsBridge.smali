.class public Lcom/facebook/search/results/model/SearchResultsBridge;
.super Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.source ""

# interfaces
.implements LX/Cz5;
.implements LX/Cz6;


# instance fields
.field private final a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/Cz3;


# direct methods
.method private constructor <init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1954146
    invoke-direct {p0}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;-><init>()V

    .line 1954147
    new-instance v0, LX/Cz3;

    invoke-direct {v0, p1}, LX/Cz3;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    .line 1954148
    invoke-static {p2}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->a:LX/0am;

    .line 1954149
    return-void
.end method

.method public static a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Ljava/lang/String;)Lcom/facebook/search/results/model/SearchResultsBridge;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1954170
    new-instance v0, Lcom/facebook/search/results/model/SearchResultsBridge;

    invoke-direct {v0, p0, p1}, Lcom/facebook/search/results/model/SearchResultsBridge;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1954169
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/Cz3;
    .locals 1

    .prologue
    .line 1954168
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    return-object v0
.end method

.method public final l()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;
    .locals 1

    .prologue
    .line 1954167
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->c()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    return-object v0
.end method

.method public final m()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954166
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->a:LX/0am;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954163
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->p()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1954164
    :cond_0
    const/4 v0, 0x0

    .line 1954165
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->p()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1954160
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1954161
    const/4 v0, 0x0

    .line 1954162
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-static {v0}, LX/8eM;->d(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1954150
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1954151
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1954152
    :goto_0
    return-object v0

    .line 1954153
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/results/model/SearchResultsBridge;->b:LX/Cz3;

    invoke-virtual {v0}, LX/Cz3;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v2

    .line 1954154
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1954155
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 1954156
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1954157
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1954158
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1954159
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
