.class public final Lcom/facebook/search/results/loader/SearchResultsLoaderController$NetworkRequestTimer$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Cyn;

.field public final synthetic b:LX/Cyd;

.field public final synthetic c:LX/Cyn;

.field public final synthetic d:LX/Cyl;


# direct methods
.method public constructor <init>(LX/Cyl;LX/Cyn;LX/Cyd;LX/Cyn;)V
    .locals 0

    .prologue
    .line 1953564
    iput-object p1, p0, Lcom/facebook/search/results/loader/SearchResultsLoaderController$NetworkRequestTimer$1;->d:LX/Cyl;

    iput-object p2, p0, Lcom/facebook/search/results/loader/SearchResultsLoaderController$NetworkRequestTimer$1;->a:LX/Cyn;

    iput-object p3, p0, Lcom/facebook/search/results/loader/SearchResultsLoaderController$NetworkRequestTimer$1;->b:LX/Cyd;

    iput-object p4, p0, Lcom/facebook/search/results/loader/SearchResultsLoaderController$NetworkRequestTimer$1;->c:LX/Cyn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1953565
    iget-object v0, p0, Lcom/facebook/search/results/loader/SearchResultsLoaderController$NetworkRequestTimer$1;->d:LX/Cyl;

    iget-object v0, v0, LX/Cyl;->a:LX/Cyn;

    iget-object v1, p0, Lcom/facebook/search/results/loader/SearchResultsLoaderController$NetworkRequestTimer$1;->b:LX/Cyd;

    invoke-virtual {v0, v1}, LX/Cyn;->a(LX/Cyd;)V

    .line 1953566
    iget-object v0, p0, Lcom/facebook/search/results/loader/SearchResultsLoaderController$NetworkRequestTimer$1;->c:LX/Cyn;

    iget-object v0, v0, LX/Cyn;->s:LX/Fdy;

    .line 1953567
    if-eqz v0, :cond_0

    .line 1953568
    iget-object v1, p0, Lcom/facebook/search/results/loader/SearchResultsLoaderController$NetworkRequestTimer$1;->d:LX/Cyl;

    iget-object v1, v1, LX/Cyl;->a:LX/Cyn;

    iget-object v1, v1, LX/Cyn;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1953569
    const/16 v1, 0x19

    .line 1953570
    iget-object v2, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-static {v2}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->C(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    .line 1953571
    iget-object v2, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;->C:LX/Cve;

    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 1953572
    iget-object v4, v3, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v3, v4

    .line 1953573
    invoke-virtual {v2, v3, v1}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;I)V

    .line 1953574
    iget-object v2, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    sget-object v3, LX/EQG;->REQUEST_TIMED_OUT:LX/EQG;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a$redex0(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/EQG;Z)V

    .line 1953575
    iget-object v2, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;->q:LX/CvY;

    iget-object v3, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 1953576
    iget-object v4, v3, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v3, v4

    .line 1953577
    iget-object v4, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v4, v4, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    .line 1953578
    iget v5, v4, LX/CzA;->c:I

    move v4, v5

    .line 1953579
    iget-object v5, v0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget v5, v5, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p0, "Timeout: "

    invoke-direct {v6, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IILjava/lang/String;)V

    .line 1953580
    :cond_0
    return-void
.end method
