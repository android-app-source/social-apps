.class public final Lcom/facebook/search/results/environment/entity/OldCanApplyEntityInlineActionImpl$2$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Cy5;


# direct methods
.method public constructor <init>(LX/Cy5;)V
    .locals 0

    .prologue
    .line 1952384
    iput-object p1, p0, Lcom/facebook/search/results/environment/entity/OldCanApplyEntityInlineActionImpl$2$1;->a:LX/Cy5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1952385
    iget-object v0, p0, Lcom/facebook/search/results/environment/entity/OldCanApplyEntityInlineActionImpl$2$1;->a:LX/Cy5;

    iget-object v0, v0, LX/Cy5;->c:LX/Cy9;

    iget-object v1, p0, Lcom/facebook/search/results/environment/entity/OldCanApplyEntityInlineActionImpl$2$1;->a:LX/Cy5;

    iget-object v1, v1, LX/Cy5;->a:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1952386
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    .line 1952387
    :goto_0
    sparse-switch v2, :sswitch_data_0

    .line 1952388
    :goto_1
    return-void

    .line 1952389
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1952390
    :sswitch_0
    iget-object v2, v0, LX/Cy9;->r:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Bf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dx()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/7Bf;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    goto :goto_1

    .line 1952391
    :sswitch_1
    iget-object v2, v0, LX/Cy9;->r:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Bf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->cc()Z

    move-result p0

    invoke-virtual {v2, v3, p0}, LX/7Bf;->a(Ljava/lang/String;Z)V

    goto :goto_1

    .line 1952392
    :sswitch_2
    iget-object v2, v0, LX/Cy9;->r:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7Bf;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->kO()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/7Bf;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x41e065f -> :sswitch_2
    .end sparse-switch
.end method
