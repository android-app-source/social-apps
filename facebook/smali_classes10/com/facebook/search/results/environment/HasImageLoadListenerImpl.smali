.class public Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pp;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6VU;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CvM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1951672
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1951673
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1951674
    iput-object v0, p0, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a:LX/0Ot;

    .line 1951675
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;
    .locals 3

    .prologue
    .line 1951676
    new-instance v1, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;

    invoke-direct {v1}, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;-><init>()V

    .line 1951677
    const/16 v0, 0x1cb8

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/CvM;->a(LX/0QB;)LX/CvM;

    move-result-object v0

    check-cast v0, LX/CvM;

    .line 1951678
    iput-object v2, v1, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a:LX/0Ot;

    iput-object v0, v1, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->b:LX/CvM;

    .line 1951679
    move-object v0, v1

    .line 1951680
    return-object v0
.end method


# virtual methods
.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 6

    .prologue
    .line 1951681
    if-eqz p3, :cond_0

    .line 1951682
    iget-object v0, p3, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 1951683
    if-nez v0, :cond_1

    .line 1951684
    :cond_0
    :goto_0
    return-void

    .line 1951685
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Lcom/facebook/common/callercontext/CallerContext;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1951686
    iget-object v1, p3, LX/1bf;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 1951687
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1951688
    iget-object v0, p0, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VU;

    invoke-virtual {v0, p1, v1, p3}, LX/1BL;->a(LX/1aZ;Ljava/lang/String;LX/1bf;)V

    .line 1951689
    iget-object v0, p0, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->b:LX/CvM;

    .line 1951690
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v0, LX/CvM;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1951691
    :cond_2
    :goto_1
    goto :goto_0

    .line 1951692
    :cond_3
    iget-object v2, v0, LX/CvM;->c:Ljava/util/Map;

    iget-object v3, v0, LX/CvM;->e:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1951693
    iget-object v2, v0, LX/CvM;->c:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1951694
    if-nez p1, :cond_0

    .line 1951695
    :goto_0
    return-void

    .line 1951696
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/results/environment/HasImageLoadListenerImpl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6VU;

    invoke-virtual {v0, p1}, LX/1BL;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
