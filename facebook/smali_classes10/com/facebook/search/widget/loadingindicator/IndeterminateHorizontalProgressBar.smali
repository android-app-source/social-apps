.class public Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;
.super Landroid/widget/ProgressBar;
.source ""


# instance fields
.field private a:Landroid/animation/Animator;

.field private b:I

.field private c:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field

.field private d:I
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2118422
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2118423
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2118420
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2118421
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2118406
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2118407
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    .line 2118408
    sget-object v0, LX/03r;->IndeterminateHorizontalProgressBar:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2118409
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c:I

    .line 2118410
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->d:I

    .line 2118411
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10e0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 2118412
    const/16 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->b:I

    .line 2118413
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2118414
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x108006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2118415
    invoke-direct {p0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c()V

    .line 2118416
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->setMax(I)V

    .line 2118417
    invoke-super {p0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 2118418
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->setIndeterminate(Z)V

    .line 2118419
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/view/animation/Interpolator;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 2118402
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {p0, p1, v0}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 2118403
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 2118404
    return-object v0

    .line 2118405
    :array_0
    .array-data 4
        0x0
        0x64
    .end array-data
.end method

.method private static b(I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p0    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2118400
    invoke-static {p0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c(I)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v0

    .line 2118401
    new-instance v1, Landroid/graphics/drawable/ClipDrawable;

    const v2, 0x800003

    const/4 v3, 0x1

    invoke-direct {v1, v0, v2, v3}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    return-object v1
.end method

.method private static c(I)Landroid/graphics/drawable/ShapeDrawable;
    .locals 2
    .param p0    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2118396
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    .line 2118397
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    .line 2118398
    invoke-virtual {v1, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 2118399
    return-object v0
.end method

.method private c()V
    .locals 8

    .prologue
    const v7, 0x102000d

    const/high16 v6, 0x1020000

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2118424
    invoke-virtual {p0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 2118425
    if-eqz v0, :cond_0

    .line 2118426
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-gt v1, v2, :cond_1

    .line 2118427
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    .line 2118428
    iget v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c:I

    invoke-static {v1}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c(I)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v1

    aput-object v1, v0, v3

    .line 2118429
    iget v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->d:I

    invoke-static {v1}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v0, v4

    .line 2118430
    iget v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c:I

    invoke-static {v1}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v0, v5

    .line 2118431
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 2118432
    invoke-virtual {v1, v3, v6}, Landroid/graphics/drawable/LayerDrawable;->setId(II)V

    .line 2118433
    const v0, 0x102000f

    invoke-virtual {v1, v4, v0}, Landroid/graphics/drawable/LayerDrawable;->setId(II)V

    .line 2118434
    invoke-virtual {v1, v5, v7}, Landroid/graphics/drawable/LayerDrawable;->setId(II)V

    .line 2118435
    invoke-virtual {p0, v1}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2118436
    :cond_0
    :goto_0
    return-void

    .line 2118437
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 2118438
    iget v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c:I

    invoke-static {v1}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c(I)Landroid/graphics/drawable/ShapeDrawable;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 2118439
    iget v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c:I

    invoke-static {v1}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 2118440
    const v1, 0x102000f

    iget v2, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->d:I

    invoke-static {v2}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 2118395
    iget-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Landroid/animation/Animator;
    .locals 5

    .prologue
    .line 2118389
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 2118390
    const-string v1, "secondaryProgress"

    new-instance v2, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v2}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-direct {p0, v1, v2}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a(Ljava/lang/String;Landroid/view/animation/Interpolator;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 2118391
    const-string v2, "progress"

    new-instance v3, Landroid/view/animation/AnticipateOvershootInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AnticipateOvershootInterpolator;-><init>()V

    invoke-direct {p0, v2, v3}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a(Ljava/lang/String;Landroid/view/animation/Interpolator;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 2118392
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v2, v3, v1

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 2118393
    iget v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->b:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 2118394
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2118385
    invoke-direct {p0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2118386
    iget-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    new-instance v1, LX/EQD;

    invoke-direct {v1, p0}, LX/EQD;-><init>(Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2118387
    iget-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 2118388
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 0
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorInt;
        .end annotation
    .end param

    .prologue
    .line 2118382
    iput p1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c:I

    .line 2118383
    invoke-direct {p0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->c()V

    .line 2118384
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2118378
    iget-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 2118379
    iget-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->removeAllListeners()V

    .line 2118380
    iget-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 2118381
    :cond_0
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4f266726

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2118374
    invoke-virtual {p0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->b()V

    .line 2118375
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    .line 2118376
    invoke-super {p0}, Landroid/widget/ProgressBar;->onDetachedFromWindow()V

    .line 2118377
    const/16 v1, 0x2d

    const v2, -0x73798999

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public declared-synchronized setIndeterminate(Z)V
    .locals 4

    .prologue
    .line 2118367
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2118368
    :goto_0
    monitor-exit p0

    return-void

    .line 2118369
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->e()Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    .line 2118370
    iget-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    iget v1, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->b:I

    div-int/lit8 v1, v1, 0x64

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 2118371
    iget-object v0, p0, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a:Landroid/animation/Animator;

    invoke-virtual {v0, p0}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 2118372
    invoke-virtual {p0}, Lcom/facebook/search/widget/loadingindicator/IndeterminateHorizontalProgressBar;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2118373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
