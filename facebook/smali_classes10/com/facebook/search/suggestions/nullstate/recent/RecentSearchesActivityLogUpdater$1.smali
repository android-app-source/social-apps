.class public final Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesActivityLogUpdater$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;

.field public final synthetic b:LX/EQ7;


# direct methods
.method public constructor <init>(LX/EQ7;Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;)V
    .locals 0

    .prologue
    .line 2118251
    iput-object p1, p0, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesActivityLogUpdater$1;->b:LX/EQ7;

    iput-object p2, p0, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesActivityLogUpdater$1;->a:Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 2118252
    :try_start_0
    iget-object v0, p0, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesActivityLogUpdater$1;->b:LX/EQ7;

    iget-object v0, v0, LX/EQ7;->c:LX/18V;

    iget-object v1, p0, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesActivityLogUpdater$1;->b:LX/EQ7;

    iget-object v1, v1, LX/EQ7;->a:LX/A0P;

    iget-object v2, p0, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesActivityLogUpdater$1;->a:Lcom/facebook/search/protocol/LogSelectedSuggestionToActivityLogParams;

    invoke-virtual {v0, v1, v2}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2118253
    :goto_0
    return-void

    .line 2118254
    :catch_0
    move-exception v0

    .line 2118255
    iget-object v1, p0, Lcom/facebook/search/suggestions/nullstate/recent/RecentSearchesActivityLogUpdater$1;->b:LX/EQ7;

    iget-object v1, v1, LX/EQ7;->d:LX/03V;

    sget-object v2, LX/3Ql;->FAILED_TO_UPDATE_ACTIVITY_LOG:LX/3Ql;

    invoke-virtual {v2}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to update activity log"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
