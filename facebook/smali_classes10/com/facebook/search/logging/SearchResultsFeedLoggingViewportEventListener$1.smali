.class public final Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0g8;

.field public final synthetic b:Ljava/lang/Object;

.field public final synthetic c:I

.field public final synthetic d:LX/CvQ;


# direct methods
.method public constructor <init>(LX/CvQ;LX/0g8;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 1948189
    iput-object p1, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->d:LX/CvQ;

    iput-object p2, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->a:LX/0g8;

    iput-object p3, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->b:Ljava/lang/Object;

    iput p4, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 1948190
    iget-object v0, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->d:LX/CvQ;

    iget-boolean v0, v0, LX/CvQ;->i:Z

    if-nez v0, :cond_1

    .line 1948191
    :cond_0
    :goto_0
    return-void

    .line 1948192
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->a:LX/0g8;

    .line 1948193
    invoke-static {v0}, LX/1BX;->b(LX/0g8;)LX/1Qr;

    move-result-object v1

    .line 1948194
    if-eqz v1, :cond_2

    invoke-interface {v1}, LX/1Qr;->d()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, LX/0g8;->B()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1948195
    :cond_2
    const/4 v1, 0x0

    .line 1948196
    :cond_3
    move-object v0, v1

    .line 1948197
    if-eqz v0, :cond_0

    .line 1948198
    iget-object v1, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->b:Ljava/lang/Object;

    invoke-static {v1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 1948199
    if-eqz v1, :cond_0

    invoke-static {v1}, LX/CvQ;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->d:LX/CvQ;

    .line 1948200
    invoke-static {v2, v1}, LX/CvQ;->b(LX/CvQ;Lcom/facebook/graphql/model/FeedUnit;)LX/Cvl;

    move-result-object v5

    .line 1948201
    iget-object v6, v2, LX/CvQ;->a:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    .line 1948202
    iget-wide v13, v5, LX/Cvl;->b:J

    move-wide v9, v13

    .line 1948203
    sub-long v9, v7, v9

    iget v6, v2, LX/CvQ;->j:I

    int-to-long v11, v6

    cmp-long v6, v9, v11

    if-gez v6, :cond_4

    .line 1948204
    const/4 v5, 0x1

    .line 1948205
    :goto_1
    move v2, v5

    .line 1948206
    if-nez v2, :cond_0

    .line 1948207
    iget-object v2, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->a:LX/0g8;

    iget v3, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->c:I

    invoke-interface {v2, v3}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v2

    .line 1948208
    iget-object v3, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->d:LX/CvQ;

    iget-object v3, v3, LX/CvQ;->h:LX/1BX;

    iget-object v4, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->a:LX/0g8;

    invoke-virtual {v3, v0, v4, v2}, LX/1BX;->a(LX/1Qr;LX/0g8;Landroid/view/View;)Z

    move-result v0

    .line 1948209
    iget-object v2, p0, Lcom/facebook/search/logging/SearchResultsFeedLoggingViewportEventListener$1;->d:LX/CvQ;

    invoke-static {v2, v1, v0}, LX/CvQ;->a$redex0(LX/CvQ;Lcom/facebook/graphql/model/FeedUnit;Z)V

    goto :goto_0

    .line 1948210
    :cond_4
    iput-wide v7, v5, LX/Cvl;->b:J

    .line 1948211
    const/4 v5, 0x0

    goto :goto_1
.end method
