.class public Lcom/facebook/search/model/TrendingTypeaheadUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""

# interfaces
.implements LX/CwB;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/Boolean;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field private final k:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;

.field public final n:LX/103;

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950942
    invoke-virtual {p1, p0}, LX/Cwh;->a(Lcom/facebook/search/model/TrendingTypeaheadUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1950943
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950944
    invoke-virtual {p1, p0}, LX/Cwg;->a(Lcom/facebook/search/model/TrendingTypeaheadUnit;)V

    .line 1950945
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1950949
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950946
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1950947
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->f:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final h()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1950948
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->k:LX/0P1;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950940
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950941
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final jA_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950939
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final jB_()LX/CwF;
    .locals 1

    .prologue
    .line 1950938
    sget-object v0, LX/CwF;->trending:LX/CwF;

    return-object v0
.end method

.method public final jC_()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1950937
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/103;
    .locals 1

    .prologue
    .line 1950936
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->n:LX/103;

    return-object v0
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950935
    sget-object v0, LX/Cwb;->TRENDING:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950934
    const/4 v0, 0x1

    return v0
.end method

.method public final n()Lcom/facebook/search/model/ReactionSearchData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1950931
    const/4 v0, 0x0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950933
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TrendingTypeaheadUnit("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/search/model/TrendingTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") {, invalidated:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 1950932
    iget-object v0, p0, Lcom/facebook/search/model/TrendingTypeaheadUnit;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
