.class public Lcom/facebook/search/model/MainFilter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/model/MainFilter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/search/model/CustomFilterValue;

.field private g:Lcom/facebook/search/results/protocol/filters/FilterValue;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1950425
    new-instance v0, LX/CwJ;

    invoke-direct {v0}, LX/CwJ;-><init>()V

    sput-object v0, Lcom/facebook/search/model/MainFilter;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1950426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950427
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/model/MainFilter;->a:Ljava/lang/String;

    .line 1950428
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/model/MainFilter;->b:Ljava/lang/String;

    .line 1950429
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/model/MainFilter;->c:Ljava/lang/String;

    .line 1950430
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/model/MainFilter;->d:Ljava/lang/String;

    .line 1950431
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1950432
    sget-object v1, Lcom/facebook/search/results/protocol/filters/FilterValue;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1950433
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/model/MainFilter;->e:LX/0Px;

    .line 1950434
    const-class v0, Lcom/facebook/search/model/CustomFilterValue;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/CustomFilterValue;

    iput-object v0, p0, Lcom/facebook/search/model/MainFilter;->f:Lcom/facebook/search/model/CustomFilterValue;

    .line 1950435
    const-class v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    iput-object v0, p0, Lcom/facebook/search/model/MainFilter;->g:Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 1950436
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1950437
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1950438
    iget-object v0, p0, Lcom/facebook/search/model/MainFilter;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950439
    iget-object v0, p0, Lcom/facebook/search/model/MainFilter;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950440
    iget-object v0, p0, Lcom/facebook/search/model/MainFilter;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950441
    iget-object v0, p0, Lcom/facebook/search/model/MainFilter;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950442
    iget-object v0, p0, Lcom/facebook/search/model/MainFilter;->e:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1950443
    iget-object v0, p0, Lcom/facebook/search/model/MainFilter;->f:Lcom/facebook/search/model/CustomFilterValue;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1950444
    iget-object v0, p0, Lcom/facebook/search/model/MainFilter;->g:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1950445
    return-void
.end method
