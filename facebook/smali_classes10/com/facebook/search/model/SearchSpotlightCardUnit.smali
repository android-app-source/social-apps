.class public Lcom/facebook/search/model/SearchSpotlightCardUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I


# direct methods
.method public constructor <init>(LX/CwX;)V
    .locals 1

    .prologue
    .line 1950795
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadUnit;-><init>()V

    .line 1950796
    iget-object v0, p1, LX/CwX;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1950797
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/SearchSpotlightCardUnit;->a:Ljava/lang/String;

    .line 1950798
    iget v0, p1, LX/CwX;->b:I

    move v0, v0

    .line 1950799
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/search/model/SearchSpotlightCardUnit;->b:I

    .line 1950800
    return-void
.end method


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950794
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950793
    return-void
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950801
    sget-object v0, LX/Cwb;->NS_SEARCH_SPOTLIGHT:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950792
    const/4 v0, 0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950789
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchSpotlightCardUnit[body: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1950790
    iget-object v1, p0, Lcom/facebook/search/model/SearchSpotlightCardUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1950791
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
