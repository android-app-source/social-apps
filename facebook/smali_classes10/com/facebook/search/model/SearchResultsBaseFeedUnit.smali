.class public abstract Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
.super Lcom/facebook/graphql/model/BaseFeedUnit;
.source ""

# interfaces
.implements LX/CvV;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1950776
    invoke-direct {p0}, Lcom/facebook/graphql/model/BaseFeedUnit;-><init>()V

    .line 1950777
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->a:Ljava/lang/String;

    .line 1950778
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1950770
    iget-boolean v0, p0, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->b:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 1950771
    :cond_0
    :goto_0
    return-void

    .line 1950772
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->b:Z

    .line 1950773
    iput-object p1, p0, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1950775
    iget-object v0, p0, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1950774
    const/4 v0, 0x0

    return-object v0
.end method
