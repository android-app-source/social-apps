.class public Lcom/facebook/search/model/NullStateModuleSuggestionUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""

# interfaces
.implements LX/CwQ;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/3bj;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;

.field public final k:Ljava/lang/String;

.field public final l:Ljava/lang/String;

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final n:Z

.field private final o:LX/3bj;

.field public final p:Z

.field public q:Z


# direct methods
.method public constructor <init>(LX/CwP;)V
    .locals 1

    .prologue
    .line 1950526
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadUnit;-><init>()V

    .line 1950527
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->q:Z

    .line 1950528
    iget-object v0, p1, LX/CwP;->a:LX/3bj;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    .line 1950529
    iget-object v0, p1, LX/CwP;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->b:Ljava/lang/String;

    .line 1950530
    iget-object v0, p1, LX/CwP;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->c:Ljava/lang/String;

    .line 1950531
    iget-object v0, p1, LX/CwP;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->d:Ljava/lang/String;

    .line 1950532
    iget-object v0, p1, LX/CwP;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->e:Ljava/lang/String;

    .line 1950533
    iget-object v0, p1, LX/CwP;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->f:Ljava/lang/String;

    .line 1950534
    iget-object v0, p1, LX/CwP;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->g:Ljava/lang/String;

    .line 1950535
    iget-object v0, p1, LX/CwP;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->h:Ljava/lang/String;

    .line 1950536
    iget-object v0, p1, LX/CwP;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->i:Ljava/lang/String;

    .line 1950537
    iget-object v0, p1, LX/CwP;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->j:Ljava/lang/String;

    .line 1950538
    iget-object v0, p1, LX/CwP;->k:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->k:Ljava/lang/String;

    .line 1950539
    iget-object v0, p1, LX/CwP;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->l:Ljava/lang/String;

    .line 1950540
    iget-object v0, p1, LX/CwP;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->m:Ljava/lang/String;

    .line 1950541
    iget-boolean v0, p1, LX/CwP;->n:Z

    iput-boolean v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->n:Z

    .line 1950542
    iget-object v0, p1, LX/CwP;->o:LX/3bj;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->o:LX/3bj;

    .line 1950543
    iget-boolean v0, p1, LX/CwP;->p:Z

    iput-boolean v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->p:Z

    .line 1950544
    return-void
.end method


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950545
    invoke-virtual {p1, p0}, LX/Cwh;->a(Lcom/facebook/search/model/NullStateModuleSuggestionUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950546
    invoke-virtual {p1, p0}, LX/Cwg;->a(Lcom/facebook/search/model/NullStateModuleSuggestionUnit;)V

    .line 1950547
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1950548
    instance-of v0, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    if-nez v0, :cond_0

    .line 1950549
    const/4 v0, 0x0

    .line 1950550
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->j:Ljava/lang/String;

    check-cast p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    .line 1950551
    iget-object v1, p1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1950552
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1950553
    iget-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950554
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950555
    const/4 v0, 0x1

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1950556
    iget-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1950557
    iget-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->d:Ljava/lang/String;

    .line 1950558
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " \u22c5 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final x()LX/3bj;
    .locals 1

    .prologue
    .line 1950559
    iget-object v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->o:LX/3bj;

    return-object v0
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 1950560
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->q:Z

    .line 1950561
    return-void
.end method
