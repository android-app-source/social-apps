.class public Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LX/Cwm;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1950976
    new-instance v0, LX/Cwl;

    invoke-direct {v0}, LX/Cwl;-><init>()V

    sput-object v0, Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Cwm;)V
    .locals 0

    .prologue
    .line 1950977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950978
    iput-object p1, p0, Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;->a:LX/Cwm;

    .line 1950979
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1950980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950981
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Cwm;->valueOf(Ljava/lang/String;)LX/Cwm;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;->a:LX/Cwm;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1950982
    :goto_0
    return-void

    .line 1950983
    :catch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;->a:LX/Cwm;

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1950984
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1950985
    iget-object v0, p0, Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;->a:LX/Cwm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;->a:LX/Cwm;

    invoke-virtual {v0}, LX/Cwm;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950986
    return-void

    .line 1950987
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
