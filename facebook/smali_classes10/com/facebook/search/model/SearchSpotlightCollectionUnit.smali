.class public Lcom/facebook/search/model/SearchSpotlightCollectionUnit;
.super Lcom/facebook/search/model/TypeaheadCollectionUnit;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1950802
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadCollectionUnit;-><init>()V

    .line 1950803
    iput-object p1, p0, Lcom/facebook/search/model/SearchSpotlightCollectionUnit;->a:LX/0Px;

    .line 1950804
    return-void
.end method


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950805
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950806
    return-void
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950807
    sget-object v0, LX/Cwb;->NS_SEARCH_SPOTLIGHT:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950808
    const/4 v0, 0x0

    return v0
.end method

.method public final o()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1950809
    iget-object v0, p0, Lcom/facebook/search/model/SearchSpotlightCollectionUnit;->a:LX/0Px;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950810
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchSpotlightCollectionUnit["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/search/model/SearchSpotlightCollectionUnit;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
