.class public Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""


# instance fields
.field private final a:LX/Cwb;


# direct methods
.method public constructor <init>(LX/Cwb;)V
    .locals 0

    .prologue
    .line 1950568
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadUnit;-><init>()V

    .line 1950569
    iput-object p1, p0, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;->a:LX/Cwb;

    .line 1950570
    return-void
.end method


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950562
    invoke-virtual {p1, p0}, LX/Cwh;->a(Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950566
    invoke-virtual {p1, p0}, LX/Cwg;->a(Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;)V

    .line 1950567
    return-void
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950565
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;->a:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950564
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950563
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NullStateSeeMoreTypeaheadUnit{mGroupType="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/search/model/NullStateSeeMoreTypeaheadUnit;->a:LX/Cwb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
