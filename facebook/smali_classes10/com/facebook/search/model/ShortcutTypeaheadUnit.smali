.class public Lcom/facebook/search/model/ShortcutTypeaheadUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CwZ;)V
    .locals 1

    .prologue
    .line 1950853
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadUnit;-><init>()V

    .line 1950854
    iget-object v0, p1, LX/CwZ;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1950855
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->a:Ljava/lang/String;

    .line 1950856
    iget-object v0, p1, LX/CwZ;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1950857
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->b:Ljava/lang/String;

    .line 1950858
    iget-object v0, p1, LX/CwZ;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 1950859
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1950860
    iget-object v0, p1, LX/CwZ;->g:Landroid/net/Uri;

    move-object v0, v0

    .line 1950861
    iput-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->g:Landroid/net/Uri;

    .line 1950862
    iget-object v0, p1, LX/CwZ;->h:Landroid/net/Uri;

    move-object v0, v0

    .line 1950863
    iput-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->h:Landroid/net/Uri;

    .line 1950864
    iget-object v0, p1, LX/CwZ;->d:Landroid/net/Uri;

    move-object v0, v0

    .line 1950865
    iput-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->d:Landroid/net/Uri;

    .line 1950866
    iget-object v0, p1, LX/CwZ;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1950867
    iput-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->e:Ljava/lang/String;

    .line 1950868
    iget-object v0, p1, LX/CwZ;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1950869
    iput-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->f:Ljava/lang/String;

    .line 1950870
    return-void
.end method


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950882
    invoke-virtual {p1, p0}, LX/Cwh;->a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950880
    invoke-virtual {p1, p0}, LX/Cwg;->a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)V

    .line 1950881
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1950877
    instance-of v0, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;

    if-nez v0, :cond_0

    .line 1950878
    const/4 v0, 0x0

    .line 1950879
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->a:Ljava/lang/String;

    check-cast p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;

    iget-object v1, p1, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1950876
    iget-object v0, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950875
    sget-object v0, LX/Cwb;->ENTITY:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950874
    const/4 v0, 0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950871
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ShortcutTypeaheadUnit["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1950872
    iget-object v1, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1950873
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
