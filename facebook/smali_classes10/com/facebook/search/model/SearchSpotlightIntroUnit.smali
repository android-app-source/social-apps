.class public Lcom/facebook/search/model/SearchSpotlightIntroUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/CwY;)V
    .locals 1

    .prologue
    .line 1950812
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadUnit;-><init>()V

    .line 1950813
    iget-object v0, p1, LX/CwY;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1950814
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/SearchSpotlightIntroUnit;->a:Ljava/lang/String;

    .line 1950815
    return-void
.end method


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950822
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950821
    return-void
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950820
    sget-object v0, LX/Cwb;->NS_SEARCH_SPOTLIGHT:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950819
    const/4 v0, 0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950816
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SearchSpotlightIntroUnit[body: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1950817
    iget-object v1, p0, Lcom/facebook/search/model/SearchSpotlightIntroUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1950818
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
