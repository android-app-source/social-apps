.class public Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""


# instance fields
.field public final a:Lcom/facebook/placetips/bootstrap/PresenceDescription;


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950719
    invoke-virtual {p1, p0}, LX/Cwh;->a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950717
    invoke-virtual {p1, p0}, LX/Cwg;->a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)V

    .line 1950718
    return-void
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950716
    iget-object v0, p0, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->a:Lcom/facebook/placetips/bootstrap/PresenceDescription;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->a:Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950715
    sget-object v0, LX/Cwb;->PLACE_TIP:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950714
    const/4 v0, 0x1

    return v0
.end method
