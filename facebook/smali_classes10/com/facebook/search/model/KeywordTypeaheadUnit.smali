.class public Lcom/facebook/search/model/KeywordTypeaheadUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""

# interfaces
.implements LX/CwB;


# instance fields
.field private final A:Z

.field private final B:Z

.field public final C:Z

.field public final D:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final E:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final F:Z

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/Boolean;

.field public final g:LX/CwF;

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final k:Lcom/facebook/search/model/ReactionSearchData;

.field public final l:LX/CwI;

.field public final m:Ljava/lang/String;

.field public final n:Z

.field public final o:D

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:LX/103;

.field public final s:Ljava/lang/String;

.field public final t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final v:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final w:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field public final x:I

.field public final y:I

.field public final z:Z


# direct methods
.method public constructor <init>(LX/CwH;)V
    .locals 3

    .prologue
    .line 1950362
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadUnit;-><init>()V

    .line 1950363
    iget-object v0, p1, LX/CwH;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a:Ljava/lang/String;

    .line 1950364
    iget-object v0, p1, LX/CwH;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->d:Ljava/lang/String;

    .line 1950365
    iget-object v0, p1, LX/CwH;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b:Ljava/lang/String;

    .line 1950366
    iget-object v0, p1, LX/CwH;->x:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->c:LX/0Px;

    .line 1950367
    iget-object v0, p1, LX/CwH;->e:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->e:Ljava/lang/String;

    .line 1950368
    iget-object v0, p1, LX/CwH;->f:Ljava/lang/Boolean;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->f:Ljava/lang/Boolean;

    .line 1950369
    iget-object v0, p1, LX/CwH;->g:LX/CwF;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->g:LX/CwF;

    .line 1950370
    iget-object v0, p1, LX/CwH;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->h:Ljava/lang/String;

    .line 1950371
    iget-object v0, p1, LX/CwH;->i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 1950372
    iget-object v0, p1, LX/CwH;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->j:Ljava/lang/String;

    .line 1950373
    iget-object v0, p1, LX/CwH;->k:Lcom/facebook/search/model/ReactionSearchData;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->k:Lcom/facebook/search/model/ReactionSearchData;

    .line 1950374
    iget-object v0, p1, LX/CwH;->l:LX/CwI;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    .line 1950375
    iget-object v0, p1, LX/CwH;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->m:Ljava/lang/String;

    .line 1950376
    iget-boolean v0, p1, LX/CwH;->n:Z

    iput-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    .line 1950377
    iget-wide v0, p1, LX/CwH;->o:D

    iput-wide v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->o:D

    .line 1950378
    iget-object v0, p1, LX/CwH;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->q:Ljava/lang/String;

    .line 1950379
    iget-boolean v0, p1, LX/CwH;->q:Z

    iput-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->z:Z

    .line 1950380
    iget-object v0, p1, LX/CwH;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->p:Ljava/lang/String;

    .line 1950381
    iget-object v0, p1, LX/CwH;->s:LX/103;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->r:LX/103;

    .line 1950382
    iget-object v0, p1, LX/CwH;->t:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->s:Ljava/lang/String;

    .line 1950383
    iget-object v0, p1, LX/CwH;->u:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->t:Ljava/lang/String;

    .line 1950384
    iget-object v0, p1, LX/CwH;->y:LX/0P1;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->w:LX/0P1;

    .line 1950385
    iget v0, p1, LX/CwH;->B:I

    iput v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->x:I

    .line 1950386
    iget v0, p1, LX/CwH;->C:I

    iput v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->y:I

    .line 1950387
    iget-boolean v0, p1, LX/CwH;->v:Z

    iput-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->A:Z

    .line 1950388
    iget-boolean v0, p1, LX/CwH;->w:Z

    iput-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->B:Z

    .line 1950389
    iget-object v0, p1, LX/CwH;->z:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->u:Ljava/lang/String;

    .line 1950390
    iget-boolean v0, p1, LX/CwH;->D:Z

    iput-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->C:Z

    .line 1950391
    iget-object v0, p1, LX/CwH;->A:LX/0P1;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->v:LX/0P1;

    .line 1950392
    iget-object v0, p1, LX/CwH;->E:Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->D:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1950393
    iget-object v0, p1, LX/CwH;->F:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->E:Ljava/lang/String;

    .line 1950394
    iget-boolean v0, p1, LX/CwH;->G:Z

    iput-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->F:Z

    .line 1950395
    iget-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    sget-object v1, LX/CwI;->BOOTSTRAP:LX/CwI;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    sget-object v1, LX/CwI;->ENTITY_BOOTSTRAP:LX/CwI;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fetch source is inconsistent with bootstrap check for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1950396
    return-void

    .line 1950397
    :cond_1
    iget-object v0, p1, LX/CwH;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 1950398
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final D()Z
    .locals 1

    .prologue
    .line 1950399
    iget-boolean v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->B:Z

    return v0
.end method

.method public final E()Z
    .locals 2

    .prologue
    .line 1950360
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->r:LX/103;

    if-eqz v0, :cond_0

    sget-object v0, LX/CwF;->local:LX/CwF;

    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->g:LX/CwF;

    invoke-virtual {v0, v1}, LX/CwF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/CwF;->local_category:LX/CwF;

    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->g:LX/CwF;

    invoke-virtual {v0, v1}, LX/CwF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final J()I
    .locals 2

    .prologue
    .line 1950400
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->g:LX/CwF;

    if-eqz v0, :cond_0

    .line 1950401
    sget-object v0, LX/CwG;->a:[I

    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->g:LX/CwF;

    invoke-virtual {v1}, LX/CwF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1950402
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1950403
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 1950404
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1950405
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950406
    invoke-virtual {p1, p0}, LX/Cwh;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950407
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950408
    invoke-virtual {p1, p0}, LX/Cwg;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V

    .line 1950409
    return-void
.end method

.method public final a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2

    .prologue
    .line 1950410
    invoke-virtual {p0}, Lcom/facebook/search/model/TypeaheadUnit;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1950411
    const-string v0, "selected_is_scoped_keyword"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1950412
    :cond_0
    const-string v0, "keyword_source"

    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1950413
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1950414
    invoke-virtual {p0}, Lcom/facebook/search/model/TypeaheadUnit;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1950415
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->r:LX/103;

    iget-object v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->w:LX/0P1;

    invoke-static {v0, v1, v2, v3}, LX/7BG;->a(LX/103;Ljava/lang/String;Ljava/lang/String;LX/0P1;)Ljava/lang/String;

    move-result-object v0

    .line 1950416
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950417
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1950418
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->f:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1950419
    instance-of v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-nez v0, :cond_0

    .line 1950420
    const/4 v0, 0x0

    .line 1950421
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a:Ljava/lang/String;

    check-cast p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final h()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1950361
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->w:LX/0P1;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1950345
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950346
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950347
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final jA_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950348
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final jB_()LX/CwF;
    .locals 1

    .prologue
    .line 1950349
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->g:LX/CwF;

    return-object v0
.end method

.method public final jC_()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1950350
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->c:LX/0Px;

    return-object v0
.end method

.method public final k()LX/103;
    .locals 1

    .prologue
    .line 1950351
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->r:LX/103;

    return-object v0
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950352
    sget-object v0, LX/Cwb;->KEYWORD:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950353
    const/4 v0, 0x1

    return v0
.end method

.method public final n()Lcom/facebook/search/model/ReactionSearchData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1950354
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->k:Lcom/facebook/search/model/ReactionSearchData;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950355
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950356
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "KeywordTypeaheadUnit("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") {type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bootstrap:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1950357
    iget-boolean v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v1, v1

    .line 1950358
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", invalidated:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/search/model/TypeaheadUnit;->D()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/CwI;
    .locals 1

    .prologue
    .line 1950359
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    return-object v0
.end method
