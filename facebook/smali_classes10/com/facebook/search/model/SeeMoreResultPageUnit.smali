.class public Lcom/facebook/search/model/SeeMoreResultPageUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""


# instance fields
.field public final a:Lcom/facebook/search/model/EntityTypeaheadUnit;


# direct methods
.method public constructor <init>(Lcom/facebook/search/model/EntityTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 1950839
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadUnit;-><init>()V

    .line 1950840
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    iput-object v0, p0, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 1950841
    return-void
.end method


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950842
    invoke-virtual {p1, p0}, LX/Cwh;->a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950832
    invoke-virtual {p1, p0}, LX/Cwg;->a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)V

    .line 1950833
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1950834
    instance-of v0, p1, Lcom/facebook/search/model/SeeMoreResultPageUnit;

    if-nez v0, :cond_0

    .line 1950835
    const/4 v0, 0x0

    .line 1950836
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    check-cast p1, Lcom/facebook/search/model/SeeMoreResultPageUnit;

    .line 1950837
    iget-object v1, p1, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-object v1, v1

    .line 1950838
    invoke-virtual {v0, v1}, Lcom/facebook/search/model/EntityTypeaheadUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1950831
    iget-object v0, p0, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    invoke-virtual {v0}, Lcom/facebook/search/model/EntityTypeaheadUnit;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950830
    sget-object v0, LX/Cwb;->ENTITY:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950829
    const/4 v0, 0x1

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950826
    iget-object v0, p0, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 1950827
    iget-object p0, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1950828
    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1950823
    iget-object v0, p0, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 1950824
    iget-object p0, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, p0

    .line 1950825
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
