.class public Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""

# interfaces
.implements LX/CwQ;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final d:Z

.field public final e:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:LX/CwF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:I

.field private final j:LX/3bj;

.field public final k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CwR;)V
    .locals 1

    .prologue
    .line 1950608
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadUnit;-><init>()V

    .line 1950609
    iget-object v0, p1, LX/CwR;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1950610
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    .line 1950611
    iget-object v0, p1, LX/CwR;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1950612
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    .line 1950613
    iget-boolean v0, p1, LX/CwR;->d:Z

    move v0, v0

    .line 1950614
    iput-boolean v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->d:Z

    .line 1950615
    iget-object v0, p1, LX/CwR;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 1950616
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1950617
    invoke-virtual {p0}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    .line 1950618
    iget-object v0, p1, LX/CwR;->f:Landroid/net/Uri;

    move-object v0, v0

    .line 1950619
    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->f:Landroid/net/Uri;

    .line 1950620
    iget-object v0, p1, LX/CwR;->g:Landroid/net/Uri;

    move-object v0, v0

    .line 1950621
    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->g:Landroid/net/Uri;

    .line 1950622
    iget-object v0, p1, LX/CwR;->h:LX/CwF;

    move-object v0, v0

    .line 1950623
    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    .line 1950624
    iget v0, p1, LX/CwR;->i:I

    move v0, v0

    .line 1950625
    iput v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->i:I

    .line 1950626
    iget-object v0, p1, LX/CwR;->j:LX/3bj;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->j:LX/3bj;

    .line 1950627
    iget-object v0, p1, LX/CwR;->k:LX/0Px;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->k:LX/0Px;

    .line 1950628
    iget-object v0, p1, LX/CwR;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->l:Ljava/lang/String;

    .line 1950629
    iget-object v0, p1, LX/CwR;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->m:Ljava/lang/String;

    .line 1950630
    return-void

    .line 1950631
    :cond_0
    iget-object v0, p1, LX/CwR;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 1950632
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;
    .locals 2

    .prologue
    .line 1950689
    new-instance v0, LX/CwR;

    invoke-direct {v0}, LX/CwR;-><init>()V

    .line 1950690
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1950691
    iput-object v1, v0, LX/CwR;->b:Ljava/lang/String;

    .line 1950692
    move-object v0, v0

    .line 1950693
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1950694
    iput-object v1, v0, LX/CwR;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1950695
    move-object v0, v0

    .line 1950696
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1950697
    iput-object v1, v0, LX/CwR;->a:Ljava/lang/String;

    .line 1950698
    move-object v0, v0

    .line 1950699
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v1, v1

    .line 1950700
    iput-object v1, v0, LX/CwR;->e:Landroid/net/Uri;

    .line 1950701
    move-object v0, v0

    .line 1950702
    const/4 v1, 0x1

    .line 1950703
    iput-boolean v1, v0, LX/CwR;->d:Z

    .line 1950704
    move-object v0, v0

    .line 1950705
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->u:Ljava/lang/String;

    move-object v1, v1

    .line 1950706
    iput-object v1, v0, LX/CwR;->l:Ljava/lang/String;

    .line 1950707
    move-object v0, v0

    .line 1950708
    invoke-virtual {v0}, LX/CwR;->j()Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;
    .locals 3

    .prologue
    .line 1950670
    new-instance v0, LX/CwR;

    invoke-direct {v0}, LX/CwR;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->b()Ljava/lang/String;

    move-result-object v1

    .line 1950671
    iput-object v1, v0, LX/CwR;->b:Ljava/lang/String;

    .line 1950672
    move-object v0, v0

    .line 1950673
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x361ab677

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1950674
    iput-object v1, v0, LX/CwR;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1950675
    move-object v0, v0

    .line 1950676
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    .line 1950677
    iput-object v1, v0, LX/CwR;->a:Ljava/lang/String;

    .line 1950678
    move-object v1, v0

    .line 1950679
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v0

    sget-object v2, LX/CwF;->escape:LX/CwF;

    if-ne v0, v2, :cond_0

    sget-object v0, LX/CwF;->keyword:LX/CwF;

    .line 1950680
    :goto_0
    iput-object v0, v1, LX/CwR;->h:LX/CwF;

    .line 1950681
    move-object v0, v1

    .line 1950682
    const/4 v1, 0x1

    .line 1950683
    iput-boolean v1, v0, LX/CwR;->d:Z

    .line 1950684
    move-object v0, v0

    .line 1950685
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jC_()LX/0Px;

    move-result-object v1

    .line 1950686
    iput-object v1, v0, LX/CwR;->k:LX/0Px;

    .line 1950687
    move-object v0, v0

    .line 1950688
    invoke-virtual {v0}, LX/CwR;->j()Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;
    .locals 3

    .prologue
    .line 1950633
    sget-object v0, LX/CwF;->keyword:LX/CwF;

    .line 1950634
    iget-object v1, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v1, v1

    .line 1950635
    sget-object v2, LX/CwF;->local:LX/CwF;

    if-eq v1, v2, :cond_0

    .line 1950636
    iget-object v1, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v1, v1

    .line 1950637
    sget-object v2, LX/CwF;->local_category:LX/CwF;

    if-ne v1, v2, :cond_1

    .line 1950638
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->h:LX/CwF;

    move-object v0, v0

    .line 1950639
    :cond_1
    new-instance v1, LX/CwR;

    invoke-direct {v1}, LX/CwR;-><init>()V

    .line 1950640
    iget-object v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1950641
    iput-object v2, v1, LX/CwR;->b:Ljava/lang/String;

    .line 1950642
    move-object v1, v1

    .line 1950643
    iget-object v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v2, v2

    .line 1950644
    iput-object v2, v1, LX/CwR;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1950645
    move-object v1, v1

    .line 1950646
    iget-object v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1950647
    iput-object v2, v1, LX/CwR;->a:Ljava/lang/String;

    .line 1950648
    move-object v1, v1

    .line 1950649
    iget-object v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 1950650
    iput-object v2, v1, LX/CwR;->e:Landroid/net/Uri;

    .line 1950651
    move-object v1, v1

    .line 1950652
    iput-object v0, v1, LX/CwR;->h:LX/CwF;

    .line 1950653
    move-object v0, v1

    .line 1950654
    const/4 v1, 0x1

    .line 1950655
    iput-boolean v1, v0, LX/CwR;->d:Z

    .line 1950656
    move-object v0, v0

    .line 1950657
    iget-object v1, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->k:LX/0Px;

    move-object v1, v1

    .line 1950658
    iput-object v1, v0, LX/CwR;->k:LX/0Px;

    .line 1950659
    move-object v0, v0

    .line 1950660
    iget-object v1, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->g:Landroid/net/Uri;

    move-object v1, v1

    .line 1950661
    iput-object v1, v0, LX/CwR;->g:Landroid/net/Uri;

    .line 1950662
    move-object v0, v0

    .line 1950663
    iget-object v1, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->l:Ljava/lang/String;

    move-object v1, v1

    .line 1950664
    iput-object v1, v0, LX/CwR;->l:Ljava/lang/String;

    .line 1950665
    move-object v0, v0

    .line 1950666
    iget-object v1, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->m:Ljava/lang/String;

    move-object v1, v1

    .line 1950667
    iput-object v1, v0, LX/CwR;->m:Ljava/lang/String;

    .line 1950668
    move-object v0, v0

    .line 1950669
    invoke-virtual {v0}, LX/CwR;->j()Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/search/model/ShortcutTypeaheadUnit;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;
    .locals 2

    .prologue
    .line 1950574
    new-instance v0, LX/CwR;

    invoke-direct {v0}, LX/CwR;-><init>()V

    .line 1950575
    iget-object v1, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1950576
    iput-object v1, v0, LX/CwR;->b:Ljava/lang/String;

    .line 1950577
    move-object v0, v0

    .line 1950578
    iget-object v1, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 1950579
    iput-object v1, v0, LX/CwR;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1950580
    move-object v0, v0

    .line 1950581
    iget-object v1, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1950582
    iput-object v1, v0, LX/CwR;->a:Ljava/lang/String;

    .line 1950583
    move-object v0, v0

    .line 1950584
    iget-object v1, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->d:Landroid/net/Uri;

    move-object v1, v1

    .line 1950585
    iput-object v1, v0, LX/CwR;->e:Landroid/net/Uri;

    .line 1950586
    move-object v0, v0

    .line 1950587
    iget-object v1, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->g:Landroid/net/Uri;

    move-object v1, v1

    .line 1950588
    iput-object v1, v0, LX/CwR;->f:Landroid/net/Uri;

    .line 1950589
    move-object v0, v0

    .line 1950590
    iget-object v1, p0, Lcom/facebook/search/model/ShortcutTypeaheadUnit;->h:Landroid/net/Uri;

    move-object v1, v1

    .line 1950591
    iput-object v1, v0, LX/CwR;->g:Landroid/net/Uri;

    .line 1950592
    move-object v0, v0

    .line 1950593
    const/4 v1, 0x1

    .line 1950594
    iput-boolean v1, v0, LX/CwR;->d:Z

    .line 1950595
    move-object v0, v0

    .line 1950596
    invoke-virtual {v0}, LX/CwR;->j()Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950607
    invoke-virtual {p1, p0}, LX/Cwh;->a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950605
    invoke-virtual {p1, p0}, LX/Cwg;->a(Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;)V

    .line 1950606
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1950709
    instance-of v0, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    if-nez v0, :cond_0

    move v0, v1

    .line 1950710
    :goto_0
    return v0

    .line 1950711
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    invoke-virtual {v0}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1950712
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    check-cast p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    iget-object v1, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1950713
    :cond_1
    iget-object v2, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    iget-object v0, v0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    check-cast p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    iget-object v2, p1, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1950604
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950603
    sget-object v0, LX/Cwb;->RECENT:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950602
    const/4 v0, 0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950599
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NullStateSuggestionTypeaheadUnit("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1950600
    iget-object v1, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1950601
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") {iskeyword: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->z()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x()LX/3bj;
    .locals 1

    .prologue
    .line 1950598
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->j:LX/3bj;

    return-object v0
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 1950597
    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x361ab677

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->c:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, -0x1bce060e

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
