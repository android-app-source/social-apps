.class public Lcom/facebook/search/model/ReactionSearchData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/search/model/ReactionSearchData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1950728
    new-instance v0, LX/CwS;

    invoke-direct {v0}, LX/CwS;-><init>()V

    sput-object v0, Lcom/facebook/search/model/ReactionSearchData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 1950735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1950736
    iput-object p1, p0, Lcom/facebook/search/model/ReactionSearchData;->a:Ljava/lang/String;

    .line 1950737
    iput-object p2, p0, Lcom/facebook/search/model/ReactionSearchData;->b:Ljava/lang/String;

    .line 1950738
    iput-object p3, p0, Lcom/facebook/search/model/ReactionSearchData;->c:Ljava/lang/String;

    .line 1950739
    iput-object p4, p0, Lcom/facebook/search/model/ReactionSearchData;->d:Ljava/lang/String;

    .line 1950740
    iput-object p5, p0, Lcom/facebook/search/model/ReactionSearchData;->e:Ljava/lang/String;

    .line 1950741
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1950742
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1950729
    iget-object v0, p0, Lcom/facebook/search/model/ReactionSearchData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950730
    iget-object v0, p0, Lcom/facebook/search/model/ReactionSearchData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950731
    iget-object v0, p0, Lcom/facebook/search/model/ReactionSearchData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950732
    iget-object v0, p0, Lcom/facebook/search/model/ReactionSearchData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950733
    iget-object v0, p0, Lcom/facebook/search/model/ReactionSearchData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1950734
    return-void
.end method
