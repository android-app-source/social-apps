.class public Lcom/facebook/search/model/EntityTypeaheadUnit;
.super Lcom/facebook/search/model/TypeaheadUnit;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field public final e:Landroid/net/Uri;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

.field public final j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final k:Z

.field public final l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

.field public final m:Z

.field public final n:D

.field public final o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final p:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Z

.field private final r:Z

.field private final s:Z

.field public final t:Z

.field public final u:Ljava/lang/String;

.field public final v:I

.field public final w:Z


# direct methods
.method public constructor <init>(LX/Cw7;)V
    .locals 4

    .prologue
    .line 1950020
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadUnit;-><init>()V

    .line 1950021
    iget-object v0, p1, LX/Cw7;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1950022
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    .line 1950023
    iget-object v0, p1, LX/Cw7;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1950024
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    .line 1950025
    iget-object v0, p1, LX/Cw7;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1950026
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->c:Ljava/lang/String;

    .line 1950027
    iget-object v0, p1, LX/Cw7;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v0, v0

    .line 1950028
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1950029
    iget-object v0, p1, LX/Cw7;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 1950030
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    .line 1950031
    iget-object v0, p1, LX/Cw7;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1950032
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->g:Ljava/lang/String;

    .line 1950033
    iget-object v0, p1, LX/Cw7;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1950034
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->f:Ljava/lang/String;

    .line 1950035
    iget-boolean v0, p1, LX/Cw7;->h:Z

    move v0, v0

    .line 1950036
    iput-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->h:Z

    .line 1950037
    iget-object v0, p1, LX/Cw7;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v0, v0

    .line 1950038
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 1950039
    iget-object v0, p1, LX/Cw7;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v0, v0

    .line 1950040
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1950041
    iget-boolean v0, p1, LX/Cw7;->k:Z

    move v0, v0

    .line 1950042
    iput-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->k:Z

    .line 1950043
    iget-object v0, p1, LX/Cw7;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object v0, v0

    .line 1950044
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1950045
    iget-boolean v0, p1, LX/Cw7;->m:Z

    move v0, v0

    .line 1950046
    iput-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    .line 1950047
    iget-wide v2, p1, LX/Cw7;->n:D

    move-wide v0, v2

    .line 1950048
    iput-wide v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->n:D

    .line 1950049
    iget-object v0, p1, LX/Cw7;->o:LX/0Px;

    move-object v0, v0

    .line 1950050
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->o:LX/0Px;

    .line 1950051
    iget-boolean v0, p1, LX/Cw7;->p:Z

    move v0, v0

    .line 1950052
    iput-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->q:Z

    .line 1950053
    iget-boolean v0, p1, LX/Cw7;->q:Z

    move v0, v0

    .line 1950054
    iput-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->r:Z

    .line 1950055
    iget-boolean v0, p1, LX/Cw7;->r:Z

    move v0, v0

    .line 1950056
    iput-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->s:Z

    .line 1950057
    iget-object v0, p1, LX/Cw7;->t:LX/0P1;

    move-object v0, v0

    .line 1950058
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->p:LX/0P1;

    .line 1950059
    iget-boolean v0, p1, LX/Cw7;->s:Z

    move v0, v0

    .line 1950060
    iput-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->t:Z

    .line 1950061
    iget-object v0, p1, LX/Cw7;->u:Ljava/lang/String;

    move-object v0, v0

    .line 1950062
    iput-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->u:Ljava/lang/String;

    .line 1950063
    iget v0, p1, LX/Cw7;->v:I

    move v0, v0

    .line 1950064
    iput v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->v:I

    .line 1950065
    iget-boolean v0, p1, LX/Cw7;->w:Z

    move v0, v0

    .line 1950066
    iput-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->w:Z

    .line 1950067
    return-void
.end method


# virtual methods
.method public final A()D
    .locals 2

    .prologue
    .line 1950018
    iget-wide v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->n:D

    return-wide v0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 1950019
    iget-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->s:Z

    return v0
.end method

.method public final E()Z
    .locals 1

    .prologue
    .line 1950068
    iget-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->r:Z

    return v0
.end method

.method public final K()Z
    .locals 2

    .prologue
    .line 1950016
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950017
    invoke-virtual {p1, p0}, LX/Cwh;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950001
    invoke-virtual {p1, p0}, LX/Cwg;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;)V

    .line 1950002
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1950003
    instance-of v0, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-nez v0, :cond_0

    .line 1950004
    const/4 v0, 0x0

    .line 1950005
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    check-cast p1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    iget-object v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1950015
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final l()LX/Cwb;
    .locals 1

    .prologue
    .line 1950006
    sget-object v0, LX/Cwb;->ENTITY:LX/Cwb;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950007
    const/4 v0, 0x1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1950008
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EntityTypeaheadUnit("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1950009
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1950010
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") {bootstrap: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1950011
    iget-boolean v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v1, v1

    .line 1950012
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", phonetic: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1950013
    iget-boolean v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->q:Z

    move v1, v1

    .line 1950014
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
