.class public Lcom/facebook/search/model/NullStateModuleCollectionUnit;
.super Lcom/facebook/search/model/TypeaheadCollectionUnit;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:LX/3bj;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final f:I

.field public final g:Z

.field public final h:I

.field public final i:LX/CwO;


# direct methods
.method public constructor <init>(LX/CwN;)V
    .locals 1

    .prologue
    .line 1950489
    invoke-direct {p0}, Lcom/facebook/search/model/TypeaheadCollectionUnit;-><init>()V

    .line 1950490
    iget-object v0, p1, LX/CwN;->a:LX/3bj;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->a:LX/3bj;

    .line 1950491
    iget-object v0, p1, LX/CwN;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->b:Ljava/lang/String;

    .line 1950492
    iget-object v0, p1, LX/CwN;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->c:Ljava/lang/String;

    .line 1950493
    iget-object v0, p1, LX/CwN;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->d:Ljava/lang/String;

    .line 1950494
    iget-object v0, p1, LX/CwN;->e:LX/0Px;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->e:LX/0Px;

    .line 1950495
    iget v0, p1, LX/CwN;->f:I

    iput v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->f:I

    .line 1950496
    iget-boolean v0, p1, LX/CwN;->g:Z

    iput-boolean v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->g:Z

    .line 1950497
    iget v0, p1, LX/CwN;->h:I

    iput v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->h:I

    .line 1950498
    iget-object v0, p1, LX/CwN;->i:LX/CwO;

    iput-object v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->i:LX/CwO;

    .line 1950499
    return-void
.end method


# virtual methods
.method public final a(LX/Cwh;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Cwh",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1950500
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/Cwg;)V
    .locals 0

    .prologue
    .line 1950501
    return-void
.end method

.method public final l()LX/Cwb;
    .locals 2

    .prologue
    .line 1950502
    iget-object v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->a:LX/3bj;

    if-nez v0, :cond_0

    .line 1950503
    sget-object v0, LX/Cwb;->NO_GROUP:LX/Cwb;

    .line 1950504
    :goto_0
    move-object v0, v0

    .line 1950505
    return-object v0

    .line 1950506
    :cond_0
    sget-object v0, LX/CwM;->a:[I

    iget-object v1, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->a:LX/3bj;

    invoke-virtual {v1}, LX/3bj;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1950507
    sget-object v0, LX/Cwb;->NO_GROUP:LX/Cwb;

    goto :goto_0

    .line 1950508
    :pswitch_0
    sget-object v0, LX/Cwb;->NS_PULSE:LX/Cwb;

    goto :goto_0

    .line 1950509
    :pswitch_1
    sget-object v0, LX/Cwb;->TRENDING:LX/Cwb;

    goto :goto_0

    .line 1950510
    :pswitch_2
    sget-object v0, LX/Cwb;->NS_SOCIAL:LX/Cwb;

    goto :goto_0

    .line 1950511
    :pswitch_3
    sget-object v0, LX/Cwb;->NS_INTERESTED:LX/Cwb;

    goto :goto_0

    .line 1950512
    :pswitch_4
    sget-object v0, LX/Cwb;->NEARBY:LX/Cwb;

    goto :goto_0

    .line 1950513
    :pswitch_5
    sget-object v0, LX/Cwb;->NS_SUGGESTED:LX/Cwb;

    goto :goto_0

    .line 1950514
    :pswitch_6
    sget-object v0, LX/Cwb;->RECENT:LX/Cwb;

    goto :goto_0

    .line 1950515
    :pswitch_7
    sget-object v0, LX/Cwb;->NS_TOP:LX/Cwb;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 1950516
    const/4 v0, 0x0

    return v0
.end method

.method public final o()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/NullStateModuleSuggestionUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1950517
    iget-object v0, p0, Lcom/facebook/search/model/NullStateModuleCollectionUnit;->e:LX/0Px;

    return-object v0
.end method
