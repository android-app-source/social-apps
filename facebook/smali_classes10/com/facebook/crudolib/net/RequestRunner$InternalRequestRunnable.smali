.class public final Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:LX/Ekj;

.field public final synthetic b:LX/Eky;


# direct methods
.method public constructor <init>(LX/Eky;LX/Ekj;)V
    .locals 0

    .prologue
    .line 2163964
    iput-object p1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->b:LX/Eky;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2163965
    iput-object p2, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163966
    return-void
.end method

.method public static a(LX/El0;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2163955
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2163956
    invoke-interface {p0}, LX/El0;->d()I

    move-result v2

    .line 2163957
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 2163958
    invoke-interface {p0, v0}, LX/El0;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163959
    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163960
    invoke-interface {p0, v0}, LX/El0;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163961
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163962
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2163963
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 2163897
    :try_start_0
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163898
    iget-object v1, v0, LX/Ekj;->d:LX/Ekl;

    move-object v0, v1

    .line 2163899
    :goto_0
    if-eqz v0, :cond_0

    .line 2163900
    invoke-interface {v0}, LX/Ekl;->a()LX/Ekv;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    invoke-virtual {v2}, LX/Ekj;->d()LX/Ekz;

    move-result-object v2

    invoke-interface {v1, v2}, LX/Ekv;->a(LX/Ekz;)V

    .line 2163901
    invoke-interface {v0}, LX/Ekl;->b()LX/Ekl;

    move-result-object v0

    goto :goto_0

    .line 2163902
    :cond_0
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    invoke-virtual {v0}, LX/Ekj;->g()V

    .line 2163903
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    invoke-virtual {v0}, LX/Ekj;->f()LX/El6;

    move-result-object v0

    .line 2163904
    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->b:LX/Eky;

    invoke-static {v1}, LX/Eky;->a(LX/Eky;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2163905
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Enqueued request for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163906
    iget-object v3, v2, LX/Ekj;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2163907
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2163908
    iget-object v2, v0, LX/El6;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2163909
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2163910
    iget-object v2, v0, LX/El6;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2163911
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a(LX/El0;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163912
    :cond_1
    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    invoke-virtual {v1}, LX/Ekj;->j()V

    .line 2163913
    move-object v0, v0

    .line 2163914
    invoke-static {p0}, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->b(Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2163915
    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->b:LX/Eky;

    iget-object v1, v1, LX/Eky;->a:LX/Ekr;

    .line 2163916
    iget-object v2, v1, LX/Ekr;->a:LX/El3;

    move-object v1, v2

    .line 2163917
    check-cast v0, LX/El6;

    .line 2163918
    new-instance v2, LX/ElA;

    invoke-direct {v2}, LX/ElA;-><init>()V

    .line 2163919
    iget-object v3, v0, LX/El6;->d:LX/ElB;

    move-object v3, v3

    .line 2163920
    iput-object v2, v3, LX/ElB;->a:LX/ElA;

    .line 2163921
    iget-object v3, v1, LX/El3;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 2163922
    iget-object v4, v0, LX/El6;->e:LX/15D;

    move-object v4, v4

    .line 2163923
    invoke-virtual {v3, v4}, Lcom/facebook/http/common/FbHttpRequestProcessor;->b(LX/15D;)LX/1j2;

    move-result-object v3

    .line 2163924
    invoke-virtual {v2, v3}, LX/ElA;->a(LX/1j2;)V

    .line 2163925
    iget-object v3, v2, LX/ElA;->a:LX/ElE;

    invoke-virtual {v3}, LX/ElE;->b()V

    .line 2163926
    move-object v0, v2

    .line 2163927
    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->b:LX/Eky;

    invoke-static {v1}, LX/Eky;->a(LX/Eky;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2163928
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Response for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163929
    iget-object v3, v2, LX/Ekj;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2163930
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, LX/ElA;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, LX/ElA;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a(LX/El0;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2163931
    :cond_2
    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163932
    iget-object v2, v1, LX/Ekj;->e:LX/Ekk;

    move-object v2, v2

    .line 2163933
    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163934
    iget-object v3, v1, LX/Ekj;->c:LX/Eko;

    move-object v3, v3

    .line 2163935
    if-eqz v3, :cond_6

    const/4 v3, 0x1

    :goto_1
    move v1, v3

    .line 2163936
    if-eqz v1, :cond_3

    .line 2163937
    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163938
    iget-object v3, v1, LX/Ekj;->c:LX/Eko;

    move-object v1, v3

    .line 2163939
    move-object v1, v1

    .line 2163940
    check-cast v1, LX/Eko;

    iget-object v3, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    invoke-interface {v1, v3, v0}, LX/Eko;->a(LX/Ekj;LX/ElA;)V

    .line 2163941
    :cond_3
    iget-boolean v1, v2, LX/Ekk;->a:Z

    move v1, v1

    .line 2163942
    if-nez v1, :cond_5

    .line 2163943
    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163944
    iget-object v2, v1, LX/Ekj;->f:LX/FXM;

    move-object v1, v2

    .line 2163945
    invoke-virtual {v1, v0}, LX/FXM;->a(LX/ElA;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2163946
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    invoke-virtual {v0}, LX/Ekj;->k()V

    .line 2163947
    :goto_3
    return-void

    .line 2163948
    :catch_0
    move-exception v0

    .line 2163949
    :try_start_1
    invoke-static {p0, v0}, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a(Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;Ljava/io/IOException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2163950
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    invoke-virtual {v0}, LX/Ekj;->k()V

    goto :goto_3

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    invoke-virtual {v1}, LX/Ekj;->k()V

    throw v0

    .line 2163951
    :cond_5
    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->b:LX/Eky;

    invoke-static {v1}, LX/Eky;->a(LX/Eky;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2163952
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163953
    iget-object v3, v2, LX/Ekj;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2163954
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " response dropped due to caller cancelation"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;Ljava/io/IOException;)V
    .locals 3

    .prologue
    .line 2163876
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->b:LX/Eky;

    invoke-static {v0}, LX/Eky;->a(LX/Eky;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2163877
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163878
    iget-object v2, v1, LX/Ekj;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2163879
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2163880
    :cond_0
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163881
    iget-object v1, v0, LX/Ekj;->e:LX/Ekk;

    move-object v0, v1

    .line 2163882
    iget-boolean v1, v0, LX/Ekk;->a:Z

    move v0, v1

    .line 2163883
    if-nez v0, :cond_3

    .line 2163884
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163885
    iget-object v1, v0, LX/Ekj;->f:LX/FXM;

    move-object v0, v1

    .line 2163886
    const/4 v1, 0x3

    invoke-static {v1}, LX/01m;->b(I)Z

    .line 2163887
    iget-object v1, v0, LX/FXM;->d:LX/FXq;

    if-eqz v1, :cond_1

    .line 2163888
    iget-object v1, v0, LX/FXM;->d:LX/FXq;

    .line 2163889
    iget-object p0, v1, LX/FXq;->a:LX/FXS;

    if-eqz p0, :cond_1

    .line 2163890
    iget-object p0, v1, LX/FXq;->a:LX/FXS;

    invoke-interface {p0}, LX/FXS;->a()V

    .line 2163891
    :cond_1
    sget-object v1, LX/FWw;->a:LX/AUg;

    new-instance v2, LX/FWu;

    iget-object p0, v0, LX/FXM;->b:Ljava/lang/String;

    invoke-direct {v2, p0}, LX/FWu;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/AUg;->b(LX/AUI;)Z

    .line 2163892
    :cond_2
    :goto_0
    return-void

    .line 2163893
    :cond_3
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->b:LX/Eky;

    invoke-static {v0}, LX/Eky;->a(LX/Eky;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2163894
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163895
    iget-object v2, v1, LX/Ekj;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2163896
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " error dropped due to caller cancellation"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private static b(Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;)Z
    .locals 1

    .prologue
    .line 2163872
    iget-object v0, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163873
    iget-object p0, v0, LX/Ekj;->e:LX/Ekk;

    move-object v0, p0

    .line 2163874
    iget-boolean p0, v0, LX/Ekk;->a:Z

    move v0, p0

    .line 2163875
    return v0
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 2163863
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 2163864
    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    .line 2163865
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RequestRunner "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a:LX/Ekj;

    .line 2163866
    iget-object v4, v3, LX/Ekj;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2163867
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 2163868
    :try_start_0
    invoke-direct {p0}, Lcom/facebook/crudolib/net/RequestRunner$InternalRequestRunnable;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2163869
    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 2163870
    return-void

    .line 2163871
    :catchall_0
    move-exception v2

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    throw v2
.end method
