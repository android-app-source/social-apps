.class public Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private a:I

.field private b:F

.field private c:F

.field private d:Z

.field private e:Landroid/view/MotionEvent;

.field private f:Landroid/view/View$OnTouchListener;

.field public g:LX/DqI;

.field public h:LX/DqJ;

.field public i:LX/DqH;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2048222
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2048223
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 2048224
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->a:I

    .line 2048225
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2048226
    iget-object v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->f:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    .line 2048227
    iget-object v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->f:Landroid/view/View$OnTouchListener;

    iget-object v1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->e:Landroid/view/MotionEvent;

    invoke-interface {v0, p0, v1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2048228
    :goto_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->e:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 2048229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->e:Landroid/view/MotionEvent;

    .line 2048230
    return-void

    .line 2048231
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->e:Landroid/view/MotionEvent;

    invoke-virtual {p0, v0}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 0

    .prologue
    .line 2048232
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->requestDisallowInterceptTouchEvent(Z)V

    .line 2048233
    iput-boolean p1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->d:Z

    .line 2048234
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2048235
    iget-boolean v2, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->d:Z

    if-eqz v2, :cond_1

    .line 2048236
    :cond_0
    :goto_0
    return v0

    .line 2048237
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 2048238
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 2048239
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 2048240
    :pswitch_1
    iput v2, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->b:F

    .line 2048241
    iput v3, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->c:F

    .line 2048242
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->e:Landroid/view/MotionEvent;

    goto :goto_0

    .line 2048243
    :pswitch_2
    iget v4, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->b:F

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 2048244
    iget v4, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->c:F

    sub-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 2048245
    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 2048246
    iget v6, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->a:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_0

    .line 2048247
    iget-object v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->h:LX/DqJ;

    if-nez v0, :cond_2

    .line 2048248
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->a()V

    move v0, v1

    .line 2048249
    goto :goto_0

    .line 2048250
    :cond_2
    cmpl-float v0, v4, v2

    if-lez v0, :cond_4

    .line 2048251
    iget v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->c:F

    cmpg-float v0, v3, v0

    if-gez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->h:LX/DqJ;

    invoke-interface {v0}, LX/DqJ;->b()Z

    move-result v0

    .line 2048252
    :goto_1
    if-eqz v0, :cond_0

    .line 2048253
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->a()V

    goto :goto_0

    .line 2048254
    :cond_3
    iget-object v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->h:LX/DqJ;

    invoke-interface {v0}, LX/DqJ;->a()Z

    move-result v0

    goto :goto_1

    .line 2048255
    :cond_4
    invoke-direct {p0}, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->a()V

    move v0, v1

    .line 2048256
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2048257
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->i:LX/DqH;

    if-eqz v1, :cond_1

    .line 2048258
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->i:LX/DqH;

    invoke-interface {v0}, LX/DqH;->a()Z

    move-result v0

    .line 2048259
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x58bb5e9c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2048260
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomFrameLayout;->onSizeChanged(IIII)V

    .line 2048261
    iget-object v1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->g:LX/DqI;

    if-eqz v1, :cond_0

    .line 2048262
    iget-object v1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->g:LX/DqI;

    invoke-interface {v1}, LX/DqI;->a()V

    .line 2048263
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x19b801f7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnBackPressedListener(LX/DqH;)V
    .locals 0

    .prologue
    .line 2048264
    iput-object p1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->i:LX/DqH;

    .line 2048265
    return-void
.end method

.method public setOnSizeChangedListener(LX/DqI;)V
    .locals 0

    .prologue
    .line 2048266
    iput-object p1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->g:LX/DqI;

    .line 2048267
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .prologue
    .line 2048268
    iput-object p1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->f:Landroid/view/View$OnTouchListener;

    .line 2048269
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2048270
    return-void
.end method

.method public setSwipeCallback(LX/DqJ;)V
    .locals 0

    .prologue
    .line 2048271
    iput-object p1, p0, Lcom/facebook/notifications/lockscreen/ui/LockScreenFrameLayout;->h:LX/DqJ;

    .line 2048272
    return-void
.end method
