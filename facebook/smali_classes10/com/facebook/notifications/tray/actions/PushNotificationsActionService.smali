.class public Lcom/facebook/notifications/tray/actions/PushNotificationsActionService;
.super LX/1ZN;
.source ""


# instance fields
.field public a:Ljava/util/Set;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Drt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2049777
    const-string v0, "PushNotificationsActionService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 2049778
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;)LX/Drs;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2049779
    iget-object v0, p0, Lcom/facebook/notifications/tray/actions/PushNotificationsActionService;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Drt;

    .line 2049780
    invoke-interface {v0, p1}, LX/Drt;->a(Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;)LX/Drs;

    move-result-object v0

    .line 2049781
    if-eqz v0, :cond_0

    .line 2049782
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/notifications/model/SystemTrayNotification;Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2049783
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/notifications/tray/actions/PushNotificationsActionService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "push_action_extra"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notification_extra"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "notification_id_extra"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/notifications/tray/actions/PushNotificationsActionService;

    invoke-static {v0}, LX/3QB;->a(LX/0QB;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/tray/actions/PushNotificationsActionService;->a:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x5e2794eb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2049784
    const-string v0, "push_action_extra"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 2049785
    invoke-direct {p0, v0}, Lcom/facebook/notifications/tray/actions/PushNotificationsActionService;->a(Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;)LX/Drs;

    move-result-object v0

    .line 2049786
    if-eqz v0, :cond_0

    .line 2049787
    invoke-interface {v0, p1}, LX/Drs;->a(Landroid/content/Intent;)Z

    .line 2049788
    :cond_0
    const/16 v0, 0x25

    const v2, -0x61d0437a

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0xe86573

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2049789
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 2049790
    invoke-static {p0, p0}, Lcom/facebook/notifications/tray/actions/PushNotificationsActionService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2049791
    const/16 v1, 0x25

    const v2, 0x1fd69a6e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
