.class public Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

.field public final b:I

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2049795
    new-instance v0, LX/Drx;

    invoke-direct {v0}, LX/Drx;-><init>()V

    sput-object v0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 2049808
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049809
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->a:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 2049810
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->b:I

    .line 2049811
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->c:Ljava/lang/String;

    .line 2049812
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->d:Ljava/lang/String;

    .line 2049813
    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2049802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049803
    iput-object p1, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->a:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    .line 2049804
    iput p2, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->b:I

    .line 2049805
    iput-object p3, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->c:Ljava/lang/String;

    .line 2049806
    iput-object p4, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->d:Ljava/lang/String;

    .line 2049807
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 2049801
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2049796
    iget-object v0, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->a:Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPushNotifActionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2049797
    iget v0, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2049798
    iget-object v0, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2049799
    iget-object v0, p0, Lcom/facebook/notifications/tray/actions/logging/PushNotificationsActionLogObject;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2049800
    return-void
.end method
