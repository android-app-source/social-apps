.class public final Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/DqC;

.field public final synthetic b:LX/2jO;


# direct methods
.method public constructor <init>(LX/2jO;LX/DqC;)V
    .locals 0

    .prologue
    .line 2048050
    iput-object p1, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->b:LX/2jO;

    iput-object p2, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->a:LX/DqC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 2048051
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2048052
    iget-object v1, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->b:LX/2jO;

    iget-object v1, v1, LX/2jO;->i:LX/DqC;

    .line 2048053
    iget-object v2, v1, LX/DqC;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2048054
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2048055
    iget-object v1, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->b:LX/2jO;

    iget-object v1, v1, LX/2jO;->e:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/util/List;Z)V

    .line 2048056
    iget-object v0, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->b:LX/2jO;

    iget-object v0, v0, LX/2jO;->i:LX/DqC;

    if-eqz v0, :cond_0

    .line 2048057
    iget-object v0, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->b:LX/2jO;

    iget-object v0, v0, LX/2jO;->e:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iget-object v1, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->b:LX/2jO;

    iget-object v1, v1, LX/2jO;->i:LX/DqC;

    iget-object v1, v1, LX/DqC;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Ljava/util/List;)V

    .line 2048058
    iget-object v0, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->b:LX/2jO;

    iget-object v1, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->a:LX/DqC;

    .line 2048059
    iput-object v1, v0, LX/2jO;->i:LX/DqC;

    .line 2048060
    :goto_0
    return-void

    .line 2048061
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/action/NotificationsRowWithActionHelper$3;->b:LX/2jO;

    iget-object v0, v0, LX/2jO;->b:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mRowWithActionTaken was null when trying to delete row from cache"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
